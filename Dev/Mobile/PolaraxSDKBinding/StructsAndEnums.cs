﻿using System;

namespace PolaraxSDKBinding
{
	public enum PolaraxSDKStatus
	{
		NO_INTERNET, // NO 3G, NO WIFI

		INVALID_CUSTOMER_ID, // CUSTOMERID with spaces, too long customerId

		SYSTEM_ERROR,

		NETWORK_TIMEOUT,

		START_SUCCESS
	}

	public enum SDKMode
	{
		Dormant,
		LimmitedRun,
		ContinousRun
	}
}

