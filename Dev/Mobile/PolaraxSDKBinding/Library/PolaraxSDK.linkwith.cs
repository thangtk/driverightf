using System;
using ObjCRuntime;

[assembly: LinkWith ("PolaraxSDK.a",  LinkTarget.ArmV7 | LinkTarget.Arm64, ForceLoad = true,
	Frameworks = "UIKit SystemConfiguration Security QuartzCore OpenGLES GLKit Foundation CoreText CoreTelephony CoreMotion CoreGraphics AVFoundation CoreData ImageIO CoreLocation",
	LinkerFlags = "-lz", IsCxx = true)]
