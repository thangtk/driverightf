//
//  PolaraxSDKDelegate.h
//  polaraxSDK
//
//  Created by unicorn-kyhomac on 2/9/15.
//  Copyright (c) 2015 polarax. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PolaraxResponse.h"

//#ifndef PARTNER_DEFAULT
//#define PARTNER_DEFAULT = 1
//#endif

//#if PARTNER_DEFAULT == 1
//@compatibility_alias PolaraxSDK PolaraxSDK_Test;
//#elif PARTNER_DEFAULT == 2
//@compatibility_alias PolaraxSDK PolaraxSDK_Yesway;
//#elif PARTNER_DEFAULT == 3
////        @compatibility_alias View UIView;
//#endif

enum SDKMode : NSInteger
{
    Dormant,LimmitedRun,ContinousRun
};

typedef void (^handlePolaraxResponse)(PolaraxResponse * response);

@protocol PolaraxSDKDelegate <NSObject>
+(void)init;
+(void)init:(NSString*) customerId;
+(void)startService:(handlePolaraxResponse)result;
+(void)setCustomerId: (NSString *) customerId result: (handlePolaraxResponse)result;
+(void)changeMode:(enum SDKMode) mode;
+(void)change3GSetting:(BOOL) isEnable;
+(void) setLogLevel: (int)logLevel;

+ (NSString*)getCustomerId;
+ (NSString *)getUserName;

+ (BOOL)get3GSetting;
+ (enum SDKMode)getCurrentMode;
+ (id)getPreference:(NSString *)key;
+ (NSUInteger)getCountUnsyncedJourneys;
@optional



@end
