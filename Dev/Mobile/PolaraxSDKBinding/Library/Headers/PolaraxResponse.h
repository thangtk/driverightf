//
//  PolaraxResponse.h
//  polaraxSDK
//
//  Created by unicorn-kyhomac on 2/10/15.
//  Copyright (c) 2015 polarax. All rights reserved.
//

#import <Foundation/Foundation.h>

enum PolaraxStatusCode
{
    NO_INTERNET, // NO 3G, NO WIFI
    
    INVALID_CUSTOMER_ID, // CUSTOMERID with spaces, too long customerId
    
    SYSTEM_ERROR,
    
    NETWORK_TIMEOUT,
    
    START_SUCCESS
};


@interface PolaraxResponse : NSObject

@property (nonatomic) enum PolaraxStatusCode statusCode;
@property NSString *statusMessage;

+(PolaraxResponse *) getResponse: ( enum PolaraxStatusCode) errorCode;
@end
