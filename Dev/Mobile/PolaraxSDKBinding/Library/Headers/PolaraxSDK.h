//
//  PolaraxSDK_Test.h
//  PolaraxSDK
//
//  Created by unicorn-kyhomac on 2/12/15.
//  Copyright (c) 2015 polarax. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PolaraxResponse.h"
#import "PolaraxSDKDelegate.h"

@interface PolaraxSDK_Test : NSObject<PolaraxSDKDelegate>
@end

@compatibility_alias PolaraxSDK PolaraxSDK_Test ;
