﻿using System;
using UIKit;
using Foundation;

namespace DriveRightF.iOS
{
	public static class FontHub
	{
		public static readonly UIFont SegoeUIBold15 = UIFont.FromName("SegoeUI-Bold", 15f);
		public static readonly UIFont SegoeUIBold14 = UIFont.FromName("SegoeUI-Bold", 14f);
		public static readonly UIFont SegoeUIBold10 = UIFont.FromName("SegoeUI-Bold", 10f);
		public static readonly UIFont SegoeUIBold13 = UIFont.FromName("SegoeUI-Bold", 13f);
		public static readonly UIFont SegoeUIBold11 = UIFont.FromName("SegoeUI-Bold", 11f);
		public static readonly UIFont SegoeUIBold20 = UIFont.FromName("SegoeUI-Bold", 20f);

		public static readonly UIFont SegoeUIRegular10 = UIFont.FromName("SegoeUI", 10f);
		public static readonly UIFont SegoeUIRegular11 = UIFont.FromName("SegoeUI", 11f);
		public static readonly UIFont SegoeUIRegular12 = UIFont.FromName("SegoeUI", 12f);
		public static readonly UIFont SegoeUIRegular13 = UIFont.FromName("SegoeUI", 13f);
		public static readonly UIFont SegoeUIRegular14 = UIFont.FromName("SegoeUI", 14f);
		public static readonly UIFont SegoeUIRegular15 = UIFont.FromName("SegoeUI", 15f);
		public static readonly UIFont SegoeUIRegular17 = UIFont.FromName("SegoeUI", 17f);
		public static readonly UIFont RobotoRegular20 = UIFont.FromName("Roboto", 20f);

		public static readonly UIFont SegoeUILight10 = UIFont.FromName("SegoeUI-Light", 10f);
		public static readonly UIFont SegoeUILight11 = UIFont.FromName("SegoeUI-Light", 11f);
		public static readonly UIFont SegoeUILight12 = UIFont.FromName("SegoeUI-Light", 12f);
		public static readonly UIFont SegoeUILight14 = UIFont.FromName("SegoeUI-Light", 14f);
		public static readonly UIFont SegoeUILight13 = UIFont.FromName("SegoeUI-Light", 13f);
		public static readonly UIFont SegoeUILight15 = UIFont.FromName("SegoeUI-Light", 15f);
		public static readonly UIFont SegoeUILight20 = UIFont.FromName("SegoeUI-Light", 20f);

		public static readonly UIFont MyriadProRegular14 = UIFont.FromName("MyriadPro-Regular", 14f);
		public static readonly UIFont MyriadProRegular15 = UIFont.FromName("MyriadPro-Regular", 15f);
		public static readonly UIFont MyriadProRegular11 = UIFont.FromName("MyriadPro-Regular", 11f);
		public static readonly UIFont MyriadProRegular20 = UIFont.FromName("MyriadPro-Regular", 20f);

		public static readonly UIFont RobotoBold15 = UIFont.FromName("Roboto-Bold", 15f);
		public static readonly UIFont RobotoBold14 = UIFont.FromName("Roboto-Bold", 14f);
		public static readonly UIFont RobotoBold10 = UIFont.FromName("Roboto-Bold", 10f);
		public static readonly UIFont RobotoBold13 = UIFont.FromName("Roboto-Bold", 13f);
		public static readonly UIFont RobotoBold11 = UIFont.FromName("Roboto-Bold", 11f);
		public static readonly UIFont RobotoBold17 = UIFont.FromName("Roboto-Bold", 17f);
		public static readonly UIFont RobotoBold20 = UIFont.FromName("Roboto-Bold", 20f);
		public static readonly UIFont RobotoBold35 = UIFont.FromName("Roboto-Bold", 35f);

		public static readonly UIFont RobotoMedium11 = UIFont.FromName("Roboto-Medium", 11f);
		public static readonly UIFont RobotoMedium13 = UIFont.FromName("Roboto-Medium", 13f);
		public static readonly UIFont RobotoMedium15 = UIFont.FromName("Roboto-Medium", 15f);
		public static readonly UIFont RobotoMedium20 = UIFont.FromName ("Roboto-Medium", 20f);

		public static readonly UIFont RobotoRegular10 = UIFont.FromName("Roboto", 10f);
		public static readonly UIFont RobotoRegular11 = UIFont.FromName("Roboto", 11f);
		public static readonly UIFont RobotoRegular12 = UIFont.FromName("Roboto", 12f);
		public static readonly UIFont RobotoRegular13 = UIFont.FromName("Roboto", 13f);
		public static readonly UIFont RobotoRegular14 = UIFont.FromName("Roboto", 14f);
		public static readonly UIFont RobotoRegular15 = UIFont.FromName("Roboto", 15f);
		public static readonly UIFont RobotoRegular17 = UIFont.FromName("Roboto", 17f);
		public static readonly UIFont RobotoRegular22 = UIFont.FromName("Roboto", 22f);
		public static readonly UIFont RobotoRegular35 = UIFont.FromName("Roboto", 35f);

		public static readonly UIFont RobotoLight10 = UIFont.FromName("Roboto-Light", 10f);
		public static readonly UIFont RobotoLight11 = UIFont.FromName("Roboto-Light", 11f);
		public static readonly UIFont RobotoLight12 = UIFont.FromName("Roboto-Light", 12f);
		public static readonly UIFont RobotoLight14 = UIFont.FromName("Roboto-Light", 14f);
		public static readonly UIFont RobotoLight13 = UIFont.FromName("Roboto-Light", 13f);
		public static readonly UIFont RobotoLight15 = UIFont.FromName("Roboto-Light", 15f);
		public static readonly UIFont RobotoLight17 = UIFont.FromName("Roboto-Light", 17f);
		public static readonly UIFont RobotoLight20 = UIFont.FromName("Roboto-Light", 20f);
		public static readonly UIFont RobotoLight25 = UIFont.FromName("Roboto-Light", 25f);
		public static readonly UIFont RobotoLight30 = UIFont.FromName("Roboto-Light", 30f);

		public static readonly UIFont RobotoCondensedRegular45 = UIFont.FromName("RobotoCondensed-Regular", 45);

		public static UIFont HeaderFont(float fontSize = 18f)
		{
			NSArray languages = (NSArray)NSUserDefaults.StandardUserDefaults.ValueForKey(new NSString("AppleLanguages"));
			NSString language = languages.GetItem<NSString>(0);

			switch(language)
			{
			case "vi":
				return UIFont.FromName("Roboto-Light", fontSize);
			default:
				return UIFont.FromName("Roboto-Light", fontSize);
			}
		}

		public static UIFont ButtonFont(float fontSize = 16f)
		{
			NSArray languages = (NSArray)NSUserDefaults.StandardUserDefaults.ValueForKey(new NSString("AppleLanguages"));
			NSString language = languages.GetItem<NSString>(0);

			switch(language)
			{
			case "vi":
				return UIFont.FromName("Roboto-Light", fontSize);
			default:
				return UIFont.FromName("Roboto-Light", fontSize);
			}
		}

		public static UIFont CoachmaskFont(float fontSize)
		{
			return UIFont.FromName("Roboto-Light", fontSize);
		}

		public static UIFont CaptionGetStartedFont(float fontSize = 30f)
		{
			return UIFont.FromName("Roboto-Light", fontSize);
		}
	}
}

