﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Unicorn.Core.Dependency;

namespace Unicorn.Core
{
	public static class DependencyService
	{
		//
		// Static Fields
		//
		private static bool initialized = false;

		private static readonly List<Type> DependencyTypes = new List<Type> ();

		private static readonly Dictionary<Type, DependencyService.DependencyData> DependencyImplementations = new Dictionary<Type, DependencyService.DependencyData> ();

		//
		// Static Methods
		//
		private static Type FindImplementor (Type target)
		{
			return DependencyService.DependencyTypes.FirstOrDefault ((Type t) => (target.IsAssignableFrom (t) || target == t ));
		}

		public static T Get<T> (DependencyFetchTarget fetchTarget = DependencyFetchTarget.GlobalInstance) where T : class
		{
			if (!DependencyService.initialized) {
				DependencyService.Initialize ();
			}
			Type typeFromHandle = typeof(T);
			if (!DependencyService.DependencyImplementations.ContainsKey (typeFromHandle)) {
				Type type = DependencyService.FindImplementor (typeFromHandle);
				DependencyService.DependencyImplementations [typeFromHandle] = ((type != null) ? new DependencyService.DependencyData {
					ImplementorType = type
				} : null);
			}
			DependencyService.DependencyData dependencyData = DependencyService.DependencyImplementations [typeFromHandle];
			if (dependencyData == null) {
				return default(T);
			}
			if (fetchTarget == DependencyFetchTarget.GlobalInstance) {
				if (dependencyData.GlobalInstance == null) {
					dependencyData.GlobalInstance = Activator.CreateInstance (dependencyData.ImplementorType);
				}
				return (T)((object)dependencyData.GlobalInstance);
			}
			return (T)((object)Activator.CreateInstance (dependencyData.ImplementorType));
		}

		internal static void Initialize ()
		{
			//#if __IOS__
			Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies ();
			//#endif 

			Type typeFromHandle = typeof(DependencyAttribute);
			Assembly[] array = assemblies;
			for (int i = 0; i < array.Length; i++) {
				Assembly element = array [i];
				Attribute[] array2 = element.GetCustomAttributes (typeFromHandle).ToArray<Attribute> ();
				if (array2.Length != 0) {
					Attribute[] array3 = array2;
					for (int j = 0; j < array3.Length; j++) {
						DependencyAttribute dependencyAttribute = (DependencyAttribute)array3 [j];
						if (!DependencyService.DependencyTypes.Contains (dependencyAttribute.Implementor)) {
							DependencyService.DependencyTypes.Add (dependencyAttribute.Implementor);
						}
					}
				}
			}
			DependencyService.initialized = true;
		}

		public static void Register<T> () where T : class
		{
			Type typeFromHandle = typeof(T);
			if (!DependencyService.DependencyTypes.Contains (typeFromHandle)) {
				DependencyService.DependencyTypes.Add (typeFromHandle);
			}
		}

		public static void Register<T, TImpl> () where T : class where TImpl : class, T
		{
			Type typeFromHandle = typeof(T);
			Type typeFromHandle2 = typeof(TImpl);
			if (!DependencyService.DependencyTypes.Contains (typeFromHandle)) {
				DependencyService.DependencyTypes.Add (typeFromHandle);
			}
			DependencyService.DependencyImplementations [typeFromHandle] = new DependencyService.DependencyData {
				ImplementorType = typeFromHandle2
			};
		}

		public static bool RegisterGlobalInstance<T> (T instance) where T : class
		{
			if (!DependencyService.initialized) {
				DependencyService.Initialize ();
			}

			Type typeFromHandle = typeof(T);
			if (!DependencyService.DependencyImplementations.ContainsKey (typeFromHandle)) {
				Type type = DependencyService.FindImplementor (typeFromHandle);
				DependencyService.DependencyImplementations [typeFromHandle] = ((type != null) ? new DependencyService.DependencyData {
					ImplementorType = type
				} : null);
			}

			DependencyService.DependencyData dependencyData = DependencyService.DependencyImplementations [typeFromHandle];
			if (dependencyData == null) {
				return false;
			}
			dependencyData.GlobalInstance = instance;
			return true;
		}
		//
		// Nested Types
		//
		private class DependencyData
		{
			public Type ImplementorType {
				get;
				set;
			}

			public object GlobalInstance {
				get;
				set;
			}
		}
	}
}

