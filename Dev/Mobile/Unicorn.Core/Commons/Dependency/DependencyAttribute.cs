﻿using System;

namespace Unicorn.Core.Dependency
{
	[AttributeUsage (AttributeTargets.Assembly, AllowMultiple = true, Inherited = false)]
	public class DependencyAttribute : Attribute
	{
		//
		// Properties
		//
		internal Type Implementor {
			get;
			private set;
		}

		//
		// Constructors
		//
		public DependencyAttribute (Type implementorType)
		{
			this.Implementor = implementorType;
		}
	}
}

