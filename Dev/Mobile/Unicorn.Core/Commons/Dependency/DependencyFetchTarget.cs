﻿using System;

namespace Unicorn.Core.Dependency
{
	public enum DependencyFetchTarget
	{
		GlobalInstance,
		NewInstance
	}
}

