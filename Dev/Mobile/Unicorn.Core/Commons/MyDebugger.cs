﻿using System;
using System.Threading;
using System.Diagnostics;

namespace Unicorn.Core
{
	public static class MyDebugger
	{
		private const string TAG_PERFORMANCE = "Performance testing";
		public static void ShowThreadInfo(string threadName){
			System.Diagnostics.Debug.WriteLine (string.Format (">>> Thread: {0} >>> ID = {1}", threadName, Thread.CurrentThread.ManagedThreadId));
		}

		public static double CostOf(Action a, string name = "unnamed"){
			DateTime d = DateTime.Now;
			a.Invoke ();
			double cost = (DateTime.Now - d).TotalMilliseconds;
			Debug.WriteLine("{0} >>> {1} >>> Cost >>> {2}", TAG_PERFORMANCE, name, cost);
			return cost;
		}

		public static double Cost(this Action a, string name = "unnamed"){
			DateTime d = DateTime.Now;
			a.Invoke ();
			double cost = (DateTime.Now - d).TotalMilliseconds;
			Debug.WriteLine("{0} >>> {1} >>> Cost >>> {2}", TAG_PERFORMANCE, name, cost);
			return cost;
		}

		private static DateTime _startTime;
		public static void StartTracking(){
			_startTime = DateTime.Now;
		}

		public static void ShowCost(string name = "unnamed"){
			double cost = (DateTime.Now - _startTime).TotalMilliseconds;
			Debug.WriteLine("{0} >>> {1} >>> Cost >>> {2}", TAG_PERFORMANCE, name, cost);
			_startTime = DateTime.Now;
		}
	}
}

