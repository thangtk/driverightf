﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unicorn.Core.Storages;

namespace Unicorn.Core.Bases
{
	/// <summary>
	/// Abstract to use SQLite ORM.
	/// Sub classes don't have to add PrimaryKey, AutoIncrement attributes.
	/// </summary>
	public abstract class BaseModel : IEntity
    {
		public BaseModel(){
		}

		[PrimaryKey, AutoIncrement]
		public int Id { get; set;}
    }
		
	public interface IEntity
	{
		int Id { get; set; }
	}
}
