﻿using System;

namespace Unicorn.Core
{
	public static class DateExtension
	{
		public static bool CompareDateTo(this DateTime date1, DateTime date2)
		{
			return (date1.Day == date2.Day
				&& date1.Month == date2.Month
				&& date1.Year == date2.Year);
		}

		public static bool CompareDateBetween(this DateTime date1, DateTime dateFrom, DateTime dateTo)
		{
			int from = dateFrom.Year * 10000 + dateFrom.Month * 100 + dateFrom.Day;
			int to = dateTo.Year * 10000 + dateTo.Month * 100 + dateTo.Day;
			int current = date1.Year * 10000 + date1.Month * 100 + date1.Day;

			return (current >= from && current <= to);
		}

		// Ho Sy Ky update unixRef to utc time 19-September-2014
		private static DateTime _unixRef = new DateTime(1970, 1, 1, 0, 0, 0,DateTimeKind.Utc);
		public static double ToUnixTimeStamp(this DateTime dt)
		{
			return (new TimeSpan(dt.Ticks - _unixRef.Ticks)).TotalMilliseconds;
		}

		public static DateTime ConvertTimeStampToDate(double timestamp)
		{
			return new DateTime(1970, 1, 1, 0, 0, 0,DateTimeKind.Utc).AddMilliseconds(timestamp);
		}

		public static string ToStringOrDefault(this DateTime? source, string format, string defaultValue)
		{
			if (source != null)
			{
				return source.Value.ToString(format);
			}

			return String.IsNullOrEmpty(defaultValue) ? String.Empty : defaultValue;
		}

		public static string ToStringOrDefault(this DateTime? source, string format)
		{
			return ToStringOrDefault(source, format, null);
		}

		public static float GetTimeZoneOffset(this DateTime source)
		{
			TimeZone timeZone = TimeZone.CurrentTimeZone;
			TimeSpan offset = timeZone.GetUtcOffset(DateTime.Now);

			return float.Parse(offset.TotalHours.ToString());
		}

		public static int MonthDiff(this DateTime d1, DateTime d2)
		{
			return (d1.Year - d2.Year) * 12 + (d1.Month - d2.Month);
		}
	}

}

