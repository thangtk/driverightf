namespace Unicorn.Core.Enums
{

	public enum NavigatorAnimationType
	{
		None,
		Slide,
		Zoom,
		Fade,
	}

}
