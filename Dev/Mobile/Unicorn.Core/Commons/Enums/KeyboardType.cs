﻿using System;

namespace Unicorn.Core.Enums
{
	public enum KeyboardType
	{
		Number,
		Phone,
		Default,
		Email,
	}
}

