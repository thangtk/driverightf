﻿using System;

namespace Unicorn.Core.Enums
{
	public enum DialogIcon
	{
		None,
		Info,
		Warning,
		Error,
		Question,
	}
}

