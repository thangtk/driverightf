﻿using System;
using System.Reflection;

namespace Unicorn.Core
{
	public static class Unicorn
	{
		private static bool nevertrue;
		public static bool IsInitialized {
			get;
			private set;
		}

		static Unicorn ()
		{
			#if __IOS__
			Unicorn.nevertrue = false;
			if (Unicorn.nevertrue) {
				Assembly.GetCallingAssembly ();
			}
			#else
			Assembly callingAssembly = Assembly.GetCallingAssembly ();
			#endif
		}

		public static void Init ()
		{
			if (Unicorn.IsInitialized) {
				return;
			}
			Unicorn.IsInitialized = true;
			// Maybe will be implement
			DependencyService.Initialize ();
		}
	}
}

