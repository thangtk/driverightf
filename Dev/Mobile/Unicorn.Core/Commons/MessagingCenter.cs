﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Unicorn.Core
{
	public static class MessagingCenter
	{
		//
		// Static Fields
		//
		private static Dictionary<Tuple<string, Type, Type>, List<Tuple<WeakReference, Action<object, object>>>> callbacks = new Dictionary<Tuple<string, Type, Type>, List<Tuple<WeakReference, Action<object, object>>>> ();

		//
		// Static Methods
		//
		internal static void ClearSubscribers ()
		{
			MessagingCenter.callbacks.Clear ();
		}

		private static void InnerSend (string message, Type senderType, Type argType, object sender, object args)
		{
			if (message == null) {
				throw new ArgumentNullException ("message");
			}
			Tuple<string, Type, Type> key = new Tuple<string, Type, Type> (message, senderType, argType);
			if (!MessagingCenter.callbacks.ContainsKey (key)) {
				return;
			}
			List<Tuple<WeakReference, Action<object, object>>> list = MessagingCenter.callbacks [key];
			if (list == null || !list.Any<Tuple<WeakReference, Action<object, object>>> ()) {
				return;
			}
			foreach (Tuple<WeakReference, Action<object, object>> current in list) {
				if (current.Item1.IsAlive) {
					current.Item2 (sender, args);
				}
			}
		}

		private static void InnerSubscribe (object subscriber, string message, Type senderType, Type argType, Action<object, object> callback)
		{
			if (message == null) {
				throw new ArgumentNullException ("message");
			}
			Tuple<string, Type, Type> key = new Tuple<string, Type, Type> (message, senderType, argType);
			Tuple<WeakReference, Action<object, object>> item = new Tuple<WeakReference, Action<object, object>> (new WeakReference (subscriber), callback);
			if (MessagingCenter.callbacks.ContainsKey (key)) {
				MessagingCenter.callbacks [key].Add (item);
				return;
			}
			List<Tuple<WeakReference, Action<object, object>>> value = new List<Tuple<WeakReference, Action<object, object>>> {
				item
			};
			MessagingCenter.callbacks [key] = value;
		}

		private static void InnerUnsubscribe (string message, Type senderType, Type argType, object subscriber)
		{
			if (subscriber == null) {
				throw new ArgumentNullException ("subscriber");
			}
			if (message == null) {
				throw new ArgumentNullException ("message");
			}
			Tuple<string, Type, Type> key = new Tuple<string, Type, Type> (message, senderType, argType);
			if (!MessagingCenter.callbacks.ContainsKey (key)) {
				return;
			}
			MessagingCenter.callbacks [key].RemoveAll ((Tuple<WeakReference, Action<object, object>> tuple) => !tuple.Item1.IsAlive || tuple.Item1.Target == subscriber);
			if (!MessagingCenter.callbacks [key].Any<Tuple<WeakReference, Action<object, object>>> ()) {
				MessagingCenter.callbacks.Remove (key);
			}
		}

		public static void Send<TSender> (TSender sender, string message) where TSender : class
		{
			if (sender == null) {
				throw new ArgumentNullException ("sender");
			}
			MessagingCenter.InnerSend (message, typeof(TSender), null, sender, null);
		}

		public static void Send<TSender, TArgs> (TSender sender, string message, TArgs args) where TSender : class
		{
			if (sender == null) {
				throw new ArgumentNullException ("sender");
			}
			MessagingCenter.InnerSend (message, typeof(TSender), typeof(TArgs), sender, args);
		}

		public static void Subscribe<TSender, TArgs> (object subscriber, string message, Action<TSender, TArgs> callback, TSender source = null) where TSender : class
		{
			if (subscriber == null) {
				throw new ArgumentNullException ("subscriber");
			}
			if (callback == null) {
				throw new ArgumentNullException ("callback");
			}
			Action<object, object> callback2 = delegate (object sender, object args) {
				TSender tSender = (TSender)((object)sender);
				if (source == null || tSender == source) {
					callback ((TSender)((object)sender), (TArgs)((object)args));
				}
			};
			MessagingCenter.InnerSubscribe (subscriber, message, typeof(TSender), typeof(TArgs), callback2);
		}

		public static void Subscribe<TSender> (object subscriber, string message, Action<TSender> callback, TSender source = null) where TSender : class
		{
			if (subscriber == null) {
				throw new ArgumentNullException ("subscriber");
			}
			if (callback == null) {
				throw new ArgumentNullException ("callback");
			}
			Action<object, object> callback2 = delegate (object sender, object args) {
				TSender tSender = (TSender)((object)sender);
				if (source == null || tSender == source) {
					callback ((TSender)((object)sender));
				}
			};
			MessagingCenter.InnerSubscribe (subscriber, message, typeof(TSender), null, callback2);
		}

		public static void Unsubscribe<TSender> (object subscriber, string message) where TSender : class
		{
			MessagingCenter.InnerUnsubscribe (message, typeof(TSender), null, subscriber);
		}

		public static void Unsubscribe<TSender, TArgs> (object subscriber, string message) where TSender : class
		{
			MessagingCenter.InnerUnsubscribe (message, typeof(TSender), typeof(TArgs), subscriber);
		}
	}
}

