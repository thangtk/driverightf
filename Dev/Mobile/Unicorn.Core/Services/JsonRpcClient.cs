using System;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Specialized;
using System.Threading; 
using System.Net.Http;
using Unicorn.Core.Services.Base;


namespace DriveRightF.Core.Services
{
    public class JsonRpcClient : UBaseServiceClient
    {
	        private const string TAG = "JsonRpcClient";
	        
	        public JsonRpcClient(IServiceConfig config)
	            : base(config)
	        {
	        }

        public T InvokeService<T>(string method, string jsonRequest) where T : UBaseResponse, new()
        {
            T serviceResponse = new T();

            string jsonResponse = null;

            // int retriesCounter = _config.NumberOfRetry;

            try
            {
                jsonResponse = CallRequest(method, jsonRequest);
//				LogManager.WriteLog(TAG, "jsonResponse " + jsonResponse);
                System.Diagnostics.Debug.WriteLine("===>>> jsonResponse [SDK] >>> " + jsonResponse);
                serviceResponse = ParseResponse<T>(jsonResponse);
            }
            catch (WebException webEx)
            {
                System.Diagnostics.Debug.WriteLine("===>>> Error >>> " + webEx.ToString());
                if (webEx.Status == WebExceptionStatus.ConnectFailure)
                {
                    serviceResponse.ErrorType = ErrorType.NoInternetConnection;
                }
                else
                {
                    if (!string.IsNullOrEmpty(jsonResponse))
                    {
                        serviceResponse.ErrorType = ErrorType.ServiceFailed;
                        JObject responseObject = JObject.Parse(jsonResponse);
                        JToken responseMessage = responseObject["error"];
                        if (responseMessage != null)
                        {
                            serviceResponse.ErrorMessage = responseMessage.ToString();
                        }
                    }
                }
            }
            catch(JsonReaderException jsEx)
            {
                System.Diagnostics.Debug.WriteLine("===>>> Error >>> " + jsEx.ToString());
                return new T() { ErrorType = ErrorType.UnknownServiceResponse, ErrorMessage = jsEx.Message, Response = jsonResponse};
            }
            catch (AggregateException aex)
            {
                foreach (Exception ex in aex.InnerExceptions)
                {
                    WebException webEx = ex as WebException;
                    if (webEx != null)
                    {
                        System.Diagnostics.Debug.WriteLine("===>>> Error >>> " + ex.ToString());
                        if (webEx.Status == WebExceptionStatus.ConnectFailure)
                        {
                            serviceResponse.ErrorType = ErrorType.NoInternetConnection;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(jsonResponse))
                            {
                                serviceResponse.ErrorType = ErrorType.ServiceFailed;
                                JObject responseObject = JObject.Parse(jsonResponse);
                                JToken responseMessage = responseObject["error"];
                                if (responseMessage != null)
                                {
                                    serviceResponse.ErrorMessage = responseMessage.ToString();
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("===>>> Error >>> " + ex.ToString());
                return new T() { ErrorType = ErrorType.UnknownError, ErrorMessage = ex.Message, Response = jsonResponse};
            }

            return serviceResponse;
        }

        private string CallRequest(string method, string jsonRequest)
        {
            var client = new HttpClient();
            client.Timeout = new TimeSpan(0, 0, _config.RequestTimeout);
            HttpContent content = new StringContent(jsonRequest);
            var response = client.PostAsync(CreateRequestUri(method), content).Result;
            var jsonResponse = response.Content.ReadAsStringAsync().Result;
            return jsonResponse;
        }

        private T ParseResponse<T>(string jsonResponse) where T : UBaseResponse, new()
        {
            JObject responseObject = JObject.Parse(jsonResponse);

            T response;
            JToken successResponse = responseObject["success"];

            if (successResponse != null)
            {
                if (successResponse.ToObject<bool>())
                {
                    JToken responseMessage = responseObject["data"];
                    if (responseMessage != null)
                    {
                        if (responseMessage.ToString().Trim() != string.Empty)
                        {
                            response = JsonConvert.DeserializeObject<T>(responseMessage.ToString());
                        }
                        else
                        {
                            response = new T();
                        }
                        
                        response.Response = responseObject.ToString();
                        if (responseObject["error"] == null)
                        {
                            response.ErrorType = ErrorType.None;
                        }
                        else
                        {
                            response.ErrorType = ErrorType.ServiceFailed;
                            response.ErrorMessage = responseObject["error"].ToString();
                        }
                    }
                    else
                    {
                        response = new T() { ErrorType = ErrorType.UnknownServiceResponse };
                    }
                }
                else
                {
                    JToken responseMessage = responseObject["message"];
                    if (responseMessage != null)
                    {
                        response = new T() { ErrorType = ErrorType.ServiceFailed, ErrorMessage = responseMessage.ToString() };
                    }
                    else
                    {
                        response = new T() { ErrorType = ErrorType.UnknownServiceResponse };
                    }
                }
            }
            else
            {
                response = new T() { ErrorType = ErrorType.UnknownServiceResponse };
            }

            return response;
        }

        public T InvokeUploadService<T>(string method, string param, string filePath) where T : UBaseResponse, new()
        {
            string jsonResponse = string.Empty;

            try
            {
                int retriesCounter = _config.NumberOfRetry;
                while (retriesCounter-- > 0)
                {
                    try
                    {
                        jsonResponse = CallUploadFileRequest(method, param, filePath);
                        break;
                    }
                    catch (WebException webEx)
                    {
                        System.Diagnostics.Debug.WriteLine("===>>> Error >>> " + webEx.ToString());
                        if (webEx.Status != WebExceptionStatus.Timeout)
                        {
                            throw;
                        }

                        // Reach Max retries
                        if (retriesCounter <= 0)
                        {
                            throw new Exception("REQUEST_TIMEOUT", webEx);
                        }

                        Thread.Sleep(1000);
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("===>>> Error >>> " + ex.ToString());
                var errorMessage = "SERVICE_FAIL";

                if (ex.Message.Equals("REQUEST_TIMEOUT"))
                {
                    errorMessage = ex.Message;
                }
                else if (ex.Message.IndexOf("ConnectFailure") >= 0 || ex.Message.IndexOf("NameResolutionFailure") > 0)
                {
                    errorMessage = "NO_CONNECTION_ALERT";
                }

                return new T()
                {
                    Response = ServiceResponseType.FAIL,
                    ErrorMessage = errorMessage,
                    ErrorCode = errorMessage
                };
            }

            // Parse json response to objects
            return ParseResponse<T>(jsonResponse);
        }

        private string CallUploadFileRequest(string method, string param, string filePath)
        {
            string jsonResponse = string.Empty;
            string requestUri = _config.ActiveUploadUrl;
            requestUri = string.Format("{0}{1}/?{2}", requestUri, method, param);
            
            jsonResponse = HttpUploadFile(requestUri, filePath, "data", "application/x-zip-compressed");
            return  jsonResponse ;
        }

        public string HttpUploadFile(string url, string filePath, string dataName, string contentType = "application/octet-stream", NameValueCollection nvc = null)
        {
            if (nvc == null)
            {
                nvc = new NameValueCollection();
                nvc.Add("submit", "Submit");
            }

            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Method = "POST";
            wr.KeepAlive = true;
            wr.Credentials = System.Net.CredentialCache.DefaultCredentials;

            Stream rs = wr.GetRequestStream();

            string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            foreach (string key in nvc.Keys)
            {
                rs.Write(boundarybytes, 0, boundarybytes.Length);
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                rs.Write(formitembytes, 0, formitembytes.Length);
            }
            rs.Write(boundarybytes, 0, boundarybytes.Length);

            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            string header = string.Format(headerTemplate, dataName, Path.GetFileName(filePath), contentType);
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
            rs.Write(headerbytes, 0, headerbytes.Length);

            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                rs.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();

            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();

            WebResponse wresp = null;
            try
            {
                wresp = wr.GetResponse();
                Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                string response = reader2.ReadToEnd();
                return response;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("===>>> Error >>> " + ex.ToString());
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                }
            }
            finally
            {
                wr = null;
            }
            return string.Empty;
        }

        private string CreateRequestUri(string method)
        {
            return string.Format("{0}{1}", _config.ActiveServiceUrl, method);
        }

        private static string UpdateErrorMessage(JToken responseMessage)
        {
            string errorMessage = string.Empty;
            if (responseMessage.Type.ToString().Equals("Array"))
            {
                if (((JArray)responseMessage).Count == 1)
                {
                    if (string.IsNullOrEmpty(responseMessage[0].ToString()))
                    {
                        errorMessage = "SERVICE_FAIL";
                    }
                    else
                    {
                        errorMessage = responseMessage[0].ToString();
                    }
                }
                else
                {
                    errorMessage = responseMessage.ToString();
                }
            }
            else if (string.IsNullOrEmpty(responseMessage.ToString()))
            {
                errorMessage = "SERVICE_FAIL";
            }
            else
            {
                errorMessage = responseMessage.ToString();
            }
            return errorMessage;
        }
    }
}
