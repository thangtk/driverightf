﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using Newtonsoft.Json;
using Unicorn.Core.Translation;

namespace Unicorn.Core.Services.Base
{
    public enum ErrorType 
    {
//        RequestTimeout,
//        ServiceFailed,
//		NotFound

		None,
		RequestTimeout,
		NoInternetConnection,
		UnknownServiceResponse,
		ServiceFailed,
		NetworkError,
		UnknownError,
		NotFound
    }
	public class ServiceException: Exception{

		public ITranslator Translator {get {return DependencyService.Get<ITranslator>();}}

		private ErrorType _errorType; 
		public ErrorType ErrorType
		{
			get
			{
				return _errorType;
			}
			set
			{
				_errorType = value;

				if (_errorType == ErrorType.RequestTimeout)
				{
					ErrorMessage = Translator.Translate ("REQUEST_TIMEOUT");
					ErrorCode = Translator.Translate("REQUEST_TIMEOUT");
				}
				else if (_errorType == ErrorType.ServiceFailed)
				{
					ErrorMessage = Translator.Translate("SERVICE_FAIL");
					ErrorCode = Translator.Translate("SERVICE_FAIL");
				}
				else if (_errorType == ErrorType.NotFound)
				{
					ErrorMessage = Translator.Translate("NOT_FOUND");
					ErrorCode = Translator.Translate("NOT_FOUND");
				}
			}
		}

		public  string ErrorCode { get; set; }

		public  string ErrorMessage { get; set; }
	}

    public class UBaseResponse
    {
		private string[] NO_INTERNET_RETURN_STRINGS = { "REQUEST_TIMEOUT", "NO_CONNECTION_ALERT", "CONNECTION_REQUIRED","SERVICE_FAIL","NOT_FOUND" };
		public ITranslator Translator {get {return DependencyService.Get<ITranslator>();}}
		public UBaseResponse()
		{
			_errorType = ErrorType.None;
			ErrorMessage = "";
			ErrorCode = "";
		}

		private ErrorType _errorType;

		[JsonIgnore]
		public ErrorType ErrorType
		{
			get
			{
				return _errorType;
			}
			set
			{
				_errorType = value;

				if (_errorType == ErrorType.RequestTimeout)
				{
					ErrorMessage = Translator.Translate ("REQUEST_TIMEOUT");
					ErrorCode = Translator.Translate ("REQUEST_TIMEOUT");
				}
				else if (_errorType == ErrorType.ServiceFailed)
				{
					ErrorMessage = Translator.Translate ("SERVICE_FAIL");
					ErrorCode = Translator.Translate ("SERVICE_FAIL");
				}
				else if (_errorType == ErrorType.NotFound)
				{
					ErrorMessage = Translator.Translate ("NOT_FOUND");
					ErrorCode = Translator.Translate ("NOT_FOUND");
				}
			}
		}

		[JsonIgnore]
		public virtual string ErrorCode { get; set; }

		[JsonIgnore]
		public virtual string ErrorMessage { get; set; }
		[JsonIgnore]
		public virtual string Response { get; set; }

		[JsonIgnore]
		public virtual bool IsSuccess { get; set; }

		[JsonIgnore]
		public  string BaseURL { get; set; }

		public virtual void RepairData(){
		}
    }
}
