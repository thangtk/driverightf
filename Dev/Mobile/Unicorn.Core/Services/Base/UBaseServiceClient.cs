﻿using System;

namespace Unicorn.Core.Services.Base
{
    public abstract class UBaseServiceClient
    {
        protected IServiceConfig _config;

        public UBaseServiceClient(IServiceConfig config)
        {
            _config = config;
        }
    }
}