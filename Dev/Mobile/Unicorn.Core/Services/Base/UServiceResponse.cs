﻿using System;

namespace Unicorn.Core.Services.Base
{
    public class UServiceResponse
    {
        public string Response { get; set; }
		public object Message { get; set; }
        public string Error { get; set; }
        public string Info { get; set; }

        public UServiceResponse() { }

        public UServiceResponse(string type, string message)
        {
            Response = type;
            Message = message;
            Error = (type == ServiceResponseType.SUCCESS) ? string.Empty : Error = message;
        }
    }

    public class ServiceResponseType
    {
        public const string SUCCESS = "success";
        public const string FAIL = "fail";
    }
}