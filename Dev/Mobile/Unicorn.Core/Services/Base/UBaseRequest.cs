﻿using System;

namespace Unicorn.Core.Services.Base
{

	/// <summary>
	/// Request include Objet Body of POST + JSON body + file + query string ...
	/// </summary>
	[AttributeUsage(AttributeTargets.All, Inherited = true,AllowMultiple=false)]
	public class RequestInformationAttribute : Attribute
	{
	}
	/// <summary>
	/// child of RequestInformation
	/// </summary>
	[AttributeUsage(AttributeTargets.All, Inherited = true,AllowMultiple=false)]
	public class ObjectBodyAttribute : Attribute
	{
	}
	/// <summary>
	/// child of RequestInformation
	/// </summary>
	[AttributeUsage(AttributeTargets.All, Inherited = true,AllowMultiple=false)]
	public class JsonBodyAttribute : Attribute
	{
	}

	/// <summary>
	/// child of RequestInformation
	/// </summary>
	[AttributeUsage(AttributeTargets.All, Inherited = true,AllowMultiple=false)]
	public class ParamQueryStringAttribute : Attribute
	{

	}
	/// <summary>
	/// child of RequestInformation
	/// </summary>
	[AttributeUsage(AttributeTargets.All, Inherited = true,AllowMultiple=false)]
	public class ParamFileAttribute : Attribute
	{

	}

	/// <summary>
	/// Use object as Objet Body of POST
	/// </summary>
	[AttributeUsage(AttributeTargets.All, Inherited = true,AllowMultiple=false)]
	public class RequestPostObjectAttribute : Attribute
	{
	}
	/// <summary>
	/// Objet Body of JSON POST
	/// </summary>
	[AttributeUsage(AttributeTargets.All, Inherited = true,AllowMultiple=false)]
	public class RequestJsonAttribute : Attribute
	{
	}

	public class UBaseRequest
	{
	}
}

