﻿using System;
using Unicorn.Core.Services.Base;
using RestSharp;
using System.Reflection;
using Newtonsoft.Json;
using System.IO;
using System.Collections.Specialized;
using System.Net;

namespace Unicorn.Core.Services
{
	public abstract class URestFulClient : UBaseServiceClient
	{
		private const string TAG = "URestFulClient";
		protected RestClient _restClient ;
		private UJsonSerializer _ser = new UJsonSerializer ();

		public URestFulClient(IServiceConfig config)
			: base(config)
		{
			_restClient = new RestClient (config.ActiveServiceUrl);

			_restClient.AddHandler("application/json", new UJsonDeserializer());
			_restClient.AddHandler("text/json", new UJsonDeserializer());
			_restClient.AddHandler("text/x-json", new UJsonDeserializer());
			_restClient.AddHandler("text/javascript", new UJsonDeserializer());
			_restClient.AddHandler("*", new UJsonDeserializer());
			_restClient.Timeout = config.RequestTimeout;
			SettingUpRestClient (config,ref _restClient);

		}

		public virtual T InvokeGetWebService<T>(string Path, UBaseRequest request) where T:UBaseResponse,new(){
			return InvokeWebService<T> (Path,Method.GET,request);
		}
		public virtual T InvokePostWebService<T>(string Path, UBaseRequest request) where T:UBaseResponse,new(){
			return InvokeWebService<T> (Path,Method.POST,request);
		}
		public virtual T InvokeDeleteWebService<T>(string Path, UBaseRequest request) where T:UBaseResponse,new(){
			return InvokeWebService<T> (Path,Method.DELETE,request);
		}
		public virtual T InvokePutWebService<T>(string Path, UBaseRequest request) where T:UBaseResponse,new(){
			return InvokeWebService<T> (Path,Method.PUT,request);
		}

		protected virtual T InvokeWebService<T>(string Path, Method method,UBaseRequest request) where T:UBaseResponse,new(){
			var restRequest = new RestRequest (Path, method);
			restRequest.JsonSerializer = _ser;
			restRequest.RequestFormat = DataFormat.Json;
			restRequest.Timeout = _config.RequestTimeout;
			restRequest.AddHeader("Accept", "application/json");
			if (Attribute.IsDefined (request.GetType (), typeof(RequestInformationAttribute))) {
				PropertyInfo[] properties = request.GetType ().GetProperties (BindingFlags.Public | BindingFlags.Instance);
				foreach (PropertyInfo p in properties) {
					// If not writable then cannot null it; if not readable then cannot check it's value
					if (!p.CanWrite || !p.CanRead) {
						continue;
					}
					MethodInfo mget = p.GetGetMethod (false);
					MethodInfo mset = p.GetSetMethod (false);
					if (method == Method.POST) {
						restRequest.AlwaysMultipartFormData = true;
					}
					// Get and set methods have to be public
					if (mget == null) {
						continue;
					}
					if (mset == null) {
						continue;
					}
					if (Attribute.IsDefined (p, typeof(ObjectBodyAttribute))) {
						restRequest.AddBody (p.GetValue (request));
					} else if (Attribute.IsDefined (p, typeof(JsonBodyAttribute))) { 
						restRequest.AddJsonBody (p.GetValue (request));
					} else if (Attribute.IsDefined (p, typeof(ParamFileAttribute))) { 
						JsonPropertyAttribute jsonAttr =
							(JsonPropertyAttribute)Attribute.GetCustomAttribute (p, typeof(JsonPropertyAttribute));
						restRequest.AddFile (jsonAttr == null ? p.Name: jsonAttr.PropertyName,(string)p.GetValue (request));
					} else if (Attribute.IsDefined (p, typeof(JsonPropertyAttribute))) {
						JsonPropertyAttribute jsonAttr =
							(JsonPropertyAttribute)Attribute.GetCustomAttribute (p, typeof(JsonPropertyAttribute));
						var value = p.GetValue (request);
						if (value is DateTime) {
							value = _ser.Serialize (value).Replace ("\"", "");
						}
						if (Attribute.IsDefined (p, typeof(ParamQueryStringAttribute))) {
							restRequest.AddQueryParameter (jsonAttr == null ? p.Name:jsonAttr.PropertyName, value == null ? "" : p.GetValue (request).ToString ());
						} else {
							restRequest.AddParameter (jsonAttr == null ? p.Name:jsonAttr.PropertyName, value);
						}
						
					} else {
						var value = p.GetValue (request);
						if (value is DateTime) {
							value = _ser.Serialize (value).Replace ("\"", "");
						}
						if (Attribute.IsDefined (p, typeof(ParamQueryStringAttribute))) {
							restRequest.AddQueryParameter (p.Name, value == null ? "" : value.ToString ());
						} else {
							restRequest.AddParameter (p.Name, value);
						}

					}
				}
			} else if (Attribute.IsDefined (request.GetType (), typeof(RequestInformationAttribute))) {
				restRequest.AddBody (request);
			}else {
				restRequest.AddHeader("Content-Type", "application/json");
				restRequest.AddJsonBody (request);
			}

			RepairRequest (ref restRequest);

			var result = _restClient.Execute<T> (restRequest);

			if (result.Data != null && (result.StatusCode == System.Net.HttpStatusCode.OK || result.StatusCode == System.Net.HttpStatusCode.Created)) {
				result.Data.IsSuccess = true;
				result.Data.BaseURL = _config.ActiveServiceUrl;
				result.Data.RepairData ();
				return result.Data;
			} 

			//TODO: Check error type not found ???.
			if (result.StatusCode == System.Net.HttpStatusCode.NotFound) {
				ServiceException ex = new ServiceException ();
				ex.ErrorType = ErrorType.NotFound;
				throw ex;
			}

			T data = new T ();
			data.BaseURL = _config.ActiveServiceUrl;
			data.IsSuccess = false;
			if (result.ResponseStatus == ResponseStatus.TimedOut) {
				ServiceException ex = new ServiceException ();
				ex.ErrorType = ErrorType.RequestTimeout;
				throw ex;
			} else {
				ServiceException ex = new ServiceException ();
				ex.ErrorType = ErrorType.ServiceFailed;
				throw ex;
			}

		}
		public string UploadRawFileRequest(string path,Method method, string param, string filePath,string contentType = "image/png", NameValueCollection nvc = null)
		{
			string jsonResponse = string.Empty;
			string requestUri = _config.ActiveServiceUrl+path;
			jsonResponse = HttpUploadFile(requestUri, method, filePath, "image", contentType,nvc);

			return jsonResponse ;
		}

		private string HttpUploadFile(string url, Method method, string filePath, string dataName, string contentType = "application/octet-stream", NameValueCollection nvc = null)
		{
			if(nvc == null)
			{
				nvc = new NameValueCollection();
				//nvc.Add("submit", "Submit");
			}

			Console.WriteLine(string.Format("Uploading {0} to {1}", filePath, url));
			string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
			byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

			HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
			wr.ContentType = "multipart/form-data; boundary=" + boundary;
			wr.Method = method == Method.PUT ? "PUT" : "POST";
			wr.KeepAlive = true;
			wr.Credentials = System.Net.CredentialCache.DefaultCredentials;

			Stream rs = wr.GetRequestStream();

			string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
			foreach(string key in nvc.Keys)
			{
				rs.Write(boundarybytes, 0, boundarybytes.Length);
				string formitem = string.Format(formdataTemplate, key, nvc[key]);
				byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
				rs.Write(formitembytes, 0, formitembytes.Length);
			}
			rs.Write(boundarybytes, 0, boundarybytes.Length);

			string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
			string header = string.Format(headerTemplate, dataName, Path.GetFileName(filePath), contentType);
			byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
			rs.Write(headerbytes, 0, headerbytes.Length);

			FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
			byte[] buffer = new byte[4096];
			int bytesRead = 0;
			while((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
			{
				rs.Write(buffer, 0, bytesRead);
			}
			fileStream.Close();

			byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
			rs.Write(trailer, 0, trailer.Length);
			rs.Close();

			WebResponse wresp = null;
			try
			{
				wresp = wr.GetResponse();
				Stream stream2 = wresp.GetResponseStream();
				StreamReader reader2 = new StreamReader(stream2);
				string response = reader2.ReadToEnd();
				Console.WriteLine(string.Format("File uploaded, server response is: {0}", response));
				return response;
			}
			catch(Exception ex1)
			{
				ServiceException ex = new ServiceException ();
				ex.ErrorType = ErrorType.ServiceFailed;
				throw ex;
			}
			finally
			{
				wr = null;
			}
			return string.Empty;
		}


		protected virtual void SettingUpRestClient(IServiceConfig config,ref RestClient restClient)
		{
		}
		protected virtual void RepairRequest(ref RestRequest restRequest)
		{
		}


	}
}

