using System;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using Unicorn.Core.Services.Base;

namespace Unicorn.Core.Services
{
	public delegate void JsonAsyncCallback(string jsonResponseData,string error);
	public delegate void JsonAsyncCallback<TResponse>(TResponse response);
	public class JsonRpcAsyncClient : UBaseServiceClient
	{
		class RequestState
		{
			WebRequest _request;
			string _data;
			JsonAsyncCallback _callback;

			public RequestState(WebRequest request, string data, JsonAsyncCallback callback)
			{
				_request = request;
				_data = data;
				_callback = callback;
			}

			public WebRequest Request
			{
				get
				{
					return _request;
				}
			}

			public string Data
			{
				get
				{
					return _data;
				}
			}

			public JsonAsyncCallback Callback
			{
				get
				{
					return _callback;
				}
			}
		}

		public JsonRpcAsyncClient(IServiceConfig config)
            : base(config)
		{
		}

		public void InvokeService(string method, string jsonData, JsonAsyncCallback asyncCallback)
		{
			HttpWebRequest webRequest = WebRequest.Create(CreateRequestUri(method)) as HttpWebRequest;
			webRequest.Method = "POST";
			webRequest.ContentType = "application/json";

			webRequest.BeginGetRequestStream(new AsyncCallback(BeginRequestStream), new RequestState(webRequest, jsonData, asyncCallback));
		}

		private void BeginRequestStream(IAsyncResult result)
		{
			if (result.IsCompleted)
			{
				RequestState state = (RequestState)result.AsyncState;
				WebRequest request = (WebRequest)state.Request;
				string data = state.Data;
				Debug.WriteLine(data);
				using (StreamWriter writer = new StreamWriter(request.EndGetRequestStream(result)))
				{
					writer.Write(data);
				}

				request.BeginGetResponse(new AsyncCallback(BeginGetResponseHandler), state);
			}
		}

		private string CreateRequestUri(string method)
		{
			return string.Format("{0}{1}", _config.ActiveServiceUrl, method);
		}

		private void BeginGetResponseHandler(IAsyncResult result)
		{
			if (result.IsCompleted)
			{
				RequestState state = (RequestState)result.AsyncState;
				WebRequest request = (WebRequest)state.Request;

				try
				{
					HttpWebResponse response =
						(HttpWebResponse)request.EndGetResponse(result);

					if (state.Callback != null)
					{
						using (StreamReader reader = new StreamReader(response.GetResponseStream()))
						{
							state.Callback(reader.ReadToEnd(), null);
						}
					}
				}
				catch (WebException webex)
				{
                    System.Diagnostics.Debug.WriteLine("===>>> Error >>> " + webex.ToString());
					if (state.Callback != null)
					{
						state.Callback(null, webex.Message);
					}
				}
				catch (Exception ex)
				{
                    System.Diagnostics.Debug.WriteLine("===>>> Error >>> " + ex.ToString());
					if (state.Callback != null)
					{
						state.Callback(null, ex.Message);
					}
				}
			}
		}

		class JsonInvoker<TResponse> where TResponse : UBaseResponse, new()
		{
			string _method;
			string _jsonData;
			JsonAsyncCallback<TResponse> _callback;
			IServiceConfig _config;

			class RequestState
			{
				WebRequest _request;
				string _data;
				Timer _timeoutTimer;
				JsonAsyncCallback<TResponse> _callback;
				bool _isTimeout;

				public RequestState(WebRequest request, string data, JsonAsyncCallback<TResponse> callback)
				{
					_request = request;
					_data = data;
					_callback = callback;
					_isTimeout = false;
				}

				public WebRequest Request
				{
					get
					{
						return _request;
					}
				}

				public string Data
				{
					get
					{
						return _data;
					}
				}

				public Timer TimeoutTimer
				{
					get
					{
						return _timeoutTimer;
					}
					set
					{
						_timeoutTimer = value;
					}
				}

				public bool IsTimeout
				{
					get
					{
						return _isTimeout;
					}
					set
					{
						_isTimeout = value;
					}
				}

				public JsonAsyncCallback<TResponse> Callback
				{
					get
					{
						return _callback;
					}
				}
			}

			public JsonInvoker(string method, string jsonData, JsonAsyncCallback<TResponse> callback, IServiceConfig config)
			{
				_method = method;
				_jsonData = jsonData;
				_callback = callback;
				_config = config;
			}

			public void Invoke()
			{
				HttpWebRequest webRequest = WebRequest.Create(CreateRequestUri(_method)) as HttpWebRequest;
				webRequest.Method = "POST";
				webRequest.ContentType = "application/json";

				RequestState rs = new RequestState(webRequest, _jsonData, _callback);
				Debug.WriteLine(_jsonData);
				// implements timeout, if there is a timeout, the callback fires and the request become aborted
				Timer timeoutTimer = new Timer(delegate(object state)
				{
					HttpWebRequest wr = (HttpWebRequest)((RequestState)state).Request;
					wr.Abort();

					RequestState requestState = (RequestState)state;
					requestState.IsTimeout = true;
					TResponse serviceResponse = new TResponse() { ErrorType = ErrorType.RequestTimeout };

					if (requestState.Callback != null)
					{
						requestState.Callback(serviceResponse);
					}
				}, rs, _config.RequestTimeout, 0);

				rs.TimeoutTimer = timeoutTimer;

				webRequest.BeginGetRequestStream(new AsyncCallback(BeginRequestStream), rs);
			}

			private string CreateRequestUri(string method)
			{
				return string.Format("{0}{1}/json", _config.ActiveServiceUrl, method);
			}

			private void BeginRequestStream(IAsyncResult result)
			{
				if (result.IsCompleted)
				{

					RequestState state = (RequestState)result.AsyncState;
					WebRequest request = (WebRequest)state.Request;

					try
					{
						string data = state.Data;
						//Debug.WriteLine(data);
						using (StreamWriter writer = new StreamWriter(request.EndGetRequestStream(result)))
						{
							writer.Write(data);
						}

						request.BeginGetResponse(new AsyncCallback(BeginGetResponseHandler), state);
					}
					catch (WebException webEx)
					{
                        System.Diagnostics.Debug.WriteLine("===>>> Error >>> " + webEx.ToString());
						TResponse serviceResponse = new TResponse();

						if (webEx.Status == WebExceptionStatus.ConnectFailure)
						{
							serviceResponse.ErrorType = ErrorType.NoInternetConnection;
						}
						else
						{
							serviceResponse.ErrorType = ErrorType.NetworkError;
							serviceResponse.ErrorMessage = webEx.Message;
						}

						if (state.Callback != null)
						{
							if (!state.IsTimeout)
							{
								if (state.TimeoutTimer != null)
								{
									state.TimeoutTimer.Dispose();
								}

								state.Callback(serviceResponse);
							}
						}
					}
				}
			}

			private void BeginGetResponseHandler(IAsyncResult result)
			{
				TResponse serviceResponse = new TResponse();
				RequestState state = (RequestState)result.AsyncState;

				if (!result.IsCompleted)
				{
					if (!state.IsTimeout)
					{
						if (state.TimeoutTimer != null)
						{
							state.TimeoutTimer.Dispose();
						}

						state.Callback(serviceResponse);
					}

					return;
				}

				WebRequest request = (WebRequest)state.Request;
				string jsonResponse = "";

				try
				{
					HttpWebResponse response =
						(HttpWebResponse)request.EndGetResponse(result);


					using (StreamReader reader = new StreamReader(response.GetResponseStream()))
					{
						jsonResponse = reader.ReadToEnd();
					}

					Debug.WriteLine(jsonResponse);

					serviceResponse = ParseResponse(jsonResponse);
				}
				catch (JsonReaderException jsEx)
				{
                    System.Diagnostics.Debug.WriteLine("===>>> Error >>> " + jsEx.ToString());
					serviceResponse = new TResponse() { ErrorType = ErrorType.UnknownServiceResponse, ErrorMessage = jsEx.Message, Response = jsonResponse };
				}
				catch (AggregateException aex)
				{
					foreach (Exception ex in aex.InnerExceptions)
					{
                        System.Diagnostics.Debug.WriteLine("===>>> Error >>> " + ex.ToString());
						WebException webEx = ex as WebException;
						if (webEx != null)
						{
							if (webEx.Status == WebExceptionStatus.ConnectFailure)
							{
								serviceResponse.ErrorType = ErrorType.NoInternetConnection;
							}
							else
							{
								if (!string.IsNullOrEmpty(jsonResponse))
								{
									serviceResponse.ErrorType = ErrorType.ServiceFailed;
									JObject responseObject = JObject.Parse(jsonResponse);
									JToken responseMessage = responseObject["error"];
									if (responseMessage != null)
									{
										serviceResponse.ErrorMessage = responseMessage.ToString();
									}
								}
							}
						}
					}
				}
				catch (Exception ex)
				{
                    System.Diagnostics.Debug.WriteLine("===>>> Error >>> " + ex.ToString());
					serviceResponse = new TResponse() { ErrorType = ErrorType.UnknownError, ErrorMessage = ex.Message, Response = jsonResponse };
				}

				if (state.Callback != null)
				{
					if (!state.IsTimeout)
					{
						if (state.TimeoutTimer != null)
						{
							state.TimeoutTimer.Dispose();
						}

						state.Callback(serviceResponse);
					}
				}
			}

			private TResponse ParseResponse(string jsonResponse)
			{
				JObject responseObject = JObject.Parse(jsonResponse);

				TResponse response;
				JToken successResponse = responseObject["success"];

				if (successResponse != null)
				{
					if (successResponse.ToObject<bool>())
					{
						JToken responseMessage = responseObject["data"];
						JTokenType responseType = responseMessage.Type;

						if (responseMessage != null)
						{
							if (responseType == JTokenType.Object)
							{
								response = JsonConvert.DeserializeObject<TResponse>(responseMessage.ToString());
								response.Response = responseObject.ToString();
								if (responseObject["error"] == null)
								{
									response.ErrorType = ErrorType.None;
								}
								else
								{
									response.ErrorType = ErrorType.ServiceFailed;
									response.ErrorMessage = responseObject["error"].ToString();
								}
							}
							else
							{
								response = new TResponse() { ErrorType = ErrorType.None };
							}
						}
						else
						{
							response = new TResponse() { ErrorType = ErrorType.UnknownServiceResponse };
						}
					}
					else
					{
						JToken responseMessage = responseObject["message"];
						JToken errorCode = responseObject["code"];
						if (responseMessage != null)
						{
							response = new TResponse() { ErrorType = ErrorType.ServiceFailed, ErrorMessage = responseMessage.ToString() };
						}
						else
						{
							response = new TResponse() { ErrorType = ErrorType.UnknownServiceResponse };
						}

						if (errorCode != null)
						{
							response.ErrorCode = errorCode.ToString();
						}

						response.Response = responseObject.ToString();
					}
				}
				else
				{
					response = new TResponse() { ErrorType = ErrorType.UnknownServiceResponse };
				}

				return response;
			}
		}

		private List<object> _responsers = new List<object>();

		public void InvokeService<TResponse>(string method, string jsonData, JsonAsyncCallback<TResponse> asyncCallback) where TResponse : UBaseResponse, new()
		{
			JsonInvoker<TResponse> jsonInvoker = new JsonInvoker<TResponse>(method, jsonData, asyncCallback, _config);
			jsonInvoker.Invoke();
			_responsers.Add(jsonInvoker);
		}
	}
}

