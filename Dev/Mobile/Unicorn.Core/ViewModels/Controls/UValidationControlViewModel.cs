﻿using System;
using ReactiveUI;
using Unicorn.Core.Enums;
using System.Collections.Generic;

namespace DriveRightF.Core
{
	public class UValidationControlViewModel : UControlViewModel, IValidate
	{
		private string _error = string.Empty;
//		private Validation _validation;
		private List<Validation> _validations;

		public string Error
		{
			get{
				return _error;
			}
			set {
				this.RaiseAndSetIfChanged (ref _error, value);

			}
		}

//		public Validation Validation {
//			get{
//				return _validation;
//			}
//			set {
//				this.RaiseAndSetIfChanged (ref _validation, value);
//			}
//		}

		public List<Validation> Validations {
			get{
				return _validations;
			}
			set {
				this.RaiseAndSetIfChanged (ref _validations, value);
			}
		}

//		public bool CheckValid()
//		{
//			return true;
//
//		}

		public bool IsValid
		{
			get {
				this.HandleValidations (Validations);
				return string.IsNullOrEmpty (this.Error);
			}
		}

		public UValidationControlViewModel ():base()
		{
		}
	}
}

