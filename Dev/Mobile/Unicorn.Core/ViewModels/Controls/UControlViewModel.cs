﻿using System;
using ReactiveUI;
using Unicorn.Core.Enums;
using Unicorn.Core;

namespace DriveRightF.Core
{
	public class UControlViewModel : UBaseViewModel, IUControl
	{
		private string _text = string.Empty;
		private string _title = string.Empty;
		private bool _isEnable;

		public string Text
		{
			get{
				return _text;
			}
			set {
				this.RaiseAndSetIfChanged (ref _text, value);
			}
		}

		public string Title
		{
			get{
				return _title;
			}
			set {
				this.RaiseAndSetIfChanged (ref _title, value);
			}
		}

		public bool IsEnable {
			get{
				return _isEnable;
			}
			set {
				this.RaiseAndSetIfChanged (ref _isEnable, value);
			}
		}

		public UControlViewModel ():base()
		{
		}

		public int CurrentScreen {
			get;
			set;
		}

//		public override int ScreenToNotify {
//			get {
//				return CurrentScreen;
//			}
//		}

	}
}

