﻿using System;
using ReactiveUI;
using Unicorn.Core.Enums;

namespace DriveRightF.Core
{
	public class UTextBoxViewModel : UValidationControlViewModel, IUTextBox
	{
		private KeyboardType _keyboardType = KeyboardType.Default;
		public KeyboardType KeyboardType
		{
			get{
				return _keyboardType;
			}
			set {
				this.RaiseAndSetIfChanged (ref _keyboardType, value);
			}
		}

		public UTextBoxViewModel ():base()
		{
		}
	}
}

