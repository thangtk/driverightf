﻿using System;
using ReactiveUI;
using Unicorn.Core.UI;
using Unicorn.Core.Navigator;
using Unicorn.Core.Translation;

namespace Unicorn.Core
{
	public class UBaseViewModel:ReactiveObject
	{
		public PopupManager PopupManager {
			get{ 
				return DependencyService.Get<PopupManager> ();
			}
		}

		public IBaseNavigator Navigator{
			get { 
				return DependencyService.Get<INavigator> ();
			}
		}

		public ITranslator Translator {get {return DependencyService.Get<ITranslator>();}}
	}
}

