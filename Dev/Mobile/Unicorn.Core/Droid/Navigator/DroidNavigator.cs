using Android.Content;
using Android.OS;
using System;
using System.Collections.Generic;
using System.Linq;
using Android.Widget;
using Android.Graphics;
using System.Threading;
using Unicorn.Core.Navigator;

namespace Contemi.Droid.Common
{
    /// <summary>
    /// Implement navigation controller for Android
    /// </summary>
    public abstract class DroidNavigator : INavigator
    {
        public FragmentChangeActivity _fragmentActivity;

        private const long EXIT_DELAY_DURATION = 4000;
        private const int DELAY_SHOW_CONTENT = 300;

        private int _currentAppId = -1;

        public SlidingMenuSharp.SlidingMenu SlideMenu
        {
            get
            {
                if (_fragmentActivity != null)
                {
                    return _fragmentActivity.SlidingMenu;
                }

                return null;
            }
        }

        public int CurrentScreen
        {
            get { return _currentAppId; }
        }

        public bool SlidingMenuEnabled
        {
            get { return SlideMenu.IsSlidingEnabled; }
            set { SlideMenu.IsSlidingEnabled = value; }
        }

        public Android.App.Activity CurrentActivity
        {
            get { return _fragmentActivity; }
        }

        public DroidNavigator()
        {
        }

        public void Navigate(int screenId, int delay)
        {
            if (delay <= 0)
            {
                this.Navigate<object>(screenId, null);
            }
        }

        public void Navigate(int screenId)
        {
            this.Navigate<object>(screenId, null);
        }

        public void Navigate<T>(int screenId, T param) where T : class
        {
            this.Navigate<T, object>(screenId, param, null);
        }

        public void Navigate<T1, T2>(int screenId, T1 param1 = null, T2 param2 = null)
            where T2 : class
            where T1 : class
        {
            Console.WriteLine(string.Format("CurrentScren: {1}, To Screen: {0}", screenId, _currentAppId));

            if (_currentAppId == screenId)
            {
                HideMenuIfOpen();
                return;
            }

            BaseFragment contentFragment = InitFragment(screenId);

            if (contentFragment is BaseFragment<T1>)
            {
                ((BaseFragment<T1>)contentFragment).Load(param1);
            }
            else if (contentFragment is BaseFragment<T1, T2>)
            {
                ((BaseFragment<T1, T2>)contentFragment).Load(param1, param2);
            }

            _fragmentActivity.UpdateContentFragment(contentFragment, IsPlainView(screenId));
            _currentAppId = screenId;
            HideMenuIfOpen();

            SlidingMenuEnabled = !IsPlainView(screenId);
        }
        public void ExitApp()
        {

        }


        private void HideMenuIfOpen()
        {
            if (SlideMenu.IsMenuShowing)
            {
                SlideMenu.Toggle(true);
            }
        }

        //public void BackToPreviousPage()
        //{
        //    _currentAppId = _fragmentActivity.BackToPreviousFragment();
        //}

        public void NavigateToStart(int screenId)
        {
            _currentAppId = _fragmentActivity.BackToPreviousFragment(screenId);

            if (_currentAppId < 0)
            {
                Navigate(screenId);
            }
        }

        public void BackTo(int screenId)
        {
            Console.WriteLine(string.Format("BackTo : {0}", screenId));
            _currentAppId = _fragmentActivity.BackToPreviousFragment(screenId);
            
        }

        public void NavigateBack(bool isRefresh = false)
        {
            Console.WriteLine(string.Format("Go To NavigateBack"));

            _currentAppId = _fragmentActivity.BackToPreviousFragment(-1, isRefresh);
        }

        public void SetAnimator(IAnimator animator)
        {
            throw new NotImplementedException();
        }


        public void OpenMenu()
        {
            SlideMenu.ShowMenu(true);
        }

        public void CloseMenu()
        {
            SlideMenu.ShowContent(true);
        }

        public void ToggleMenu()
        {
            SlideMenu.Toggle(true);
        }

        protected abstract BaseFragment InitFragment(int appScreen, params object[] data);

        protected abstract Type GetActivityType(int appScreenId);

        protected virtual bool IsPlainView(int screenId) { return true; }
    }
}
