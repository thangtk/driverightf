using System;
using Android.Views;
using Unicorn.Core.UI;
using Unicorn.Core.Enums;
using Android.App;
using Android.Content;

namespace Unicorn.Core.Navigator
{

	public static class NavigatorTransition
	{
		public static void ViewNavigate(ViewGroup container, BaseViewWrapper fromScreen, BaseViewWrapper toScreen, bool isBack = false, NavigatorAnimationType type = NavigatorAnimationType.Slide)
		{
			if (container == null || toScreen == null)
			{
				throw new Exception("Container and destination screen cannot be null");
			}
			// Remove view from super view
			if (toScreen.View.Parent != null) {
				((ViewGroup)toScreen.View.Parent).RemoveView(toScreen.View);
			}
			int index = isBack ? container.ChildCount - 1 : container.ChildCount;
			container.AddView(toScreen.View, index);
			toScreen.View.Visibility = ViewStates.Visible;
			var animationScreen = isBack ? fromScreen : toScreen;
			if (type == NavigatorAnimationType.Slide)
			{
				animationScreen.View.SlideHorizontal(container.Width, !isBack, animationEnd: () =>
					{
						if (fromScreen != null)
						{
							// TODO: Check to ensure view has been removed
							fromScreen.View.Visibility = ViewStates.Invisible;
							container.RemoveView(fromScreen.View);
						}
						// invoke animation ended for to Screen
						toScreen.OnAnimationEnd();
					});
			} else if (type == NavigatorAnimationType.Zoom){
				animationScreen.View.Zoom(isBack, animationEnd: () =>
					{
						if (fromScreen != null)
						{
							// TODO: Check to ensure view has been removed
							container.RemoveView(fromScreen.View);
						}
						// invoke animation ended for to Screen
						toScreen.OnAnimationEnd();
					});
			}
		}

        /// <summary>
        /// For test only
        /// </summary>
        /// <param name="container"></param>
        /// <param name="fromScreen"></param>
        /// <param name="toScreen"></param>
        /// <param name="isBack"></param>
        /// <param name="type"></param>
        public static void FragmentNavigate(ViewGroup container, BaseFragment fromScreen, BaseFragment toScreen, bool isBack = false, NavigatorAnimationType type = NavigatorAnimationType.Slide)
        {
            var activity = DependencyService.Get<UBaseActivity>();
            var fm = activity.FragmentManager;
            var ft = fm.BeginTransaction();
            if (fromScreen == null)
            {
                ft.Add(container.Id, toScreen).Commit();
            }
            else {
                // TODO: impelement back
                // Use Fragment backstack??? - try it later
                if (type == NavigatorAnimationType.Slide)
                {
                    ft.SetCustomAnimations(Resource.Animation.slide_in_right, 0);
                }
                else { 
                    // Not implemented
                }
                ft.Add(container.Id, toScreen).Commit();
                ft.Remove(fromScreen);
            }
        }
	}

}
