﻿using System;
using System.Collections.Generic;
using Android.Views;
using Android.Content;
using Unicorn.Core.UI;
using Android.App;
using Unicorn.Core.Enums;

namespace Unicorn.Core.Navigator
{
	public abstract class BaseNavigator : INavigator
	{
		#region IBaseNavigator implementation

		public void Navigate (int screenId, object[] objs = null, object[] transitionParam = null)
		{
			NavigateTo (screenId, animationType: NavigatorAnimationType.Slide, args: objs);
		}

		public void NavigateBack (int screenId, bool isRefresh = false)
		{
		}

		public void NavigateToStart (int screenId)
		{
			throw new NotImplementedException ();
		}

		public void OpenMenu ()
		{
			throw new NotImplementedException ();
		}

		public void CloseMenu ()
		{
			throw new NotImplementedException ();
		}

		public void ToggleMenu ()
		{
			throw new NotImplementedException ();
		}

		#endregion

        public ScreenType ScreenType { get; set; }
        /// <summary>
        /// Enable back to exit app from RootView
        /// </summary>
        public bool EnableBackToExitApp { get; set; }
		public event Action<int> OnNavigated;

		private void RaiseOnNavigatedEvent(){
			if (OnNavigated != null) {
				OnNavigated (CurrentScreen);
			}
		}

		public BaseNavigator()
		{
			CurrentScreen = 0;
            ScreenType = Navigator.ScreenType.View;
            //ContainerViews = new List<ContainerView>();
            EnableBackToExitApp = true;
		}

		public BaseNavigator(ViewGroup containerView) : this(){
			ContainerView = containerView;
            //ContainerViews.Clear();
            //this.AddContainer (rootView);
		} 

		private Dictionary<int, INavigableScreen> _cachedScreens = new Dictionary<int, INavigableScreen>();
		public int CurrentScreen { get; set; }
        public ViewGroup ContainerView { get; set; }
        //public List<ContainerView> ContainerViews { get; set; }
		public Context Context {
			get{ 
				return DependencyService.Get<UBaseActivity> ();
			}
		}
		private bool _isBusy;

		public bool IsBusy
		{
			get { return _isBusy; }
			set
			{
				if (_isBusy != value)
				{
					_isBusy = value;
                    //ContainerViews.ForEach(v => v.Container.Enabled = !_isBusy);
                    ContainerView.Enabled = !_isBusy;
					// TODO: edit later (navigated)
					if(!_isBusy){
						RaiseOnNavigatedEvent ();
					}
				}
			}
		}

        //public void AddContainer(ViewGroup containerView){
        //    if (ContainerViews == null) {
        //        ContainerViews = new List<ContainerView> ();
        //    }
        //    ContainerViews.Add(new ContainerView(){
        //        Container = containerView
        //    });
        //}

		private INavigableScreen GetScreen(int screenId)
		{
			if (_cachedScreens.ContainsKey(screenId))
			{
				return _cachedScreens[screenId];
			}
			else
			{
				return GetScreenByKey(Context, screenId);
			}
		}

		public void NavigateTo(int toScreen, NavigatorAnimationType animationType = NavigatorAnimationType.Slide, bool parentIsCurrentScreen = true, params object[] args)
		{
			if (toScreen == CurrentScreen)
				return;
			// Impl:
			// Lock
			// Get ViewNavigator
			// Set parent screen for ViewNavigator
			// ____Default parent is current screen
			// Navigate
			// Call some private methods (LoadData...)
			// Cache
			// Remove previous view from RootView
			// Set Current Screen
			if (IsBusy) return;
			IsBusy = true;
			var toView = GetScreen((int)toScreen);
			if (toView == null) {
				throw new Exception ("Cannot exist screen with id is " + toScreen);
			}
			var fromView = GetScreen(CurrentScreen);
			if (!toView.IsRoot && parentIsCurrentScreen)
			{
				// SHOULD: get parent key recursively (in case fromView was not cached)
				toView.ParentKey = CurrentScreen;
			}
            //var containerView = GetContainerView(toView.ContainerViewIndex);
            if (ScreenType == Navigator.ScreenType.View) {
                // Skip check type
                NavigatorTransition.ViewNavigate(ContainerView, (BaseViewWrapper)fromView, (BaseViewWrapper)toView, type: animationType);
            }
            else if (ScreenType == Navigator.ScreenType.Fragment) {
                // Skip check type
                NavigatorTransition.FragmentNavigate(ContainerView, (BaseFragment)fromView, (BaseFragment)toView);
            }
            //
			if(!toView.NoCache){
				CacheScreen(toView);
                //containerView.CurrentScreenKey = toScreen;
			}
            CurrentScreen = toScreen;
			// TODO: review later
			toView.Args = args;
			toView.RefreshData (args);
			IsBusy = false;
		}

        //private ContainerView GetContainerView(int index) {
        //    if (index < 0 || index >= ContainerViews.Count)
        //    {
        //        throw new Exception("ContainerViewIndex is not implemented corretly (Derived class from INavigableScreen)");
        //    }
        //    else {
        //        return ContainerViews[index]; ;
        //    }
        //}

		private void CacheScreen(INavigableScreen view)
		{
			var key = view.Key;
			if (_cachedScreens.ContainsKey(key))
			{
				_cachedScreens[key] = view;
			}
			else
			{
				_cachedScreens.Add(key, view);
			}
		}

        /// <summary>
        /// Check until reach root container
        /// </summary>
        /// <returns></returns>
        //private bool IsRootScreen() {
        //    var currentView = GetScreen(CurrentScreen);
        //    if (currentView == null)
        //    {
        //        ExitApp();
        //        return false;
        //    }
        //    //
        //    if (!currentView.IsRoot) return false;
        //    if (currentView.ContainerViewIndex == 0)
        //    {
        //        return true;
        //    }
        //    else if (currentView.ContainerViewIndex >= 0)
        //    {
        //        // Get view from higher container
        //        int containerIndex = currentView.ContainerViewIndex--;
        //        CurrentScreen = GetContainerView(containerIndex).CurrentScreenKey;
        //        return IsRootScreen();
        //    }
        //    else {
        //        throw new Exception("CoantinerIndex out of range!!!");
        //    }
        //}

		public void NavigateBack(bool refresh = false)
		{
			if (IsBusy) return;
			IsBusy = true;
            //if (IsRootScreen()) {
            //    ExitApp();
            //    IsBusy = false;
            //    return;
            //}
            // Check if parent exist
			var fromView = GetScreen(CurrentScreen);
			if (fromView == null || fromView.IsRoot) {
                ExitApp();
                IsBusy = false;
                return;
            }
			var toView = GetScreen(fromView.ParentKey);
			if (toView == null) {
				IsBusy = false;
                throw new Exception("Cannot back to null screen. Do u mean it's a root screen? (set IsRoot is true)");
			}
			// Back to parent screen
            //var containerView = GetContainerView(toView.ContainerViewIndex);
            //NavigatorTrasition.ViewNavigate(ContainerView, fromView, toView, true);
            if (ScreenType == Navigator.ScreenType.View)
            {
                // Skip check type
                NavigatorTransition.ViewNavigate(ContainerView, (BaseViewWrapper)fromView, (BaseViewWrapper)toView, true);
            }
            else if (ScreenType == Navigator.ScreenType.Fragment)
            {
                // Skip check type
                NavigatorTransition.FragmentNavigate(ContainerView, (BaseFragment)fromView, (BaseFragment)toView, true);
            }
            //
			CurrentScreen = toView.Key;
			IsBusy = false;
		}

		private const long EXIT_DELAY_DURATION = 4000;
		private long LAST_BACK_PRESS_TIME = 0;
		public void ExitApp(Activity activity)
		{
			long currentTime = DateTime.UtcNow.Ticks / 10000;
			if(LAST_BACK_PRESS_TIME < currentTime - EXIT_DELAY_DURATION)
			{
				DependencyService.Get<PopupManager>().ShowToast("Press back again to exit app", ToastLength.Long); // activity.GetString(Resource.String.EXIT_BACK_PRESS)
				LAST_BACK_PRESS_TIME = currentTime;
			}
			else
			{
				activity.Finish();
			}
		}

        private void ExitApp() {
            // Exit app
            var mainActivity = DependencyService.Get<UBaseActivity>();
            if (mainActivity != null)
            {
                ExitApp(mainActivity);
            }
            else
            {
                throw new Exception("MainActivity has not resgistered as global instance yet!!!");
            }
        }

		protected abstract INavigableScreen GetScreenByKey(Context ctx, int screenId);
	}

    //public class ContainerView {
    //    public int CurrentScreenKey { get; set; }
    //    public ViewGroup Container { get; set; }
    //}

    public enum ScreenType { 
        View,
        Fragment,
    }

	public interface ICanNavigate {
		INavigator Navigator { get; set; }
	}

	public interface INavigableScreen : ICanNavigate
    {
        int Key { get; set; }
        int ParentKey { get; set; }
        //object ContentPresenter { get; }
        bool IsRoot { get; set; }
        void OnAnimationStart();
        void OnAnimationEnd();
        object[] Args { get; set; }
        void RefreshData(object[] args);
        ScreenType Type { get; }
        int ContainerViewIndex { get; set; } // Just use for multiple containers navigator
        bool NoCache { get; set; }
    }
}

