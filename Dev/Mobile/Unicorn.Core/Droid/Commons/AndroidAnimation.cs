using System;
using Android.Views;
using Android.Views.Animations;

namespace Unicorn.Core
{

	public static class AndroidAnimation
	{

		public static void SlideHorizontal(this View view, float distance, bool isIn, bool fromRight = true, long duration = 500, Action animationEnd
			= null)
		{
			float XDelta = fromRight ? distance : -distance;
			float fromX, toX;
			if (isIn)
			{
				fromX = XDelta;
				toX = 0;
			}
			else
			{
				fromX = 0;
				toX = XDelta;
			}
			TranslateAnimation animate = new TranslateAnimation(fromX, toX, 0, 0);
			animate.Duration = duration;
			animate.FillAfter = true;
			animate.Interpolator = new DecelerateInterpolator ();
			animate.AnimationStart += (s, e) => { };
			animate.AnimationEnd += (sender, e) =>
			{
				if (animationEnd != null)
				{
					animationEnd.Invoke();
				}
			};
			view.Visibility = ViewStates.Visible;
			view.StartAnimation(animate);
		}

		public static void Zoom(this View view, bool isIn, long duration = 500, Action animationStart = null, Action animationEnd
			= null){
			float fromScale = isIn ? 1 : 0;
			ScaleAnimation animate = new ScaleAnimation (fromScale, 1-fromScale, fromScale, 1-fromScale, Dimension.RelativeToSelf, 0.5f, Dimension.RelativeToSelf, 0.5f);
			animate.Duration = duration;
			animate.FillAfter = true;
			animate.AnimationStart += (s, e) => {
				if (animationStart != null) {
					animationStart.Invoke();
				}
			};
			animate.AnimationEnd += (sender, e) =>
			{
				if (animationEnd != null)
				{
					animationEnd.Invoke();
				}
			};
			view.StartAnimation (animate);
		}
	}

}
