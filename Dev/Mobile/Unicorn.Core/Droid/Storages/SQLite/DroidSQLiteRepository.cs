﻿using System;
using System.IO;
using Unicorn.Storages;
using Unicorn.Core.Storages;
using Unicorn.Core.Droid.Commons;

namespace Unicorn.Core.Storages
{
	public abstract partial class SQLiteRepository
	{
		partial void InitDatabase()
		{
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), RepositoryName);

            FileAssessHelper.CopyDatabaseIfNotExists(path, "DriveRight.db");
            _db = new SQLiteConnectionWithLock(new SQLiteConnectionString(path, false));

		}
	}
}

