﻿using System;
using Unicorn.Core;
using Unicorn.Core.Dependency;

[assembly:Dependency(typeof(Localize))]
namespace Unicorn.Core
{
	public class Localize : ILocalize
	{
		public Localize ()
		{
		}

		#region ILocalize implementation

		public System.Globalization.CultureInfo GetCurrentCultureInfo ()
		{
			var androidLocale = Java.Util.Locale.Default;
			var netLanguage = androidLocale.ToString().Replace ("_", "-"); // turns pt_BR into pt-BR
			return new System.Globalization.CultureInfo(netLanguage);
		}

		#endregion
	}
}

