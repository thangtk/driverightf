

using System;
using System.Linq.Expressions;
namespace ReactiveUI.Unicorn.Extensions
{
    static public class SpecialCallBinding
    {
        public static IReactiveBinding<TView, TViewModel, TVProp> OneWayBind<TViewModel, TView, TVMProp, TVProp>(
               TView view,
               TViewModel viewModel,
               Expression<Func<TViewModel, TVMProp>> vmProperty,
               Expression<Func<TView, TVProp>> viewProperty,
               Func<TVMProp> fallbackValue = null,
               object conversionHint = null,
               IBindingTypeConverter vmToViewConverterOverride = null)
            where TViewModel : class
            where TView : IViewFor
        {
            return BindingMixins.OneWayBind<TViewModel, TView, TVMProp, TVProp>(
                view,
               viewModel,
               vmProperty,
               viewProperty,
               fallbackValue,
               conversionHint,
               vmToViewConverterOverride
            );
        }


        public static Expression<Func<TParam, TResult>> CreateCustormExpress<TParam, TResult>
            (TParam model, TResult result, Expression body, ParameterExpression param)
        {
            var rs= Expression.Lambda<Func<TParam, TResult>>(body, param);
            return rs;
        }
    }
}