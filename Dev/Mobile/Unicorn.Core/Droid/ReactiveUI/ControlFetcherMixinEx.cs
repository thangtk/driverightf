using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using Android.App;
using Android.Views;
using Java.Interop;
using ReactiveUI.Unicorn.Extensions;
using ReactiveUI;
using System.Reactive.Linq;
using System.Linq.Expressions;

namespace ReactiveUI.Unicorn
{
    /// <summary>
    /// ControlFetcherMixin helps you automatically wire-up Activities and
    /// Fragments via property names, similar to Butter Knife, as well as allows
    /// you to fetch controls manually.
    /// </summary>
    public static class ControlFetcherMixinEx
    {
        static readonly Dictionary<string, int> controlIds;
        static readonly ConditionalWeakTable<object, Dictionary<string, View>> viewCache =
            new ConditionalWeakTable<object, Dictionary<string, View>>();

        static readonly MethodInfo getControlActivity;
        static readonly MethodInfo getControlView;
        static PropertyBindingUnicornExt propertyBinding;

        static ControlFetcherMixinEx()
        {
            // NB: This is some hacky shit, but on Xamarin.Android at the moment,
            // this is always the entry assembly.
            propertyBinding = new PropertyBindingUnicornExt();
            var assm = AppDomain.CurrentDomain.GetAssemblies()[1];
            var resources = assm.GetModules().SelectMany(x => x.GetTypes()).First(x => x.Name == "Resource");

            var res = resources.GetNestedType("Id").GetFields()
                .Where(x => x.FieldType == typeof(int));
            // cuong.hoang remove duplicate key
            controlIds = new Dictionary<string, int>();
            foreach (var re in res)
            {
                if (!controlIds.Keys.Contains(re.Name))
                {
                    controlIds.Add(re.Name, (int)re.GetRawConstantValue());
                }

            }
            //controlIds = resources.GetNestedType("Id").GetFields()
            //    .Where(x => x.FieldType == typeof(int))
            //    .ToDictionary(k => k.Name.ToLowerInvariant(), v => (int)v.GetRawConstantValue());

            var type = typeof(ControlFetcherMixinEx);
            getControlActivity = type.GetMethod("GetControlEx", new[] { typeof(Activity), typeof(string) });
            getControlView = type.GetMethod("GetControlEx", new[] { typeof(View), typeof(string) });
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public static T GetControlEx<T>(this Activity This, [CallerMemberName]string propertyName = null)
            where T : View
        {
            return (T)getCachedControlEx(propertyName, This,
                () => This.FindViewById(controlIds[propertyName]).JavaCast<T>());
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public static T GetControlEx<T>(this View This, [CallerMemberName]string propertyName = null)
            where T : View
        {
            return (T)getCachedControlEx(propertyName, This,
                () => This.FindViewById(controlIds[propertyName]).JavaCast<T>());
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public static T GetControlEx<T>(this Fragment This, [CallerMemberName]string propertyName = null)
            where T : View
        {
            return GetControlEx<T>(This.View, propertyName);
        }

        /// <summary>
        ///
        /// </summary>
        public static void WireUpControlsEx(this ILayoutViewHost This)
        {
            AutoWireUpEx(This, This.View);
        }

        /// <summary>
        ///
        /// </summary>
        public static void WireUpControlsEx(this View This)
        {
            AutoWireUpEx(This, This);
        }

        /// <summary>
        /// This should be called in the Fragement's OnCreateView, with the newly inflated layout
        /// </summary>
        /// <param name="This"></param>
        /// <param name="inflatedView"></param>
        public static void WireUpControlsEx(this Fragment This, View inflatedView)
        {
            AutoWireUpEx(This, inflatedView);
        }

        /// <summary>
        ///
        /// </summary>
        public static void WireUpControlsEx(this Activity This)
        {
            AutoWireUpEx(This, null);
        }

        static View getControlInternalEx(this View parent, Type viewType, string name)
        {
            var mi = getControlView.MakeGenericMethod(new[] { viewType });
            return (View)mi.Invoke(null, new object[] { parent, name });
        }

        static View getControlInternalEx(this Activity parent, Type viewType, string name)
        {
            var mi = getControlActivity.MakeGenericMethod(new[] { viewType });
            return (View)mi.Invoke(null, new object[] { parent, name });
        }

        static View getCachedControlEx(string propertyName, object rootView, Func<View> fetchControlFromView)
        {
            var ret = default(View);
            var ourViewCache = viewCache.GetOrCreateValue(rootView);

            if (ourViewCache.TryGetValue(propertyName, out ret))
            {
                return ret;
            }

            ret = fetchControlFromView();

            ourViewCache.Add(propertyName, ret);
            return ret;
        }


        static void AutoWireUpEx(object This, View View)
        {
            var members = This.GetType().GetRuntimeProperties().Where(m => m.PropertyType.IsSameOrSubclass(typeof(View)) && Attribute.IsDefined(m, typeof(AutoWireUpAttribute)));
            members.ToList().ForEach(m =>
            {
                AutoWireUpAttribute attr = (AutoWireUpAttribute)Attribute.GetCustomAttribute(m, typeof(AutoWireUpAttribute));
                // Auto Assign
                if (attr.AutoAssign)
                {
                    AutoAssignPropertiesEx(This, View, attr, m as MemberInfo);
                }

                // autobind
                if (attr.AutoBind)
                {
                    AutoBindPropertiesEx(This as IViewFor, attr, m as MemberInfo);
                }
            });

            var memberFs = This.GetType().GetRuntimeFields().Where(m => m.FieldType.IsSameOrSubclass(typeof(View)) && Attribute.IsDefined(m, typeof(AutoWireUpAttribute)));
            memberFs.ToList().ForEach(m =>
             {
                 AutoWireUpAttribute attr = (AutoWireUpAttribute)Attribute.GetCustomAttribute(m, typeof(AutoWireUpAttribute));
                 // Auto Assign
                 if (attr.AutoAssign)
                 {
                     AutoAssignPropertiesEx(This, View, attr, m as MemberInfo);
                 }
                 // autobind
                 if (attr.AutoBind)
                 {
                     AutoBindPropertiesEx(This as IViewFor, attr, m as MemberInfo);
                 }
             });
        }

        static void AutoAssignPropertiesEx(object This, View View, AutoWireUpAttribute attr, MemberInfo m)
        {

            try
            {
                string pName;
                if (attr.AssignId == "")
                {
                    pName = m.Name.StartsWith("_") ? m.Name.Substring(1, m.Name.Length - 1) : m.Name;
                    pName = (Char.ToLowerInvariant(pName[0]) + pName.Substring(1));
                }
                else
                {
                    pName = attr.AssignId;
                }
                Type propType = typeof(PropertyInfo).IsAssignableFrom(m.GetType()) ? ((PropertyInfo)m).PropertyType : ((FieldInfo)m).FieldType;
                View view;
                if (This.GetType().IsSameOrSubclass(typeof(Activity)))
                {
                    view = ((Activity)This).getControlInternalEx(propType, pName);
                }
                else
                {
                    view = View.getControlInternalEx(propType, pName);
                }
                // Set the activity field's value to the view with that identifier
                if (typeof(PropertyInfo).IsAssignableFrom(m.GetType()))
                {
                    ((PropertyInfo)m).SetValue(This, view);
                }
                else
                {
                    ((FieldInfo)m).SetValue(This, view);
                }
            }
            catch (Exception ex)
            {
                throw new MissingFieldException("Failed to wire up the Property "
                    + m.Name + " to a View in your layout with a corresponding identifier", ex);
            }
        }

        static void AutoBindPropertiesEx<TTView>(TTView This, AutoWireUpAttribute attr, MemberInfo child)
        {
            try
            {
                if (!typeof(IViewFor).IsAssignableFrom(This.GetType()))
                {
                    throw new Exception("class " + This.GetType().FullName + " is not an IViewFor ");
                }
                IViewFor view = This as IViewFor;

                if (!typeof(ReactiveObject).IsAssignableFrom(view.ViewModel.GetType()))
                {
                    throw new Exception("Class " + view.ViewModel.GetType().FullName + " Is not a ReactiveObject");
                }
                ReactiveObject viewmodel = view.ViewModel as ReactiveObject;


                var a = This.GetType().GetGenericArguments();
                // check vmproperties
                string pName;
                if (attr.VMProperty == "")
                {
                    pName = child.Name.StartsWith("_") ? child.Name.Substring(1, child.Name.Length - 1) : child.Name;
                    pName = (Char.ToUpperInvariant(pName[0]) + pName.Substring(1));
                }
                else
                {
                    pName = attr.VMProperty;
                }

                // View Expression
                var viewExpressParam = Expression.Parameter(This.GetType(), "v");
                Expression viewExpressBody = Expression.MakeMemberAccess(viewExpressParam, child);

                // TODO: not support special view name 

                var control = Reflection.GetValueFetcherForProperty(viewExpressBody.GetMemberInfo())(view, viewExpressBody.GetArgumentsArray());
                if (control == null)
                {
                    throw new Exception(String.Format("Tried to bind to control but it was null: {0}.{1}", view.GetType().FullName,
                        viewExpressBody.GetMemberInfo().Name));
                }

                var defaultProperty = DefaultPropertyBinding.GetPropertyForControl(control);
                if (defaultProperty == null)
                {
                    throw new Exception(String.Format("Couldn't find a default property for type {0}", control.GetType()));
                }
                viewExpressBody = Expression.MakeMemberAccess(viewExpressBody, control.GetType().GetRuntimeProperty(defaultProperty));
                Type genericViewType = typeof(Func<,>).MakeGenericType(This.GetType(), typeof(object));
                Expression viewExpress = Expression.Lambda(genericViewType, viewExpressBody, viewExpressParam);



                // ViewModel Expression
                var vmProperty = (MemberInfo)viewmodel.GetType().GetRuntimeField(pName) ?? viewmodel.GetType().GetRuntimeProperty(pName);
                var vmExpressParam = Expression.Parameter(view.ViewModel.GetType(), "vm");
                Expression vmExpressBody = Expression.MakeMemberAccess(vmExpressParam, vmProperty);
                Type genericVMType = typeof(Func<,>).MakeGenericType(view.ViewModel.GetType(), typeof(object));
                var vmExpress = Expression.Lambda(genericVMType, vmExpressBody, vmExpressParam);

                // begin binding oneway
                if (!attr.IsTwoWay)
                {
                    MethodInfo mi = propertyBinding.GetType()
                        .GetTypeInfo()
                        .GetDeclaredMethod("OneWayBindExt")
                        .MakeGenericMethod(view.ViewModel.GetType(), This.GetType()
                        , typeof(object), typeof(object));
                    object obj = mi.Invoke(propertyBinding, new object[] { view.ViewModel, This, vmExpress, viewExpress, null, null, null });

                    PropertyInfo changedPro = obj.GetType().GetRuntimeProperty("Changed");
                    object changed = changedPro.GetValue(obj, null);

                    if (attr.OneWayMethod != "")
                    {
                        MethodInfo method = This.GetType().GetMethod(attr.OneWayMethod, new Type[] { typeof(IObservable<object>) });
                        if (method == null)
                        {
                            throw new Exception("Cant find method : " + attr.OneWayMethod + "(IObservable<object> changed); In class " + viewmodel.GetType().FullName);
                        }
                        method.Invoke(This, new object[] { changed });
                    }
                }
                else
                // begin binding twoway
                {
                    MethodInfo mi = propertyBinding.GetType()
                        .GetTypeInfo().GetDeclaredMethod("BindExt")
                        .MakeGenericMethod(view.ViewModel.GetType(), This.GetType()
                        , typeof(object), typeof(object), typeof(System.Reactive.Unit));
                    object obj = mi.Invoke(propertyBinding
                        , new object[] { view.ViewModel, This, vmExpress, viewExpress, null, null, null, null });

                    PropertyInfo changedPro = obj.GetType().GetRuntimeProperty("Changed");
                    object changed = changedPro.GetValue(obj, null);

                    if (attr.TwoWayMethod != "")
                    {
                        MethodInfo method = This.GetType().GetMethod(attr.TwoWayMethod, new Type[] { typeof(IObservable<Tuple<object, bool>>) });
                        if (method == null)
                        {
                            throw new Exception("Cant find method : " + attr.OneWayMethod + "(IObservable<Tuple<object,bool>> changed); In class " + viewmodel.GetType().FullName);
                        }
                        method.Invoke(This, new object[] { changed });
                    }
                }
            }
            catch (Exception ex)
            {
                // TODO: doesnt delarce Exception
                throw new MissingFieldException("Failed to auto binding", ex);
            }
        }


    }
}
