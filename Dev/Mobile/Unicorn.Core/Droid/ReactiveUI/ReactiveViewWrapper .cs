

using Android.Content;
using Android.Views;
using ReactiveUI;
using System;
using System.ComponentModel;
using System.Reactive;
using System.Reactive.Subjects;


namespace Unicorn.Core.ReactiveUI
{
    public abstract class ReactiveViewWrapper<TViewModel> : ReactiveViewWrapper, IViewFor<TViewModel>, ICanActivate
        where TViewModel : class
    {
        TViewModel _ViewModel;
        public TViewModel ViewModel
        {
            get { return _ViewModel; }
            set { this.RaiseAndSetIfChanged(ref _ViewModel, value); }
        }

        object IViewFor.ViewModel
        {
            get { return _ViewModel; }
            set { _ViewModel = (TViewModel)value; }
        }
    }

    public abstract class ReactiveViewWrapper : Java.Lang.Object, IReactiveNotifyPropertyChanged<ReactiveViewWrapper>,
    IReactiveObject, IHandleObservableErrors, ILayoutViewHost
    {
        private readonly Subject<Unit> activated = new Subject<Unit>();
        public IObservable<Unit> Activated { get { return activated; } }

        private readonly Subject<Unit> deactivated = new Subject<Unit>();
        public IObservable<Unit> Deactivated { get { return deactivated; } }

        protected Context _context;
        protected LayoutInflater _inflater;
        protected ViewGroup _rootView;

        public abstract void InitViewWrapper(Context context);

        public abstract void RefreshContainerView(params object[] args);

        public abstract void OnBackPress();
        public ViewGroup GetContainerView()
        {
            return _rootView;
        }

        public virtual void FinishAnimationIn()
        {
            ((Subject<Unit>)Activated).OnNext(Unit.Default);
        }
        public virtual void FinishAnimationOut()
        {
            ((Subject<Unit>)Deactivated).OnNext(Unit.Default);
        }
        public virtual void RefreshUI()
        {

        }

        public View FindViewById(int id)
        {
            if (_rootView == null)
            {
                return null;
            }
            return _rootView.FindViewById(id);
        }

        public View View
        {
            get
            {
                return _rootView;
            }
        }

        public IObservable<IReactivePropertyChangedEventArgs<ReactiveViewWrapper>> Changed
        {
            get { throw new NotImplementedException(); }
        }

        public IObservable<IReactivePropertyChangedEventArgs<ReactiveViewWrapper>> Changing
        {
            get { throw new NotImplementedException(); }
        }

        public IDisposable SuppressChangeNotifications()
        {
            throw new NotImplementedException();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public event global::ReactiveUI.PropertyChangingEventHandler PropertyChanging;

        public void RaisePropertyChanged(PropertyChangedEventArgs args)
        {
            throw new NotImplementedException();
        }

        public void RaisePropertyChanging(global::ReactiveUI.PropertyChangingEventArgs args)
        {
            throw new NotImplementedException();
        }

        public IObservable<Exception> ThrownExceptions
        {
            get { throw new NotImplementedException(); }
        }
    }
}