using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ReactiveUI;
using Unicorn.Core;
using Unicorn.Core.Navigator;

namespace Unicorn.Core.UI
{
    public abstract class BaseViewWrapper<TViewModel> : BaseViewWrapper, IViewFor<TViewModel> where TViewModel : UBaseViewModel
    {

        TViewModel _viewModel;
        public TViewModel ViewModel
        {
            get { return _viewModel; }
            set { _viewModel = value; }
        }

        object IViewFor.ViewModel
        {
            get { return _viewModel; }
            set { _viewModel = (TViewModel)value; }
        }

        protected BaseViewWrapper(Context ctx) : base(ctx) { }
    }


    public abstract class BaseViewWrapper : LayoutViewHost, INavigableScreen
    {
        public int Key { get; set; }
        public int ParentKey { get; set; }
        public bool IsRoot { get; set; }
        public virtual void OnAnimationStart() { }
        public virtual void OnAnimationEnd() { }
        public object[] Args { get; set; }
        public virtual void RefreshData(object[] args) { }
        public ScreenType Type { get { return ScreenType.View; } }
		public INavigator Navigator { get; set; }

        public View FindViewById(int id)
        {
            if (View == null)
            {
                return null;
            }
            return View.FindViewById(id);
        }

        private BaseViewWrapper() {
            ContainerViewIndex = 0;
        }

        protected BaseViewWrapper(Context ctx) : this()
        {
            var id = GetLayoutId();
            InflateLayout(ctx, id);
        }

        private void InflateLayout(Context ctx, int layoutId)
        {
            var inflater = LayoutInflater.FromContext(ctx);
            View = inflater.Inflate(layoutId, null);
        }

        protected abstract int GetLayoutId();

        public int ContainerViewIndex { get; set; }

        public bool NoCache { get; set; }
    }
}