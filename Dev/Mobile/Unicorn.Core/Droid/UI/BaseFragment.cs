using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ReactiveUI;
using Unicorn.Core.Navigator;

namespace Unicorn.Core.UI
{
    public abstract class BaseFragment<TViewModel> : BaseFragment, IViewFor<TViewModel> where TViewModel : UBaseViewModel
    {
        private TViewModel _viewModel;
        public TViewModel ViewModel
        {
            get
            {
                return _viewModel;
            }
            set
            {
                _viewModel = value;
            }
        }

        object IViewFor.ViewModel
        {
            get
            {
                return _viewModel;
            }
            set
            {
                _viewModel = (TViewModel)value;
            }
        }
    }

	public abstract class BaseFragment : ReactiveFragment, INavigableScreen
    {
        public int Key { get; set; }
        public int ParentKey { get; set; }
        public bool IsRoot { get; set; }
        public virtual void OnAnimationStart() { }
        public virtual void OnAnimationEnd() { }
        public object[] Args { get; set; }
        public virtual void RefreshData(object[] args) { }
		public INavigator Navigator { get; set; }

        public BaseFragment()
        {
            ContainerViewIndex = 0;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View v = inflater.Inflate(GetLayoutId(), container, false);
            return v;
        }

        protected abstract int GetLayoutId();
        public ScreenType Type { get { return ScreenType.Fragment; } }
        public int ContainerViewIndex { get; set; }
        public bool NoCache { get; set; }

        #region Try it later
        public float XFraction
        {
            get
            {
                if (View.Width > 0)
                {
                    return View.GetX() / View.Width;
                }
                return 0;
            }
            set
            {
                var width = View.Width;
                View.SetX(width > 0 ? value * View.Width : -9999);
            }
        }
        #endregion
    }

}