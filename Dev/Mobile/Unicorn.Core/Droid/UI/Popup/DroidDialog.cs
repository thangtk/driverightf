using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Contemi.Core.UI;

namespace Contemi.Droid.Common
{
    public class DroidDialog : IDialog
    {
        protected INavigator Navigator
        {
            get
            {
                return Contemi.Core.ContemiIoC.Instance.Resolve<INavigator>();
            }
        }

        protected Activity DialogContext
        {
            get
            {
                return ((DroidNavigator)Navigator).CurrentActivity;
            }
        }

        public DroidDialog()
        {

        }

        public virtual void Show(int screenId, Action callback = null)
        {
        }

        public virtual void Show<T>(int screenId, T param, Action callback = null) where T : class
        {
        }

        public virtual void Show<T1, T2>(int screenId, T1 param1, T2 param2, Action callback = null)
            where T1 : class
            where T2 : class
        {
        }

        public void Hide()
        {
        }

        public Action<object> OnCloseDialog { get; set; }
    }
}