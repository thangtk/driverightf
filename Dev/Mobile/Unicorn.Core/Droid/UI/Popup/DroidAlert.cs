using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;
using Android.Graphics;
using Contemi.Droid;
using Unicorn.Core.UI;

namespace Contemi.Droid.Common
{
	public class DroidAlert<T> : IAlert where T : ContemiAlertDialog
	{
		protected INavigator Navigator
		{
			get
			{
				return Contemi.Core.ContemiIoC.Instance.Resolve<INavigator> ();
			}
		}

		protected Activity DialogContext
		{
			get
			{
				return ((DroidNavigator)Navigator).CurrentActivity;
			}
		}

		protected ContemiAlertDialog _dialog;

		public DroidAlert ()
		{
		}

		public void Show (string title, string content, string submitTitle = "OK", string cancelTitle = "", int drawable = -1, Action<bool> onSubmit = null)
		{
			if (_dialog == null)
			{
				CreateDialog (DialogContext);
			}

			LoadAlertData (title, content, submitTitle, cancelTitle, drawable, onSubmit);

			_dialog.Show ();
		}

		public void Hide (bool isOK = false)
		{
			if (_dialog != null && _dialog.IsShowing)
			{
				_dialog.Hide ();

				if (_dialog.OnSubmitted != null)
				{
					_dialog.OnSubmitted (isOK);
				}
			}
		}

		protected virtual void CreateDialog (Context context)
		{
			_dialog = (T)Activator.CreateInstance (typeof(T), context);
		}

		protected virtual void LoadAlertData (string title, string content, string submitTitle = "OK", string cancelTitle = "", int drawable = -1, Action<bool> onSubmit = null)
		{
			_dialog.Title = title;
			_dialog.Content = content;
			_dialog.PositiveButtonTitle = submitTitle;
			_dialog.NegativeButtonTitle = cancelTitle;
			_dialog.OnSubmitted += onSubmit;
			if (drawable != -1)
			{
				_dialog.Drawable = drawable;
			}
		}
	}
}