using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Unicorn.Core.Navigator;

namespace Unicorn.Core.UI
{
    public class LoadingAsyncTask : AsyncTask
    {
        ProgressDialog _dialog;
        string t;
        Action a, c;

        public LoadingAsyncTask(ProgressDialog dialog, string title, Action action, Action complete)
		{
            _dialog = dialog;
            t = title;
            a = action;
            c = complete;
		}

        async Task<int> DoAction()
        {
             if (a != null)
            {
                a();
            }

             return 1;
        }

        protected override Java.Lang.Object DoInBackground(params Java.Lang.Object[] native_parms)
        {
            if (a != null)
            {
                a();
            }
            //Thread.Sleep(6000);
            return 1;
        }

        protected override void OnPreExecute()
        {
            base.OnPreExecute();

            _dialog.SetMessage(t);
            _dialog.Show();
        }

        protected override void OnPostExecute(Java.Lang.Object result)
        {
            base.OnPostExecute(result);

            if (c != null)
            {
                c();
            }
        }
    }

    public class DroidLoading : ILoading
    {
        protected ProgressDialog _dialog;

        protected INavigator Navigator
        {
            get
            {
                return DependencyService.Get<INavigator>();
            }
        }

        protected Activity DialogContext
        {
            get
            {
                return null;
            }
        }

        public DroidLoading()
        {
        }

        public void Show(string title, Action action, Action complete)
        {
            if (_dialog == null)
            {
                CreateDialog(DialogContext);
            }

            new LoadingAsyncTask(_dialog, title, action, complete).Execute(new Java.Lang.Object[] { });
        }

        public void Show(string title = "Loading...", Action del = null)
        {
            if(_dialog==null)
            {
                CreateDialog(DialogContext);
            }

            _dialog.SetMessage(title);
            _dialog.Show();
        }

        private void CreateDialog(Context context)
        {
            _dialog = new ProgressDialog(context);
        }

        public void Hide()
        {
            if(_dialog != null && _dialog.IsShowing)
            {
                _dialog.Dismiss();
            }
        }
    }
}