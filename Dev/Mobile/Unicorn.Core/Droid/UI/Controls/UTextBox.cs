﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Unicorn.Core
{
	public class UTextBox : RelativeLayout
	{
		private Context _context;
		public UTextBox (Context context) :
			base (context)
		{
			_context = context;
			Initialize ();
		}

		public UTextBox (Context context, IAttributeSet attrs) :
			base (context, attrs)
		{
			_context = context;
			Initialize ();
		}

		public UTextBox (Context context, IAttributeSet attrs, int defStyle) :
			base (context, attrs, defStyle)
		{
			_context = context;
			Initialize ();
		}

		void Initialize ()
		{
			View viewGroup = View.Inflate(_context, Android.Resource.Layout.ListContent, this);
		}
	}
}

