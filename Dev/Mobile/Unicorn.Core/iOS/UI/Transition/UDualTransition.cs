﻿using System;
using CoreAnimation;
using UIKit;

namespace Unicorn.Core.Navigator 
{
	public class UDualTransition:UTransition
	{
		public UDualTransition(TimeSpan duration):base(duration){

		}
			

		private void SetupLayers(params CALayer[] layers) {
			foreach (CALayer layer in layers) {
				layer.ShouldRasterize = true;
				layer.RasterizationScale = UIScreen.MainScreen.Scale;
			}
		}

		private void TeardownLayers(params CALayer[] layers) {
			foreach (CALayer layer in layers) {
				layer.ShouldRasterize = false;
			}
		}

		public override void Animation(object vin, object vout){
			UIView viewIn,viewOut;

			if (vin is UIViewController) {
				viewIn = (vin as UIViewController).View;
			} else {
				viewIn =(UIView)vin;
			}

			if (vout is UIViewController) {
				viewOut = (vout as UIViewController).View;
			} else {
				viewOut =(UIView) vout;
			}

			viewIn.Layer.DoubleSided = false;
			viewOut.Layer.DoubleSided = false;

			SetupLayers (viewIn.Layer, viewOut.Layer);
			CATransaction.CompletionBlock += delegate {
				TeardownLayers(viewIn.Layer,viewOut.Layer);
			};
			viewIn.Layer.AddAnimation (InAnimation, null);
			viewOut.Layer.AddAnimation (OutAnimation, null);
		}

//		public override UTransition GetReverseTransition (){
//			CAAnimation inAnimationCopy = (CAAnimation)InAnimation.Copy();
//			CAAnimation outAnimationCopy = (CAAnimation)OutAnimation.Copy();
//			UDualTransition reserve = new UDualTransition (Duration);
//			reserve.InAnimation = outAnimationCopy;
//			reserve.OutAnimation = inAnimationCopy;
//			reserve.InAnimation.Speed = -1.0f * reserve.InAnimation.Speed;
//			reserve.OutAnimation.Speed = -1.0f * reserve.OutAnimation.Speed;
//			reserve.Type = UTransitionType.Null;
//
//			if (Type == UTransitionType.Push) {
//				reserve.Type = UTransitionType.Pop;
//			} else if (Type == UTransitionType.Pop) {
//				reserve.Type = UTransitionType.Push;
//			}
//			reserve.FinishInit ();
//			return reserve;
//		}

		public void FinishInit(){
			InAnimation.SetValueForKey (UTransitionAnimationInValue, UTransitionAnimationKey);
			InAnimation.Delegate = this;
			OutAnimation.SetValueForKey (UTransitionAnimationOutValue, UTransitionAnimationKey);
			OutAnimation.Delegate = this;
		}

		public override void AnimationStopped(CAAnimation anim,bool flag){
			if (anim.ValueForKey (UTransitionAnimationKey).Equals (UTransitionAnimationInValue)) {
				base.AnimationStopped (anim, flag);
			}
		}

	}
}

