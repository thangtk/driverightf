﻿using System;
using CoreAnimation;
using Foundation;

namespace Unicorn.Core.Navigator
{
	public class NullTransition :UDualTransition
	{
		public NullTransition():base(TimeSpan.FromMilliseconds(0)){

			var inAnim = CABasicAnimation.FromKeyPath ("opacity");
			inAnim.Duration = 0;
			inAnim.From = NSNumber.FromInt32(0);
			inAnim.To = NSNumber.FromInt32(1);

			InAnimation = inAnim;

			var outAnim = CABasicAnimation.FromKeyPath ("opacity");
			outAnim.Duration = 0;
			outAnim.From = NSNumber.FromInt32(1);
			outAnim.To = NSNumber.FromInt32(0);

			OutAnimation = outAnim;
			FinishInit ();
		}

		public override UTransition GetReverseTransition (){

			return this;
		}
	}
}

