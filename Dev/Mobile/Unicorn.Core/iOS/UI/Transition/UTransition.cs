﻿using System;
using CoreAnimation;
using Foundation;

namespace Unicorn.Core.Navigator
{
	public enum UTransitionType{
		Null,
		Push,
		Pop
	};

	public enum UTransitionOrientation{
		RightToLeft,
		LeftToRight,
		TopToBottom,
		BottomToTop
	};

	public class UTransitionDelegate :ITransitionDelegate {
		public virtual void PopTransitionDidFinish (ITransition transition){
		}
		public virtual void PushTransitionDidFinish(ITransition transition){
		}
	}

	public class UTransition : CAAnimationDelegate, ITransition
	{
		protected NSString UTransitionAnimationKey = new NSString("UTransitionAnimationKey");
		protected NSString UTransitionAnimationInValue = new NSString("UTransitionAnimationInValue");
		protected NSString UTransitionAnimationOutValue = new NSString("UTransitionAnimationOutValue");
		private static UTransition _nullTransaction;
		public static UTransition NullTransaction{
			get
			{
				if (_nullTransaction == null) {
					_nullTransaction = new NullTransition ();
				}
				return _nullTransaction;
			}
		}

		public ITransitionDelegate Delegate { get; set;}
		public CAAnimation InAnimation{ get; set;}
		public CAAnimation OutAnimation{ get; set;}

		public virtual UTransitionType Type { get; set;}
		public virtual TimeSpan Duration {get;set;}
		public UTransition ReverseTransition { get; set; }

		public UTransition(TimeSpan duration){
			Duration = duration;
		}

		public UTransition(CAAnimation inA,CAAnimation outA){
			InAnimation = inA;
			OutAnimation = outA;
		}

		public UTransition(TimeSpan dura,CAAnimation inA,CAAnimation outA){
			InAnimation = inA;
			OutAnimation = outA;
			Duration = dura;
		}
		public CAMediaTimingFunction[] GetCircleApproximationTimingFunctions (){
			float kappa = 4.0f / 3.0f * ((float)Math.Sqrt (2.0) - 1.0f) / (float)Math.Sqrt (2.0f);
			var first = CAMediaTimingFunction.FromControlPoints (kappa / (float)(Math.PI / 2.0), kappa, 1.0f - kappa, 1.0f);
			var second = CAMediaTimingFunction.FromControlPoints (kappa, 0.0f, 1.0f - (kappa/(float)(Math.PI/2.0f)), 1.0f-kappa);
			return new CAMediaTimingFunction[]{first,second};
		}
		public virtual void Animation(object viewIn, object viewOut){
		}



		public virtual UTransition GetReverseTransition (){
			return new UTransition (Duration);
		}

		public override void AnimationStopped(CAAnimation anim,bool flag){
			switch (Type) {
			case UTransitionType.Pop:
				Delegate.PopTransitionDidFinish(this);
							break;
			case UTransitionType.Push:
				Delegate.PushTransitionDidFinish(this);
							break;
						default:
							break;
						}
		}
//		- (void)animationDidStop:(CAAnimation *)animation finished:(BOOL)flag {    
//			switch (self.type) {
//			case ADTransitionTypePop:
//				[self.delegate popTransitionDidFinish:self];
//				break;
//			case ADTransitionTypePush:
//				[self.delegate pushTransitionDidFinish:self];
//				break;
//			default:
//				NSAssert(FALSE, @"Unexpected case in switch statement !");
//				break;
//			}
//		}
	}
}

