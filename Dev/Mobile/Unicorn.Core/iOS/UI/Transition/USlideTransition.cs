﻿using System;
using CoreGraphics;
using CoreAnimation;
using Foundation;

namespace Unicorn.Core.Navigator 
{
	public class USlideTransition:UDualTransition
	{
		CGRect _rect;
		UTransitionOrientation _orien;
		public USlideTransition(TimeSpan duration, UTransitionOrientation orien,CGRect rect):base(duration){
			_rect = rect;
			_orien=orien; 

			float viewWidth = (float)rect.Width;
			float viewHeight = (float)rect.Height;
			float scaleFactor = 0.7f;
			CATransform3D inTranslationTransform = CATransform3D.Identity;
			CATransform3D outTranslationTransform = CATransform3D.Identity;



			switch (orien) {
			case UTransitionOrientation.RightToLeft:
				{
					inTranslationTransform = CATransform3D.Identity.Translate(viewWidth, 0, 0);
					outTranslationTransform =  CATransform3D.Identity.Translate(-viewWidth, 0, 0);
				}
				break;
			case UTransitionOrientation.LeftToRight:
				{
					inTranslationTransform =  CATransform3D.Identity.Translate( -viewWidth, 0, 0);
					outTranslationTransform =  CATransform3D.Identity.Translate( viewWidth, 0, 0);
				}
				break;
			case UTransitionOrientation.TopToBottom:
				{
					inTranslationTransform =  CATransform3D.Identity.Translate( 0, -viewHeight, 0);
					outTranslationTransform =  CATransform3D.Identity.Translate( 0, viewHeight, 0);
				}
				break;
			case UTransitionOrientation.BottomToTop:
				{
					inTranslationTransform =  CATransform3D.Identity.Translate( 0, viewHeight, 0);
					outTranslationTransform =  CATransform3D.Identity.Translate( 0, -viewHeight, 0);
				}
				break;
			default:
//				NSAssert(FALSE, @"Unhandled ADTransitionOrientation");
				break;
			}
//
			CATransform3D inScaleDownTransform = CATransform3D.Identity.Scale(scaleFactor, scaleFactor, 1.0f);
			CATransform3D inTranslationScaleTransform = inTranslationTransform.Scale(scaleFactor, scaleFactor, 1.0f  );


			CAKeyFrameAnimation inKeyFrameTransformAnimation = CAKeyFrameAnimation.FromKeyPath ("transform");  //:@"transform"];
			inKeyFrameTransformAnimation.Values = new NSValue[] {NSValue.FromCATransform3D (inTranslationTransform),
				NSValue.FromCATransform3D (inScaleDownTransform),
				NSValue.FromCATransform3D (CATransform3D.Identity)
			};


			CAKeyFrameAnimation inKeyFrameOpacityAnimation = CAKeyFrameAnimation.FromKeyPath ("opacity");
			inKeyFrameOpacityAnimation.Values = new NSNumber[]{NSNumber.FromFloat(0.0f),NSNumber.FromFloat(0.0f),NSNumber.FromFloat(0.5f),NSNumber.FromFloat(1f)};

			CABasicAnimation inZPositionAnimation = CABasicAnimation.FromKeyPath ("zPosition");


			inZPositionAnimation.From = NSNumber.FromFloat( -0.001f);
			inZPositionAnimation.To = NSNumber.FromFloat(-0.001f);
//
			CAAnimationGroup inAnimation = CAAnimationGroup.CreateAnimation();
			inAnimation.Animations = new CAAnimation[]{ inKeyFrameTransformAnimation, inKeyFrameOpacityAnimation, inZPositionAnimation};//@[inKeyFrameTransformAnimation, inKeyFrameOpacityAnimation, inZPositionAnimation];
			inAnimation.Duration = Duration.TotalSeconds;
//
			CATransform3D outScaleDownTransform = CATransform3D.Identity.Scale( scaleFactor, scaleFactor, 1.0f);
			CATransform3D outTranslationScaleTransform = outTranslationTransform.Scale(scaleFactor, scaleFactor, 1.0f  );
//
			CAKeyFrameAnimation outKeyFrameTransformAnimation = CAKeyFrameAnimation.FromKeyPath ("transform"); 
			outKeyFrameTransformAnimation.Values = new NSValue[]{
				NSValue.FromCATransform3D(CATransform3D.Identity),
				NSValue.FromCATransform3D(outScaleDownTransform) ,
				NSValue.FromCATransform3D(outTranslationScaleTransform) ,
				NSValue.FromCATransform3D(outTranslationTransform) ,
			};

			CAKeyFrameAnimation outKeyFrameOpacityAnimation = CAKeyFrameAnimation.FromKeyPath ("opacity");// animationWithKeyPath:@"opacity"];

			outKeyFrameOpacityAnimation.Values = new NSNumber[]{ NSNumber.FromFloat(1.0f),NSNumber.FromFloat(0.5f),NSNumber.FromFloat(0.0f),NSNumber.FromFloat(0.0f) };
			CABasicAnimation outZPositionAnimation = CABasicAnimation.FromKeyPath ("zPosition"); //animationWithKeyPath:@"zPosition"];

			outZPositionAnimation.From = NSNumber.FromFloat(-0.001f);
			outZPositionAnimation.To = NSNumber.FromFloat(-0.001f);
			CAAnimationGroup outAnimation = CAAnimationGroup.CreateAnimation();
			outAnimation.Animations = new CAAnimation[]{ outKeyFrameTransformAnimation, outKeyFrameOpacityAnimation, outZPositionAnimation }; // ];
			outAnimation.Duration = Duration.TotalSeconds;

			InAnimation = inAnimation;
			OutAnimation = outAnimation;

			FinishInit ();
		}

		public override UTransition GetReverseTransition (){
			UDualTransition reserve = new UDualTransition (Duration);

			float viewWidth = (float)_rect.Width;
			float viewHeight = (float)_rect.Height;
			float scaleFactor = 0.7f;
			CATransform3D inTranslationTransform = CATransform3D.Identity;
			CATransform3D outTranslationTransform = CATransform3D.Identity;



			switch (_orien) {
			case UTransitionOrientation.RightToLeft:
				{
					inTranslationTransform =  CATransform3D.Identity.Translate( -viewWidth, 0, 0);
					outTranslationTransform =  CATransform3D.Identity.Translate( viewWidth, 0, 0);
				}
				break;
			case UTransitionOrientation.LeftToRight:
				{

					inTranslationTransform = CATransform3D.Identity.Translate(viewWidth, 0, 0);
					outTranslationTransform =  CATransform3D.Identity.Translate(-viewWidth, 0, 0);
				}
				break;
			case UTransitionOrientation.TopToBottom:
				{
					inTranslationTransform =  CATransform3D.Identity.Translate( 0, viewHeight, 0);
					outTranslationTransform =  CATransform3D.Identity.Translate( 0, -viewHeight, 0);
				}
				break;
			case UTransitionOrientation.BottomToTop:
				{

					inTranslationTransform =  CATransform3D.Identity.Translate( 0, -viewHeight, 0);
					outTranslationTransform =  CATransform3D.Identity.Translate( 0, viewHeight, 0);
				}
				break;
			default:
				break;
			}
			//
			CATransform3D inScaleDownTransform = CATransform3D.Identity.Scale(scaleFactor, scaleFactor, 1.0f);
			CATransform3D inTranslationScaleTransform = inTranslationTransform.Scale(scaleFactor, scaleFactor, 1.0f  );


			CAKeyFrameAnimation inKeyFrameTransformAnimation = CAKeyFrameAnimation.FromKeyPath ("transform");  //:@"transform"];
			inKeyFrameTransformAnimation.Values = new NSValue[] {NSValue.FromCATransform3D (inTranslationTransform),
				NSValue.FromCATransform3D (inScaleDownTransform),
				NSValue.FromCATransform3D (CATransform3D.Identity)
			};


			CAKeyFrameAnimation inKeyFrameOpacityAnimation = CAKeyFrameAnimation.FromKeyPath ("opacity");
			inKeyFrameOpacityAnimation.Values = new NSNumber[]{NSNumber.FromFloat(0.0f),NSNumber.FromFloat(0.0f),NSNumber.FromFloat(0.5f),NSNumber.FromFloat(1f)};

			CABasicAnimation inZPositionAnimation = CABasicAnimation.FromKeyPath ("zPosition");


			inZPositionAnimation.From = NSNumber.FromFloat( -0.001f);
			inZPositionAnimation.To = NSNumber.FromFloat(-0.001f);
			//
			CAAnimationGroup inAnimation = CAAnimationGroup.CreateAnimation();
			inAnimation.Animations = new CAAnimation[]{ inKeyFrameTransformAnimation, inKeyFrameOpacityAnimation, inZPositionAnimation};//@[inKeyFrameTransformAnimation, inKeyFrameOpacityAnimation, inZPositionAnimation];
			inAnimation.Duration = Duration.TotalSeconds;
			//
			CATransform3D outScaleDownTransform = CATransform3D.Identity.Scale( scaleFactor, scaleFactor, 1.0f);
			CATransform3D outTranslationScaleTransform = outTranslationTransform.Scale(scaleFactor, scaleFactor, 1.0f  );
			//
			CAKeyFrameAnimation outKeyFrameTransformAnimation = CAKeyFrameAnimation.FromKeyPath ("transform"); 
			outKeyFrameTransformAnimation.Values = new NSValue[]{
				NSValue.FromCATransform3D(CATransform3D.Identity),
				NSValue.FromCATransform3D(outScaleDownTransform) ,
				NSValue.FromCATransform3D(outTranslationScaleTransform) ,
				NSValue.FromCATransform3D(outTranslationTransform) ,
			};

			CAKeyFrameAnimation outKeyFrameOpacityAnimation = CAKeyFrameAnimation.FromKeyPath ("opacity");// animationWithKeyPath:@"opacity"];

			outKeyFrameOpacityAnimation.Values = new NSNumber[]{ NSNumber.FromFloat(1.0f),NSNumber.FromFloat(0.5f),NSNumber.FromFloat(0.0f),NSNumber.FromFloat(0.0f) };
			CABasicAnimation outZPositionAnimation = CABasicAnimation.FromKeyPath ("zPosition"); //animationWithKeyPath:@"zPosition"];

			outZPositionAnimation.From = NSNumber.FromFloat(-0.001f);
			outZPositionAnimation.To = NSNumber.FromFloat(-0.001f);
			CAAnimationGroup outAnimation = CAAnimationGroup.CreateAnimation();
			outAnimation.Animations = new CAAnimation[]{ outKeyFrameTransformAnimation, outKeyFrameOpacityAnimation, outZPositionAnimation }; // ];
			outAnimation.Duration = Duration.TotalSeconds;

			reserve.InAnimation = outAnimation;
			reserve.OutAnimation = inAnimation;

			reserve.FinishInit ();

			return reserve;
		}
	}
}

