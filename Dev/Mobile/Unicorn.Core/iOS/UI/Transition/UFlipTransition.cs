﻿using System;
using CoreGraphics;
using CoreAnimation;
using Foundation;

namespace Unicorn.Core.Navigator 
{
	public class UFlipTransition:UDualTransition
	{
		CGRect _rect;
		UTransitionOrientation _orien;
		public UFlipTransition(TimeSpan duration, UTransitionOrientation orien,CGRect rect):base(duration){
			_rect = rect;
			_orien=orien; 
			float viewW = (float)rect.Width;
			float viewH = (float)rect.Height;

			CAKeyFrameAnimation ztran = CAKeyFrameAnimation.FromKeyPath("zPosition");

			CATransform3D inPivotTransform = CATransform3D.Identity;
			CATransform3D outPivotTransform = CATransform3D.Identity;

			switch (orien) {
			case UTransitionOrientation.RightToLeft:
				{
					ztran.Values = new NSObject[]{NSNumber.FromFloat(0.0f),NSNumber.FromFloat(-viewW*0.5f),NSNumber.FromFloat(0.0f)};
					inPivotTransform = inPivotTransform.Rotate(NMath.PI - 0.001f, 0.0f, 1.0f, 0.0f);
					outPivotTransform = outPivotTransform.Rotate( -NMath.PI + 0.001f, 0.0f, 1.0f, 0.0f);
				}
				break;
			case UTransitionOrientation.LeftToRight:
				{
					ztran.Values = new NSObject[]{NSNumber.FromFloat(0.0f),NSNumber.FromFloat(-viewW*0.5f),NSNumber.FromFloat(0.0f)};
					inPivotTransform = inPivotTransform.Rotate(NMath.PI + 0.001f, 0.0f, 1.0f, 0.0f);
					outPivotTransform = outPivotTransform.Rotate( - NMath.PI - 0.001f, 0.0f, 1.0f, 0.0f);
				}
				break;
			case UTransitionOrientation.BottomToTop:
				{
					ztran.Values = new NSObject[]{NSNumber.FromFloat(0.0f),NSNumber.FromFloat(-viewH*0.5f),NSNumber.FromFloat(0.0f)};
					inPivotTransform = inPivotTransform.Rotate( NMath.PI + 0.001f, 1.0f, .0f, 0.0f);
					outPivotTransform = outPivotTransform.Rotate( - NMath.PI - 0.001f, 1.0f, 0.0f, 0.0f);
				}
				break;
			case UTransitionOrientation.TopToBottom:
				{
					ztran.Values = new NSObject[]{NSNumber.FromFloat(0.0f),NSNumber.FromFloat(-viewH*0.5f),NSNumber.FromFloat(0.0f)};
					inPivotTransform = inPivotTransform.Rotate( NMath.PI - 0.001f, 1.0f, .0f, 0.0f);
					outPivotTransform = outPivotTransform.Rotate( - NMath.PI + 0.001f, 1.0f, 0.0f, 0.0f);
				}
				break;
			default:
				break;
			}
//
			ztran.TimingFunctions = GetCircleApproximationTimingFunctions();
//
			CAKeyFrameAnimation inFlipAnimation =CAKeyFrameAnimation.FromKeyPath("transform");
			inFlipAnimation.Values =new NSObject[]{NSValue.FromCATransform3D(inPivotTransform),NSValue.FromCATransform3D(CATransform3D.Identity)};
//			@[[NSValue valueWithCATransform3D:inPivotTransform], [NSValue valueWithCATransform3D:CATransform3DIdentity]];
			inFlipAnimation.TimingFunctions = new CAMediaTimingFunction[]{CAMediaTimingFunction.FromName(CAMediaTimingFunction.Linear)};
//
			CAKeyFrameAnimation outFlipAnimation = CAKeyFrameAnimation.FromKeyPath("transform");
			outFlipAnimation.Values = new NSObject[]{NSValue.FromCATransform3D(CATransform3D.Identity),NSValue.FromCATransform3D(outPivotTransform)};
			outFlipAnimation.TimingFunctions = new CAMediaTimingFunction[]{CAMediaTimingFunction.FromName(CAMediaTimingFunction.Linear)};
			// @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
//
			CAAnimationGroup inAnimation = CAAnimationGroup.CreateAnimation ();// [CAAnimationGroup animation];
			inAnimation.Animations = new CAAnimation[]{inFlipAnimation, ztran};
			inAnimation.Duration = Duration.TotalSeconds;
//
			CAAnimationGroup outAnimation = CAAnimationGroup.CreateAnimation ();
			outAnimation.Animations = new CAAnimation[]{outFlipAnimation, ztran};
			outAnimation.Duration = Duration.TotalSeconds;
			OutAnimation = outAnimation;
			InAnimation = inAnimation;
			FinishInit ();
//
		}

				public override UTransition GetReverseTransition (){
					UDualTransition reserve = new UDualTransition (Duration);
			float viewW = (float)_rect.Width;
			float viewH = (float)_rect.Height;

			CAKeyFrameAnimation ztran = CAKeyFrameAnimation.FromKeyPath("zPosition");

			CATransform3D inPivotTransform = CATransform3D.Identity;
			CATransform3D outPivotTransform = CATransform3D.Identity;

			switch (_orien) {
			case UTransitionOrientation.RightToLeft:
				{
					ztran.Values = new NSObject[]{NSNumber.FromFloat(0.0f),NSNumber.FromFloat(-viewW*0.5f),NSNumber.FromFloat(0.0f)};
					inPivotTransform = inPivotTransform.Rotate(NMath.PI + 0.001f, 0.0f, 1.0f, 0.0f);
					outPivotTransform = outPivotTransform.Rotate( - NMath.PI - 0.001f, 0.0f, 1.0f, 0.0f);
				}
				break;
			case UTransitionOrientation.LeftToRight:
				{
					ztran.Values = new NSObject[]{NSNumber.FromFloat(0.0f),NSNumber.FromFloat(-viewW*0.5f),NSNumber.FromFloat(0.0f)};
					inPivotTransform = inPivotTransform.Rotate(NMath.PI - 0.001f, 0.0f, 1.0f, 0.0f);
					outPivotTransform = outPivotTransform.Rotate( -NMath.PI + 0.001f, 0.0f, 1.0f, 0.0f);
				}
				break;
			case UTransitionOrientation.BottomToTop:
				{
					ztran.Values = new NSObject[]{NSNumber.FromFloat(0.0f),NSNumber.FromFloat(-viewH*0.5f),NSNumber.FromFloat(0.0f)};
					inPivotTransform = inPivotTransform.Rotate( NMath.PI - 0.001f, 1.0f, .0f, 0.0f);
					outPivotTransform = outPivotTransform.Rotate( - NMath.PI + 0.001f, 1.0f, 0.0f, 0.0f);
				}
				break;
			case UTransitionOrientation.TopToBottom:
				{
					ztran.Values = new NSObject[]{NSNumber.FromFloat(0.0f),NSNumber.FromFloat(-viewH*0.5f),NSNumber.FromFloat(0.0f)};
					inPivotTransform = inPivotTransform.Rotate( NMath.PI + 0.001f, 1.0f, .0f, 0.0f);
					outPivotTransform = outPivotTransform.Rotate( - NMath.PI - 0.001f, 1.0f, 0.0f, 0.0f);
				}
				break;
			default:
				break;
			}
			//
			ztran.TimingFunctions = GetCircleApproximationTimingFunctions();
			//
			CAKeyFrameAnimation inFlipAnimation =CAKeyFrameAnimation.FromKeyPath("transform");
			inFlipAnimation.Values =new NSObject[]{NSValue.FromCATransform3D(inPivotTransform),NSValue.FromCATransform3D(CATransform3D.Identity)};
			//			@[[NSValue valueWithCATransform3D:inPivotTransform], [NSValue valueWithCATransform3D:CATransform3DIdentity]];
			inFlipAnimation.TimingFunctions = new CAMediaTimingFunction[]{CAMediaTimingFunction.FromName(CAMediaTimingFunction.Linear)};
			//
			CAKeyFrameAnimation outFlipAnimation = CAKeyFrameAnimation.FromKeyPath("transform");
			outFlipAnimation.Values = new NSObject[]{NSValue.FromCATransform3D(CATransform3D.Identity),NSValue.FromCATransform3D(outPivotTransform)};
			outFlipAnimation.TimingFunctions = new CAMediaTimingFunction[]{CAMediaTimingFunction.FromName(CAMediaTimingFunction.Linear)};
			// @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
			//
			CAAnimationGroup inAnimation = CAAnimationGroup.CreateAnimation ();// [CAAnimationGroup animation];
			inAnimation.Animations = new CAAnimation[]{inFlipAnimation, ztran};
			inAnimation.Duration = Duration.TotalSeconds;
			//
			CAAnimationGroup outAnimation = CAAnimationGroup.CreateAnimation ();
			outAnimation.Animations = new CAAnimation[]{outFlipAnimation, ztran};
			outAnimation.Duration = Duration.TotalSeconds;
			reserve.OutAnimation = outAnimation;
			reserve.InAnimation = inAnimation;
			reserve.FinishInit ();
					return reserve;
				}
	}
}

