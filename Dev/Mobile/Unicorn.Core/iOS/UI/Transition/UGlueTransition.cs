﻿using System;
using CoreGraphics;
using CoreAnimation;
using Foundation;

namespace Unicorn.Core.Navigator
{
	public class UGlueTransition:UDualTransition
	{
		CGRect _rect;
		UTransitionOrientation _orien;

		public UGlueTransition (TimeSpan duration, UTransitionOrientation orien, CGRect rect) : base (duration)
		{
			_rect = rect;
			_orien = orien; 

			float viewWidth = (float)rect.Width;
			float viewHeight = (float)rect.Height;

			float widthAngle = (float)Math.PI / 4.0f;
			float heightAngle = (float)Math.PI / 6.0f;
//
			CABasicAnimation inSwipeAnimation = CABasicAnimation.FromKeyPath ("transform"); // animationWithKeyPath:@"transform"];
			inSwipeAnimation.To = NSValue.FromCATransform3D (CATransform3D.Identity); //[NSValue valueWithCATransform3D:CATransform3DIdentity];
//
			CGPoint anchorPoint = CGPoint.Empty;
			CATransform3D startTranslation = CATransform3D.Identity;
			CATransform3D rotation = CATransform3D.Identity;
			switch (orien) {
			case UTransitionOrientation.RightToLeft:
					
				inSwipeAnimation.From = NSValue.FromCATransform3D (CATransform3D.MakeTranslation (1.5f * viewWidth, 0.0f, 0.0f));
							//[NSValue valueWithCATransform3D:CATransform3DMakeTranslation(1.5 *viewWidth, 0.0f, 0.0f)];
				anchorPoint = new CGPoint (0, 0.5f);
				startTranslation = CATransform3D.MakeTranslation (-viewWidth * 0.5f, 0, 0);
				rotation = startTranslation.Rotate (widthAngle, 0, 1.0f, 0);
					
				break;
			case UTransitionOrientation.LeftToRight:
					
				inSwipeAnimation.From = NSValue.FromCATransform3D (CATransform3D.MakeTranslation (-1.5f * viewWidth, 0.0f, 0.0f));
							// [NSValue valueWithCATransform3D:CATransform3DMakeTranslation(- 1.5 * viewWidth, 0.0f, 0.0f)];
				anchorPoint = new CGPoint (1.0f, 0.5f);
				startTranslation = CATransform3D.MakeTranslation (viewWidth * 0.5f, 0, 0);
				rotation = startTranslation.Rotate (-widthAngle, 0, 1.0f, 0);

				break;
			case UTransitionOrientation.TopToBottom:

				inSwipeAnimation.From = NSValue.FromCATransform3D (CATransform3D.MakeTranslation (0.0f, -1.5f * viewHeight, 0.0f)); 
								// [NSValue valueWithCATransform3D:CATransform3DMakeTranslation(0.0f, - 1.5 * viewHeight, 0.0f)];
				anchorPoint = new CGPoint (0.5f, 1.0f);
				startTranslation = CATransform3D.MakeTranslation (0, viewHeight * 0.5f, 0);
				rotation = startTranslation.Rotate (heightAngle, 1.0f, 0, 0);

				break;
			case UTransitionOrientation.BottomToTop:

				inSwipeAnimation.From = NSValue.FromCATransform3D (CATransform3D.MakeTranslation (0.0f, 1.5f * viewHeight, 0.0f));
									// [NSValue valueWithCATransform3D:CATransform3DMakeTranslation(0.0f, 1.5 * viewHeight, 0.0f)];
				anchorPoint = new CGPoint (0.5f, 0);
				startTranslation = CATransform3D.MakeTranslation (0, -viewHeight * 0.5f, 0);
				rotation = startTranslation.Rotate (-heightAngle, 1.0f, 0, 0);

				break;
			default:
	//				NSAssert(FALSE, @"Unhandled ADTransitionOrientation");
				break;
			}


			inSwipeAnimation.Duration = Duration.TotalSeconds;
//
			CABasicAnimation inPositionAnimation = CABasicAnimation.FromKeyPath ("zPosition"); // [CABasicAnimation animationWithKeyPath:@"zPosition"];
			inPositionAnimation.From = NSNumber.FromFloat (-0.001f);
			inPositionAnimation.To = NSNumber.FromFloat (-0.001f);
			inPositionAnimation.Duration = Duration.TotalSeconds;
//
			CAAnimationGroup inAnimation = CAAnimationGroup.CreateAnimation ();
			inAnimation.Animations = new CAAnimation[]{ inSwipeAnimation, inPositionAnimation };
			inAnimation.Duration = Duration.TotalSeconds;
//
			CATransform3D endTranslation = startTranslation.Translate (0, 0, -viewWidth * 0.7f);
//
			CABasicAnimation outAnchorPointAnimation = CABasicAnimation.FromKeyPath ("anchorPoint");// [CABasicAnimation animationWithKeyPath:@"anchorPoint"];
			outAnchorPointAnimation.From = NSValue.FromCGPoint (anchorPoint);// [NSValue valueWithCGPoint:anchorPoint];
			outAnchorPointAnimation.To = NSValue.FromCGPoint (anchorPoint);//[NSValue valueWithCGPoint:anchorPoint];
//
			CAKeyFrameAnimation outTransformKeyFrameAnimation = CAKeyFrameAnimation.FromKeyPath ("transform"); // [CAKeyframeAnimation animationWithKeyPath:@"transform"];
			outTransformKeyFrameAnimation.Values = new NSObject[] {
				NSValue.FromCATransform3D (startTranslation),
				NSValue.FromCATransform3D (endTranslation)
			};
			//@[[NSValue valueWithCATransform3D:startTranslation], [NSValue valueWithCATransform3D:rotation], [NSValue valueWithCATransform3D:endTranslation]];

			outTransformKeyFrameAnimation.TimingFunctions = GetCircleApproximationTimingFunctions ();//[self getCircleApproximationTimingFunctions];
//
			CAKeyFrameAnimation outOpacityKeyFrameAnimation = CAKeyFrameAnimation.FromKeyPath ("opacity");//[CAKeyframeAnimation animationWithKeyPath:@"opacity"];
			outOpacityKeyFrameAnimation.Values = new NSObject[] {
				NSNumber.FromFloat (1.0f),
				NSNumber.FromFloat (1.0f),
				NSNumber.FromFloat (0.0f)
			};//@[@1.0f, @1.0f, @0.0f];
//
			CAAnimationGroup outAnimation = CAAnimationGroup.CreateAnimation ();
			outAnimation.Animations = new CAAnimation[] {
//				outOpacityKeyFrameAnimation,
				outTransformKeyFrameAnimation,
				outAnchorPointAnimation
			};
//						[outAnimation setAnimations:@[outOpacityKeyFrameAnimation, outTransformKeyFrameAnimation, outAnchorPointAnimation]];
			outAnimation.Duration = Duration.TotalSeconds;

			InAnimation = inAnimation;
			OutAnimation = outAnimation;

			FinishInit ();
		}

		public override UTransition GetReverseTransition ()
		{
			UDualTransition reserve = new UDualTransition (Duration);

			float viewWidth = (float)_rect.Width;
			float viewHeight = (float)_rect.Height;


			float widthAngle = (float)Math.PI / 4.0f;
			float heightAngle = (float)Math.PI / 6.0f;
			//
			CABasicAnimation inSwipeAnimation = CABasicAnimation.FromKeyPath ("transform"); // animationWithKeyPath:@"transform"];
			inSwipeAnimation.From = NSValue.FromCATransform3D (CATransform3D.Identity); //[NSValue valueWithCATransform3D:CATransform3DIdentity];
			//
			CGPoint anchorPoint = CGPoint.Empty;
			CATransform3D startTranslation = CATransform3D.Identity;
			CATransform3D rotation = CATransform3D.Identity;
			switch (_orien) {
			case UTransitionOrientation.RightToLeft:

				inSwipeAnimation.To = NSValue.FromCATransform3D (CATransform3D.MakeTranslation (1.5f * viewWidth, 0.0f, 0.0f));
				//[NSValue valueWithCATransform3D:CATransform3DMakeTranslation(1.5 *viewWidth, 0.0f, 0.0f)];
				anchorPoint = new CGPoint (0, 0.5f);
				startTranslation = CATransform3D.MakeTranslation (-viewWidth * 0.5f, 0, 0);
				rotation = startTranslation.Rotate (widthAngle, 0, 1.0f, 0);

				break;
			case UTransitionOrientation.LeftToRight:

				inSwipeAnimation.To = NSValue.FromCATransform3D (CATransform3D.MakeTranslation (-1.5f * viewWidth, 0.0f, 0.0f));
				// [NSValue valueWithCATransform3D:CATransform3DMakeTranslation(- 1.5 * viewWidth, 0.0f, 0.0f)];
				anchorPoint = new CGPoint (1.0f, 0.5f);
				startTranslation = CATransform3D.MakeTranslation (viewWidth * 0.5f, 0, 0);
				rotation = startTranslation.Rotate (-widthAngle, 0, 1.0f, 0);

				break;
			case UTransitionOrientation.TopToBottom:

				inSwipeAnimation.From = NSValue.FromCATransform3D (CATransform3D.MakeTranslation (0.0f, -1.5f * viewHeight, 0.0f)); 
				// [NSValue valueWithCATransform3D:CATransform3DMakeTranslation(0.0f, - 1.5 * viewHeight, 0.0f)];
				anchorPoint = new CGPoint (0.5f, 1.0f);
				startTranslation = CATransform3D.MakeTranslation (0, viewHeight * 0.5f, 0);
				rotation = startTranslation.Rotate (heightAngle, 1.0f, 0, 0);

				break;
			case UTransitionOrientation.BottomToTop:

				inSwipeAnimation.From = NSValue.FromCATransform3D (CATransform3D.MakeTranslation (0.0f, 1.5f * viewHeight, 0.0f));
				// [NSValue valueWithCATransform3D:CATransform3DMakeTranslation(0.0f, 1.5 * viewHeight, 0.0f)];
				anchorPoint = new CGPoint (0.5f, 0);
				startTranslation = CATransform3D.MakeTranslation (0, -viewHeight * 0.5f, 0);
				rotation = startTranslation.Rotate (-heightAngle, 1.0f, 0, 0);

				break;
			default:
				//				NSAssert(FALSE, @"Unhandled ADTransitionOrientation");
				break;
			}


			inSwipeAnimation.Duration = Duration.TotalSeconds;
			//
			CABasicAnimation inPositionAnimation = CABasicAnimation.FromKeyPath ("zPosition"); // [CABasicAnimation animationWithKeyPath:@"zPosition"];
			inPositionAnimation.From = NSNumber.FromFloat (-0.001f);
			inPositionAnimation.To = NSNumber.FromFloat (-0.001f);
			inPositionAnimation.Duration = Duration.TotalSeconds;
			//
			CAAnimationGroup inAnimation = CAAnimationGroup.CreateAnimation ();
			inAnimation.Animations = new CAAnimation[]{ inSwipeAnimation, inPositionAnimation };
			inAnimation.Duration = Duration.TotalSeconds;
			//
			CATransform3D endTranslation = startTranslation.Translate (0, 0, -viewWidth * 0.7f);
			//
			CABasicAnimation outAnchorPointAnimation = CABasicAnimation.FromKeyPath ("anchorPoint");// [CABasicAnimation animationWithKeyPath:@"anchorPoint"];
			outAnchorPointAnimation.From = NSValue.FromCGPoint (anchorPoint);// [NSValue valueWithCGPoint:anchorPoint];
			outAnchorPointAnimation.To = NSValue.FromCGPoint (anchorPoint);//[NSValue valueWithCGPoint:anchorPoint];
			//
			CAKeyFrameAnimation outTransformKeyFrameAnimation = CAKeyFrameAnimation.FromKeyPath ("transform"); // [CAKeyframeAnimation animationWithKeyPath:@"transform"];
			outTransformKeyFrameAnimation.Values = new NSObject[] {
				NSValue.FromCATransform3D (startTranslation),
				NSValue.FromCATransform3D (endTranslation)
			};
			//@[[NSValue valueWithCATransform3D:startTranslation], [NSValue valueWithCATransform3D:rotation], [NSValue valueWithCATransform3D:endTranslation]];

			outTransformKeyFrameAnimation.TimingFunctions = GetCircleApproximationTimingFunctions ();//[self getCircleApproximationTimingFunctions];
			//
			CAKeyFrameAnimation outOpacityKeyFrameAnimation = CAKeyFrameAnimation.FromKeyPath ("opacity");//[CAKeyframeAnimation animationWithKeyPath:@"opacity"];
//			outOpacityKeyFrameAnimation.Values = new NSObject[] {
//				NSNumber.FromFloat (0.0f),
//				NSNumber.FromFloat (1.0f),
//				NSNumber.FromFloat (1.0f),
//
//			};//@[@1.0f, @1.0f, @0.0f];
			//
			CAAnimationGroup outAnimation = CAAnimationGroup.CreateAnimation ();
			outAnimation.Animations = new CAAnimation[] {
//				outOpacityKeyFrameAnimation,
				outTransformKeyFrameAnimation,
				outAnchorPointAnimation
			};
			//						[outAnimation setAnimations:@[outOpacityKeyFrameAnimation, outTransformKeyFrameAnimation, outAnchorPointAnimation]];
			outAnimation.Duration = Duration.TotalSeconds;

			reserve.InAnimation = outAnimation;
			reserve.OutAnimation = inAnimation;

			reserve.FinishInit ();

			return reserve;
		}
	}
}

