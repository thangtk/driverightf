﻿using System;
using Unicorn.Core.Navigator;
using CoreGraphics;
using CoreAnimation;
using Foundation;
using UIKit;

namespace Unicorn.Core.Navigator 
{
	public class UZoomTransition:UDualTransition
	{
		protected CGRect _targetRect, _sourceRect;
		public UZoomTransition(TimeSpan duration, UTransitionOrientation orien,CGRect targetRect,CGRect sourceRect):base(duration){
			_targetRect = targetRect;
			_sourceRect = sourceRect;
//			CAKeyframeAnimation * backFadeTranslation = [CAKeyframeAnimation animationWithKeyPath:@"zPosition"];
//			backFadeTranslation.values = @[@0.0f, @-2000.0f, @0.0f];
			CAKeyFrameAnimation inTrans = CAKeyFrameAnimation.FromKeyPath ("zPosition");
			inTrans.Values = new NSNumber[]{NSNumber.FromFloat(0.0f),NSNumber.FromFloat(2000f),NSNumber.FromFloat(0.0f)};;
			CABasicAnimation zoomAnimation = CABasicAnimation.FromKeyPath ("transform"); //animationWithKeyPath:@"transform"];
			CATransform3D transform = CATransform3D.Identity;
			CGPoint sourceCenter = RectCenter(sourceRect);
			CGPoint targetCenter = RectCenter(targetRect);
			transform = transform.Translate(sourceCenter.X - targetCenter.X, sourceCenter.Y - targetCenter.Y, 0.0f);
			transform = transform.Scale( sourceRect.Width/targetRect.Width, sourceRect.Height/targetRect.Height, 1.0f);
			zoomAnimation.From = NSValue.FromCATransform3D(transform);// valueWithCATransform3D:transform];
			zoomAnimation.To = NSValue.FromCATransform3D(CATransform3D.Identity);//valueWithCATransform3D:CATransform3DIdentity];
//			zoomAnimation.Duration = duration.TotalSeconds;
			//
//			UIView a;a.Layer.z
			CABasicAnimation outAnimation = CABasicAnimation.FromKeyPath ("zPosition");// animationWithKeyPath:@"zPosition"];
			outAnimation.From = NSNumber.FromFloat( -0.001f);
			outAnimation.To = NSNumber.FromFloat(-0.001f);
			outAnimation.Duration = duration.TotalSeconds;

//			CAAnimationGroup * inAnimation = [CAAnimationGroup animation];
//			[inAnimation setAnimations:@[backFadeTranslation, inFadeAnimation]];
//			inAnimation.duration = duration;
			CAAnimationGroup inGroup = CAAnimationGroup.CreateAnimation ();
			inGroup.Animations = new CAAnimation[]{inTrans,zoomAnimation };
			inGroup.Duration = duration.TotalSeconds;
			InAnimation = inGroup;
			OutAnimation = outAnimation;
			FinishInit ();
			//
			//			self = [super initWithInAnimation:zoomAnimation andOutAnimation:outAnimation];

		}

		private CGPoint RectCenter(CGRect rect) {
			return new CGPoint(rect.X + rect.Width/2.0f, rect.Y + rect.Height/2.0f);
		}

		public override UTransition GetReverseTransition (){

			UDualTransition reserve = new UDualTransition (Duration);

			CABasicAnimation zoomAnimation = CABasicAnimation.FromKeyPath ("transform"); //animationWithKeyPath:@"transform"];
			CATransform3D transform = CATransform3D.Identity;
			CGPoint sourceCenter = RectCenter(_sourceRect);
			CGPoint targetCenter = RectCenter(_targetRect);
			transform = transform.Translate(sourceCenter.X - targetCenter.X, sourceCenter.Y - targetCenter.Y, 0.0f);
			transform = transform.Scale( _sourceRect.Width/_targetRect.Width, _sourceRect.Height/_targetRect.Height, 1.0f);
			zoomAnimation.To = NSValue.FromCATransform3D(transform);// valueWithCATransform3D:transform];
			zoomAnimation.From = NSValue.FromCATransform3D(CATransform3D.Identity);//valueWithCATransform3D:CATransform3DIdentity];
			zoomAnimation.Duration = Duration.TotalSeconds;
			//
			CABasicAnimation outAnimation = CABasicAnimation.FromKeyPath ("zPosition");// animationWithKeyPath:@"zPosition"];
			outAnimation.From = NSNumber.FromFloat( -0.001f);
			outAnimation.To = NSNumber.FromFloat(-0.001f);
			outAnimation.Duration = Duration.TotalSeconds;
			reserve.OutAnimation = zoomAnimation;
			reserve.InAnimation = outAnimation;
			reserve.FinishInit ();
			return reserve;
		}
	}
}

