﻿using System;
using Unicorn.Core.UI;
using System.Collections.Concurrent;

namespace Unicorn.Core.Navigator 
{
	public abstract class UBaseViewControllerFactory
	{
		private ConcurrentDictionary<int,UBaseViewController> _controllerList = new ConcurrentDictionary<int,UBaseViewController> ();

		public UBaseViewController CreateViewController (int s){
			if (_controllerList.ContainsKey (s)) {
				return _controllerList [s];
			}
			var ubase = CreateViewInnerController (s);
			if (ubase.IsShouldCache()) {
				_controllerList.TryAdd(s,ubase);
			}
			return ubase;
		}
		protected abstract UBaseViewController CreateViewInnerController (int s);
	}
}

