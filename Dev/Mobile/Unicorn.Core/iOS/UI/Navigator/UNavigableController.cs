﻿using System;
using UIKit;
using CoreAnimation;
using System.Collections.Generic;
using Unicorn.Core.UI;
using Foundation;

namespace Unicorn.Core.Navigator
{
	public abstract class UNavigableController : UBaseViewController,ITransitionDelegate
	{
		public UTransitionControllerDelegate UDelegate{ get; set;}
		public List<UIViewController> ViewControllers{
			get{ return _listControllers; }
		}

		private  float UNavigationBarHeight = 44.0f;
		private  float UToolbarHeight = 44.0f;
		private  float UZDistance = 1000.0f;

		protected UIView _navigationBar;
		protected UIView _toolbar;
		protected UIView _containerView;

		public UIView ContainerView{
			get{ return _containerView;}
			set{ _containerView = value;}
		}

		private List<UIViewController> _listControllers =new List<UIViewController>();
		private List<UTransition> _transitions = new List<UTransition>();
		private bool _isContainerViewTransitioning = false;
		private bool _isNavigationBarTransitioning = false;
		private bool _shouldPopItem = false;
		public UIViewController TopViewController {
			get{
				if (_listControllers.Count <= 0)
					return null;
				return _listControllers [_listControllers.Count - 1];
			}
		}
		public UIViewController VisibleViewController()
		{
			var top = this.TopViewController;
			if (top.PresentedViewController != null) {
				return top.PresentedViewController;
			} else {
				return top;
			}
		}

		protected UNavigableController (string nibNameOrNull, NSBundle nibBundleOrNull)
			:base(nibNameOrNull,nibBundleOrNull)
		{

		}

		protected UNavigableController (IntPtr handle)
			:base(handle)
		{

		}

		protected UNavigableController (NSObjectFlag f)
			:base(f)
		{

		}

		protected UNavigableController ():base()
		{
		}

		protected UNavigableController (NSCoder c):base(c)
		{

		}

		//		#pragma mark -
		//		#pragma mark Appearance
		// Forwarding appearance messages when the container appears or disappears
		public override void ViewWillAppear(bool animated){
			base.ViewWillAppear (animated);
			if(_listControllers.Count > 0)
				TopViewController .BeginAppearanceTransition (true, animated);
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear (animated);
			if(_listControllers.Count > 0)
				TopViewController .EndAppearanceTransition ();
		}

		public override void ViewWillDisappear(bool animated){
			base.ViewWillDisappear (animated);
			if(_listControllers.Count > 0)
				TopViewController .BeginAppearanceTransition (false, true);
		}

		public override void ViewDidDisappear(bool animated){
			base.ViewDidDisappear (animated);
			if(_listControllers.Count > 0)
				TopViewController .EndAppearanceTransition ();
		}

		public override bool ShouldAutomaticallyForwardAppearanceMethods{
			get{
				return false;
			}
		}


		//		#pragma mark -
		//		#pragma mark Push
		public void PushViewController(UIViewController controller, UTransition transition){
			if (_isContainerViewTransitioning || _isNavigationBarTransitioning) {
				return;
			}
			var removeView = TopViewController ;
			_listControllers.Add (controller);
			if (transition == null) {
				_transitions.Add (UTransition.NullTransaction);
			} else {
				_transitions.Add (transition);
			}

			if (_containerView == null) {

				return;
			}

			bool animated = transition != null;

			UIView viewIn = controller.View;

			if (UDelegate != null && removeView != null) {
				UDelegate.StartPushView (this, null, animated);
			}

			//			viewIn.Frame = Nav
			AddChildViewController (controller);
			controller.BeginAppearanceTransition (true, animated);
			if (UDelegate != null) {
				UDelegate.WillShowViewController (this, controller, animated);
			}

			var frame = _containerView.Frame;
			frame.X = 0f;
			frame.Y = 0f;
			viewIn.Frame = frame;
			//viewIn.AutosizesSubviews = true;
			//viewIn.AutoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth;
			_containerView.AddSubview (viewIn);

			if (removeView != null) {
				removeView.BeginAppearanceTransition (true, animated);
				UIView viewOut = removeView.View;
				_isContainerViewTransitioning = animated;
				if (transition != null) {
					transition.Delegate = this;
					transition.Type = UTransitionType.Push;
				}
				TransitionFromView (viewOut, viewIn, transition);
				if (!animated) {
					PushTransitionDidFinish (null);
				}
			}
		}

		public virtual void PushTransitionDidFinish(ITransition transition ){
			bool animated = transition == null ? false : true;
			if (_listControllers.Count >= 2) {
				UIViewController viewOut = _listControllers [_listControllers.Count - 2];
				viewOut.View.RemoveFromSuperview ();
				viewOut.EndAppearanceTransition ();
			}
			UIViewController viewIn = TopViewController ;
			viewIn.EndAppearanceTransition ();
			viewIn.DidMoveToParentViewController (this);
			_isContainerViewTransitioning = false;
			if (UDelegate != null) {
				UDelegate.DidShowViewController (this, viewIn, animated);
				UDelegate.PushViewDidFinish (this, viewIn, animated);
			}
		}

		public UIViewController PopViewController(){

			if (_transitions.Count <= 0) {
				return null;
			}
			var transition = _transitions[_transitions.Count-1].GetReverseTransition() ;
			return PopViewControllerWithTransition (transition);
		}

		public UIViewController PopViewControllerWithTransition(UTransition transition){
			if (_listControllers.Count < 2 || _isContainerViewTransitioning || _isNavigationBarTransitioning) {
				return null;
			}

			bool animated = transition == null ? false : true;

			_transitions.RemoveAt (_transitions.Count - 1);
			UIViewController viewIn = _listControllers [_listControllers.Count - 2];

			if (UDelegate != null) {
				UDelegate.StartPopView (this, viewIn, animated);
			}

			viewIn.BeginAppearanceTransition (true, animated);
			if (UDelegate != null) {
				UDelegate.WillShowViewController (this, viewIn, animated);
			}
			viewIn.View.Frame = _containerView.Bounds;
			_containerView.AddSubview(viewIn.View);

			var viewOut = TopViewController ;
			viewOut.WillMoveToParentViewController (null);
			viewOut.BeginAppearanceTransition (false, animated);
			//			_isNavigationBarTransitioning = animated;

			_shouldPopItem = true;

			_isContainerViewTransitioning = animated;

			if (transition != null) {
				transition.Delegate = this;
				transition.Type = UTransitionType.Pop;
			}

			TransitionFromView (viewOut.View, viewIn.View, transition);
			if (!animated) {
				PopTransitionDidFinish (null);
			}
			return viewOut;
		}

		public UIViewController[] PopToViewController(UIViewController controller){
			if (_transitions.Count<=0) {
				return null;
			}

			return PopToViewController (controller, null);
		}


		public UIViewController[] PopToViewController(UIViewController controller,UTransition transactionGlo = null){
			int index = _listControllers.IndexOf (controller);
			if (index < 0 || _isContainerViewTransitioning || _isNavigationBarTransitioning) {
				return null;
			}

			int totalCount = _listControllers.Count - index - 1;
			UIViewController[] outControllers = new UIViewController[totalCount];


			UTransition transaction = _transitions [_transitions.Count - 1].GetReverseTransition();

			for (int i = index + 1,j=0; i < _listControllers.Count; i++) {
				outControllers [j++] = _listControllers [i];
			}

			foreach(var viewController in outControllers){
				_listControllers.RemoveAt (_listControllers.Count - 1);
				_transitions.RemoveAt (_transitions.Count - 1);
				viewController.WillMoveToParentViewController (null);
				viewController.RemoveFromParentViewController ();
			}

			_transitions.Add (transaction);

			if (outControllers.Length > 0) {
				UIViewController viewOut = outControllers [outControllers.Length - 1];
				_listControllers.Add (viewOut);
				AddChildViewController (viewOut);
				viewOut.DidMoveToParentViewController (this);
			}

			PopViewControllerWithTransition (transactionGlo == null ? transaction : transactionGlo);

			return outControllers;
		}

		public UIViewController[] PopToRootViewController(){
			if (_transitions.Count <= 0) {
				return null;
			}
			return PopToRootViewControllerWithTransition(_transitions[_transitions.Count-1].GetReverseTransition());
		}

		public UIViewController[] PopToRootViewControllerWithTransition(UTransition transition){
			if (_listControllers.Count > 1) {
				return PopToViewController (_listControllers [0], transition);
			}
			return null;
		}

		public virtual void PopTransitionDidFinish(ITransition transition){
			bool animated = transition == null ? false : true;
			_containerView.Layer.Transform = CATransform3D.Identity;

			UIViewController viewOut = TopViewController ;
			viewOut.View.RemoveFromSuperview ();
			viewOut.EndAppearanceTransition ();
			viewOut.RemoveFromParentViewController ();
			_listControllers.RemoveAt (_listControllers.Count-1); 

			var viewIn = TopViewController ;
			viewIn.View.Layer.Transform = CATransform3D.Identity;
			viewIn.EndAppearanceTransition ();
			_isContainerViewTransitioning = false;
			if (UDelegate != null) {
				UDelegate.DidShowViewController (this, viewIn, animated);
				UDelegate.PopViewDidFinish (this, viewIn, animated);
			}
		}

		//		#pragma mark -
		//		#pragma mark UINavigationBar

		public void SetNavigationBarHidden(bool hidden, bool animated){
			float navH = (float)_navigationBar.Frame.Height;
			if (animated) {
				UIView.BeginAnimations(null);
			}

			if (animated) {
				UIView.CommitAnimations ();
			}
		}

		//		#pragma mark -
		//		#pragma mark UINavigationBarDelegate

		public bool NavigationBarShouldPopItem(){
			if (_shouldPopItem) {
				_shouldPopItem = false;
				return true;
			} else {
				this.PopViewController ();
				return false;
			}
		}

		private void SetupLayers(params CALayer[] layers){
			foreach (var layer in layers) {
				layer.ShouldRasterize = true;
				layer.RasterizationScale = UIScreen.MainScreen.Scale;
			}
		}

		private void TearDownLayers(params CALayer[] layers){
			foreach (var layer in layers) {
				layer.ShouldRasterize = false;
			}
		}

		private void TransitionFromView(UIView viewOut, UIView viewIn,UTransition transition){
			viewIn.Layer.DoubleSided = false;
			viewOut.Layer.DoubleSided = false;

			SetupLayers (viewIn.Layer, viewOut.Layer);
			//			CATransaction.Begin ();
			CATransaction.CompletionBlock = ()=>{
				TearDownLayers(viewIn.Layer,viewOut.Layer);
			};
			//			CATransaction.Commit ();
			transition.Animation (viewIn, viewOut);
		}

		public override void LoadView(){
			base.LoadView ();
//			_containerView = vPage;
		}


	}
	public class UTransitionControllerDelegate {
		public virtual void WillShowViewController(UIViewController transitionController,UIViewController viewController , bool animated){
		}
		public virtual void DidShowViewController(UIViewController transitionController ,UIViewController viewController ,bool animated){

		}

		public virtual void PopViewDidFinish(UIViewController transitionController ,UIViewController viewController ,bool animated){

		}

		public virtual void PushViewDidFinish(UIViewController transitionController ,UIViewController viewController ,bool animated){

		}

		public virtual void StartPopView(UIViewController transitionController ,UIViewController viewController ,bool animated){

		}

		public virtual void StartPushView(UIViewController transitionController ,UIViewController viewController ,bool animated){

		}
	}

}

