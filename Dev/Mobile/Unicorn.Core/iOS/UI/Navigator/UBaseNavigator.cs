﻿using System;
using UIKit;
using ReactiveUI;
using System.Collections.Generic;
using Unicorn.Core.UI;
using Foundation;
using System.Collections;
using System.Collections.Concurrent;
using System.Linq;

namespace Unicorn.Core.Navigator 
{
	public abstract class UBaseNavigator : INavigator
	{
		private UIWindow _mainWindow;


		private UINavigationController _navController;
		private UIViewController _rootViewController;
		private Stack<int> _previousScreens = new Stack<int> ();
		public int PreviousScreen { get; set; }

		public int CurrentScreen { get; private set; }
		public UBaseViewControllerFactory ViewControllerFactory {get{ return DependencyService.Get<UBaseViewControllerFactory> ();}} 
		public UINavigationController NavigationController
		{
			get
			{
				return _navController;
			}
		}
		public UIWindow Window
		{
			get
			{
				return _mainWindow;
			}
		}

		public UBaseNavigator (UIWindow window)
		{
			_mainWindow = window;
			_navController = new UBaseNavController ();
			_rootViewController = new UIViewController ();
			_rootViewController.View.Frame = _mainWindow.Frame;

			_rootViewController.View.AddSubview (_navController.View);

			_navController.View.Frame = _mainWindow.Frame;
			_navController.NavigationBarHidden = true;
			_mainWindow.RootViewController = _rootViewController;
			CurrentScreen = -1;
			PreviousScreen = -1;
		}

		public void NavigateBack (int screenId, bool isRefresh = false)
		{
			UBaseViewController ubase = ViewControllerFactory.CreateViewController(screenId);
			if (_navController.ViewControllers.Contains (ubase)) {
				_navController.PopToViewController (ubase, true);
				if (isRefresh && _navController.TopViewController is UBaseNavController) {
					UBaseViewController controller = _navController.TopViewController as UBaseViewController;
					controller.ReloadView ();
				}
			} else {
				Navigate (screenId, new object[0]);
			}

		}

		public void NavigateBack (bool isRefresh = false)
		{
			_navController.PopViewController (true);

			if (isRefresh )
			{
				UBaseViewController controller = _navController.TopViewController as UBaseViewController;
				if(controller != null)
				{
					controller.ReloadView ();
				}
			}

		}




		public void NavigateToStart (int screenId)
		{
			_navController.PopToRootViewController (false);
			_previousScreens.Clear ();
			PreviousScreen = CurrentScreen = -1;
			UBaseViewController ubase = ViewControllerFactory.CreateViewController(screenId);

			if (_navController.TopViewController != ubase) {
				_navController.PushViewController (ubase, false);
			}


			ubase.LoadData (new object[0]);
		}

		public void Navigate (int screenId,object[] objs,object[] transitionParam = null)
		{
			UBaseViewController ubase = ViewControllerFactory.CreateViewController(screenId);
			if (_navController.ViewControllers.Contains (ubase)) {
				_navController.PopToViewController (ubase, true);
				if ( _navController.TopViewController is UBaseNavController) {
					UBaseViewController controller = _navController.TopViewController as UBaseViewController;
//					
				}
				ubase.LoadData (objs);
				ubase.ReloadView ();
			} else {
				if (_navController.TopViewController != ubase) {
					_navController.PushViewController (ubase, true);
				}
				ubase.LoadData (objs);
				ubase.ReloadView ();
			}
			CurrentScreen = screenId;
		}

		public void OpenMenu ()
		{
		}

		public void CloseMenu ()
		{

		}

		public void ToggleMenu ()
		{

		}

		private void CleanUpMemory (int screen, List<int> targetScreens)
		{
			if (targetScreens.Contains (screen))
			{
				ReleaseViewControllers ();
			}
		}

		private void ReleaseViewControllers ()
		{
//			_controllerList.RemoveAll (c => true);
			GC.Collect ();
		}

		protected virtual void OnNavigating (int nextScreen)
		{

		}

		private void HideKeyboard ()
		{
			if (_navController == null || _navController.TopViewController == null)
			{
				return;
			}

			UIView topView = _navController.TopViewController.View;
			ResignResponder (topView);
		}

		private void ResignResponder (UIView topView)
		{
			topView.EndEditing (true);

		}

		protected abstract UBaseViewController CreateViewController (int screen);

	}
}

