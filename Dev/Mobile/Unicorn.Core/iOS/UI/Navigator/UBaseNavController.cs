﻿using System;
using UIKit;

namespace Unicorn.Core.Navigator
{
	public class UBaseNavController : UINavigationController
	{
		public bool HasPlainView { get; set; }

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
		}
	}
}

