﻿using System;
using UIKit;
using CoreGraphics;
using Unicorn.Core.Enums;
using Foundation;
using System.Collections.Generic;
using Unicorn.Core.iOS;

[assembly: Unicorn.Core.Dependency.Dependency (typeof (Unicorn.Core.UI.iOSDialog))]
namespace Unicorn.Core.UI
{
	/// <summary>
	/// Unused
	/// </summary>
	public class iOSDialog : UIView, IDialog
	{
		protected readonly int MAX_CONTENT_LINES = 300;
		protected readonly nfloat HEADER_HEIGHT = 36f;
		protected readonly nfloat FOOTER_HEIGHT = 40f;
		protected readonly nfloat MIN_CONTENT_HEIGHT = 60f;
		protected readonly nfloat MAX_DIALOG_HEIGHT = 600f;
		protected readonly nfloat CONTENT_PADDING_TOP = 4f;
		protected readonly nfloat CONTENT_PADDING_BOTTOM = 12f;
		protected readonly nfloat CONTENT_PADDING_LEFT = 12f;
		protected readonly nfloat CONTENT_PADDING_RIGHT = 12f;
		protected readonly nfloat ICON_SIZE = 36f;
		protected readonly nfloat ICON_PADDING_LEFT = 12f;
		protected const float OVERLAY = 0.3f;
		private const float WIDTH_MULTIPLIER = 0.9f;

		protected UILabel _lblHeader, _lblContent;
		protected UIImageView _dialogIcon;
		protected UIView _parentView, _dialogView, _headerView, _contentView, _footerView, _iconView;

		public UIView ParentView {
			get{ 
				if (_parentView == null) {
					_parentView = UIApplication.SharedApplication.KeyWindow.Subviews [0];
				}
				return _parentView;
			}
		}

		public nfloat ContentHeight {
			get{ 
				if (_contentView == null || _contentView.Frame.Height < MIN_CONTENT_HEIGHT) {
					return MIN_CONTENT_HEIGHT;
				}
				return _contentView.Frame.Height;
			}
			set{ 
				if (_contentView.Frame.Height != value) {
					var temp = _contentView.Frame;
					temp.Size = new CGSize (temp.Size.Width, value);
					_contentView.Frame = temp;
					// Update layout
					UpdateLayout ();
				}
			}
		}

		public nfloat IconViewWidth {
			get{ 
				return _iconView.Frame.Width;
			}
			set { 
				if (_iconView.Frame.Width != value) {
					var temp = _iconView.Frame;
					_iconView.Frame = new CGRect (temp.Location, new CGSize (value, temp.Height));
					// Update Content Height
					UpdateContentView ();
				}
			}
		}

		public nfloat DialogHeight {
			get { 
				return HEADER_HEIGHT + FOOTER_HEIGHT + ContentHeight;
			}
		}

		public nfloat DialogWidth{
			get { 
				return this.Frame.Width*WIDTH_MULTIPLIER;
			}
		}

		protected void UpdateLayout(){
			_dialogView.Frame = new CGRect ((UIScreen.MainScreen.Bounds.Width - DialogWidth) / 2, (UIScreen.MainScreen.Bounds.Height - DialogHeight) / 2,
				DialogWidth, DialogHeight);
			CGRect temp;
			temp = _footerView.Frame;
			temp.Location = new CGPoint (0, DialogHeight - FOOTER_HEIGHT);
			_footerView.Frame =  temp;
		}

		public void Initialize()
		{
			BackgroundColor = UIColor.Clear;
			// Add Overlay view
			UIView overlayView = new UIView (this.Frame);
			overlayView.BackgroundColor = UIColor.Black;
			overlayView.Layer.Opacity = OVERLAY;
			this.AddSubview (overlayView);
			// Create views first
			CreateHeader();
			CreateContent();
			InitFooter();
			//
			_dialogView = new UIView();
			_dialogView.BackgroundColor = UIColor.White;
			_dialogView.Layer.MasksToBounds = true;
			_dialogView.Layer.CornerRadius = 10f;
			_dialogView.Frame = new CGRect (0, 0, DialogWidth, DialogHeight);
			//
			_dialogView.AddSubviews(_headerView, _contentView, _footerView);
			this.AddSubview(_dialogView);
			// Default layout - it will be changed by icon or content
			UpdateLayout ();
		}

		private void CreateHeader()
		{
			_lblHeader = new UILabel();
			_lblHeader.TextAlignment = UITextAlignment.Center;
			_lblHeader.TextColor = UIColor.Black;
			_lblHeader.Font = UIFont.SystemFontOfSize(20);
			_lblHeader.BackgroundColor = UIColor.Clear;
			_lblHeader.Frame = new CGRect(0, 0, DialogWidth, HEADER_HEIGHT);
			//
			_headerView = new UIView(new CGRect(0, 0, DialogWidth, HEADER_HEIGHT));
			_headerView.BackgroundColor = UIColor.Clear;
			_headerView.AddSubview (_lblHeader);
		}

		private void CreateContent()
		{
			// Create icon area
			_dialogIcon = new UIImageView ();
			_dialogIcon.Frame = new CGRect(ICON_PADDING_LEFT, 0, ICON_SIZE, ICON_SIZE);
			_dialogIcon.Image = GetImage (Icon);
			//
			_iconView = new UIView ();
			_iconView.BackgroundColor = UIColor.Clear;
			_iconView.AddSubview (_dialogIcon);
			//
			_lblContent = new UILabel();
			_lblContent.TextAlignment = UITextAlignment.Center;
			_lblContent.TextColor = UIColor.Black;
			_lblContent.LineBreakMode = UILineBreakMode.WordWrap;
			_lblContent.Lines = MAX_CONTENT_LINES;
			_lblContent.BackgroundColor = UIColor.Clear;
			_lblContent.Frame = new CGRect (0, 0, 0, 0);
			// Defalt height is MIN_CONTENT_HEIGHT
			_contentView = new UIView (new CGRect (0, HEADER_HEIGHT, DialogWidth, MIN_CONTENT_HEIGHT));
			_contentView.AddSubviews (_iconView, _lblContent);
		}

		private void InitFooter (){
			_footerView = new UIView (new CGRect(0, 0, DialogWidth, FOOTER_HEIGHT));
			_footerView.BackgroundColor = UIColor.LightGray;
		}

		private void CreateFooterContent(List<DialogButton> buttons){
			foreach (var v in _footerView.Subviews) {
				v.RemoveFromSuperview ();
			}
			//
			if (buttons == null || buttons.Count == 0) {
				buttons = new List<DialogButton> (){ 
					new DialogButton(){
						Text = "OK",
						OnClicked = null
					}
				};
			}

			// Caculate size
			int count = buttons.Count;
			nfloat buttonWidth = (_footerView.Frame.Width - count + 1) / count;
			nfloat buttonHeight = _footerView.Frame.Height - 1;

			for (int i = 0; i < buttons.Count; i++) {
				var p = buttons [i];
				UIButton btn = new UIButton (UIButtonType.System);
				btn.SetTitleColor(UIColor.Blue, UIControlState.Normal);
				btn.BackgroundColor = UIColor.White;
				btn.SetTitle (p.Text, UIControlState.Normal);
				btn.TouchUpInside += (sender, e) => {
					if(p.OnClicked != null){
						p.OnClicked.Invoke();
					}
					this.Hide();
				};
				btn.Frame = new CGRect (i*buttonWidth + i, 1, buttonWidth, buttonHeight);
				_footerView.AddSubview (btn);
			}
		}

		protected UIImage GetImage(DialogIcon type){
			string iconFile = "icon_info.png";
			switch (type) {
			case DialogIcon.Error:
				iconFile = "icon_error.png";
				break;
			case DialogIcon.Info:
				iconFile = "icon_info.png";
				break;
			case DialogIcon.Question:
				iconFile = "icon_question.png";
				break;
			case DialogIcon.Warning:
				iconFile = "icon_warning.png";
				break;
			case DialogIcon.None:
				iconFile = string.Empty;
				break;
			}
			//
			if(string.IsNullOrEmpty(iconFile)){
				return null;
			}
			//
			return UIImage.FromBundle (iconFile);
		}

		private void UpdateContentView(){
			CGRect temp;
			temp = _lblContent.Frame;
			temp.Size = new CGSize (DialogWidth - IconViewWidth - CONTENT_PADDING_LEFT - CONTENT_PADDING_RIGHT, ContentHeight - CONTENT_PADDING_TOP - CONTENT_PADDING_BOTTOM);
			temp.Location = new CGPoint (IconViewWidth + CONTENT_PADDING_LEFT, 0);
			_lblContent.Frame = temp;
			_lblContent.ResizeHeigthWithText ();
//			ResizeHeigthWithText (_lblContent);
			//
			var contentHeight = _lblContent.Frame.Height + CONTENT_PADDING_TOP + CONTENT_PADDING_BOTTOM;
			if (contentHeight < MIN_CONTENT_HEIGHT) {
				contentHeight = MIN_CONTENT_HEIGHT;
			} else if (DialogHeight > MAX_DIALOG_HEIGHT) {
				contentHeight = MAX_DIALOG_HEIGHT - HEADER_HEIGHT - FOOTER_HEIGHT;
				// TODO: wrap label content into a scroll view. Do it later
			}
			//
			ContentHeight = contentHeight;
		}

		#region Implements
		private NSObject _owner = new NSObject ();

		public string Title {
			get {
				return _lblHeader.Text;
			}
			set {
				_lblHeader.Text = value;
			}
		}

		public string Content {
			get {
				return _lblContent.Text;
			}
			set {
				if (_lblContent.Text != value) {
					_lblContent.Text = value;
					UpdateContentView ();
				}
			}
		}

		public void Show ()
		{
			if (DialogButtons == null) {
				CreateFooterContent (null);
			}
			//
			_dialogView.Scale (true); // Animation
			ParentView.AddSubview (this);
		}

		public void Hide ()
		{
			_owner.BeginInvokeOnMainThread (this.RemoveFromSuperview);
		}

		private DialogIcon _icon = DialogIcon.None;

		public DialogIcon Icon {
			get{ return _icon; }
			set {
				if (_icon != value) {
					_icon = value;
					if (_icon == DialogIcon.None) {
						IconViewWidth = 0;
					} else {
						_dialogIcon.Image = GetImage (_icon);
						IconViewWidth = ICON_SIZE + ICON_PADDING_LEFT;
					}
				}
			}
		}

		public iOSDialog ()
		{
			this.Frame = ParentView.Frame;
			Initialize ();
		}

		private  List<DialogButton> _dialogButtons = null;
		public List<DialogButton> DialogButtons {
			get {
				return _dialogButtons;
			}
			set {
				if (value != null) {
					_dialogButtons = value;
					CreateFooterContent (value);
				}
			}
		}

		#endregion
	}
}

