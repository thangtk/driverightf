﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;
using Unicorn.Core.iOS;

namespace Unicorn.Core
{
	public class ViewWithKeyboard : UIView
	{
		protected bool _isShown = false;
		private UIView _activeView;
		private float _scrollAmount = 0.0f;
		private bool _moveViewUp = false;

		private void RegisterKeyboardObserver ()
		{
			NSNotificationCenter.DefaultCenter.AddObserver
			(UIKeyboard.WillShowNotification, notification => {
				KeyboardWillShow (notification);
			});
			//
			NSNotificationCenter.DefaultCenter.AddObserver
			(UIKeyboard.WillHideNotification, n => KeyboardWillHide ());
		}

		public ViewWithKeyboard ()
		{
			RegisterKeyboardObserver ();
		}

		private void KeyboardWillShow (NSNotification notification)
		{

			// get the keyboard size
			CGRect r = UIKeyboard.BoundsFromNotification(notification);

			_activeView = KeyboardExtension.GetViewResponder(this);

			// Calculate how far we need to scroll
			if(!_isShown && _activeView != null)
			{
				CGPoint relativeFrame = _activeView.ConvertPointToParentView(this);

				nfloat bottom = Frame.Height - relativeFrame.Y - _activeView.Frame.Height;
				_scrollAmount = (float)(r.Height - bottom);
				if(_scrollAmount > 0)
				{
					_moveViewUp = true;
					SetViewMovedUp(_moveViewUp);
				}
				else
				{
					_moveViewUp = false;
				}
				_isShown = true;
			}
		}

		void KeyboardWillHide ()
		{
			if(_moveViewUp && _isShown)
			{
				SetViewMovedUp(false);
			}
			_isShown = false;
		}

		void DidBeginEditing (UITextField textField)
		{
			
		}

		protected nfloat _bottomSpace { get; set; }

		void SetViewMovedUp (bool move)
		{
			UIView.BeginAnimations(string.Empty, System.IntPtr.Zero);
			UIView.SetAnimationDuration(0.3);

			CGRect frame = this.Frame;

			if(move)
			{
				frame.Y -= _scrollAmount;
			}
			else
			{
				frame.Y += _scrollAmount;
				_scrollAmount = 0;
			}

			this.Frame = frame;
			UIView.CommitAnimations();
		}

	}
}

