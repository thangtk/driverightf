﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;
using Unicorn.Core.iOS;

[assembly: Unicorn.Core.Dependency.Dependency (typeof(Unicorn.Core.UI.iOSToast))]
namespace Unicorn.Core.UI
{
	public class iOSToast : UIView, IToast
	{
		private NSObject _owner = new NSObject ();

		private UIView _parentView;
		public UIView ParentView {
			get{ 
				if (_parentView == null) {
					_parentView = UIApplication.SharedApplication.KeyWindow.Subviews [0];
				}
				return _parentView;
			}
		}

		private UIView _bgView;
		private UILabel _contentView;

		private const float WIDTH_MULTIPLIER = 0.9f;
		private const float MIN_HEIGHT = 40f;
		private const float MAX_HEIGHT = 300f; // Not used
		private const float VERTICAL_PADDING = 12;
		private const float HORIZONTAL_PADDING = 8;
		private const float VERTICAL_MARGIN = 12;
		protected readonly int MAX_CONTENT_LINES = 300;

		private DialogPosition _dialogPosition;
		public DialogPosition DialogPosition {
			get{ 
				return _dialogPosition;
			}
			set{ 
				if (_dialogPosition != value) {
					_dialogPosition = value;
					UpdateLayout ();
				}
			}
		}

		public float DialogWidth{
			get { 
				return (float)this.ParentView.Frame.Width*WIDTH_MULTIPLIER;
			}
		}

		private float _dialogHeight = MIN_HEIGHT;
		public float DialogHeight {
			get{ 
				return _dialogHeight;
			}
			set{ 
				if (_dialogHeight != value && value > MIN_HEIGHT) {
					_dialogHeight = value;
					UpdateLayout ();
				}
			}
		}

		public iOSToast ()
		{
			InitView ();
		}

		private void InitView(){
//			this.BackgroundColor = UIColor.Black;
//			this.Layer.Opacity = 0.5f;
//			this.Layer.MasksToBounds = true;
//			this.Layer.CornerRadius = 8f;
			this.Frame = new CGRect (0, 0, DialogWidth, DialogHeight);
			this.BackgroundColor = UIColor.Clear;
			//
			_bgView = new UIView();
			_bgView.BackgroundColor = UIColor.Black;
			_bgView.Layer.Opacity = 0.5f;
			_bgView.Layer.MasksToBounds = true;
			_bgView.Layer.CornerRadius = 8f;
			_bgView.Frame = new CGRect (0, 0, this.Frame.Width, this.Frame.Height);
			//
			// Init content view as a label
			_contentView = new UILabel();
			_contentView.Frame = new CGRect (HORIZONTAL_PADDING, VERTICAL_PADDING, DialogWidth - 2 * HORIZONTAL_PADDING, 0);
			_contentView.TextAlignment = UITextAlignment.Center;
			_contentView.TextColor = UIColor.White;
			_contentView.LineBreakMode = UILineBreakMode.WordWrap;
			_contentView.Lines = MAX_CONTENT_LINES;
			_contentView.BackgroundColor = UIColor.Clear;
			//
			this.AddSubviews (_bgView, _contentView);
			UpdateLayout ();
		}

		/// <summary>
		/// Relocate
		/// </summary>
		private void UpdateLayout(){
			CGPoint pos = this.Frame.Location;
			var parentSize = ParentView.Frame.Size;
			pos.X = (parentSize.Width - DialogWidth)/2;
			switch (DialogPosition) {
			case DialogPosition.Bottom:
				pos.Y = parentSize.Height - DialogHeight - VERTICAL_MARGIN;
				break;

			case DialogPosition.Center:
				// Not implemented
				break;

			case DialogPosition.Top:
				// Not implemented
				break;
			}
			this.Frame = new CGRect(pos.X, pos.Y, DialogWidth, DialogHeight);
			_bgView.Frame = new CGRect (0, 0, this.Frame.Width, this.Frame.Height);
		}

		/// <summary>
		/// Animation of showing dialog
		/// </summary>
		private void Animate(){
			this.Fade (true, 0.0f, (float)this.Alpha, 1);
		}

		/// <summary>
		/// Update content view (content & size) and dialog height also
		/// </summary>
		/// <param name="content">Content.</param>
		private void UpdateContentView (string content){
			_contentView.Text = content;
			_contentView.ResizeHeigthWithText ();
			DialogHeight = (float)_contentView.Frame.Height + 2 * VERTICAL_PADDING;
		}

		#region IToast implementation

		public void Show ()
		{
			this.Animate ();
			ParentView.AddSubview (this);
		}

		public void Hide ()
		{
			_owner.BeginInvokeOnMainThread (this.RemoveFromSuperview);
		}

		private string _content;
		public string Content {
			get {
				return _content;
			}
			set {
				if (_content != value) {
					_content = value;
					string c = value ?? string.Empty;
					UpdateContentView (c);
				}
			}
		}

		public double Length { get; set; }

		#endregion
	}

}

