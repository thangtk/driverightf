﻿using System;
using UIKit;
using CoreGraphics;
using Unicorn.Core.Enums;
using Unicorn.Core.iOS;

[assembly: Unicorn.Core.Dependency.Dependency (typeof (Unicorn.Core.UI.iOSDialogContent))]
namespace Unicorn.Core.UI
{
	public class iOSDialogContent : UIView, IDialogContent
	{
		protected readonly int MAX_CONTENT_LINES = 300;
		protected readonly nfloat MIN_CONTENT_HEIGHT = 60f;
		protected readonly nfloat CONTENT_PADDING_TOP = 12f;
		protected readonly nfloat CONTENT_PADDING_BOTTOM = 12f;
		protected readonly nfloat CONTENT_PADDING_LEFT = 10f;
		protected readonly nfloat CONTENT_PADDING_RIGHT = 10f;
		protected readonly nfloat ICON_SIZE = 36f;
		protected readonly nfloat ICON_PADDING_LEFT = 12f;
		protected const float OVERLAY = 0.3f;
		private const float WIDTH_MULTIPLIER = 0.9f;

		protected UILabel _lblContent;
		protected UIImageView _dialogIcon;
		protected UIView _iconView;

		public nfloat ContentHeight {
			get{ 
				return this.Frame.Height;
			}
			set{ 
				if (this.Frame.Height != value) {
					var temp = Frame;
					temp.Size = new CGSize (temp.Size.Width, value);
					this.Frame = temp;
				}
			}
		}

		public nfloat IconViewWidth {
			get{ 
				return _iconView.Frame.Width;
			}
			set { 
				if (_iconView.Frame.Width != value) {
					var temp = _iconView.Frame;
					_iconView.Frame = new CGRect (temp.Location, new CGSize (value, temp.Height));
					// Update Content Height
					UpdateLayout ();
				}
			}
		}

		public nfloat ContentWidth{
			get { 
				return UIScreen.MainScreen.Bounds.Width*WIDTH_MULTIPLIER;
			}
		}

		public iOSDialogContent(){
			Initialize ();
		}

		private void Initialize()
		{
			// Create icon area
			_dialogIcon = new UIImageView ();
			_dialogIcon.Frame = new CGRect(ICON_PADDING_LEFT, 5, ICON_SIZE, ICON_SIZE);
			_dialogIcon.Image = GetImage (Icon);
			//
			_iconView = new UIView ();
			_iconView.BackgroundColor = UIColor.Clear;
			_iconView.AddSubview (_dialogIcon);
			//
			_lblContent = new UILabel();
			_lblContent.TextAlignment = UITextAlignment.Center;

			_lblContent.TextColor = UIColor.Black;
			_lblContent.LineBreakMode = UILineBreakMode.WordWrap;
			_lblContent.Lines = MAX_CONTENT_LINES;
			_lblContent.BackgroundColor = UIColor.Clear;
			_lblContent.Font = UIFont.SystemFontOfSize(15f);
			_lblContent.Frame = new CGRect (0, 0, 0, 0);
			// Defalt height is MIN_CONTENT_HEIGHT
			Frame = new CGRect (0, 0, ContentWidth, MIN_CONTENT_HEIGHT);
			this.AddSubviews (_iconView, _lblContent);
//			_lblContent.SizeToFit ();
		}

		private UIImage GetImage(DialogIcon type){
			string iconFile = "icon_info.png";
			switch (type) {
			case DialogIcon.Error:
				iconFile = "icon_error.png";
				break;
			case DialogIcon.Info:
				iconFile = "icon_info.png";
				break;
			case DialogIcon.Question:
				iconFile = "icon_question.png";
				break;
			case DialogIcon.Warning:
				iconFile = "icon_warning.png";
				break;
			case DialogIcon.None:
				iconFile = string.Empty;
				break;
			}
			//
			if(string.IsNullOrEmpty(iconFile)){
				return null;
			}
			//
			return UIImage.FromBundle (iconFile);
		}

		private void UpdateLayout(){
			CGRect temp;
			temp = _lblContent.Frame;
			temp.Size = new CGSize (ContentWidth - IconViewWidth - CONTENT_PADDING_LEFT - CONTENT_PADDING_RIGHT, ContentHeight - CONTENT_PADDING_TOP - CONTENT_PADDING_BOTTOM);
			temp.Location = new CGPoint (IconViewWidth + CONTENT_PADDING_LEFT, CONTENT_PADDING_TOP);
			_lblContent.Frame = temp;
			_lblContent.ResizeHeigthWithText ();
			//
			var contentHeight = _lblContent.Frame.Height + CONTENT_PADDING_TOP + CONTENT_PADDING_BOTTOM;
			if (contentHeight < MIN_CONTENT_HEIGHT) {
				contentHeight = MIN_CONTENT_HEIGHT;
			}
			//
			ContentHeight = contentHeight;
		}

		#region Implements

		public string Content {
			get {
				return _lblContent.Text;
			}
			set {
				if (_lblContent.Text != value) {
					_lblContent.Text = value;
					UpdateLayout ();
				}
			}
		}

		private DialogIcon _icon = DialogIcon.None;

		public DialogIcon Icon {
			get{ return _icon; }
			set {
				if (_icon != value) {
					_icon = value;
					if (_icon == DialogIcon.None) {
						IconViewWidth = 0;
					} else {
						_dialogIcon.Image = GetImage (_icon);
						IconViewWidth = ICON_SIZE + ICON_PADDING_LEFT;
					}
				}
			}
		}

		#endregion
	}
}

