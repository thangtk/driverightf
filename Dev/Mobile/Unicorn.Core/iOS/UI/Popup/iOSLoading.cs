using System;
using UIKit;
using System.Diagnostics;
using Foundation;

[assembly: Unicorn.Core.Dependency.Dependency (typeof (Unicorn.Core.UI.iOSLoading))]
namespace Unicorn.Core.UI
{
	public partial class iOSLoading : UIView, ILoading
	{
		private NSObject _owner = new NSObject ();
		public string Title { get; set; }

		public string Message {
			get {
				return _lblMessage.Text;
			}
			set {
				_lblMessage.Text = value;
			}
		}

		public void Show()
		{
			_owner.BeginInvokeOnMainThread (() => {
				this.ScaleAnimation();
				this.Rotate();
				_parentView.AddSubview(this);
			});
		}

		public void Hide()
		{
			_owner.BeginInvokeOnMainThread (this.RemoveFromSuperview);
		}
	}
}

