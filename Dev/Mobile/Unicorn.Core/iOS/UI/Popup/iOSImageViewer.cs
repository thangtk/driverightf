﻿using System;
using UIKit;
using CoreGraphics;
using Unicorn.Core.iOS;

[assembly: Unicorn.Core.Dependency.Dependency (typeof (Unicorn.Core.UI.iOSImageViewer))]
namespace Unicorn.Core.UI
{
	public class iOSImageViewer : UIView
	{
		private UIImageView _imageView;
		private UIView _layerView;
		private bool _isShowed = false;

		private readonly nfloat ImageWidthRatio = 0.8f;
		private readonly nfloat MaxImageHeightRatio = 0.8f;

		public UIImage Image {
			get;
			set;
		}

		public double AnimationDuration {
			get;
			set;
		}

		public iOSImageViewer (UIImage image, CGRect frame)
		{
			Frame = frame;
		
			InitParams ();

			_layerView = new UIView (frame);
			_layerView.BackgroundColor = UIColor.Black.ColorWithAlpha (0.5f);
			AddSubview (_layerView);

			//configure fit size to zoom
			nfloat imageHeight;
			nfloat imageWidth;

			nfloat standardRatio = frame.Height / frame.Width;
			nfloat imageRatio = image.Size.Height / image.Size.Width;

			if (imageRatio > standardRatio) {
				imageHeight = MaxImageHeightRatio * frame.Height;
				imageWidth = imageHeight / imageRatio;
			} else {
				imageWidth = ImageWidthRatio * frame.Width;
				imageHeight = imageWidth * imageRatio;
			}

			//set image
			Image = image;
			_imageView = new UIImageView (new CGRect(0, 0, imageWidth, imageHeight));
			_imageView.Center = Center;
			_imageView.Image = image;
			AddSubview (_imageView);
		}

		public override void MovedToSuperview ()
		{
			base.MovedToSuperview ();
			Show();
		}

		private void InitParams()
		{
			AnimationDuration = 0.2;
		}

		public override void TouchesEnded (Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			var touch = (UITouch)touches.AnyObject;

			var point = touch.LocationInView (_imageView);
			//if out of image base
			if (!(point.X < _imageView.Frame.Width && point.Y < _imageView.Frame.Height && point.X > 0 && point.Y > 0)) {
				Hide ();
			}
		}

		public void Show ()
		{
			if (_isShowed)
				return;
			//if not have superview
			if (Superview == null) {
				UIApplication.SharedApplication.KeyWindow.Subviews [0].AddSubview (this);
				return;
			}
			//Animation
			_imageView.Scale(true, AnimationDuration, () => _isShowed = true);
		}

		public void Hide()
		{
			RemoveFromSuperview ();
			_isShowed = false;
		}
	}
}

