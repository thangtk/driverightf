﻿using System;
using UIKit;
using CoreGraphics;
using Unicorn.Core.Enums;
using Unicorn.Core.iOS;
using System.Collections.Generic;
using Foundation;
using System.Drawing;
using Unicorn.Core.Translation;

[assembly: Unicorn.Core.Dependency.Dependency(typeof(Unicorn.Core.UI.iOSDynamicContentDialog))]
namespace Unicorn.Core.UI
{
	public class iOSDynamicContentDialog : ViewWithKeyboard, IDynamicContentDialog
	{
		#region implemented abstract members of ViewWithKeyboard

		//		protected override void KeyBoardUpNotification (NSNotification notification)
		//		{
		//			_isShown = true;
		//			CGRect keyboardSize = UIKeyboard.BoundsFromNotification (notification);
		//		}
		//
		//		protected override void KeyBoardDownNotification (NSNotification notification)
		//		{
		//			throw new NotImplementedException ();
		//		}
		//
		public ITranslator Translator {get {return DependencyService.Get<ITranslator>();}}

		#endregion

		protected readonly nfloat HEADER_HEIGHT = 40f;
//36f;
		protected readonly nfloat FOOTER_HEIGHT = 40f;
		protected readonly nfloat MIN_CONTENT_HEIGHT = 60f;
		protected const float OVERLAY = 0.3f;
		private const float WIDTH_MULTIPLIER = 0.9f;
		private UIColor dialogColor = UIColor.FromRGB(255, 255, 255);
		protected UIView _parentView, _dialogView, _contentView;

		public object ContentView
		{
			get
			{ 
				return _contentView;
			}
			set
			{ 
				if(value != null)
				{
					try
					{
						var x = value as UIView;
						x.Frame = new CGRect (0, HeaderHeight, x.Frame.Width, x.Frame.Height);
						_contentView = x;

					}
					catch
					{
						throw new Exception ("Content View must be a UIView");
					}
				}
			}
		}

		public UIView ParentView
		{
			get
			{ 
				if(_parentView == null)
				{
//					_parentView = UIApplication.SharedApplication.KeyWindow.Subviews [0];
					_parentView = UIApplication.SharedApplication.KeyWindow.RootViewController.View;
				}
				return _parentView;
			}
		}

		public nfloat DialogHeight
		{
			get
			{ 
				return HeaderHeight + FOOTER_HEIGHT + (_contentView == null ? MIN_CONTENT_HEIGHT : _contentView.Frame.Height);
			}
		}

		public nfloat HeaderHeight
		{
			get
			{ 
				return ShowTitle ? HEADER_HEIGHT : 0f;
			}
		}

		public nfloat DialogWidth
		{
			get
			{ 
				return _contentView == null ? Frame.Width * WIDTH_MULTIPLIER : _contentView.Frame.Width; // TODO: Check null
			}
		}

		public iOSDynamicContentDialog() : base()
		{
			Frame = ParentView.Frame;
		}

		public void CreateViews()
		{
			BackgroundColor = UIColor.Clear;
			// Add Overlay view
			UIView overlayView = new UIView (this.Frame);
			overlayView.BackgroundColor = UIColor.Black;
			overlayView.Layer.Opacity = OVERLAY;
			if(TouchOutsideToClose)
			{
				overlayView.AddGestureRecognizer(new UITapGestureRecognizer (this.Hide));
			}
			this.AddSubview(overlayView);
			//
			_dialogView = new UIView ();
			_dialogView.BackgroundColor = dialogColor;
			_dialogView.Layer.MasksToBounds = true;
			_dialogView.Layer.CornerRadius = 3f;
			_dialogView.Frame = new CGRect ((Frame.Width - DialogWidth) / 2, (Frame.Height - DialogHeight) / 2, DialogWidth, DialogHeight);

//			var blur = UIBlurEffect.FromStyle (UIBlurEffectStyle.Light);
//			var blurView = new UIVisualEffectView (blur) {
//				Frame = new CGRect (0, 0, _dialogView.Frame.Width, _dialogView.Frame.Height)
//			};
//
//			_dialogView.Add(blurView);
			//
			var header = GetHeader();
			if(header != null)
			{
				_dialogView.AddSubview(header);
			}
			if(_contentView != null)
			{
				_dialogView.AddSubview(_contentView);
			}
			var footer = GetFooter(DialogButtons);
			_dialogView.AddSubview(footer);
			//
			this.AddSubview(_dialogView);
			//
			_bottomSpace = UIScreen.MainScreen.Bounds.Height - _dialogView.Frame.Height - _dialogView.Frame.Y;
			KeyboardExtension.AddHideButtonToKeyboard(_contentView);
		}

		private UIView GetHeader()
		{
			if(!ShowTitle)
			{
				return null;
			}
			//
			UIView headerView = new UIView ();
			UILabel lblHeader = new UILabel ();
			lblHeader.TextAlignment = UITextAlignment.Center;
			lblHeader.TextColor = UIColor.Black;
			lblHeader.Font = UIFont.BoldSystemFontOfSize(18f);
			lblHeader.BackgroundColor = UIColor.Clear;
			lblHeader.Text = Title ?? string.Empty;
			lblHeader.Frame = new CGRect (0, 10, DialogWidth, HEADER_HEIGHT);
			//
			headerView.Frame = new CGRect (0, 0, DialogWidth, HEADER_HEIGHT);
			headerView.BackgroundColor = UIColor.Clear;
			headerView.AddSubview(lblHeader);
//			headerView.BackgroundColor = UIColor.Blue;
			//
			return headerView;
		}

		private UIView GetFooter(List<DialogButton> buttons)
		{
			UIView footerView = new UIView ();
			footerView.Frame = new CGRect (0, DialogHeight - FOOTER_HEIGHT, DialogWidth, FOOTER_HEIGHT);
			footerView.BackgroundColor = UIColor.FromRGB(239, 239, 239);

			//
			foreach (var v in footerView.Subviews)
			{
				v.RemoveFromSuperview();
			}
			//
			if(buttons == null || buttons.Count == 0)
			{
				buttons = new List<DialogButton> () { 
					new DialogButton () {
						Text = Translator.Translate("BUTTON_OK"),
						OnClicked = null
					}
				};
			}

			// Caculate size
			int count = buttons.Count;
			nfloat buttonWidth = (footerView.Frame.Width - count + 1) / count;
			nfloat buttonHeight = footerView.Frame.Height - 1;

			for(int i = 0; i < buttons.Count; i++)
			{
				var p = buttons [i];
				UIButton btn = new UIButton (UIButtonType.System);
				btn.BackgroundColor = dialogColor;
				btn.SetTitle(p.Text, UIControlState.Normal);
				btn.SetTitleColor(UIColor.FromRGB(11, 96, 255), UIControlState.Normal);
				btn.Font = UIFont.SystemFontOfSize(16f);
				btn.TouchUpInside += (sender, e) =>
				{
					if(p.OnClicked != null)
					{
						p.OnClicked.Invoke();
					}
					if(p.CloseAfterClick)
					{
						this.Hide();
					}
				};
				if(p.IsBold)
				{
					btn.Font = UIFont.BoldSystemFontOfSize(17);
				}
				btn.Frame = new CGRect (i * buttonWidth + i, 1, buttonWidth, buttonHeight);

				footerView.AddSubview(btn);
			}
			//
			return footerView;
		}

		#region Implements

		private readonly NSObject _owner = new NSObject ();

		public string Title
		{
			get;
			set;
		}

		public void Show()
		{
			CreateViews();
			//
			_dialogView.Scale(true); // Animation
			ParentView.AddSubview(this);
		}

		public bool ShowTitle
		{
			get;
			set;
		}

		public void Hide()
		{
			_owner.BeginInvokeOnMainThread(this.RemoveFromSuperview);
		}

		public List<DialogButton> DialogButtons
		{
			get;
			set;
		}

		public bool TouchOutsideToClose
		{
			get;
			set;
		}

		#endregion
	}
}

