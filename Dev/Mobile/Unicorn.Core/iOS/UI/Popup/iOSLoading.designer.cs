using System;
using UIKit;
using System.Drawing;
using CoreAnimation;
using Foundation;
using CoreGraphics;


namespace Unicorn.Core.UI
{
	public partial class iOSLoading
	{
		private UIImage _loading;
		private UIImageView _loadingView;
		private UILabel _lblMessage;
		private UIView _containterView;
		private UIView _parentView;

		public iOSLoading () : base ()
		{
			_parentView = UIApplication.SharedApplication.KeyWindow.Subviews [0];
			this.Frame = _parentView.Frame;
			//
			var overlayView = new UIView (){
				Frame = new CGRect(0,0, this.Frame.Width, this.Frame.Height),
				BackgroundColor = UIColor.Black,
				};
			overlayView.Layer.Opacity = 0.5f;
			this.Add (overlayView);
			//
			_containterView = new UIView (new CGRect ((Frame.Width - 90) / 2, (Frame.Height - 90) / 2, 90, 90));
			_containterView.BackgroundColor = UIColor.Black;
			_containterView.Layer.Opacity = 0.2f;
			_containterView.Layer.CornerRadius = 45f;

			_loading = UIImage.FromFile ("loading_new.png");
			_lblMessage = new UILabel (new CGRect (60, _containterView.Frame.Height / 2 - 15, 150, 30));
			_lblMessage.Text = "LOADING";
			_lblMessage.BackgroundColor = UIColor.Clear;
			//            _loadingTxt.Font = AppTheme.FontBold;
			_lblMessage.TextColor = UIColor.White;

			_loadingView = new UIImageView (_loading);
			_loadingView.Frame = new CGRect (_containterView.Frame.X + 20, _containterView.Frame.Y + 20, 50, 50);

			this.Add (_containterView);
			this.Add (_loadingView);
		}

		public void Rotate ()
		{
			_loadingView.Layer.RemoveAllAnimations ();

			CABasicAnimation rotationAnimation = CABasicAnimation.FromKeyPath ("transform.rotation");           
			rotationAnimation.To = NSNumber.FromDouble (Math.PI * 2);
			rotationAnimation.RepeatCount = int.MaxValue;
			rotationAnimation.Duration = 1.2;
			_loadingView.Layer.AddAnimation (rotationAnimation, "rotationAnimation");
		}

		public void ScaleAnimation ()
		{
			CABasicAnimation scaleAnimation = CABasicAnimation.FromKeyPath ("transform.scale");
			scaleAnimation.Duration = 0.8;
			scaleAnimation.RepeatCount = int.MaxValue;
			scaleAnimation.AutoReverses = true;
			scaleAnimation.From = NSNumber.FromFloat (0.9f);
			scaleAnimation.To = NSNumber.FromFloat (0.6f);

			_containterView.Layer.AddAnimation (scaleAnimation, "transform.scale");
		}
	}
}

