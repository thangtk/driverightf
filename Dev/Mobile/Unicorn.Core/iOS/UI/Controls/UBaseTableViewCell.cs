﻿using System;
using ReactiveUI;
using System.Drawing;
using Foundation;
using CoreGraphics;
using UIKit;

namespace Unicorn.Core.UI
{
	public class UBaseTableViewCell<T> : UBaseTableViewCell, IViewFor<T>
		where T: ReactiveObject
	{
		protected T _viewModel; 
		public T ViewModel
		{
			get { return _viewModel; }
			set { this.RaiseAndSetIfChanged(ref _viewModel, value); }
		}

		object IViewFor.ViewModel
		{
			get { return _viewModel; }
			set { ViewModel = (T)value; }
		}

		protected UBaseTableViewCell (IntPtr handle):base(handle)
		{

		}

		protected UBaseTableViewCell ():base()
		{

		}

		protected UBaseTableViewCell (NSCoder c):base(c)
		{

		}

		protected UBaseTableViewCell (CGRect size):base(size)
		{

		}

		protected UBaseTableViewCell (NSObjectFlag f):base(f)
		{

		}

		protected UBaseTableViewCell (UITableViewCellStyle style, NSString reuseIdentifier) : base (style, reuseIdentifier){

		}
	}
		
	public class UBaseTableViewCell : ReactiveTableViewCell
	{
		protected UBaseTableViewCell (IntPtr handle):base(handle)
		{

		}

		protected UBaseTableViewCell ():base()
		{

		}

		protected UBaseTableViewCell (NSCoder c):base(c)
		{

		}

		protected UBaseTableViewCell (CGRect size):base(size)
		{

		}

		protected UBaseTableViewCell (NSObjectFlag f):base(f)
		{

		}

		protected UBaseTableViewCell (UITableViewCellStyle style, NSString reuseIdentifier) : base (style, reuseIdentifier){
			
		}
	}
}

