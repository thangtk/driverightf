﻿using System;
using CoreGraphics;
using UIKit;
using CoreAnimation;
using System.Linq;

namespace Unicorn.iOS.Controls
{
	public class ViewPager3DTransition : ViewPager
	{
		public ViewPager3DTransition (CGRect frame) : base (frame)
		{
		}

		public ViewPager3DTransition (IntPtr handler) : base (handler)
		{
		}

		protected override UIView GetPageView (int index)
		{
			UILabel label = new UILabel ();
			label.TextAlignment = UITextAlignment.Center;
			label.Text = "Page " + (index + 1).ToString ();
			UIColor c = UIColor.Black;
			switch (index) {
			case 0:
				c = UIColor.Green;
				break;
			case 1:
				c = UIColor.Brown;
				break;
			case 2:
				c = UIColor.Blue;
				break;
			case 3:
				c = UIColor.Yellow;
				break;
			case 4:
				c = UIColor.Red;
				break;
			}
			label.BackgroundColor = c;
			//
			return label;
		}

		protected override void ScrollEvent (object sender, EventArgs e)
		{
			var deltaX = ScrollView.ContentOffset.X - ContentWidth * CurrentPage;
			// No change
			if (deltaX == 0)
				return;
			int d = deltaX > 0 ? 1 : -1;
			// Check page change
			if (Math.Abs (deltaX) >= ContentWidth) {
				CurrentPage += d;
				//				_lastOffsetX = ContentWidth+CurrentPage;
				if (CurrentPage == 0 || CurrentPage == PageCount - 1)
					return;
//				System.Diagnostics.Debug.WriteLine (string.Format (">>>>> Page >>>> {0}     {1}", d, CurrentPage));
				deltaX -= d * ContentWidth;
			}
			// No change
			if (deltaX == 0)
				return;
			//
			//----------------------------------
			// Get views
			var viewOut = ScrollView.Subviews [CurrentPage];
			var viewIn = GetCommingView(d);
			// Transition
			// Config params
			const float MAX_ANGLE = (float)(Math.PI * 0.5f);
			const float perspective = 1 / 700f;
			var angle = MAX_ANGLE * deltaX / ContentWidth;
			//
			// Set transition for view-out
			if (viewOut != null) {
				CATransform3D transOut = CATransform3D.Identity;
				var angleOut = angle;
				var zOut = ContentWidth * Math.Sin (Math.Abs (angleOut)) / 2;
				var xOut = zOut * Math.Tan (Math.Abs (angleOut / 2)) * d;
				//
				transOut.m34 = perspective;
				transOut = transOut.Translate ((nfloat)xOut, 0, (nfloat)zOut);
				transOut = transOut.Rotate ((nfloat)angleOut, 0, 1f, 0);
				// Set transforms
				viewOut.Layer.Transform = transOut;
			}
			// Set transition for view-in
			if (viewIn != null) {
				CATransform3D transIn = CATransform3D.Identity;
				var angleIn = (Math.Abs (angle) - MAX_ANGLE) * d;
				var zIn = ContentWidth * Math.Sin (Math.Abs (angleIn)) / 2;
				var xIn = -zIn * Math.Tan (Math.Abs (angleIn / 2)) * d;
				transIn.m34 = perspective;
				//
				transIn = transIn.Translate ((nfloat)(xIn), 0, (nfloat)(zIn));
				transIn = transIn.Rotate ((nfloat)(angleIn), 0, 1f, 0);
				// Set transforms
				viewIn.Layer.Transform = transIn;
			}
			//
//			System.Diagnostics.Debug.WriteLine (string.Format (">>>>>>>>>> scrolling >>> {0}      {1}      {2}      {3}     {4}     {5} ", deltaX, xOut, xIn, d, angle * 180 / Math.PI, CurrentPage));
		}

		/// <summary>
		/// Get comming view
		/// </summary>
		/// <param name="direction"next = 1 & previous = -1</param>
		private UIView GetCommingView(int direction){
			UIView viewIn = null;
			int indexIn = (int)(CurrentPage + direction);
			if (indexIn > PageCount - 1 || indexIn < 0) {
				if (LoopScrollEnabled) {
					if (indexIn == PageCount) {
						indexIn = 0;
					} else if (indexIn < 0) {
						indexIn = PageCount - 1;
					}
					// Not implemented yet
					throw new NotImplementedException ("Please wait for next version (KT)");
				} else {
					viewIn = null;
				}
			} else {
				viewIn = ScrollView.Subviews [indexIn];
			}
			return viewIn;
		}
	}

	/// <summary>
	/// STATE: in progress
	/// </summary>
	public class ViewPager3DWithLayer  : ViewPager
	{
		public ViewPager3DWithLayer (CGRect frame) : base (frame)
		{
		}

		public ViewPager3DWithLayer (IntPtr handler) : base (handler)
		{
		}

		protected override UIView GetPageView (int index)
		{
			UILabel label = new UILabel ();
			label.TextAlignment = UITextAlignment.Center;
			label.Text = "Page " + (index + 1).ToString ();
			UIColor c = UIColor.Black;
			switch (index) {
			case 0:
				c = UIColor.Green;
				break;
			case 1:
				c = UIColor.Brown;
				break;
			case 2:
				c = UIColor.Blue;
				break;
			case 3:
				c = UIColor.Yellow;
				break;
			case 4:
				c = UIColor.Red;
				break;
			}
			label.BackgroundColor = c;
			//
			return label;
		}
	
		private UIView _transformView;
		private CATransformLayer _transformLayer;

		protected override void InitView ()
		{
			base.InitView ();
			// Add transform view
			_transformView = new UIView ();
			_transformView.Frame = new CGRect (new CGPoint(0,160), this.PageSize);
			this.AddSubview (_transformView);
		}

		protected override void Recreate (int count)
		{
			// Fake infinite horizontal scrollview
			ScrollView.ContentSize = new CGSize(1000000f, 1f);
			this.BringSubviewToFront (ScrollView);
			ScrollView.BackgroundColor = UIColor.Clear;
			ScrollView.PagingEnabled = false;
			// TODO: uncomment
//			ScrollView.ContentOffset = new CGPoint (ScrollView.ContentSize.Width / 2, 0f);
			// Set background if need
			//
			// Create layer instead of view
			_transformLayer = new CATransformLayer ();
			_transformLayer.Frame = new CGRect (CGPoint.Empty, PageSize);
			_transformLayer.AnchorPointZ = -_transformLayer.Frame.Width/2;
			//
			for (int i = 0; i < count; i++) {
				_transformLayer.AddSublayer (GetLayer (i));
			}
			// TODO: remove other layers first
			_transformView.Layer.AddSublayer (_transformLayer);

			System.Diagnostics.Debug.WriteLine(_transformView.Layer.AnchorPoint.ToString() + " >>>> " + _transformView.Layer.AnchorPointZ.ToString());
		}

		private CALayer GetLayer(int index){
			// NOTE: compatible with perspective is negative
			CALayer layer = new CALayer();
			layer.Frame = new CGRect (CGPoint.Empty, PageSize);
			var layerWidth = layer.Frame.Width;
			//
			int i = index % 4;
			nfloat rootAngle = (nfloat)(Math.PI / 2);
			nfloat angle = 0;
			nfloat x = 0;
			nfloat z = 0;
			switch (i) {
			case 0:
				angle = 0;
				x = 0;
				z = 0;
				break;
			case 1:
				angle = rootAngle;
				x = layerWidth/2;
				z = -layerWidth / 2;
				break;
			case 2:
				angle = 2*rootAngle;
				x = 0;
				z = -4*layerWidth / 4;
				break;
			case 3:
				angle = -rootAngle;
				x = -layerWidth/2;
				z = -layerWidth / 2;
				break;
			}
			//
			var v = GetPageView(index);
			v.Frame = layer.Frame;
			layer.AddSublayer (v.Layer);
			//
			CATransform3D transform = CATransform3D.Identity;
			transform = transform.Translate (x, 0, z);
			transform = transform.Rotate (angle, 0, 1f, 0);
			layer.Transform = transform;
			//
			return layer;
		}

		protected override void ScrollEvent (object sender, EventArgs e)
		{
			// TODO: rotate transform layer here
			System.Diagnostics.Debug.WriteLine(">>>> " + ScrollView.ContentOffset.X);
			var angle = Math.PI*0.5f* ScrollView.ContentOffset.X/ContentWidth;
			var transform = CATransform3D.Identity;
			transform.m34 = -1 / 500f;
			transform = transform.Scale (0.75f);
			transform = transform.Rotate ((nfloat)(angle), 0, 1f, 0);
			_transformLayer.Transform = transform;
		}
	}
}

