﻿using System;
using UIKit;
using ReactiveUI;
using Foundation;
using System.Drawing;
using CoreGraphics;
using Unicorn.Core.Translation;
using Unicorn.Core.Navigator;

namespace Unicorn.Core.UI
{
	public class UBaseView<T>: UBaseView , IViewFor<T>
		where T: ReactiveObject
	{
		public ITranslator Translator {get {return DependencyService.Get<ITranslator>();}}

		protected T _viewModel; 
		public T ViewModel
		{
			get { return _viewModel; }
			set { this.RaiseAndSetIfChanged(ref _viewModel, value); }
		}

		object IViewFor.ViewModel
		{
			get { return _viewModel; }
			set { ViewModel = (T)value; }
		}

		protected UBaseView (IntPtr handle):base(handle)
		{

		}

		protected UBaseView ():base()
		{

		}

		protected UBaseView (NSCoder c):base(c)
		{

		}

		protected UBaseView (RectangleF size):base(size)
		{

		}

		protected UBaseView (CGRect size):base(size)
		{

		}

		protected UBaseView (NSObjectFlag f):base(f)
		{

		}
	}
	public class UBaseView:ReactiveView
	{
		protected UBaseView (IntPtr handle):base(handle)
		{

		}

		protected UBaseView ():base()
		{

		}

		protected UBaseView (NSCoder c):base(c)
		{

		}

		protected UBaseView (RectangleF size):base(size)
		{

		}

		protected UBaseView (CGRect size):base(size)
		{

		}

		protected UBaseView (NSObjectFlag f):base(f)
		{

		}
	}
}

