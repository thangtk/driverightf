﻿using System;
using UIKit;
using Foundation;
using ObjCRuntime;
using System.Drawing;
using CoreGraphics;
using Unicorn.Core.iOS;
using Unicorn.Core.Enums;
using Unicorn.Core.UI;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using ReactiveUI;

namespace DriveRightF.Core.UI
{
	[Register("UTextBox")]
	public partial class UTextBox : UBaseView<UTextBoxViewModel>
	{
		private UIView _rootView;
		private string _error = string.Empty;
		private string _title = string.Empty;
		private string _text = string.Empty;
		private bool _isEnable = true;
		private bool _isValid = true;
		private bool _isSecurity = false;
		private KeyboardType _typeKeyboard;

		#region IDriveRightTextBox implementation

		public event EventHandler<string> ValueChanged ;
			
		public KeyboardType KeyboardType {
			get {
				return _typeKeyboard;
			}
			set {
				_typeKeyboard = value;
				txtInput.AddHideButtonToKeyboard (_typeKeyboard);
			}
		}

		public string Error {
			get {
				return _error;
			}
			set {
				_error = value;
				SetError ();
			}
		}

		public string Title {
			get {
				return _title;
			}
			set {
				_title = value;
				SetTitle ();
				//Waring run on mainthread!

			}
		}

		public bool IsValid {
			get {
				return _isValid;
			}
			set {
				_isValid = value;
				InvokeOnMainThread (() => {
					lblError.Hidden = _isValid;
					UpdateFrame();
					ResizeView();
				});
			}
		}

		public string Text {
			get {
				return _text;
			}
			set {
				_text = value;
				InvokeOnMainThread (() => {
					txtInput.Text = _text;
				});


			}
		}


		public bool IsEnable {
			get {
				return _isEnable;
			}
			set {
				_isEnable = value;
				txtInput.Enabled = _isEnable;
			}
		}

		public bool IsSecurity {
			get {
				return _isSecurity;
			}
			set {
				_isSecurity = value;
				InvokeOnMainThread (delegate {
					txtInput.SecureTextEntry = value;
				});
			}
		}

		#endregion

		public UTextBox(IntPtr handle) : base(handle)
		{
			InitializeView();
		}

		public UTextBox()
		{
			InitializeView();
		}

		public override CoreGraphics.CGRect Frame {
			get {
				return base.Frame;
			}
			set {
				base.Frame = value;
				UpdateFrame ();
				ResizeView ();
			}
		}

		private void InitializeView()
		{
			LoadNib ();
			InitBindings ();
			InitCustomization();
		}

		private void InitBindings()
		{
			this.WhenAnyValue (v => v.ViewModel)
				.Where (vModel => vModel != null)
				.ObserveOn(RxApp.TaskpoolScheduler)
				.Subscribe (vM => HandleBindings ());
		}

		private void HandleBindings()
		{

			this.Bind (ViewModel, vm => vm.Text, v => v.Text);

			ViewModel
				.WhenAnyValue (x => x.Error)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (error => {
					Error = error;
				});
			ViewModel
				.WhenAnyValue (x => x.Title)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (title => {
					Title = title;
			});

		}

		public virtual void InitCustomization()
		{
			//TODO: set background, font...

			txtInput.LeftView = new UIView (new CGRect (0, 0, 10, 10));
			txtInput.LeftViewMode = UITextFieldViewMode.Always;
			txtInput.Font = FontHub.HelveticaThin13;
		
			lbTitle.Lines = 0;
			lbTitle.LineBreakMode = UILineBreakMode.WordWrap;
			lbTitle.Font = FontHub.HelveticaThin13;

			lblError.Font = FontHub.HelveticaThin13;

			Error = string.Empty;
			Title = string.Empty;
			txtInput.Text = string.Empty;

			KeyboardType = KeyboardType.Default;

			txtInput.EditingChanged += (object sender, EventArgs e) => 
			{
				Text = txtInput.Text;
				if (ValueChanged != null)
				{
					ValueChanged (this, _text);
				}
			};
		}

		private void LoadNib()
		{
			var nibObjects = NSBundle.MainBundle.LoadNib("UTextBox", this, null);
			_rootView = Runtime.GetNSObject((nibObjects.ValueAt(0))) as UIView;
			AddSubview(_rootView);
		}

		private void SetError()
		{
			InvokeOnMainThread (() => {
				lblError.Text = _error;
				lblError.Frame = new CGRect(lblError.Frame.Location, new CGSize(txtInput.Frame.Width - 10f, lblError.Frame.Height));
				lblError.SizeToFit();
				lblError.Hidden = string.IsNullOrEmpty(_error);

				UpdateFrame();
				ResizeView();
			});
		}

		private void SetTitle()
		{
			InvokeOnMainThread (() => {
				lbTitle.Text = Title;
				lbTitle.Frame = new CGRect (lbTitle.Frame.Location, new CGSize (txtInput.Frame.X - 10f, lbTitle.Frame.Height));
				lbTitle.SizeToFit ();
			});
		}

		private void UpdateFrame()
		{
			nfloat heightFrame = (lblError.Hidden ? 0f : lblError.Frame.Height) + lblError.Frame.Y + txtInput.Frame.Y + 5f;
			nfloat totalHeightTitle = lbTitle.Frame.Height + lbTitle.Frame.Y + txtInput.Frame.Y;
			heightFrame = (nfloat) Math.Max (heightFrame, totalHeightTitle);
			nfloat offset = heightFrame - Frame.Height;
			base.Frame = new CGRect(Frame.Location, new CGSize(Frame.Width, heightFrame));

			//relocation views that beneath
			if (Superview != null) {
				foreach (var view in Superview.Subviews) {
					if (view.Frame.Y > Frame.Y) {
						var viewFrame = view.Frame;
						viewFrame.Y += offset;
						view.Frame = viewFrame;
					}
				}
			}
		}

		private void ResizeView()
		{
			if (_rootView != null)
			{
				_rootView.Frame = new CGRect(PointF.Empty, Frame.Size);
			}
		}


	}
}

