﻿
using System;

using Foundation;
using UIKit;
using CoreGraphics;
using ObjCRuntime;
using Unicorn.Core.UI;
using System.Linq;

namespace Unicorn.iOS.Controls
{
	public partial class ViewPager : UBaseView
	{
		private int _pageCount;

		public int PageCount { 
			get { 
				return _pageCount;
			}
			set { 
				if (_pageCount != value) {
					_pageCount = value;
					Recreate (value);
					PageControl.Pages = value;
				}
			}
		}

		public bool AutoPlay { get; set; }
		// Implement later
		public bool LoopScrollEnabled { get; set; }
		// Implement later

		private int _currentPage;
		public int CurrentPage { get { return _currentPage;
			} set { 
				if(_currentPage != value && value >= 0 && value < PageCount){
					_currentPage = value;
					PageControl.CurrentPage = value;
				}
			} }

		protected CGSize PageSize {
			get { 
				return scrollView.Frame.Size;
			}
		}

		protected float ContentWidth {
			get { 
				return (float)this.scrollView.Frame.Width;
			}
		}

		protected float ContentHeight {
			get { 
				return (float)this.scrollView.Frame.Height;
			}
		}

		public ViewPager (CGRect frame) : base (frame)
		{
			Initialize ();
		}

		public ViewPager (IntPtr handler) : base (handler)
		{
			Initialize ();
		}

		protected UIScrollView ScrollView {
			get { 
				return scrollView;
			}
		}

		protected UIPageControl PageControl {
			get{ 
				return pageControl;
			}
		}

		private void Initialize ()
		{
			var arr = NSBundle.MainBundle.LoadNib ("ViewPager", this, null);
			var v = Runtime.GetNSObject (arr.ValueAt (0)) as UIView;
			v.Frame = new CGRect (CGPoint.Empty, Frame.Size);
			AddSubview (v);
			//
			this.SetNeedsLayout ();
			this.LayoutIfNeeded ();
			//
			InitView ();
			//
			InitData ();
		}

		/// <summary>
		/// This method is invoked after initializing view
		/// </summary>
		protected virtual void InitData ()
		{
			PageCount = 0;
		}

		protected virtual void InitView ()
		{
			pageControl.Enabled = false;
			scrollView.PagingEnabled = true;
			scrollView.ShowsHorizontalScrollIndicator = false;
			scrollView.ShowsVerticalScrollIndicator = false;
			scrollView.BackgroundColor = UIColor.LightGray; // UIColor.FromRGB(222,222,222);
			//
			scrollView.Scrolled += ScrollEvent;
			scrollView.DecelerationEnded += (sender, e) => {};
		}

		protected bool IsScrolling { get; set; }

		protected void ScrollToPage (int index)
		{
			var offset = scrollView.ContentOffset;
			offset.X = ContentWidth * index;
			scrollView.SetContentOffset (offset, true);
		}

		protected virtual UIView GetPageView (int index)
		{
			UILabel label = new UILabel ();
			label.TextAlignment = UITextAlignment.Center;
			label.Text = "Page " + (index + 1).ToString ();
			//
			return label;
		}

		protected virtual void Recreate (int count)
		{
			if (count < 0) {
				throw new Exception ("Page count is not valid");
			}
			scrollView.Subviews.ToList ().ForEach (x => x.RemoveFromSuperview ());
			for (int i = 0; i < count; i++) {
				var pageView = GetPageView (i);
				var frame = new CGRect (CGPoint.Empty, PageSize);
				frame.X = this.ContentWidth * i;
				pageView.Frame = frame;
				scrollView.AddSubview (pageView);
			}
			scrollView.ContentSize = new CGSize (count * ContentWidth, 1f); // Disable horizontal scroll
			CurrentPage = 0;
		}

		protected virtual void ScrollEvent (object sender, EventArgs e)
		{
			if (!LoopScrollEnabled) {
				// Disable scroll
				var offset = scrollView.ContentOffset;
				if (offset.X < 0 || offset.X > ContentWidth * (PageCount - 1)) {
					offset.X = CurrentPage * ContentWidth; // or bound page
					scrollView.SetContentOffset (offset, false);
				}
			}
			// Change page
			var deltaX = ScrollView.ContentOffset.X - ContentWidth * CurrentPage;
			if(Math.Abs(deltaX) >= ContentWidth){
				CurrentPage += (int)(deltaX/Math.Abs(deltaX));
			}
		}
	}
}

