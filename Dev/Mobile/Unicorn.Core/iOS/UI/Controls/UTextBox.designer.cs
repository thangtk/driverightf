// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.Core.UI
{
	partial class UTextBox
	{
		[Outlet]
		UIKit.UILabel lblError { get; set; }

		[Outlet]
		UIKit.UILabel lbTitle { get; set; }

		[Outlet]
		UIKit.UITextField txtInput { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (lbTitle != null) {
				lbTitle.Dispose ();
				lbTitle = null;
			}

			if (txtInput != null) {
				txtInput.Dispose ();
				txtInput = null;
			}

			if (lblError != null) {
				lblError.Dispose ();
				lblError = null;
			}
		}
	}
}
