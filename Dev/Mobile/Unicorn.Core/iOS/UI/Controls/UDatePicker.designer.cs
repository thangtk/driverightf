// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.Core
{
	partial class UDatePicker
	{
		[Outlet]
		UIKit.UILabel lblError { get; set; }

		[Outlet]
		UIKit.UILabel lblTitle { get; set; }

		[Outlet]
		UIKit.UITextField txtInput { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (txtInput != null) {
				txtInput.Dispose ();
				txtInput = null;
			}

			if (lblError != null) {
				lblError.Dispose ();
				lblError = null;
			}

			if (lblTitle != null) {
				lblTitle.Dispose ();
				lblTitle = null;
			}
		}
	}
}
