﻿using System;
using ReactiveUI;
using Foundation;
using Unicorn.Core.Translation;
using Unicorn.Core.Navigator;

namespace Unicorn.Core.UI
{
	public abstract class UBaseViewController<T> : UBaseViewController,IViewFor<T>
		where T: ReactiveObject
	{
		protected UBaseViewController (string nibNameOrNull, NSBundle nibBundleOrNull)
			:base(nibNameOrNull,nibBundleOrNull)
		{

		}

		protected UBaseViewController (IntPtr handle)
			:base(handle)
		{

		}

		protected UBaseViewController (NSObjectFlag f)
			:base(f)
		{

		}

		protected UBaseViewController ():base()
		{
		}

		protected UBaseViewController (NSCoder c):base(c)
		{

		}
		protected T _viewModel; 
		public T ViewModel
		{
			get { return _viewModel; }
			set { this.RaiseAndSetIfChanged(ref _viewModel, value); }
		}

		object IViewFor.ViewModel
		{
			get { return _viewModel; }
			set { ViewModel = (T)value; }
		}
	}

	public abstract class UBaseViewController : ReactiveViewController{
		//
		// Constructors

		public ITranslator Translator {get {return DependencyService.Get<ITranslator>();}}
		public IBaseNavigator Navigator {get;set;}
		protected UBaseViewController (string nibNameOrNull, NSBundle nibBundleOrNull)
			:base(nibNameOrNull,nibBundleOrNull)
		{

		}

		protected UBaseViewController (IntPtr handle)
			:base(handle)
		{

		}

		protected UBaseViewController (NSObjectFlag f)
			:base(f)
		{

		}

		protected UBaseViewController ():base()
		{
		}

		protected UBaseViewController (NSCoder c):base(c)
		{

		}

		public abstract void ReloadView();
		public abstract void LoadData(params object[] objs);
		public abstract bool IsShouldCache();
		public virtual void BeforeBack(){
		}
	}
}

