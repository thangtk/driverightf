using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Unicorn.Core.Storages;
using Foundation;

namespace Unicorn.Core.Storages
{
    /// <summary>
    /// SQLite repository provides generic queries.
    /// </summary>
    public abstract partial class SQLiteRepository
    {
        partial void InitDatabase()
        {
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), RepositoryName);
			var sourcePath = @"DataDumps/DriveRight.db";
//				Path.Combine(NSBundle.MainBundle.BundlePath,"DataDumps/DriveRight.db");
			if(!File.Exists(path) && File.Exists(sourcePath)){
				File.Copy(sourcePath, path);
			}
            _db = new SQLiteConnectionWithLock(new SQLiteConnectionString(path, false));
        }
    }
}