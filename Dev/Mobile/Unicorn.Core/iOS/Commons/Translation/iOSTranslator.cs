﻿using System;
using Unicorn.Core.Translation;
using Foundation;
using System.Linq;

[assembly:Unicorn.Core.Dependency.Dependency (typeof(Unicorn.Core.iOSTranslator))]
namespace Unicorn.Core
{
	public class iOSTranslator : ITranslator
	{
		private NSBundle _bundle;
		protected Language _language;

		public iOSTranslator ()
		{
			InitLocalization ();
		}

		#region ITranslator implementation

		public string Translate (string key)
		{
			string language = _bundle.LocalizedString (key, "");
			return language;
		}

		public string Translate (int key)
		{
			return Translate (key.ToString ());
		}

		public Language Language {
			set {
				if (_language == value) {
					return;
				}

				string strLanguage = "en";
				switch (value) 
				{
				case Language.Chinese:
					strLanguage = "zh-Hans";
					_language = Language.Chinese;
					break;
				case Language.English:
					strLanguage = "en";
					_language = Language.English;
					break;

				default:
					_language = Language.English;
					break;

				}
				NSUserDefaults.StandardUserDefaults.SetValueForKey (NSArray.FromStrings (strLanguage), new NSString ("AppleLanguages"));
				NSUserDefaults.StandardUserDefaults.Synchronize ();

				InitLocalization ();
			}

			get {
				return _language;
			}
		}

		#endregion

		private void InitLocalization ()
		{
			string path = null;

//			NSArray languages = (NSArray)NSUserDefaults.StandardUserDefaults.ValueForKey (new NSString ("AppleLanguages"));
//			NSString language = languages.GetItem<NSString> (0);

			string language = NSLocale.PreferredLanguages.FirstOrDefault ();

			NSLocale locale = NSLocale.CurrentLocale;

			path = NSBundle.MainBundle.PathForResource (language.ToString(), "lproj");

			if (language.ToString () == "zh-Hans") {
				if (locale.CountryCode == "TW") {
					path = NSBundle.MainBundle.PathForResource (language.ToString () + "-" + locale.CountryCode, "lproj");
				}			
			}

			if (string.IsNullOrEmpty (path)) {
				path = NSBundle.MainBundle.PathForResource ("en", "lproj");
			}
			_bundle = NSBundle.FromPath (path);
		}
	}
}

