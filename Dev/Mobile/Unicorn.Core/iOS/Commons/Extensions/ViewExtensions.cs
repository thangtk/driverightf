﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;

namespace Unicorn.Core.iOS
{
	public static class ViewExtensions
	{
		public static void ResizeHeigthWithText(this UILabel label,float maxHeight = 960f) 
		{
			float width = (float)label.Frame.Width; 
			if (label.Text == null) {
				label.Text = string.Empty;
			}
			CGSize size = ((NSString)label.Text).StringSize(label.Font,constrainedToSize:new CGSize(width,maxHeight),
				lineBreakMode:UILineBreakMode.WordWrap);
			var labelFrame = label.Frame;
			labelFrame.Size = new CGSize(width,size.Height);
			label.Frame = labelFrame;
		}

		public static CGPoint ConvertPointToParentView(this UIView view, UIView parentView)
		{
			CGPoint point = view.Frame.Location;
			bool isCheckParentView = false;
			while (view.Superview != null && !isCheckParentView) {
				view = view.Superview;
				point.X += view.Frame.X;
				point.Y += view.Frame.Y;
				isCheckParentView |= view == parentView;
			}

			if (isCheckParentView) {
				return point;
			} else {
				return new CGPoint ();
			}
		}

		public static void FixSupperView(this UIView childView, UIView supperView)
		{
			childView.TranslatesAutoresizingMaskIntoConstraints = false;
			supperView.Add (childView);
			NSLayoutConstraint[] constraints = {
				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Top,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Top,1,0),
				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Trailing,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Trailing,1,0),
				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Leading,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Leading,1,0),
				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Bottom,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Bottom,1,0),

			};
			supperView.AddConstraints (constraints);
		}
	}
}

