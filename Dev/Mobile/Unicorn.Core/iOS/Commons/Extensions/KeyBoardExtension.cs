﻿using System;
using UIKit;
using Unicorn.Core.Enums;
using Unicorn.Core.Translation;

namespace Unicorn.Core.iOS
{
	public static class KeyboardExtension
	{
		public static ITranslator Translator {get {return DependencyService.Get<ITranslator>();}}
		
		public static void AddHideButtonToKeyboard (this UITextField txt, KeyboardType keyboardType)
		{
			txt.KeyboardType = GetType(keyboardType);

			AddHideButtonToKeyboard (txt);
		}

		public static void AddHideButtonToKeyboard (this UITextField txt, UIKeyboardType keyboardType)
		{
			AddHideButtonToKeyboard (txt);
		}

		public static void AddHideButtonToKeyboard (this UITextView txt, KeyboardType keyboardType)
		{
			txt.KeyboardType = GetType(keyboardType);

			AddHideButtonToKeyboard (txt);
		}

		public static void AddHideButtonToKeyboard(this UITextView txt, string nameHideButton = "DONE")
		{
			if (txt.InputAccessoryView != null) {
				return;
			}

			UIToolbar toolbar = new UIToolbar ();
			toolbar.BarStyle = UIBarStyle.Black;
			toolbar.Translucent = true;
			toolbar.SizeToFit ();
			UIBarButtonItem doneButton = new UIBarButtonItem (Translator.Translate("BUTTON_DONE") , UIBarButtonItemStyle.Done,
				(s, e) => txt.ResignFirstResponder ());
			toolbar.SetItems (new UIBarButtonItem[]{new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace),doneButton}, true);

			txt.InputAccessoryView = toolbar;
		}
			
		public static void AddHideButtonToKeyboard (this UITextField txt, string nameHideButton = "DONE")
		{
			if (txt.InputAccessoryView != null) {
				return;
			}

			UIToolbar toolbar = new UIToolbar ();
			toolbar.BarStyle = UIBarStyle.Black;
			toolbar.Translucent = true;
			toolbar.SizeToFit ();
			UIBarButtonItem doneButton = new UIBarButtonItem (Translator.Translate("BUTTON_DONE") , UIBarButtonItemStyle.Done,
				                             (s, e) => txt.ResignFirstResponder ());
			toolbar.SetItems (new UIBarButtonItem[]{new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace),doneButton}, true);

			txt.InputAccessoryView = toolbar;
		}

		public static UIView GetViewResponder(UIView viewGroup)
		{
			UIView a = null;
			if(viewGroup.Subviews != null && viewGroup.Subviews.Length > 0)
			{
				foreach (UIView view in viewGroup.Subviews)
				{
					if(view.IsFirstResponder)
					{
						a = view;
						;
						break;
					}
					else
					{
						UIView v = GetViewResponder(view);
						a = v;
						if(a != null)
						{
							break;
						}
					}
				}
			}
			return a;
		}

		public static void AddHideButtonToKeyboard(UIView viewGroup)
		{
			if(viewGroup.Subviews != null && viewGroup.Subviews.Length > 0)
			{
				foreach (UIView view in viewGroup.Subviews)
				{
					if(view is UITextView || view is UITextField)
					{
						view.AddHideButtonToKeyboard();
					}
					else
					{
						AddHideButtonToKeyboard(view);
					}
				}
			}
		}

		public static void AddHideButtonToKeyboard(this UIView txt, string nameHideButton = "DONE")
		{
			if (txt != null) {
				if (txt is UITextView) {
					AddHideButtonToKeyboard (txt as UITextView, nameHideButton);
				} else if (txt is UITextField) {
					AddHideButtonToKeyboard (txt as UITextField, nameHideButton);
				}
			}

		}

		public static UIKeyboardType GetType(KeyboardType type)
		{
			switch (type) {
			case KeyboardType.Default:
				return UIKeyboardType.Default;
			case KeyboardType.Email:
				return UIKeyboardType.EmailAddress;
			case KeyboardType.Number:
				return UIKeyboardType.NumberPad;
			case KeyboardType.Phone:
				return UIKeyboardType.PhonePad;
			default:
				return UIKeyboardType.Default;
			};
		}
	}
}

