﻿using System;
using System.Drawing;
using UIKit;
using CoreGraphics;

namespace Unicorn.Core.iOS
{
    public static class FrameExtension
    {
        public static void ChangeFrameAttributes(this UIView view, float x = -1f, float y = -1f, float width = -1f, float height = -1f)
        {
			CGRect frame = view.Frame;
            frame.X = x == -1f ? frame.X : x;
            frame.Y = y == -1f ? frame.Y : y;
            frame.Width = width == -1f ? frame.Width : width;
            frame.Height = height == -1f ? frame.Height : height;

            view.Frame = frame;
        }
    }
}

