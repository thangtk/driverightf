﻿using System;
using UIKit;
using Foundation;

namespace Unicorn.Core.iOS
{
	public static class ColorHub
	{
		public static readonly UIColor ProfileBackgroundColor = UIColor.FromRGB(240, 239, 244);
		public static readonly UIColor MainBlueColor = UIColor.FromRGB(0, 121, 193);
		public static readonly UIColor PlaceHolderGrayColor = UIColor.FromRGB(179, 179, 179);
		public static readonly UIColor TextFieldGrayColor = UIColor.FromRGB(102, 102, 102);
		public static readonly UIColor DisableTextFieldColor = UIColor.FromRGB(211, 211, 211);
		public static readonly UIColor CustomButtonColorBrown = UIColor.FromRGB(232, 141, 12);
		public static readonly UIColor CustomButtonColorYellow = UIColor.FromRGB(255, 209, 0);
		public static readonly UIColor CustomButtonColorPink = UIColor.FromRGB(255, 61, 106);
		public static readonly UIColor CustomButtonColorPurple = UIColor.FromRGB(110, 12, 232);
		public static readonly UIColor CustomButtonColorPurple2 = UIColor.FromRGB(187, 12, 232);
		public static readonly UIColor CustomButtonColorBlue = UIColor.FromRGB(13, 158, 255);
		public static readonly UIColor CustomButtonColorYellow2 = UIColor.FromRGB(232, 198, 72);
		public static readonly UIColor CustomButtonColorOrange = UIColor.FromRGB(255, 67, 15);
	}
}
