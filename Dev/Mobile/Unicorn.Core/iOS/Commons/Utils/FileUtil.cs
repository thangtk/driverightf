﻿using System;
using UIKit;
using Foundation;
using System.Threading.Tasks;
using System.Drawing;
using CoreImage;
using CoreGraphics;
using System.Runtime.InteropServices;
using MonoTouch;
using System.Collections.Generic;
using AssetsLibrary;
using ObjCRuntime;

namespace Unicorn.Core.iOS
{
	public enum CropPosition
	{
		Start,
		Center,
		End
	}

	public static class FileUtil
	{
		public static UIImage ConvertBase64ToImage(string base64string)
		{
			NSData data = ConvertBase64StringToData(base64string);                          
			return UIImage.LoadFromData(data);
		}

		public static NSData ConvertBase64StringToData(string base64string)
		{
			byte[] encodedDataAsBytes = System.Convert.FromBase64String(base64string);
			return NSData.FromArray(encodedDataAsBytes);      
		}


		public static string GetBase64StringOfImage(string imagePath)
		{
			return ConvertImageToBase64(UIImage.FromFile(imagePath));
		}

		public static string ConvertImageToBase64(UIImage image)
		{
			string base64string = "";

			using(NSData imageData = image.AsPNG())
			{
				Byte[] myByteArray = new Byte[imageData.Length];
				System.Runtime.InteropServices.Marshal.Copy(imageData.Bytes, myByteArray, 0, Convert.ToInt32(imageData.Length));
				base64string = System.Convert.ToBase64String(myByteArray);
			}

			return base64string;
		}

		public static string ConvertDataToBase64(NSData data)
		{
			if(data == null)
			{
				return null;
			}

			string base64string = "";

			Byte[] myByteArray = new Byte[data.Length];
			System.Runtime.InteropServices.Marshal.Copy(data.Bytes, myByteArray, 0, Convert.ToInt32(data.Length));
			base64string = System.Convert.ToBase64String(myByteArray);

			return base64string;
		}

		public static string ConvertDataToHex(NSData data)
		{
			string buf = "";
			for(int i = 0; i < Convert.ToInt32(data.Length); i++)
			{
				buf += string.Format("{0:X2}", data[i]);
			}

			return buf;
		}

		public static async Task<UIImage> ImageFromUrl(string imageUrl)
		{
			if(imageUrl == null)
			{
				return null;
			}

			return await Task.Run(() =>
			{
				NSData imageData = NSData.FromUrl(NSUrl.FromString(imageUrl));
				if(imageData == null)
				{
					return null;
				}

				UIImage img = UIImage.LoadFromData(imageData);
				return img;
			});
		}

		/// <summary>
		/// strech resize
		/// </summary>
		public static UIImage Resize(UIImage modifiedImage, float width, float height)
		{
			UIGraphics.BeginImageContext(new SizeF(width, height));
			//
			modifiedImage.Draw(new RectangleF(0, 0, width, height));
			modifiedImage = UIGraphics.GetImageFromCurrentImageContext();
			//
			UIGraphics.EndImageContext();

			return modifiedImage;
		}

		public static UIImage RatioResizeToWidth(UIImage modifiedImage, float width)
		{
			Console.WriteLine("resize to width: " + width);
			float cur_width = (float)modifiedImage.Size.Width;
			if(cur_width > width)
			{
				float ratio = width / cur_width;
				var height = (float)modifiedImage.Size.Height * ratio;
				//resize
				modifiedImage = Resize(modifiedImage, width, height);
			}

			return modifiedImage;
		}

		public static UIImage RatioResizeToHeight(UIImage modifiedImage, float height)
		{
			Console.WriteLine("resize to height: " + height);
			float cur_height = (float)modifiedImage.Size.Height;
			if(cur_height > height)
			{
				var ratio = height / cur_height;
				float width = (float)modifiedImage.Size.Width * ratio;
				//
				Resize(modifiedImage, width, height);
			}

			return modifiedImage;
		}

		public static UIImage CropImage(this UIImage modifiedImage, RectangleF section, CropPosition position = CropPosition.Center)
		{
			float width = section.Width;
			float height = section.Height;

			CGSize ImgSize = modifiedImage.Size;
			//
			if(ImgSize.Width < width)
				width = (float)ImgSize.Width;
			if(ImgSize.Height < height)
				height = (float)ImgSize.Height;
			//
			float crop_x = 0;
			float crop_y = 0;
			if(ImgSize.Width / width < ImgSize.Height / height)
			{
				//scad din width
				modifiedImage = RatioResizeToWidth(modifiedImage, width);
				ImgSize = modifiedImage.Size;
				//compute crop_y
				if(position == CropPosition.Center)
					crop_y = ((float)ImgSize.Height / 2) - (height / 2);
				if(position == CropPosition.End)
					crop_y = (float)ImgSize.Height - height;
			}
			else
			{
				//change height
				modifiedImage = RatioResizeToHeight(modifiedImage, height);
				ImgSize = modifiedImage.Size;
				//calculeaza crop_x
				if(position == CropPosition.Center)
					crop_x = ((float)ImgSize.Width / 2) - (width / 2);
				if(position == CropPosition.End)
					crop_x = (float)ImgSize.Width - width;
			}
			//create new contect
			UIGraphics.BeginImageContext(new SizeF(width, height));
			CGContext context = UIGraphics.GetCurrentContext();
			//crops the new context to the desired height and width
			RectangleF clippedRect = new RectangleF(0, 0, width, height);
			context.ClipToRect(clippedRect);
			//draw my image on the context
			RectangleF drawRect = new RectangleF(-crop_x, -crop_y, (float)ImgSize.Width, (float)ImgSize.Height);
			modifiedImage.Draw(drawRect);
			//save the context in modifiedImage
			modifiedImage = UIGraphics.GetImageFromCurrentImageContext();
			//close the context
			UIGraphics.EndImageContext();

			return modifiedImage;
		}

		public static UIImage CropAndReSizeImage(this UIImage sourceImage, RectangleF rec, SizeF size)
		{
			var imgSize = sourceImage.Size;
			UIGraphics.BeginImageContext(size);
			var context = UIGraphics.GetCurrentContext();
			var clippedRect = new RectangleF(0, 0, size.Width, size.Height);
			context.ClipToRect(clippedRect);
			var ratio = size.Width / rec.Width;
			var drawRect = new RectangleF(-rec.X*ratio, -rec.Y*ratio, (float)imgSize.Width*ratio, (float)imgSize.Height*ratio);
			sourceImage.Draw(drawRect);
			var modifiedImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();
			sourceImage.Dispose ();
			return modifiedImage;

		}
		public static UIImage ReSizeImage(this UIImage sourceImage,float max)
		{
			var imgSize = sourceImage.Size;
			bool isResize = false;
			float newW = (float)imgSize.Width;
			float newH = (float)imgSize.Height;
			if (imgSize.Width >= imgSize.Height && imgSize.Width > max) {
				newW = max;newH = newH * newW / (float)imgSize.Width;
				isResize = true;
			}
			if (imgSize.Width < imgSize.Height && imgSize.Height > max) {
				newH = max;newW = newW * newH / (float)imgSize.Height;
				isResize = true;
			}
			if (!isResize) {
				return sourceImage;
			}
			UIGraphics.BeginImageContext(new SizeF(newW,newH));
			var context = UIGraphics.GetCurrentContext();
			var drawRect = new RectangleF(0, 0,newW, newH);
			sourceImage.Draw(drawRect);
			var modifiedImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();
			sourceImage.Dispose ();
			return modifiedImage;

		}
		public static UIImage ConvertToGrayScale(UIImage image)
		{

			RectangleF imageRect = new RectangleF(0, 0, (float)image.Size.Width, (float)image.Size.Height);
			using(var colorSpace = CGColorSpace.CreateDeviceGray())
			using(var context = new CGBitmapContext(IntPtr.Zero, (int)imageRect.Width, (int)imageRect.Height, 8, 0, colorSpace, CGImageAlphaInfo.None))
			{
				context.DrawImage(imageRect, image.CGImage);
				using(var imageRef = context.ToImage())
					return new UIImage(imageRef);
			}
		}

		struct vImage_Buffer
		{
			public System.IntPtr data;
			public uint height;
			public uint width;
			public uint rowBytes;
		};

		[DllImport(Constants.AccelerateImageLibrary)]
		static extern int vImageBoxConvolve_ARGB8888(ref vImage_Buffer src, ref vImage_Buffer dest, IntPtr tempBuffer, uint srcOffsetToROI_X, uint srcOffsetToROI_Y, uint kernel_height, uint kernel_width, byte[] backgroundColor, uint flags);

		[DllImport(Constants.AccelerateImageLibrary)]
		static extern int vImageMatrixMultiply_ARGB8888(ref vImage_Buffer src, ref vImage_Buffer dest, short[] matrix, int divisor, IntPtr pre_bias, IntPtr post_bias, uint flags);

		public static UIImage ApplyLightEffect(this UIImage image)
		{
			UIColor tintColor = UIColor.FromWhiteAlpha(1.0f, 0.3f);
			return ApplyBlur(image, 30f, tintColor, 1.8f, null);
		}

		public static UIImage ApplyExtraLightEffect(this UIImage image)
		{
			UIColor tintColor = UIColor.FromWhiteAlpha(0.97f, 0.82f);
			return ApplyBlur(image, 20f, tintColor, 1.8f, null);
		}

		public static UIImage ApplyDarkEffect(this UIImage image)
		{
			UIColor tintColor = UIColor.FromWhiteAlpha(0.11f, 0.73f);
			return ApplyBlur(image, 20f, tintColor, 1.8f, null);
		}

		public static UIImage ApplyTintEffectWithColor(this UIImage image, UIColor tintColor)
		{
			const float EFFECT_COLOR_ALPHA = 0.6f;
			UIColor effectColor = tintColor;
			int componentCount = (int)tintColor.CGColor.NumberOfComponents;
			if(componentCount == 2)
			{
				nfloat b, a;
				if(tintColor.GetWhite(out b, out a))
				{
					effectColor = UIColor.FromWhiteAlpha(b, EFFECT_COLOR_ALPHA);
				}
			}
			else
			{
				nfloat r, g, b, a;
				try
				{
					tintColor.GetRGBA(out r, out g, out b, out a);
					effectColor = UIColor.FromRGBA(r, g, b, EFFECT_COLOR_ALPHA);
				}
				catch
				{
				}
			}
			return ApplyBlur(image, 10f, effectColor, -1.0f, null);
		}

		public static UIImage ApplyBlur(this UIImage image, float blurRadius, UIColor tintColor, float saturationDeltaFactor, UIImage maskImage)
		{
			if(image.Size.Width < 1 || image.Size.Height < 1)
			{
				Console.WriteLine("*** error: invalid size: ({0} x .{1}). Both dimensions must be >= 1: {2}", image.Size.Width, image.Size.Height, image);
				return null;
			}
			if(image.CGImage == null)
			{
				Console.WriteLine("*** error: image must be backed by a CGImage: {0}", image);
				return null;
			}
			if(maskImage != null && maskImage.CGImage == null)
			{
				Console.WriteLine("*** error: maskImage must be backed by a CGImage: {0}", maskImage);
				return null;
			}

			RectangleF imageRect = new RectangleF(0, 0, (float)image.Size.Width, (float)image.Size.Height);
			UIImage effectImage = image;

			bool hasBlur = blurRadius > float.Epsilon;
			bool hasSaturationChange = Math.Abs(saturationDeltaFactor - 1.0f) > float.Epsilon;
			if(hasBlur || hasSaturationChange)
			{
				UIGraphics.BeginImageContextWithOptions(image.Size, false, UIScreen.MainScreen.Scale);
				CGContext effectInContext = UIGraphics.GetCurrentContext();
				effectInContext.ScaleCTM(1.0f, -1.0f);
				effectInContext.TranslateCTM(0.0f, -image.Size.Height);
				effectInContext.DrawImage(imageRect, image.CGImage);

				CGBitmapContext effectInContextAsBitmapContext = effectInContext.AsBitmapContext();

				vImage_Buffer effectInBuffer;
				effectInBuffer.data = effectInContextAsBitmapContext.Data;
				effectInBuffer.width = (uint)effectInContextAsBitmapContext.Width;
				effectInBuffer.height = (uint)effectInContextAsBitmapContext.Height;
				effectInBuffer.rowBytes = (uint)effectInContextAsBitmapContext.BytesPerRow;

				UIGraphics.BeginImageContextWithOptions(image.Size, false, UIScreen.MainScreen.Scale);
				CGContext effectOutContext = UIGraphics.GetCurrentContext();

				CGBitmapContext effectOutContextAsBitmapContext = effectOutContext.AsBitmapContext();

				vImage_Buffer effectOutBuffer;
				effectOutBuffer.data = effectOutContextAsBitmapContext.Data;
				effectOutBuffer.width = (uint)effectOutContextAsBitmapContext.Width;
				effectOutBuffer.height = (uint)effectOutContextAsBitmapContext.Height;
				effectOutBuffer.rowBytes = (uint)effectOutContextAsBitmapContext.BytesPerRow;

				if(hasBlur)
				{
					float inputRadius = blurRadius * (float)UIScreen.MainScreen.Scale;
					uint radius = (uint)Math.Floor(inputRadius * 3.0 * Math.Sqrt(2 * Math.PI) / 4 + 0.5);
					if(radius % 2 != 1)
					{
						radius += 1;
					}
					const uint kvImageEdgeExtend = 8;
					vImageBoxConvolve_ARGB8888(ref effectInBuffer, ref effectOutBuffer, IntPtr.Zero, 0, 0, radius, radius, null, kvImageEdgeExtend);
					vImageBoxConvolve_ARGB8888(ref effectOutBuffer, ref effectInBuffer, IntPtr.Zero, 0, 0, radius, radius, null, kvImageEdgeExtend);
					vImageBoxConvolve_ARGB8888(ref effectInBuffer, ref effectOutBuffer, IntPtr.Zero, 0, 0, radius, radius, null, kvImageEdgeExtend);
				}
				bool effectImageBuffersAreSwapped = false;
				if(hasSaturationChange)
				{
					float s = saturationDeltaFactor;
					float[] floatingPointSaturationMatrix = new float[] {
						0.0722f + 0.9278f * s,
						0.0722f - 0.0722f * s,
						0.0722f - 0.0722f * s,
						0f,
						0.7152f - 0.7152f * s,
						0.7152f + 0.2848f * s,
						0.7152f - 0.7152f * s,
						0f,
						0.2126f - 0.2126f * s,
						0.2126f - 0.2126f * s,
						0.2126f + 0.7873f * s,
						0f,
						0f,
						0f,
						0f,
						1f,
					};
					const int divisor = 256;
					uint matrixSize = (uint)floatingPointSaturationMatrix.Length;
					short[] saturationMatrix = new short[matrixSize];
					for(uint i = 0; i < matrixSize; ++i)
					{
						saturationMatrix[i] = (short)Math.Round(floatingPointSaturationMatrix[i] * divisor);
					}
					if(hasBlur)
					{
						const uint kvImageNoFlags = 0;
						vImageMatrixMultiply_ARGB8888(ref effectOutBuffer, ref effectInBuffer, saturationMatrix, divisor, IntPtr.Zero, IntPtr.Zero, kvImageNoFlags);
						effectImageBuffersAreSwapped = true;
					}
					else
					{
						const uint kvImageNoFlags = 0;
						vImageMatrixMultiply_ARGB8888(ref effectInBuffer, ref effectOutBuffer, saturationMatrix, divisor, IntPtr.Zero, IntPtr.Zero, kvImageNoFlags);
					}
				}
				if(!effectImageBuffersAreSwapped)
				{
					effectImage = UIGraphics.GetImageFromCurrentImageContext();
				}
				UIGraphics.EndImageContext();

				if(effectImageBuffersAreSwapped)
				{
					effectImage = UIGraphics.GetImageFromCurrentImageContext();
				}
				UIGraphics.EndImageContext();
			}

			UIGraphics.BeginImageContextWithOptions(image.Size, false, UIScreen.MainScreen.Scale);
			CGContext outputContext = UIGraphics.GetCurrentContext();
			outputContext.ScaleCTM(1.0f, -1.0f);
			outputContext.TranslateCTM(0, -image.Size.Height);

			outputContext.DrawImage(imageRect, image.CGImage);

			if(hasBlur)
			{
				outputContext.SaveState();
				if(maskImage != null)
				{
					outputContext.ClipToMask(imageRect, maskImage.CGImage);
				}
				outputContext.DrawImage(imageRect, effectImage.CGImage);
				outputContext.RestoreState();
			}

			if(tintColor != null)
			{
				outputContext.SaveState();
				outputContext.SetFillColor(tintColor.CGColor.Components);
				outputContext.FillRect(imageRect);
				outputContext.RestoreState();
			}

			UIImage outputImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();

			return outputImage;
		}
	}
}

