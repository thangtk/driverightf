﻿using System;
using UIKit;
using System.Globalization;

namespace Unicorn.Core.iOS
{
	public static class DeviceUtil
	{
		public static double OSVersion
		{
			get
			{
				string systemVersion =  UIDevice.CurrentDevice.SystemVersion.Substring(0,1);

				return double.Parse(systemVersion);
			}
		}

		public static bool IsPhone
		{
			get
			{
				return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone;
			}
		}

		public static bool IsPad
		{
			get
			{
				return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad;
			}
		}

		public static bool IsiPhone5
		{
			get
			{ 
				return UIScreen.MainScreen.Bounds.Size.Height == 568;
			}
		}
	}
}

