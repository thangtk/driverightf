﻿using System;
using Foundation;
using UIKit;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(Unicorn.Core.iOS.DeviceManager))]
namespace Unicorn.Core.iOS
{
	/// <summary>
	/// iOS DeviceManager: functions to get device info
	/// </summary>
	public class DeviceManager
	{
		private const string KEY_UUID = "uuid";
		public const int IPHONE_5_HEIGHT = 568;
		private DeviceInfo _deviceInfo = null;

		public DeviceManager ()
		{
		}

		/// <summary>
		/// Gets the device info.
		/// </summary>
		/// <returns>The device info.</returns>
		public DeviceInfo GetDeviceInfo(){
			if (_deviceInfo != null) {
				return _deviceInfo;
			}
			//
			_deviceInfo = new DeviceInfo ();
			string uuid = NSUserDefaults.StandardUserDefaults.StringForKey(KEY_UUID);

			if (!string.IsNullOrEmpty(uuid))
			{
				_deviceInfo.DeviceId = uuid;
			}
			else
			{
				_deviceInfo.DeviceId = CreateUUID();
				NSUserDefaults.StandardUserDefaults.SetString(_deviceInfo.DeviceId, KEY_UUID);
			}
			//
			string ver = UIDevice.CurrentDevice.SystemVersion;
			string model = DeviceHardware.Version;
			_deviceInfo.DeviceName = "iOS " + ver + " " + model + " " + UIDevice.CurrentDevice.Name;
			_deviceInfo.OSName = "iOS " + ver;
			_deviceInfo.OSVersion = ver;
			//
			return _deviceInfo;
		}

		private string CreateUUID()
		{
			System.Diagnostics.Debug.WriteLine("++++ Device ID: {0}", UIDevice.CurrentDevice.IdentifierForVendor.AsString());

			//            string id = UIDevice.CurrentDevice.IdentifierForVendor.AsString();
			//            string systemVerion = UIDevice.CurrentDevice.SystemVersion;
			//            string model = UIDevice.CurrentDevice.Model;
			//           
			//            return string.Format("{0}-{1}-{2}", id, model, systemVerion);

			return UIDevice.CurrentDevice.IdentifierForVendor.AsString();
		}
  
		public double OSVersion
		{
			get
			{
				string systemVersion =  UIDevice.CurrentDevice.SystemVersion.Substring(0,1);

				return double.Parse(systemVersion);
			}
		}

		public bool IsPhone
		{
			get
			{
				return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone;
			}
		}

		public bool IsPad
		{
			get
			{
				return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad;
			}
		}

		public bool IsiPhone5
		{
			get
			{ 
				return UIScreen.MainScreen.Bounds.Size.Height == IPHONE_5_HEIGHT;
			}
		}
	}

}

