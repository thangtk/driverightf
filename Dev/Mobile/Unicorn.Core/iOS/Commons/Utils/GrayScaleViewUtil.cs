﻿using System;
using UIKit;
using CoreImage;
using CoreGraphics;
using System.Drawing;
using Unicorn.Core;

namespace Unicorn.Core.iOS
{
	public static class GrayScaleViewUtil
	{
		private static UIImage ConvertToGrayScaleNoAlpha(UIImage image)
		{
			if (image == null) {
				return null;
			}

			RectangleF imageRect = new RectangleF (0f, 0f, (float) image.Size.Width,(float) image.Size.Height);
			using (var colorSpace = CGColorSpace.CreateCalibratedGray(new nfloat[]{50, 50, 50}, new nfloat[]{50, 50, 90}, 0.7f))
			using (var context = new CGBitmapContext (IntPtr.Zero, (int) imageRect.Width, (int) imageRect.Height, 8, 0, colorSpace, CGImageAlphaInfo.None)) {

				context.DrawImage (imageRect, image.CGImage);
				using (var imageRef = context.ToImage ()) {	
					return UIImage.FromImage (imageRef);
				}
			}
		}

		private static UIImage ConvertToGrayScaleAlpha (UIImage image)
		{
			if (image == null) {
				return null;
			}

			RectangleF imageRect = new RectangleF (0f, 0f, (float)image.Size.Width, (float)image.Size.Height);
			using (var colorSpace = CGColorSpace.Null)
			using (var context = new CGBitmapContext (IntPtr.Zero, (int) imageRect.Width, (int) imageRect.Height, 8, 0, colorSpace, CGImageAlphaInfo.Only)) {
				{
					context.DrawImage (imageRect, image.CGImage);
					using (var imageRef = context.ToImage ()) {
						return UIImage.FromImage (imageRef);
					}
				}
			}
		}

		public static UIImage ConvertToGrayScale(UIImage image)
		{
			MyDebugger.ShowThreadInfo ("ConvertToGrayScale");
			if (image == null) {
				return null;
			}

			CGImage imageNoAlpha = ConvertToGrayScaleNoAlpha (image).CGImage;
			CGImage imageAlpha = ConvertToGrayScaleAlpha (image).CGImage;

			using (CGImage grayScaleImage = imageNoAlpha.WithMask (imageAlpha)) {

				return UIImage.FromImage (grayScaleImage, image.CurrentScale, image.Orientation);
			}

		}

	}
}

