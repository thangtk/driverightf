using System;
using UIKit;
using CoreImage;
using CoreGraphics;
using System.Drawing;
using Unicorn.Core.iOS;

namespace Unicorn.Core.iOS
{
	public static class BlurViewUtil
	{
		private static float _blurRadius = 5f;
		public static UIImage _imgBlur1;
		public static UIImage _imgBlur2;

		public static float BlurRadius {
			get { return _blurRadius;}
			set { _blurRadius = value;}
		}

		public static UIImage BlurView (UIView view, float radius = 5f)
		{
			if (_imgBlur1 == null)
			{
				_imgBlur1 = BlurImage (Snapshot (view), radius);
			}

			return _imgBlur1;
		}

		public static UIImage BlurImage (UIImage image, float radius = 5f)
		{   
			BlurRadius = radius;
			if (_imgBlur2 != null)
			{
				return _imgBlur2;
			}

			using (var blur = new CIGaussianBlur ())
			{
				blur.Image = new CIImage (image);
				blur.Radius = BlurRadius;

				float width = (float)image.Size.Width;
				float height = (float)image.Size.Height;
				using (CIImage output = blur.OutputImage)
				using (CIContext context = CIContext.FromOptions (null))
				using (CGImage cgimage = context.CreateCGImage (output, new RectangleF (0, 0, width, height)))
				{
					UIImage cropImage = UIImage.FromImage (Crop (CIImage.FromCGImage (cgimage), width, height));
					_imgBlur2 = FileUtil.Resize (cropImage, width, height);

					return _imgBlur2;
				}
			}
		}

		private static CIImage Crop (CIImage image, float width, float height)
		{
			var crop = new CICrop { 
				Image = image,
				Rectangle = new CIVector (10, 10, width - 20, height - 20) 
			};

			return crop.OutputImage; 
		}

		private static UIImage Snapshot (UIView View)
		{
			UIImage image;
			UIGraphics.BeginImageContext (View.Bounds.Size);
			//pre-iOS 7 using layer to snapshot
			if (DeviceUtil.OSVersion < 7)
			{
				View.Layer.RenderInContext (UIGraphics.GetCurrentContext ());
			} else
			{
				View.DrawViewHierarchy (View.Bounds, true);
			}
			image = UIGraphics.GetImageFromCurrentImageContext ();
			UIGraphics.EndImageContext ();
			return image;
		}
	}
}

