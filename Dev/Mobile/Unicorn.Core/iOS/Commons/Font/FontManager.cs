﻿using System;
using UIKit;
using System.Linq;

namespace Unicorn.Core
{
	public abstract class FontManager
	{
		//		private string _defaultFont = "Helvetica";

		public abstract string MainFont
		{
			get;
		}

		public UIFont GetMainFont(float fontSize = 17f, bool bold = false)
		{

			return GetFont(MainFont, fontSize, bold);
		}

		public UIFont GetFont(string fontName, float fontSize = 17f, bool bold = false)
		{
			// Skip check
			//			var b = ExistInSystemFonts (fontName);
			var font = GetFontByName(fontName, fontSize);
			if(bold)
			{
				font = MakeBold(font);
			}
			//
			return font;
		}

		private UIFont GetFontByName(string fontName, float fontSize)
		{
			var font = UIFont.FromName(fontName, fontSize);
			if(font == null)
			{
				throw new Exception ("Don't exist font with name is " + fontName);
				// Or use default font
			}
			return font;
		}

		public UIFont MakeBold(UIFont font)
		{
			var desc = font.FontDescriptor;

			// create a new descriptor based on the baseline, with
			// the requested Bold trait
			var bold = desc.CreateWithTraits(UIFontDescriptorSymbolicTraits.Bold);

			// Create font from the bold descriptor
			return UIFont.FromDescriptor(bold, font.PointSize);
		}

		public UIFont MakeItalic(UIFont font)
		{
			var desc = font.FontDescriptor;

			// create a new descriptor based on the baseline, with
			// the requested Bold trait
			var italic = desc.CreateWithTraits(UIFontDescriptorSymbolicTraits.Italic);

			// Create font from the bold descriptor
			return UIFont.FromDescriptor(italic, font.PointSize);
		}

		private bool ExistInSystemFonts(string fontName)
		{
			var sysFontNames = UIFont.FamilyNames.ToList();
			sysFontNames.ForEach(x => System.Diagnostics.Debug.WriteLine(">>> Font >>> " + x));
			return sysFontNames.Find(x => x.Equals(fontName, StringComparison.OrdinalIgnoreCase)).Count() > 0;
		}

	}
}

