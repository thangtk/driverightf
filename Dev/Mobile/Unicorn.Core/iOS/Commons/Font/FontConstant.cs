﻿using System;

namespace Unicorn.Core
{
	public static class FontConstant
	{
		public static readonly string Helvetica_Neue = "Helvetica Neue";
		public static readonly string HelveticaNeue_CondensedBlack = "HelveticaNeue-CondensedBlack";
	}
}

