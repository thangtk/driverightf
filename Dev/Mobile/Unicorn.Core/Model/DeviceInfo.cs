﻿using System;

namespace Unicorn.Core
{
	public class DeviceInfo
	{
		public string DeviceId {
			get;
			set;
		}

		public string DeviceName {
			get;
			set;
		}

		public string OSName {
			get;
			set;
		}

		public string OSVersion {
			get;
			set;
		}
	}
}

