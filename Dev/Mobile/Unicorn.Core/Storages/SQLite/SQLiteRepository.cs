using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Unicorn.Core.Bases;

namespace Unicorn.Core.Storages
{
    /// <summary>
    /// SQLite repository provides generic queries.
    /// </summary>
    public abstract partial class SQLiteRepository : BaseRepository
    {
        protected SQLiteConnectionWithLock _db;

        /// <summary>
        /// Thread safe locker.
        /// </summary>
        protected static object _locker = new object();


        public SQLiteConnectionWithLock Connection
        {
            get
            {
                return _db;
            }
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="SQLiteRepository"/> class.
        /// </summary>
        public SQLiteRepository()
        {
            InitDatabase();
        }

        partial void InitDatabase();

        public TableQuery<T> GetTableQuery<T>() where T : IEntity, new()
        {
            return _db.Table<T>();
        }

        /// <summary>
        /// Gets the item based on the id.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public T Get<T>(int id = 0) where T : IEntity, new()
        {
            lock (_locker)
            {
                using (_db.Lock())
                {
                    if (id == 0)
                    {
                        return _db.Table<T>().FirstOrDefault();
                    }
                    else
                    {
                        return _db.Table<T>().FirstOrDefault(x => x.Id == id);
                    }
                }
            }
        }

        /// <summary>
        /// Get item by condition
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filter"></param>
        /// <returns></returns>
        public T Get<T>(Func<T, bool> filter) where T : IEntity, new()
        {
            lock (_locker)
            {
                using (_db.Lock())
                {
                    return _db.Table<T>().FirstOrDefault(filter);
                }
            }
        }

        /// <summary>
        /// Get last item
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filter"></param>
        /// <returns></returns>
        public T GetLast<T>() where T : IEntity, new()
        {
            lock (_locker)
            {
                using (_db.Lock())
                {
                    return _db.Table<T>().LastOrDefault();
                }
            }
        }

        /// <summary>
        /// Gets all the items.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public List<T> GetAll<T>() where T : IEntity, new()
        {
            lock (_locker)
            {
                using (_db.Lock())
                {
                    return (from i in _db.Table<T>()
                            select i).ToList();
                }
            }
        }

        /// <summary>
        /// Gets all the items.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public List<T> GetList<T>(Func<T, bool> filter) where T : IEntity, new()
        {
            lock (_locker)
            {
                using (_db.Lock())
                {
                    return _db.Table<T>().Where(filter).ToList();
                }
                //return (from i in _db.Table<T>() select i).ToList();
            }
        }


        /// <summary>
        /// Saves the item.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public override int Save<T>(T item)
        {
            lock (_locker)
            {
                using (_db.Lock())
                {
                    if (item != null)
                    {
                        if (item.Id != 0)
                        {
                            _db.Update(item);
                            return item.Id;
                        }
                        else
                        {
                            return _db.Insert(item);
                        }
                    }
                    return -1;
                }
            }
        }

        /// <summary>
        /// Saves all the items.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items">The items.</param>
        public void Save<T>(IList<T> items) where T : IEntity
        {
            lock (_locker)
            {
                using (_db.Lock())
                {
                    _db.BeginTransaction();
                    foreach (T item in items)
                    {
                        try
                        {
                            Save<T>(item);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Error >>> " + e.ToString());
                        }
                    }
                    _db.Commit();
                }
            }
        }

        /// <summary>
        /// Deletes the item by the id.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public int Delete<T>(int id = 0) where T : IEntity, new()
        {
            lock (_locker)
            {
                using (_db.Lock())
                {
                    return _db.Delete<T>(id);
                }
            }
        }

        /// <summary>
        /// Deletes the items by the count.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id">The count.</param>
        /// <returns></returns>
        public void DeleteByCount<T>(int count) where T : IEntity, new()
        {
            lock (_locker)
            {
                using (_db.Lock())
                {
                    for (int i = 0; i < count; i++)
                    {
                        var first = _db.Table<T>().FirstOrDefault();
                        if (first != null)
                        {
                            _db.Delete<T>(first.Id);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Deletes all 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public void DeleteAll<T>() where T : IEntity, new()
        {
            lock (_locker)
            {
                using (_db.Lock())
                {
                    _db.DeleteAll<T>();
                }
            }
        }

        public void Execute(string query)
        {
            lock (_locker)
            {
                using (_db.Lock())
                {
                    _db.Execute(query);
                }
            }
        }

        public void Update<T>(IList<T> items,Func<T, bool> filter) where T : IEntity, new()
        {
            lock (_locker)
            {
                using (_db.Lock())
                {
                    IList<T> updateItems = items.Where(filter).ToList();
                    if(updateItems != null && updateItems.Count > 0)
                    {
                        _db.UpdateAll(updateItems);
                    }
                }
            }
        }

        public void Update<T>(IList<T> items)
        {
            lock (_locker)
            {
                using (_db.Lock())
                {
                    if (items != null && items.Count > 0)
                    {
                        _db.UpdateAll(items);
                    }
                }
            }
        }

		public void DeleteList<T>(IList<T> items) where T : BaseModel,new()
        {
            lock (_locker)
            {
                using (_db.Lock())
                {
                    if (items != null && items.Count > 0)
                    {
                        foreach (T item in items)
                        {
                            _db.Delete<T>(item.Id);
                        }
                    }
                }
            }
        }
    }
}