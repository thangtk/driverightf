﻿using Unicorn.Core.Bases;

namespace Contemi.Storages
{
    public abstract class BaseRepository
    {
        /// <summary>
        /// Gets the name of the repository.
        /// </summary>
        /// <value>
        /// The name of the repository.
        /// </value>
        public abstract string RepositoryName { get; }

        /// <summary>
        /// Gets the repository version.
        /// </summary>
        /// <value>
        /// The repository version.
        /// </value>
        public abstract string RepositoryVersion { get; }

        /// <summary>
        /// Saves the specified item.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public abstract int Save<T>(T item) where T : IEntity;

        /// <summary>
        /// Get repository content
        /// </summary>
        /// <returns></returns>
        public abstract string GetContent();
    }
}