﻿using System;
using System.IO;
using System.Text;

namespace Contemi.Storages
{
    public abstract class FileRepository : BaseRepository
    {
        public string FilePath { get; private set; }

        public FileRepository()
        {
            FilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), RepositoryName);
        }

        public override int Save<T>(T item)
        {
            string msg = item.ToString() + Environment.NewLine;

            File.AppendAllText(FilePath, msg, Encoding.UTF8);

            return 0;
        }

        public string Read()
        {
            if (!File.Exists(FilePath))
            {
                return string.Empty;
            }

            return File.ReadAllText(FilePath);
        }

        public bool DeleteFile()
        {
            if (!File.Exists(FilePath))
            {
                return false;
            }

            try
            {
                File.Delete(FilePath);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public override string GetContent()
        {
            return Read();
        }
    }
}