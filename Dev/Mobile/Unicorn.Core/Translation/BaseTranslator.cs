﻿using System;
using Unicorn.Core.Translation;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;


namespace Unicorn.Core
{
    /// <summary>
    /// No need to use ITranslator for this solution
    /// Remove it later
    /// </summary>
	public abstract class BaseTranslator : ITranslator
    {
		public BaseTranslator()
        {
			Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
        }

		public Language Language {
			get {
				throw new NotImplementedException ();
			}
			set {
				throw new NotImplementedException ();
			}
		}

        public string Translate(int key)
        {
            throw new NotImplementedException();
        }

		/// <summary>
		/// TODO: Consider about using ConcurrentDictionary
		/// </summary>
        private Dictionary<string, string> _stringResources = null;

        /// <summary>
        /// Get string in current language
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string Translate(string key)
        {
            if (_stringResources == null)
            {
                // Get resource strings
				_stringResources = GetStringResources(Culture);
            }
            //
            if (_stringResources.ContainsKey(key))
            {
                return _stringResources[key];
            }
            else
            {
                return key;
            }
        }

        /// <summary>
        /// Get resource strings by language
        /// </summary>
		private Dictionary<string, string> GetStringResources(CultureInfo culture)
        {
            // There are some other ways to implement
			return GetResourceFromFile(culture);
        }

		private string GetResourceFileName(CultureInfo culture){
			return culture.ToString() + ".strings";
		}

		/// <summary>
		/// NOTE: Dependen on folder and file name in project
		/// </summary>
		/// <returns>The resource from file.</returns>
		/// <param name="culture">Culture.</param>
		private Dictionary<string, string> GetResourceFromFile(CultureInfo culture) {
			var assembly = GetResourceAssembly ();
			string name = string.Concat(assembly.GetName().Name, ".", GetResourceDomain(), ".", GetResourceFileName(culture));
			var stream = assembly.GetManifestResourceStream(name);
            if (stream == null) {
				throw new NotImplementedException(string.Format("Cannot find resource file of {0}. Please correct file path as \"{1}\"!!!", culture.DisplayName, name));
            }
            // TODO: Implement parse data from stream to Dictionary

            return new Dictionary<string,string>();
        }

		protected abstract Assembly GetResourceAssembly ();

		/// <summary>
		/// Separate by dot charactor (.)
		/// </summary>
		/// <returns>The resource domain.</returns>
		protected abstract string GetResourceDomain ();

		private CultureInfo _culture = null;
		public CultureInfo Culture {
			get{ 
				return _culture;
			}
			set { 
				if (_culture != value) {
					_culture = value;
					_stringResources = GetStringResources (_culture);
				}
			}
		}
    }
}

