using System;

namespace Unicorn.Core.Translation
{
    public enum Language
    {
        English,
        French,
        VietNamese,
		Chinese
    }

    public interface ITranslator
    {
        string Translate(string key);

        string Translate(int key);

        Language Language { get; set; }
    }
}

