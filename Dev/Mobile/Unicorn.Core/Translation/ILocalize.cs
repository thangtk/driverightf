﻿using System;
using System.Globalization;

namespace Unicorn.Core
{
	public interface ILocalize
	{
		CultureInfo GetCurrentCultureInfo ();
	}
}

