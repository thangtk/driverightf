using System;

namespace Unicorn.Core.UI
{
    public interface IParamTransfer<T>
    {
        void TransferParam(T param);
    }

    public interface IParamTransfer<T1, T2>
    {
        void TransferParam(T1 param1, T2 param2);
    }
}

