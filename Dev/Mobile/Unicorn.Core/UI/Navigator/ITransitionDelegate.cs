﻿using System;

namespace Unicorn.Core.Navigator
{
	public interface ITransitionDelegate
	{
		void PopTransitionDidFinish (ITransition transition);
		void PushTransitionDidFinish (ITransition transition);
	}
}

