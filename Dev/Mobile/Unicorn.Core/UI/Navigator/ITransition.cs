﻿using System;

namespace Unicorn.Core.Navigator
{
	public interface ITransition
	{
		void Animation(object viewIn, object viewOut);
		//ITransition GetReverseTransition ();
		UTransitionType Type { get; set;}
		TimeSpan Duration {get;set;}
	}


}

