﻿using System;

namespace Unicorn.Core.Navigator
{
	public interface INavAnimator
	{
		void Animate();
	}
}

