using System;
using Unicorn.Core.Enums;
using System.Collections.Generic;

namespace Unicorn.Core.UI
{
	public interface IDynamicContentDialog
	{
		void Show();

		void Hide();

		string Title
		{
			get;
			set;
		}

		/// <summary>
		/// UIView in iOS
		/// View in Android
		/// </summary>
		/// <value>The content view.</value>
		object ContentView
		{
			get;
			set;
		}

		List<DialogButton> DialogButtons
		{
			get;
			set;
		}

		bool TouchOutsideToClose
		{
			get;
			set;
		}

		bool ShowTitle
		{
			get;
			set;
		}
	}

	public interface IDialogContent
	{
		string Content
		{
			get;
			set;
		}

		DialogIcon Icon
		{
			get;
			set;
		}
	}

	public class DialogButton
	{
		public string Text
		{
			get;
			set;
		}

		public Action OnClicked
		{
			get;
			set;
		}

		public bool CloseAfterClick
		{
			get;
			set;
		}

		public bool IsBold { get; set; }

		public DialogButton()
		{
			Text = string.Empty;
			OnClicked = null;
			CloseAfterClick = true;
			IsBold = false;
		}
	}
}

