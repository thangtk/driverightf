using System;

namespace Unicorn.Core.UI
{
	public interface ILoading
	{
//        void Show(string title = "Loading...", Action del = null);
//		void Hide();

		string Title { get; set; }
		string Message { get; set; }
		void Show();
		void Hide();
	}
}

