﻿using System;

namespace Unicorn.Core
{
	public interface IToast
	{
		string Content { get; set; }
		double Length { get; set; }
		void Show();
		void Hide();
		DialogPosition DialogPosition { get; set; }
	}

	public enum DialogPosition{
		Bottom,
		Center,
		Top,
	}
}

