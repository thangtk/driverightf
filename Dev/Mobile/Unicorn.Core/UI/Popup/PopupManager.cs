using System;
using Unicorn.Core.Enums;
using Unicorn.Core.UI;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Reactive.Linq;
using Unicorn.Core.Translation;

[assembly: Unicorn.Core.Dependency.Dependency(typeof(PopupManager))]
namespace Unicorn.Core.UI
{
	public class PopupManager
	{
		public ITranslator Translator {get {return DependencyService.Get<ITranslator>();}}
		private ILoading Loading
		{
			get
			{ 
				// DEBUG: sometimes cannot get ILoading instance
				try {
					var x = DependencyService.Get<ILoading> ();
					return x;
				} catch (Exception ex) {
					System.Diagnostics.Debug.WriteLine (">>> ERROR >>> " + ex.ToString ());
				}
				return null;
			}
		}

		/// <summary>
		/// Create more overload if necessary
		/// </summary>
		/// <param name="title">Title.</param>
		/// <param name="message">Message.</param>
		/// <param name="longRunning">Long running.</param>
		public void ShowLoading(string title, string message, Action longRunning = null)
		{
			Loading.Title = title;
			Loading.Message = message;
			if(longRunning != null)
			{
				Loading.Show();
				Task.Factory.StartNew(longRunning).ContinueWith(t => Loading.Hide()); // Hide method run on main thread itself
			}
			else
			{
				Loading.Show();
			}
		}

		public void ShowLoading()
		{
			Loading.Show();
		}

		public void HideLoading()
		{
			Loading.Hide();
		}

		public void ShowAlert(string title, string content, DialogIcon icon = DialogIcon.None, bool showTitle = false, Action callback = null, string submitText = "OK")
		{
			showTitle = !string.IsNullOrEmpty(title);
			var dialog = DependencyService.Get<IDynamicContentDialog>(global::Unicorn.Core.Dependency.DependencyFetchTarget.NewInstance);
			dialog.ShowTitle = showTitle;
			dialog.ContentView = GetDialogContentView(content, icon);
			dialog.Title = title;
            dialog.DialogButtons = new List<DialogButton>() {
                new DialogButton () {
                    Text = submitText,
                    OnClicked = callback
                }
            };

			dialog.Show();

//			var dialog = DependencyService.Get<IDialog> (global::Unicorn.Core.Dependency.DependencyFetchTarget.NewInstance);
//			dialog.Title = title;
//			dialog.Content = content;
//			dialog.Icon = icon;
//			dialog.DialogButtons = new List<DialogButton>(){
//				new DialogButton(){
//					Text = submitText,
//					OnClicked = callback
//				}
//			};
//
//			dialog.Show ();
		}

		public void ShowConfirm(string title, string content, Action submitAction, Action cancelAction, string submitText = "OK", string cancelText = "Cancel", bool showTitle = false)
		{
			var dialog = DependencyService.Get<IDynamicContentDialog>(global::Unicorn.Core.Dependency.DependencyFetchTarget.NewInstance);
			dialog.ShowTitle = showTitle;
			dialog.Title = title;
			dialog.ContentView = GetDialogContentView(content, DialogIcon.None);
			dialog.DialogButtons = new List<DialogButton> () {
				new DialogButton () {
					Text = cancelText,
					OnClicked = cancelAction
				},
				new DialogButton () {
					Text = submitText,
					OnClicked = submitAction,
					IsBold = true
				}
			};

			dialog.Show();
		}

		/// <summary>
		/// Shows the dialog with many options.
		/// </summary>
		/// <param name="title">Title.</param>
		/// <param name="content">Content.</param>
		/// <param name="icon">Icon.</param>
		/// <param name="options">Options</param>
		public void ShowDialogManyOptions(string title, string content, DialogIcon icon, List<DialogButton> options, bool showTitle = true)
		{
			var dialog = DependencyService.Get<IDynamicContentDialog>(global::Unicorn.Core.Dependency.DependencyFetchTarget.NewInstance);
			dialog.ShowTitle = showTitle;
			dialog.ContentView = GetDialogContentView(content, icon);
			dialog.Title = title;
			dialog.DialogButtons = options;
			dialog.Show();
		}

		/// <summary>
		/// Show notification as Android toast
		/// </summary>
		/// <param name="content">Content.</param>
		/// <param name="length">Length.</param>
		public async void ShowToast(string content, ToastLength length)
		{
			int duration = (int)length;
			var toast = DependencyService.Get<IToast>(global::Unicorn.Core.Dependency.DependencyFetchTarget.NewInstance);
			toast.DialogPosition = DialogPosition.Bottom;
			toast.Content = content;
			toast.Show();
			await Task.Delay(duration);
			toast.Hide();
		}

		/// <summary>
		/// Shows the popup with custom content view
		/// </summary>
		/// <param name="title">Title.</param>
		/// <param name="contentView">Content view: UIView for iOS/ View for Android</param>
		/// <param name="options">Options.</param>
		/// <param name="touchOutsideToClose">If set to <c>true</c> touch outside to close.</param>
		/// <param name="showTitle">If set to <c>true</c> show title.</param>
		public void ShowPopup(string title, object contentView, List<DialogButton> options, bool touchOutsideToClose = false, bool showTitle = true)
		{
			var dialog = DependencyService.Get<IDynamicContentDialog>(global::Unicorn.Core.Dependency.DependencyFetchTarget.NewInstance);
			dialog.ShowTitle = showTitle;
			dialog.ContentView = contentView;
			dialog.TouchOutsideToClose = touchOutsideToClose;
			dialog.Title = title;

			dialog.DialogButtons = options;
			dialog.Show();
		}

		private IDialogContent GetDialogContentView(string content, DialogIcon icon)
		{
			var view = DependencyService.Get<IDialogContent>(global::Unicorn.Core.Dependency.DependencyFetchTarget.NewInstance);
			view.Icon = icon;
			view.Content = content;
			return view;
		}
	}

	/// <summary>
	/// Android  toast: 2 & 3.5
	/// </summary>
	public enum ToastLength
	{
		Short = 2000,
		Long = 3500,
	}
}

