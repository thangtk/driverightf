﻿using System;

namespace DriveRightF.Core
{
	public interface IUControl
	{
		string Title {get;set;}
		string Text {get;set;}

		bool IsEnable{get;set;}
	}
}

