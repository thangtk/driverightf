﻿using System;

namespace DriveRightF.Core
{
	public class ValidationFactory
	{
		public static Validation CreateValidation(ValidationType type)
		{
			Validation validation = new Validation ();
			switch (type) {
			case ValidationType.Mail:
				break;
			case ValidationType.PhoneNumber:
				validation.Rex = @"^([0-9]{8,12})$";
				validation.ErrorCode = "Must be 8 to 12 number";
				break;
			case ValidationType.Wechat:
				break;
			default:
				break;
			}
			return validation;

		}
	}
}

