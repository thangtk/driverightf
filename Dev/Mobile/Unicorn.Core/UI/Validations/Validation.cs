﻿using System;
using Unicorn.Core.Enums;

namespace DriveRightF.Core
{
	public class Validation
	{
		public ValidationType Type { get; set; }
		public string ErrorCode { get; set; }
		public string Rex { get; set; }
//		public KeyboardType KeyboardType { get; set; }

	}

	public enum ValidationType
	{
		PhoneNumber,
		Mail,
		Number,
		Text,
		Wechat,
		Required,
		Pattern
	}
}

