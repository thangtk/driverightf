﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace DriveRightF.Core
{
	public static class ValidationUtils
	{
//		public static void HandleValidate<T>(this T control) where T: IValidateControl, IUControl
//		{
//
//				if (!string.IsNullOrEmpty (control.Text)) {
//					HandleValidatePattern (control, control.Validation);
//				} 
//				else 
//				{
//					if (!control.IsRequired) {
//						control.Error = string.Empty;
//						control.IsValid = true;
//					} else {
//						control.Error = "Field is not empty";
//						control.IsValid = false;
//					}
//
//				}
//		}

		private static void HandleValidateRequired <T> (T control, Validation validation) where T: IValidate, IUControl
		{
			if (string.IsNullOrEmpty (control.Text)) {
				control.Error = validation.ErrorCode ?? "Field is not empty";
			}
		}

		private static void HandleValidationPattern<T> (T control, Validation validation) where T: IValidate, IUControl
		{
			if (validation == null)
				return;
			if (string.IsNullOrEmpty (validation.Rex))
				return;
			
			Match match = Regex.Match (control.Text, validation.Rex);
			if (!match.Success) {
				control.Error = validation.ErrorCode;
			}
			else {
				control.Error = string.Empty;
			}
		}

		private static void HandleValidation<T>(this T control, Validation validation) where T: IValidate, IUControl
		{
			if (validation == null) {
				return;
			}
			if (validation.Type == ValidationType.Required) {
				HandleValidateRequired (control, validation);
			} else if (validation.Type == ValidationType.Pattern) {
				HandleValidationPattern (control, validation);
			} else {
				Validation newValidation = ValidationFactory.CreateValidation (validation.Type);
				HandleValidationPattern (control, newValidation);
			}
		}

		private static bool IsValidControl<T>(this T control, Validation validation) where T: IValidate, IUControl
		{
			HandleValidation<T>(control,validation);
			return string.IsNullOrEmpty (control.Error);
		}

		public static void HandleValidations<T>(this T control, List<Validation> validations) where T: IValidate, IUControl
		{
			if (validations == null || validations.Count == 0)
				return;
			foreach (Validation validation in validations) {
				bool isValid = IsValidControl (control, validation);
				if (!isValid) {
					break;
				}
			}

		}
	}
}

