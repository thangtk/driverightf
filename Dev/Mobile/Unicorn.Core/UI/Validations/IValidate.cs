﻿using System;
using System.Collections.Generic;

namespace DriveRightF.Core
{
	public interface IValidate
	{
		bool IsValid { get;}
		string Error { get; set;}
//		bool IsRequired {get;set;}

		List<Validation> Validations { get; set;}
//		bool CheckValidate ();
	}
}

