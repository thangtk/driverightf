using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;


namespace Unicorn.Core.UI
{
    public abstract class UBaseActivity : Activity
    {
        public ViewGroup RootView
        {
            get
            {
                return (ViewGroup)Window.DecorView.RootView;
            }
        }
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            
            // Create your application here
        }
    }
}