﻿using System;
using Unicorn.Core.UI;
using Android.Content;
using DriveRight.Droid.Screens.CustomViews;
using Android.Widget;
using Android.Views;
using DriveRight.Core;
using DriveRightF.Core.Enums;

namespace DriveRight.Droid
{
	public class HomeScreen : BaseViewWrapper
	{
		private Context _context;
		public HomeScreen (Context ctx) : base(ctx)
		{
			_context = ctx;
			InitView ();
		}

		#region implemented abstract members of BaseViewWrapper

		protected override int GetLayoutId ()
		{
			return Resource.Layout.HomeScreen;
		}

		#endregion

		private void InitView(){
			_tabView = View.FindViewById<TabView> (Resource.Id.tabView);
		}

		private TabView _tabView;
		public override void OnAnimationEnd ()
		{
			_tabView.SelectItem (0);
		}
	}
}

