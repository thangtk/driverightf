﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Views.Animations;
using Android.Animation;
using DriveRight.Droid.Screens;
using DriveRight.Droid;
using Unicorn.Core;
using Unicorn.Core.UI;
using System.Threading.Tasks;
using Android.Graphics;
using DriveRight.Droid.Screens.Signup;
using Unicorn.Core.Navigator;
using DriveRightF.Core.Enums;
using Unicorn.Core.Translation;

[assembly: Unicorn.Core.Dependency.Dependency(typeof(MainActivity))]
namespace DriveRight.Droid
{
    [Activity(
        Label = "Drive Right",
        MainLauncher = true,
        Icon = "@drawable/icon_driveright",
        NoHistory = false,
        ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait,
        WindowSoftInputMode = SoftInput.AdjustResize,
        LaunchMode = Android.Content.PM.LaunchMode.SingleInstance
    )]
    public class MainActivity : UBaseActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            DependencyService.RegisterGlobalInstance<UBaseActivity>(this);
            this.RequestWindowFeature(WindowFeatures.NoTitle);
            this.Window.SetSoftInputMode(SoftInput.AdjustPan);
            SetContentView(Resource.Layout.Main);
            //RootView.AddView(new HeaderView("Hello", Resource.Drawable.icon_back, null, Resource.Drawable.icon_camera, null));
            //RootView.AddView(new TabItem());
			// Don't remove InitApp
			// Full Screen mode
//			this.Window.AddFlags (WindowManagerFlags.Fullscreen);
			// Init navigator
			InitNavigator ();
            //
//            var splashScreen = new SecondSignupScreen();
//            RootView.AddView(splashScreen);
			DependencyService.Get<INavigator>().Navigate((int) Screen.Home);
            //DependencyService.Get<PopupManager>().ShowAlert("Alert", "Run for your life!");

            //DependencyService.Get<PopupManager>().ShowConfirm("Header", "Từ khi tour du lịch mạo hiểm vào hang động lớn nhất thế giới được khai thác năm 2014, khoảng 500 khách nước ngoài cùng 70 người Việt Nam đã đặt chân vào kỳ quan này.", null, null);

            //DependencyService.Get<PopupManager>().ShowDialogManyOptions(
            //    "Dialog", "Choose options below.",
            //    Unicorn.Core.Enums.DialogIcon.None,
            //    new System.Collections.Generic.List<DialogButton>()
            //    {
            //        new DialogButton() {
            //            Text = "Cancel",
            //        },
            //        new DialogButton() {
            //            Text = "Resend",
            //            CloseAfterClick = false,
            //        },
            //        new DialogButton() {
            //            Text = "Continue"
            //        },
            //});

            //var linearLayout = new LinearLayout(this);
            //linearLayout.Orientation = Orientation.Vertical;


            //var textView = new EditText(this);
            //textView.Hint = "Hashtag";
            //textView.SetTextColor(Color.LawnGreen);
            //textView.SetTextSize(Android.Util.ComplexUnitType.Sp, 30);

            //var textView1 = new EditText(this);
            //textView1.Hint = "Chop Chop";
            //textView1.SetTextColor(Color.LightGreen);
            //textView1.SetTextSize(Android.Util.ComplexUnitType.Sp, 30);

            //linearLayout.AddView(textView);
            //linearLayout.AddView(textView1);

            //DependencyService.Get<PopupManager>().ShowPopup("Custom content view", linearLayout,
            //         new System.Collections.Generic.List<DialogButton>()
            //    {
            //        new DialogButton() {
            //            Text = "Cancel",
            //        },
            //        new DialogButton() {
            //            Text = "Resend",
            //            CloseAfterClick = false,
            //        },
            //        new DialogButton() {
            //            Text = "Continue"
            //        },
            //    }
            //);


            //DependencyService.Get<PopupManager>().ShowLoading();
            
            //DependencyService.Get<PopupManager>().ShowToast("I am Toast", Unicorn.Core.UI.ToastLength.Long);
        }

		private void InitNavigator(){
			// Init navigator
			var nav = DependencyService.Get<INavigator>() as BaseNavigator;
            nav.ContainerView = RootView;
            //nav.AddContainer (this.RootView);
		}

		public override void OnBackPressed ()
		{
			DependencyService.Get<INavigator> ().NavigateBack ();
		}

    }
}

