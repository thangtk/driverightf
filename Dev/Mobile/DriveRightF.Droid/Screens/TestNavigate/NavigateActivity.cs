﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Unicorn.Core.UI;
using Unicorn.Core;
using Java.Interop;
using Android.Transitions;
using DriveRight.Droid.Screens.TestNavigate;

namespace DriveRight.Droid.Screens
{
    [Activity(Label = "NavigateActivity", MainLauncher = true)]
    public class NavigateActivity : UBaseActivity
    {
        private TransitionManager mTransitionManager;
        private Scene mScene1;
        private Scene mScene2;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            DependencyService.RegisterGlobalInstance<UBaseActivity>(this);
            SetContentView(Resource.Layout.Navigator);
            ViewGroup viewRoot = FindViewById<ViewGroup>(Resource.Id.fragmentContent);

            var fragment = new TestFragment();
            //var fragment2 = new TestFragment2();
            this.FragmentManager.BeginTransaction().Add(Resource.Id.fragmentContent, fragment).Commit();
            //this.FragmentManager.BeginTransaction().Add(Resource.Id.fragment2, fragment2).Commit();

            TransitionInflater transitionInflater = TransitionInflater.From(this);
            mTransitionManager = transitionInflater.InflateTransitionManager(Resource.Transition.transition_manager, viewRoot);
            mScene1 = Scene.GetSceneForLayout(viewRoot, Resource.Layout.fragment_transition_scene_1, this);
            mScene2 = Scene.GetSceneForLayout(viewRoot, Resource.Layout.fragment_transition_scene_2, this);
        }

        [Export("goToScene1")]
        public void goToScene1(View v)
        {
            mTransitionManager.TransitionTo(mScene1);
        }

        [Export("goToScene2")]
        public void goToScene2(View v)
        {
            mTransitionManager.TransitionTo(mScene2);
        }
    }
}