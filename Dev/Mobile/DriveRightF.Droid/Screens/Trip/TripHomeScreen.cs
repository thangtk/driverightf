﻿using System;
using Unicorn.Core.UI;
using DriveRightF.Core.ViewModels;
using Android.Content;
using Android.Widget;
using DriveRight.Core;
using DriveRightF.Core.Enums;

namespace DriveRight.Droid
{
	public class TripHomeScreen : BaseViewWrapper<TripTileListViewModel>
	{
		public TripHomeScreen (Context ctx) : base (ctx)
		{
			InitView ();
		}

		private void InitView(){
			Button btnNext = View.FindViewById<Button> (Resource.Id.btnTripDetail);
			btnNext.Click += (sender, e) => {
				Navigator.Navigate((int)Screen.TripStatement);
			};
		}

		#region implemented abstract members of BaseViewWrapper

		protected override int GetLayoutId ()
		{
			return Resource.Layout.TripHomeScreen;
		}

		#endregion
	}
}

