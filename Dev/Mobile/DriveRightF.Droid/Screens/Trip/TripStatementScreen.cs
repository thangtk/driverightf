﻿using System;
using DriveRightF.Core.ViewModels;
using Unicorn.Core.UI;
using Android.Content;
using Android.Widget;
using DriveRight.Core;

namespace DriveRight.Droid
{
	public class TripStatementScreen : BaseViewWrapper<TripStatementViewModel>
	{
		public TripStatementScreen (Context ctx) : base (ctx)
		{
			InitView ();
		}

		private void InitView(){
			Button btnNext = View.FindViewById<Button> (Resource.Id.btnBackTripDetail);
			btnNext.Click += (sender, e) => {
				Navigator.NavigateBack();
			};
		}

		#region implemented abstract members of BaseViewWrapper

		protected override int GetLayoutId ()
		{
			return Resource.Layout.TripStatementScreen;
		}

		#endregion
	}
}

