﻿using System;
using Unicorn.Core.UI;
using DriveRightF.Core.ViewModels;
using DriveRight.Core.ViewModels;
using Android.Content;
using Android.Widget;
using DriveRight.Core;

namespace DriveRight.Droid
{
	public class AwardDetailScreen : BaseViewWrapper<AwardDetailViewModel>
	{
		public AwardDetailScreen (Context ctx) : base (ctx)
		{
			InitView ();
		}

		private void InitView(){
			Button btnNext = View.FindViewById<Button> (Resource.Id.btnBackAwardDetail);
			btnNext.Click += (sender, e) => {
				Navigator.NavigateBack();
			};
		}

		#region implemented abstract members of BaseViewWrapper

		protected override int GetLayoutId ()
		{
			return Resource.Layout.AwardDetailScreen;
		}

		#endregion
	}
}

