﻿using System;
using DriveRightF.Core.ViewModels;
using Unicorn.Core.UI;
using Android.Content;
using Android.Widget;
using DriveRight.Core;
using DriveRightF.Core.Enums;

namespace DriveRight.Droid
{
	public class AwardHomeScreen : BaseViewWrapper<AwardListViewModel>
	{
		public AwardHomeScreen (Context ctx) : base (ctx)
		{
			InitView ();
		}

		private void InitView(){
			Button btnNext = View.FindViewById<Button> (Resource.Id.btnAwardDetail);
			btnNext.Click += (sender, e) => {
				Navigator.Navigate((int)Screen.AwardDetail);
			};
		}

		#region implemented abstract members of BaseViewWrapper

		protected override int GetLayoutId ()
		{
			return Resource.Layout.AwardHomeScreen;
		}

		#endregion
	}
}

