using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using System.Net;

namespace DriveRight.Droid.Screens.Profile
{
    public class ProfileFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var url = "http://sms.latestsms.in/wp-content/uploads/facebook-profile-pictures14.jpg";
            var px = (int)TypedValue.ApplyDimension(ComplexUnitType.Dip, 120, this.Resources.DisplayMetrics);

            Bitmap _bimage = GetImageBitmapFromUrl(url);

            Bitmap _bfinal = GetRoundedCornerBitmap(_bimage);

            //ImageView imageview = FindViewById<ImageView>(Resource.Id.imgAvatar);

            //imageview.SetImageBitmap(_bfinal);

        }

        public Bitmap GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;
            if (!(url == "null"))
                using (var webClient = new WebClient())
                {
                    var imageBytes = webClient.DownloadData(url);
                    if (imageBytes != null && imageBytes.Length > 0)
                    {
                        imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                    }
                }

            System.Console.Out.WriteLine("Return fn");
            return imageBitmap;
        }


        private static Bitmap GetRoundedCornerBitmap(Bitmap bitmap)
        {
            int w = bitmap.Width;
            int h = bitmap.Height;


            int radius = Java.Lang.Math.Min(h / 2, w / 2);

            Bitmap output = Bitmap.CreateBitmap(w + 90, h, Bitmap.Config.Argb8888);

            Canvas canvas = new Canvas(output);
            Paint paint = new Paint();

            paint.AntiAlias = true;
            canvas.DrawARGB(0, 0, 0, 0);
            paint.SetStyle(Paint.Style.Fill);

            canvas.DrawCircle((w / 2) + 45, (h / 2), radius, paint);
            paint.SetXfermode(new PorterDuffXfermode(PorterDuff.Mode.SrcIn));
            canvas.DrawBitmap(bitmap, 45, 0, paint);

            paint.SetXfermode(null);
            paint.SetStyle(Paint.Style.Stroke);
            paint.Color = Color.Rgb(137, 192, 229);
            paint.StrokeWidth = 40;

            radius += (int)paint.StrokeWidth / 2;

            canvas.DrawCircle((w / 2) + 45, (h / 2), radius, paint);

            return output;
        }

    }
}