using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reactive.Linq;
using System.Reactive.Concurrency;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Unicorn.Core.UI;
using Unicorn.Core;
using DriveRightF.Core;
using ReactiveUI;
using DriveRight.Droid.Screens.CustomViews;
using Android.Views.InputMethods;

namespace DriveRight.Droid.Screens.Signup
{
    class SecondSignupScreen : BaseViewWrapper<SecondStepSignupViewModel>
    {
        private Context _context;
        private LayoutInflater _inflater;
        private EditText _tbxPlateNumber;
        private ImageButton _btnPhoto;
        private Button _btnNext;
        private Button _btnDecline;
        private TextView _txtDescription;
        private TextView _txtDescription1;

        private static UBaseActivity Activity
        {
            get
            {
                return DependencyService.Get<UBaseActivity>();
            }
        }

        

        public SecondSignupScreen(Context context) :
            base(context)
        {
            _context = context;
            Initialize();
        }

        public SecondSignupScreen() :
            base(Activity)
        {
            _context = Activity;
            Initialize();
        }

        private void Initialize()
        {
            ViewModel = new SecondStepSignupViewModel();
            InitView();
            CreateBindingAsync();
            InitBindCommands();
        }

        private void InitView()
        {
            _tbxPlateNumber = View.FindViewById<EditText>(Resource.Id.tbxPlateNumber);
            _btnPhoto = View.FindViewById<ImageButton>(Resource.Id.btnPhoto);
            _btnNext = View.FindViewById<Button>(Resource.Id.btnNext);
            _btnDecline = View.FindViewById<Button>(Resource.Id.btnDecline);
            _txtDescription = View.FindViewById<TextView>(Resource.Id.txtDescription);
            _txtDescription1 = View.FindViewById<TextView>(Resource.Id.txtDescription1);

            _btnNext.Text = ViewModel.TextNextButton;
            _btnDecline.Text = ViewModel.TextNoThanksButton;
            _txtDescription.Text = ViewModel.TextDescription;
            _txtDescription1.Text = ViewModel.TextDescription1;
        }

        private void CreateBindingAsync()
        {
            RxApp.TaskpoolScheduler.Schedule(() =>
            {
                InitBindings();
            });
        }

        private void InitBindCommands()
        {
            _btnNext.Click += _btnNext_Click;
            _btnDecline.Click += _btnDecline_Click;
            _btnPhoto.Click += _btnPhoto_Click;
            _tbxPlateNumber.TextChanged += _tbxPlateNumber_TextChanged;
            _tbxPlateNumber.FocusChange += _tbxPlateNumber_FocusChange;
        }

        void _tbxPlateNumber_FocusChange(object sender, View.FocusChangeEventArgs e)
        {
            var textbox = sender as EditText;
            if (!textbox.HasFocus)
            {
                InputMethodManager inputMethodManager = Activity.GetSystemService(Context.InputMethodService) as InputMethodManager;
                inputMethodManager.HideSoftInputFromWindow(textbox.WindowToken, HideSoftInputFlags.None);
            }
        }

        private void InitBindings()
        {
            ViewModel.WhenAnyValue(vm => vm.PlateNumber)
                .Where(x => x != null)
                .ObserveOn(RxApp.MainThreadScheduler)
                .Subscribe(x => _tbxPlateNumber.Text = x);
        }

        void _tbxPlateNumber_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            ViewModel.PlateNumber = _tbxPlateNumber.Text;
        }

        void _btnDecline_Click(object sender, EventArgs e)
        {
            ViewModel.SecondStepDeclineCommand.Execute(null);
        }

        void _btnPhoto_Click(object sender, EventArgs e)
        {
            ViewModel.GetPlateNumberByOCRCommand.Execute(null);
        }

        void _btnNext_Click(object sender, EventArgs e)
        {
            ViewModel.SecondStepNextCommand.Execute(null);
        }

        protected override int GetLayoutId()
        {
            return Resource.Layout.SecondSignupScreen;
        }
    }
}