using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Unicorn.Core.UI;
using Unicorn.Core;
using DriveRightF.Core.ViewModels;
using Android.Graphics;
using Android.Content.Res;
using Android.Views.InputMethods;
using Android.InputMethodServices;

namespace DriveRight.Droid.Screens.Signup
{
    class SignupScreen : BaseViewWrapper<SignupViewModel>
    {
        private Context _context;
        private EditText _tbxLogin;
        private Button _btnLogin;
        private TextView _txtSignUpNow;

        private static UBaseActivity Activity
        {
            get
            {
                return DependencyService.Get<UBaseActivity>();
            }
        }

        public SignupScreen(Context context)
            : base(context)
        {
            _context = context;
            Initialize();
        }

        public SignupScreen()
            : base(Activity)
        {
            _context = Activity;
            Initialize();
        }

        private void Initialize()
        {
            ViewModel = new SignupViewModel();
            InitView();
            InitBinding();
        }

        private void InitView()
        {
//            _inflater = (LayoutInflater)_context.GetSystemService(Context.LayoutInflaterService);
//            View rootView = _inflater.Inflate(Resource.Layout.SignupScreen, null);
//            View = rootView;

            _tbxLogin = View.FindViewById<EditText>(Resource.Id.tbxLogin);
            _btnLogin = View.FindViewById<Button>(Resource.Id.btnLogin);
            _txtSignUpNow = View.FindViewById<TextView>(Resource.Id.txtLoginLabel);

            _tbxLogin.Hint = ViewModel.TextBoxLoginPlaceholder;
            _btnLogin.Text = ViewModel.ButtonLoginTitle;
            _txtSignUpNow.Text = ViewModel.LabelSignUpNow;
        }

        private void InitBinding()
        {
            _btnLogin.Click += _btnLogin_Click;
            _tbxLogin.TextChanged += _tbxLogin_TextChanged;
            _tbxLogin.FocusChange += _tbxLogin_FocusChange;
        }

        void _tbxLogin_FocusChange(object sender, View.FocusChangeEventArgs e)
        {
            //var textbox = sender as EditText;
            //if (!textbox.HasFocus)
            //{
            //    InputMethodManager inputMethodManager = Activity.GetSystemService(Context.InputMethodService) as InputMethodManager;
            //    inputMethodManager.HideSoftInputFromWindow(textbox.WindowToken, HideSoftInputFlags.None);
            //}
        }

        void _tbxLogin_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            ViewModel.PhoneNumber = _tbxLogin.Text;
        }

        void _btnLogin_Click(object sender, EventArgs e)
        {
            var contentView = CreatePhonePopupContent(null);
            ViewModel.GetSMSCommand.Execute(contentView);
        }

        private View CreatePhonePopupContent(Action<string> textChanged)
        {
            var textboxLayout = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MatchParent, FrameLayout.LayoutParams.WrapContent);

            EditText textbox = new EditText(Activity);

            textbox.SetBackgroundColor(Color.Rgb(239, 239, 239));
            textbox.SetTextSize(ComplexUnitType.Sp, View.Resources.GetDimension(Resource.Dimension.dialog_text_size));
            textbox.SetTextColor(Color.Rgb(102, 102, 102));
            textbox.SetLines(1);
            textbox.FocusChange += textbox_FocusChange;
            textbox.InputType = Android.Text.InputTypes.ClassPhone;
            
            int padding = (int)View.Resources.GetDimension(Resource.Dimension.dialog_content_padding);
            textbox.SetPadding(padding, padding, 0, padding);

             textbox.TextChanged += (sender, e) =>
            {
                ViewModel.SMSCode = (sender as EditText).Text;
                if (textChanged != null)
                    textChanged.Invoke(ViewModel.SMSCode);
            };

            return textbox;
        }

        void textbox_FocusChange(object sender, View.FocusChangeEventArgs e)
        {
            var textbox = sender as EditText;
            if (!textbox.HasFocus)
            {
                InputMethodManager inputMethodManager = Activity.GetSystemService(Context.InputMethodService) as InputMethodManager;
                inputMethodManager.HideSoftInputFromWindow(textbox.WindowToken, HideSoftInputFlags.None);
            }
        }

        protected override int GetLayoutId()
        {
            return Resource.Layout.SignupScreen;
        }
    }
}