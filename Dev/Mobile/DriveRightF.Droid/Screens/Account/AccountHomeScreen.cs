﻿using System;
using Unicorn.Core.UI;
using DriveRightF.Core.ViewModels;
using Android.Content;

namespace DriveRight.Droid
{
	public class AccountHomeScreen : BaseViewWrapper<AccountListViewModel>
	{
		public AccountHomeScreen (Context ctx) : base (ctx)
		{
		}

		#region implemented abstract members of BaseViewWrapper

		protected override int GetLayoutId ()
		{
			return Resource.Layout.AccountHomeScreen;
		}

		#endregion
	}
}

