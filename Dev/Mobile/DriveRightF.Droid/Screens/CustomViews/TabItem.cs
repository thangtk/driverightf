using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Unicorn.Core.UI;
using Unicorn.Core;
using Android.Graphics;

namespace DriveRight.Droid.Screens.CustomViews
{
    class TabItem : RelativeLayout
    {
        private ImageView _imgIcon;
        private TextView _txtTitle;

        public string Title
        {
            get { return _txtTitle.Text; }
            set { _txtTitle.Text = value; }
        }

        public int DefaultImageResource { get; set; }

        public int SelectedImageResource { get; set; }

        private bool _selected;

        public bool Selected
        {
            get { return _selected; }
            set { 
                _selected = value;

                if (value)
                {
                    _txtTitle.SetTextColor(Color.White);
                    _imgIcon.SetImageResource(SelectedImageResource);
                }
                else
                {
                    _txtTitle.SetTextColor(Color.Black);
                    _imgIcon.SetImageResource(DefaultImageResource);
                }
            }
        }
        

        private static UBaseActivity Activity
        {
            get
            {
                return DependencyService.Get<UBaseActivity>();
            }
        }

        public TabItem()
            : base(Activity)
        {
            Initialize();
        }

        private void Initialize()
        {
            InitView();
        }

        private void InitView()
        {
            SetBackgroundColor(Color.Red);

            LayoutInflater inflater = (LayoutInflater)Activity.GetSystemService(Context.LayoutInflaterService);
            View rootView = inflater.Inflate(Resource.Layout.TabItem, null);
            var layout = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent, RelativeLayout.LayoutParams.WrapContent);
            layout.TopMargin = 100;
            this.AddViewInLayout(rootView, 0, layout);

            _imgIcon = rootView.FindViewById<ImageView>(Resource.Id.imgIcon);
            _txtTitle = rootView.FindViewById<TextView>(Resource.Id.txtTitle);

            _txtTitle.SetTextColor(Color.White);

            _imgIcon.SetImageResource(Resource.Drawable.icon_award_white);
            _txtTitle.Text = "Awards";
        }
    }
}