using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Graphics.Drawables.Shapes;
using Android.Animation;
using Android.Views.Animations;

namespace DriveRight.Droid.Screens.CustomViews
{
    class LoadingView : View
    {

        public int Progress { get; set; }

        public LoadingView(Context context)
            : base(context)
        {

            InitParams();
        }

        private void InitParams()
        {
            Progress = 0;

            ValueAnimator animator = ValueAnimator.OfInt(0, 539);
            animator.SetDuration(2000);
            animator.SetInterpolator(new DecelerateInterpolator());
            animator.RepeatCount = int.MaxValue;
            animator.RepeatMode = ValueAnimatorRepeatMode.Restart;

            animator.Update += (object sender, ValueAnimator.AnimatorUpdateEventArgs e) =>
            {
                int newValue = (int)e.Animation.AnimatedValue;

                Progress = newValue;
                Invalidate();
            };

            animator.AnimationRepeat += (sender, e) =>
            {
                PivotX = Width / 2;
                PivotY = Height / 2;
                Rotation -= 180;
            };

            animator.Start();
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);

            var paint = new Paint();
            paint.SetARGB(255, 200, 255, 0);
            paint.SetStyle(Paint.Style.Stroke);
            paint.StrokeWidth = 20;

            var bound = new RectF(Width / 2 - 50, Height / 2 - 50, Width / 2 + 50, Height / 2 + 50);
            var path = new Path();


            //float startRadius = Progress % 360;

            //float sweepRadius;

            //if (startRadius < 270)
            //{
            //    sweepRadius = startRadius - (270 * startRadius / 270 / 5);
            //    path.AddArc(bound, startRadius, -(sweepRadius > 15 ? sweepRadius : 15));
            //    Console.WriteLine("<<<<<" + startRadius + ">>>>>>> " + (-startRadius + (270 * startRadius / 270 / 10)));
            //}
            //else
            //{
            //    sweepRadius = 270 * 4 / 5;
            //    path.AddArc(bound, startRadius, -((1 - (startRadius - 270) / 90) * (sweepRadius - 15) + 15));
            //}

            float startRadius = Progress;

            float sweepRadius;

            if (startRadius < 360)
            {
                sweepRadius = startRadius * 9 / 10;
                path.AddArc(bound, startRadius, -(sweepRadius > 15 ? sweepRadius : 15));
                Console.WriteLine("<<<<<" + startRadius + ">>>>>>> " + (-startRadius + (270 * startRadius / 270 / 10)));
            }
            else
            {
                sweepRadius = (1 - (startRadius - 360) / 180) * (360 * 9 / 10);
                path.AddArc(bound, startRadius, -(sweepRadius > 15 ? sweepRadius : 15));
            }
            canvas.DrawPath(path, paint);
        }

    }
}