using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Unicorn.Core.UI;
using Unicorn.Core;
using Android.Graphics.Drawables;
using Unicorn.Core.Navigator;

namespace DriveRight.Droid.Screens.CustomViews
{
    class HeaderView : FrameLayout
    {
        private Context _context;
        private ImageButton _btnLeft;
        private ImageButton _btnRight;
        private TextView _txtTitle;

        public HeaderType HeaderType {
            set {
                if (value == HeaderType.BackingType)
                {
                    _btnLeft.Visibility = ViewStates.Visible;
                    _btnRight.Visibility = ViewStates.Invisible;
                    LeftButtonClicked = new Action(() =>
                    {
                        DependencyService.Get<INavigator>().NavigateBack();
                    });
                }
                else
                {
                    _btnLeft.Visibility = ViewStates.Visible;
                    _btnRight.Visibility = ViewStates.Visible;
                }
            }
        }

        public string Title {
            get { return _txtTitle.Text; }
            set { _txtTitle.Text = value; }
        }

        public int LeftImageResource {
            set { _btnLeft.SetBackgroundResource(value); }
        }

        public Action LeftButtonClicked { get; set; }

        public int RightImageResource
        {
            set { _btnRight.SetBackgroundResource(value); }
        }
        public Action RightButtonClicked { get; set; }

        public int MyProperty { get; set; }

        private static UBaseActivity Activity
        {
            get
            {
                return DependencyService.Get<UBaseActivity>();
            }
        }

        public HeaderView(string title = "")
            : base(Activity)
        {
            _context = Activity;
            Initialize();

            HeaderType = HeaderType.BackingType;
            Title = title;
        }

        public HeaderView(string title, int leftImageResource, Action leftButtonClicked, int rightImageResource, Action rightButtonClicked)
            : base(Activity)
        {
            _context = Activity;
            Initialize();

            HeaderType = HeaderType.NormalType;
            Title = title;
            LeftImageResource = leftImageResource;
            LeftButtonClicked = leftButtonClicked;
            RightImageResource = rightImageResource;
            RightButtonClicked = rightButtonClicked;
        }


        private void Initialize()
        {
            InitView();
            InitParams();
        }

        private void InitParams()
        {
            LeftImageResource = Resource.Drawable.icon_back;
            Title = "";
        }

        private void InitView()
        {
            var inflater = (LayoutInflater)_context.GetSystemService(Context.LayoutInflaterService);
            View rootView = inflater.Inflate(Resource.Layout.Header, null);
            AddView(rootView);


            _btnLeft = rootView.FindViewById<ImageButton>(Resource.Id.btnLeft);
            _btnRight = rootView.FindViewById<ImageButton>(Resource.Id.btnRight);
            _txtTitle = rootView.FindViewById<TextView>(Resource.Id.txtTitle);

            _btnLeft.Click += _btnLeft_Click;
            _btnRight.Click += _btnRight_Click;
        }
        void _btnLeft_Click(object sender, EventArgs e)
        {
            if (LeftButtonClicked != null)
                LeftButtonClicked.Invoke();
        }

        void _btnRight_Click(object sender, EventArgs e)
        {
            if (RightButtonClicked != null)
                RightButtonClicked.Invoke();
        }
    }

    public enum HeaderType
    {
        BackingType,
        NormalType
    }
}