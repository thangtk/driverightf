using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Unicorn.Core.UI;
using DriveRightF.Core.ViewModels;
using Unicorn.Core;
using Unicorn.Core.Translation;
using DriveRight.Core;

namespace DriveRight.Droid
{
    public class SplashScreen : BaseViewWrapper<SplashViewModel>
    {
        private Context _context;
        private TextView _txtDescription;

        private static UBaseActivity Activity
        {
            get
            {
                return DependencyService.Get<UBaseActivity>();
            }
        }

        public SplashScreen(Context context)
            : base(context)
        {
            _context = context;
            Initialize();

            ViewModel = new SplashViewModel();
            
        }

        private void Initialize()
        {
//            var _inflater = (LayoutInflater)_context.GetSystemService(Context.LayoutInflaterService);
//            View rootView = _inflater.Inflate(Resource.Layout.SplashScreen, null);
//            View = rootView;

            _txtDescription = View.FindViewById<TextView>(Resource.Id.txtDescriptionApp);
        }

        protected override int GetLayoutId()
        {
            return Resource.Layout.SplashScreen;
        }

		public override void OnAnimationEnd ()
		{
			base.OnAnimationEnd ();
			InitApp ();
			//
			ViewModel.LoadComponentCommand.Execute(null);
		}

		private void InitApp(){
			// Init something here
		}
    }
}