﻿using System;
using Unicorn.Core.Navigator;
using DriveRightF.Core.Enums;
using DriveRight.Droid.Screens.Signup;
using Android.Content;
using Unicorn.Core.UI;
using Unicorn.Core;

[assembly: Unicorn.Core.Dependency.Dependency (typeof(DriveRight.Droid.Navigator))]
namespace DriveRight.Droid
{
	public class Navigator : BaseNavigator
	{

		protected override INavigableScreen GetScreenByKey (Context ctx, int screenId)
		{
			INavigableScreen screenContent = null;
			Screen screen = (Screen)screenId;
			switch (screen) {
			case Screen.Splash:
				screenContent = new SplashScreen (ctx);
                screenContent.IsRoot = true;
				screenContent.NoCache = true;
                    //screenContent = new TestFragment1();
				break;

			case Screen.Signup:
				screenContent = new SignupScreen (ctx);
                screenContent.IsRoot = true;
				screenContent.NoCache = true;
                //screenContent = new TestFragment2();
				break;

			case Screen.SecondSignup:
				screenContent = new SecondSignupScreen (ctx);
                screenContent.IsRoot = true;
				screenContent.NoCache = true;
				break;

			case Screen.Home:
				screenContent = new HomeScreen (ctx);
				screenContent.IsRoot = true;
				break;

			case Screen.HomeAward:
				screenContent = new AwardHomeScreen (ctx);
				screenContent.IsRoot = true;
				break;

			case Screen.HomeTrips:
				screenContent = new TripHomeScreen (ctx);
				screenContent.IsRoot = true;
				break;

			case Screen.HomeAccount:
				screenContent = new AccountHomeScreen (ctx);
				screenContent.IsRoot = true;
				break;

			case Screen.TripStatement:
				screenContent = new TripStatementScreen (ctx);
				break;

			case Screen.AwardDetail:
				screenContent = new AwardDetailScreen (ctx);
				break;
			
			}
			//
			if (screen != Screen.None && screenContent == null) {
				throw new Exception ("DriveRight notice >>> Missing create Screen at Navigator class. Please add a new one to continue >>>");
			}
			//
			if (screenContent != null) {
				screenContent.Key = (int)screen;
				screenContent.Navigator = this;
			}
			return screenContent;
		}
	}
}

