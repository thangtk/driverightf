﻿using System;

namespace DriveRightF.Core
{
	public class AppConfig
	{
		/// <summary>
		/// true for release
		/// false for debug
		/// </summary>
		public const bool SDK_ENABLED = false;
	}
}

