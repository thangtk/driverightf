﻿using System;
using Android.Views;
using Android.Widget;
using Android.Content;
using Android.Util;
using Unicorn.Core.Navigator;
using Unicorn.Core;
using DriveRightF.Core.Enums;
using System.Collections.Generic;

namespace DriveRight.Core
{
	public class TabView : LinearLayout, ICanNavigate
	{
		#region ICanNavigate implementation

		public INavigator Navigator { get; set; }

		#endregion

		public ViewGroup TabContentView { get; set; }
		public LinearLayout TabItemsHolder { get; set; }
		private View _rootView;
		Context _context;
		// TEST
		private int _activatedIndex = 0;
		private List<TabItem> _tabItems = new List<TabItem> ();

		public TabView (Context ctx) : base(ctx)
		{
			Initialize (ctx);
		}

		public TabView (Context ctx, IAttributeSet attrs) : base (ctx, attrs){
			Initialize (ctx);
		}

		private void Initialize(Context ctx){
			_context = ctx;
			var inflater = (LayoutInflater)ctx.GetSystemService(Context.LayoutInflaterService);
			_rootView = inflater.Inflate(Resource.Layout.TabView, null);
			_rootView.LayoutParameters = new LinearLayout.LayoutParams (LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.MatchParent);
			this.AddView (_rootView);
			//
			InitView(_rootView);
			CreateTabItems();
		}

		private void InitTabNavigator(ViewGroup containerView){
			var x = DependencyService.Get<INavigator>(Unicorn.Core.Dependency.DependencyFetchTarget.NewInstance);
			var nav = (x as BaseNavigator);
			nav.ContainerView = containerView;
			nav.EnableBackToExitApp = false;
			nav.OnNavigated += (int obj) => {
				// TODO: update activated tab item screen
				_tabItems[_activatedIndex].Tag = obj;
			};
			this.Navigator = nav;
		}

		private void InitView (View view){
			// TODO: get controls
			TabContentView = FindViewById<ViewGroup>(Resource.Id.tabContentView);
			TabItemsHolder = FindViewById<LinearLayout>(Resource.Id.tabItemsHolder);
			//
			InitTabNavigator(TabContentView);
		}

		private void CreateTabItems(){
			// TODO: calculate width and height
			int itemWidth = 150;//this.Width / 3;
			int itemHeigt = 60;//TabItemsHolder.Height;
			//

			_tabItems = new List<TabItem> (3);
			for(int i = 1; i<=_tabItems.Capacity; i++){
				TabItem item = new TabItem (_context) {
					Navigator = this.Navigator
				};
				//
				if (i == 1) {
					item.RootScreen = (int)Screen.HomeAward;	
				} else if (i == 2) {
					item.RootScreen = (int)Screen.HomeTrips;	
				} else if (i == 3) {
					item.RootScreen = (int)Screen.HomeAccount;	
				}
				//
				item.Click += (sender, e) => {
					// Skip click to root
					_activatedIndex = _tabItems.IndexOf(item);
					int screenKey = (int) (sender as Button).Tag;
//					int key = item.Navigator.CurrentScreen == 0? item.RootScreen: item.Navigator.CurrentScreen;
					if(screenKey == 0){
						screenKey = item.RootScreen ;
					}
					(item.Navigator as BaseNavigator).NavigateTo(screenKey, parentIsCurrentScreen:false);
				};
				item.Text = "Item " + i.ToString ();
				_tabItems.Add (item);
				TabItemsHolder.AddView (item, itemWidth, itemHeigt);
			}
		}

		public void SelectItem(int index){
			_tabItems [index].PerformClick ();
		}
	}
}

