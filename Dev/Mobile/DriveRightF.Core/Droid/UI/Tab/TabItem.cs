﻿using System;
using Android.Widget;
using Unicorn.Core.Navigator;
using Android.Content;

namespace DriveRight.Core
{
	/// <summary>
	/// TEST only
	/// </summary>
	public class TabItem : Button, ICanNavigate
	{
		#region ICanNavigate implementation

		public INavigator Navigator { get; set; }

		#endregion

		public bool IsActivated { get; set; }
		public int RootScreen { get; set; }

		public TabItem (Context ctx) : base(ctx)
		{
		}
	}
}

