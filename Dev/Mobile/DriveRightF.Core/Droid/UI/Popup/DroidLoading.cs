using System;

using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using Android.Util;
using Android.Graphics;
using Android.Views.Animations;
using Unicorn.Core.UI;
using Unicorn.Core;
using DriveRight.Core;
using Android.Animation;

[assembly: Unicorn.Core.Dependency.Dependency(typeof(DroidLoading))]
namespace DriveRight.Core
{
    class DroidLoading : FrameLayout, ILoading
    {

        private RadialProgress _loadingView;
        private TextView _messageView;
        public int Progress { get; set; }

        private static UBaseActivity Activity
        {
            get
            {
                return DependencyService.Get<UBaseActivity>();
            }
        }

        public DroidLoading()
            : base(Activity)
        {
            InitParams();
            InitView();
        }

        private void InitParams()
        {
            Progress = 0;
        }

        private void InitView()
        {
            SetBackgroundColor(Color.Argb(10, 0, 0, 0));
            _loadingView = new RadialProgress();

            _messageView = new TextView(Activity);
            _messageView.Gravity = GravityFlags.Center;
            _messageView.SetPadding(0, 300, 0, 0);
            _messageView.SetTextColor(Color.Argb(255, 41, 160, 213));
            _messageView.SetTextSize(ComplexUnitType.Sp, (int)this.Resources.GetDimension(Resource.Dimension.alert_content_font));


            AddView(_loadingView);
            AddView(_messageView);
        }

        public string Title
        {
            get;
            set;
        }

        public string Message
        {
            get { return _messageView.Text; }
            set { _messageView.Text = value; }
        }

        public void Show()
        {
            ValueAnimator animator = ValueAnimator.OfInt(0, 719);
            animator.SetDuration(1500);
            animator.SetInterpolator(new LinearInterpolator());
            animator.RepeatCount = int.MaxValue;
            animator.RepeatMode = ValueAnimatorRepeatMode.Restart;

            animator.Update += (object sender, ValueAnimator.AnimatorUpdateEventArgs e) =>
            {
                int newValue = (int)e.Animation.AnimatedValue;

                _loadingView.Progress = newValue;
                _loadingView.Invalidate();
            };

            Activity.RootView.AddView(this);
            animator.Start();
        }

        public void Hide()
        {
            if (Parent != null)
                (Parent as ViewGroup).RemoveView(this);
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            return true;
        }

    }

    class RadialProgress : View
    {
        public int Progress { get; set; }

        private static UBaseActivity Activity
        {
            get
            {
                return DependencyService.Get<UBaseActivity>();
            }
        }

        public RadialProgress()
            : base(Activity)
        {
            InitParams();
        }

        private void InitParams()
        {
            Progress = 0;
        }

        protected override void OnDraw(Canvas canvas)
        {
            var bound = new RectF(Width / 2 - 50, Height / 2 - 50, Width / 2 + 50, Height / 2 + 50);



            int[] colors = { Color.Red, Color.Violet };
            float[] positions = { 0, 1 };
            var gradient = new LinearGradient(bound.Left, bound.CenterY(), bound.Right, bound.CenterY(), colors, positions, Shader.TileMode.Mirror);


            var paint = new Paint();
            paint.AntiAlias = true;
            paint.SetARGB(255, 41, 160, 213);
            paint.SetStyle(Paint.Style.Stroke);
            paint.SetShader(gradient);
            paint.StrokeWidth = 10;

            var path = new Path();

            //var layerPaint = new Paint();
            //layerPaint.AntiAlias = true;
            //layerPaint.SetARGB(255, 126, 186, 170);
            //layerPaint.SetStyle(Paint.Style.Stroke);
            //layerPaint.StrokeWidth = Progress / 18;

            //var layerPath = new Path();

            float startRadius = Progress;

            float sweepRadius;

            if (startRadius < 360)
            {
                sweepRadius = startRadius * 4 / 5;
                sweepRadius = -(sweepRadius > 30 ? sweepRadius : 30);
                path.AddArc(bound, startRadius, sweepRadius);
            }
            else
            {
                sweepRadius = (1 - (startRadius - 360) / 360) * (360 * 4 / 5);
                sweepRadius = -(sweepRadius > 30 ? sweepRadius : 30);
                path.AddArc(bound, startRadius, sweepRadius);
            }
            //layerPath.AddArc(bound, 0, 360);

            //canvas.DrawPath(layerPath, layerPaint);
            canvas.DrawPath(path, paint);
        }
    }
}