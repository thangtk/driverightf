using System;

using Android.Content;
using Android.Views;
using Android.Widget;
using Android.Util;
using DriveRight.Core;

namespace DriveRight.Core
{
    class DroidConfirm : FrameLayout
    {
        private Context _context;
        private TextView _txtHeader;
        private TextView _txtContent;
        private Button _btnOK;
        private Action _onClick;

        public String Content
        {
            get { return _txtContent.Text; }
            set { _txtContent.Text = value; }
        }

        public String Header
        {
            get { return _txtHeader.Text; }
            set { _txtHeader.Text = value; }
        }

        public DroidConfirm(Context context) :
            base(context)
        {
            _context = context;
            Initialize();
        }
        public DroidConfirm(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            _context = context;
            Initialize();
        }

        public DroidConfirm(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            _context = context;
            Initialize();
        }

        private void Initialize()
        {
            InitView();
        }

        private void InitView()
        {
            LayoutInflater inflater = (LayoutInflater)_context.GetSystemService(Context.LayoutInflaterService);
            View rootView = inflater.Inflate(Resource.Layout.DroidAlertLayout, null);

            _txtHeader = rootView.FindViewById<TextView>(Resource.Id.txtHeader);
            _txtContent = rootView.FindViewById<TextView>(Resource.Id.txtContent);
            _btnOK = rootView.FindViewById<Button>(Resource.Id.btnOk);
            AddView(rootView);


            _btnOK.Click += _btnOK_Click;
        }

        public void Show(string title, string content, Action submitAction, Action cancelAction, string submitText = "OK", string cancelText = "Cancel", bool showTitle = false)
        {
            _txtHeader.Text = title;
            _txtContent.Text = content;
            _onClick = submitAction;
        }

        public void Hide()
        {
            (Parent as ViewGroup).RemoveView(this);
        }

        void _btnOK_Click(object sender, EventArgs e)
        {
            Hide();
            if (_onClick != null)
            {
                _onClick.Invoke();
            }
        }
    }
}