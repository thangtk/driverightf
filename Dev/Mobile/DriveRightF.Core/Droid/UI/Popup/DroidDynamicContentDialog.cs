using System;
using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using Android.Util;
using Android.Graphics;
using Android.Views.Animations;
using Unicorn.Core;
using Unicorn.Core.UI;
using DriveRight.Core;

[assembly: Unicorn.Core.Dependency.Dependency(typeof(DroidDynamicContentDialog))]
namespace DriveRight.Core
{
    class DroidDynamicContentDialog : FrameLayout, IDynamicContentDialog
    {
        private Context _context;
        private LinearLayout _dialogView;
        private TextView _txtHeader;
        private FrameLayout _contentContainer;
        private LinearLayout _footer;
        private ScaleAnimation _scaleAnimation;

        private static UBaseActivity Activity
        {
            get
            {
                return DependencyService.Get<UBaseActivity>();
            }
        }

        private View _contentView;

        public object ContentView
        {
            get { return _contentView; }
            set
            {
                if (value != null)
                {
                    try
                    {
                        _contentView = value as View;
                    }
                    catch
                    {
                        throw new Exception("Content View must be a View");
                    }
                }
            }
        }
        

        public DroidDynamicContentDialog(Context context) :
            base(context)
        {
            _context = context;
            Initialize();
        }

        public DroidDynamicContentDialog() :
            base(Activity)
        {
            _context = Activity;
            Initialize();
        }

        public DroidDynamicContentDialog(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            _context = context;
            Initialize();
        }

        public DroidDynamicContentDialog(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            _context = context;
            Initialize();
        }

        private void Initialize()
        {
            InitView();
            InitParams();
        }

        private void InitParams()
        {
            Title = "Alert";
        }

        private void InitView()
        {
            LayoutInflater inflater = (LayoutInflater)_context.GetSystemService(Context.LayoutInflaterService);
            View rootView = inflater.Inflate(Resource.Layout.DroidDynamicContentDialog, null);

            _dialogView = rootView.FindViewById<LinearLayout>(Resource.Id.alertDialog);
            _txtHeader = rootView.FindViewById<TextView>(Resource.Id.txtHeader);
            _footer = rootView.FindViewById<LinearLayout>(Resource.Id.footer);
            _contentContainer = rootView.FindViewById<FrameLayout>(Resource.Id.alertContent);
            AddView(rootView);

            SetBackgroundColor(Color.Argb(100, 0, 0, 0));

            _scaleAnimation = new ScaleAnimation(0, 1, 0, 1, 300, 400);
            _scaleAnimation.Duration = 200;
            _scaleAnimation.Interpolator = new DecelerateInterpolator();
            _dialogView.Animation = _scaleAnimation;
            //_dialogView.Animate();
        }

        private void CreateViews()
        {
            //var testContent = new TextView(_context);
            //testContent.Text = "Hold tight, buddy.";
            _contentContainer.RemoveAllViews();
            _footer.RemoveAllViews();
            if (_contentView != null)
                _contentContainer.AddView(_contentView);

            LinearLayout.LayoutParams weightLayout = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MatchParent);
            weightLayout.Weight = 1;

            var weightView = new View(_context);
            weightView.LayoutParameters = weightLayout;
            _footer.AddView(weightView);

            float scale = _context.Resources.DisplayMetrics.Density;

            if (DialogButtons != null)
            {
                foreach (var dialogButton in DialogButtons)
                {
                    LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);
                    param.SetMargins((int)(2 * scale + 0.5), 0, (int)(2 * scale + 0.5), 0);

                    var button = new Button(_context);
                    button.LayoutParameters = param;
                    button.SetBackgroundResource(Resource.Drawable.alert_button);
                    button.SetTextColor(Color.Rgb(0, 153, 204));
                    button.SetTextSize(ComplexUnitType.Sp, Resources.GetDimension(Resource.Dimension.dialog_text_size));
                    button.Gravity = GravityFlags.Center;
                    button.Text = dialogButton.Text;
                    button.Click += (sender, e) => {

                        if (dialogButton.OnClicked != null)
                            dialogButton.OnClicked.Invoke();

                        if (dialogButton.CloseAfterClick)
                            Hide();

                    };
                    _footer.AddView(button);
                }
            }
        }

        public void Show()
        {
            CreateViews();
            Activity.RootView.AddView(this);
            _dialogView.Animate();
        }
        public void Hide()
        {
            _footer.RemoveAllViews();
            (Parent as ViewGroup).RemoveView(this);
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            return true;
        }


        public List<Unicorn.Core.UI.DialogButton> DialogButtons
        {
            get;
            set;
        }

        public bool TouchOutsideToClose
        {
            get;
            set;
        }

        public bool ShowTitle
        {
            get;
            set;
        }

        public string Title
        {
            get
            {
                return _txtHeader.Text;
            }

            set
            {
                _txtHeader.Text = value;
            }
        }
    }
}