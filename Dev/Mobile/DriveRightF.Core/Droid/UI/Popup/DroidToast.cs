using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Unicorn.Core;
using Android.Util;
using Unicorn.Core.UI;
using DriveRight.Core.Droid.UI.Popup;
using ReactiveUI;
using System.Reactive.Concurrency;

[assembly: Unicorn.Core.Dependency.Dependency(typeof(DroidToast))]
namespace DriveRight.Core.Droid.UI.Popup
{
    class DroidToast :  IToast
    {
        private Context _context;

        private static UBaseActivity Activity
        {
            get
            {
                return DependencyService.Get<UBaseActivity>();
            }
        }

        public Android.Widget.ToastLength ToastLength { get; set; }

        public DroidToast() 
        {
            _context = Activity;
            Initialize();
        }

        private void Initialize()
        {
            InitParams();
            InitView();
        }

        private void InitParams()
        {
            Content = "";
            ToastLength = Android.Widget.ToastLength.Short;
        }

        private void InitView()
        {
        }

        public void Show()
        {
            Toast.MakeText(Activity, Content, ToastLength).Show();
        }

        public string Content
        {
            get;
            set;
        }


        public double Length
        {
            get
            {
                return 0; 
            }
            set
            {
                if (value == 2000)
                    ToastLength = Android.Widget.ToastLength.Short;
                else
                    ToastLength = Android.Widget.ToastLength.Long;
            }
        }

        public void Hide()
        {
        }

        public DialogPosition DialogPosition
        {
            get;
            set;
        }
    }
}