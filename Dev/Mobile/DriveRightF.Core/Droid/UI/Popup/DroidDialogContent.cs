
using Android.Content;
using Android.Widget;
using Unicorn.Core.UI;
using Unicorn.Core;
using Android.Util;
using DriveRight.Core;

[assembly: Unicorn.Core.Dependency.Dependency(typeof(DroidDialogContent))]
namespace DriveRight.Core
{
    public class DroidDialogContent : TextView, IDialogContent
    {
        private Context _context;
        //private TextView _textContent;
        private static UBaseActivity Activity
        {
            get
            {
                return DependencyService.Get<UBaseActivity>();
            }
        }
        public string Content
        {
            get
            {
                return this.Text;
            }
            set
            {
                this.Text = value;
            }
        }

        public Unicorn.Core.Enums.DialogIcon Icon
        {
            get;
            set;
        }

          public DroidDialogContent() :
            base(Activity)
        {
            _context = Activity;
            Initialize();
        }

        public DroidDialogContent(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            _context = context;
            Initialize();
        }

        public DroidDialogContent(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            _context = context;
            Initialize();
        }

        private void Initialize()
        {
            //TextView _textContent = new TextView(_context);
            //FrameLayout.LayoutParams contentParameter = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MatchParent, FrameLayout.LayoutParams.MatchParent);
            //contentParameter.Gravity = GravityFlags.CenterHorizontal | GravityFlags.CenterVertical;

            //_textContent.LayoutParameters = contentParameter;

            //this.AddView(_textContent);
            SetTextSize(ComplexUnitType.Sp, Resources.GetDimension(Resource.Dimension.dialog_text_size));
            SetTextColor(new Android.Graphics.Color(190, 190, 190));
        }
    }
}