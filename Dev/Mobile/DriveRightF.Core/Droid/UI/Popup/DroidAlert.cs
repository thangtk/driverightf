using System;

using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using Android.Util;
using Unicorn.Core;
using Unicorn.Core.UI;
using DriveRight.Core;

//[assembly: Unicorn.Core.Dependency.Dependency(typeof(DroidLoading))]
namespace DriveRight.Core
{
    class DroidAlert : FrameLayout
    {
        private Context _context;
        private TextView _txtHeader;
        private TextView _txtContent;
        private Button _btnOK;
        private Action _onClick;

        private static UBaseActivity Activity
        {
            get
            {
                return DependencyService.Get<UBaseActivity>();
            }
        }

        public String Content 
        {
            get { return _txtContent.Text; }
            set { _txtContent.Text = value; }
        }

        public String Header
        {
            get { return _txtHeader.Text; }
            set { _txtHeader.Text = value; }
        }

        public DroidAlert(Context context) :
            base(context)
        {
            _context = context;
            Initialize();
        }

        public DroidAlert()
            : base(Activity)
        {
            _context = Activity;
            Initialize();
        }


        public DroidAlert(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            _context = context;
            Initialize();
        }

        public DroidAlert(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            _context = context;
            Initialize();
        }

        private void Initialize()
        {
            InitView();
        }

        private void InitView()
        {
            LayoutInflater inflater = (LayoutInflater)_context.GetSystemService(Context.LayoutInflaterService);
            View rootView = inflater.Inflate(Resource.Layout.DroidAlertLayout, null);

            _txtHeader = rootView.FindViewById<TextView>(Resource.Id.txtHeader);
            _txtContent = rootView.FindViewById<TextView>(Resource.Id.txtContent);
            _btnOK = rootView.FindViewById<Button>(Resource.Id.btnOk);
            AddView(rootView);


            _btnOK.Click += _btnOK_Click;
        }

        public void Show(string title, string content, Action callback = null, string submitText = "OK")
        {
            _txtHeader.Text = title;
            _txtContent.Text = content;
            _btnOK.Text = submitText;
            _onClick = callback;
            Activity.RootView.AddView(this);
        }

        public void Hide()
        {
            (Parent as ViewGroup).RemoveView(this);
        }

        void _btnOK_Click(object sender, EventArgs e)
        {
            Hide();
            if (_onClick != null)
            {
                _onClick.Invoke();
            }
        }
    }
}