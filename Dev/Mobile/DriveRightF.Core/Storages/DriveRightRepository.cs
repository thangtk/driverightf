using System.Collections.Generic;
using Unicorn.Core.Storages;
using DriveRightF.Core.Models;

[assembly: Unicorn.Core.Dependency.Dependency (typeof (DriveRightF.Core.Storages.DriveRightRepository))]
namespace DriveRightF.Core.Storages
{
	/// <summary>
	/// Repository for the app
	/// </summary>
	public class DriveRightRepository : SQLiteRepository
	{
		public DriveRightRepository()
			: base()
		{

			try {
				_db.CreateTable<Account> ();
				_db.CreateTable<Config> ();
				_db.CreateTable<Award> ();
				_db.CreateTable<Notification> ();
				_db.CreateTable<Profile> ();
				_db.CreateTable<TripMonth> ();
				_db.CreateTable<Trip> ();
				_db.CreateTable<TripSummary> ();
				_db.CreateTable<AccountTransaction> ();
				_db.CreateTable<GPS> ();
				// -----------------
//				_db.CreateTable<Config> ();
//				Transaction trans = new Transaction(){
//					Description="aaaaa",
//					StartTime = System.DateTime.Now
//
//				};

//				Save(trans);
				//_db.CreateTable<User>();


//				_db.CreateTable<Perk> ();
//				_db.CreateTable<Sponsor> ();
//				_db.CreateTable<Trip> ();
//				_db.CreateTable<Earning> ();
//				_db.CreateTable<CarRegistration> ();
//				_db.CreateTable<Insurance> ();
//				_db.CreateTable<DriverLicense> ();
//				_db.CreateTable<AvailablePerk> ();



			} catch (System.Exception ex) {
				System.Diagnostics.Debug.WriteLine (ex.ToString ());
			}
//			_db.CreateTable<AppInfo>();
//			_db.CreateTable<LogInfo>();
//
//			_db.CreateTable<Trip>();
//			_db.CreateTable<TripSyncQueue>();
//
//			_db.CreateTable<TripDataPart>();
//			_db.CreateTable<Award>();
//			_db.CreateTable<Theme>();
//
//
//			_db.CreateTable<Compass>();
//			_db.CreateTable<GPS>();
//			_db.CreateTable<Proximity>();
//			_db.CreateTable<Gyroscope>();
//			_db.CreateTable<Settings>();
//			_db.CreateTable<Options>();
//
//			_db.CreateTable<Province>();
//
//			_db.CreateTable<Insurer>();
//			_db.CreateTable<CarMake>();
//			_db.CreateTable<CarModel>();
//			_db.CreateTable<Dealer>();
//			_db.CreateTable<Garage>();
//			_db.CreateTable<InsurerProvinceMapping>();
//			_db.CreateTable<DealerProvinceMapping>();
//			_db.CreateTable<GarageProvinceMapping>();
//
//			_db.CreateTable<MonthSummaryTripData>();
//
//			_db.CreateTable<PhoneUsageState>();
//            //_db.CreateTable<AllSensorData>();
//			// Add more tables here
		}

		public override string RepositoryName
		{
			get { return DriveRightF.Core.Constants.ConfigKey.DATABASE_NAME; }
		}

		public override string RepositoryVersion
		{
			get { return DriveRightF.Core.Constants.ConfigKey.DATABASE_VERSION; }
		}

		public override string GetContent()
		{
			return string.Empty;
		}
	}
}