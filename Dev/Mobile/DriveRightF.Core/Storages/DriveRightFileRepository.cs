﻿using Contemi.Storages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DriveRight.Core.Storages
{
    public class DriveRightFileRepository : FileRepository
    {
        public DriveRightFileRepository() : base()
        {
        }

        public override string RepositoryName
        {
            get { return "DriveRight.txt"; }
        }

        public override string RepositoryVersion
        {
            get { return "1.0"; }
        }
    }
}
