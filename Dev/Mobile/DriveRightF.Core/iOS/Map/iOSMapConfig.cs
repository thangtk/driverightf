using System;
using DriveRight.Core;

namespace DriveRight.Core.iOS
{
    public class iOSMapConfig : BaseMapConfig
    {
        public int MapPage
        {
            get;
            set;
        }

        public MapType MapType { get; set; }
        public string FilePath
        {
            get
            {
                return GetFilePath();
            }
        }
        public iOSMapConfig()
        {
            MapType = MapType.Baidu;
        }
        private string GetFilePath()
        {

//            switch (MapPage)
//            {
//                case (int) AppScreen.GoMap:
//                    return GetFilePathGoMap();
//                    break;
//                default:
                    return GetFilePathMain();
//                    break;
//            }
        }

        private string GetFilePathMain()
        {
//            switch (MapType)
//            {
//                case MapType.Baidu:
                    return @"Content/baiduMap.html";
//                case MapType.Google:
//                    return @"Content/googleMap.html";
//                case MapType.Nokia:
//                    return @"Content/nokiaMap.html";
//                default:
//                    return @"Content/googleMap.html";
//            }
        }

        private string GetFilePathGoMap()
        {
//            switch (MapType)
//            {
//                case MapType.Baidu:
			return @"Content/GoMap/goMapBaidu.html";
//                case MapType.Google:
//                    return @"Content/GoMap/go_map_baidu.html";
//                case MapType.Nokia:
//                    return @"Content/GoMap/go_map_baidu.html";
//                default:
//                    return @"Content/GoMap/go_map_baidu.html";
//            }
        }
    }
}

