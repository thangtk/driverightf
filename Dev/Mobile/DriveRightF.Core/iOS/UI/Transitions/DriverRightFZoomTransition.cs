﻿using System;
using Unicorn.Core.Navigator;
using CoreGraphics;
using CoreAnimation;
using Foundation;
using UIKit;

namespace DriveRightF.Core
{
	public class DriverRightFZoomTransition:UDualTransition
	{
		private CGRect _targetRect, _sourceRect;
		private UIView _backView;
		//private 
		public DriverRightFZoomTransition(TimeSpan duration, UTransitionOrientation orien,CGRect targetRect,CGRect sourceRect,UIView backView):base(duration){
			_targetRect = targetRect;
			_sourceRect = sourceRect;
			_backView = backView;

			CAKeyFrameAnimation inTrans = CAKeyFrameAnimation.FromKeyPath ("zPosition");
			inTrans.Values = new NSNumber[]{NSNumber.FromFloat(0.0f),NSNumber.FromFloat(2000f),NSNumber.FromFloat(0.0f)};;
			CABasicAnimation zoomAnimation = CABasicAnimation.FromKeyPath ("transform"); //animationWithKeyPath:@"transform"];
			CATransform3D transform = CATransform3D.Identity;
			CGPoint sourceCenter = RectCenter(sourceRect);
			CGPoint targetCenter = RectCenter(targetRect);
			transform = transform.Translate(sourceCenter.X - targetCenter.X, sourceCenter.Y - targetCenter.Y, 0.0f);
			transform = transform.Scale( sourceRect.Width/targetRect.Width, sourceRect.Height/targetRect.Height, 1.0f);
			zoomAnimation.From = NSValue.FromCATransform3D(transform);
			zoomAnimation.To = NSValue.FromCATransform3D(CATransform3D.Identity);

			CABasicAnimation outAnimation = CABasicAnimation.FromKeyPath ("zPosition");
			outAnimation.From = NSNumber.FromFloat( -0.001f);
			outAnimation.To = NSNumber.FromFloat(-0.001f);
			outAnimation.Duration = duration.TotalSeconds;

			CAAnimationGroup inGroup = CAAnimationGroup.CreateAnimation ();
			inGroup.Animations = new CAAnimation[]{inTrans,zoomAnimation };
			inGroup.Duration = duration.TotalSeconds;
			InAnimation = inGroup;
			OutAnimation = outAnimation;
			FinishInit ();

		}

		private CGPoint RectCenter(CGRect rect) {
			return new CGPoint(rect.X + rect.Width/2.0f, rect.Y + rect.Height/2.0f);
		}

		public override UTransition GetReverseTransition (){

			UDualTransition reserve = new UDualTransition (Duration);
			if (_backView.Superview != null) {
				_sourceRect = _backView.ConvertRectToView(_backView.Bounds,null);
			}
			CABasicAnimation zoomAnimation = CABasicAnimation.FromKeyPath ("transform"); //animationWithKeyPath:@"transform"];
			CATransform3D transform = CATransform3D.Identity;
			CGPoint sourceCenter = RectCenter(_sourceRect);
			CGPoint targetCenter = RectCenter(_targetRect);
			transform = transform.Translate(sourceCenter.X - targetCenter.X, sourceCenter.Y - targetCenter.Y, 0.0f);
			transform = transform.Scale( _sourceRect.Width/_targetRect.Width, _sourceRect.Height/_targetRect.Height, 1.0f);
			zoomAnimation.To = NSValue.FromCATransform3D(transform);// valueWithCATransform3D:transform];
			zoomAnimation.From = NSValue.FromCATransform3D(CATransform3D.Identity);//valueWithCATransform3D:CATransform3DIdentity];
			zoomAnimation.Duration = Duration.TotalSeconds;
			//
			CABasicAnimation outAnimation = CABasicAnimation.FromKeyPath ("zPosition");// animationWithKeyPath:@"zPosition"];
			outAnimation.From = NSNumber.FromFloat( -0.001f);
			outAnimation.To = NSNumber.FromFloat(-0.001f);
			outAnimation.Duration = Duration.TotalSeconds;
			reserve.OutAnimation = zoomAnimation;
			reserve.InAnimation = outAnimation;
			reserve.FinishInit ();
			return reserve;
		}
	}

	/// <summary>
	/// Driver right cell zoom transition. Just for Ky.
	/// </summary>

	public class DriverRightCellZoomTransition: UZoomTransition
	{
		private UIView _backView;
		//private 
		public DriverRightCellZoomTransition(TimeSpan duration, UTransitionOrientation orien,CGRect targetRect,CGRect sourceRect,UIView backView):base(duration,orien,targetRect, sourceRect){
			_backView = backView;
		}

		public override UTransition GetReverseTransition (){

			//TODO: update sourceRect frame when backview change frame.
			if (_backView != null && _backView.Superview != null) {

				//TODO: ConvertRectToView of BackView not working:
				//_backView.ConvertRectToView(_backView.Bounds,null) not working!
				//Need convert to windown and check some constanst value.
				CGRect rect = _backView.Superview.ConvertRectToView(_backView.Frame,UIApplication.SharedApplication.KeyWindow);
				nfloat heightHeader = 60;
				nfloat ratio = _backView.Frame.Height / rect.Height;
				_sourceRect = new CGRect (rect.X * ratio, rect.Y * ratio + heightHeader , rect.Width * ratio, rect.Height * ratio);
			}

			return base.GetReverseTransition();
		}
	}
}

