﻿using System;
using Unicorn.Core.UI;
using ReactiveUI;
using UIKit;
using Foundation;
using CoreGraphics;
using Unicorn.Core.Translation;
using Unicorn.Core;

namespace DriveRightF.Core
{
	public interface IDriveRightTableViewCell<TModel>
	{
		void UpdateCell (TModel item, int index, int count);
		void Initialize();
	}

	public abstract  class DriveRightTableViewCell<TViewModel> : UBaseTableViewCell<TViewModel>, IDriveRightTableViewCell<TViewModel> where TViewModel: ReactiveObject
	{

		#region DriveRightTableViewCell implementation

		public  abstract void UpdateCell (TViewModel item, int index, int count);

		public  abstract void Initialize ();

		#endregion
		public ITranslator Translator {get {return DependencyService.Get<ITranslator>();}}

		protected DriveRightTableViewCell (IntPtr handle):base(handle)
		{

		}

		protected DriveRightTableViewCell ():base()
		{

		}

		protected DriveRightTableViewCell (NSCoder c):base(c)
		{

		}

		protected DriveRightTableViewCell (CGRect size):base(size)
		{

		}

		protected DriveRightTableViewCell (NSObjectFlag f):base(f)
		{

		}

		protected DriveRightTableViewCell (UITableViewCellStyle style, NSString reuseIdentifier) : base (style, reuseIdentifier){

		}
	}


	public abstract  class DriveRightTableViewCell<TModel, TViewModel> : UBaseTableViewCell<TViewModel>, IDriveRightTableViewCell<TModel> where TViewModel: ReactiveObject
	{

		#region DriveRightTableViewCell implementation

		public  abstract void UpdateCell (TModel item, int index, int count);

		public  abstract void Initialize ();

		#endregion
		public ITranslator Translator {get {return DependencyService.Get<ITranslator>();}}

		protected DriveRightTableViewCell (IntPtr handle):base(handle)
		{

		}

		protected DriveRightTableViewCell ():base()
		{

		}

		protected DriveRightTableViewCell (NSCoder c):base(c)
		{

		}

		protected DriveRightTableViewCell (CGRect size):base(size)
		{

		}

		protected DriveRightTableViewCell (NSObjectFlag f):base(f)
		{

		}

		protected DriveRightTableViewCell (UITableViewCellStyle style, NSString reuseIdentifier) : base (style, reuseIdentifier){

		}
	}
}

