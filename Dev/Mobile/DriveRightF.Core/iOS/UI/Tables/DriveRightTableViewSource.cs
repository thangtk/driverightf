﻿using System;
using UIKit;
using System.Collections.Generic;
using Foundation;
using ObjCRuntime;
using DriveRightF.Core.ViewModels;
using System.Reflection;
using DriveRightF.Core.iOS;

namespace DriveRightF.Core
{
	public class DriveRightTableViewSource<TModel, TCell> : DRTableViewSource<TModel> where TCell : UITableViewCell, IDriveRightTableViewCell<TModel>
	{
		//		private List<TModel> _items = new List<TModel>();
		//
		//		public List<TModel> Items {
		//			get {
		//				return _items;
		//			}
		//			set {
		//				_items = value;
		//			}
		//		}

		private bool _animationEnabled = false;

		public virtual bool AnimationEnabled {
			get { return _animationEnabled; }
			set { _animationEnabled = value; }
		}

		public DriveRightTableViewSource ()
		{
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (typeof(TCell).Name) as TCell;
			if (cell == null) {
				var views = NSBundle.MainBundle.LoadNib (typeof(TCell).Name, tableView, null);
				cell = Runtime.GetNSObject (views.ValueAt (0)) as TCell;

				cell.Initialize ();

				if (_animationEnabled)
					AnimationUtil.Amimate (cell, AnimationStyle.LeftToRight, indexPath.Row * 0.15f);
			}

			int index = indexPath.Row;
			if (Items != null && Items.Count > index) {
				cell.UpdateCell (Items [index], index, Items.Count);
			}
			return cell;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return Items.Count;
		}
	}

	public abstract class DRTableViewSource<TModel> : UITableViewSource
	{
		private List<TModel> _items = new List<TModel>();

		public List<TModel> Items {
			get {
				return _items;
			}
			set {
				_items = value;
			}
		}
	}
}

