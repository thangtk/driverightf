﻿using System;
using UIKit;
using Foundation;
using ObjCRuntime;
using System.Drawing;
using CoreGraphics;
using Unicorn.Core.iOS;
using Unicorn.Core;
using DriveRightF.Core.iOS;
using DriveRightF.iOS;
using Unicorn.Core.UI;
using ReactiveUI;
using System.Reactive.Linq;

namespace DriveRightF.Core
{
	[Register("DriveRightDatePicker")]
	public partial class DriveRightDatePicker : UBaseView<UDatePickerViewModel>, IUDatePicker
	{
		private UIView _rootView;
		private UIDatePicker _picker;
		private UITextView _overlay;
		private UIToolbar _toolBar;

		private string _error = string.Empty;
		private string _title = string.Empty;
		private string _text = string.Empty;
		private bool _isEnable = true;
		private bool _isValid = true;
		private bool _isSecurity = false;

		public event EventHandler<string> ValueChanged ;

		#region IUDatePicker implementation

		public bool CheckValidate ()
		{
			if (Validation != null) {
				this.HandleValidate ();
				return IsValid;
			}
			return true;
		}

		public bool IsRequired {
			get;
			set;
		}

		public Validation Validation {
			get;
			set;
		}
			

		public string Error {
			get {
				return _error;
			}
			set {
				_error = value;
				SetError ();
			}
		}

		public string Title {
			get {
				return _title;
			}
			set {
				_title = value;
				SetTitle ();

			}
		}

		public bool IsValid {
			get {
				return _isValid;
			}
			set {
				_isValid = value;
				InvokeOnMainThread (() => {
					lblError.Hidden = _isValid;
					UpdateFrame();
					ResizeView();
				});
			}
		}

		public string Text {
			get {
				return _text;
			}
			set {
				_text = value;
				InvokeOnMainThread (() => {
					txtInput.Text = _text;
					if (_picker != null) {
						DateTime date;
						DateTime.TryParse (value, out date);
						_picker.Date = date.DateTimeToNSDate ();
						if (ValueChanged != null)
						{
							ValueChanged (this, _text);
						}
					}
				});
			}
		}


		public bool IsEnable {
			get {
				return _isEnable;
			}
			set {
				_isEnable = value;
				txtInput.Enabled = _isEnable;
			}
		}

		public bool IsSecurity {
			get {
				return _isSecurity;
			}
			set {
				_isSecurity = value;
				InvokeOnMainThread (delegate {
					txtInput.SecureTextEntry = value;
				});
			}
		}

		#endregion

		public DriveRightDatePicker(IntPtr handle) : base(handle)
		{
			InitializeView();
		}

		public DriveRightDatePicker()
		{
			InitializeView();
		}

		public override CoreGraphics.CGRect Frame {
			get {
				return base.Frame;
			}
			set {
				base.Frame = value;
				UpdateFrame ();
				ResizeView ();
			}
		}

		private void InitializeView()
		{
			LoadNib ();
			InitControls ();
			InitBindings ();
			InitCustomization();
		}

		private void InitBindings()
		{
			this.WhenAnyValue (v => v.ViewModel)
				.Where (vModel => vModel != null)
				.ObserveOn(RxApp.TaskpoolScheduler)
				.Subscribe (vM => HandleBindings ());
		}

		private void HandleBindings()
		{
			ViewModel.ControlView = this;

			this.Bind (ViewModel, vm => vm.Text, v => v.Text);

			ViewModel
				.WhenAnyValue (x => x.Error)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (error => {
					Error = error;
				});
			ViewModel
				.WhenAnyValue (x => x.Title)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (title => {
					Title = title;
				});

			ViewModel
				.WhenAnyValue (x => x.Validation)
				.Where (x => x != null)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (validation => {
				Validation = validation;
			});

		}

		private void InitPickerView ()
		{
			_picker = new UIDatePicker ();
			_picker.Mode = UIDatePickerMode.Date;
			_picker.ValueChanged += (object sender, EventArgs e) =>  {
				Text = _picker.Date.NSDateToDateTime ().ToString ("yyyy-MM-dd");
			};
			_picker.MinimumDate = new DateTime (1900, 1, 1).DateTimeToNSDate ();
			_picker.MaximumDate = DateTime.Now.DateTimeToNSDate ();
		}

		private void InitToolbar ()
		{
			 _toolBar = new UIToolbar ();
			_toolBar.BarStyle = UIBarStyle.Black;
			_toolBar.Translucent = true;
			_toolBar.SizeToFit ();
			// Create a 'done' button for the toolbar and add it to the toolbar
			UIBarButtonItem doneButton = new UIBarButtonItem ("DONE", UIBarButtonItemStyle.Done, (s, e) =>  {
				_overlay.ResignFirstResponder ();
				//Text = _picker.Date.NSDateToDateTime ().ToString ("yyyy-MM-dd");
			});
			_toolBar.SetItems (new UIBarButtonItem[] {
				new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace),
				doneButton
			}, true);
		}

		private void InitOverlayView ()
		{
			_overlay = new UITextView ();
			_overlay.UserInteractionEnabled = true;
			_overlay.Font = UIFont.BoldSystemFontOfSize (0);
			_overlay.InputView = _picker;
			_overlay.InputAccessoryView = _toolBar;
			_overlay.BackgroundColor = UIColor.Clear;
			_overlay.Frame = new CGRect (txtInput.Frame.X, txtInput.Frame.Y, Frame.Width, Frame.Height);
			_overlay.ResignFirstResponder ();
			_overlay.TintColor = UIColor.Clear;
			_rootView.Add (_overlay);
		}

		private void InitControls()
		{
			InitPickerView ();
			InitToolbar ();
			InitOverlayView ();
		}

		private void InitCustomization()
		{
			//TODO: set background, font...

			txtInput.LeftView = new UIView (new CGRect (0, 0, 10, 10));
			txtInput.LeftViewMode = UITextFieldViewMode.Always;
			txtInput.Font = FontHub.RobotoLight13;
			txtInput.TintColor = txtInput.BackgroundColor;

			lblTitle.Lines = 0;
			lblTitle.LineBreakMode = UILineBreakMode.WordWrap;
			lblTitle.Font = FontHub.RobotoLight12;

			lblError.Font = FontHub.RobotoLight12;

			Error = string.Empty;
			Title = string.Empty;
			txtInput.Text = string.Empty;

			txtInput.EditingDidBegin += (object sender, EventArgs e) => 
			txtInput.ResignFirstResponder ();
		}

		private void LoadNib()
		{
			var nibObjects = NSBundle.MainBundle.LoadNib("DriveRightDatePicker", this, null);
			_rootView = Runtime.GetNSObject((nibObjects.ValueAt(0))) as UIView;
			AddSubview(_rootView);
		}

		private void SetError()
		{
			InvokeOnMainThread (() => {
				lblError.Text = _error;
				lblError.Frame = new CGRect(lblError.Frame.Location, new CGSize(txtInput.Frame.Width - 10f, lblError.Frame.Height));
				lblError.SizeToFit();
				lblError.Hidden = string.IsNullOrEmpty(_error);

				UpdateFrame();
				ResizeView();
			});
		}

		private void SetTitle()
		{
			InvokeOnMainThread (() => {
				lblTitle.Text = Title;
				lblTitle.Frame = new CGRect (lblTitle.Frame.Location, new CGSize (txtInput.Frame.X - 10f, lblTitle.Frame.Height));
				lblTitle.SizeToFit ();
			});
		}

		private void UpdateFrame()
		{
			nfloat heightFrame = (lblError.Hidden ? 0f : lblError.Frame.Height) + lblError.Frame.Y + txtInput.Frame.Y + 5f;
			nfloat totalHeightTitle = lblTitle.Frame.Height + lblTitle.Frame.Y + txtInput.Frame.Y;
			heightFrame = (nfloat) Math.Max (heightFrame, totalHeightTitle);
			nfloat offset = heightFrame - Frame.Height;
			base.Frame = new CGRect(Frame.Location, new CGSize(Frame.Width, heightFrame));

			//relocation views that beneath
			if (Superview != null) {
				foreach (var view in Superview.Subviews) {
					if (view.Frame.Y > Frame.Y) {
						var viewFrame = view.Frame;
						viewFrame.Y += offset;
						view.Frame = viewFrame;
					}
				}
			}
		}

		private void ResizeView()
		{
			if (_rootView != null)
			{
				_rootView.Frame = new CGRect(PointF.Empty, Frame.Size);
			}
		}	
	}
}

