﻿using System;
using UIKit;
using Foundation;
using ObjCRuntime;
using System.Collections.Generic;
using Unicorn.Core.iOS;

namespace DriveRightF.Core
{
	public abstract class BaseTabBar<T,TtabItem> : DriveRightBaseView<T>
		where T: UTabBarViewModel
		where TtabItem: UIView, ITabItem
	{
		protected List<TtabItem> TabItems { get; set;}
		protected nfloat WidthTabItem { get; set; }
		public event EventHandler<int> OnTabSelected;

		/// <summary>
		/// Gets the content view which contain TabItem
		/// </summary>
		/// <value>The content view.</value>
		public abstract UIView ContentView {
			get;
		}

		public BaseTabBar (IntPtr handle) : base (handle)
		{
		}

		public BaseTabBar () : base ()
		{
		}

		void HandleTabPosition ()
		{
			nfloat ratio = 1f / TabItems.Count;
			for (int i = 0; i < TabItems.Count; i++) {
				var item = TabItems [i];
				item.RemoveFromSuperview ();
				item.TranslatesAutoresizingMaskIntoConstraints = false;
				ContentView.Add (item);
				NSLayoutConstraint[] constraints =  {
					NSLayoutConstraint.Create (item, NSLayoutAttribute.Top, NSLayoutRelation.Equal, ContentView, NSLayoutAttribute.Top, 1, 0),
					NSLayoutConstraint.Create (item, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ContentView, NSLayoutAttribute.Width, ratio, 0),
					NSLayoutConstraint.Create (item, NSLayoutAttribute.Leading, NSLayoutRelation.Equal, ContentView, NSLayoutAttribute.Leading, 1, WidthTabItem * i),
					NSLayoutConstraint.Create (item, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, ContentView, NSLayoutAttribute.Bottom, 1, 0),
				};
				ContentView.AddConstraints (constraints);
			}
		}

		public virtual void AddTabItems(List<TtabItem> items)
		{
			TabItems = items;
			WidthTabItem = ContentView.Frame.Width / items.Count;

			HandleTabPosition ();
			HandleTabSelected();
		}

		private void HandleTabSelected()
		{
			for(int i = 0; i < TabItems.Count; i++)
			{
				var item = TabItems [i];
				item.ValueChanged += (object sender, string e) =>
				{
					int index = TabItems.FindIndex(x => x == item);
					SelectedTab(index);
					if(OnTabSelected != null)
					{
						OnTabSelected(sender, index);
					}
				};
			}
			;
		}

		private void SelectedTab(int index)
		{
			this.ViewModel.ItemSelected = index;
		}
	}
}

