﻿using System;
using UIKit;

namespace DriveRightF.Core
{
	public interface ITabItem
	{
		event EventHandler<string> ValueChanged;
	}

	public class BaseTabItem<T> : DriveRightBaseView<T>, ITabItem
		where T : UTabItemViewModel
	{
		#region ITabItem implementation

		public event EventHandler<string> ValueChanged;

		#endregion

		public BaseTabItem () : base()
		{
			InitControls ();
		}
		public BaseTabItem (IntPtr handle) : base(handle)
		{
			InitControls ();
		}

		public BaseTabItem (T viewModel) : base()
		{
			InitControls ();
		}

		private void InitControls ()
		{
			AddGestureRecognizer (new UITapGestureRecognizer (() => {
				if(ValueChanged != null)
				{
					if (ValueChanged != null) {
						string nameTab = ViewModel != null ? ViewModel.NameTab : string.Empty;
						ValueChanged(this,nameTab);
					}
				}
			}));
		}
	}
}

