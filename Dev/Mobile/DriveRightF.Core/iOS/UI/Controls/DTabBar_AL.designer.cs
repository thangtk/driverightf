// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.Core
{
	partial class DTabBar_AL
	{
		[Outlet]
		UIKit.UIView vBackGround { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (vBackGround != null) {
				vBackGround.Dispose ();
				vBackGround = null;
			}
		}
	}
}
