﻿using System;
using UIKit;
using System.Drawing;
using Foundation;
using CoreGraphics;

namespace DriveRightF.iOS
{
	[Register("DriveRightCropImage")] 
	public class DriveRightCropImage : UIImageView
	{
		private float _systemScale;

//		public RectangleF CurrentRectangle
//		{ 
//			get 
//			{ 
//				var frame = Frame;return new RectangleF ((float)frame.X*_systemScale,(float)frame.Y*_systemScale,(float)frame.Width*_systemScale,(float)frame.Height*_systemScale); 
//			}
//			set
//			{
//				var frame = Frame;frame.X = value.X/_systemScale;frame.Y = value.Y/_systemScale;
//				frame.Width = value.Width/_systemScale;frame.Height = value.Height/_systemScale;
//				Frame = frame;
//				if (RectangleChanged != null) {
//					RectangleChanged (this, value);
//				}
//				if (Image == null) {
//					return;
//				}
//				CurrentScale = value.Width / (float)Image.Size.Width;
//			}
//		}

		private RectangleF _currentRec = RectangleF.Empty;
		public RectangleF CurrentRectangle
		{ 
			get 
			{ 
				return _currentRec;
			}
			set
			{
				if (_currentRec.Equals (value)) {
					return;
				}
				_currentRec = value;
				if (RectangleChanged != null) {
					RectangleChanged (this, value);
				}
				if (Image == null) {
					return;
				}
				CurrentScale = value.Width / (float)Image.Size.Width;
			}
		}

		private float _currentScale;
		/// <summary>
		/// This properties only for set local
		/// </summary>
		public float CurrentScale
		{ 
			get 
			{ 
				return _currentScale;
			}
			set
			{
				if (_currentScale == value) {
					return;
				}
				_currentScale = value;
				if (ScaleChanged != null) {
					ScaleChanged (this,value);
				}
			}
		}

		public event EventHandler<RectangleF> RectangleChanged;
		public event EventHandler<float> ScaleChanged;


		public DriveRightCropImage(CGRect frame) : base (frame)
		{
			_systemScale = (float)UIScreen.MainScreen.Scale;
		}
		public DriveRightCropImage() : base ()
		{
			_systemScale = (float)UIScreen.MainScreen.Scale;
		}
		public DriveRightCropImage(IntPtr h) : base (h)
		{
			_systemScale = (float)UIScreen.MainScreen.Scale;
		}
	}
}

