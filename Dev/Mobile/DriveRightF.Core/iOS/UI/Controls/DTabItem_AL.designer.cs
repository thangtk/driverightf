// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.Core
{
	partial class DTabItem_AL
	{
		[Outlet]
		UIKit.UIImageView imgTab { get; set; }

		[Outlet]
		UIKit.UILabel lblName { get; set; }

		[Outlet]
		UIKit.UIView vBackGround { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (imgTab != null) {
				imgTab.Dispose ();
				imgTab = null;
			}

			if (lblName != null) {
				lblName.Dispose ();
				lblName = null;
			}

			if (vBackGround != null) {
				vBackGround.Dispose ();
				vBackGround = null;
			}
		}
	}
}
