﻿using System;
using Foundation;
using UIKit;
using ObjCRuntime;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using Unicorn.Core.iOS;

namespace DriveRightF.Core
{
	[Register ("DTabItem_AL")]
	public partial class DTabItem_AL : BaseTabItem<UTabItemViewModel>
	{
		private UIView _rootView;
		private UIView _vBackground;
		public UIView RootView { get { return _rootView; } }
		public string BackGroundImage { get; set;}

		public DTabItem_AL (IntPtr handle) : base (handle)
		{
			
			InitializeView ();
		}

		public DTabItem_AL (UTabItemViewModel vm, string background) : base ()
		{
//			ViewModel = vm;
			BackGroundImage=background;
			SetViewModel (vm);

			InitializeView ();
		}

		public void SetViewModel (UTabItemViewModel viewModel)
		{
			ViewModel = viewModel;
			if (ViewModel != null) {
				RxApp.TaskpoolScheduler.Schedule (HandleBinding);
			}
		}

		public DTabItem_AL () : base ()
		{
			InitializeView ();
		}

		public UIView ViewBackground {
			get {
				return _vBackground;
			}
			set {
				if (_vBackground != null) {
					_vBackground.RemoveFromSuperview ();
				}
				_vBackground = value;
				_vBackground.Hidden = true;
				vBackGround.Add (_vBackground);
				_vBackground.FixSupperView(vBackGround);
			}
		}

		private void InitializeView ()
		{
			LoadNib ();
			SetupView ();
			InitBinding ();
		}	

		private void InitBinding ()
		{
			this.WhenAnyValue (x => x.ViewModel)
				.Where (vm => vm != null)
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (vm => HandleBinding ());
			
		}

		public void SetupView ()
		{
			if (!string.IsNullOrEmpty (BackGroundImage)) {
				ViewBackground = new UIImageView () {
					Image = UIImage.FromFile (BackGroundImage),
				};
				ViewBackground.Hidden = !ViewModel.IsSelected;
			}

			lblName.TextColor = ViewModel.IsSelected ? UIColor.FromRGB(255, 180, 0) : UIColor.FromRGB(153, 153, 153);
			lblName.Text = ViewModel.NameTab;
			imgTab.Image = UIImage.FromFile (ViewModel.IsSelected ? ViewModel.IconSelected:ViewModel.IconUnSelected);;

		}

		private void HandleBinding ()
		{
			ViewModel.WhenAnyValue (x => x.IsSelected)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				//TODO hidden viewbackground select, change image, change text color.
				ViewModel.Icon = x ? ViewModel.IconSelected : ViewModel.IconUnSelected;

				if (ViewBackground != null) {
					ViewBackground.Hidden = !x;
				}
					lblName.TextColor = x ? UIColor.FromRGB(255, 180, 0) : UIColor.FromRGB(153, 153, 153);
			});
			
			ViewModel.WhenAnyValue (x => x.NameTab)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				lblName.Text = x;
			});
			ViewModel.WhenAnyValue (x => x.Icon)
				.Where (x => !string.IsNullOrEmpty (x))
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				imgTab.Image = UIImage.FromFile (x);
			});
		}

		private void LoadNib ()
		{
			var nibObjects = NSBundle.MainBundle.LoadNib ("DTabItem_AL", this, null);
			_rootView = Runtime.GetNSObject ((nibObjects.ValueAt (0))) as UIView;
			_rootView.FixSupperView (this);
		}
	}
}

