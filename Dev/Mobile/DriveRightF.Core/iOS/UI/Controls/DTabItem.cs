﻿using System;
using Foundation;
using UIKit;
using ObjCRuntime;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using Unicorn.Core.iOS;
using Unicorn.Core;

namespace DriveRightF.Core
{
	[Register("DTabItem")]
	public partial class DTabItem : DriveRightBaseView<UTabItemViewModel>
	{
		private UIView _rootView;
		private UIView _vBackground;

		public string BackGroundImage
		{
			get;
			set;
		}

		public event EventHandler<string> ValueChanged;

		public UIView RootView { get { return _rootView; } }

		public DTabItem(IntPtr handle) : base(handle)
		{
			
			InitializeView();
		}

		public DTabItem(CoreGraphics.CGRect frame) : base(frame)
		{
			InitializeView();
		}

		public DTabItem(UTabItemViewModel vm, string background) : base()
		{
//			ViewModel = vm;
			BackGroundImage = background;
			SetViewModel(vm);
			InitializeView();
		}

		public void SetViewModel(UTabItemViewModel viewModel)
		{
			ViewModel = viewModel;
			if(ViewModel != null)
			{
				RxApp.TaskpoolScheduler.Schedule(HandleBinding);
			}
		}

		public DTabItem() : base()
		{
			InitializeView();
		}

		public UIView ViewBackground
		{
			get
			{
				return _vBackground;
			}
			set
			{
				if(_vBackground != null)
				{
					_vBackground.RemoveFromSuperview();
				}
				_vBackground = value;
				_vBackground.Hidden = true;
				vBackGround.Add(_vBackground);
				var frame = vBackGround.Frame;
				_vBackground.Frame = new CoreGraphics.CGRect (0, 0, frame.Width, frame.Height);
//				FixSupperView (vBackGround, _vBackground);
			}
		}

		private void InitializeView()
		{
			LoadNib();
			SetupView();
			InitControls();
//			InitBinding ();
//			InitFont();
//			RxApp.TaskpoolScheduler.Schedule (InitBinding);
		}

		private void InitFont()
		{
			lblName.Font = FontHub.HelveticaMedium13;
		}

		private void InitControls()
		{
			//TODO: prevent multi touch of button.
			btnTab.ExclusiveTouch = true;
			btnTab.TouchUpInside += (object sender, EventArgs e) =>
			{
//				if(ViewModel != null && !ViewModel.IsSelected)
				{
					if(ValueChanged != null)
					{
						ValueChanged(this, lblName.Text);
					}
				}
			};
		}
		//
		//		private void InitBinding ()
		//		{
		//			this.WhenAnyValue (x => x.ViewModel)
		//				.Where (vm => vm != null)
		//				.ObserveOn (RxApp.TaskpoolScheduler)
		//				.Subscribe (vm => HandleBinding ());
		//
		//		}

		public void SetupView()
		{
			
//			ViewBackground = new UIImageView () {
//				Image = UIImage.FromFile (BackGroundImage),
//			};
//			ViewBackground.Hidden = !ViewModel.IsSelected;
			lblName.TextColor = ViewModel.IsSelected ? UIColor.FromRGB(255, 180, 0) : UIColor.FromRGB(153, 153, 153);
			lblName.Text = ViewModel.NameTab;
			imgTab.Image = UIImage.FromFile(ViewModel.IsSelected ? ViewModel.IconSelected : ViewModel.IconUnSelected);

		}

		private void HandleBinding()
		{
//			Debugger.ShowThreadInfo ("HandleBinding - TabItem");

			ViewModel.WhenAnyValue(x => x.IsSelected)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(x =>
			{
				if(ViewBackground != null)
				{
					ViewBackground.Hidden = !x;
				}
				lblName.TextColor = x ? UIColor.FromRGB(255, 180, 0) : UIColor.FromRGB(153, 153, 153);
				// Use ViewModel.Icon get accessor to check
				// Or change it in ViewModel
				string icon = x ? ViewModel.IconSelected : ViewModel.IconUnSelected;
				if(!string.IsNullOrEmpty(icon))
				{
					imgTab.Image = UIImage.FromFile(icon);
				}
			});
			
//			ViewModel.WhenAnyValue (x => x.NameTab)
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (x => {
//				lblName.Text = x;
//			});
			ViewModel.WhenAnyValue(x => x.Icon)
				.Where(x => !string.IsNullOrEmpty(x))
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(x =>
			{
				imgTab.Image = UIImage.FromFile(x);
			});
		}

		private void LoadNib()
		{
			var nibObjects = NSBundle.MainBundle.LoadNib("DTabItem", this, null);
			_rootView = Runtime.GetNSObject((nibObjects.ValueAt(0))) as UIView;
//			FixSupperView (this, _rootView);
		}
	}
}

