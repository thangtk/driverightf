﻿using System;
using Foundation;
using System.Drawing;
using ReactiveUI;
using Unicorn.Core.UI;
using CoreGraphics;
using Unicorn.Core.Translation;
using Unicorn.Core;

namespace DriveRightF.Core
{
	public class DriveRightBaseView<T> : UBaseView<T> 
		where T:ReactiveObject
	{
		protected DriveRightBaseView (IntPtr handle):base(handle)
		{

		}

		protected DriveRightBaseView ():base()
		{

		}

		protected DriveRightBaseView (NSCoder c):base(c)
		{

		}

		protected DriveRightBaseView (RectangleF size):base(size)
		{

		}
		protected DriveRightBaseView (CGRect size):base(size)
		{

		}
		protected DriveRightBaseView (NSObjectFlag f):base(f)
		{

		}
	}

	public class DriveRightBaseView : UBaseView 
	{
		protected DriveRightBaseView (IntPtr handle):base(handle)
		{

		}

		protected DriveRightBaseView ():base()
		{

		}

		protected DriveRightBaseView (NSCoder c):base(c)
		{

		}

		protected DriveRightBaseView (RectangleF size):base(size)
		{

		}
		protected DriveRightBaseView (CGRect size):base(size)
		{

		}

		protected DriveRightBaseView (NSObjectFlag f):base(f)
		{

		}
	}
}

