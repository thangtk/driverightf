﻿using System;
using UIKit;
using Foundation;
using ObjCRuntime;
using System.Collections.Generic;

namespace DriveRightF.Core
{
	[Register ("DTabBar")]
	public partial class DTabBar : DriveRightBaseView<UTabBarViewModel>
	{
		private UIView _rootView;
		private List<DTabItem> _tabItems;
		public  UIView RootView {get{ return _rootView;}}
		public event EventHandler<int> OnTabSelected;

		public DTabBar (IntPtr handle) : base (handle)
		{
			InitializeView ();
		}

		public DTabBar (CoreGraphics.CGRect frame) : base (frame)
		{
			InitializeView ();
		}

		public DTabBar () : base ()
		{
			InitializeView ();
		}

		public void AddTabItems (List<DTabItem> items)
		{
			_tabItems = items;
			nfloat widthItem = _rootView.Frame.Width / items.Count;
//			nfloat ratio = 1f / items.Count;
			nfloat x = 0;
			for (int i = 0; i < items.Count; i++) {
				var item = items [i];
				item.RootView.RemoveFromSuperview ();
				_rootView.Add (item.RootView);
				switch (i)
				{
					case 0: 
						item.RootView.Frame = new CoreGraphics.CGRect (0 , 0, widthItem-0.5f, _rootView.Frame.Height);
						break;
					case 1:
						item.RootView.Frame = new CoreGraphics.CGRect (i * widthItem + 0.5f , 0, widthItem - 1, _rootView.Frame.Height);
						break;
					case 2:
						item.RootView.Frame = new CoreGraphics.CGRect (i * widthItem + 0.5f , 0, widthItem, _rootView.Frame.Height);
						break;
				}

			} 
//			HandleBindings ();
			HandleTabItems ();
		}

		private void HandleBindings()
		{
			if (ViewModel != null && ViewModel.ItemModels != null) {
				for (int i = 0; i < _tabItems.Count; i++) {
					if (_tabItems [i].ViewModel == null) {
						//_tabItems [i].ViewModel = ViewModel.ItemModels [i];
					}
				}
			}

		}


		private void HandleTabItems ()
		{
			for (int i = 0; i < _tabItems.Count; i++) {
				var item = _tabItems [i];
				item.ValueChanged += (object sender, string e) => {
					int index = _tabItems.FindIndex (x => x == item);
					SelectedTab (index);
					if (OnTabSelected != null) {
						OnTabSelected (sender, index);
					}
				};
			}
			;
		}

		private void SelectedTab (int index)
		{
			for (int i = 0; i < _tabItems.Count; i++) {
				var item = _tabItems [i];
				item.ViewModel.IsSelected = index == i;// ? true : false;
			} 
		}

		private void InitializeView ()
		{
			this.Frame = new CoreGraphics.CGRect (0, 0, 320, 44);
			LoadNib ();

			//			this.BackgroundColor = UIColor.FromRGB (2, 2, 248);

			InitBinding ();
		}

		private void InitBinding ()
		{

		}

		private void LoadNib ()
		{
			var nibObjects = NSBundle.MainBundle.LoadNib ("DTabBar", this, null);
			_rootView = Runtime.GetNSObject ((nibObjects.ValueAt (0))) as UIView;
			_rootView.Frame = new CoreGraphics.CGRect (0, 0, Frame.Width, Frame.Height);
		}

	}
}

