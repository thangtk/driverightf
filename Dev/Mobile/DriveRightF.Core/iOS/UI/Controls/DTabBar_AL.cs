﻿using System;
using UIKit;
using Foundation;
using ObjCRuntime;
using System.Collections.Generic;
using Unicorn.Core.iOS;

namespace DriveRightF.Core
{
	[Register("DTabBar_AL")]
	public partial class DTabBar_AL : BaseTabBar<DRTabBarViewModel, DTabItem_AL>
	{
		#region implemented abstract members of BaseTabBar

		public override UIView ContentView {
			get {
				return _rootView;
			}
		}

		#endregion

		private UIView _rootView;
		public DTabBar_AL(IntPtr handle) : base(handle)
		{
			InitializeView();
		}

		public DTabBar_AL() : base()
		{
			InitializeView();
		}

		private void InitializeView()
		{
			LoadNib();

			InitBinding();
		}

		private void InitBinding()
		{

		}

		private void LoadNib()
		{
			var nibObjects = NSBundle.MainBundle.LoadNib("DTabBar_AL", this, null);
			_rootView = Runtime.GetNSObject((nibObjects.ValueAt(0))) as UIView;
			_rootView.FixSupperView (this);
		}
	}
}

