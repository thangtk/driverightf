﻿using System;
using Unicorn.Core.UI;
using ReactiveUI;
using Foundation;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using UIKit;
using CoreGraphics;
using Unicorn.Core.iOS;

namespace DriveRightF.Core
{
	public abstract class BaseHandlerKeyboardViewController<T>:UBaseViewController<T>
		where T:ReactiveObject
	{
		private UIView _activeView;
		private float _offsetKeyboard = 0f;
		private float _scrollAmount = 0.0f;   
		private float _bottom = 0.0f;                
		private bool _moveViewUp = false;
		private bool _isShowKeyboard = false;

		private NSObject _hideObj;
		private NSObject _showObj;

		public virtual float OffsetKeyboard
		{
			get {
				return _offsetKeyboard;
			}
		}
			

		protected BaseHandlerKeyboardViewController (string nibNameOrNull, NSBundle nibBundleOrNull)
			:base(nibNameOrNull,nibBundleOrNull)
		{
		}

		protected BaseHandlerKeyboardViewController (IntPtr handle)
			:base(handle)
		{
		}

		protected BaseHandlerKeyboardViewController (NSObjectFlag f)
			:base(f)
		{
		}

		protected BaseHandlerKeyboardViewController ():base()
		{
		}

		protected BaseHandlerKeyboardViewController (NSCoder c):base(c)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			RegisterKeyboardObserver ();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
		
			RegisterKeyboardObserver ();
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			RemoveKeyboardObserver ();
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);

			RemoveKeyboardObserver ();
		}
			
		private void RegisterKeyboardObserver()
		{
			if (_showObj == null && _hideObj == null) {
				// Keyboard popup
				_showObj = NSNotificationCenter.DefaultCenter.AddObserver
			(UIKeyboard.WillShowNotification, KeyBoardUpNotification);

				// Keyboard Down
				_hideObj = NSNotificationCenter.DefaultCenter.AddObserver
			(UIKeyboard.WillHideNotification, KeyBoardDownNotification);
			}
		}

		protected void RemoveKeyboardObserver ()
		{
			if (_hideObj != null)
			{
				NSNotificationCenter.DefaultCenter.RemoveObserver (_hideObj);
				_hideObj.Dispose ();
				_hideObj = null;
			}

			if (_showObj != null)
			{
				NSNotificationCenter.DefaultCenter.RemoveObserver (_showObj);
				_showObj.Dispose ();
				_showObj = null;
			}
		}
			
		private UIView GetViewResponder(UIView viewGroup)
		{
			UIView a = null;
			if (viewGroup.Subviews != null && viewGroup.Subviews.Length > 0) {
				foreach (UIView view in viewGroup.Subviews) {
					if (view.IsFirstResponder) {
						a = view;
						;
						break;
					} else {
							UIView v = GetViewResponder (view);
							a = v;
							if (a != null) {
								break;
						}
					}
				}
			}
			return a;
		}

		public virtual void KeyBoardUpNotification(NSNotification notification)
		{
			// get the keyboard size
			CGRect r = UIKeyboard.BoundsFromNotification (notification);

			_activeView = GetViewResponder (this.View);
			_activeView.AddHideButtonToKeyboard ();
			//TODO add toolbar done button on keyboard.


			// Calculate how far we need to scroll
			if(!_isShowKeyboard &&_activeView != null)
			{
				UIView relativePositionView = UIApplication.SharedApplication.KeyWindow;
				CGPoint relativeFrame = _activeView.ConvertPointToView(_activeView.Frame.Location, _activeView.Window);
				nfloat bottom = relativeFrame.Y + _activeView.Frame.Height + OffsetKeyboard;
				_scrollAmount = (float)(r.Height - (View.Frame.Size.Height - bottom));
				if (_scrollAmount > 0) {
					_moveViewUp = true;
					ScrollTheView (_moveViewUp);
				} else {
					_moveViewUp = false;
				}
				_isShowKeyboard = true;
			}
		}

		private void ScrollTheView(bool move)
		{
			// scroll the view up or down
			UIView.BeginAnimations (string.Empty, System.IntPtr.Zero);
			UIView.SetAnimationDuration (0.3);

			CGRect frame = View.Frame;

			if (move) {
				frame.Y -= _scrollAmount;
			} else {
				frame.Y += _scrollAmount;
				_scrollAmount = 0;
			}

			View.Frame = frame;
			UIView.CommitAnimations();
		}

		public virtual void KeyBoardDownNotification(NSNotification notification)
		{
			if(_moveViewUp && _isShowKeyboard) {
				ScrollTheView(false);
			}
			_isShowKeyboard = false;
		}
	}
}

