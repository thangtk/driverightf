﻿using System;
using Foundation;

namespace DriveRightF.Core
{
	public class BaseHandlerHeaderViewController<T> :DriveRightBaseViewController<T> where T : BaseHandlerHeaderViewModel
	{

		protected BaseHandlerHeaderViewController (string nibNameOrNull, NSBundle nibBundleOrNull)
			:base(nibNameOrNull,nibBundleOrNull)
		{
		}

		protected BaseHandlerHeaderViewController (IntPtr handle)
			:base(handle)
		{
		}

		protected BaseHandlerHeaderViewController (NSObjectFlag f)
			:base(f)
		{
		}

		protected BaseHandlerHeaderViewController ():base()
		{

		}

		protected BaseHandlerHeaderViewController (NSCoder c):base(c)
		{

		}

		public override void ReloadView ()
		{
			base.ReloadView ();
			if (ViewModel != null) {
				ViewModel.UpdateHeaderFooter ();
			}
		}
	}
}

