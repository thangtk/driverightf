﻿using System;
using ReactiveUI;
using Unicorn.Core.Navigator;
using Foundation;

namespace DriveRightF.Core
{
	public abstract class BaseNavigableViewController<T> : UNavigableController
		where T:ReactiveObject
	{
		protected BaseNavigableViewController (string nibNameOrNull, NSBundle nibBundleOrNull)
			:base(nibNameOrNull,nibBundleOrNull)
		{

		}

		protected BaseNavigableViewController (IntPtr handle)
			:base(handle)
		{

		}

		protected BaseNavigableViewController (NSObjectFlag f)
			:base(f)
		{

		}

		protected BaseNavigableViewController ():base()
		{
		}

		protected BaseNavigableViewController (NSCoder c):base(c)
		{

		}

		public override void ReloadView(){
		}
		public override void LoadData(params object[] objs){
		}
		public override bool IsShouldCache(){
			return true;
		}
	}
}

