﻿using System;
using Unicorn.Core.Navigator;
using Unicorn.Core.UI;

namespace DriveRightF.Core
{
	public interface ITabItemNavigator : IBaseNavigator
	{
		void TabClick ();
		UBaseViewController CurrentView { get; set;}
	}
}

