﻿using System;
using Unicorn.Core.Navigator;

namespace DriveRightF.iOS
{
	public interface IHomeTabNavigator : IBaseNavigator
	{
		BaseTabItemScreen CurrentTab { get; set;}
	}
}

