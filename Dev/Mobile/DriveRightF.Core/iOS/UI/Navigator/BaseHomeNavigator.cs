﻿using System;
using UIKit;
using System.Collections.Concurrent;
using Unicorn.Core.UI;
using System.Collections.Generic;
using Unicorn.Core.Navigator;
using System.Linq;
using DriveRightF.Core.Enums;
using Unicorn.Core;


namespace DriveRightF.iOS
{

	public abstract class BaseHomeNavigator:IHomeTabNavigator
	{

//		private ConcurrentDictionary<int,UBaseViewController> _controllerList = new ConcurrentDictionary<int,UBaseViewController> ();
//		public ConcurrentDictionary<int,UBaseViewController> ControllerList {get{ return _controllerList;}}
		private UNavigableController _navController;
		private UIViewController _rootViewController;
		private Stack<int> _previousScreens = new Stack<int> ();
		public int PreviousScreen { get; set; }

		public BaseTabItemScreen CurrentTab { get; set;}

		public int CurrentScreen { get; private set; }

		public UNavigableController NavigationController
		{
			get
			{
				return _navController;
			}
		}

		public UBaseViewControllerFactory ViewControllerFactory {get{ return DependencyService.Get<UBaseViewControllerFactory> ();}} 

		public BaseHomeNavigator (UNavigableController home)
		{
			_navController = home;
			_delegate = new UTransitionControllerDelegate ();
//			_previousScreens.Push (-1);
			CurrentScreen = -1;
			PreviousScreen = -1;
		}
		public virtual void NavigateBack (int screenId, bool isRefresh = false)
		{
			
			UBaseViewController ubase = ViewControllerFactory.CreateViewController(screenId);
			if (ubase is BaseTabItemScreen) {
				(ubase as BaseTabItemScreen).UDelegate = NavigationController.UDelegate;
			}
			if (_navController.ViewControllers.Contains (ubase)) {
				_navController.PopToViewController (ubase, GetTransition(CurrentScreen,screenId,null));
				if (isRefresh && _navController.TopViewController is UBaseNavController) {
					UBaseViewController controller = _navController.TopViewController as UBaseViewController;
					controller.ReloadView ();
				}
				var list = _previousScreens.ToList ();
				int index = list.IndexOf (screenId);
				if (index > 0) {
					for (int i = list.Count; i >= list.Count - index; i--) {
						_previousScreens.Pop ();
					}
				} else {
				}
				CurrentScreen = screenId;
				if (ubase is BaseTabItemScreen) {
					CurrentTab = (ubase as BaseTabItemScreen);
				}
			} else {
				Navigate (screenId, new object[0]);
			}

		}
		UTransitionControllerDelegate _delegate;
		public virtual void NavigateBack (bool isRefresh = false)
		{
			UIViewController backNav = _navController.ViewControllers.Count > 1 ? _navController.ViewControllers [_navController.ViewControllers.Count - 2] : null;
			_navController.PopViewController ();


			if (isRefresh )
			{
				UBaseViewController controller = backNav as UBaseViewController;
				if(controller != null)
				{
					controller.ReloadView ();
				}
			}
			CurrentScreen = _previousScreens.Pop ();
			if (backNav is BaseTabItemScreen) {
				CurrentTab = (backNav as BaseTabItemScreen);
			}
		}




		public void NavigateToStart (int screenId)
		{
			_navController.PopToRootViewControllerWithTransition (GetTransition(CurrentScreen,-1,null));
			_previousScreens.Clear ();
			PreviousScreen = CurrentScreen = -1;
			UBaseViewController ubase = ViewControllerFactory.CreateViewController(screenId);
			if (ubase is BaseTabItemScreen) {
				(ubase as BaseTabItemScreen).UDelegate = NavigationController.UDelegate;
			}

			if (_navController.TopViewController != ubase) {
				_navController.PushViewController (ubase, GetTransition(CurrentScreen,screenId,null));
			}


			ubase.LoadData (new object[0]);
		}

		public virtual void Navigate (int screenId,object[] objs = null,object[] transitionParam = null)
		{
			if (screenId == CurrentScreen) {
				return;
			}
			UBaseViewController ubase = ViewControllerFactory.CreateViewController(screenId);
			if (ubase is BaseTabItemScreen) {
				(ubase as BaseTabItemScreen).UDelegate = NavigationController.UDelegate;
			}
			if (_navController.ViewControllers.Contains (ubase)) {
				_navController.PopToViewController (ubase, GetTransition(CurrentScreen,screenId,transitionParam));
				if ( _navController.TopViewController is UBaseNavController) {
					UBaseViewController controller = _navController.TopViewController as UBaseViewController;
//					
				}
				ubase.LoadData (objs);
				ubase.ReloadView ();
			} else {
				if (_navController.TopViewController != ubase) {
					_navController.PushViewController (ubase, GetTransition(CurrentScreen,screenId,transitionParam));
//					
				}
				ubase.LoadData (objs);
				ubase.ReloadView ();
			}
			_previousScreens.Push (CurrentScreen);
			CurrentScreen = screenId;
			if (ubase is BaseTabItemScreen) {
				CurrentTab = (ubase as BaseTabItemScreen);
			}

		}

		public void OpenMenu ()
		{
		}

		public void CloseMenu ()
		{

		}

		public void ToggleMenu ()
		{

		}

		private void CleanUpMemory (int screen, List<int> targetScreens)
		{
			if (targetScreens.Contains (screen))
			{
				ReleaseViewControllers ();
			}
		}

		private void ReleaseViewControllers ()
		{
			//			_controllerList.RemoveAll (c => true);
			GC.Collect ();
		}

		protected virtual void OnNavigating (int nextScreen)
		{

		}

		private void HideKeyboard ()
		{
			if (_navController == null || _navController.TopViewController == null)
			{
				return;
			}

			UIView topView = _navController.TopViewController.View;
			ResignResponder (topView);
		}

		private void ResignResponder (UIView topView)
		{
			topView.EndEditing (true);

		}

		public virtual void WillShowViewController(UIViewController transitionController,UIViewController viewController , bool animated){
		}
		public virtual void DidShowViewController(UIViewController transitionController ,UIViewController viewController ,bool animated){

		}
		public virtual void PopViewDidFinish(UIViewController transitionController ,UIViewController viewController ,bool animated){

		}

		public virtual void PushViewDidFinish(UIViewController transitionController ,UIViewController viewController ,bool animated){

		}

//		protected abstract UBaseViewController CreateViewController (int screen);
		protected abstract UTransition GetTransition (int screenF,int screenT,object[] transitionParams);
	}

	public class HomeControllerDelegate : UTransitionControllerDelegate{
		BaseHomeNavigator _nav;

		public HomeControllerDelegate(BaseHomeNavigator nav){
			_nav = nav;
		}

		public override void WillShowViewController(UIViewController transitionController,UIViewController viewController , bool animated){
			_nav.WillShowViewController (transitionController, viewController, animated);
		}
		public override void DidShowViewController(UIViewController transitionController ,UIViewController viewController ,bool animated){
			_nav.DidShowViewController (transitionController, viewController, animated);
		}
		public override void PopViewDidFinish(UIViewController transitionController ,UIViewController viewController ,bool animated){
			_nav.PopViewDidFinish (transitionController, viewController, animated);
		}

		public override void PushViewDidFinish(UIViewController transitionController ,UIViewController viewController ,bool animated){
			_nav.PushViewDidFinish (transitionController, viewController, animated);
		}
	}
}

