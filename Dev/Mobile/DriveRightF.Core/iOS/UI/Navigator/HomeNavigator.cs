﻿using System;
using Unicorn.Core.UI;
using DriveRightF.Core.Enums;
using Unicorn.Core.Navigator;
using CoreGraphics;
using UIKit;
using DriveRightF.Core;


using DriveRightF.Core.Constants;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.iOS.HomeNavigator))]
namespace DriveRightF.iOS
{
	public class HomeNavigator:BaseHomeNavigator
	{
		public HomeNavigator (UNavigableController home):base(home)
		{

		}

		private const int Duration = SystemConfig.TRANSITION_DURATION;
		private UGlueTransition _glue;
		private UTransition _gluere;
		private UTransition _flip;

		protected override UTransition GetTransition (int screenF,int screenT,object[] transitionParams){
			if (_glue == null) {
				_glue = new UGlueTransition (TimeSpan.FromMilliseconds (Duration), UTransitionOrientation.RightToLeft, NavigationController.ContainerView.Frame);
				_gluere = _glue.GetReverseTransition ();
			}
			if (_flip == null) {
				_flip = new UFlipTransition(TimeSpan.FromMilliseconds (Duration), UTransitionOrientation.RightToLeft, NavigationController.ContainerView.Frame);
			}
			var frame  = NavigationController.ContainerView.ConvertRectToView(NavigationController.ContainerView.Bounds,null);
			if (screenF == -1) {
				return new NullTransition ();
			}
			Screen sf = (Screen)screenF;
			Screen st = (Screen)screenT;
			if((st == Screen.TabTrips|| st ==Screen.TabAccount ) && (
				sf == Screen.TabAward )){
				return _glue;
			}

			if(st ==Screen.TabAccount  && sf == Screen.TabTrips ){
				return _glue;
			}

			if(st ==Screen.TabTrips  && 
				sf == Screen.TabAccount ){
				return _gluere;
			}

			if(st == Screen.TabAward && sf != Screen.TabAward ){
				return _gluere;
			}

			return _flip;
		}




//		protected override UTransition GetTransition (int screenF,int screenT,object[] transitionParams){
//			if (_glue == null) {
//				_glue = new UGlueTransition (TimeSpan.FromMilliseconds (Duration), UTransitionOrientation.RightToLeft, NavigationController.ContainerView.Frame);
//				_gluere = _glue.GetReverseTransition ();
//			}
//			if (_flip == null) {
//				_flip = new UFlipTransition(TimeSpan.FromMilliseconds (Duration), UTransitionOrientation.RightToLeft, NavigationController.ContainerView.Frame);
//			}
//			var frame  = NavigationController.ContainerView.ConvertRectToView(NavigationController.ContainerView.Bounds,null);
//			if (screenF == -1) {
//				return new NullTransition ();
//			}
//			Screen sf = (Screen)screenF;
//			Screen st = (Screen)screenT;
//
//			// 
//			if ((sf == Screen.HomeAward && st == Screen.AwardDetail) 
//				|| (sf == Screen.HomeAward && (st == Screen.CardDetail || st == Screen.AwardDetail)) 
//				|| (sf == Screen.HomeAccount && (st == Screen.AccountStatement || st == Screen.AccountDetail))
//				|| (sf == Screen.HomeTrips && st == Screen.TripStatement)) {
//				UIView view = (UIView)transitionParams [0];
//				CGRect newFrame = view.ConvertRectToView(view.Bounds,null);
//
//				return new DriverRightCellZoomTransition (TimeSpan.FromMilliseconds (Duration), UTransitionOrientation.LeftToRight, frame,newFrame,view);
//			}
//
//
//
//			// between 
//			if((st == Screen.HomeTrips|| st ==Screen.HomeAccount ) && (
//				sf == Screen.HomeAward || sf == Screen.Award || 
//				sf == Screen.AwardDetail || sf == Screen.CardDetail ||sf == Screen.Gamble 
//			)){
//				return _glue;
//			}
//
//			if((st ==Screen.HomeAccount ) && (
//				sf == Screen.HomeTrips || 
//				sf == Screen.Trips || sf == Screen.TripDetail ||sf == Screen.TripStatement 
//			)){
//				return _glue;
//			}
//
//			if((st ==Screen.HomeTrips ) && (
//				sf == Screen.HomeAccount || 
//				sf == Screen.AccountDetail || 
//				sf == Screen.AccountStatement 
//			)){
//				return _gluere;
//			}
//
//			if((st == Screen.HomeAward ) && (
//				sf != Screen.HomeAward || sf != Screen.Award || 
//				sf != Screen.AwardDetail || sf != Screen.CardDetail ||sf != Screen.Gamble 
//			)){
//				return _gluere;
//			}
//
//			return _flip;
//		}

//		private  UTransition Create(int screenF, int screenT)
//		{
//			if ((sf == Screen.HomeAward && st == Screen.AwardDetail) || (sf == Screen.HomeAward && st == Screen.CardDetail) ) {
//				return new UZoomTransition (TimeSpan.FromMilliseconds (250), UTransitionOrientation.LeftToRight, NavigationController.ContainerView.Frame,(CGRect)transitionParams[0]);
//			}
//			else
//				 return
//		}
	}
}

