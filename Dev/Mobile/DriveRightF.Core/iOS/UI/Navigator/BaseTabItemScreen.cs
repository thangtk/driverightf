﻿using System;
using Unicorn.Core.Navigator;
using Foundation;
using ReactiveUI;
using DriveRightF.Core;
using DriveRightF.Core.Constants;
using UIKit;
using System.Collections.Generic;
using Unicorn.Core;
using Unicorn.Core.UI;
using System.Linq;
using DriveRightF.Core.Enums;
using CoreGraphics;

namespace DriveRightF.iOS
{
//	public abstract class BaseTabItemScreen<T> : BaseTabItemScreen
//		where T:BaseHandlerHeaderViewModel{
//		protected BaseTabItemScreen (string nibNameOrNull, NSBundle nibBundleOrNull)
//			: base (nibNameOrNull, nibBundleOrNull)
//		{
//		}
//
//		protected BaseTabItemScreen (IntPtr handle)
//			: base (handle)
//		{
//		}
//
//		protected BaseTabItemScreen (NSObjectFlag f)
//			: base (f)
//		{
//		}
//
//		protected BaseTabItemScreen () : base ()
//		{
//		}
//
//		protected BaseTabItemScreen (NSCoder c) : base (c)
//		{
//		}
//
//		protected T _viewModel; 
//		public T ViewModel
//		{
//			get { return _viewModel; }
//			set { this.RaiseAndSetIfChanged(ref _viewModel, value); }
//		}
//
//		public override void ReloadView ()
//		{
//			base.ReloadView ();
//			if (ViewModel != null) {
//				ViewModel.UpdateHeader ();
//			}
//		}
//	}
	public abstract class BaseTabItemScreen: UNavigableController,ITabItemNavigator
	{
		protected BaseTabItemScreen (string nibNameOrNull, NSBundle nibBundleOrNull)
			: base (nibNameOrNull, nibBundleOrNull)
		{
			InitData ();
		}

		protected BaseTabItemScreen (IntPtr handle)
			: base (handle)
		{
			InitData ();
		}

		protected BaseTabItemScreen (NSObjectFlag f)
			: base (f)
		{
			InitData ();
		}

		protected BaseTabItemScreen () : base ()
		{
			InitData ();
		}

		protected BaseTabItemScreen (NSCoder c) : base (c)
		{
			InitData ();
		}
		public UBaseViewController CurrentView { get; set;}
		private UIViewController _rootViewController;
		private Stack<int> _previousScreens = new Stack<int> ();

		public int PreviousScreen { get; set; }

		protected int Duration = SystemConfig.TRANSITION_DURATION;

		public int CurrentScreen { get; private set; }

		public UBaseViewControllerFactory ViewControllerFactory { get { return DependencyService.Get<UBaseViewControllerFactory> (); } }

		private UTransition _flip;

		protected virtual void InitData ()
		{
			CurrentScreen = -1;
			PreviousScreen = -1;

		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			ContainerView = View;

		}

		public virtual void TabClick (){
		}

		public virtual void NavigateBack (int screenId, bool isRefresh = false)
		{

			UBaseViewController ubase = ViewControllerFactory.CreateViewController (screenId);
			CurrentView = ubase;
			ubase.Navigator = this;
			if (this.ViewControllers.Contains (ubase)) {
				this.PopToViewController (ubase);
				if (isRefresh) {
					ubase.ReloadView ();
				}
				var list = _previousScreens.ToList ();
				int index = list.IndexOf (screenId);
				if (index > 0) {
					for (int i = list.Count; i >= list.Count - index; i--) {
						_previousScreens.Pop ();
					}
				} else {
				}
				CurrentScreen = screenId;

			} else {
				Navigate (screenId, new object[0]);
			}

		}

		public virtual void NavigateBack (bool isRefresh = false)
		{
			UIViewController backNav = this.ViewControllers.Count > 1 ? this.ViewControllers [this.ViewControllers.Count - 2] : null;
			this.PopViewController ();


			if (isRefresh) {
				UBaseViewController controller = backNav as UBaseViewController;
				if (controller != null) {
					controller.ReloadView ();
				}
			}
			CurrentScreen = _previousScreens.Pop ();
			CurrentView = backNav as UBaseViewController;
		}




		public void NavigateToStart (int screenId)
		{
			this.PopToRootViewControllerWithTransition (GetTransition (CurrentScreen, -1, null));
			_previousScreens.Clear ();
			PreviousScreen = CurrentScreen = -1;
			CurrentView = null;
			UBaseViewController ubase = ViewControllerFactory.CreateViewController (screenId);
			ubase.Navigator = this;
			if (this.TopViewController != ubase) {
				this.PushViewController (ubase, GetTransition (CurrentScreen, screenId, null));
			}


			ubase.LoadData (new object[0]);
		}

		public virtual void Navigate (int screenId, object[] objs = null, object[] transitionParam = null)
		{
			if (screenId == CurrentScreen) {
				return;
			}
			UBaseViewController ubase = ViewControllerFactory.CreateViewController (screenId);
			ubase.Navigator = this;
			if (this.ViewControllers.Contains (ubase)) {
				this.PopToViewController (ubase, GetTransition (CurrentScreen, screenId, transitionParam));
				if (this.TopViewController is UBaseNavController) {
					UBaseViewController controller = this.TopViewController as UBaseViewController;
					//					
				}
				ubase.LoadData (objs);
				ubase.ReloadView ();
			} else {
				if (this.TopViewController != ubase) {
					this.PushViewController (ubase, GetTransition (CurrentScreen, screenId, transitionParam));
					//					
				}
				ubase.LoadData (objs);
				ubase.ReloadView ();
			}
			_previousScreens.Push (CurrentScreen);
			CurrentScreen = screenId;
			CurrentView = ubase;
		}

		public void OpenMenu ()
		{
		}

		public void CloseMenu ()
		{

		}

		public void ToggleMenu ()
		{

		}

		private void CleanUpMemory (int screen, List<int> targetScreens)
		{
			if (targetScreens.Contains (screen)) {
				ReleaseViewControllers ();
			}
		}

		private void ReleaseViewControllers ()
		{
			//			_controllerList.RemoveAll (c => true);
			GC.Collect ();
		}

		protected virtual void OnNavigating (int nextScreen)
		{

		}

		private void HideKeyboard ()
		{
			if (this.TopViewController == null) {
				return;
			}

			UIView topView = this.TopViewController.View;
			ResignResponder (topView);
		}

		private void ResignResponder (UIView topView)
		{
			topView.EndEditing (true);

		}

		public override void ReloadView ()
		{
			if (CurrentView != null) {
				CurrentView.ReloadView ();
			}
		}

		public override void LoadData (params object[] objs)
		{
		}

		public override bool IsShouldCache ()
		{
			return true;
		}

		public virtual void WillShowViewController (UIViewController transitionController, UIViewController viewController, bool animated)
		{
		}

		public virtual void DidShowViewController (UIViewController transitionController, UIViewController viewController, bool animated)
		{

		}

		public virtual void PopViewDidFinish (UIViewController transitionController, UIViewController viewController, bool animated)
		{

		}

		public virtual void PushViewDidFinish (UIViewController transitionController, UIViewController viewController, bool animated)
		{

		}
		//		protected virtual UTransition GetTransition (int screenF,int screenT,object[] transitionParams){
		//			return _flip;
		//		}

		protected virtual UTransition GetTransition (int screenF, int screenT, object[] transitionParams)
		{
			
			if (_flip == null) {
				_flip = new UFlipTransition (TimeSpan.FromMilliseconds (Duration), UTransitionOrientation.RightToLeft, this.ContainerView.Frame);
			}
			var frame = this.ContainerView.ConvertRectToView (this.ContainerView.Bounds, null);
			if (screenF == -1) {
				return new NullTransition ();
			}
			Screen sf = (Screen)screenF;
			Screen st = (Screen)screenT;
			
			// 
//			if ((sf == Screen.HomeAward && st == Screen.AwardDetail)
//			       || (sf == Screen.HomeAward && (st == Screen.CardDetail || st == Screen.AwardDetail))
//			       || (sf == Screen.HomeAccount && (st == Screen.AccountStatement || st == Screen.AccountDetail))
//			       || (sf == Screen.HomeTrips && st == Screen.TripStatement)
//			){
//				UIView view = (UIView)transitionParams [0];
//				CGRect newFrame = view.ConvertRectToView (view.Bounds, null);
//			
//				return new DriverRightCellZoomTransition (TimeSpan.FromMilliseconds (Duration), UTransitionOrientation.LeftToRight, frame, newFrame, view);
//			}
			
			return _flip;
		}
		
	}
}

