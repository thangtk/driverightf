﻿using System;
using ReactiveUI;
using Foundation;
using DriveRightF.iOS;
using DriveRightF.Core.UI;

namespace DriveRightF.Core.iOS
{
	[Preserve]
	public class UnicorniOSObservableForWigets: UIKitObservableForPropertyBase
	{
		public static Lazy<UIKitObservableForProperty> Instance = new Lazy<UIKitObservableForProperty>();

		public UnicorniOSObservableForWigets () : base()
		{
			Register(typeof(UTextBox), "Text", 30, (s, p)=> ObservableFromEvent(s, p, "ValueChanged"));
			Register(typeof(UDatePicker), "Text", 30, (s, p)=> ObservableFromEvent(s, p, "ValueChanged"));
			Register(typeof(DriveRightCropImage), "CurrentRectangle", 30, (s, p)=> ObservableFromEvent(s, p, "RectangleChanged"));
			Register(typeof(DriveRightCropImage), "CurrentScale", 30, (s, p)=> ObservableFromEvent(s, p, "ScaleChanged"));
		}
	}
}

