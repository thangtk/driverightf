﻿using System;
using ReactiveUI;
using System.Linq;

namespace DriveRightF.Core.iOS
{
	public class UnicorniOSDefaultPropertyBinding: IDefaultPropertyBindingProvider
	{
		public Tuple<string, int> GetPropertyForControl(object control)
		{
			// NB: These are intentionally arranged in priority order from most
			// specific to least specific.
			var items = new[] {
				new { Type = typeof(IUTextBox), Property = "Text" },
				new { Type = typeof(IUTextBox), Property = "Error" },
				new { Type = typeof(IUTextBox), Property = "Label" },
			};


			var type = control.GetType();
			var kvp = items.FirstOrDefault(x => x.Type.IsAssignableFrom(type));

			return kvp != null ? Tuple.Create(kvp.Property, 5) : null;
		}
	}
}

