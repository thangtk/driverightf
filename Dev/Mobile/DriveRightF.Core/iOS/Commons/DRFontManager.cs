﻿using System;
using Unicorn.Core;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.Core.DRFontManager))]
namespace DriveRightF.Core
{
	public class DRFontManager : FontManager
	{
		public DRFontManager ()
		{
		}

		#region implemented abstract members of FontManager

		public override string MainFont {
			get {
				return "Helvetica";
			}
		}

		#endregion
	}
}

