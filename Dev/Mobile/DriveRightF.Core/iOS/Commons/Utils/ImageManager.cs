﻿using System;
using UIKit;
using DriveRightF.iOS;
using Foundation;
using DriveRightF.Core;
using System.Drawing;
using Unicorn.Core.iOS;
using System.Threading.Tasks;


[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.iOS.ImageManager))]
namespace DriveRightF.iOS
{
	public class ImageManager : IImageManager
	{
		#region IImageManager implementation
		public string SaveImage (object image, string imageName = null)
		{
			UIImage img = image as UIImage;
			if (img == null) {
				return string.Empty;
			}

			imageName = imageName ?? string.Format ("{0}.png", DateTime.UtcNow.Ticks);

			return ImageUtil.SaveImage (img, imageName);
		}

		public string SaveImage (byte[] image, string imageName = null)
		{
			if (image == null) {
				return string.Empty;
			}

			NSData data = NSData.FromArray (image);

			return ImageUtil.SaveNSData (data,imageName);
		}
			

		public bool DeleteImage (string namePath)
		{
			return ImageUtil.DeleteFile(namePath);

		}

		public object CropResizeImage(object image, SizeF size, RectangleF rec){
			UIImage img = image as UIImage;
			if (img == null) {
				return null;
			}

			return img.CropAndReSizeImage(rec,size);
		}

		public byte[] GetImageData(string path){
			return UIImage.FromFile (path).ToNSData ();

		}

		public string GetImageBase64(string path){
			return Convert.ToBase64String( UIImage.FromFile (path).ToNSData ());

		}
			
		public async Task<string> DownloadImage (string imageUrl, string imageName)
		{
			return await ImageUtil.DownloadImage (imageUrl, imageName);
		}
		#endregion
	}
}

