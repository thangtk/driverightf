﻿using System;
using UIKit;
using System.Threading.Tasks;
using DriveRightF.iOS;
using Unicorn.Core;
using Unicorn.Core.Bases;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.Core.iOS.iOSImageManager))]
namespace DriveRightF.Core.iOS
{
	public class iOSImageManager : CachedImageManager
	{
		protected async override Task<string> DownloadImageAsync (string url)
		{
			// Guid.NewGuid ().ToString () always return 1 value?
			return await ImageUtil.DownloadImage (url, Guid.NewGuid ().ToString ());
			// TODO: check again
			return string.Empty;
		}

		protected override bool ImageExists(string localPath)
		{
			return UIImage.FromFile (localPath) != null;
		}
	}
}

