﻿using System;
using UIKit;
using Foundation;
using System.IO;
using CoreGraphics;
using System.Drawing;
using System.Net;
using System.Threading.Tasks;
using Unicorn.Core;
using System.Threading;

namespace DriveRightF.iOS
{
	public static class ImageUtil
	{
		public static byte[] ToNSData (this UIImage image)
		{

			if (image == null) {
				return null;
			}
			NSData data = null;

			try {
				data = image.AsPNG ();
				return data.ToArray ();
			} catch (Exception) {
				return null;
			} finally {
				if (image != null) {
					image.Dispose ();
					image = null;
				}
				if (data != null) {
					data.Dispose ();
					data = null;
				}
			}

		}

		public static UIImage ToImage (this byte[] data)
		{
			if (data == null) {
				return null;
			}
			UIImage image = null;
			try {
				image = new UIImage (NSData.FromArray (data));
				data = null;
			} catch (Exception) {
				return null;
			}
			return image;
		}

		public static string SaveImage (UIImage image, string imageName)
		{
			NSData imgData = image.Scale (new CGSize (200, 200)).AsPNG ();
			var data = SaveNSData (imgData, imageName);
			return data;
		}

		public static string SaveNSData (NSData data, string dataName)
		{
			var documentsDirectory = Environment.GetFolderPath
				(Environment.SpecialFolder.Personal);
			string jpgFilename = System.IO.Path.Combine (documentsDirectory, dataName); // hardcoded filename, overwritten each time
			bool isSucess = data.Save (jpgFilename, false);
			return isSucess ? jpgFilename : string.Empty;
		}

		public static byte[] GetDataFromImage(string path)
		{
			return UIImage.FromFile (path).ToNSData ();
		}

		/// <summary>
		/// Downloads the image.
		/// </summary>
		/// <returns>The image path.</returns>
		/// <param name="imageUrl">Image URL.</param>
		public static async Task<string> DownloadImage (string imageUrl, string imageName)
		{
			if (String.IsNullOrEmpty (imageUrl))
				return string.Empty;

//			var webClient = new WebClient ();
//			var url = new Uri (imageUrl);
//
//			try {
//				var task = await webClient.DownloadDataTaskAsync (url);
//				return SaveImage (task.ToImage (), imageName);
//			} 
//			catch (Exception ex) {
//				return null;
//			}

			try {
				HttpWebRequest request = (HttpWebRequest)WebRequest.Create (imageUrl);
				HttpWebResponse response = (HttpWebResponse)request.GetResponse ();

				if ((response.StatusCode == HttpStatusCode.OK ||
				    response.StatusCode == HttpStatusCode.Moved ||
				    response.StatusCode == HttpStatusCode.Redirect) &&
				    response.ContentType.StartsWith ("image", StringComparison.OrdinalIgnoreCase)) {

					// if the remote file was found, download oit
					using (Stream inputStream = response.GetResponseStream ()) {
						byte[] buffer = ReadToEnd (inputStream);
						return SaveImage (ToImage (buffer), imageName);
					}
				}
			} catch {
				return string.Empty;
			}

			return string.Empty;
		}

		public static byte[] ReadToEnd(System.IO.Stream stream)
		{
			long originalPosition = 0;

			if(stream.CanSeek)
			{
				originalPosition = stream.Position;
				stream.Position = 0;
			}

			try
			{
				byte[] readBuffer = new byte[4096];

				int totalBytesRead = 0;
				int bytesRead;

				while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
				{
					totalBytesRead += bytesRead;

					if (totalBytesRead == readBuffer.Length)
					{
						int nextByte = stream.ReadByte();
						if (nextByte != -1)
						{
							byte[] temp = new byte[readBuffer.Length * 2];
							Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
							Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
							readBuffer = temp;
							totalBytesRead++;
						}
					}
				}

				byte[] buffer = readBuffer;
				if (readBuffer.Length != totalBytesRead)
				{
					buffer = new byte[totalBytesRead];
					Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
				}
				return buffer;
			}
			finally
			{
				if(stream.CanSeek)
				{
					stream.Position = originalPosition; 
				}
			}
		}

		public static bool DeleteFile (string fileName)
		{		
			if (string.IsNullOrEmpty (fileName)) {
				return false;
			}	
			if (!File.Exists (fileName)) {
				return false;
			}

			File.Delete (fileName);
			return true;
		}

		public static UIImage ScaleAndRotateImage (UIImage imageIn, UIImageOrientation orIn)
		{
			int kMaxResolution = 2048;

			CGImage imgRef = imageIn.CGImage;
			float width = imgRef.Width;
			float height = imgRef.Height;
			CGAffineTransform transform = CGAffineTransform.MakeIdentity ();
			RectangleF bounds = new RectangleF (0, 0, width, height);

			if (width > kMaxResolution || height > kMaxResolution) {
				float ratio = width / height;

				if (ratio > 1) {
					bounds.Width = kMaxResolution;
					bounds.Height = bounds.Width / ratio;
				} else {
					bounds.Height = kMaxResolution;
					bounds.Width = bounds.Height * ratio;
				}
			}

			float scaleRatio = bounds.Width / width;
			SizeF imageSize = new SizeF (width, height);
			UIImageOrientation orient = orIn;
			float boundHeight;

			switch (orient) {
			case UIImageOrientation.Up:                                        //EXIF = 1
				transform = CGAffineTransform.MakeIdentity ();
				break;

			case UIImageOrientation.UpMirrored:                                //EXIF = 2
				transform = CGAffineTransform.MakeTranslation (imageSize.Width, 0f);
				transform = CGAffineTransform.MakeScale (-1.0f, 1.0f);
				break;

			case UIImageOrientation.Down:                                      //EXIF = 3
				transform = CGAffineTransform.MakeTranslation (imageSize.Width, imageSize.Height);
				transform = CGAffineTransform.Rotate (transform, (float)Math.PI);
				break;

			case UIImageOrientation.DownMirrored:                              //EXIF = 4
				transform = CGAffineTransform.MakeTranslation (0f, imageSize.Height);
				transform = CGAffineTransform.MakeScale (1.0f, -1.0f);
				break;

			case UIImageOrientation.LeftMirrored:                              //EXIF = 5
				boundHeight = bounds.Height;
				bounds.Height = bounds.Width;
				bounds.Width = boundHeight;
				transform = CGAffineTransform.MakeTranslation (imageSize.Height, imageSize.Width);
				transform = CGAffineTransform.MakeScale (-1.0f, 1.0f);
				transform = CGAffineTransform.Rotate (transform, 3.0f * (float)Math.PI / 2.0f);
				break;

			case UIImageOrientation.Left:                                      //EXIF = 6
				boundHeight = bounds.Height;
				bounds.Height = bounds.Width;
				bounds.Width = boundHeight;
				transform = CGAffineTransform.MakeTranslation (0.0f, imageSize.Width);
				transform = CGAffineTransform.Rotate (transform, 3.0f * (float)Math.PI / 2.0f);
				break;

			case UIImageOrientation.RightMirrored:                             //EXIF = 7
				boundHeight = bounds.Height;
				bounds.Height = bounds.Width;
				bounds.Width = boundHeight;
				transform = CGAffineTransform.MakeScale (-1.0f, 1.0f);
				transform = CGAffineTransform.Rotate (transform, (float)Math.PI / 2.0f);
				break;

			case UIImageOrientation.Right:                                     //EXIF = 8
				boundHeight = bounds.Height;
				bounds.Height = bounds.Width;
				bounds.Width = boundHeight;
				transform = CGAffineTransform.MakeTranslation (imageSize.Height, 0.0f);
				transform = CGAffineTransform.Rotate (transform, (float)Math.PI / 2.0f);
				break;

			default:
				throw new Exception ("Invalid image orientation");
				break;
			}

			UIGraphics.BeginImageContext (bounds.Size);

			CGContext context = UIGraphics.GetCurrentContext ();

			if (orient == UIImageOrientation.Right || orient == UIImageOrientation.Left) {
				context.ScaleCTM (-scaleRatio, scaleRatio);
				context.TranslateCTM (-height, 0);
			} else {
				context.ScaleCTM (scaleRatio, -scaleRatio);
				context.TranslateCTM (0, -height);
			}

			context.ConcatCTM (transform);
			context.DrawImage (new RectangleF (0, 0, width, height), imgRef);

			UIImage imageCopy = UIGraphics.GetImageFromCurrentImageContext ();
			UIGraphics.EndImageContext ();

			return imageCopy;
		}

		public static UIImage ResizeImage (UIImage imageIn)
		{
			int kMaxResolution = 2048;
			UIImage rs = imageIn;
			if (imageIn.Size.Width > kMaxResolution || imageIn.Size.Height > kMaxResolution) {
				nfloat ratio = imageIn.Size.Width / imageIn.Size.Height;

				if (ratio > 1) {
					rs = imageIn.Scale (new CGSize (kMaxResolution, kMaxResolution / ratio));
				} else {
					rs = imageIn.Scale (new CGSize (kMaxResolution * ratio, kMaxResolution));
				}
			}
			return rs;
		}
	}
}

