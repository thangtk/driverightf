﻿using System;
using Foundation;
using UIKit;
using DriveRightF.Core;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.iOS.ApplicationUtil))]
namespace DriveRightF.iOS
{
	public class ApplicationUtil : IApplication
	{
		private ApplicationStatus _status = ApplicationStatus.New;

		public ApplicationStatus Status {
			get { return _status; }
			set { _status = value;	}
		}

		public ApplicationUtil ()
		{
		}

		#region IApplication implementation

		public void NavigateToLocationSetting (string url)
		{
			UIApplication.SharedApplication.OpenUrl(new NSUrl (url));
		}

		#endregion
	}
}

