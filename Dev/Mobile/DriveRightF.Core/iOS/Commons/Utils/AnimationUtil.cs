﻿using System;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using UIKit;

namespace DriveRightF.Core.iOS
{
	public class AnimationUtil
	{
		private AnimationUtil ()
		{
		}

		public static void Amimate(UIView view, AnimationStyle style, double delayDuration = 0)
		{
			switch (style) {
			case AnimationStyle.LeftToRight:
				LeftToRight (view, delayDuration);
				break;
			case AnimationStyle.ExpandFromCenter:
				ExpandFromCenter (view);
				break;
			case AnimationStyle.FromBottom:
				FromBottom (view, delayDuration);
				break;
			default:
				break;
			}
		}

		/// <summary>
		/// Animation left to right
		/// </summary>
		/// <param name="view">View.</param>
		private static void LeftToRight(UIView view, double delayDuration = 0)
		{
			view.Alpha = 0;

			UIView.Animate (0.3f,
				delayDuration,
				UIViewAnimationOptions.CurveLinear,
				() => {
					view.Transform = CGAffineTransform.MakeTranslation(200f, 0);
					view.Alpha = 1;
				},
				() => {
				}
			);
		}

		private static void FromBottom(UIView view, double delayDuration = 0)
		{
			view.Alpha = 0;
			view.Transform = CGAffineTransform.MakeTranslation(0, UIScreen.MainScreen.Bounds.Height - 70);

			UIView.Animate (0.7,
				delayDuration,
				UIViewAnimationOptions.CurveLinear,
				() => {
					view.Transform = CGAffineTransform.MakeIdentity();
					view.Alpha = 1;
				},
				() => {
				}
			);
		}

		/// <summary>
		/// Animation expand the circle from its center
		/// </summary>
		/// <param name="view">View.</param>
		private static void ExpandFromCenter(UIView view)
		{
			float radius = (float)Math.Sqrt (Math.Pow (160, 2) + Math.Pow (37.5f, 2));
			float originX = 320f / 2f - radius;
			float originY = 75f / 2f - radius;

			CALayer blackLayer = new CALayer ();
			blackLayer.Bounds = view.Layer.Bounds;
			blackLayer.BackgroundColor = UIColor.FromRGB(233, 233, 233).CGColor;
			blackLayer.Opacity = 1;
			blackLayer.Position = view.Center;
			view.Layer.AddSublayer (blackLayer);

			CAShapeLayer maskLayer = new CAShapeLayer ();
			maskLayer.BorderColor = UIColor.Purple.CGColor;
			maskLayer.BorderWidth = 0.1f;
			maskLayer.Bounds = view.Layer.Bounds;

			UIBezierPath path = UIBezierPath.FromOval (new CGRect (
				                    (view.Frame.Width / 2),
				                    (view.Frame.Height / 2),
				                    0, 0));
	
			UIBezierPath newPath = UIBezierPath.FromOval (new CGRect (
				                       originX, originY, 2f * radius, 2f * radius));

			path.AppendPath (UIBezierPath.FromRect (maskLayer.Bounds));
			newPath.AppendPath (UIBezierPath.FromRect (maskLayer.Bounds));

			maskLayer.Path = path.CGPath;
			maskLayer.Path = newPath.CGPath;

			maskLayer.Position = new CGPoint (blackLayer.Bounds.Width / 2, blackLayer.Bounds.Height / 2);
			maskLayer.FillRule = CAShapeLayer.FillRuleEvenOdd;
			blackLayer.Mask = maskLayer;
			view.Layer.AddSublayer (blackLayer);

			// Animation
			CABasicAnimation posAnimation = CABasicAnimation.FromKeyPath ("path");
			posAnimation.From = NSObject.FromObject(path.CGPath);
			posAnimation.To = NSObject.FromObject(newPath.CGPath);
			posAnimation.Duration = 0.4f;
			posAnimation.RepeatCount = 1f;
			posAnimation.AutoReverses = false;

			maskLayer.AddAnimation (posAnimation, "path");
		}
	}
}

