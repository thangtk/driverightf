﻿using System;
using CoreLocation;
using UIKit;
using Unicorn.Core.iOS;
using DriveRightF.Core.iOS;
using Unicorn.Core.UI;
using Unicorn.Core;
using Unicorn.Core.Translation;
using Foundation;

[assembly:Unicorn.Core.Dependency.Dependency (typeof(GpsNotificationManager))]
namespace DriveRightF.Core.iOS
{
	public class GpsNotificationManager
	{
		private static bool _isShowPopup = false;
		private static int _numberBagdes = 0;
		CLLocationManager _locationManager;
		private bool _isRegistered = false;
		private PopupManager PopupManager{
			get{ 
				return DependencyService.Get<PopupManager> ();
			}
		}

		private ITranslator Translator {
			get {
				return DependencyService.Get<ITranslator> ();
			}
		}

		public static int NumberBagdes {
			get {
				return _numberBagdes;
			}
			set {
				_numberBagdes = value;
			}
		}

		public GpsNotificationManager ()
		{
			_locationManager = new CLLocationManager ();
		}

		public void RequestAuthorization ()
		{
			if (CLLocationManager.Status == CLAuthorizationStatus.NotDetermined) {
				if (DeviceUtil.OSVersion >= 8) // (UIDevice.CurrentDevice.CheckSystemVersion (8, 0)) {
				{
					_locationManager.RequestAlwaysAuthorization (); // works in background
					//_locationManager.RequestWhenInUseAuthorization (); // only in foreground
				} else {
					_locationManager.StartUpdatingLocation ();
					_locationManager.StopUpdatingLocation ();
				}
			} else if (CLLocationManager.Status == CLAuthorizationStatus.Denied) {
				// If user denied => Navigate to setting app
				if (DeviceUtil.OSVersion >= 8) {
					if (_isShowPopup == false) {
						_isShowPopup = true;
						PopupManager.ShowConfirm (Translator.Translate ("GPS_PERMISSION_REQUIRED"), 
							Translator.Translate ("PERMISSION_LOCATION_CONTENT"),
							() => {
								UIApplication.SharedApplication.OpenUrl (NSUrl.FromString (UIApplication.OpenSettingsUrlString.ToString ()));
								_isShowPopup = false;
							},
							() => _isShowPopup = false,
							Translator.Translate ("BUTTON_SETTING"),
							Translator.Translate ("BUTTON_CANCEL"),
							true);
					}
				} else {
					if (CLLocationManager.LocationServicesEnabled && _isShowPopup == false) {
						_isShowPopup = true;
						PopupManager.ShowAlert (Translator.Translate ("GPS_PERMISSION_REQUIRED"),
							Translator.Translate ("PERMISSION_LOCATION_INSTRUCTION"),
							showTitle: true,
							submitText: Translator.Translate ("BUTTON_OK"),
							callback: () => _isShowPopup = false);
					}
				}
			}
		}

		private void AuthorizationChanged (object sender, CLAuthorizationChangedEventArgs e)
		{
			if (e.Status == CLAuthorizationStatus.Denied) {
				// Show notification
				System.Diagnostics.Debug.WriteLine (">....................AuthorizationChanged");
				NotifyGpsTurnOff ();
			}
		}

		public void RegisterNofifyGpsOff ()
		{
			if (!_isRegistered) {
				_locationManager.StartUpdatingLocation ();
				_locationManager.StartMonitoringSignificantLocationChanges (); // Still die
				_locationManager.AuthorizationChanged += AuthorizationChanged;
				_isRegistered = true;
				System.Diagnostics.Debug.WriteLine (">....................RegisterNofifyGpsOff done!!!");
			}
		}

		public void UnRegisterNotifyGpsOff ()
		{
			if (_isRegistered) {
				_locationManager.StopMonitoringSignificantLocationChanges ();
				_locationManager.AuthorizationChanged -= AuthorizationChanged;
				_isRegistered = false;
				System.Diagnostics.Debug.WriteLine (">....................UnregisterNofifyGpsOff done!!!");
			}
		}

		private void NotifyGpsTurnOff ()
		{
			System.Diagnostics.Debug.WriteLine (">....................About to send notification");
			// create the notification
			var notification = new UILocalNotification ();

			// set the fire date (the date time in which it will fire)
			notification.FireDate = DateTime.Now.AddSeconds (1).DateTimeToNSDate ();

			// configure the alert stuff
			notification.AlertAction = "DriveRight";
			notification.AlertBody = Translator.Translate ("GPS_OFF_NOTIFICATION"); // "Location Services is disabled!";

			// modify the badge
			notification.ApplicationIconBadgeNumber = ++NumberBagdes;

			// set the sound to be the default sound
			notification.SoundName = UILocalNotification.DefaultSoundName;

			// schedule it
			UIApplication.SharedApplication.ScheduleLocalNotification (notification);
		}
	}
}

