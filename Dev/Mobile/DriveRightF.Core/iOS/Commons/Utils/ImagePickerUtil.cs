﻿using System;
using UIKit;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using DriveRightF.Core;
using ReactiveUI;
using System.Reactive.Concurrency;
using Unicorn.Core.iOS;
using AVFoundation;
using Unicorn.Core.Translation;
using AssetsLibrary;
using Unicorn.Core.UI;
using Foundation;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.iOS.ImagePickerUtil))]
namespace DriveRightF.iOS
{
	public class ImagePickerUtil : IImagePicker
	{
		private UIActionSheet _actionSheet;
		private UIAlertController _alertController;
		private UIImagePickerController _imagePicker;
		private Action<object> _callback;
		private Action _actionInput;
		private nint _buttonIndex = 2;
		//		private

		public UBaseNavigator Navigator
		{
			get
			{
				return (UBaseNavigator)DependencyService.Get<INavigator>();
			}
		}

		public PopupManager PopupManager
		{
			get
			{
				return DependencyService.Get<PopupManager>();
			}
		}

		public ITranslator Translator { get { return DependencyService.Get<ITranslator>(); } }

		public void PickImage(Action<object> callback, Action input = null)
		{
			_callback = callback;
			_actionInput = input;
			CreateImagePicker();
			if(DeviceUtil.OSVersion < 8f)
			{
				CreateActionSheet();
			}
			else
			{
				CreateAlertController();
			}
		}

		public void PickByteImage(Action<byte[]> callback, Action input = null)
		{
			PickImage(x =>
			{
				var uIImage = x as UIImage;
				if(uIImage != null)
				{
					byte[] datas = uIImage.ToNSData();
					if(callback != null)
					{
						callback(datas);
					}
				}
			}, input);
		}

		private void PickImageBelowiOS8(Action<UIImage> callback)
		{
			CreateActionSheet();
		}

		private void PickImageiOS8()
		{
			CreateAlertController();
		}

		private void CreateAlertController()
		{
			if(_alertController == null)
			{
				_alertController = new UIAlertController ();
				_alertController.AddAction(UIAlertAction.Create(Translator.Translate("TAKE_PHOTO"), UIAlertActionStyle.Default, (UIAlertAction obj) => ShowImagePicker(UIImagePickerControllerSourceType.Camera)));
				_alertController.AddAction(UIAlertAction.Create(Translator.Translate("CHOOSE_FROM_GALLERY"), UIAlertActionStyle.Default, (UIAlertAction obj) => ShowImagePicker(UIImagePickerControllerSourceType.PhotoLibrary)));
				_alertController.AddAction(UIAlertAction.Create(Translator.Translate("BUTTON_CANCEL"), UIAlertActionStyle.Cancel, delegate(UIAlertAction obj)
				{
					if(_callback != null)
					{
						_callback(null);
					}
				}));
			}
			Navigator.NavigationController.PresentViewController(_alertController, true, () =>
			{
				Console.WriteLine("PresentViewController");
			});
		}

		private void HandleActionSheetClick(nint buttonIndex)
		{
			if(buttonIndex == 2)
			{
				if(_callback != null)
				{
					_callback(null);
				}
				return;
			}
			UIImagePickerControllerSourceType type = buttonIndex == 1 ? UIImagePickerControllerSourceType.PhotoLibrary : UIImagePickerControllerSourceType.Camera;
			ShowImagePicker(type);
		}

		private void CreateActionSheet()
		{
			if(_actionSheet == null)
			{
				_actionSheet = new UIActionSheet ("");
				_actionSheet.AddButton(Translator.Translate("TAKE_PHOTO"));
				_actionSheet.AddButton(Translator.Translate("CHOOSE_FROM_GALLERY"));
				_actionSheet.AddButton(Translator.Translate("BUTTON_CANCEL"));
				_actionSheet.CancelButtonIndex = 2;
				_actionSheet.Clicked += (object sender, UIButtonEventArgs e) =>
				{

					//TODO: BUG can't show popup when actionSheet show.
					_buttonIndex = e.ButtonIndex;
//					HandleActionSheetClick (e.ButtonIndex);
				};
					
				_actionSheet.Dismissed += (object sender, UIButtonEventArgs e) =>
				{
					HandleActionSheetClick(_buttonIndex);
				};
			} 
			_actionSheet.ShowInView(Navigator.Window);
		}

		private void CreateImagePicker()
		{
			if(_imagePicker == null)
			{
				_imagePicker = new UIImagePickerController ();
				_imagePicker.FinishedPickingMedia += OnImagePicked;
				_imagePicker.Canceled += OnCancelled;
			}
		}

		private void OnImagePicked(object sender, UIImagePickerMediaPickedEventArgs e)
		{
			_imagePicker.DismissViewController(true, () => RxApp.TaskpoolScheduler.Schedule(() =>
			{
				if(_actionInput != null)
				{
					_actionInput();
				}

				if(_callback != null)
				{
					//UIImage imageResize = e.OriginalImage.ReSizeImage(2048);
					_callback(e.OriginalImage);
				}
			})
			);
		}

		private void OnCancelled(object sender, EventArgs e)
		{
			_imagePicker.DismissViewController(true, null);
		}

		void HandleImagePicker(UIImagePickerControllerSourceType type)
		{
			if(UIImagePickerController.IsSourceTypeAvailable(type))
			{
				_imagePicker.SourceType = type;
				_imagePicker.MediaTypes = UIImagePickerController.AvailableMediaTypes(type);
			}
			Navigator.NavigationController.PresentViewController(_imagePicker, true, null);
		}

		private void ShowImagePicker(UIImagePickerControllerSourceType type)
		{
			//TODO: check permission to show alert camera or gallery in iOS7 & 8.
			// Don't need check ios 6. App don't support ios <=6
			if(type == UIImagePickerControllerSourceType.Camera)
			{
				AVAuthorizationStatus status = AVCaptureDevice.GetAuthorizationStatus(AVMediaType.Video);
				if(status == AVAuthorizationStatus.Denied)
				{
					ShowPopupRequiredPermission(type);
				}
				else
				{
					HandleImagePicker(type);
				}

			}
			else if(type == UIImagePickerControllerSourceType.PhotoLibrary)
			{
				ALAuthorizationStatus status = ALAssetsLibrary.AuthorizationStatus;
				if(status == ALAuthorizationStatus.Denied)
				{
					ShowPopupRequiredPermission(type);
				}
				else
				{
					HandleImagePicker(type);
				}
			}	
		}

		private void ShowPopupRequiredPermission(UIImagePickerControllerSourceType type)
		{
			//TODO: check can open setting app
			bool isCanOpenSetting = false;
			NSUrl url = null;
			NSString urlString = UIApplication.OpenSettingsUrlString;
			if(urlString != null)
			{
				url = new NSUrl (urlString);
				isCanOpenSetting = UIApplication.SharedApplication.CanOpenUrl(url);
			}

			//TODO: set content string of popup
			string contentText = string.Empty;
			string titleText = string.Empty;

			if(type == UIImagePickerControllerSourceType.Camera)
			{
				titleText = Translator.Translate("PERMISSION_CAMERA");
				contentText = Translator.Translate("PERMISSION_CAMERA_CONTENT");
			}
			else if(type == UIImagePickerControllerSourceType.PhotoLibrary)
			{
				titleText = Translator.Translate("PHOTO_ACCESS_REQUIRED");
				contentText = Translator.Translate("PHOTO_ACCESS_CONTENT");
			}

			//TODO: show popup or alert
			if(isCanOpenSetting)
			{
				PopupManager.ShowConfirm(titleText, contentText, submitAction: () =>
				{
					UIApplication.SharedApplication.OpenUrl(url);
				}, cancelAction: null, submitText: Translator.Translate("BUTTON_SETTING"), cancelText: Translator.Translate("BUTTON_DISMISS"), showTitle: true);
			}
			else
			{
				PopupManager.ShowAlert(titleText, contentText, callback: () =>
				{
					Console.WriteLine("Dismiss");
				}, submitText: Translator.Translate("BUTTON_DISMISS"));
			}
		}
	}
}

