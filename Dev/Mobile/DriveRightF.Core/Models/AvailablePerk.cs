﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unicorn.Core.Bases;
using Newtonsoft.Json;

namespace DriveRightF.Core.Models
{
	public class AvailablePerk : BaseModel
	{
		public string PerkId { get; set;}
		public string Icon { get; set;}
	}
}

