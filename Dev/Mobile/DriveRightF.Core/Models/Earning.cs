﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unicorn.Core.Bases;
using Newtonsoft.Json;

namespace DriveRightF.Core.Models
{
	public class Earning : BaseModel, ICheckChangeModel
    {


		private const string ICON_DEFAULT = "award_coffee.png";
		private const string IMAGE_DEFAULT = "coffee_snob.jpg";

		private string _icon = string.Empty;
		private string _image = string.Empty;

		[JsonProperty(PropertyName="uuid")]
		public string EarningId { get; set;}

		[JsonProperty(PropertyName="name")]
        public string Name { get; set; }

		[JsonIgnore]
		public string Icon { 
			get { 
				if (string.IsNullOrEmpty(IconURL)) {
					return ICON_DEFAULT;
				}
				return _icon;
			} 
			set
			{
				_icon = value;
			} 
		}

		[JsonIgnore]
		public string Image { 
			get { 
				if (string.IsNullOrEmpty(ImageURL)) {
					return IMAGE_DEFAULT;
				}
				return _image;
			} 
			set
			{
				_image = value;
			} 
		}

		[JsonIgnore]
		public string IconURL { get; set; }

		[JsonIgnore]
		public string ImageURL { get; set; }

		[JsonIgnore]
		public string BriefDesc { get; set; }

		[JsonProperty(PropertyName="description")]
        public string Description { get; set; }

		[JsonProperty(PropertyName="reward")]
		public string Archieve { get; set; }

		[JsonIgnore]
		public string Get { get; set; }

		[JsonProperty(PropertyName="begin")]
        public DateTime StartTime { get; set; }

		[JsonIgnore]
        public DateTime EndTime { get; set; }

		[JsonProperty(PropertyName="progress")]
        public int Progress { get; set; }

		[JsonIgnore]
        public string AvailablePerks { get; set; }

		[JsonIgnore]
		public string GainedPerks { get; set; }

		[JsonIgnore]
        public EarningStatus Status { get; set; }

		[JsonProperty(PropertyName="status")]
		public string StatusRaw { get; set; }

		[JsonProperty(PropertyName="modified")]
		public DateTime UpdateTime { get; set; }

		[JsonIgnore]
		public ItemStatus ItemStatus { get; set; }

		[JsonIgnore]
		public DateTime Modified {
			get {
				return UpdateTime;
			}
		}

		[JsonIgnore]
		public string ServerId {
			get {
				return EarningId;
			}
		}
    }
}
