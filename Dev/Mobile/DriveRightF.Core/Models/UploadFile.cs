﻿using System;
using Newtonsoft.Json;

namespace DriveRightF.Core.Models
{
	public class UploadFile
	{
		[JsonProperty(PropertyName="image_content_type")]
		public string ImageContentType{ get;set;}
		[JsonProperty(PropertyName="image64")]
		public string Image64 { get; set;}
	}
}

