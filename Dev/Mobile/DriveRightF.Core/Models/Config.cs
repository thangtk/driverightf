﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unicorn.Core.Bases;

namespace DriveRightF.Core.Models
{
    public class Config : BaseModel
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
