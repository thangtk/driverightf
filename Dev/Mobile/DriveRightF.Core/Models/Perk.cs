﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unicorn.Core.Bases;
using Newtonsoft.Json;
using Unicorn.Core.Storages;

namespace DriveRightF.Core.Models
{
	public class Perk : BaseModel, ICheckChangeModel
    {

		private const string ICON_DEFAULT = "award_coffee.png";
		private const string IMAGE_DEFAULT = "coffee_snob.jpg";

		private string _icon = string.Empty;
		private string _image = string.Empty;


		[JsonProperty(PropertyName="uuid")]
		public string PerkId { get; set;}
		[JsonProperty(PropertyName="name")]
        public string Name { get; set; }

		[JsonIgnore]
        public string Icon { 
			get { 
				if (string.IsNullOrEmpty(IconURL)) {
					return ICON_DEFAULT;
				}
				return _icon;
			} 
			set
			{
				_icon = value;
			} 
		}

		[JsonIgnore]
		public string Image { 
			get { 
				if (string.IsNullOrEmpty(ImageURL)) {
					return IMAGE_DEFAULT;
				}
				return _image;
			} 
			set
			{
				_image = value;
			} 
		}

		public string IconURL { get; set; }

		public string ImageURL { get; set; }

		[JsonProperty(PropertyName="qr_code")]
        public string QRCode { get; set; }

		[Ignore]
		[JsonProperty(PropertyName="sponsor")]
		public string SponsorHref { get; set; }

		[JsonIgnore]
		public string SponsorId { get; set; }

		[JsonIgnore]
		public string Sponsor { get; set; }

		[JsonProperty(PropertyName="expiry")]
        public DateTime Expiry { get; set; }

		public string BriefDesc { get; set; }

		[JsonProperty(PropertyName="description")]
        public string Description { get; set; }

        public bool Used { get; set; }
		[JsonProperty(PropertyName="modified")]
		public DateTime UpdatedTime { get; set; }

		[JsonIgnore]
		public ItemStatus ItemStatus { get; set; }

		[JsonIgnore]
		public DateTime Modified {
			get {
				return UpdatedTime;
			}
		}

		[JsonIgnore]
		public string ServerId {
			get {
				return PerkId;
			}
		}
    }
}
