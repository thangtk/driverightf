﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unicorn.Core.Bases;

namespace DriveRightF.Core.Models
{
    public class Insurance : BaseModel
    {
        public string SponsorId { get; set; }

        public string PolicyNo { get; set; }

        public string Insurer { get; set; }

        public DateTime Expiry { get; set; }

        public int DriverLicenseId { get; set; }

        public int CarRegistrationId { get; set; }
    }
}
