﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DriveRightF.Core.Enums;
using Newtonsoft.Json;
using Unicorn.Core.Bases;
using Unicorn.Core.Storages;

namespace DriveRightF.Core.Models
{
	public class Sponsor : BaseModel, ICheckChangeModel
    {
		private const string ICON_DEFAULT = "award_coffee.png";
		private const string IMAGE_DEFAULT = "coffee_snob.jpg";

		private string _icon = string.Empty;
		private string _image = string.Empty;

		[JsonProperty(PropertyName="uuid")]
		public string SponsorID { get; set; }
		[JsonProperty(PropertyName="sponsor_uuid")]
		public string SponsorRawID { get; set; }
		public string Href { get; set; }

		[JsonProperty(PropertyName="sponsor_name")]
        public string Name { get; set; }

//		[Ignore]
//		[JsonProperty(PropertyName="sponsor")]
//		public string SponsorUrl { get; set; }
		[JsonIgnore]
		public string Icon { 
			get { 
				if (string.IsNullOrEmpty(IconURL)) {
					return ICON_DEFAULT;
				}
				return _icon;
			} 
			set
			{
				_icon = value;
			} 
		}

		[JsonIgnore]
		public string Image { 
			get { 
				if (string.IsNullOrEmpty(ImageURL)) {
					return IMAGE_DEFAULT;
				}
				return _image;
			} 
			set
			{
				_image = value;
			} 
		}

		public string IconURL { get; set; }

		public string ImageURL { get; set; }

		[JsonProperty(PropertyName="sponsor_type")]
		public string RawType { get; set; }

        public SponsorType Type { get; set; }

        public SponsorStatus Status { get; set; }

		[JsonProperty(PropertyName="modified")]
		public DateTime UpdateTime { get; set; }
		[JsonProperty(PropertyName="num_contracts")]
		public int NumContracts { get; set; }


		[JsonIgnore]
		public ItemStatus ItemStatus { get; set; }

		public DateTime Modified {
			get {
				return UpdateTime;
			}
		}

		public string ServerId {
			get {
				return SponsorID;
			}
		}
    }
}
