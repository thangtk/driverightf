﻿using System;
using Unicorn.Core.Bases;
using DriveRightF.Core.Enums;

namespace DriveRightF.Core
{
	public class LeftMenuItem : BaseModel
	{
		public string Title {
			get;
			set;
		}

		public string Icon {
			get;
			set;
		}

		public Screen? Screen {
			get;
			set;
		}
	}
}

