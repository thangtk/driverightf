﻿using System;
using Unicorn.Core.Bases;
using Newtonsoft.Json;

namespace DriveRightF.Core.Models
{
	public class Contract: BaseModel
	{

		[JsonProperty(PropertyName="policy_no")]
		public string PolicyNo { get; set; }

		[JsonProperty(PropertyName="policy_insurer")]
		public string Insurer { get; set; }

		[JsonProperty(PropertyName="modified")]
		public DateTime UpdateTime { get; set; }

		[JsonProperty(PropertyName="licence_no")]
		public string LicenseNo { get; set; }

		[JsonProperty(PropertyName="car")]
		public string CarModel { get; set; }

		[JsonProperty(PropertyName="car_no")]
		public string CarNo { get; set; }

		[JsonProperty(PropertyName="policy_expiry")]
		public DateTime PolicyExpiry { get; set; }

		[JsonProperty(PropertyName="licence_expiry")]
		public DateTime LicenceExpiry { get; set; }
	}

	public class ContractSumary: BaseModel
	{
		[JsonProperty(PropertyName="uuid")]
		public string ContractId { get; set; }

		[JsonProperty(PropertyName="policy_no")]
		public string PolicyNo { get; set; }

		[JsonProperty(PropertyName="policy_insurer")]
		public string Insurer { get; set; }

		[JsonProperty(PropertyName="modified")]
		public DateTime UpdateTime { get; set; }
	}
}

