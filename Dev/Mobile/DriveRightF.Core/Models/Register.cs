﻿using System;
using Newtonsoft.Json;

namespace DriveRightF.Core.Models
{
	public class Register
	{
		[JsonProperty(PropertyName="partner_ref")]
		public string Partner { get; set; }
		[JsonProperty(PropertyName="customer_ref")]
		public string Customer { get; set; }
	}
}

