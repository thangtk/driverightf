﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unicorn.Core.Bases;

namespace DriveRightF.Core.Models
{
    public class DriverLicense : BaseModel
    {
		public string SponsorId {get;set;}

        public string LicenseNo { get; set; }

		public DateTime LicenseExpiry { get; set; }

		public string Insurer { get; set; }
    }
}
