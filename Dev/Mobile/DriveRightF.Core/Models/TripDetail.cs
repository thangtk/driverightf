﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unicorn.Core.Bases;
using Newtonsoft.Json;

namespace DriveRightF.Core.Models
{
    public class TripDetail : BaseModel
    {
		[JsonProperty(PropertyName="uuid")]
        public string TripId { get; set; }
        // Some information of GPS here
		[JsonProperty(PropertyName="gps_points")]
		public List<GPS> Coordinates {
			get;
			set;
		}
    }

	public class GPS : BaseModel {

		public string TripId { get; set; }

		public double Latitude { get; set;}

		public double Longitude { get; set;}

		public DateTime Time { get; set;}
	}
}
