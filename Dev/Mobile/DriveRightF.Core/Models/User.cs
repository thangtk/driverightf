﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unicorn.Core.Bases;
using Newtonsoft.Json;

namespace DriveRightF.Core.Models
{
    public class User : BaseModel
    {
		[JsonIgnore]
		public string UserId { get; set; }
		[JsonProperty(PropertyName="name")]
        public string Name { get; set; }
		[JsonProperty(PropertyName="social_security")]
        public string SocialSecurityNo { get; set; }
		[JsonIgnore]
        public int Age { get; set; }
		[JsonProperty(PropertyName="dob")]
		public string DOB { get; set; }
		[JsonProperty(PropertyName="phone")]
        public string PhoneNumber { get; set; }
		[JsonIgnore]
        public string Image { get; set; }
		[JsonIgnore]
		public string ImageURL { get; set; }
		[JsonIgnore]
        public string Password { get; set; }
		[JsonProperty(PropertyName="wechat")]
        public string WeChat { get; set; }
    }

	public class ProfileSumary : BaseModel{
		public string Uuid { get; set; }
		public string Href { get; set; }
		public string Name { get; set; }
	}
}
