﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unicorn.Core.Bases;

namespace DriveRightF.Core.Models
{
    public class CarRegistration : BaseModel
    {
		public string SponsorId {get;set;}

        public string CarModel { get; set; }

        public string CarNo { get; set; }
    }
}
