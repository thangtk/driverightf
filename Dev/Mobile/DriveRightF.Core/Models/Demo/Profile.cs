﻿using System;
using Unicorn.Core.Bases;

namespace DriveRightF.Core.Models
{

	// May be not need because it'll be storage to Config
	public class Profile : BaseModel
	{
		public string ProfileID { get; set; }
		public string Name{ get; set;}
		public string Image { get; set;}
		public string ImageUrl { get; set;}
		public string PhoneNumber { get; set;}
		public string NumberPlate { get; set; }
		public string PaymentAccount { get; set;}
		public string PaymentType { get; set;} // alipay & wechat pay & none
		public string PaymentName { get; set;}
		public string PaymentPassword { get; set;}
	}
}

