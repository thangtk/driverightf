﻿using System;
using Newtonsoft.Json;
using Unicorn.Core.Storages;
using Unicorn.Core.Bases;

namespace DriveRightF.Core
{
	public class Award: BaseModel
		{

			private const string ICON_DEFAULT = "award_coffee.png";
			private const string IMAGE_DEFAULT = "coffee_snob.jpg";

			public string AwardId { get; set;}

			public string Type { get; set;} // 1-> card 2=> award

			public string Unit { get; set;} // Card and RMB

			public float Value { get; set;} // 1 with award and 3.99 with card

			public string Name { get; set; }

			public string ImageURL { get; set; } // image in detail screen

			public string IconURL { get; set; } // Icon at home

			public string QRCode { get; set; }

			public string Sponsor { get; set; }

			public DateTime Expiry { get; set; }

			public string Description { get; set; }

			public bool Used { get; set; }

			public DateTime UpdatedTime { get; set; }


	}
}

