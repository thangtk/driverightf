﻿using System;
using Unicorn.Core.Bases;

namespace DriveRightF.Core.Models
{
	public class Account: BaseModel
	{
		public string AccountId { get; set;}

		public float StartBalance { get; set;}

		public float EndBalance { get; set;}

		public float StartCard { get; set;}

		public float EndCard { get; set;}

		public float CashEarned { get; set;}

		public float CardEarned{ get; set;} // total card 

		public string Month { get; set;} // format 2014-08 => thang 8 nam 2014
	}

	public class AccountTransaction : BaseModel
	{
		public string AccountId { get; set;} // account uuid

		public DateTime Time { get; set;}

		public string Description { get; set;} // GAMBLE, CASH, AWARDS ...

		public string Type { get; set;} // GAMBLE, CASH, AWARDS ...

		public float Value { get; set;}

		public string Unit { get; set;} // RMB,Card
	}
}

