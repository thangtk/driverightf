﻿using System;
using Unicorn.Core.Bases;

namespace DriveRightF.Core
{
	public class Notification : BaseModel
	{
		public string Description { get; set; }
		public DateTime UpdateTime { get; set; }
	}
}

