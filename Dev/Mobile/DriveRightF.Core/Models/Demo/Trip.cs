﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unicorn.Core.Bases;
using Unicorn.Core.Storages;
using Newtonsoft.Json;

namespace DriveRightF.Core.Models
{
	public class TripSummary : BaseModel, ICheckChangeModel
	{
		[JsonProperty (PropertyName = "uuid")]
		public string TripId { get; set; }

		[JsonProperty (PropertyName = "duration")]
		public long Duration { get; set; }

		[JsonProperty (PropertyName = "distance")]
		public double Distance { get; set; }

		[JsonProperty (PropertyName = "type")]
		public string Type { get; set; }

		[JsonProperty (PropertyName = "trip_score")]
		public double Score { get; set; }

		[JsonProperty (PropertyName = "utc_start")]
		public DateTime StartTime { get; set; }

		#region ICheckChangeModel implementation

		public ItemStatus ItemStatus { get; set; }

		public DateTime Modified { get; set; }

		public string ServerId { get { return TripId; } }

		#endregion
	}

	public class TripMonthSummary: BaseModel
	{
		[JsonProperty (PropertyName = "mmyy")]
		public string MonthRaw { get; set; }

		[JsonIgnore]
		public DateTime Month { get; set; }

		[JsonProperty (PropertyName = "overall_score")]
		public double Score { get; set; }

		[JsonProperty (PropertyName = "num_trips")]
		public int TripCount { get; set; }
	}

	public class Trip : BaseModel
	{
		public string TripId { get; set; }

		public DateTime UpdatedTime { get; set; }

		public DateTime StartTime { get; set; }

		public long Duration { get; set; }

		public DateTime EndTime { get; set; }

		public double Distance { get; set; }

		public double AverageSpeed { get; set; }

		public double Score { get; set; }

		public bool Used { get; set; }

		public DateTime UpdatedDate { get; set; }

		public double SmoothScore { get; set; }

		public double SpeedScore{ get; set; }

		public double DistractionScore{ get; set; }

		public double TimeOfDayScore{ get; set; }

		[Ignore]
		public List<GPS> Coordinates {
			get;
			set;
		}
	}

	public class TripMonth : BaseModel
	{

		public DateTime Month { get; set; }
		// 2014-08 => thang 8 nam 2014

		public float Score { get; set; }
		// total score

		public float SmoothScore { get; set; }

		public double SpeedScore{ get; set; }

		public double DistractionScore{ get; set; }

		public double TimeOfDayScore{ get; set; }

		public double TotalDistance { get; set; }

		public double TotalDuration { get; set; }

		public double AverageSpeed { get; set; }

		public double AverageDistance{ get; set; }
		// Average Distance per trip

		public double AverageDuration{ get; set; }
		// Average Duration per trip

		public int TripCount { get; set; }

		public string ImageUrl { get; set; }

		public string City { get; set; }

	}

}
