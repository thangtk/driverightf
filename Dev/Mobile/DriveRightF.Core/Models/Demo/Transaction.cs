﻿using System;
using Unicorn.Core.Bases;

namespace DriveRightF.Core
{
	public enum TransactionType
	{
		Awards,
		Gamble,
		WidthDraw,
		TakeCash,
	}

	public class Transaction: BaseModel
	{
		public string Description { get; set; }
		public DateTime StartTime { get; set; }
		public TransactionType TransactionType { get; set; }
		public float Value { get; set; }
		public string Unit { get; set; }

	}
}

