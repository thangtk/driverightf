﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unicorn.Core.Bases;
using Unicorn.Core.Storages;
using Newtonsoft.Json;

namespace DriveRightF.Core.Models
{
	public class Trip : BaseModel, ICheckChangeModel
    {
		[JsonProperty(PropertyName="uuid")]
		public string TripId { get; set; }

		public DateTime UpdatedTime { get; set; }

		[JsonProperty(PropertyName="utc_start")]
		public DateTime StartTime { get; set; }

		[JsonProperty(PropertyName="duration")]
		public long Duration { get; set; }

		[JsonProperty(PropertyName="utc_end")]
		public DateTime EndTime { get; set; }

//		[IgnoreAttribute]
//		public TimeSpan Duration { get; set; }
		[JsonProperty(PropertyName="distance")]
		public double Distance { get; set; }

		public double AverageSpeed { get; set; }

		[JsonProperty(PropertyName="trip_score")]
		public double Score { get; set; }

		public bool Used { get; set; }

		public DateTime UpdatedDate { get; set; }

		public double SmoothScore { get; set;}

		public double SpeedScore{ get; set;}

		public double DistractionScore{ get; set;}

		public double TimeOfDayScore{ get; set;}

		#region ICheckChangeModel implementation
		public ItemStatus ItemStatus {
			get;
			set;
		}
		public DateTime Modified {
			get {
				return UpdatedDate;
			}
		}
		public string ServerId {
			get {
				return TripId;
			}
		}
		#endregion
		[Ignore]
		public List<GPS> Coordinates {
			get;
			set;
		}
    }
}
