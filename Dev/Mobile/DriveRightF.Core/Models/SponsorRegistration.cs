﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unicorn.Core.Bases;

namespace DriveRightF.Core.Models
{
	public class SponsorRegistration 
	{
		public CarRegistration CarRegistration {
			get;
			set;
		}
		public DriverLicense DriverLicense {
			get;
			set;
		}
		public Insurance Insurance {
			get;
			set;
		}
	}
}
