﻿using System;
using Unicorn.Core.Bases;

namespace DriveRightF.Core
{
	public interface ICheckChangeModel
	{
		ItemStatus ItemStatus { get; set; }

		DateTime Modified { get; }

		string ServerId { get; }
	}
}

