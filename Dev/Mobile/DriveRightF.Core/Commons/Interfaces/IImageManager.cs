﻿using System;
using System.Drawing;
using System.Threading.Tasks;

namespace DriveRightF.Core
{
	public interface IImageManager
	{
		string SaveImage(object image, string imageName = null);
		string SaveImage(byte[] image, string imageName = null);
		bool DeleteImage (string namePath);
		object CropResizeImage(object image, SizeF size, RectangleF rec);

		byte[] GetImageData(string path);
		string GetImageBase64(string path);
		Task<string> DownloadImage(string imageUrl, string imageName);
	}
}

