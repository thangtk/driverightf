﻿using System;
using System.Drawing;
using DriveRightF.Core.Enums;

namespace DriveRightF.Core
{
	public interface ITile
	{
		Size Size { get; set; }

		Point Position { get; set; }

		string Colors { get; set; }

		string Image { get; set; }
	}
}

