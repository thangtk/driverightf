﻿using System;
using Unicorn.Core.Services.Base;

namespace DriveRightF.Core
{
	public class SDKResponse : UBaseResponse
	{
	}

	public interface IPolaraxSDK
	{
		bool IsEnable {
			get;
		}

		void StartService (string customerId, Action<SDKResponse> response);

		void StartService (Action<SDKResponse> response);

		bool IsEnable3G {
			get;
			set;
		}
	}
}

