﻿using System;

namespace DriveRightF.Core
{
	public interface IImagePicker
	{
		void PickImage (Action<object> imageCallback, Action input = null);
		void PickByteImage (Action<byte[]> imageCallback, Action input = null);
	}
}

