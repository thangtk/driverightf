﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveRight.Core
{
    public interface BaseMapConfig
    {
        MapType MapType { get; set; }

        int MapPage { get; set; }
        
       
        string FilePath { get; }
    }

    public enum MapType
    {
        Nokia,
        Google,
        Baidu
    }
}
