﻿using System;

namespace DriveRightF.Core
{
	public interface ISDK
	{
	}

	public interface ISDK<T> : ISDK
	{
		void StartSevice(string customerId , Action<T> response);
		void SetCustomerId(string customerId, Action<T> response);
	}
}

