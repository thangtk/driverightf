﻿using System;

namespace DriveRightF.Core
{
	public interface IApplication
	{
		void NavigateToLocationSetting (string url);
	}
}

