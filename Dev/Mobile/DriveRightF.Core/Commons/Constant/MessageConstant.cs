﻿using System;

namespace DriveRightF.Core.Constants
{
	public static class MessageConstant
	{
		/// <summary>
		/// Send: AwardDetail Screen
		/// Subcrible: HomeAward screen
		/// UseFor: sort cell metro when award is used.
		/// </summary>
		public const string AWARD_IS_USED = "Award.isUsed";


		/// <summary>
		/// Send: SlotManchine Screen when finish play card.
		/// Subcrible: Home Award Screen, Home Account screen
		/// UseFor: remove card cell in home award screen and update balance of home account screen
		/// </summary>
		public const string CARD_GAMBLE = "Card.Gamble";

		/// <summary>
		/// Send: CardDetail Screen when click button take money
		/// Subcrible: Home Account screen & Home Award screen.
		/// UseFor: remove card cel in home award screen and update balance of home account screen
		/// </summary>
		public const string CARD_TAKE_MONEY = "Card.TakeMoney";

		/// <summary>
		/// Send: AccountDetail Screen when click Pay button
		/// Subcrible: Home Account Screen
		/// UseFor: update value of balance account (card, cash) for Home Account screen
		/// </summary>
		public const string CARD_WITH_DRAW_CASH = "Card.WithDrawCash";

		/// <summary>
		/// Send: CoinAnimateView when animate.
		/// Subcrible: TabView
		/// UseFor: aniamte award tab item.
		/// </summary>
		public const string COIN_ANIMATE = "Coin.Animate";

		/// <summary>
		/// Send: Profile Screen when update profile completely.
		/// Subcrible: Menu Screen
		/// UseFor: update image icon user for menu screen
		/// </summary>
		public const string PROFILE_UPDATE = "Profile.Update";

		/// <summary>
		/// Send: BaseViewModel.
		/// Subcrible: Home Screen
		/// UseFor: Block or UnBlock UI on Main Home Screen
		/// </summary>
		public const string SCREEN_BLOCK = "Screen.Block";

		/// <summary>
		/// Send: Screen need to block the tab view
		/// Subscribe: Home screen
		/// UseFor: Block tab screen
		/// </summary>
		public const string TAB_BAR_BLOCK = "Tab.Block";

		/// <summary>
		/// Send: Screen need to back to home
		/// Subscribe: Gamble screen
		/// UseFor: Navigate to award home screen
		/// </summary>
		public const string NAVIGATE_TO_HOME = "Screen.NavigateHome";
	}
}

