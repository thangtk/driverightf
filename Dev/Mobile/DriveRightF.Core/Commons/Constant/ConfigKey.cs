﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveRightF.Core.Constants
{
    public class ConfigKey
    {
		public const string APP_USER_ID = "app_user_id";
        public const long REFRESH_SERVICE_NORMAL = 10000;

		// Key for perks
//		public const long PERTS_DURATION_REFRESH = 10000;
//		public const string PERTS_TIMESTAMP = "perks_timestamp";
		public const string PERK_LAST_UPDATED = "perk_last_update";
//		public const string PERTS_LASTED_TIME_CALL_SEVICE = "perts_time_call_service";

		// Key for sponsors
		public const string SPONSOR_LAST_UPDATED = "sponsor_last_update";
//		public const string SPONSORS_LASTED_TIME_CALL_SEVICE = "sponsors_time_call_service";

		// Key for Trips
		public const string TRIPS_TIMESTAMP = "trips_timestamp";
		public const string TRIPS_LASTED_TIME_CALL_SEVICE = "trips_time_call_service";

		// Earning
		public const string EARNING_LAST_UPDATED = "earning_last_update";

        // Key for profile
		public const string PROFILE_SYNCED = "profile_exist";
        public const string PROFILE_NAME = "profile_name";
		public const string PROFILE_ID = "profile_id";
		public const string PROFILE_AGE = "profile_age";
		public const string PROFILE_PHONE_NUMBER = "profile_phone_number";
		public const string PROFILE_SOCIAL_SECURITY_NUMBER = "profile_social_security_number";
		public const string PROFILE_IMAGE = "profile_image";
		public const string PROFILE_PASSWORD = "profile_password";
		public const string PROFILE_WECHAT = "profile_wechat";
		public const string PROFILE_IMAGE_URL = "profile_image_url";
		public const string PROFILE_DOB = "profile_dob";

		public const string CUSTOMER_REF = "customer_ref";

		// App config
		public const string DATABASE_NAME = "DriveRight.db";
		public const string DATABASE_VERSION = "1.0";
    }
}
