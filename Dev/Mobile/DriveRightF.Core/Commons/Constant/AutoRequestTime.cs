﻿using System;

namespace DriveRightF.Core
{
	public class AutoRequestTime
	{
		public const long REQUEST_TIME_TRIP = 10000; // in milliseconds
		public const long REQUEST_TIME_SPONSOR = 10000; // in milliseconds
		public const long REQUEST_TIME_PERK = 10000; // in milliseconds
		public const long REQUEST_TIME_EARNING = 10000; // in milliseconds
	}
}

