﻿using System;

namespace DriveRightF.Core.Enums
{
	public enum ViewState{
		None,
		NoDataFound,
		Requesting,
		DataDisplaying,
	}
}

