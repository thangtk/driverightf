﻿using System;

namespace DriveRightF.Core
{
	public enum ApplicationStatus : int
	{
		New,
		AtFirstTime,
		AtOtherTimes
	}
}

