﻿using System;

namespace DriveRightF.Core
{
	public enum EarningStatus
	{
		New = 0,
		InProgress,
		Done,
		Expired,
		None,
	}
}

