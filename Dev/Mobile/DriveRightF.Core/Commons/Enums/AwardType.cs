﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DriveRightF.Core.Enums
{
    public enum AwardType : int
    {
        CardSmall = 0,
		CardLarge,
		AwardSmall,
		AwardLarge
    }
}
