﻿using System;

namespace DriveRightF.Core
{
	public enum AnimationStyle
	{
		LeftToRight,
		FromBottom,
		ExpandFromCenter
	}
}

