﻿using System;

namespace DriveRightF.Core.Enums
{
	public enum Screen
	{
		None,
		GetStart,
		Login,
		Signup,
		SecondSignup,
		ThirdSignup,
		Home,
		Perks,
		Trips,
		TripDetail,
		Earnings,
		Sponsors,
		Splash,
		AwardDetail,
		Profile,
		SponsorRegistration,
		EarningDetail,
		Award,
		CardDetail,
		HomeAward,
		HomeAccount,
		HomeTrips,
		HomeProfile,
		HomeMore,

		TabAward,
		TabAccount,
		TabTrips,
		TabProfile,
		TabMore,
		TabMain,

		Notification,
		Menu,
		Test,
		TripStatement,
		Setting,
		About,
		Help,
		Legal,
		Gamble,
		SlotMachine,
		AccountDetail,
		TripStatementInfo,
		AccountStatement,
		HomeAward2,
		NewHome,
		Disclaimer,
		ProductIntroduction
	}
}

