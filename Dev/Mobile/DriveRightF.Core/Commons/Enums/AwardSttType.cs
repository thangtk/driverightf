﻿using System;

namespace DriveRightF.Core.Enums
{
    public enum AwardSttType : int
    {
        InvalidPolicy = 0,
        ValidPolicy = 1,
        Used = 2,
        NotUsed = 3,
    }
}

