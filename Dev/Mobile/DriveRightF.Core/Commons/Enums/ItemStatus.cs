﻿using System;

namespace DriveRightF.Core
{
	public enum ItemStatus
	{
		None,
		New,
		Modified,
		Deleted,
	}
}

