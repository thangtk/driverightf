﻿using System;

namespace DriveRightF.Core
{
	public enum ScoreStatus
	{
		None,
		Height,
		Medium,
		Low,
	}

	public class ScoreUtils
	{
		public static ScoreStatus GetScoreStatus(float score)
		{
			if (score < 60) {
				return ScoreStatus.Low;
			} else if (score < 80) {
				return ScoreStatus.Medium;
			} else {
				return ScoreStatus.Height;
			}
		}

		public static string GetScoreColor(ScoreStatus status)
		{
			if (status == ScoreStatus.Height)
				return "9edc6b";
			if (status == ScoreStatus.Medium)
				return "ffbd52";
			if (status == ScoreStatus.Low)
				return "f76b6b";
			return "";
		}

		public static string GetScoreColor(float score)
		{
			var status = GetScoreStatus(score);
			return GetScoreColor(status);
		}
	}
}

