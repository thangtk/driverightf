﻿using System;
using Unicorn.Core;
using Unicorn.Core.Translation;
using DriveRight.Core;
using System.Reflection;
using System.IO;
using System.Resources;
using System.Globalization;

[assembly: Unicorn.Core.Dependency.Dependency (typeof(DRTranslator))]
namespace DriveRight.Core
{
	public class DRTranslator : BaseTranslator
	{
		public DRTranslator () : base()
		{
		}

		#region implemented abstract members of BaseTranslator

		protected override Assembly GetResourceAssembly ()
		{
			return typeof(DRTranslator).Assembly;
//			return Assembly.GetExecutingAssembly ();
		}

		/// <summary>
		/// Path of folder where resource files are in
		/// Separate by dot charactor (.)
		/// </summary>
		/// <returns>The resource domain.</returns>
		protected override string GetResourceDomain ()
		{
			return "Languages";
		}

		#endregion
    }
}

