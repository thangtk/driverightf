using System;

namespace DriveRight.Core
{
    public static class GPSConverter
    {
        private const double PI = 3.14159265358979323;

        //
        // Krasovsky 1940
        //
        // a = 6378245.0, 1/f = 298.3
        // b = a * (1 - f)
        // ee = (a^2 - b^2) / a^2;
        private const double a = 6378245.0;
        private const double ee = 0.00669342162296594323;

        //
        // World Geodetic System ==> Mars Geodetic System ==> Baidu GPS
        public static void ConvertToCorrectedGPS(double wgLat, double wgLng, out double mgLat, out double mgLng)
        {
            // Ho Sy Ky check outsidechina when build app 21-11-2014
            if (false)
            {
                mgLat = wgLat;
                mgLng = wgLng;
                return;
            }
//            if (IsOutsideChina(wgLat, wgLng))
//            {
//                mgLat = wgLat;
//                mgLng = wgLng;
//                return;
//            }
			double dLat = ConvertLat(wgLng - 105.0, wgLat - 35.0);
			double dLon = ConvertLong(wgLng - 105.0, wgLat - 35.0);

            double radLat = wgLat / 180.0 * PI;
            double magic = Math.Sin(radLat);
            magic = 1 - ee * magic * magic;
            double sqrtMagic = Math.Sqrt(magic);

            dLat = (dLat * 180.0) / ((a * (1 - ee)) / (magic * sqrtMagic) * PI);
            dLon = (dLon * 180.0) / (a / sqrtMagic * Math.Cos(radLat) * PI);

            double ggLat = wgLat + dLat;
            double ggLng = wgLng + dLon;

            double bdLat, bdLng;
            bd_encrypt(ggLat, ggLng, out bdLat, out bdLng);
            mgLng = bdLng;
            mgLat = bdLat;
        }

//        public static GPS ConvertToCorrectedGPS(GPS gps)
//        {
//            GPS gpsClone = gps.Clone();
//            double lat, lng;
//            ConvertToCorrectedGPS(gpsClone.Lat, gpsClone.Long, out lat, out lng);
//            gpsClone.Lat = lat;
//            gpsClone.Long = lng;
//            return gpsClone;
//        }


        // convert Mars GPS to Baidu GPS
        public static void bd_encrypt(double gg_lat, double gg_lon, out double bd_lat, out double bd_lon)  
        {
            const double x_pi = 3.14159265358979324 * 3000.0 / 180.0;
            double x = gg_lon, y = gg_lat;
            double z = Math.Sqrt(x * x + y * y) + 0.00002 * Math.Sin(y * x_pi);
            double theta = Math.Atan2(y, x) + 0.000003 * Math.Cos(x * x_pi);
            bd_lon = z * Math.Cos(theta) + 0.0065;
            bd_lat = z * Math.Sin(theta) + 0.006;  
        }

        private static bool IsOutsideChina(double lat, double lng)
        {
            if (lng < 72.004 || lng > 137.8347)
                return true;
            if (lat < 0.8293 || lat > 55.8271)
                return true;
            return false;
        }

        private static double ConvertLat(double lat, double lng)
        {
            double ret = -100.0 + 2.0 * lat + 3.0 * lng + 0.2 * lng * lng + 0.1 * lat * lng + 0.2 * Math.Sqrt(Math.Abs(lat));
            ret += (20.0 * Math.Sin(6.0 * lat * PI) + 20.0 * Math.Sin(2.0 * lat * PI)) * 2.0 / 3.0;
            ret += (20.0 * Math.Sin(lng * PI) + 40.0 * Math.Sin(lng / 3.0 * PI)) * 2.0 / 3.0;
            ret += (160.0 * Math.Sin(lng / 12.0 * PI) + 320 * Math.Sin(lng * PI / 30.0)) * 2.0 / 3.0;

            return ret;
        }

        private static double ConvertLong(double lat, double lng)
        {
            double ret = 300.0 + lat + 2.0 * lng + 0.1 * lat * lat + 0.1 * lat * lng + 0.1 * Math.Sqrt(Math.Abs(lat));
            ret += (20.0 * Math.Sin(6.0 * lat * PI) + 20.0 * Math.Sin(2.0 * lat * PI)) * 2.0 / 3.0;
            ret += (20.0 * Math.Sin(lat * PI) + 40.0 * Math.Sin(lat / 3.0 * PI)) * 2.0 / 3.0;
            ret += (150.0 * Math.Sin(lat / 12.0 * PI) + 300.0 * Math.Sin(lat / 30.0 * PI)) * 2.0 / 3.0;

            return ret;
        }
    }
}

