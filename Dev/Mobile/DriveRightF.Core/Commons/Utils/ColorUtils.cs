﻿using System;
using System.Collections.Generic;

namespace DriveRightF.Core
{
	public static class ColorUtils
	{
		public static List<string> GetColorList()
		{
			return new List<string> () {
				"30AEE1",
				"F6623E",
				"8EC853",
				"2AB87D",
				"F55398",
				"9A3993",

				"30AEE1",
				"F6623E",
				"8EC853",
				"2AB87D",
				"F55398",
				"9A3993",

				"30AEE1",
				"F6623E",
				"8EC853"

			};
		}
	}
}

