﻿using Unicorn.Core;
using Unicorn.Core.UI;
using System;
using ReactiveUI;
using Unicorn.Core.Navigator;
using DriveRightF.Core;
using System.Reactive.Concurrency;
using Unicorn.Core.Services.Base;
using Unicorn.Core.Translation;
using System.Diagnostics;

[assembly: Unicorn.Core.Dependency.Dependency (typeof(ErrorHandler))]
namespace DriveRightF.Core
{
	public class ErrorHandler
	{
		public ITranslator Translator {get {return DependencyService.Get<ITranslator>();}}
		
		private PopupManager _popupManager;
		public ErrorHandler ()
		{
			_popupManager = DependencyService.Get<PopupManager> ();
		}

		/// <summary>
		/// Show error by Alert
		/// </summary>
		/// <param name="ex">Ex.</param>
		/// <param name="screen">Screen.</param>
		/// <param name="title">Title.</param>
		/// <param name="tag">Tag.</param>
		public void Handle(Exception ex, int screen, string title = "ERROR", string tag = ""){
			title = Translator.Translate (title);
			RxApp.MainThreadScheduler.Schedule (() => ShowError(ex, screen, title));
			RxApp.TaskpoolScheduler.Schedule (() => LogError (ex, tag));
		}

		public void Handle(Exception ex, string tag = ""){
			RxApp.TaskpoolScheduler.Schedule (() => LogError (ex, tag));
		}

		private void ShowError(Exception ex, int screen, String title){
			// Hide loading view before show alert (if exists)
			_popupManager.HideLoading ();
			// Check screen to show alert or not
			var navigator = DependencyService.Get<INavigator> ();
			var currentScreen = navigator.CurrentScreen;
			// TODO: still miss something here
			if (screen < 0 || screen == currentScreen) {
				string message;
				if (ex is ServiceException) {
					message = (ex as ServiceException).ErrorMessage;
				} else {
					message = ex.Message;
				}
				//
				_popupManager.ShowAlert (title, message, Unicorn.Core.Enums.DialogIcon.None, true,null,Translator.Translate("BUTTON_OK"));
			}
		}

		private void LogError(Exception ex, string tag){
			// TODO: write log to file or/and database
			System.Diagnostics.Debug.WriteLine(">>> Error >>> " + ex.ToString());
		}
	}

	public enum ErrorReportType{
		None,
		Dialog,
		// SomethingElse,
	}

	/// <summary>
	/// Thinking...
	/// </summary>
	public struct ErrorHandlerConfig{
		public int ScreenId {
			get;
			set;
		}

		public ErrorReportType ErrorReportType {
			get;
			set;
		}

		public string Tag {
			get;
			set;
		}
	}
}

