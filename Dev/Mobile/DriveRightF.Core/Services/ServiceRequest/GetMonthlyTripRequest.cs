﻿using System;
using Unicorn.Core;
using DriveRightF.Services.Base;

namespace DriveRightF.Services.Requests
{
	public class GetMonthlyTripRequest: DriveRightFBaseRequest
	{
		public GetMonthlyTripRequest ()
		{
		}

		public DateTime TimeStamp { get; set; }

		public DeviceInfo DeviceInfo { get; set; }

		public String UserId {get;set;}


		public DateTime CurentTime { get; set; }

	}
}

