﻿using System;
using DriveRightF.Services.Base;
using Unicorn.Core.Services.Base;
using Newtonsoft.Json;
using DriveRightF.Core.Models;

namespace DriveRightF.Services.Requests
{
	public class UploadProfileImageRequest: DriveRightFBaseRequest
	{
//		[ParamQuery]
//		[JsonProperty(PropertyName="image_content_type")]
//		public string ImageContentType{ get;set;}
//		[ParamQuery]
//		[JsonProperty(PropertyName="image64")]
//		public string Image64 { get; set;}
		[ObjectBody]
		public UploadFile UploadFile{ get;set;}
	}
}

