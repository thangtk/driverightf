﻿using System;
using Unicorn.Core;
using DriveRightF.Services.Base;

namespace DriveRightF.Services.Requests
{
	public class GetEarningsRequest : DriveRightFBaseRequest
	{
		public DeviceInfo DeviceInfo { get; set; }
		public string UserId { get; set;}
		public long TimeStamp { get; set;}
	}
}

