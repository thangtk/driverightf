﻿using System;
using DriveRightF.Services.Base;
using Newtonsoft.Json;
using System.Collections.Generic;
using DriveRightF.Core.Models;
using Unicorn.Core.Services.Base;

namespace DriveRightF.Services.Requests
{

	public class MonthlyTripRequest: DriveRightFBaseRequest
	{
		public const string MONTH_FORMAT = "MMyy";

		//month=0115 ~ 01/2015
		[JsonProperty(PropertyName="month")]
		public string Month { get; set;}
		//page_size=100
		[JsonProperty(PropertyName="page_size")]
		public int PageSize { get; set;}

	}
}

