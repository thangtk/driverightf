﻿using System;
using DriveRightF.Services.Base;
using Newtonsoft.Json;
using DriveRightF.Core.Models;
using Unicorn.Core.Services.Base;

namespace DriveRightF.Services.Requests
{
	public class RegisterRequest : DriveRightFBaseRequest
	{
//		[ObjectBody]
//		public Register Register { get; set;}
		[JsonProperty(PropertyName="partner_ref")]
		public string Partner { get; set; }
		[JsonProperty(PropertyName="customer_ref")]
		public string Customer { get; set; }
	}
}

