﻿using System;
using DriveRightF.Services.Base;
using Unicorn.Core.Services.Base;
using DriveRightF.Core.Models;

namespace DriveRightF.Services.Requests
{
	public class UpdateContractRequest : DriveRightFBaseRequest
	{
		[ObjectBody]
		public Contract Contract { get; set; }
	}
}

