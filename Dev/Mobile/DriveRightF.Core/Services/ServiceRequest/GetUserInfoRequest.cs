﻿using System;
using DriveRightF.Services.Base;

namespace DriveRightF.Services.Requests
{
	public class GetUserInfoRequest: DriveRightFBaseRequest
	{
		public string PhoneNumber {
			get;
			set;
		}
		public long TimeStamp {
			get;
			set;
		}
	}
}

