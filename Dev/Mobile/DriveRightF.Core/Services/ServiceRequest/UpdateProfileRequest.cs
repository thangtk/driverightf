﻿using System;
using Unicorn.Core.Services.Base;
using DriveRightF.Core.Models;
using DriveRightF.Services.Base;
using Newtonsoft.Json;

namespace DriveRightF.Services.Requests
{
	public class UpdateProfileRequest: DriveRightFBaseRequest
	{
//		[ObjectBody]
//		public User User { get; set; }
		[JsonProperty(PropertyName="name")]
		public string Name { get; set; }
		[JsonProperty(PropertyName="social_security")]
		public string SocialSecurityNo { get; set; }
		[JsonProperty(PropertyName="dob")]
		public string DOB { get; set; }
		[JsonProperty(PropertyName="phone")]
		public string PhoneNumber { get; set; }
		[JsonProperty(PropertyName="wechat")]
		public string WeChat { get; set; }
	}
}

