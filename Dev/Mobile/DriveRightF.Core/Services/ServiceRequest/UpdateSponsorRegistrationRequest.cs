﻿using System;
using DriveRightF.Core.Models;
using DriveRightF.Services.Base;

namespace DriveRightF.Services.Requests
{
	public class UpdateSponsorRegistrationRequest: DriveRightFBaseRequest
	{
//		public CarRegistration CarRegistration {
//			get;
//			set;
//		}
//		public DriverLicense DriverLicense {
//			get;
//			set;
//		}
//		public Insurance Insurance {
//			get;
//			set;
//		}
		public SponsorRegistration SponsorRegistration {get;set;}
	}
}

