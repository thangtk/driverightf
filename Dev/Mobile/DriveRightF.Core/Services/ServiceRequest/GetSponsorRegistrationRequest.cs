﻿using System;
using Unicorn.Core;
using DriveRightF.Services.Base;

namespace DriveRightF.Services.Requests
{
	public class GetSponsorRegistrationRequest: DriveRightFBaseRequest
	{
		public string SponsorId {get;set;}

		public DeviceInfo DeviceInfo {get;set;}

		public string UserId { get; set;}
	}
}

