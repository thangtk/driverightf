﻿using System;
using DriveRightF.Services.Base;

namespace DriveRightF.Services.Requests
{
	public class GetPerkRequest: DriveRightFBaseRequest
	{
		public int PerkId {
			get;
			set;
		}
	}
}

