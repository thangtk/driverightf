﻿using System;
using DriveRightF.Services.Base;
using Unicorn.Core.Services.Base;
using Newtonsoft.Json;

namespace DriveRightF.Services.Requests
{
//	[RequestInformation]
	public class UploadFileRequest: UBaseRequest
	{
//		[ParamQuery]
		[JsonProperty(PropertyName="image_content_type")]
		public string ImageType { get; set; }

//		[ParamQuery]
		[JsonProperty(PropertyName="image64")]
		public string ImageBase64 { get; set; }

		public UploadFileRequest(){
			ImageType = "image/png";
		}
	}
}

