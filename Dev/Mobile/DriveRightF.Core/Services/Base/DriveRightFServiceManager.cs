using DriveRightF.Services.Responses;
using DriveRightF.Services.Requests;

namespace DriveRightF.Services.Base
{
    public abstract class DriveRightFServiceManager
    {
       
		public abstract GetPerksResponse GetPerks(GetPerksRequest request);
		public abstract GetUserInfoResponse GetUserInfo(GetUserInfoRequest request);
    }
}