﻿using System;
using System.Threading.Tasks;
using DriveRightF.Core.Services;
using System.Collections.Generic;
using DriveRightF.Core.Models;
using Unicorn.Core.Storages;
using Unicorn.Core;
using DriveRightF.Services.Responses;
using DriveRightF.Services.Requests;
using DriveRightF.Services;
using Unicorn.Core.Services.Base;
using System.Diagnostics;
using DriveRightF.Core.Enums;
using DriveRightF.Core.Managers;
using DriveRightF.Core.Constants;
using System.Globalization;
using System.Collections.Specialized;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightFServiceManager))]
namespace DriveRightF.Core.Services
{
	public class DriveRightFServiceManager : BaseManager
	{
//		private const string PARTNER_ID = "5gi4pontqrkyetlbmf9oma"; // YESWAY (DEV)
		private const string PARTNER_ID = "I25LNCINTAEQEQYWAJBFWW";// POLARAX
//		private const string TEMP_DRIVER_ID = "030744b6-07cc-4f73-97bb-433959337080";
		// YESWAY
		private string _userId;

		public string UserId { 
			get { 
				if (string.IsNullOrEmpty (_userId)) {
					throw new ServiceException (){
						ErrorMessage = "Have to set userid before using any request to server - except login and register"
					};
				}
				return _userId; 
			} 
			set { _userId = value; }
		}

		private DriveRightFRestFulService _service;
		private DriveRightFServiceConfig _config;

		public DriveRightFServiceManager ()
		{
			_config = new DriveRightFServiceConfig ();
			_service = new DriveRightFRestFulService (_config);
			UserId=DependencyService.Get<ConfigManager> ().GetValue(ConfigKey.APP_USER_ID,string.Empty);
		}

		public string Login (string customerId)
		{
			string path = string.Format ("logins/{0}/{1}", PARTNER_ID, customerId);
			var response = _service.InvokeGetWebService<LoginResponse> (path, 
				               new LoginRequest ());
			// bonus
			UserId = response.DriverId;
			if (!string.IsNullOrEmpty (UserId)) {
				DependencyService.Get<ConfigManager> ().SaveData (ConfigKey.APP_USER_ID,UserId);
			}
			return response.DriverId;
		}

		public string Register (string customerId)
		{
			string path = string.Format ("logins/");
			RegisterResponse result = _service.InvokePostWebService<RegisterResponse> (path, 
				                          new RegisterRequest (){ 
						Partner = PARTNER_ID,
						Customer = customerId,
				});
			// bonus
			UserId = result.DriverId;
			UpdateProfile (new Profile () {
				PhoneNumber = customerId,
				Name = string.Empty,
				Image = string.Empty,
				ImageUrl = string.Empty
			});
			//TODO save customerRef;
			if (!string.IsNullOrEmpty (UserId)) {
				DependencyService.Get<ConfigManager> ().SaveData (ConfigKey.APP_USER_ID,UserId);
			}
			return result.DriverId;
		}

		public string UpdateProfile (User profile)
		{
			string path = string.Format ("drivers/{0}/profiles/", UserId);
			var response = _service.InvokeGetWebService<ListProfileResponse>(path, new ListProfileRequest ());
			bool usePost = false;
			string profileId = string.Empty;

			if (response.Count <= 0) {
				path = string.Format ("drivers/{0}/profiles/", UserId);
				usePost = true;
			} else {
				profileId = response.ListProfile [0].Uuid;
				path = string.Format ("drivers/{0}/profiles/{1}/", UserId,profileId);
			}

			if (usePost) {
				UpdateProfileResponse result = _service.InvokePostWebService<UpdateProfileResponse> (path, new UpdateProfileRequest () {
					Name=profile.Name,
					PhoneNumber=profile.PhoneNumber,
					SocialSecurityNo=profile.SocialSecurityNo,
					WeChat=profile.WeChat,
					DOB = (profile.DOB == string.Empty || profile.DOB == null) ? "1986-11-07" : profile.DOB
				});
				//TODO: must remove bullshit
				path = string.Format ("drivers/{0}/profiles/", UserId);
				response = _service.InvokeGetWebService<ListProfileResponse>(path, new ListProfileRequest ());
				profileId = response.ListProfile [0].Uuid;
			} else {
				UpdateProfileResponse result = _service.InvokePutWebService<UpdateProfileResponse> (path, new UpdateProfileRequest () {
					Name=profile.Name,
					PhoneNumber=profile.PhoneNumber,
					SocialSecurityNo=profile.SocialSecurityNo,
					WeChat=profile.WeChat,
					DOB = (profile.DOB == string.Empty || profile.DOB == null) ? "1986-11-07" : profile.DOB
				});
			}
			return profileId;
		}

		public string UpdateProfile (Profile profile)
		{
			string path = string.Format ("drivers/{0}/profiles/", UserId);
			var response = _service.InvokeGetWebService<ListProfileResponse>(path, new ListProfileRequest ());
			bool usePost = false;
			string profileId = string.Empty;

			if (response.Count <= 0) {
				path = string.Format ("drivers/{0}/profiles/", UserId);
				usePost = true;
			} else {
				profileId = response.ListProfile [0].Uuid;
				path = string.Format ("drivers/{0}/profiles/{1}/", UserId,profileId);
			}

			if (usePost) {
				UpdateProfileResponse result = _service.InvokePostWebService<UpdateProfileResponse> (path, new UpdateProfileRequest () {
					Name=profile.Name,
					PhoneNumber=profile.PhoneNumber,
					SocialSecurityNo="",
					WeChat="",
					DOB = "" 
				});
				//TODO: must remove bullshit
				path = string.Format ("drivers/{0}/profiles/", UserId);
				response = _service.InvokeGetWebService<ListProfileResponse>(path, new ListProfileRequest ());
				profileId = response.ListProfile [0].Uuid;
			} else {
				UpdateProfileResponse result = _service.InvokePutWebService<UpdateProfileResponse> (path, new UpdateProfileRequest () {
					Name=profile.Name,
					PhoneNumber=profile.PhoneNumber,
					SocialSecurityNo="",
					WeChat="",
					DOB = "" 
				});
			}
			return profileId;
		}


		public string UpdateRawImageProfile (string profileId,string imagePath,string contentType = "image/png")
		{
			string path = string.Format ("drivers/{0}/profiles/{1}/picture?format=image", UserId,profileId);
			NameValueCollection nvc = new NameValueCollection(); 
			nvc.Add("image_content_type", contentType);
			var result = _service.UploadRawFileRequest(path,RestSharp.Method.PUT,"image",imagePath,contentType,nvc);
			if (result != "None") {
				ServiceException ex = new ServiceException ();
				ex.ErrorType = ErrorType.ServiceFailed;
				throw ex;
			}
			return string.Format (_config.ActiveServiceUrl+ "drivers/{0}/profiles/{1}/picture?format=image", UserId,profileId);
		}

		/// <summary>
		/// Updates the image profile.
		/// </summary>
		/// <returns>The image url.</returns>
		/// <param name="profileId">Profile identifier.</param>
		/// <param name="imagePath">Image path.</param>
		public string UpdateImageProfile (string profileId,string imagePath)
		{
			string path = string.Format ("drivers/{0}/profiles/{1}/picture?format=json", UserId,profileId);
			var imagedata = DependencyService.Get<IImageManager>().GetImageBase64(imagePath);
            var result = _service.InvokePutWebService<UploadFileResponse>(path,new UploadFileRequest(){
                ImageBase64=imagedata
            });
            return string.Format (_config.ActiveServiceUrl+ "drivers/{0}/profiles/{1}/picture?format=image", UserId,profileId);
		}

		/// <summary>
		/// Updates the image profile.
		/// </summary>
		/// <returns>The image url.</returns>
		/// <param name="profileId">Profile identifier.</param>
		/// <param name="base64">Image base64 data.</param>
		public string UpdateImage64Profile (string profileId,string base64)
		{
			string path = string.Format ("drivers/{0}/profiles/{1}/picture?format=json", UserId,profileId);
//			var response = _service.InvokeGetWebService<ListProfileResponse>(path, new ListProfileRequest ());
//			var imagedata = Convert.ToBase64String(ImageUtil.GetDataFromImage(imagePath));
//			var result = _service.InvokePutWebService<UploadFileResponse>(path,new UploadFileRequest(){
//				ImageBase64=base64
//			});
			return string.Format (_config.ActiveServiceUrl+ "drivers/{0}/profiles/{1}/picture?format=image", UserId,profileId);
		}

		/// <summary>
		/// Updates the image profile.
		/// </summary>
		/// <returns>The image url.</returns>
		/// <param name="profileId">Profile identifier.</param>
		/// <param name="dataRaw">byte[] data of image.</param>
		public string UpdateImageRawProfile (string profileId,byte[] datas)
		{
			string path = string.Format ("drivers/{0}/profiles/{1}/picture?format=json", UserId,profileId);
			var result = _service.InvokePutWebService<UploadFileResponse>(path,new UploadFileRequest(){
				ImageBase64=Convert.ToBase64String(datas)
			});
			return string.Format (_config.ActiveServiceUrl+ "drivers/{0}/profiles/{1}/picture?format=image", UserId,profileId);
		}

		public List<TripSummary> GetTripByMonth (DateTime month)
		{
			string path = string.Format ("drivers/{0}/trips/", UserId);
//			string path = string.Format ("drivers/030744b6-07cc-4f73-97bb-433959337080/trips/");

			Debug.WriteLine("path: {0}{1}?month={2}", DriveRightFServiceConfig.DEV_TEMP_SERVICE, path,month.ToString (MonthlyTripRequest.MONTH_FORMAT));

			var response = _service.InvokeGetWebService<MonthlyTripResponse> (path, new MonthlyTripRequest () {
				Month = month.ToString (MonthlyTripRequest.MONTH_FORMAT),
				PageSize=250
			});
			return response.ListTrip;
		}

		public TripMonth GetTripMonthDetail (DateTime month)
		{
			string path = string.Format ("drivers/{0}/summaries/{1}", UserId,month.ToString("MMyy"));

			var result = _service.InvokeGetWebService<GetTripMonthDetailResponse> (path, new GetTripMonthDetailRequest () {
			});
			var trip = new TripMonth () {
				Score = result.Score,
				SmoothScore = result.SmoothScore,
				SpeedScore = result.SpeedScore,
				DistractionScore = result.DistractionScore,
				TimeOfDayScore = result.TimeOfDayScore,
				AverageDistance = result.AverageDistance,
				AverageSpeed = result.AverageSpeed,
				AverageDuration = result.AverageDuration,
				Month = month,
				TotalDistance = result.TotalDistance,
				TotalDuration = result.TotalDuration,
				TripCount = result.TripCount
			};
			return trip;
		}

		public TripMonth GetTripSummary6Month ()
		{
			string path = string.Format ("drivers/{0}/summaries/last6months", UserId);

			var result = _service.InvokeGetWebService<GetTripMonthDetailResponse> (path, new GetTripMonthDetailRequest () {
			});
			var trip = new TripMonth () {
				Score = result.Score,
				SmoothScore = result.SmoothScore,
				SpeedScore = result.SpeedScore,
				DistractionScore = result.DistractionScore,
				TimeOfDayScore = result.TimeOfDayScore,
				AverageDistance = result.AverageDistance,
				AverageSpeed = result.AverageSpeed,
				AverageDuration = result.AverageDuration,
				Month = DateTime.ParseExact("9999-01","yyyy-MM", CultureInfo.InvariantCulture),
				TotalDistance = result.TotalDistance,
				TotalDuration = result.TotalDuration,
				TripCount = result.TripCount
			};
//			Console.WriteLine ();
//			DateTime.TryParseExact(result.Month, "MMyy"

			return trip;
		}

		public List<TripMonthSummary> GetTripMonthSummary ()
		{
			string path = string.Format ("drivers/{0}/summaries", UserId);

			var result = _service.InvokeGetWebService<ListTripSummaryResponse> (path, new ListTripSummaryRequest () {
			});

			path = string.Format ("drivers/{0}/summaries/last6months", UserId);

			var response = _service.InvokeGetWebService<GetTripMonthDetailResponse> (path, new GetTripMonthDetailRequest () {
			});
			var listRs = new List<TripMonthSummary> (result.Count + 1);
			listRs.Add (new TripMonthSummary () {
				TripCount = response.TripCount,
				Score = response.Score,
				MonthRaw = "9999-01",
				Month=DateTime.ParseExact("9999-01","yyyy-MM", CultureInfo.InvariantCulture)
			});
			listRs.AddRange (result.ListTripMonth);
			return listRs;
		}

		public Trip GetTripDetail (string tripId)
		{
			string path = string.Format ("drivers/{0}/trips/{1}", UserId, tripId);
			//			string path = string.Format ("drivers/030744b6-07cc-4f73-97bb-433959337080/trips/{0}", tripId); 
			Debug.WriteLine("path: {0}{1}", DriveRightFServiceConfig.DEV_TEMP_SERVICE, path);

			TripDetailResponse result = _service.InvokeGetWebService<TripDetailResponse> (path, new TripDetailRequest () {

			});
			Trip trip = new Trip () {
				TripId = result.TripId,
				StartTime =result.StartTime,
				Duration = result.Duration,
				EndTime = result.EndTime,
				Distance = result.Distance,
				Score = result.Score,
				SmoothScore = result.SmoothScore,
				SpeedScore = result.SpeedScore,
				DistractionScore = result.DistractionScore,
				TimeOfDayScore = result.TimeOfDayScore,
				Coordinates = result.Coordinates
			};

			return trip;
		}

		public List<Earning> GetChallenges ()
		{
			string path = string.Format ("drivers/{0}/challenges/", UserId);
			var response = _service.InvokeGetWebService<ListChallengeResponse> (path, new ListChallengeRequest ());
			for (int i = 0; i < response.ListEarning.Count; i++)
			{
				if (response.ListEarning [i].Progress < 100)
				{
					response.ListEarning [i].Status = EarningStatus.InProgress;
				}
			}
			return response.ListEarning;
		}

		public List<Perk> GetPerks ()
		{
			string path = string.Format ("drivers/{0}/perks/", UserId);
			var response = _service.InvokeGetWebService<ListPerkResponse> (path, new ListPerkRequest ());
			return response.ListPerk;
		}

		public List<Sponsor> GetSponsors ()
		{
			_service = new DriveRightFRestFulService (new DriveRightFServiceConfig ());
			string path = string.Format ("drivers/{0}/sponsorships/", UserId);
			Debug.WriteLine("path: {0}{1}", DriveRightFServiceConfig.DEV_TEMP_SERVICE, path);
			var response = _service.InvokeGetWebService<ListSponsorResponse> (path, new ListSponsorRequest ());
//			if (response.Count <= 0) {
//				return new List<Sponsor> ();
//			}
//			foreach (var sponsor in response.ListSponsor) {
//				path = string.Format ("drivers/{0}/sponsorships/{1}/contracts/", UserId,sponsor.SponsorID);
//				ListContractResponse result = _service.InvokeGetWebService<ListContractResponse> (path, new ListContractRequest ());
//				sponsor.Status = result.Count > 0 ? SponsorStatus.Registered : SponsorStatus.NotRegistered;
//			}

			return response.ListSponsor;
//			return new List<Sponsor>() {
//				new Sponsor() {
//					SponsorID = "1",
//					Name = "Asgard",
//					Icon = "award_coffee.png"
//				},
//				new Sponsor() {
//					SponsorID = "2",
//					Name = "Asgard",
//					Icon = "award_coffee.png"
//				},
//				new Sponsor() {
//					SponsorID = "3",
//					Name = "Asgard",
//					Icon = "award_coffee.png"
//				},
//				new Sponsor() {
//					SponsorID = "4",
//					Name = "Asgard",
//					Icon = "award_coffee.png"
//				},
//				new Sponsor() {
//					SponsorID = "5",
//					Name = "Asgard",
//					Icon = "award_coffee.png"
//				},
//				new Sponsor() {
//					SponsorID = "6",
//					Name = "Asgard",
//					Icon = "award_coffee.png"
//				},
//				new Sponsor() {
//					SponsorID = "7",
//					Name = "Asgard",
//					Icon = "award_coffee.png"
//				},
//				new Sponsor() {
//					SponsorID = "8",
//					Name = "Asgard",
//					Icon = "award_coffee.png"
//				},
//			};
		}

		public List<Perk> GetAvailablePerks ()
		{
			// PENDING
			return null;
		}

		public bool UpdatePerk (Perk perk)
		{
			return false;
		}

//		public User GetProfile(){
//			// get list profile
//			string path = string.Format ("drivers/{0}/profiles/", UserId);
//			var response = _service.InvokeGetWebService<ListProfileResponse>(path, new ListProfileRequest ());
//			if (response.Count <= 0) {
//				return null;
//			}
//			string profileId = response.ListProfile [0].Uuid;
//			path = string.Format ("drivers/{0}/profiles/{1}/", UserId,profileId);
//			ProfileDetailResponse result = _service.InvokeGetWebService<ProfileDetailResponse> (path, new ProfileDetailRequest ());
//			User user = new User (){ 
//				UserId = result.UserId,
//				DOB=result.DOB,
//				Image = result.ImageUrl,
//				Name = result.Name,
//				SocialSecurityNo = result.SocialSecurityNo,
//				PhoneNumber = result.PhoneNumber,
//				WeChat = result.WeChat,
//
//			};
//			return user;
//		}

		public Profile GetProfile(){
			// get list profile
			string path = string.Format ("drivers/{0}/profiles/", UserId);
			var response = _service.InvokeGetWebService<ListProfileResponse>(path, new ListProfileRequest ());
			if (response.Count <= 0) {
				return null;
			}
			string profileId = response.ListProfile [0].Uuid;
			path = string.Format ("drivers/{0}/profiles/{1}/", UserId,profileId);
			ProfileDetailResponse result = _service.InvokeGetWebService<ProfileDetailResponse> (path, new ProfileDetailRequest ());
			Profile profile = new Profile () {
				ProfileID = result.UserId,
				ImageUrl = result.ImageUrl,
				Image = DependencyService.Get<IImageManager> ().DownloadImage (result.ImageUrl, "profile.png").Result,
				Name = result.Name,
				PhoneNumber = result.PhoneNumber,
				PaymentAccount = string.Empty,
				PaymentType = string.Empty,
				PaymentName = string.Empty,
				PaymentPassword = string.Empty,
			};
			return profile;
		}

		public SponsorRegistration GetSponsorRegistration (string sponsorId){
			string path = string.Format ("drivers/{0}/sponsorships/{1}/contracts/", UserId,sponsorId);
			ListContractResponse result = _service.InvokeGetWebService<ListContractResponse> (path, new ListContractRequest ());
			if (result.Count <= 0) {
				return null;
			}
			string contractId = result.ListContract [0].ContractId;
			path = string.Format ("drivers/{0}/sponsorships/{1}/contracts/{2}/", UserId,sponsorId,contractId);
			ContractDetailResponse response = _service.InvokeGetWebService<ContractDetailResponse> (path, new ContractDetailRequest ());
			SponsorRegistration sponsor = new SponsorRegistration () {
				CarRegistration = new CarRegistration(){
					SponsorId = sponsorId,
					CarModel = response.CarModel,
					CarNo = response.CarNo
				},
				DriverLicense = new DriverLicense(){
					SponsorId = sponsorId,
					LicenseNo = response.LicenseNo,
					Insurer = response.Insurer,
					LicenseExpiry = response.LicenceExpiry
				},
				Insurance = new Insurance(){
					SponsorId = sponsorId,
					PolicyNo = response.PolicyNo,
					Insurer = response.Insurer,
					Expiry = response.PolicyExpiry,

				}
			};
			return sponsor;
		}

		public void UpdateSponsorRegistration(SponsorRegistration input){
			string sponsorId = input.Insurance.SponsorId;
			string path = string.Format ("drivers/{0}/sponsorships/{1}/contracts/", UserId,input.Insurance.SponsorId);
			ListContractResponse result = _service.InvokeGetWebService<ListContractResponse> (path, new ListContractRequest ());
			bool usePost = false;
			string contractId = string.Empty;
			if (result.Count <= 0) {
				path = string.Format ("drivers/{0}/sponsorships/{1}/contracts/", UserId, sponsorId);
				usePost = true;
			} else {
				contractId = result.ListContract [0].ContractId;
				path = string.Format ("drivers/{0}/sponsorships/{1}/contracts/{2}/", UserId,sponsorId,contractId);
			}

			if (usePost) {
				Console.WriteLine (">>>>>>>>>>Use post");
				AddContractResponse result2 = _service.InvokePostWebService<AddContractResponse> (path, 
					new AddContractRequest (){
						Insurer = input.Insurance.Insurer,
						PolicyNo=input.Insurance.PolicyNo,
						PolicyExpiry=input.Insurance.Expiry,
						LicenseNo=input.DriverLicense.LicenseNo,
						LicenceExpiry=input.DriverLicense.LicenseExpiry,
						CarNo=input.CarRegistration.CarNo,
						CarModel= input.CarRegistration.CarModel
					}
				);
			} else {
				Console.WriteLine (">>>>>>>>>>Use put");
				UpdateContractResponse result2 = _service.InvokePutWebService<UpdateContractResponse> (path, new UpdateContractRequest () {
					Contract = new Contract(){
						Insurer = input.Insurance.Insurer,
						PolicyNo=input.Insurance.PolicyNo,
						PolicyExpiry=input.Insurance.Expiry,
						LicenseNo=input.DriverLicense.LicenseNo,
						LicenceExpiry=input.DriverLicense.LicenseExpiry,
						CarNo=input.CarRegistration.CarNo,
						CarModel= input.CarRegistration.CarModel
					}
				});
			}

		}









		// TODO: remove


				public async Task<GetPerksResponse> GetPerks(GetPerksRequest request)
				{
					await Task.Delay (2000);
					int count = Repository.GetAll<Perk> ().Count;
					Random r = new Random();
		
					GetPerksResponse response = new GetPerksResponse ();
		
					int caseRequest = r.Next(0, 5); //for ints
					Console.WriteLine("caseRequest: {0}", caseRequest);
					switch (caseRequest) {
					case 0:
						response.ErrorMessage = "Request not valid";
						break;
					case 1:
						response.Perks = new List<Perk> {new Perk () {
								PerkId = (count + 1).ToString (),
								Name = "An award coffee.",
								Used = false,
								Expiry = DateTime.Now.AddDays(10),
								QRCode = "A light and lively coffee with tangy citrus topnotes balanced by a lingering fruity sweetness." + (count + 1).ToString(),
								Image = "coffee_snob.jpg",
								Icon = "award_coffee.png",
								Sponsor = "Hashbro",
								UpdatedTime = DateTime.UtcNow,
								ItemStatus = ItemStatus.New,
								Description = "\t A naturally processed, full-bodied coffee with a silky mouth feel and intense fruit sweetness. " +
									" A treasure from the birthplace of coffee.  Roast: City or Full City to highlight fruity topnotes, Vienna or N. Italian for a sweeter, heavier cup. " + count.ToString()
							}
						};
						break;
					case 2:
						response.Perks = new List<Perk> {new Perk () {
								PerkId = (count + 1).ToString (),
								Name = "An award crown.",
								Used = false,
								Expiry = DateTime.Now.AddDays (9),
								QRCode = "A light and lively coffee with tangy citrus topnotes balanced by a lingering fruity sweetness." + (count + 1).ToString(),
								Image = "coffee_snob.jpg",
								Icon = "award_crown.png",
								Sponsor = "kyho",
								UpdatedTime = DateTime.UtcNow,
								ItemStatus = ItemStatus.New,
								Description = "\t A naturally processed, full-bodied coffee with a silky mouth feel and intense fruit sweetness.  " +
									"A treasure from the birthplace of coffee.  Roast: City or Full City to highlight fruity topnotes, Vienna or N. Italian for a sweeter, heavier cup." +
									"\t A naturally processed, full-bodied coffee with a silky mouth feel and intense fruit sweetness.  " +
									"A treasure from the birthplace of coffee.  Roast: City or Full City to highlight fruity topnotes, Vienna or N. Italian for a sweeter, heavier cup." + count.ToString()
							}
						};
						break;
					case 3:
						response.Perks = new List<Perk> {new Perk () {
								PerkId = (count + 1).ToString (),
								Name = "An award dimond.",
								Used = false,
								Expiry = DateTime.Now.AddDays(7),
								QRCode = "A light and lively coffee with tangy citrus topnotes balanced by a lingering fruity sweetness." + (count + 1).ToString(),
								Image = "coffee_snob.jpg",
								Icon = "award_dimond.png",
								Sponsor = "xxxxxx",
								UpdatedTime = DateTime.UtcNow,
								ItemStatus = ItemStatus.New,
								Description = "\t A naturally processed, full-bodied coffee with a silky mouth feel and intense fruit sweetness.  " +
									"A treasure from the birthplace of coffee.  Roast: City or Full City to highlight fruity topnotes, Vienna or N. Italian for a sweeter, heavier cup." +
									"\t A naturally processed, full-bodied coffee with a silky mouth feel and intense fruit sweetness.  " +
									"A treasure from the birthplace of coffee.  Roast: City or Full City to highlight fruity topnotes, Vienna or N. Italian for a sweeter, heavier cup." + count.ToString()
							}
						};
						break;
					case 4:
						response.Perks = new List<Perk> {new Perk () {
								PerkId = (count + 1).ToString (),
								Name = "An award thunderbolt.",
								Used = false,
								Expiry = DateTime.Now.AddDays(15),
								QRCode = "A light and lively coffee with tangy citrus topnotes balanced by a lingering fruity sweetness." + (count + 1).ToString(),
								Image = "coffee_snob.jpg",
								Icon = "award_thunderbolt.png",
								Sponsor = "thunderbolt",
								UpdatedTime = DateTime.UtcNow,
								ItemStatus = ItemStatus.New,
								Description = "\t A naturally processed, full-bodied coffee with a silky mouth feel and intense fruit sweetness." +
									"  A treasure from the birthplace of coffee.  Roast: City or Full City to highlight fruity topnotes, Vienna or N. Italian for a sweeter, heavier cup." + count.ToString()
							}
						};
						break;
					}
					return response;
				}
		//
				public async Task<GetSponsorRegistrationResponse> GetSponsorRegistration(GetSponsorRegistrationRequest request)
				{
					await Task.Delay (2000);
					Random r = new Random();
		
					GetSponsorRegistrationResponse response = new GetSponsorRegistrationResponse ();
		
					int caseRequest = r.Next(1, 2); //for ints
					Console.WriteLine("caseRequest: {0}", caseRequest);
		
					switch (caseRequest) {
					case 0:
						response.ErrorMessage = "Invalid request";
						response.Response = string.Empty;;
						break;
					case 1:
						int count = Repository.GetAll<Sponsor> ().Count;
						response.Response = "success";
						response.SponsorRegistration = new SponsorRegistration {
							CarRegistration = new CarRegistration()
							{
								SponsorId = request.SponsorId,
								CarNo = "CarNo " + request.SponsorId,
								CarModel = "CarModel " + request.SponsorId,
							},
							DriverLicense = new DriverLicense()
							{
								SponsorId = request.SponsorId,
								LicenseNo = "LicenseNo " + request.SponsorId,
								Insurer = "Insurer " + request.SponsorId,
							},
							Insurance = new Insurance()
							{
								SponsorId = request.SponsorId,
								PolicyNo = "PolicyNo " + request.SponsorId,
								Insurer = "Insurer " + request.SponsorId,
								Expiry = DateTime.UtcNow.AddDays(-20),
							}
						};
						break;
					}
		
		
					return response;
				}
		
				public async Task<UpdateProfileResponse> UpdateProfile(UpdateProfileRequest request)
				{
					await Task.Delay (2000);
					Random r = new Random();
		
					UpdateProfileResponse response = new UpdateProfileResponse ();
		
					int caseRequest = r.Next(1, 2); //for ints
					Console.WriteLine("caseRequest: {0}", caseRequest);
		
					switch (caseRequest) {
					case 0:
						response.ErrorMessage = "UserName not valid";
						response.Response = string.Empty;;
						break;
					case 1:
						response.Response = "Uodate Profile success";
						response.ErrorMessage = string.Empty;
						break;
					}
		
		
					return response;
				}
		
		
				public async Task<UpdateSponsorRegistrationResponse> UpdateSponsorRegistration(UpdateSponsorRegistrationRequest request)
				{
					await Task.Delay (2000);
					Random r = new Random();
		
					UpdateSponsorRegistrationResponse response = new UpdateSponsorRegistrationResponse ();
		
					int caseRequest = r.Next(1, 2); //for ints
					Console.WriteLine("caseRequest: {0}", caseRequest);
					switch (caseRequest) {
					case 0:
						response.ErrorMessage = "UserName not invalid";
						break;
					case 1:
						response.Response = "Update Profile success";
						break;
					}
		
		
					return response;
				}
		
				public async Task<GetEarningsResponse> GetEarnings(GetEarningsRequest request)
				{
					await Task.Delay (2000);
					Random r = new Random();
		
					GetEarningsResponse response = new GetEarningsResponse ();
		
					int caseRequest = r.Next(0, 4); //for ints
					switch (caseRequest) {
					case 0:
						response.ErrorMessage = "Error";
						break;
					case 1:
						response.Earnings = new List<Earning> () {
							new Earning () {
								Icon = "",
								Progress = 40,
								Description = "Description",
								Name = "Name",
								Status = EarningStatus.Done,
							}
						};
						break;
					case 2:
						response.Earnings = new List<Earning> () {
							new Earning () {
								Icon = "",
								Progress = 40,
								Description = "Description",
								Name = "Name",
								Status = EarningStatus.InProgress,
							}
						};
						break;
					case 3:
						response.Earnings = new List<Earning> () {
							new Earning () {
								Icon = "",
								Progress = 40,
								Description = "Description",
								Name = "Name",
								Status = EarningStatus.New,
							}
						};
						break;
					}
		
		
					return response;
				}
	}
}

