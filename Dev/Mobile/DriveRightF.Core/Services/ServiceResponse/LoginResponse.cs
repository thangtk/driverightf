﻿using System;
using DriveRightF.Services.Base;
using Newtonsoft.Json;

namespace DriveRightF.Services.Responses
{
	public class LoginResponse : DriveRightFBaseResponse
	{
		[JsonProperty(PropertyName="href")]
		public string Href{ get; set; }
		[JsonProperty(PropertyName="partner")]
		public string Partner{ get; set;}
		[JsonProperty(PropertyName="customer_ref")]
		public string CustomerRef{ get; set; }
		[JsonProperty(PropertyName="driver")]
		public string DriverRef{ get; set; }
		[JsonProperty(PropertyName="driver_uuid")]
		public string DriverId{ get; set; }

//		public override void RepairData(){
//			if (DriverRef == string.Empty) {
//				return;
//			}
//			string[] str=DriverRef.Split ('/');
//			DriverId = str [str.Length - 2];
//		}
	}
}

