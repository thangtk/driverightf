﻿using System;
using DriveRightF.Services.Base;
using Newtonsoft.Json;
using System.Collections.Generic;
using DriveRightF.Core.Models;
using DriveRightF.Core;

namespace DriveRightF.Services.Responses
{
	public class ListChallengeResponse : DriveRightFBaseResponse
	{
		public int Count { get; set; }

		[JsonProperty(PropertyName="results")]
		public List<Earning> ListEarning{ get; set; }

		public override void RepairData(){
			if (ListEarning == null) {
				ListEarning = new List<Earning> ();
				return;
			}
			foreach (var earning in ListEarning) {
				switch (earning.StatusRaw) {
				case "InProgress":
					earning.Status = EarningStatus.InProgress;
					break;
					case "Done":
						earning.Status = EarningStatus.Done;
					break;
					case "Expired":
						earning.Status = EarningStatus.Expired;
					break;
					case "None":
						earning.Status = EarningStatus.None;
					break;
					default:
						earning.Status = EarningStatus.New;
					break;
				}
			}
		}
	}
}

