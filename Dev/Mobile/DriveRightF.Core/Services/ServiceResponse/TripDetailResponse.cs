﻿using System;
using DriveRightF.Services.Base;
using Newtonsoft.Json;
using System.Collections.Generic;
using DriveRightF.Core.Models;
using Newtonsoft.Json.Linq;

namespace DriveRightF.Services.Responses
{
	public class TripDetailResponse: DriveRightFBaseResponse
	{
		[JsonProperty(PropertyName="uuid")]
		public string TripId { get; set; }

		[JsonProperty(PropertyName="utc_start")]
		public DateTime StartTime { get; set; }

		[JsonProperty(PropertyName="distance")]
		public double Distance { get; set; }

		[JsonProperty(PropertyName="utc_end")]
		public DateTime EndTime { get; set; }

		[JsonProperty(PropertyName="duration")]
		public long Duration { get; set; }

		[JsonProperty(PropertyName="smooth_score")]
		public double SmoothScore { get; set;}
		[JsonProperty(PropertyName="speed_score")]
		public double SpeedScore { get; set;}
		[JsonProperty(PropertyName="distraction_score")]
		public double DistractionScore { get; set;}

		[JsonProperty(PropertyName="time_of_day_score")]
		public double TimeOfDayScore { get; set;}

		[JsonProperty(PropertyName="trip_score")]
		public double Score { get; set; }


		// Some information of GPS here
		[JsonProperty(PropertyName="gps_points")]
		public List<object> RawList {
			get;
			set;
		}
		[JsonIgnore]
		public List<GPS> Coordinates {
			get;
			set;
		}

		public override void RepairData(){
			Coordinates = new List<GPS> ();
			foreach (var gps in RawList) {
				var arr = gps as JArray;
				var vals = arr.GetEnumerator();
				vals.MoveNext ();

				var val = vals.Current;
				var time = val.Value<DateTime> ();
				vals.MoveNext ();
				val = vals.Current;
				var lattitude = val.Value<double> ();
				vals.MoveNext ();
				val = vals.Current;
				var longtitude = val.Value<double> ();
				Coordinates.Add (new GPS (){ 
					Time = time,
					Longitude = longtitude,
					Latitude = lattitude
				});
			}
			if (RawList != null) {
				RawList.Clear ();
			}
		}

	}
}

