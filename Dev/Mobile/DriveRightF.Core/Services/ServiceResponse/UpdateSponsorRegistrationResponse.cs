﻿using System;
using DriveRightF.Core.Services;
using DriveRightF.Services.Base;

namespace DriveRightF.Services.Responses
{
	public class UpdateSponsorRegistrationResponse : DriveRightFBaseResponse
	{
		public long TimeStamp{get;set;}
	}
}

