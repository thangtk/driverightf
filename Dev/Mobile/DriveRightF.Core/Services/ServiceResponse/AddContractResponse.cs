﻿using System;
using DriveRightF.Services.Base;
using Newtonsoft.Json;

namespace DriveRightF.Services.Responses
{
	public class AddContractResponse : DriveRightFBaseResponse
	{
	

		[JsonProperty(PropertyName="policy_no")]
		public string PolicyNo { get; set; }

		[JsonProperty(PropertyName="policy_insurer")]
		public string Insurer { get; set; }


		[JsonProperty(PropertyName="licence_no")]
		public string LicenseNo { get; set; }

		[JsonProperty(PropertyName="car")]
		public string CarModel { get; set; }

		[JsonProperty(PropertyName="car_no")]
		public string CarNo { get; set; }

		[JsonProperty(PropertyName="policy_expiry")]
		public DateTime PolicyExpiry { get; set; }

		[JsonProperty(PropertyName="licence_expiry")]
		public DateTime LicenceExpiry { get; set; }
	}
}

