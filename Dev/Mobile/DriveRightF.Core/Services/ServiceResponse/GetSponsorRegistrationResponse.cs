﻿using System;
using DriveRightF.Core.Services;
using DriveRightF.Services.Base;
using DriveRightF.Services.Requests;
using DriveRightF.Core.Models;

namespace DriveRightF.Services.Responses
{
	public class GetSponsorRegistrationResponse : DriveRightFBaseResponse
	{
		public SponsorRegistration SponsorRegistration { get; set;}
	}
}

