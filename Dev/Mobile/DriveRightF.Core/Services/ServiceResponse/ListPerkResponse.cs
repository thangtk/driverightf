﻿using System;
using DriveRightF.Services.Base;
using System.Collections.Generic;
using DriveRightF.Core.Models;
using Newtonsoft.Json;

namespace DriveRightF.Services.Responses
{
	public class ListPerkResponse : DriveRightFBaseResponse
	{
		public int Count { get; set; }

		[JsonProperty(PropertyName="results")]
		public List<Perk> ListPerk{ get; set; }

		public override void RepairData(){

			foreach (var perk in ListPerk) {
				if (perk.SponsorHref == string.Empty) {
					return;
				}
				string[] str=perk.SponsorHref.Split ('/');
				perk.SponsorId = str [str.Length - 2];
			}
		}

	}
}

