﻿using System;
using DriveRightF.Core.Models;
using System.Collections.Generic;
using DriveRightF.Core.Services;
using DriveRightF.Services.Base;

namespace DriveRightF.Services.Responses
{
	public class GetEarningsResponse : DriveRightFBaseResponse
	{
		public long TimeStamp { get; set; }
		public List<Earning> Earnings { get; set; }
	}
}

