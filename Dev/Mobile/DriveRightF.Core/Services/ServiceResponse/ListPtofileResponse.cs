﻿using System;
using DriveRightF.Services.Base;
using DriveRightF.Core.Models;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace DriveRightF.Services.Responses
{
	public class ListProfileResponse: DriveRightFBaseResponse
	{
		[JsonProperty(PropertyName="count")]
		public int Count { get;set;}
		[JsonProperty(PropertyName="results")]
		public List<ProfileSumary> ListProfile {get;set;}
	}
}

