﻿using System;
using DriveRightF.Services.Base;
using Newtonsoft.Json;

namespace DriveRightF.Services.Responses
{
	public class RegisterResponse : DriveRightFBaseResponse
	{
		[JsonProperty(PropertyName="customer_ref")]
		public string Customer { get; set; }
		[JsonProperty(PropertyName="driver")]
		public string DriverRef { get; set; }
		[JsonIgnore]
		public string DriverId{ get; set; }

		public override void RepairData(){
			if (DriverRef == string.Empty) {
				return;
			}
			string[] str=DriverRef.Split ('/');
			DriverId = str [str.Length - 2];
		}
	}
}

