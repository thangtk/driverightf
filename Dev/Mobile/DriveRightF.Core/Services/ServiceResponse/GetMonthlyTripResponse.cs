﻿using System;
using System.Collections.Generic;
using DriveRightF.Core.Models;
using DriveRightF.Core.Services;
using DriveRightF.Services.Base;

namespace DriveRightF.Services.Responses
{
	public class GetMonthlyTripResponse : DriveRightFBaseResponse
	{
		public DateTime Timestamp {
			get;
			set;
		}


		public GroupCurrentMonthlyTrips GroupCurrentMonthlyTrips { get; set; }
	}

	public class GroupCurrentMonthlyTrips
	{
		public List<Trip> CurrentTrips {
			get;
			set;
		}

		public List<Trip> PreviousTrips {
			get;
			set;
		}
	}
}

