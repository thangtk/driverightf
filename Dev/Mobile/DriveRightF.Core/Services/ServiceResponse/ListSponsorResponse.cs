﻿using System;
using DriveRightF.Services.Base;
using System.Collections.Generic;
using DriveRightF.Core.Models;
using Newtonsoft.Json;
using Unicorn.Core;
using DriveRightF.Core.Services;

namespace DriveRightF.Services.Responses
{
	public class ListSponsorResponse : DriveRightFBaseResponse
	{
		public int Count { get; set; }

		[JsonProperty(PropertyName="results")]
		public List<Sponsor> ListSponsor{ get; set; }

		public override void RepairData(){
			if (ListSponsor == null) {
				return;
			}
			foreach (var sponsor in ListSponsor) {
				sponsor.IconURL = string.Format(this.BaseURL+"sponsors/{0}/icon?format=image",sponsor.SponsorRawID);
//				sponsor.IconURL = "http://ec2-54-154-200-156.eu-west-1.compute.amazonaws.com/app/sponsors/6ed2cebe-53d4-4285-bbd8-361ab62f7d6b/icon?format=image" ;//string.Format(this.BaseURL+"sponsors/{0}/icon?format=image",sponsor.SponsorID);
				if (sponsor.RawType.Equals ("Insurer", StringComparison.CurrentCultureIgnoreCase)) {
					sponsor.Type = DriveRightF.Core.Enums.SponsorType.Insurer;
				} else {
					sponsor.Type = DriveRightF.Core.Enums.SponsorType.None;
				}
				// TEST
				// FAKE: registered
				sponsor.Status = sponsor.NumContracts > 0 ? DriveRightF.Core.Enums.SponsorStatus.Registered:DriveRightF.Core.Enums.SponsorStatus.NotRegistered;
				sponsor.UpdateTime = DateTime.Now;
			}
		}
	}
}

