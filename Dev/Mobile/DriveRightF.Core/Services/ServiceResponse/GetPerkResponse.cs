﻿using System;
using DriveRightF.Core.Models;
using DriveRightF.Core.Services;
using DriveRightF.Services.Base;

namespace DriveRightF.Services.Responses
{
	public class GetPerkResponse : DriveRightFBaseResponse
	{
		public long TimeStamp { get; set; }
		public Perk Perk { get; set; }
	}
}

