﻿using System;
using DriveRightF.Services.Base;
using Newtonsoft.Json;

namespace DriveRightF.Services.Responses
{
	public class SonsorShipDetailResponse: DriveRightFBaseResponse
	{
		[JsonProperty(PropertyName="sponsor")]
		public string SponsorUrl { get; set; }
		[JsonIgnore]
		public string SponsorUuid { get; set; }
		public override void RepairData(){
			if (SponsorUrl == string.Empty) {
				return;
			}
			string[] str=SponsorUrl.Split ('/');
			SponsorUuid = str [str.Length - 2];
		}
	}
}

