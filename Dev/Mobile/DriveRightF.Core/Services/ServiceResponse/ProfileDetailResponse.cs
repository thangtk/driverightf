﻿using System;
using DriveRightF.Services.Base;
using Newtonsoft.Json;

namespace DriveRightF.Services.Responses
{
	public class ProfileDetailResponse: DriveRightFBaseResponse
	{
		[JsonProperty(PropertyName="uuid")]
		public string UserId { get; set; }
		public string Name { get; set; }
		public string Href { get; set; }
		[JsonProperty(PropertyName="social_security")]
		public string SocialSecurityNo { get; set; }
		public string DOB { get; set; }
		[JsonProperty(PropertyName="phone")]
		public string PhoneNumber { get; set; }
		public string ImageUrl { get; set; }
		[JsonProperty(PropertyName="picture")]
		public string Base64Url { get; set; }
		[JsonProperty(PropertyName="wechat")]
		public string WeChat { get; set; }
		[JsonProperty(PropertyName="modified")]
		public DateTime UpdatedDate {get;set;}

		public override void RepairData(){
			ImageUrl = Href + "picture?format=image";
		}
	}
}

