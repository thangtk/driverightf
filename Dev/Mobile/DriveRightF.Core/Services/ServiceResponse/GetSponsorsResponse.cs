﻿using System;
using System.Collections.Generic;
using DriveRightF.Core.Models;
using DriveRightF.Core.Services;
using DriveRightF.Services.Base;

namespace DriveRightF.Services.Responses
{
	public class GetSponsorsResponse : DriveRightFBaseResponse
	{
		public long TimeStamp {
			get;
			set;
		}

		public List<Sponsor> Sponsors {
			get;
			set;
		}
	}
}

