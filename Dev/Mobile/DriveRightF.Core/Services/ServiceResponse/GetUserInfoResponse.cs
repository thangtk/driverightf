﻿using System;
using DriveRightF.Core.Models;
using System.Collections.Generic;
using DriveRightF.Core.Services;
using DriveRightF.Services.Base;

namespace DriveRightF.Services.Responses
{
	public class GetUserInfoResponse : DriveRightFBaseResponse
	{
		public long TimeStamp {
			get;
			set;
		}

		public User UserInfo {
			get;
			set;
		}
	}
}

