﻿using System;
using DriveRightF.Services.Base;
using Newtonsoft.Json;
using System.Collections.Generic;
using DriveRightF.Core.Models;

namespace DriveRightF.Services.Responses
{
	public class ListContractResponse: DriveRightFBaseResponse
	{
		public int Count { get; set; }

		[JsonProperty(PropertyName="results")]
		public List<ContractSumary> ListContract{ get; set; }
	}
}

