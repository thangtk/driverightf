﻿using System;
using DriveRightF.Services.Base;
using Newtonsoft.Json;
using DriveRightF.Core.Models;
using System.Collections.Generic;
using System.Globalization;

namespace DriveRightF.Services.Responses
{
	public class ListTripSummaryResponse: DriveRightFBaseResponse
	{
		public int Count { get; set; }

		[JsonProperty(PropertyName="results")]
		public List<TripMonthSummary> ListTripMonth{ get; set; }
		public override void RepairData(){
			if (ListTripMonth == null) {
				return;
			}
			foreach (var trip in ListTripMonth) {
				trip.Month = DateTime.ParseExact (trip.MonthRaw, "MMyy", CultureInfo.InvariantCulture);
			}
		}
	}
}

