﻿using System;
using DriveRightF.Services.Base;
using Newtonsoft.Json;

namespace DriveRightF.Services.Responses
{
	public class GetTripMonthDetailResponse: DriveRightFBaseResponse
	{
		[JsonProperty(PropertyName="mmyy")]
		public string Month { get; set; }
		[JsonProperty(PropertyName="overall_score")]
		public float Score { get; set; }
//
		[JsonProperty(PropertyName="smooth_score")]
		public float SmoothScore { get; set; }

		[JsonProperty(PropertyName="speed_score")]
		public double SpeedScore{ get; set; }

		[JsonProperty(PropertyName="distraction_score")]
		public double DistractionScore{ get; set; }

		[JsonProperty(PropertyName="time_of_day_score")]
		public double TimeOfDayScore{ get; set; }

		[JsonProperty(PropertyName="total_distance")]
		public double TotalDistance { get; set; }
//
		[JsonProperty(PropertyName="total_duration")]
		public double TotalDuration { get; set; }
//
		[JsonProperty(PropertyName="average_speed")]
		public double AverageSpeed { get; set; }

		[JsonProperty(PropertyName="average_distance")]
		public double AverageDistance{ get; set; }
		// Average Distance per trip

		[JsonProperty(PropertyName="average_duration")]
		public double AverageDuration{ get; set; }
//		// Average Duration per trip
//
		[JsonProperty(PropertyName="num_trips")]
		public int TripCount { get; set; }
//
//		public override void RepairData(){
//			
//		}
	}
}

