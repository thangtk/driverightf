﻿using System;
using DriveRightF.Services.Base;
using Newtonsoft.Json;
using System.Collections.Generic;
using DriveRightF.Core.Models;

namespace DriveRightF.Services.Responses
{
	public class MonthlyTripResponse: DriveRightFBaseResponse
	{

		public int Count { get; set; }
		public string Next { get; set;}
		public string Previous{ get; set; }

		[JsonProperty(PropertyName="results")]
		public List<TripSummary> ListTrip { get; set; }
	}
}

