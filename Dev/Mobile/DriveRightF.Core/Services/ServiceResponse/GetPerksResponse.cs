﻿using System;
using DriveRightF.Core.Models;
using System.Collections.Generic;
using DriveRightF.Core.Services;
using DriveRightF.Services.Base;

namespace DriveRightF.Services.Responses
{
	public class GetPerksResponse : DriveRightFBaseResponse
	{
		public long TimeStamp { get; set; }
		public List<Perk> Perks { get; set; }
	}
}

