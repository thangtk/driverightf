﻿using System;
using System.Threading.Tasks;
using DriveRightF.Core.Services;
using System.Collections.Generic;
using DriveRightF.Core.Models;
using Unicorn.Core.Storages;
using Unicorn.Core;
using DriveRightF.Services.Responses;
using DriveRightF.Services.Requests;
using DriveRightF.Services;

//[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightFRFServiceManager))]
namespace DriveRightF.Core.Services
{
	public class DriveRightFRFServiceManager 
	{
		private const string PARTNER_ID = "";
		private DriveRightFRestFulService _service;
		public DriveRightFRFServiceManager(){
			_service = new DriveRightFRestFulService (new DriveRightFServiceConfig ());
		}
		private SQLiteRepository Repository {
			get {
				return DependencyService.Get<SQLiteRepository> ();
			}
		}

		public async Task<GetPerksResponse> GetPerks(GetPerksRequest request)
		{
			await Task.Delay (2000);
			int count = Repository.GetAll<Perk> ().Count;
			Random r = new Random();

			GetPerksResponse response = new GetPerksResponse ();
			response.IsSuccess = true;

			int caseRequest = r.Next(0, 5); //for ints
			Console.WriteLine("caseRequest: {0}", caseRequest);
			switch (caseRequest) {
			case 0:
				response.ErrorMessage = "Request not valid";
				response.IsSuccess = false;
				break;
			case 1:
				response.Perks = new List<Perk> {new Perk () {
						PerkId = (count + 1).ToString (),
						Name = "An award coffee.",
						Used = false,
						Expiry = DateTime.Now.AddDays(10),
						QRCode = "A light and lively coffee with tangy citrus topnotes balanced by a lingering fruity sweetness." + (count + 1).ToString(),
						Image = "coffee_snob.jpg",
						Icon = "award_coffee.png",
						Sponsor = "Hashbro",
						UpdatedTime = DateTime.UtcNow,
						ItemStatus = ItemStatus.New,
						Description = "\t A naturally processed, full-bodied coffee with a silky mouth feel and intense fruit sweetness. " +
							" A treasure from the birthplace of coffee.  Roast: City or Full City to highlight fruity topnotes, Vienna or N. Italian for a sweeter, heavier cup. " + count.ToString()
					}
				};
				break;
			case 2:
				response.Perks = new List<Perk> {new Perk () {
						PerkId = (count + 1).ToString (),
						Name = "An award crown.",
						Used = false,
						Expiry = DateTime.Now.AddDays (9),
						QRCode = "A light and lively coffee with tangy citrus topnotes balanced by a lingering fruity sweetness." + (count + 1).ToString(),
						Image = "coffee_snob.jpg",
						Icon = "award_crown.png",
						Sponsor = "kyho",
						UpdatedTime = DateTime.UtcNow,
						ItemStatus = ItemStatus.New,
						Description = "\t A naturally processed, full-bodied coffee with a silky mouth feel and intense fruit sweetness.  " +
							"A treasure from the birthplace of coffee.  Roast: City or Full City to highlight fruity topnotes, Vienna or N. Italian for a sweeter, heavier cup." +
							"\t A naturally processed, full-bodied coffee with a silky mouth feel and intense fruit sweetness.  " +
							"A treasure from the birthplace of coffee.  Roast: City or Full City to highlight fruity topnotes, Vienna or N. Italian for a sweeter, heavier cup." + count.ToString()
					}
				};
				break;
			case 3:
				response.Perks = new List<Perk> {new Perk () {
						PerkId = (count + 1).ToString (),
						Name = "An award dimond.",
						Used = false,
						Expiry = DateTime.Now.AddDays(7),
						QRCode = "A light and lively coffee with tangy citrus topnotes balanced by a lingering fruity sweetness." + (count + 1).ToString(),
						Image = "coffee_snob.jpg",
						Icon = "award_dimond.png",
						Sponsor = "xxxxxx",
						UpdatedTime = DateTime.UtcNow,
						ItemStatus = ItemStatus.New,
						Description = "\t A naturally processed, full-bodied coffee with a silky mouth feel and intense fruit sweetness.  " +
							"A treasure from the birthplace of coffee.  Roast: City or Full City to highlight fruity topnotes, Vienna or N. Italian for a sweeter, heavier cup." +
							"\t A naturally processed, full-bodied coffee with a silky mouth feel and intense fruit sweetness.  " +
							"A treasure from the birthplace of coffee.  Roast: City or Full City to highlight fruity topnotes, Vienna or N. Italian for a sweeter, heavier cup." + count.ToString()
					}
				};
				break;
			case 4:
				response.Perks = new List<Perk> {new Perk () {
						PerkId = (count + 1).ToString (),
						Name = "An award thunderbolt.",
						Used = false,
						Expiry = DateTime.Now.AddDays(15),
						QRCode = "A light and lively coffee with tangy citrus topnotes balanced by a lingering fruity sweetness." + (count + 1).ToString(),
						Image = "coffee_snob.jpg",
						Icon = "award_thunderbolt.png",
						Sponsor = "thunderbolt",
						UpdatedTime = DateTime.UtcNow,
						ItemStatus = ItemStatus.New,
						Description = "\t A naturally processed, full-bodied coffee with a silky mouth feel and intense fruit sweetness." +
							"  A treasure from the birthplace of coffee.  Roast: City or Full City to highlight fruity topnotes, Vienna or N. Italian for a sweeter, heavier cup." + count.ToString()
					}
				};
				break;
			}
			return response;		
		}
//
		public async Task<GetSponsorRegistrationResponse> GetSponsorRegistration(GetSponsorRegistrationRequest request)
		{
			await Task.Delay (2000);
			Random r = new Random();

			GetSponsorRegistrationResponse response = new GetSponsorRegistrationResponse ();

			int caseRequest = r.Next(1, 2); //for ints
			Console.WriteLine("caseRequest: {0}", caseRequest);

			switch (caseRequest) {
			case 0:
				response.ErrorMessage = "Invalid request";
				response.Response = string.Empty;;
				break;
			case 1:
				int count = Repository.GetAll<Sponsor> ().Count;
				response.Response = "success";
				response.SponsorRegistration = new SponsorRegistration {
					CarRegistration = new CarRegistration()
					{
						SponsorId = request.SponsorId,
						CarNo = "CarNo " + request.SponsorId,
						CarModel = "CarModel " + request.SponsorId,
					},
					DriverLicense = new DriverLicense()
					{
						SponsorId = request.SponsorId,
						LicenseNo = "LicenseNo " + request.SponsorId,
						Insurer = "Insurer " + request.SponsorId,
					},
					Insurance = new Insurance()
					{
						SponsorId = request.SponsorId,
						PolicyNo = "PolicyNo " + request.SponsorId,
						Insurer = "Insurer " + request.SponsorId,
						Expiry = DateTime.UtcNow.AddDays(-20),
					}
				};
				break;
			}


			return response;
		}

		public async Task<UpdateProfileResponse> UpdateProfile(UpdateProfileRequest request)
		{
			await Task.Delay (2000);
			Random r = new Random();

			UpdateProfileResponse response = new UpdateProfileResponse ();

			int caseRequest = r.Next(1, 2); //for ints
			Console.WriteLine("caseRequest: {0}", caseRequest);

			switch (caseRequest) {
			case 0:
				response.ErrorMessage = "UserName not valid";
				response.Response = string.Empty;;
				break;
			case 1:
				response.Response = "Uodate Profile success";
				response.ErrorMessage = string.Empty;
				break;
			}


			return response;
		}
			

		public async Task<UpdateSponsorRegistrationResponse> UpdateSponsorRegistration(UpdateSponsorRegistrationRequest request)
		{
			await Task.Delay (2000);
			Random r = new Random();

			UpdateSponsorRegistrationResponse response = new UpdateSponsorRegistrationResponse ();

			int caseRequest = r.Next(1, 2); //for ints
			Console.WriteLine("caseRequest: {0}", caseRequest);
			switch (caseRequest) {
			case 0:
				response.ErrorMessage = "UserName not invalid";
				break;
			case 1:
				response.Response = "Update Profile success";
				break;
			}


			return response;
		}

		public async Task<GetEarningsResponse> GetEarnings(GetEarningsRequest request)
		{
			await Task.Delay (2000);
			Random r = new Random();

			GetEarningsResponse response = new GetEarningsResponse ();
		
			int caseRequest = r.Next(0, 4); //for ints
			switch (caseRequest) {
			case 0:
				response.ErrorMessage = "Error";
				break;
			case 1:
				response.Earnings = new List<Earning> () {
					new Earning () {
						Icon = "",
						Progress = 40,
						Description = "Description",
						Name = "Name",
						Status = EarningStatus.Done,
					}
				};
				break;
			case 2:
				response.Earnings = new List<Earning> () {
					new Earning () {
						Icon = "",
						Progress = 40,
						Description = "Description",
						Name = "Name",
						Status = EarningStatus.InProgress,
					}
				};
				break;
			case 3:
				response.Earnings = new List<Earning> () {
					new Earning () {
						Icon = "",
						Progress = 40,
						Description = "Description",
						Name = "Name",
						Status = EarningStatus.New,
					}
				};
				break;
			}


			return response;
		}
	}
}

