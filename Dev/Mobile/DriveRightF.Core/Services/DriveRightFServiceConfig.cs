﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Unicorn.Core.Services.Base;

namespace DriveRightF.Services
{
	/// <summary>
	/// Configuration for json rpc service
	/// </summary>
	public class DriveRightFServiceConfig : IServiceConfig
	{
		// Development and Test servers
		public const string DEV_TEMP_SERVICE = "http://ec2-54-154-200-156.eu-west-1.compute.amazonaws.com/app/";

		private string _activeServiceUrl = string.Empty;
        public DriveRightFServiceConfig()
		{
			_activeServiceUrl = DefaultServiceUrl;
		}

        public string ActiveUploadUrl
        { 
            get
            {
				return DEV_TEMP_SERVICE;
            } 
        }

        public string ActiveServiceUrl
		{
            get { return _activeServiceUrl; }

            set { _activeServiceUrl = value; }
		}

        public string DefaultServiceUrl
        {
			get { return DEV_TEMP_SERVICE; }
        }

		public string ServiceVersion
		{
			get { return "211761"; }
		}

		public int RequestTimeout
		{
			get { return 10000; } // 20s
		}

		public int NumberOfRetry
		{
			get { return 2; }
		}
	}
}