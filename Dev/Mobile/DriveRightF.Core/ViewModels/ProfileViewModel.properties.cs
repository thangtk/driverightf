﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReactiveUI;
using DriveRightF.Core.ViewModels;
using DriveRightF.Core.Enums;
using DriveRightF.Core.Managers;
using Unicorn.Core;
using Unicorn.Core.UI;
using DriveRightF.Core.Models;

namespace DriveRightF.Core.ViewModels
{
	public partial class ProfileViewModel
	{

		private string _imagePath = String.Empty;
		private string _iconUrl = String.Empty;
		private string _screenTitle = String.Empty;

//		private UTextBoxViewModel _nameModel, _socialModel, _phoneModel, _passModel, _wechatModel;
//		private UDatePickerViewModel _DOBModel;

//		private bool _isAllPass = true;
//		private bool _isEnableButton = true;
		private bool _isPictureLoading = false;

//		private IReactiveCommand<object> _loadDataCommand = ReactiveCommand.Create();
		private IReactiveCommand<object> _onBackClicked = ReactiveCommand.Create();
//		private IReactiveCommand<object> _btnAddPersonClicked = ReactiveCommand.Create();
		private IReactiveCommand<object> _onCaptureClicked = ReactiveCommand.Create();
//		private IReactiveCommand<object> _loadViewContent = ReactiveCommand.Create();
//		private IReactiveCommand<object> _checkUserChanged = ReactiveCommand.Create ();
//		private IReactiveCommand<object> _itemClickCommand = ReactiveCommand.Create ();

		public IReactiveCommand<Profile> GetProfileCommand { get; set; }
		public IReactiveCommand<bool> UpdateProfileCommand { get; set; }

		#region IReactiveCommand
		//		public IReactiveCommand<object> LoadDataCommand 
		//		{
		//			get {
		//				return _loadDataCommand;
		//			}
		//			set{ 
		//				_loadDataCommand = value;
		//			}
		//		}

		public IReactiveCommand<object> OnBackClicked 
		{
			get {
				return _onBackClicked;
			}
			set{ 
				_onBackClicked = value;
			}
		}

//		public IReactiveCommand<object> LoadViewContent 
//		{
//			get {
//				return _loadViewContent;
//			}
//			set{ 
//				_loadViewContent = value;
//			}
//		}

//		public IReactiveCommand<object> BtnAddPersonClicked 
//		{
//			get {
//				return _btnAddPersonClicked;
//			}
//			set{ 
//				_btnAddPersonClicked = value;
//			}
//		}
		public IReactiveCommand<object> OnCaptureClicked 
		{
			get {
				return _onCaptureClicked;
			}
			set{ 
				_onCaptureClicked = value;
			}
		}

//		public IReactiveCommand<object> CheckUserChanged {
//			get {
//				return _checkUserChanged;
//			}
//			set {
//				_checkUserChanged = value;
//			}
//		}
		#endregion

		public ProfileManager ProfileManager{
			get { 
				return DependencyService.Get<ProfileManager> ();
			}

		}

		public IImagePicker ImagePicker{
			get { 
				return DependencyService.Get<IImagePicker> ();
			}
		}

		public IImageManager ImageManager{
			get { 
				return DependencyService.Get<IImageManager> ();
			}
		}

		public PopupManager PopupManager{
			get { 
				return DependencyService.Get<PopupManager> ();
			}
		}

		public bool IsPictureLoading
		{
			get {
				return _isPictureLoading;
			}
			set {
				this.RaiseAndSetIfChanged (ref _isPictureLoading, value);
			}
		}

//		public bool IsAllPass {
//			get {
//				return _isAllPass;
//			}
//			set {
//				this.RaiseAndSetIfChanged (ref _isAllPass, value);
//			}
//		}

		//		public bool IsEnableButton {
		//			get {
		//				return _isEnableButton;
		//			}
		//			set {
		//				this.RaiseAndSetIfChanged (ref _isEnableButton, value);
		//			}
		//		}

		public string ImagePath {
			get { return _imagePath;}
			set { 
				this.RaiseAndSetIfChanged (ref _imagePath, value);
			}
		}

		public string IconUrl {
			get { return _iconUrl ?? "";} 
			set { this.RaiseAndSetIfChanged (ref _iconUrl, value);}
		}

		public object IconData {
			get { return _iconData;}
			set { 
				this.RaiseAndSetIfChanged (ref _iconData, value);
			}
		}

		//TODO: create viewmodel to check data.
//		public UTextBoxViewModel NameModel {
//			get { return _nameModel;}
//			set { 
//				this.RaiseAndSetIfChanged (ref _nameModel, value);
//			}
//		}
//		public UTextBoxViewModel SocialModel {
//			get {
//				return _socialModel;
//			}
//			set { 
//				this.RaiseAndSetIfChanged (ref _socialModel, value);
//			}
//		}
//		public UDatePickerViewModel DOBModel {
//			get { return _DOBModel;}
//			set { 
//				this.RaiseAndSetIfChanged (ref _DOBModel, value);
//			}
//		}
//		public UTextBoxViewModel PhoneNumberModel {
//			get { return _phoneModel;}
//			set { 
//				this.RaiseAndSetIfChanged (ref _phoneModel, value);
//			}
//		}
//		public UTextBoxViewModel PasswordModel {
//			get { return _passModel;}
//			set { 
//				this.RaiseAndSetIfChanged (ref _passModel, value);
//			}
//		}
//		public UTextBoxViewModel WechatModel {
//			get { return _wechatModel;}
//			set { 
//				this.RaiseAndSetIfChanged (ref _wechatModel, value);
//			}
//		}

		public string ScreenTitle {
			get { return _screenTitle;}
			set { 
				this.RaiseAndSetIfChanged (ref _screenTitle, value);
			}
		}

		public string SMSCode
		{
			get;
			set;
		}
	}
}

