﻿using System;
using DriveRightF.Core;
using System.Windows.Input;
using ReactiveUI;
using System.Reactive.Linq;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using DriveRightF.Core.Enums;

namespace DriveRightF.Core.ViewModels
{

	public class HomeViewModel : BaseViewModel
	{
		private IReactiveCommand<object> _perkButtonClickCommand  = ReactiveCommand.Create();
		private IReactiveCommand<object> _tripButtonClickCommand  = ReactiveCommand.Create();
		private IReactiveCommand<object> _earningButtonClickCommand  = ReactiveCommand.Create();
		private IReactiveCommand<object> _sponsorButtonClickCommand  = ReactiveCommand.Create();
		private IReactiveCommand<object> _menuButtonClickCommand  = ReactiveCommand.Create();
		private IReactiveCommand<object> _profileButtonClickCommand  = ReactiveCommand.Create();

		public IReactiveCommand<object> PerkButtonClickCommand {
			get {
				return _perkButtonClickCommand;
			}
			set{ 
				_perkButtonClickCommand = value;
			}
		}
		public IReactiveCommand<object> TripButtonClickCommand {
			get {
				return _tripButtonClickCommand;
			}
			set{ 
				_tripButtonClickCommand = value;
			}
		}
		public IReactiveCommand<object> EarningButtonClickCommand {
			get {
				return _earningButtonClickCommand;
			}
			set{ 
				_earningButtonClickCommand = value;
			}
		}
		public IReactiveCommand<object> SponsorButtonClickCommand {
			get {
				return _sponsorButtonClickCommand;
			}
			set{ 
				_sponsorButtonClickCommand = value;
			}
		}
		public IReactiveCommand<object> MenuButtonClickCommand {
			get {
				return _menuButtonClickCommand;
			}
			set{ 
				_menuButtonClickCommand = value;
			}
		}
		public IReactiveCommand<object> ProfileButtonClickCommand {
			get {
				return _profileButtonClickCommand;
			}
			set{ 
				_profileButtonClickCommand = value;
			}
		}


		public override int ScreenToNotify {
			get {
				return (int)Screen.Home;
			}
		}

		public HomeViewModel() : base()
		{
			
		}

		protected override void InitCommands()
		{
			PerkButtonClickCommand
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(obj => {
					DependencyService.Get<INavigator>().Navigate((int)Screen.Perks, null);
				});

			TripButtonClickCommand
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(obj => {
					DependencyService.Get<INavigator>().Navigate((int)Screen.Trips, null);
				});

			EarningButtonClickCommand
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(obj => {
					DependencyService.Get<INavigator>().Navigate((int)Screen.Earnings, null);
				});

			SponsorButtonClickCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (action => {
					DependencyService.Get<INavigator>().Navigate((int)Screen.Sponsors, null);
			});

			MenuButtonClickCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (action => {
				if (action != null)
					((Action)action).Invoke ();
			});

			ProfileButtonClickCommand
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(obj => {
					DependencyService.Get<INavigator>().Navigate((int)Screen.Profile, null);
				});
		}
	}
}

