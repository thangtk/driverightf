﻿using System;
using DriveRightF.Core;
using ReactiveUI;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using System.Text.RegularExpressions;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using System.Diagnostics;
using System.Threading;
using DriveRightF.Core.Managers;
using DriveRightF.Core.Services;
using System.Threading.Tasks;
using DriveRightF.Core.Enums;
using Unicorn.Core.Services.Base;
using System.Collections.Generic;
using Unicorn.Core.UI;

namespace DriveRightF.Core.ViewModels
{
	public class LoginViewModel:BaseViewModel
	{
//		public IReactiveCommand<string> LoginButtonClickCommand;
//		public IReactiveCommand<object> LoginTextboxSelectedCommand = ReactiveCommand.Create();

		public IReactiveCommand<object> _formatValidate;
		public IReactiveCommand<bool> _loginCommand;
		public IReactiveCommand<bool> _registerCommand;

		public IReactiveCommand<object> FormatValidate {
			get {
				return _formatValidate;
			}
			set{ 
				_formatValidate = value;
			}
		}
		public IReactiveCommand<bool> LoginCommand {
			get {
				return _loginCommand;
			}
			set{ 
				_loginCommand = value;
			}
		}
		public IReactiveCommand<bool> RegisterCommand {
			get {
				return _registerCommand;
			}
			set{ 
				_registerCommand = value;
			}
		}
		private string _loginText = string.Empty;
		private bool _errorLabelEnabled = true;
		private bool _loginTextboxSelected;

		public override int ScreenToNotify {
			get {
				return (int)Screen.Login;
			}
		}

		private UserManager UserManager {
			get { return DependencyService.Get<UserManager> ();}
		}

		private IPolaraxSDK PolaraxSDK {
			get { return DependencyService.Get<IPolaraxSDK> ();}
		}

		public bool LoginTextboxSelected {
			get { return _loginTextboxSelected;}
			set {this.RaiseAndSetIfChanged (ref _loginTextboxSelected, value);}
		}

		public bool ErrorLabelHidden {
			get { return _errorLabelEnabled;}
			set { this.RaiseAndSetIfChanged (ref _errorLabelEnabled, value);}
		}

		public string LoginText {
			get { return _loginText ?? string.Empty;}
			set {
				ErrorLabelHidden = true;
				this.RaiseAndSetIfChanged (ref _loginText, value);}
		}

		public LoginViewModel() : base()
		{
			ErrorReportType = ErrorReportType.None;
		}

		protected override void InitCommands()
		{
			FormatValidate = ReactiveCommand.Create ();
			FormatValidate.Subscribe (_ => {
				bool valid = ValidatePhoneNumber(LoginText);
				if(valid){
					LoginCommand.Execute(null);
				}
				else{
					ErrorLabelHidden = false;
					// QUESTION: where is error text?
				}
			});

			LoginCommand = ReactiveCommand.CreateAsyncObservable (obj => {
				PopupManager.ShowLoading();
				return Observable.Start (() => UserManager.Login (LoginText));
			});

			LoginCommand
				.Subscribe (b => {
					if (b) {
						HandleLoginSuccess();
						
					} 
//					else {
//						PopupManager.ShowConfirm("Error", "Login failed!\nDo you want to register with the phone number " + LoginText + " or retry to login?"
//							, ()=>{
//								RegisterCommand.Execute(null);
//							}
//							, ()=> {
//								LoginCommand.Execute(null);
//							}, "Register", "Retry Login");
//					}
			});
			LoginCommand.ThrownExceptions.Subscribe (e => {
				PopupManager.HideLoading ();
				var options = new List<DialogButton>();
				options.Add(new DialogButton(){
					Text = "Register",
					OnClicked = new Action(() => {
						RegisterCommand.Execute (null);
					})
				});
				options.Add(new DialogButton(){
					Text = "Retry Login",
					OnClicked = new Action(() => {
						LoginCommand.Execute (null);
					})
				});options.Add(new DialogButton(){
					Text = "Cancel",
					OnClicked = null
				});

				PopupManager.ShowDialogManyOptions ("Error", "Login failed!\nDo you want to register with the phone number \"" + LoginText + "\"?",
					Unicorn.Core.Enums.DialogIcon.Error,
					options);
//				PopupManager.ShowConfirm ("Error", "Login failed!\nDo you want to register with the phone number \"" + LoginText + "\" or retry to login?"
//					, () => {
//					RegisterCommand.Execute (null);
//				}
//					, () => {
//					LoginCommand.Execute (null);
//				}, "Register", "Retry Login");
			});
			// Register command
			RegisterCommand = ReactiveCommand.CreateAsyncObservable (obj => {
				PopupManager.ShowLoading();
				return Observable.Start (() => UserManager.Register (LoginText));
			});
			RegisterCommand
				.Subscribe (b => {
					if (b) {
						HandleLoginSuccess();
					} else {
//						PopupManager.ShowConfirm("Error", "Login failed!\nDo you want to register with the phone number " + LoginText + " or retry to login?"
//							, ()=>{
//								RegisterCommand.Execute(null);
//							}
//							, ()=> {
//								LoginCommand.Execute(null);
//							}, "Register", "Retry Login");
					}
				});
			RegisterCommand.ThrownExceptions.Subscribe (e => {
				PopupManager.HideLoading ();
				string msg;
				if(e is ServiceException){
					msg = (e as ServiceException).ErrorMessage;
				} else {
					msg = e.Message;
				}
				PopupManager.ShowAlert("Error", e.Message, Unicorn.Core.Enums.DialogIcon.Error);
			});
		}

		private void HandleLoginSuccess()
		{
			PopupManager.HideLoading();
			UserManager.SaveCustomerRef (LoginText);
			DependencyService.Get<INavigator>().Navigate((int)Screen.Home);
			RxApp.MainThreadScheduler.Schedule(() =>
				PolaraxSDK.StartService (x => Console.WriteLine (x.ErrorMessage)));
		}

		private bool ValidatePhoneNumber(string phoneNumber){
			return !string.IsNullOrEmpty(phoneNumber);
			Match match = Regex.Match(LoginText, @"^[+. ]?\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4,6})$");
			return match.Success;
		}
	}
}