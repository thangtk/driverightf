﻿using System;
using DriveRightF.Core.Models;
using ReactiveUI;
using System.Collections.Generic;
using System.Reactive.Linq;
using DriveRightF.Core.Managers;
using Unicorn.Core;
using System.Linq;

namespace DriveRightF.Core
{
	public class TripHomeViewModel : BaseHomeListViewModel<TripMonth>
	{
		public TripHomeContentViewModel TripContentViewModel {
			get;
			set;
		}
		public TripHomeViewModel () : base()
		{
			BannerViewModel = new BannerViewModel ()
			{
				BannerTile = Translator.Translate("TRIP_DETAILS_SCORE"),
				BannerIcon = "car_ico.png",
				BannerBackground = "trip_bg.png",
			};

			TripContentViewModel = new TripHomeContentViewModel (BannerViewModel);
		}

		#region implemented abstract members of BaseHandlerHeaderViewModel

		public override void UpdateHeaderFooter ()
		{
			HeaderViewModel.IsShowBtnBack = true;
			HeaderViewModel.Title = Translator.Translate("TAB_TRIPS");

			TabBarViewModel.IsHidden = false;
			TabBarViewModel.ItemSelected = -1;
		}

		#endregion
	}
}

