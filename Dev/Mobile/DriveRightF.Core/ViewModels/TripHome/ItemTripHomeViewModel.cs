﻿using System;
using DriveRightF.Core.Models;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using ReactiveUI;
using DriveRightF.Core.ViewModels;

namespace DriveRightF.Core
{
	public class ItemTripHomeViewModel : BaseViewModel
	{
		private TripMonth _trip;
		private DateTime _month;
		private float _tripScore;
		private ScoreStatus _scoreStatus;

		public TripMonth Trip {
			get
			{
				return _trip;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _trip, value);
			}
		}
			
		public DateTime Month {
			get
			{
				return _month;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _month, value);
			}
		}

		public float TripScore {
			get
			{
				return _tripScore;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _tripScore, value);
			}
		}

		public ScoreStatus ScoreStatus {
			get
			{
				return _scoreStatus;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _scoreStatus, value);
			}
		}

		public ItemTripHomeViewModel ()
		{
			this.WhenAnyValue (x => x.Trip)
				.Where (x => x != null)
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (x => {
					Month = Trip.Month;
					TripScore = Trip.Score;
					ScoreStatus = ScoreUtils.GetScoreStatus(Trip.Score);
			});
		}

		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get {
				throw new NotImplementedException ();
			}
		}

		#endregion
	}
}

