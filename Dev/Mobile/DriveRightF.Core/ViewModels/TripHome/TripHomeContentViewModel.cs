﻿using System;
using DriveRightF.Core.Models;
using ReactiveUI;
using System.Collections.Generic;
using System.Reactive.Linq;
using DriveRightF.Core.Managers;
using Unicorn.Core;
using System.Linq;

namespace DriveRightF.Core
{
	public class TripHomeContentViewModel : HomeListContentViewModel<TripMonth>
	{
		public IReactiveCommand<List<TripMonth>> GetDataFromDb { get; set; }

		public IReactiveCommand<List<TripMonth>> GetDataFromServer { get; set; }

		public TripStatementManager TripStatementManager {
			get {
				return DependencyService.Get<TripStatementManager> ();
			}
		}

		public TripHomeContentViewModel (BannerViewModel bannerViewModel) : base()
		{
			NoDataContent = Translator.Translate("TRIP_NO_DATA");
			BannerViewModel = bannerViewModel;
			GetDataFromDb.Execute (null);
		}

		protected override void InitCommands ()
		{
			base.InitCommands ();
			//
			GetDataFromDb = ReactiveCommand.CreateAsyncObservable (obj => {
				return Observable.Start (() => {
					var data = TripStatementManager.GetTripStatementsList();
					if(data != null){
						UpdateTripSummary(data);
					}
					return data;
				});
			});
			GetDataFromDb.Subscribe (x => {
				GetDataFromServer.Execute(null);
			});

			GetDataFromServer = ReactiveCommand.CreateAsyncObservable (obj => {
				return Observable.Start (() => {
					var data = TripStatementManager.SyncTripStatement();
					if (data != null && data.Count > 0) {
						UpdateTripSummary(data);
					}
					return data;
				});
			});
		}

		private void UpdateTripSummary(List<TripMonth> data){
			//TODO: need reload data 
			if (data == null || data.Count == 0) {
				BannerViewModel.BannerValue = "--";
				DataSources = new List<TripMonth> ();
				return;
			}
			data.OrderByDescending(x => x.Month);

			BannerViewModel.BannerValue = Math.Round(data [0].Score, 0).ToString ();

			//TODO: load datasoure
			data.RemoveAt(0);
			DataSources = data;
		}
	}
}

