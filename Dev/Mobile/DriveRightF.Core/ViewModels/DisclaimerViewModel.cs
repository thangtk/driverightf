﻿using System;
using DriveRightF.Core.Enums;

namespace DriveRightF.Core
{
	public class DisclaimerViewModel : BaseHandlerHeaderViewModel
	{
		#region implemented abstract members of BaseHandlerHeaderViewModel

		public override int ScreenToNotify {
			get {
				return (int)Screen.Disclaimer;
			}
		}

		public override void UpdateHeaderFooter ()
		{
//			HeaderViewModel.Title = Translator.Translate ("HELP_HEADER");
//			HeaderViewModel.IsShowBtnBack = true;
		}

		#endregion

		public DisclaimerViewModel () : base()
		{
		}
	}
}

