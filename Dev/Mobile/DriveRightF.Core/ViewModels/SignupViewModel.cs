﻿using System;
using ReactiveUI;
using DriveRightF.Core.Enums;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using System.Text.RegularExpressions;
using DriveRightF.Core.Managers;
using Unicorn.Core.UI;
using System.Collections.Generic;
using Unicorn.Core;
using Unicorn.Core.Services.Base;

[assembly: Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.Core.ViewModels.SignupViewModel))]
namespace DriveRightF.Core.ViewModels
{
    public class SignupViewModel : BaseViewModel
    {
        private object _smsContentView;
        private string _smsCode;
        private string _phoneNumber;

        public IReactiveCommand<bool> _loginCommand;
        public IReactiveCommand<bool> _registerCommand;

        public IReactiveCommand<bool> LoginCommand
        {
            get
            {
                return _loginCommand;
            }
            set
            {
                _loginCommand = value;
            }
        }

        public IReactiveCommand<bool> RegisterCommand
        {
            get
            {
                return _registerCommand;
            }
            set
            {
                _registerCommand = value;
            }
        }

        public string TextBoxLoginPlaceholder
        {
            get { return Translator.Translate("PLACEHOLDER_PHONENUMBER"); }
        }

        public string ButtonLoginTitle
        {
            get { return Translator.Translate("BUTTON_GET_SMS"); }
        }

        public string LabelSignUpNow
        {
            get { return Translator.Translate("SIGN_UP_NOW"); }
        }

        protected UserManager UserManager
        {
            get
            {
                return DependencyService.Get<UserManager>();
            }
        }

        #region implemented abstract members of BaseViewModel

        public override int ScreenToNotify
        {
            get
            {
                return (int)Screen.Signup;
            }
        }

        #endregion

        public IReactiveCommand<object> GetSMSCommand
        {
            get;
            set;
        }

        //		public IReactiveCommand<object> BindSMSCodeContentViewCommand {
        //			get;
        //			set;
        //		}

        public object SMSContentVIew
        {
            get
            {
                return _smsContentView;
            }
            set
            {
                this.RaiseAndSetIfChanged(ref _smsContentView, value);
            }
        }

        public string SMSCode
        {
            get
            {
                return _smsCode;
            }
            set
            {
                this.RaiseAndSetIfChanged(ref _smsCode, value);
            }
        }

        public string PhoneNumber
        {
            get
            {
                return _phoneNumber;
            }
            set
            {
                this.RaiseAndSetIfChanged(ref _phoneNumber, value);
            }
        }

        public SignupViewModel()
            : base()
        {
            ErrorReportType = ErrorReportType.None;
        }

        protected override void InitCommands()
        {
            base.InitCommands();

            LoginCommand = ReactiveCommand.CreateAsyncObservable(obj =>
            {
                PopupManager.ShowLoading();
                return Observable.Start(() => UserManager.Login(PhoneNumber));
            });

            LoginCommand
                .Subscribe(b =>
            {
                if (b)
                {
                    HandleLoginSuccess();

                }
            });

            LoginCommand.ThrownExceptions.Subscribe(e =>
            {
                var serviceException = e as ServiceException;
                if (serviceException != null)
                {
                    //TODO: just register when sevice not found.
                    if (serviceException.ErrorType == ErrorType.NotFound)
                    {
                        RegisterCommand.Execute(null);
                    }
                    else
                    {
                        PopupManager.HideLoading();
                        PopupManager.ShowAlert(Translator.Translate("ERROR"), Translator.Translate(serviceException.ErrorMessage), Unicorn.Core.Enums.DialogIcon.None, false, null, Translator.Translate("BUTTON_OK"));
                    }
                }
                else
                {
                    PopupManager.HideLoading();
                    //					PopupManager.ShowAlert("Error", e.Message, Unicorn.Core.Enums.DialogIcon.None, false);
                }

            });
            // Register command
            RegisterCommand = ReactiveCommand.CreateAsyncObservable(obj =>
            {
                PopupManager.ShowLoading();
                return Observable.Start(() => UserManager.Register(PhoneNumber));
            });
            RegisterCommand
                .Subscribe(b =>
            {
                if (b)
                {
                    HandleLoginSuccess();
                }
                else
                {
                }
            });
            RegisterCommand.ThrownExceptions.Subscribe(e =>
            {
                PopupManager.HideLoading();
                var serviceException = e as ServiceException;
                if (serviceException != null)
                {
                    PopupManager.ShowAlert(Translator.Translate("ERROR"), Translator.Translate(serviceException.ErrorMessage), Unicorn.Core.Enums.DialogIcon.None, false, null, Translator.Translate("BUTTON_OK"));
                }
                else
                {
                    //					PopupManager.ShowAlert("Error", e.Message, Unicorn.Core.Enums.DialogIcon.None, false);
                }
            });

            GetSMSCommand = ReactiveCommand.Create();
            GetSMSCommand
                .ObserveOn(RxApp.MainThreadScheduler)
                .Subscribe(contentView =>
            {
                if (ValidatePhoneNumber())
                {
                    PopupManager.ShowPopup(
                        Translator.Translate("ENTER_SMS_CODE"),
                        contentView,
                        new System.Collections.Generic.List<Unicorn.Core.UI.DialogButton>() {
							new Unicorn.Core.UI.DialogButton () {
								Text = Translator.Translate("BUTTON_CANCEL"),
								OnClicked = new Action (delegate()
								{
								})
							},
							new DialogButton () {
								Text = Translator.Translate("BUTTON_RESEND"),
								OnClicked = ResendMessage,
								CloseAfterClick = false
							},
							new Unicorn.Core.UI.DialogButton () {
								Text = Translator.Translate("BUTTON_CONTINUE"),
								OnClicked = new Action (delegate()
								{
									if(ValidateSMSCode())
									{
										// TODO: save phone number
//											var manager = new ProfileManager();
//											var profile = manager.GetProfile();
//											if(profile == null){
//												profile = new DriveRightF.Core.Models.Profile();
//											}
//											profile.PhoneNumber = PhoneNumber;
//											manager.Save(profile);
										//

										//TODO: Call old service to login or register.

										CallServiceLogin();
//											Navigator.Navigate ((int)Screen.SecondSignup, this);
									}
									else
									{
										SMSCode = "";
										PopupManager.ShowAlert(Translator.Translate("ERROR"), Translator.Translate("SMS_VERIFY_WRONG"), Unicorn.Core.Enums.DialogIcon.None, false, null, Translator.Translate("BUTTON_OK"));
									}
								}),
										IsBold = true
							},
						},
                        false);
                }
                else
                {
                    PopupManager.ShowAlert(Translator.Translate("ALERT"), Translator.Translate("PHONE_NUMBER_EMPTY_WRONG"), Unicorn.Core.Enums.DialogIcon.None, false, null, Translator.Translate("BUTTON_OK"));
                }
            });


            //			BindSMSCodeContentViewCommand = ReactiveCommand.Create ();
            //			BindSMSCodeContentViewCommand
            //				.ObserveOn (RxApp.MainThreadScheduler)
            //				.Subscribe (view => 
            //					SMSContentVIew = view);
        }

        private bool ValidatePhoneNumber()
        {
            //TODO: add validation in here
            if (PhoneNumber == "12345")
            {
                return true;
            }
            if (!String.IsNullOrEmpty(PhoneNumber))
            {
//                Match match = Regex.Match(PhoneNumber, @"^[+. ]?\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4,6})$");
				Match match = Regex.Match(PhoneNumber, @"^\+(?:[0-9] ?){6,14}[0-9]$");
                return match.Success;
            }
            return false;
        }

        private bool ValidateSMSCode()
        {
            if (!String.IsNullOrEmpty(SMSCode))
            {
                //				#if DEBUG
                if (SMSCode == "12345")
                {
                    return true;
                }
                //				#endif
            }
            return false;
        }

        private void ResendMessage()
        {
        }

        private void CallServiceLogin()
        {
            LoginCommand.Execute(null);
        }

        private void HandleLoginSuccess()
        {
            UserManager.SaveCustomerRef(PhoneNumber);
            PopupManager.HideLoading();
            Navigator.Navigate((int)Screen.SecondSignup, new object[] { this });
            //			DependencyService.Get<INavigator>().Navigate((int)Screen.Home);
            //			RxApp.MainThreadScheduler.Schedule(() =>
            //				PolaraxSDK.StartService (x => Console.WriteLine (x.ErrorMessage)));
        }
    }
}

