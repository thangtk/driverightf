﻿using System;
using ReactiveUI;
using System.Drawing;
using System.Reactive.Linq;
using Unicorn.Core;

namespace DriveRightF.Core.ViewModels
{
	public class CaptureImageViewModel : BaseViewModel
	{
		private IReactiveCommand<object> _doneButtonCommand;
		private IReactiveCommand<object> _cancelButtonCommand;

		//public IReactiveCommand<object> PanMoveCommand  { get; set;}

		private object _imageData;

		public IReactiveCommand<object> DoneButtonCommand {
			get
			{
				return _doneButtonCommand;
			}
			set
			{ 
				_doneButtonCommand = value;
			}
		}

		public IReactiveCommand<object> CancelButtonCommand {
			get
			{
				return _cancelButtonCommand;
			}
			set
			{ 
				_cancelButtonCommand = value;
			}
		}

		public object ImageData {
			get { return _imageData; }
			set { this.RaiseAndSetIfChanged (ref _imageData, value); }
		}

		private float _scale = 1f;

		public float ImageScale {
			get { return _scale; }
			set { this.RaiseAndSetIfChanged (ref _scale, value); }
		}

		private RectangleF _imgRec = RectangleF.Empty;

		public RectangleF ImageRectangle {
			get{ return _imgRec; }
			set { this.RaiseAndSetIfChanged (ref _imgRec, value); }
		}

		private bool _visible = false;

		public bool Visible {
			get{ return _visible; }
			set{ this.RaiseAndSetIfChanged (ref _visible, value); }
		}

		public IImageManager ImageManager {
			get
			{ 
				return DependencyService.Get<IImageManager> ();
			}
		}

		public override int ScreenToNotify {
			get
			{
				return -1;
			}
		}

		public RectangleF CropRectangle { get; set; }
		//public RectangleF BaseRectangle { get;set;}

		public CaptureImageViewModel() : base ()
		{

		}

		protected override void InitCommands ()
		{
			CancelButtonCommand = ReactiveCommand.Create ();
			DoneButtonCommand = ReactiveCommand.Create ();
			CancelButtonCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (obj => {
				Visible = false;
			});
			DoneButtonCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (obj => {
				Visible = false;
			});
		}

		public object CropResizeImage ()
		{

			var scale = ImageScale;
			SizeF size = CropRectangle.Size;
			size.Width = size.Width / scale;
			size.Height = size.Height / scale;
			var rec = CropRectangle;
			rec.Width = rec.Width / scale;
			rec.Height = rec.Height / scale;
			rec.X = (rec.X - ImageRectangle.X) / scale;
			rec.Y = (rec.Y - ImageRectangle.Y) / scale;

			return ImageManager.CropResizeImage (ImageData, size, rec);
		}
	}
}

