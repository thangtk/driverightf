﻿using System;
using System.Collections.Generic;
using DriveRightF.Core.Enums;
using DriveRightF.Core.Managers;
using Unicorn.Core;
using ReactiveUI;
using System.Collections;
using DriveRight.Core.ViewModels;
using System.Reactive.Concurrency;
using DriveRightF.Core.Constants;
using System.Threading.Tasks;

namespace DriveRightF.Core.ViewModels
{

	public class SortAwards : IComparer<Award>
	{
		#region IComparer implementation

		public int Compare (Award x, Award y)
		{
			Award a = x as Award;
			Award b = y as Award;
			if (a.Id > 0) {
				if (b.Id == 0)
					return 1;
				else {
					return CompareUsed (a, b);
				}
			}
			else{
				if (b.Id > 0)
					return -1;
				else
					return 0;
			};

		}

		private int CompareUsed(Award a, Award b)
		{
			if (!a.Used) {
				if (b.Used)
					return 1;
				else
					return b.Expiry.CompareTo (a.Expiry);
			} else {
				if (!b.Used)
					return -1;
				else
					return b.Expiry.CompareTo (a.Expiry);
			}
		}

		#endregion
	}

	public class SortAwardsViewModel : IComparer<AwardTileViewModel>
	{
		#region IComparer implementation

		public int Compare (AwardTileViewModel x, AwardTileViewModel y)
		{
			AwardTileViewModel a = x as AwardTileViewModel;
			AwardTileViewModel b = y as AwardTileViewModel;
			 
			return new SortAwards ().Compare (a.Award, b.Award);

		}

		#endregion
	}

	public interface IHandleCell
	{
		 int Index { get; set; }
	}

	public class LastestCell : IHandleCell
	{
		#region IHandleCell implementation
		public int Index {
			get;
			set;
		}

		public int NextIndex {
			get;
			set;
		}
		#endregion
		
	}

	public class RemoveCell : IHandleCell
	{
		#region IHandleCell implementation
		public int Index {
			get;
			set;
		}
		#endregion

	}

	public class UpdateCell : IHandleCell
	{
		#region IHandleCell implementation
		public int Index {
			get;
			set;
		}
		#endregion

	}


	public class AwardListViewModel : BaseListTileVM<AwardTileViewModel>
	{
		private UpdateCell _updateCell = null;
		private RemoveCell _removeCell = null;
		private LastestCell _lastestCell = null;
		private List<Award> _awards = new List<Award>();


		public UpdateCell UpdateCell
		{
			get {
				return _updateCell;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _updateCell, value);
			}
		}

		public RemoveCell RemoveCell
		{
			get {
				return _removeCell;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _removeCell, value);
			}
		}

		public LastestCell LastestCell
		{
			get {
				return _lastestCell;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _lastestCell, value);
			}
		}

		#region implemented abstract members of BaseHandlerHeaderViewModel

		public override void UpdateHeaderFooter ()
		{
			HeaderViewModel.IsShowBtnBack = false;
			HeaderViewModel.Title = Translator.Translate("DRIVE_RIGHT_HEADER");

			TabBarViewModel.IsHidden = true;
		}

		#endregion

		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get
			{
				throw new NotImplementedException ();
			}
		}

		#endregion

		public AwardManager AwardManager { 
			get { return DependencyService.Get<AwardManager> (); } 
		}

//		public void UpdateColors ()
//		{
//			int i = 0;
//			int j = 0;
//			for (i = 0; i < TileViewModels.Count; i++, j++) {
//				TileViewModels [i].Colors = _colorsList [j];
//				if (j + 1 == _colorsList.Count)
//					j = -1;
//			}
//		}

		private const int MIN_ITEMS_COUNT = 15;

		private void CreateDefaultData(){
			TileViewModels = new List<AwardTileViewModel> ();
			TileViewModels.Add (new AwardTileViewModel (){ 
				Type = AwardType.AwardLarge,
				Award = new Award()
			});
			for (int i = 1; i < MIN_ITEMS_COUNT; i++) {
				TileViewModels.Add (new AwardTileViewModel (){ 
					Type = AwardType.AwardSmall,
					Award = new Award()
				});
			}
		}

		private void LoadData(){
//			System.Threading.Thread.Sleep (5000);
			//TODO: get list award not used and used
			_awards = AwardManager.GetAwardsList ();

			_awards.Sort (new SortAwards ());
			_awards.Reverse ();

			ConvertToAwardListVM (_awards);
			NormalizeData (TileViewModels);

//			UpdateColors ();

			MessagingCenter.Subscribe<Award> (this, MessageConstant.CARD_GAMBLE, x => {
				try{
					AwardManager.Delete(x);
				}catch(Exception e){
					(new ErrorHandler()).Handle(e);
				}
				RemoveCellViewModel();
			});


			MessagingCenter.Subscribe<Award> (this, MessageConstant.CARD_TAKE_MONEY, x => {
				try{
					AwardManager.Delete(x);
				}catch(Exception e){
					(new ErrorHandler()).Handle(e);
				}
				RemoveCellViewModel ();

			});

			MessagingCenter.Subscribe<Award> (this, MessageConstant.AWARD_IS_USED, aa => {
				//TODO: update position to lasted viewmodel

				_awards[CurrentIndex] =aa;
				TileViewModels[CurrentIndex].Award = aa;

				_awards.Sort(new SortAwards());
				_awards.Reverse();

				TileViewModels.Sort(new SortAwardsViewModel());
				TileViewModels.Reverse();

				//				UpdateColors();
				CurrentIndex = _awards.FindIndex (x => x == aa);
				UpdateCell = new UpdateCell()
				{
					Index = CurrentIndex,
				};

				//TODO: remove view and update frame for screen.
			});
		}

		public AwardListViewModel() : base()
		{
			CreateDefaultData ();
			Task.Factory.StartNew (LoadData);
		}

		private void RemoveCellViewModel ()
		{
			//TODO: update list viewmodel
			TileViewModels.RemoveAt (CurrentIndex);
			_awards.RemoveAt (CurrentIndex);
			NormalizeData (TileViewModels);
			//				UpdateColors();
			RemoveCell = new RemoveCell () {
				Index = CurrentIndex
			};
			//TODO: remove view and update frame for screen.
		}

		private void ConvertToAwardListVM(List<Award> award)
		{
//			List< AwardTileViewModel> result = new List<AwardTileViewModel> ();
//			award.ForEach (item => result.Add (new AwardTileViewModel () { Award = item }));
//			TileViewModels = result;
			for (int i = 0; i < award.Count; i++) {
				if (i < TileViewModels.Count) {
					TileViewModels [i].Award = award [i];
				} else {
					TileViewModels.Add (new AwardTileViewModel (){ Award = award [i] });
				}
			}
		}

		private void NormalizeData(List<AwardTileViewModel> tiles)
		{			
			int numberMissingTiles = MIN_ITEMS_COUNT - tiles.Count;

			if (numberMissingTiles > 0) 
				AddMissingData (tiles, numberMissingTiles);
			else if (numberMissingTiles < 0)
			{
				numberMissingTiles = (int)((Math.Abs (numberMissingTiles)) % 3f);
				AddMissingData (tiles, numberMissingTiles);
			}
		}

		private void AddMissingData(List<AwardTileViewModel> tiles, int numberMissingTiles)
		{
			for (int i = 0; i < numberMissingTiles; i++)
			{
				AwardTileViewModel viewmodel = new AwardTileViewModel () {
					Award = new Award(),
					Type = AwardType.AwardSmall
				};
				tiles.Add (viewmodel);
			}
		}
	}
}

