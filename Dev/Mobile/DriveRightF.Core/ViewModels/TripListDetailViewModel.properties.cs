﻿using System;
using ReactiveUI;
using DriveRightF.Core.ViewModels;

namespace DriveRight.Core.ViewModels
{
	public partial class TripListDetailViewModel
	{
		private string _tripId;
		private string _headerTitle;
		private DateTime _startTime;
		private DateTime _endTime;
		private string _startTimeString = "--:--";
		private string _endTimeString = "--:--";
		private string _startTimeAmOrPm;
		private string _endTimeAmOrPm;
		private string _score = "---";
		private string _headerLogo;

		private string _distance = "----";
		private string _duration = "----";
		private string _averageSpeed = "----";
		private string _smoothScore;
		private string _speedScore;
		private string _distractionScore;
		private string _timeOfDayScore;

		public string HeaderLogo {
			get { return _headerLogo; }
			set { this.RaiseAndSetIfChanged (ref _headerLogo, value); }
		}

		public string TripId {
			get { return _tripId; }
			set { this.RaiseAndSetIfChanged (ref _tripId, value); }
		}

		public string HeaderTitle {
			get { return _headerTitle; }
			set { this.RaiseAndSetIfChanged (ref _headerTitle, value); }
		}

		public string Distance {
			get { return _distance; }
			set { this.RaiseAndSetIfChanged (ref _distance, value); }
		}

		public string Score {
			get { return _score; }
			set { this.RaiseAndSetIfChanged (ref _score, value); }
		}

		public string Duration {
			get { return _duration; }
			set { this.RaiseAndSetIfChanged (ref _duration, value); }
		}

		public string AverageSpeed {
			get { return _averageSpeed; }
			set { this.RaiseAndSetIfChanged (ref _averageSpeed, value); }
		}

		public string SmoothScore {
			get { return _smoothScore; }
			set { this.RaiseAndSetIfChanged (ref _smoothScore, value); }
		}

		public string SpeedScore {
			get { return _speedScore; }
			set { this.RaiseAndSetIfChanged (ref _speedScore, value); }
		}

		public string DistractionScore {
			get { return _distractionScore; }
			set { this.RaiseAndSetIfChanged (ref _distractionScore, value); }
		}

		public string TimeOfDayScore {
			get { return _timeOfDayScore; }
			set { this.RaiseAndSetIfChanged (ref _timeOfDayScore, value); }
		}

		public string StartTimeString {
			get { return _startTimeString; }
			set { this.RaiseAndSetIfChanged (ref _startTimeString, value); }
		}

		public string EndTimeString {
			get { return _endTimeString; }
			set { this.RaiseAndSetIfChanged (ref _endTimeString, value); }
		}

		public string StartTimeAmOrPm {
			get { return _startTimeAmOrPm; }
			set { this.RaiseAndSetIfChanged (ref _startTimeAmOrPm, value); }
		}

		public string EndTimeAmOrPm {
			get { return _endTimeAmOrPm; }
			set { this.RaiseAndSetIfChanged (ref _endTimeAmOrPm, value); }
		}

		public DateTime StartTime {
			get { return _startTime; }
			set { this.RaiseAndSetIfChanged (ref _startTime, value); }
		}

		public DateTime EndTime {
			get { return _endTime; }
			set { this.RaiseAndSetIfChanged (ref _endTime, value); }
		}
	}
}

