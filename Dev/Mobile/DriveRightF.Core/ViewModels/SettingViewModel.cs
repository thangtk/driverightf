﻿using System;
using ReactiveUI;
using System.Reactive.Linq;
using DriveRightF.Core.Enums;
using Unicorn.Core;
using Unicorn.Core.Navigator;


namespace DriveRightF.Core.ViewModels
{
	public class SettingViewModel : BaseHandlerHeaderViewModel
	{
		protected IPolaraxSDK PolaraxSDK
		{
			get {
				return DependencyService.Get<IPolaraxSDK> ();
			}
		}

		private bool _isWifiUploadOnly;

		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get {
				return (int)Screen.Setting;
			}
		}

		public override void UpdateHeaderFooter ()
		{
//			HeaderViewModel.Title = Translator.Translate ("SETTING_HEADER");
//			HeaderViewModel.IsShowBtnBack = true;
//
//			TabBarViewModel.IsHidden = false;
		}

		#endregion

		public IReactiveCommand<object> _btnBackClickCommand = ReactiveCommand.Create ();

		public IReactiveCommand<object> BtnBackClickCommand
		{
			get
			{
				return _btnBackClickCommand;
			}
			set
			{ 
				_btnBackClickCommand = value;
			}
		}

		public bool IsWifiUploadOnly {
			get
			{
				return _isWifiUploadOnly;
			}
			set {
				this.RaiseAndSetIfChanged (ref _isWifiUploadOnly, value);
			}
		}

		public void SetWifiUploadOnly(bool isWifiUpload)
		{
			// Save to SDK setting
			PolaraxSDK.IsEnable3G = !isWifiUpload;

			// Set from SDK setting. Warning maybe can't update PolaraxSDK 3G after set value
			IsWifiUploadOnly = !PolaraxSDK.IsEnable3G;
		}


		public SettingViewModel () :base()
		{
			//TODO: set wifi from SDK
			IsWifiUploadOnly = !PolaraxSDK.IsEnable3G;

			BtnBackClickCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => DependencyService.Get<INavigator> ().NavigateBack ());
		}
	}
}

