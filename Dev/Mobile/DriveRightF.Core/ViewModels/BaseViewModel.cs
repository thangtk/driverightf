﻿using System;
using Unicorn.Core;
using ReactiveUI;
using System.Linq;

namespace DriveRightF.Core.ViewModels
{
	/// <summary>
	/// TODO: Refactor later
	/// </summary>
	public abstract class BaseViewModel : UBaseViewModel
	{
		public ErrorHandler ErrorHandler {
			get{ 
				return DependencyService.Get<ErrorHandler> ();
			}
		}

		public abstract int ScreenToNotify { get; }

		private ErrorReportType _errorDisplayingType = ErrorReportType.Dialog;
		protected ErrorReportType ErrorReportType {
			get { 
				return _errorDisplayingType;
			}
			set{ 
				_errorDisplayingType = value;
			}
		}

		public BaseViewModel () : base()
		{
			InitViewModel (null);
		}

		public BaseViewModel(object param) : base()
		{
			InitViewModel (param);
		}

		private void InitViewModel(object param){
			Initalize (param);
			InitCommands ();
			HandleExReactiveCommands ();
		}

		/// <summary>
		/// It's invoked before InitCommands
		/// </summary>
		/// <param name="param">Parameter.</param>
		protected virtual void Initalize(object param){
		}

		protected virtual void InitCommands (){}

		private void HandleExReactiveCommands(){
			var properties = this.GetType ().GetProperties ().Where(p => typeof(IReactiveCommand).IsAssignableFrom(p.PropertyType));
			if (properties == null) {
				return;
			}
			foreach (var p in properties) {
				var command = (IReactiveCommand)p.GetValue (this) ;
				if (command == null)
					continue;
				command.ThrownExceptions.Subscribe (ex => {
					switch (ErrorReportType) {
					case ErrorReportType.Dialog:
						ErrorHandler.Handle (ex, ScreenToNotify);
						break;
					case ErrorReportType.None:
						ErrorHandler.Handle (ex, "ReactiveCommand");
						break;
						// TODO: other case
					}
				});

			}
		}

	}
}

