﻿using System;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using System.Reactive;

namespace DriveRightF.Core
{
	public class BannerViewModel : BaseViewModel
	{
		public string _bannerBackground;
		public string _bannerTitle;
		public string _bannerValue;
		public string _bannerIcon;


		public string BannerBackground {
			get {
				return _bannerBackground;
			}
			set {
				this.RaiseAndSetIfChanged (ref _bannerBackground, value);
			}
		}

		public string BannerIcon {
			get {
				return _bannerIcon;
			}
			set {
				this.RaiseAndSetIfChanged (ref _bannerIcon, value);
			}
		}

		public string BannerTile {
			get {
				return _bannerTitle;
			}
			set {
				this.RaiseAndSetIfChanged (ref _bannerTitle, value);
			}
		}

		public string BannerValue {
			get {
				return _bannerValue;
			}
			set {
				this.RaiseAndSetIfChanged (ref _bannerValue, value);
			}
		}

		public BannerViewModel ()
		{
		}

		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get {
				throw new NotImplementedException ();
			}
		}

		#endregion
	}
}

