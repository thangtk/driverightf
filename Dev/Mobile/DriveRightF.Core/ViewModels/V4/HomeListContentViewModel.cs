﻿using System;
using DriveRightF.Core.ViewModels;
using System.Collections.Generic;
using ReactiveUI;
using DriveRightF.Core.Enums;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

namespace DriveRightF.Core
{
	public class HomeListContentViewModel <TCellModel> : BaseViewModel
	{
		private List<TCellModel> _dataSources = new List<TCellModel>();
		private string _noDataContent;

		private IReactiveCommand<List<TCellModel>> _requestNewData;

		private IReactiveCommand<List<TCellModel>> _loadDataFromDb;

		public BannerViewModel BannerViewModel {
			get;
			set;
		}

		public List<TCellModel> DataSources {
			get {
				return _dataSources;
			}
			set {
				this.RaiseAndSetIfChanged (ref _dataSources, value);
			}
		}

		public string NoDataContent{
			get {
				return _noDataContent;
			}
			set {
				this.RaiseAndSetIfChanged (ref _noDataContent, value);
			}
		}

		public IReactiveCommand<List<TCellModel>> RequestNewData {
			get {
				return _requestNewData;
			}
			set {
				_requestNewData = value;
			}
		}

		public IReactiveCommand<List<TCellModel>> LoadDataFromDb {
			get {
				return _loadDataFromDb;
			}
			set {
				_loadDataFromDb = value;
			}
		}

		private ViewState _state;

		public ViewState State {
			get { return _state; }
			set { 
				this.RaiseAndSetIfChanged (ref _state, value);
			}
		}

		public HomeListContentViewModel ()
		{
//			NoDataContent = "NoData";

			this.WhenAnyValue (x => x.DataSources)
				.ObserveOn(RxApp.TaskpoolScheduler)
				.Subscribe (data => {
					if(data == null || data.Count == 0)
					{
						State = ViewState.NoDataFound;
					}
					else
					{
						State = ViewState.DataDisplaying;
					}
			});
				
		}

		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get {
				throw new NotImplementedException ();
			}
		}

		#endregion
	}
}

