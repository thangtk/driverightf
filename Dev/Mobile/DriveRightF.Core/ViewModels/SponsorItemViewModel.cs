﻿using System;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using System.Reactive.Linq;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using DriveRightF.Core.Enums;
using DriveRightF.Core.Models;
using DriveRightF.Core.Managers;
using System.Reactive.Concurrency;

namespace DriveRightF.Core.ViewModels
{
	public class SponsorItemViewModel : BaseViewModel
	{
		private IReactiveCommand<object> _addClickCommand;
		private IReactiveCommand<object> _removeItemStatusCommand;
		private IReactiveCommand<object> _saveSponsorCommand;

		private ImageMultiStatusViewModel _itemIconViewModel;

		private string _name;
		private SponsorStatus _status;
		private SponsorType _type;
		private Sponsor _item;
		public override int ScreenToNotify {
			get {
				return (int)Screen.Sponsors;
			}
		}

		public SponsorStatus Status {
			get{ 
				return _status;
			}
			set{ 
				this.RaiseAndSetIfChanged (ref _status, value);
			}
		}

		public SponsorType Type {
			get{ 
				return _type;
			}
			set{ 
				this.RaiseAndSetIfChanged (ref _type, value);
			}
		}

		public Sponsor Item {
			get{ 
				return _item;
			}

			set{ 
				this.RaiseAndSetIfChanged (ref _item, value);
			}
		}

		public ImageMultiStatusViewModel ItemIconViewModel {
			get
			{ 
				return _itemIconViewModel;
			}
			set{ 
				this.RaiseAndSetIfChanged (ref _itemIconViewModel, value);
			}
		}

		public string Name 
		{
			get
			{ 
				return _name ?? string.Empty;
			}
			set
			{ 
				this.RaiseAndSetIfChanged (ref _name, value);
			}
		}

		public IReactiveCommand<object> AddClickCommand {
			get {
				return _addClickCommand;
			}
			set{ 
				_addClickCommand = value;
			}
		}

		public IReactiveCommand<object> RemoveItemStatusCommand {
			get {
				return _removeItemStatusCommand;
			}
			set{ 
				_removeItemStatusCommand = value;
			}
		}

		public IReactiveCommand<object> SaveSponsorCommand {
			get {
				return _saveSponsorCommand;
			}
			set{ 
				_saveSponsorCommand = value;
			}
		}

		public SponsorItemViewModel () : base()
		{
			InitStatus ();
		}

		protected override void Initalize (object param)
		{
			
		}

		private void InitStatus()
		{
			Status = SponsorStatus.NotRegistered;
		}

		protected override void InitCommands()
		{
			this.WhenAnyValue (vm => vm.Item)
			.Where(i => i != null)
				.Subscribe (item => {
					Name = item.Name;
					Status = item.Status;
					Type = item.Type;
					RxApp.TaskpoolScheduler.Schedule(()=>{

						var imageManager = DependencyService.Get<CachedImageManager>();
						var image = imageManager.GetImage (new CachedImage () { LocalPath = item.Icon, URL = item.IconURL }, item, "Icon");

						var icon = "";
						var iconUrl = "";
						if (image.Cached)
							icon = image.LocalPath;
						else {
							iconUrl = item.IconURL;
						}

//						ItemIconViewModel = new ImageMultiStatusViewModel((int)Screen.Sponsors){
//							Icon = image.LocalPath,
//							IconURl = image.URL,
//							Status = item.ItemStatus
//						};

						if(ItemIconViewModel != null)
						{
							ItemIconViewModel.Icon = icon;
							ItemIconViewModel.IconURl = iconUrl;
							ItemIconViewModel.Status = item.ItemStatus;
						}
					});

			});
			//
			AddClickCommand = ReactiveCommand.Create ();
			AddClickCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe ((object sponsorId) =>
					{
						// WTH?
						MessagingCenter.Send(this,"Click");
						DependencyService.Get<INavigator> ().Navigate ((int)Screen.SponsorRegistration, Item.SponsorID);
					});
			//
			RemoveItemStatusCommand = ReactiveCommand.Create ();
			RemoveItemStatusCommand
				.Subscribe (_ => {
				    ItemIconViewModel.Status = ItemStatus.None;
					Item.ItemStatus = ItemStatus.None;
					DependencyService.Get<SponsorManager> ().Save(Item);
				});

			SaveSponsorCommand = ReactiveCommand.Create ();
			SaveSponsorCommand
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (sponsor => {
				var sponsorManager = new SponsorManager ();
					sponsorManager.Save ((Sponsor)sponsor);
			});
		}
	}
}

