﻿using System;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using System.Collections.Generic;
using System.Reactive.Linq;
using Unicorn.Core;
using DriveRightF.Core.Managers;

namespace DriveRightF.Core
{
	public class HomeNewViewModel : BaseHandlerHeaderViewModel
	{
		#region implemented abstract members of BaseHandlerHeaderViewModel

		public override void UpdateHeaderFooter ()
		{
			HeaderViewModel.Title = Translator.Translate ("DRIVE_RIGHT_HEADER");
			HeaderViewModel.IsShowBtnBack = false;
			HeaderViewModel.IsShowBtnNotification = true;
			HeaderViewModel.IsShowBtnInfo = false;
			HeaderViewModel.IsShowBtnShare = false;

			TabBarViewModel.IsHidden = false;
			TabBarViewModel.ItemSelected = 0;
		}

		#endregion

		public HomeNewViewModel () : base ()
		{
			MockData ();
		}

		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get {
				throw new NotImplementedException ();
			}
		}

		#endregion

		private float _summaryTripScore;

		public float SummaryTripScore {
			get { return _summaryTripScore; }
			set { this.RaiseAndSetIfChanged (ref _summaryTripScore, value); }
		}

		private int _numOfAwards4;

		public int NumOfAwards {
			get { return _numOfAwards4; }
			set { this.RaiseAndSetIfChanged (ref _numOfAwards4, value); }
		}

		private double _currentBalance;

		public double CurrentBalance {
			get { return _currentBalance; }
			set { this.RaiseAndSetIfChanged (ref _currentBalance, value); }
		}

		private List<string> _bannerImages;

		public List<string> BannerImages {
			get { return _bannerImages; }
			set { this.RaiseAndSetIfChanged (ref _bannerImages, value); }
		}

		private int _newNotifications;

		public int NewNotifications {
			get { return _newNotifications; }
			set { this.RaiseAndSetIfChanged (ref _newNotifications, value); }
		}

		public IReactiveCommand<Tuple<float,int,double>> LoadLocalData { get; set; }

		public IReactiveCommand<object> GetNotificationCommand { get; set; }
		// LATER

		protected override void InitCommands ()
		{
			base.InitCommands ();
			//
			LoadLocalData = ReactiveCommand.CreateAsyncObservable(obj => Observable.Start(LoadDataFromDb));

			LoadLocalData.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					SummaryTripScore = x.Item1;
					NumOfAwards = x.Item2;
					CurrentBalance = x.Item3;
					// TEST
//					TestCommand.Execute(null);
			});

			this.WhenAnyValue (vm => vm.NewNotifications).Subscribe (x => HeaderViewModel.NumOfNewNotification = x);
			// TEST
//			TestCommand = ReactiveCommand.CreateAsyncObservable(obj => {
//				return Observable.Interval(TimeSpan.FromMilliseconds(10));
//			});

//			TestCommand.ObserveOn(RxApp.MainThreadScheduler).Subscribe (x => CurrentBalance = x);
		}

//		public ReactiveCommand<long> TestCommand { get; set; }

		private Tuple<float,int,double> LoadDataFromDb(){
			MyDebugger.ShowThreadInfo ("LoadDataFromDb");
			// Get trip score
			var score = DependencyService.Get<TripStatementManager> ().GetSummaryTripScore ();
			// Get number of awards
			var awards  = DependencyService.Get<AwardManager> ().GetNumOfAwards ();
			// Get current balance
			var balance = DependencyService.Get<AccountManager> ().GetCurrentBalance ();
			return Tuple.Create (score, awards, balance);
		}

		private void MockData ()
		{
			BannerImages = new List<string> () {
//				"banner1.png",
				"banner2.png",
				"banner3.png",
				"banner4.png",
				"banner5.png",
			};
			//
			NewNotifications = (new Random ()).Next (5);
		}
	}
}

