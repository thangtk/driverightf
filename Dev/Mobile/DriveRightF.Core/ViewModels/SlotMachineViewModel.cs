﻿using System;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using System.Reactive.Linq;
using Unicorn.Core;
using DriveRightF.Core.Enums;
using DriveRightF.Core.Managers;
using DriveRightF.Core.Models;
using DriveRightF.Core.Constants;

namespace DriveRightF.Core
{
	public class SlotMachineViewModel : BaseHandlerHeaderViewModel
	{
		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get {
				return (int)Screen.Gamble;
			}
		}

		#endregion

		private string _sponsor;

		public string Sponsor {
			get { return _sponsor; } 
			set {
				this.RaiseAndSetIfChanged (ref _sponsor, value);
			}
		}

		private string _expiry;

		public string Expiry {
			get { return _expiry; } 
			set {
				this.RaiseAndSetIfChanged (ref _expiry, value);
			}
		}

		public bool IsEnableButtonBack {
			set {
				HeaderViewModel.IsEnableBtnBack = value;
			}
		}

		public IReactiveCommand<object> LoadDataCommand { get; set;	}

		public IReactiveCommand<object> UpdateAwardCommand { get; set;	}

		public IReactiveCommand<object> UpdateAccountCommand { get; set; }

		public IReactiveCommand<object> NavigateToHomeCommand { get; set; }

		private AccountManager AccountManager {
			get { 
				return DependencyService.Get<AccountManager> ();
			}
		}

		private AwardManager AwardManager {
			get { 
				return DependencyService.Get<AwardManager> ();
			}
		}

		public SlotMachineViewModel ()
		{
			
		}
		Award _award;
		public void SetAwardData(Award award)
		{
			Sponsor = award.Sponsor;
			Expiry = award.Expiry.ToString("dd/MM/yyyy");
			_award = award;
		}

		protected override void InitCommands ()
		{
			base.InitCommands ();

			LoadDataCommand = ReactiveCommand.Create ();
			LoadDataCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					//TODO: load data here
			});

			UpdateAwardCommand = ReactiveCommand.Create();
			UpdateAwardCommand
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (x => {
					UpdateAward((int)x);
				});

			UpdateAccountCommand = ReactiveCommand.Create();
			UpdateAccountCommand
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (x => {
					UpdateAccount((float)x);
					//TODO: sendmessage to remove cardaward!
					MessagingCenter.Send<Award>(_award,MessageConstant.CARD_GAMBLE);

			});

			NavigateToHomeCommand = ReactiveCommand.Create ();
			NavigateToHomeCommand
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (x => {
					if (x != null)
						HeaderViewModel.IsNavigateToHome = (bool)x;
			});
		}

		/// <summary>
		/// Updates the account for Gamble
		/// </summary>
		private void UpdateAccount(float amount)
		{
			Account account = AccountManager.GetCurrentAccount ("1");
			if (account != null) {
				account.CashEarned = (float)Math.Round (account.CashEarned + amount, 2);
				account.EndCard = account.EndCard - 1;
				account.EndBalance = (float)Math.Round (account.EndBalance + amount, 2);

				AccountManager.SaveAccount (account);

				//Create cash transaction history
				AccountTransaction cashTransaction = new AccountTransaction {
					AccountId = "1",
					Time = DateTime.Now,
					Description = Translator.Translate ("BUTTON_GAMBLE"),
					Type = Translator.Translate ("BUTTON_GAMBLE"),
					Value = amount,
					Unit = Translator.Translate ("RMB")
				};
				AccountManager.SaveAccountTransaction (cashTransaction);

				//Create card transaction history
				AccountTransaction cardTransaction = new AccountTransaction {
					AccountId = "1",
					Time = DateTime.Now,
					Description = Translator.Translate ("BUTTON_GAMBLE"),
					Type = Translator.Translate ("BUTTON_GAMBLE"),
					Value = -1,
					Unit = Translator.Translate ("ACCOUNT_STATEMENT_CARD")
				};
				AccountManager.SaveAccountTransaction (cardTransaction);

			}
		}

		/// <summary>
		/// Updates the award.
		/// </summary>
		/// <param name="awardId">Award identifier.</param>
		private void UpdateAward(int awardId)
		{
			var award = AwardManager.GetAward (awardId);
			award.Used = true;
			AwardManager.Save (award);
//			MessagingCenter.Send (this, "Update Award " + awardId.ToString ());
		}

		public override void UpdateHeaderFooter ()
		{
			HeaderViewModel.IsShowBtnBack = true;
			HeaderViewModel.Title = Translator.Translate("BUTTON_GAMBLE");

			TabBarViewModel.IsHidden = true;
		}
	}
}

