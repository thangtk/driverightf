﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Timers;
using DriveRightF.Core.Managers;
using DriveRightF.Core.Models;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using DriveRightF.Services.Responses;

namespace DriveRightF.Core.ViewModels
{
	public class SponsorsViewModel : BaseViewModel// BaseAutoRequestViewModel<Sponsor>
	{
//		public override string Name {
//			get {
//				return "SponsorsViewModel";
//			}
//		}
//
//		public override BaseAutoRequestManager<Sponsor> Manager {
//			get {
//				return DependencyService.Get<SponsorManager> ();
//			}
//		}
//
//		public IReactiveCommand<object> BackCommand { get; set; }
//
//		protected override void InitCommands ()
//		{
//			base.InitCommands ();
//			//
//			BackCommand = ReactiveCommand.Create();
//			BackCommand
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (obj => {
//					Navigator.NavigateBack (false);
//				});
//		}

		private TimeSpan CONFIG_TIME = new TimeSpan(0, 0, 0, 10);

		private static bool _isHaveRequest = false;
		private List<Sponsor> _sponsorItems = new List<Sponsor>();
		private List<Sponsor> _newSponsors;
		private bool _hasNewData = false;
		private bool _scrollToTop;
		private Timer _timerUpdate;
		private DateTime _lastUpdateTime = new DateTime(2015, 3, 4, 0, 0, 0);

		public bool IsHaveRequest {
			get {
				return _isHaveRequest;
			}
			set {
				_isHaveRequest = value;
			}
		}

		public List<Sponsor> SponsorItems {
			get {
				return _sponsorItems;
			}
			set {
				if ((value.Count != 0 && value != null)) {
					this.RaiseAndSetIfChanged (ref _sponsorItems, value);
				} else {
					if (_isHaveRequest) {
						IsHaveRequest = true;
					} else {
						IsHaveRequest = false;
						this.RaiseAndSetIfChanged (ref _sponsorItems, new List<Sponsor> ());
					}
				}
			}
		}

		public bool ScrollToTop {
			get {
				return _scrollToTop;
			}
			set {
				this.RaiseAndSetIfChanged (ref _scrollToTop, value);
			}
		}

		public bool HasNewData {
			get {
				return _hasNewData;
			}
			set {
				_hasNewData = value;
			}
		}
	
		public IReactiveCommand<object> BtnBackClick {
			get;
			private set;
		}

		public IReactiveCommand<object> PullToRefresh {
			get;
			private set;
		}

		public IReactiveCommand<object> BtnNewDataClick {
			get;
			private set;
		}

		public IReactiveCommand<object> ScrollToTopCommand {
			get;
			private set;
		}

		public SponsorsViewModel () : base ()
		{
			InitData ();
			InitCommand ();
		}

		private async void InitData()
		{
			//DependencyService.Get<SponsorManager> ().Repository.DeleteAll<Sponsor> ();

			GetSponsorFromLocal ();
			GetSponsorFromService ();
		}

		private void InitCommand ()
		{
			BtnBackClick = ReactiveCommand.Create ();
			BtnBackClick
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (obj => {
					DependencyService.Get<INavigator> ().NavigateBack (false);
			});

			PullToRefresh = ReactiveCommand.Create ();
			PullToRefresh
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (action => {
					if (action != null)
						((Action)action).Invoke();

					if (_isHaveRequest) {
					} else {
						CallServiceSponsors ();
					}
			});

			BtnNewDataClick = ReactiveCommand.Create ();
			BtnNewDataClick
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (action => {
					if (action != null)
						((Action)action).Invoke();
					ScrollToTop = true;
			});

			ScrollToTopCommand = ReactiveCommand.Create ();
			ScrollToTopCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (action => {
					if (action != null)
						((Action)action).Invoke();
					ScrollToTop = true;
				});
		}

		private async Task GetSponsorFromService()
		{
			DateTime now = DateTime.Now;
			TimeSpan span = now.Subtract (_lastUpdateTime);
			if (TimeSpan.Compare (span, CONFIG_TIME) == 1) {
				//if (IsHaveRequest == false)
				IsHaveRequest = true;
				CallServiceSponsors ();
			}
		}

		private async Task GetSponsorFromLocal()
		{
			SponsorItems = await DependencyService.Get<SponsorManager> ().GetAllSponsorLocal ();
		}

		private async Task CallServiceSponsors()
		{
			GetSponsorsResponse response = await DependencyService.Get<SponsorManager> ().GetSponsorFromService ();
			if (response.Response == "success") {
				_newSponsors = response.Sponsors;
				_hasNewData = CheckHasNewData ();
				if (_hasNewData) {
					SaveData (_newSponsors);
					SponsorItems = _newSponsors;
				}
			} else if (response.Response == "failure") {

			}
			_lastUpdateTime = DateTime.Now;
			IsHaveRequest = false;
		}

		private void SaveData(List<Sponsor> data)
		{
			DependencyService.Get<SponsorManager> ().Save (data);
		}

		private List<Sponsor> UpdateStatusForNewData()
		{
			List<Sponsor> lastSponsors = _sponsorItems;

			if (_newSponsors == null || _newSponsors.Count <= 0)
				return _newSponsors;

			_newSponsors.ForEach (item => {
				item.IsNew = true;
			});

			if (_newSponsors != null && _newSponsors.Count > 0) {
				_newSponsors.ForEach (newItem => {
					lastSponsors.ForEach (oldItem => {
						if (newItem.SponsorID == oldItem.SponsorID)
							newItem.IsNew = false;
					});
				});
			}

			return _newSponsors;
		}

		private bool CheckHasNewData ()
		{
			if (_newSponsors == null || _newSponsors.Count == 0) {
				return false;
			}

			if (_sponsorItems == null || _sponsorItems.Count == 0) {
				return true;
			}

			return _newSponsors != _sponsorItems;
		}
	}
}

