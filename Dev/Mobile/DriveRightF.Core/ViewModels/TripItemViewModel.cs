﻿
using System;
using ReactiveUI;
using DriveRightF.Core.Managers;
using Unicorn.Core;
using DriveRightF.Core.Models;
using System.Reactive.Linq;
using Unicorn.Core.Navigator;
using DriveRightF.Core.Enums;
using System.Drawing;

namespace DriveRightF.Core.ViewModels
{
	public class TripItemViewModel : BaseViewModel
	{
		public override int ScreenToNotify
		{
			get
			{
				return (int)Screen.Trips;
			}
		}

		private TripSummary _item;

		public TripSummary Item
		{
			get
			{
				return _item;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref _item, value);
			}
		}

		private TripManager TripManager
		{
			get
			{
				return DependencyService.Get<TripManager>();
			}
		}

		//		public string StartDay {
		//			get
		//			{
		//				return Item == null ? string.Empty : Item.StartTime.ToLocalTime().ToString ("dd-MM-yyyy");
		//			}
		//		}

		public string StartTime
		{
			get
			{ 
				return Item == null ? string.Empty : Item.StartTime.ToLocalTime().ToString("dd/MM/yyyy HH:mm");
			}
		}

		public string Duration
		{
			get
			{
				if(Item == null)
				{
					return "--";
				}
//				TimeSpan t = Item.EndTime.Subtract (
//					Item.StartTime);

				//TEMP: get duration from duration service (milisecond)
				double duration = Item.Duration;
				return (int)TimeSpan.FromMilliseconds(duration).TotalMinutes + Translator.Translate("MINUTES");
			}
		}

		private string GetDuarationString(double minutes)
		{
			string s = string.Empty;
			if(minutes <= 0)
			{
				s = "--";
			}
			else if(minutes <= 180)
			{
				s = Math.Round(minutes, 0).ToString() + Translator.Translate("MINUTES");
			}
			else
			{
				s = Math.Round(minutes / 60, 1).ToString() + Translator.Translate("HOURS");
			}
			return s;
		}

		public string Distance
		{
			get
			{ 
				return Item == null ? string.Empty : Math.Round(Item.Distance, 1).ToString() + Translator.Translate("TRIPS_STATEMENT_KM");
			}
		}

		public string Score
		{
			get
			{ 
				return Item == null ? string.Empty : Math.Round(Item.Score, 0).ToString();
			}
		}

		public Color ScoreColor
		{
			get
			{ 
				if(Item == null)
				{
					return Color.Black;
				}
				return DependencyService.Get<ScoreManager> ().GetColorByScore ((float)Item.Score);
			}
		}

		private IReactiveCommand<object> _itemClickCommand = ReactiveCommand.Create();

		public IReactiveCommand<object> ItemClickCommand
		{
			get
			{
				return _itemClickCommand;
			}
		}


		public TripItemViewModel() : base()
		{
			
		}

		#region implemented abstract members of BaseViewModel

		protected override void InitCommands()
		{
			ItemClickCommand
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(obj =>
			{
//				MessagingCenter.Send (this, "Click");
				if(obj != null)
				{
					((Action)obj).Invoke();
				}
			});
//			this.WhenAnyValue (vm => vm.Item)
//				.Where (t => t != null)
//				.Subscribe (t => {
//					this.RaisePropertyChanged ("StartDay");
//					this.RaisePropertyChanged ("StartTime");
//					this.RaisePropertyChanged ("Duration");
//					this.RaisePropertyChanged ("Distance");
//					this.RaisePropertyChanged ("Score");
//				});
		}

		#endregion
	}
}


