﻿using System;
using DriveRightF.Core.Enums;

namespace DriveRightF.Core
{
	public class LegalViewModel : BaseHandlerHeaderViewModel
	{
		#region implemented abstract members of BaseHandlerHeaderViewModel

		public override int ScreenToNotify {
			get {
				return (int)Screen.Legal;
			}
		}

		public override void UpdateHeaderFooter ()
		{
//			HeaderViewModel.Title = Translator.Translate ("LEGAL_HEADER");
//			HeaderViewModel.IsShowBtnBack = true;
		}

		#endregion

		public LegalViewModel () : base()
		{
		}
	}
}

