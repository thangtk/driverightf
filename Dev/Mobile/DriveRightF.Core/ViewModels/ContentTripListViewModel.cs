﻿using System;
using ReactiveUI;
using System.Collections.Generic;
using DriveRightF.Core.Models;
using System.Reactive.Linq;
using Unicorn.Core;
using DriveRightF.Core.Managers;
using DriveRightF.Core.Enums;
using System.Threading;

namespace DriveRightF.Core.ViewModels
{
	public class MonthlyTripModel : BaseViewModel
	{
		/// <summary>
		/// For Debug
		/// </summary>
		/// <value>The name.</value>
		public string Name {
			get { 
				return Month.ToString ("MMMM-yyyy");
			}
		}

		public override int ScreenToNotify {
			get {
				return (int)Screen.Trips;
			}
		}

//		public bool InitializedBinding { get; set; }

		private bool _manualRequest;
		public bool AutoRequestSuccess { get; set; }

		private List<TripSummary> _newData = new List<TripSummary>();

		public List<TripSummary> NewData {
			get { 
				return _newData;
			}
			set { 
				this.RaiseAndSetIfChanged (ref _newData, value);
			}
		}

		private bool _requesting;

		public bool Requesting {
			get { 
				return _requesting;
			}
			set { 
				this.RaiseAndSetIfChanged (ref _requesting, value);
			}
		}

		private bool _loading;

		public bool Loading {
			get { 
				return _loading;
			}
			set { 
				this.RaiseAndSetIfChanged (ref _loading, value);
			}
		}

		//		ObservableAsPropertyHelper<ViewState> _state;
		//
		//		public ViewState State {
		//			get { return _state.Value; }
		//		}

		private ViewState _state;

		public ViewState State {
			get { return _state; }
			set { 
				this.RaiseAndSetIfChanged (ref _state, value);
			}
		}

		/// <summary>
		/// Parameter for Manual request = true, auto request = false
		/// </summary>
		/// <value>The request new data.</value>
		private IReactiveCommand<List<TripSummary>> _requestNewData;

		private IReactiveCommand<List<TripSummary>> _loadDataFromDb;
		private IReactiveCommand<bool> _checkAutoRequest;
		public IReactiveCommand<List<TripSummary>> RequestNewData {
			get {
				return _requestNewData;
			}
			set {
				_requestNewData = value;
			}
		}

		public IReactiveCommand<List<TripSummary>> LoadDataFromDb {
			get {
				return _loadDataFromDb;
			}
			set {
				_loadDataFromDb = value;
			}
		}

		public IReactiveCommand<bool> CheckAutoRequest {
			get {
				return _checkAutoRequest;
			}
			set {
				_checkAutoRequest = value;
			}
		}

		public DateTime Month { get; set; }

		private List<TripSummary> _tripList = new List<TripSummary>();

		public List<TripSummary> TripList {
			get {
				return _tripList; // ?? new List<Trip>();
			}
			set {
				this.RaiseAndSetIfChanged (ref _tripList, value);
			}
		}
			
		private bool _contentViewHidden;

		public bool ContentViewHidden {
			get { 
				return _contentViewHidden;
			}
			set { 
				this.RaiseAndSetIfChanged (ref _contentViewHidden, value);
			}
		}

		private bool _noDataHidden;

		public bool NoDataHidden {
			get { 
				return _noDataHidden;
			}
			set { 
				this.RaiseAndSetIfChanged (ref _noDataHidden, value);
			}
		}

		private bool _requestingViewHidden;

		public bool RequestingViewHidden {
			get { 
				return _requestingViewHidden;
			}
			set { 
				this.RaiseAndSetIfChanged (ref _requestingViewHidden, value);
			}
		}

		private TripManager TripManager {
			get { 
				return DependencyService.Get<TripManager> ();
			}
		}

		public MonthlyTripModel (DateTime month) : base()
		{
			AutoRequestSuccess = false;
			Month = month;
		}

//		private void Reset ()
//		{
//			TripList = null;
////			NewData = null;
//			State = ViewState.None;
////			HasNewData = false;
//			Requesting = false;
//		}

		public void LoadData ()
		{
//			Reset ();
			GetCachedOrReloadData ();
			//
			DoAutoRequest ();
		}

		private void GetCachedOrReloadData ()
		{
			if (TripList == null || TripList.Count == 0) {
				LoadDataFromDb.Execute (null);
			}
			else {
				MergeData();
				this.RaisePropertyChanged ("TripList");
			}
		}

		public void DoAutoRequest(){
			if (!AutoRequestSuccess && !Requesting) {
				CheckAutoRequest.Execute (null);
			}
		}

		private void UpdateTrips (List<TripSummary> trips)
		{
			if(trips == null || trips.Count == 0){
				return;
			}
			// When cannot get anything from db
			if (TripList == null || TripList.Count == 0) {
				TripList = trips;
			} else {
				// Notify change immediately
				NewData = trips;
				// If data get from db come later it will be new data -> failure.
				// TODO: edit later
			}
		}

		public void MergeData(){
			if (NewData != null && NewData.Count > 0) {
				TripList.InsertRange (0, NewData); 
				NewData.Clear ();
			}
			// Check to ensure it don't notify 2 times
		}

		protected override void InitCommands()
		{
			LoadDataFromDb = ReactiveCommand.CreateAsyncObservable (obj => {
				Loading = true;
				return Observable.Start (() => TripManager.GetTripsLocal (Month));
//				return Observable.Start (() => TripManager.GetTripFromServer (Month));
			});
			CheckAutoRequest = ReactiveCommand.CreateAsyncObservable (obj => Observable.Start (() => TripManager.CheckAutoRequest (Month)));
			RequestNewData = ReactiveCommand.CreateAsyncObservable (obj => {
				_manualRequest = true.Equals(obj);
				Requesting = true;
				return Observable.Start (() => TripManager.GetTripFromServer (Month));
			});
			// Combine exception handling
			RequestNewData.ThrownExceptions.Subscribe (ex => {
				Requesting = false;
				_manualRequest = false;
			});
			// Subcribe
			LoadDataFromDb
				.Where (x => x != null)
				.Subscribe (data => {
					UpdateTrips(data);
					Loading = false;
				});
			//
			CheckAutoRequest
				.Where (b => b)
				.Subscribe (b => RequestNewData.Execute (false));
			//
			RequestNewData.Subscribe (data => {
				AutoRequestSuccess = true;
				//
				if (data != null && data.Count > 0) {
					UpdateTrips (data);
				}
				//
				Requesting = false;
				_manualRequest = false;
			});
			//
			this.WhenAnyValue (vm => vm.TripList, vm => vm.Requesting, vm => vm.Loading, (t, r, l) => {
				if ((t != null && t.Count > 0)) {
					return ViewState.DataDisplaying;
				} else if ((r || l) && !_manualRequest) {
					return ViewState.Requesting;
				} else {
					return ViewState.NoDataFound;
				}
			}).Subscribe (s => State = s);

			//
			this.WhenAnyValue (vm => vm.State)
				.Subscribe (s => {
//					System.Diagnostics.Debug.WriteLine (">>> Status: " + s.ToString () + " >>> Month >>> " + Name);
				switch (s) {
				case ViewState.None:
					ContentViewHidden = true;
					NoDataHidden = true;
					RequestingViewHidden = true;
					break;

				case ViewState.DataDisplaying:
					ContentViewHidden = false;
					NoDataHidden = true;
					RequestingViewHidden = true;
					break;

				case ViewState.NoDataFound:
					ContentViewHidden = true;
					NoDataHidden = false;
					RequestingViewHidden = true;
					break;

				case ViewState.Requesting:
					ContentViewHidden = true;
					NoDataHidden = true;
					RequestingViewHidden = false;
					break;
				}
			});
			// Change the way to report error
			this.ObservableForProperty (vm => vm.TripList)
				.Subscribe (x => {
					if(x.Value == null || x.Value.Count == 0){
						ErrorReportType = ErrorReportType.Dialog;
					}
					else{
						ErrorReportType = ErrorReportType.None;
					}
				});
		}
	}
}

