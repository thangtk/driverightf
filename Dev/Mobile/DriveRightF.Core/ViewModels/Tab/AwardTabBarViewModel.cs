﻿using System;
using System.Collections.Generic;

namespace DriveRightF.Core
{
	public class AwardTabBarViewModel : UTabBarViewModel
	{
		public AwardTabBarViewModel () : base()
		{
			ItemModels = new List<UTabItemViewModel> {
				new UTabItemViewModel () {
					NameTab = Translator.Translate ("AWARD_BRAND_NEW"),
					IsSelected = true,
				},
				new UTabItemViewModel () {
					NameTab = Translator.Translate ("AWARD_USED"),
					IsSelected = false,
				},
			};
		}
	}
}

