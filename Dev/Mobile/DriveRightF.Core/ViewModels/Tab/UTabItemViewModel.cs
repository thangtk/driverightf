﻿using System;
using DriveRightF.Core.ViewModels;
using ReactiveUI;

namespace DriveRightF.Core
{
	public class UTabItemViewModel : BaseViewModel
	{
		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get {
				return -1;
			}
		}

		#endregion

		private string _nameTab;
		private string _iconSelected;
		private string _iconUnSelected;
		private string _icon;
		private bool _isSelected = false;

		public string NameTab {
			get
			{
				return _nameTab;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _nameTab, value);
			}
		}

		public string IconSelected {
			get;
			set;
		}

		public string IconUnSelected {
			get;
			set;
		}

		public string Icon
		{
			get
			{
				return _icon;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _icon, value);
			}
		}


		public bool IsSelected {
			get
			{
				return _isSelected;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _isSelected, value);
			}
		}

		private IReactiveCommand<object> _tabSelectedCommand =  ReactiveCommand.Create ();

//		public DTabItemViewModel ()
//		{
//			_tabSelectedCommand.
//		}
	}
}

