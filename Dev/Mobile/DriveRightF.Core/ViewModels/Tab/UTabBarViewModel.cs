﻿using System;
using DriveRightF.Core.ViewModels;
using System.Collections.Generic;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Linq;

[assembly: Unicorn.Core.Dependency.Dependency (typeof (DriveRightF.Core.UTabBarViewModel))]
namespace DriveRightF.Core
{
	public class UTabBarViewModel: BaseViewModel
	{
		#region implemented abstract members of BaseViewModel

		private bool _hidden = false;
		private int _itemSelected = -1;
		public bool IsHidden
		{
			get {
				return _hidden;
			}
			set {
				this.RaiseAndSetIfChanged (ref _hidden, value);
			}
		}

		public int ItemSelected
		{
			get {
				return _itemSelected;
			}
			set {
				this.RaiseAndSetIfChanged (ref _itemSelected, value);
			}
		}


		private List<UTabItemViewModel> _items;

		public List<UTabItemViewModel> ItemModels {
			get { return _items; } 
			set
			{
				this.RaiseAndSetIfChanged (ref _items, value);
			}
		}

		public override int ScreenToNotify {
			get {
				return -1;
			}
		}

		#endregion

		public UTabBarViewModel ()
		{
			this.WhenAnyValue (x => x.ItemSelected)
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (x => {
					if(ItemModels != null && ItemModels.Count > 0)
					{
						for (int i = 0; i < ItemModels.Count; i++) {
							ItemModels[i].IsSelected = x==i ? true : false;
						}
					}
			});
		}
	}
}

