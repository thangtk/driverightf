﻿using System;
using System.Collections.Generic;
using Unicorn.Core;
using ReactiveUI;
using System.Threading;
using System.Reactive.Concurrency;
using DriveRightF.Core.Constants;

namespace DriveRightF.Core
{
	public class DRTabBarViewModel : UTabBarViewModel
	{

		public DRTabBarViewModel () : base()
		{
			ItemModels = new List<UTabItemViewModel> {
				new UTabItemViewModel () {
					NameTab = Translator.Translate("TAB_HOME"),
					IconSelected = "home_ico_d@2x.png",
					IconUnSelected = "home_ico@2x.png",
//					Icon = ,
					IsSelected = true,
				},
				new UTabItemViewModel () {
					NameTab = Translator.Translate("TAB_PROFILE"),
					IconSelected = "profile_ico_d@2x.png",
					IconUnSelected = "profile_ico@2x.png",
//					Icon = ,
					IsSelected = false,
				},
				new UTabItemViewModel () {
					NameTab = Translator.Translate("TAB_MORE"),
					IconSelected = "more_ico_d@2x.png",
					IconUnSelected = "more_ico@2x.png",
//					Icon= ,
					IsSelected = false,
				}
			};

            // REM by KT
            MessagingCenter.Subscribe<ICoinView> (this, MessageConstant.COIN_ANIMATE, x => {
                UTabItemViewModel item = ItemModels[2] ;
                if(item != null)
                {
                    item.Icon = !item.IsSelected ? "icon_treasure_chest_open.png" : "icon_treasure_chest_open_white.png";
                    Console.WriteLine("ChangeImage");
                    RxApp.TaskpoolScheduler.Schedule(() => {
                        Thread.Sleep(2000);
                        item.Icon = !item.IsSelected ? item.IconUnSelected : item.IconSelected;
                    });;
                }
            });
		}
	}
}

