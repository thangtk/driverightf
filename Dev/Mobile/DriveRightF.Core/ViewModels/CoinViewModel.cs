﻿using System;
using DriveRightF.Core.ViewModels;
using System.Drawing;

[assembly: Unicorn.Core.Dependency.Dependency (typeof (DriveRightF.Core.CoinViewModel))]
namespace DriveRightF.Core
{
	public interface ICoinView
	{
		void AnimateCoins (int quantity, Action endAnimate, Point startPosition = new Point (), Point endPosition = new Point (),bool isInit = false);
	}

	public class CoinViewModel 
	//	,ICoinView
	{
		//		#region ICoinView implementation

		//		public ICoinView CoinView { get; set; }
		//
		//		public void AnimateCoins (int numberCoint)
		//		{
		//			if (CoinView != null) {
		//				CoinView.AnimateCoins (numberCoint);
		//			}
		//		}
		//
		//		#endregion
		//
		//		#region implemented abstract members of BaseViewModel

		public int CoinNumber { get; set; }
		//
		//		public override int ScreenToNotify {
		//			get {
		//				return -1;
		//			}
		//		}
		//
		//		#endregion
		//
		//		public CoinViewModel ()
		//		{
		//		}

	}
}

