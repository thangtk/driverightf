﻿using DriveRightF.Core.Models;
using Unicorn.Core;
using DriveRightF.Core.Enums;

namespace DriveRightF.Core
{
	public class SponsorViewModel : BaseAutoRequestViewModel<Sponsor>
	{
		
		public SponsorViewModel ()
		{
		}

		#region implemented abstract members of BaseAutoRequestViewModel

		public override int ScreenToNotify {
			get {
				return (int)Screen.Sponsors;
			}
		}

		public override BaseAutoRequestManager<Sponsor> Manager {
			get {
				return DependencyService.Get<BaseAutoRequestManager<Sponsor>> ();
			}
		}
		protected override void Initalize (object param)
		{
			base.Initalize (param);
			HeaderTitle = "Sponsors";
			HeaderLogo = "logo_sponsor.png";
		}
		protected override void InitCommands ()
		{
			base.InitCommands ();
		}
		#endregion
	}
}

