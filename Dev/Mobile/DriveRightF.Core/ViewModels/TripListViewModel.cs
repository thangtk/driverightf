﻿using System;
using System.Collections.Generic;
using ReactiveUI;
using System.Reactive.Linq;
using Unicorn.Core;
using DriveRightF.Core.Enums;
using System.Diagnostics;

namespace DriveRightF.Core.ViewModels
{
	public class ListTripViewModel: BaseHandlerHeaderViewModel
	{
		public const string KEY_MONTHLY_VIEWMODEL = "yyyy-MM";

		public override int ScreenToNotify {
			get {
				return (int)Screen.Trips;
			}
		}

		private DateTime _currentMonth;

		public DateTime CurrentMonth { 
			get {
				return _currentMonth;
			}
			set { 
				this.RaiseAndSetIfChanged (ref _currentMonth, value);
			} 
		}

		private string _monthTitle;

		public string MonthTitle {
			get { return _monthTitle; } 
			set {
				this.RaiseAndSetIfChanged (ref _monthTitle, value);
			}
		}

		private bool _enableNextButton = true;

		public bool EnableNextButton {
			get { 
				return _enableNextButton;
			}
			set { 
				this.RaiseAndSetIfChanged (ref _enableNextButton, value);
			}
		}

		private bool _enablePreviousButton = true;

		public bool EnablePreviousButton {
			get { 
				return _enablePreviousButton;
			}
			set { 
				this.RaiseAndSetIfChanged (ref _enablePreviousButton, value);
			}
		}

		private bool _scrolling = false;

		public bool Scrolling {
			get { 
				return _scrolling;
			}
			set { 
				this.RaiseAndSetIfChanged (ref _scrolling, value);
			}
		}

		private readonly Dictionary<string, MonthlyTripModel> _caches = new Dictionary<string, MonthlyTripModel> ();

		private IReactiveCommand<object> _backCommand;

		public IReactiveCommand<object> BackCommand {
			get {
				return _backCommand;
			}
			set { 
				_backCommand = value;
			}
		}

		public override void UpdateHeaderFooter ()
		{
			HeaderViewModel.Title = Translator.Translate ("TRIP_LIST_HEADER");
			HeaderViewModel.IsShowBtnBack = true;
			HeaderViewModel.IsShowBtnInfo = false;
			HeaderViewModel.IsShowBtnShare = false;

			TabBarViewModel.IsHidden = true;
		}

		public ListTripViewModel () : base ()
		{
		}

		protected override void InitCommands ()
		{
			// Obsever Month changed
			this.WhenAnyValue (vm => vm.CurrentMonth)
				.Skip(1)
//				.ObserveOn (RxApp.MainThreadScheduler) // this line is very important to keep all value changed will be handled
				.Subscribe (month => {
				MonthTitle = month.ToString ("MMMM, yyyy");
				EnableNextButton = DateTime.Today.MonthDiff (month) > 0;
				EnablePreviousButton = true;
			});

			// Back command
			BackCommand = ReactiveCommand.Create ();
			BackCommand.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (r => {
				ClearCache ();
//				Navigator.NavigateBack ();
			});
			//
			this.WhenAnyValue (vm => vm.Scrolling)
				.Subscribe (b => {
				EnableNextButton = !b && DateTime.Today.MonthDiff (CurrentMonth) > 0;
				EnablePreviousButton = !b;
			});
		}

		private void ClearCache ()
		{
			_caches.Clear ();
		}

		public MonthlyTripModel GetMonthlyViewModel (DateTime month)
		{
			string key = month.ToString (KEY_MONTHLY_VIEWMODEL);
			if (!_caches.ContainsKey (key)) {
				_caches [key] = new MonthlyTripModel (month);
			}
			return _caches [key];
		}
	}
}

