﻿using System;
using DriveRightF.Core.Enums;

namespace DriveRightF.Core
{
	public class HelpViewModel : BaseHandlerHeaderViewModel
	{
		#region implemented abstract members of BaseHandlerHeaderViewModel

		public override int ScreenToNotify {
			get {
				return (int)Screen.Help;
			}
		}

		public override void UpdateHeaderFooter ()
		{
		}

		#endregion

		public HelpViewModel () : base()
		{
		}
	}
}

