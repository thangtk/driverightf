﻿using System;

using ReactiveUI;
using System.Reactive.Linq;
using Unicorn.Core;
using DriveRightF.Core.Enums;
using System.Reactive.Concurrency;
using DriveRightF.Core.Managers;
using DriveRightF.Core.ViewModels;
using DriveRightF.Core.Models;

namespace DriveRightF.Core
{
    public class SecondStepSignupViewModel : BaseViewModel
    {
        private string _plateNumber;

        public SignupViewModel Step1
        {
            get;
            set;
        }

        #region implemented abstract members of BaseViewModel

        public override int ScreenToNotify
        {
            get
            {
                return (int)Screen.Signup;
            }
        }

        #endregion

        public bool IsNoThank
        {
            get;
            set;
        }

        public string TextNoThanksButton
        {
            get
            {
                return Translator.Translate("BUTTON_NO_THANK_YOU");
            }
        }
        public string TextNextButton
        {
            get { return Translator.Translate("BUTTON_NEXT"); }
        }

        public string TextDescription
        {
            get { return Translator.Translate("SIGN_UP_DESCRIPTION_2_0"); }
        }

        public string TextDescription1
        {
            get { return Translator.Translate("SIGN_UP_DESCRIPTION_2_1"); }
        }

        public ProfileManager ProfileManager
        {
            get { return DependencyService.Get<ProfileManager>(); }
        }

        public IReactiveCommand<object> GetPlateNumberByOCRCommand
        {
            get;
            set;
        }

        public IReactiveCommand<object> SecondStepNextCommand
        {
            get;
            set;
        }

        public IReactiveCommand<object> SecondStepDeclineCommand
        {
            get;
            set;
        }

        public string PlateNumber
        {
            get
            {
                return _plateNumber;
            }
            set
            {
                this.RaiseAndSetIfChanged(ref _plateNumber, value);
            }
        }

        public SecondStepSignupViewModel()
        {
        }

        protected override void InitCommands()
        {
            base.InitCommands();

            SecondStepNextCommand = ReactiveCommand.Create();
            SecondStepNextCommand
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Subscribe(x =>
                    {
                        if (ValidatePlateNumber())
                        {

                            var ProfileManager = DependencyService.Get<ProfileManager>();
                            var profile = new Profile()
                            {
                                NumberPlate = PlateNumber,
                                PhoneNumber = Step1.PhoneNumber
                            };

                            Observable.Start(() =>
                            {
                                PopupManager.ShowLoading();
                            }, RxApp.MainThreadScheduler)
                                    .Delay(TimeSpan.FromMilliseconds(100))
                                    .ObserveOn(RxApp.TaskpoolScheduler)
                                    .Subscribe(x1 =>
                                    {
                                        //ProfileManager.GetProfileFromService(PlateNumber);

                                        RxApp.MainThreadScheduler.Schedule(() =>
                                        {
                                            PopupManager.HideLoading();
                                            Navigator.Navigate((int)Screen.Home);
                                        });
                                    });
                            IsNoThank = false;
                        }
                        else
                            PopupManager.ShowAlert(Translator.Translate("ALERT"), Translator.Translate("PAYMENT_METHOD_EMPTY_WRONG"), Unicorn.Core.Enums.DialogIcon.None, false, null, Translator.Translate("BUTTON_OK"));
                    });

            SecondStepDeclineCommand = ReactiveCommand.Create();
            SecondStepDeclineCommand
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Subscribe(x =>
                    {
                        IsNoThank = true;

                        PopupManager.ShowLoading();
                        RxApp.TaskpoolScheduler.Schedule(() =>
                        {
                            //ProfileManager.GetProfileFromService();

                            RxApp.MainThreadScheduler.Schedule(() =>
                            {
                                PopupManager.HideLoading();
                                Navigator.Navigate((int)Screen.Home);
                            });
                        });
                    });

            GetPlateNumberByOCRCommand = ReactiveCommand.Create();
            GetPlateNumberByOCRCommand
                .ObserveOn(RxApp.MainThreadScheduler)
                .Subscribe(x => PopupManager.ShowAlert(Translator.Translate("ALERT"), Translator.Translate("FUNCTION_COMMING_SOON"), Unicorn.Core.Enums.DialogIcon.None, false, null, Translator.Translate("BUTTON_OK")));

        }

        private bool ValidatePlateNumber()
        {
            //TODO: add validation in here
            return !string.IsNullOrEmpty(PlateNumber);
        }
    }
}

