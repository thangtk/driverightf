﻿using System;
using ReactiveUI;
using DriveRightF.Core.Managers;
using Unicorn.Core;
using DriveRightF.Core.Models;
using System.Reactive.Linq;
using Unicorn.Core.Navigator;
using DriveRightF.Core.Enums;
using System.Reactive.Concurrency;

namespace DriveRightF.Core.ViewModels
{
	public class PerkItemViewModel : BaseViewModel
	{
		private Perk _perk;
		private bool _isUsed;
		private bool _isNew = false;
		private string _name;

		public override int ScreenToNotify {
			get {
				return (int)Screen.Perks;
			}
		}

		public Perk Item {
			get
			{
				return _perk;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _perk, value);
//				_perk = value;
//				UpdateValue ();
			}
		}

		public string Name
		{
			get {
				return _name;
			}
			set {
				this.RaiseAndSetIfChanged (ref _name, value);
			}
		}

		public bool IsNew
		{
			get {
				return _isNew;
			}
			set {
				this.RaiseAndSetIfChanged (ref _isNew, value);
			}
		}

		public bool IsUsed
		{
			get {
				return _isUsed;
			}
			set {
				this.RaiseAndSetIfChanged (ref _isUsed, value);
			}
		}

		private PerkManager PerkManager
		{
			get {
				return DependencyService.Get<PerkManager> ();
			}
		}

		private IReactiveCommand<object> _itemClickCommand = ReactiveCommand.Create ();

		public IReactiveCommand<object> ItemClickCommand 
		{
			get {
				return _itemClickCommand;
			}
			set{ 
				_itemClickCommand = value;
			}

		}
		private IReactiveCommand<object> _removeItemStatusCommand;
		private IReactiveCommand<object> _savePerkCommand;
		public IReactiveCommand<object> RemoveItemStatusCommand {
			get {
				return _removeItemStatusCommand;
			}
			set{ 
				_removeItemStatusCommand = value;
			}
		}

		public IReactiveCommand<object> SavePerkCommand {
			get {
				return _savePerkCommand;
			}
			set{ 
				_savePerkCommand = value;
			}
		}

		private ImageMultiStatusViewModel _itemIconViewModel;
		public ImageMultiStatusViewModel ItemIconViewModel {
			get{ 
				return _itemIconViewModel;
			}
			set{ 
				this.RaiseAndSetIfChanged (ref _itemIconViewModel, value);
			}
		}


		public PerkItemViewModel () : base()
		{
			
		}

		#region implemented abstract members of BaseViewModel

		protected override void InitCommands ()
		{
			ItemClickCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (obj => 
					{
						MessagingCenter.Send(this,"Click");
						DependencyService.Get<INavigator> ().Navigate ((int)Screen.PerkDetail, Item.PerkId);
					});

			this.WhenAnyValue (vm => vm.Item)
				.Where(i => i != null)
				.Subscribe (item => {
					Name = item.Name;
					RxApp.TaskpoolScheduler.Schedule(()=>{

						var imageManager = DependencyService.Get<CachedImageManager>();
						var image = imageManager.GetImage (new CachedImage () { LocalPath = item.Icon, URL = item.IconURL }, item, "Icon");

						var icon = "";
						var iconUrl = "";
						if (image.Cached)
							icon = image.LocalPath;
						else {
							iconUrl = item.IconURL;
						}

						// Have to check if Icon is empty
//						ItemIconViewModel = new ImageMultiStatusViewModel((int)Screen.Perks){
//							Icon = image.LocalPath,
//							IconURl = image.URL,
//							Status = item.ItemStatus,
//							IsGrayStyle = Item.Used
//						};

						if(ItemIconViewModel != null)
						{
							ItemIconViewModel.Icon = icon;
							ItemIconViewModel.IconURl = iconUrl;
							ItemIconViewModel.Status = item.ItemStatus;
							ItemIconViewModel.IsGrayStyle = Item.Used;
						}
					});

				});
			//
			RemoveItemStatusCommand = ReactiveCommand.Create ();
			RemoveItemStatusCommand
				.Subscribe (_ => {
					ItemIconViewModel.Status = ItemStatus.None;
					Item.ItemStatus = ItemStatus.None;
					DependencyService.Get<PerkManager> ().Save(Item);
				});

			SavePerkCommand = ReactiveCommand.Create ();
			SavePerkCommand
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (perk => {
					var perkManager = new PerkManager();
					perkManager.Save((Perk)perk);
				});
		}

		#endregion

		private void UpdateValue()
		{
			if (_perk != null) {
				IsUsed = _perk.Used;
//				IsNew = _perk.IsNew;
//				_perk.IsNew = false;
			}
		}
	}
}

