﻿using System;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using System.Reactive.Linq;
using DriveRightF.Core.Managers;
using Unicorn.Core;
using DriveRightF.Core.Constants;
using DriveRightF.Core.Models;

namespace DriveRightF.Core
{
	public class MenuViewModel : BaseHandlerHeaderViewModel
	{
//		private string _userName;
//		private Profile _profile;
		private IReactiveCommand<object> _settingMenuItemCommand = ReactiveCommand.Create ();
		private IReactiveCommand<object> _aboutMenuItemCommand = ReactiveCommand.Create ();
		private IReactiveCommand<object> _helpMenuItemCommand = ReactiveCommand.Create ();
		private IReactiveCommand<object> _legalMenuItemCommand = ReactiveCommand.Create ();

		public override int ScreenToNotify {
			get {
				throw new NotImplementedException ();
			}
		}

		#region implemented abstract members of BaseHandlerHeaderViewModel

		public override void UpdateHeaderFooter ()
		{
			HeaderViewModel.IsShowBtnNotification = false;
			HeaderViewModel.Title = Translator.Translate("MORE_HEADER");
			HeaderViewModel.IsShowBtnBack = false;
			HeaderViewModel.IsShowBtnShare = false;
			HeaderViewModel.IsShowBtnInfo = false;

			TabBarViewModel.IsHidden = false;
		}

		#endregion

		public IReactiveCommand<object> LegalMenuItemCommand {
			get {
				return _legalMenuItemCommand;
			}
			private set {
				_legalMenuItemCommand = value;
			}
		}

		public IReactiveCommand<object> HelpMenuItemCommand {
			get {
				return _helpMenuItemCommand;
			}
			private	set {
				_helpMenuItemCommand = value;
			}
		}

		public IReactiveCommand<object> AboutMenuItemCommand {
			get {
				return _aboutMenuItemCommand;
			}
			private set {
				_aboutMenuItemCommand = value;
			}
		}

		public IReactiveCommand<object> SettingMenuItemCommand {
			get {
				return _settingMenuItemCommand;
			}
			private	set {
				_settingMenuItemCommand = value;
			}
		}

//		private IReactiveCommand<object> _loadDataCommand;
//
//		public IReactiveCommand<object> LoadDataCommand {
//			get { return _loadDataCommand; }
//			set { this.RaiseAndSetIfChanged (ref _loadDataCommand, value); }
//		}
//
//		public ProfileManager ProfileManager {
//			get{ 
//				return DependencyService.Get<ProfileManager> ();
//			}
//		}

		public MenuViewModel () : base()
		{
			
		}

		protected override void InitCommands ()
		{
			SettingMenuItemCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					if(x != null && x is Action)
					{
						(x as Action).Invoke();
					}
				});	

			AboutMenuItemCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					if(x != null && x is Action)
					{
						(x as Action).Invoke();
					}
				});

			HelpMenuItemCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					if(x != null && x is Action)
					{
						(x as Action).Invoke();
					}
				});

			LegalMenuItemCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					if(x != null && x is Action)
					{
						(x as Action).Invoke();
						//Test:
						// Clear data and navigate to start screen.

						 DependencyService.Get<ProfileManager> ().ClearAllData();

					}
				});

//			LoadDataCommand = ReactiveCommand.Create ();
//			LoadDataCommand.SubscribeOn (RxApp.TaskpoolScheduler)
//				.Select (x => ProfileManager.Profile)
//				.Where (x => x != null)
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (x => {
//					Profile = new Profile() {
//						ProfileID = x.ProfileID,
//						Name = x.Name,
//						Image = x.Image,
//						ImageUrl = x.ImageUrl,
//						PhoneNumber = x.PhoneNumber,
//						NumberPlate = x.NumberPlate,
//						PaymentAccount = x.PaymentAccount,
//						PaymentType = x.PaymentType,
//						PaymentName = x.PaymentName
//					};
//				});
		}
	}
}

