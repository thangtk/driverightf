﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReactiveUI;
using DriveRightF.Core.Models;
using DriveRightF.Core.Managers;
using Unicorn.Core;

namespace DriveRight.Core.ViewModels
{
    public partial class PerkDetailViewModel
    {
		private string _awardTitle;
		private string _description;
		private string _briefDesc;
		private bool _isUsed;
		private string _sponsoredName;
		private string _expiryDate;
		private string _awardIcon;
		private string _awardBackground;
		private string _iconUrl;
		private string _imageUrl;
		private string _number;
		private string _perkId;
		private Perk _perkCurrent;
		private bool _isIndefinite;
		private bool _isPerkUsed;

		private string _sponsoredByText;
		private string _expDateText;
		private string _checkBoxIsUseLabel;

		private IReactiveCommand<object> _loadPerkCommand = ReactiveCommand.Create ();
		private IReactiveCommand<object> _savePerkCommand = ReactiveCommand.Create ();
		private IReactiveCommand<object> _btnBackClicked = ReactiveCommand.Create();
		private IReactiveCommand<object> _checkChanged = ReactiveCommand.Create();

		public IReactiveCommand<object> LoadPerkCommand {
			get {
				return _loadPerkCommand;
			}
			set{ 
				_loadPerkCommand = value;
			}
		}
		public IReactiveCommand<object> SavePerkCommand {
			get {
				return _savePerkCommand;
			}
			set{ 
				_savePerkCommand = value;
			}
		}
		public IReactiveCommand<object> BtnBackClicked {
			get {
				return _btnBackClicked;
			}
			set{ 
				_btnBackClicked = value;
			}
		}
		public IReactiveCommand<object> CheckChanged {
			get {
				return _checkChanged;
			}
			set{ 
				_checkChanged = value;
			}
		}
		public string SponsoredByText
		{
			get { return _sponsoredByText; }
			set { 
				this.RaiseAndSetIfChanged (ref _sponsoredByText, value);
			}
		}

		public string ExpDateText
		{
			get { return _expDateText; }
			set { 
				this.RaiseAndSetIfChanged (ref _expDateText, value);
			}
		}

		public string CheckBoxIsUseLabel
		{
			get { return _checkBoxIsUseLabel; }
			set { 
				this.RaiseAndSetIfChanged (ref _checkBoxIsUseLabel, value);
			}
		}

		public bool IsPerkUsed {
			get { return _isPerkUsed;}
			set { 
				this.RaiseAndSetIfChanged (ref _isPerkUsed, value);
				PerkCurrent.Used = value;
			}
		}

		public bool IsIndefinite
		{
			get { return _isIndefinite; }
			set { 
				this.RaiseAndSetIfChanged (ref _isIndefinite, value);
			}
		}

		public Perk PerkCurrent
		{
			get { return _perkCurrent; }
			set
			{
				this.RaiseAndSetIfChanged(ref _perkCurrent, value);
				Used = _perkCurrent.Used;
			}
		}


		protected PerkManager PerkManager
		{
			get
			{
				return DependencyService.Get<PerkManager> ();
			}
		}

		public string PerkID {
			get { return _perkId;}
			set { this.RaiseAndSetIfChanged (ref _perkId, value);}
		}

        public string TxtAwardTitle
        {
			get { return _awardTitle ?? "Perk"; }
			set { this.RaiseAndSetIfChanged (ref _awardTitle, value);}
        }
        
        public string TxtDescription
        {
			get { return _description ?? ""; }
			set { this.RaiseAndSetIfChanged (ref _description, value);}
        }
        
		public string BriefDesc {
			get { return _briefDesc ?? "";}
			set { this.RaiseAndSetIfChanged (ref _briefDesc, value);}
		}

		public bool Used {
			get { return _isUsed;}
			set { this.RaiseAndSetIfChanged (ref _isUsed, value);}
		}

        public string TxtSponsoredName
        {
			get { return _sponsoredName ?? ""; }
			set { this.RaiseAndSetIfChanged (ref _sponsoredName, value);}
        }
			
        public string TxtAwardExpireDate
        {
			get { return _expiryDate == DateTime.MinValue.ToLocalTime().ToString ("yy-MM-dd") ? "" : _expiryDate; }
			set { this.RaiseAndSetIfChanged (ref _expiryDate, value);}}
        
        public string AwardIcon
        {
			get { return _awardIcon ?? "";}
			set { this.RaiseAndSetIfChanged (ref _awardIcon, value);}
        }

		public string IconUrl {
			get { return _iconUrl ?? "";} 
			set { this.RaiseAndSetIfChanged (ref _iconUrl, value);}
		}
			
        public string AwardBackgroundPicture
        {
			get { return _awardBackground ?? ""; }
			set { this.RaiseAndSetIfChanged (ref _awardBackground, value);}
        }

		public string ImageUrl {
			get { return _imageUrl ?? "";}
			set { this.RaiseAndSetIfChanged (ref _imageUrl, value);}
		}

        public string AwardQRNumber
        {
			get { return _number ?? ""; }
			set { this.RaiseAndSetIfChanged (ref _number, value);}
        }
    }
}
