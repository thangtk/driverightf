﻿using System;
using System.Reactive.Linq;
using DriveRightF.Core;
using DriveRightF.Core.Enums;
using DriveRightF.Core.Managers;
using DriveRightF.Core.Models;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using Unicorn.Core;
using Unicorn.Core.Navigator;


namespace DriveRight.Core.ViewModels
{
    public partial class PerkDetailViewModel : BaseViewModel
    {
		public override int ScreenToNotify {
			get {
				return (int)Screen.PerkDetail;
			}
		}


		public PerkDetailViewModel() : base()
		{
			
		}

		protected override void Initalize (object param)
		{
			base.Initalize (param);
			CheckBoxIsUseLabel = "Used perk?";
			SponsoredByText = "Sponsor By";
			ExpDateText = "Expired Date";

		}
		protected override void InitCommands()
        {
			LoadPerkCommand
				.ObserveOn(RxApp.TaskpoolScheduler)
				.Subscribe (obj => {
					LoadPerk((string)obj);
			    });

			SavePerkCommand
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (perk => {
				SavePerk ((Perk)perk);
			});

            BtnBackClicked
                .ObserveOn(RxApp.MainThreadScheduler)
                .Subscribe(obj =>
                {
					DependencyService.Get<INavigator>().NavigateBack(true);
//						DependencyService.Get<INavigator>().Navigate((int)Screen.Perks, null);
                });
        }

		private void SavePerk(Perk perk)
		{
			PerkManager.Save (perk);
		}

		private void LoadPerk(string perkId)
		{
			Perk perk = PerkManager.GetPerk(perkId);
			if (perk == null) {
				perk = new Perk ();
			}
		

			TxtAwardTitle = perk.Name;
			TxtDescription = perk.Description;
			TxtSponsoredName = perk.Sponsor;
			TxtAwardExpireDate = perk.Expiry.ToLocalTime().ToString ("yy-MM-dd");
			BriefDesc = perk.BriefDesc;

			var imageManager = DependencyService.Get<CachedImageManager>();
			var image = imageManager.GetImage (new CachedImage () { LocalPath = perk.Icon, URL = perk.IconURL }, perk, "Icon");

			if (image.Cached)
				AwardIcon = image.LocalPath;
			else {
				IconUrl = perk.IconURL;
			}

			image = imageManager.GetImage (new CachedImage () { LocalPath = perk.Image, URL = perk.ImageURL }, perk, "Image");

			if (image.Cached)
				AwardBackgroundPicture = image.LocalPath;
			else {
				ImageUrl = perk.IconURL;
			}

//			AwardIcon = perk.Icon;
			//for Used raise anyway
			AwardBackgroundPicture = perk.Image;
			Used = !Used;
			Used = perk.Used;
			AwardQRNumber = perk.QRCode;
			PerkID = perk.PerkId;

			PerkCurrent = ValidatePerk (perk);
		}

		private Perk ValidatePerk(Perk perk)
		{
			perk.Name = TxtAwardTitle;
			perk.Description = TxtDescription;
			perk.Icon = AwardIcon;
			perk.Image = AwardBackgroundPicture;
			perk.Sponsor = TxtSponsoredName;
			perk.QRCode = AwardQRNumber;
			perk.BriefDesc = BriefDesc;
			perk.PerkId = PerkID;
			perk.IconURL = IconUrl;
			perk.ImageURL = ImageUrl;

			if (perk.Expiry == DateTime.MinValue)
					IsIndefinite = true;
			return perk;
		}
    }
}
