﻿using System;
using System.Globalization;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text.RegularExpressions;
using DriveRightF.Core.Enums;
using DriveRightF.Core.Models;
using ReactiveUI;
using Unicorn.Core;
using Unicorn.Core.Enums;
using Unicorn.Core.Navigator;

namespace DriveRightF.Core.ViewModels
{
	public partial class SponsorRegistrationViewModel : BaseViewModel
	{
		public void InitData ()
		{
			//LoadData ();

			HeaderLogo = "logo_sponsor.png";

			HeaderTitle = "Insurance";
			PolicyNoTitle = "No";
			PolicyInsurerTitle = "Insurer";
			PolicyExpiryTitle = "Expiry";
			DriverNoTitle = "No";
			DriverInsurerTitle = "Insurer";
			CarTitle = "Car";
			CarNoTitle = "No";

			//IsEnableButton = false;
		}

		public SponsorRegistrationViewModel () : base ()
		{
			InitData ();
		}

		protected override void InitCommands ()
		{
			BtnBackCliked
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				if (CheckInsuranceChanged ()) {
					PopupManager.ShowConfirm ("Confirm", "Do you want to back. Informations will be lost!", () =>
								DependencyService.Get<INavigator> ().NavigateBack (true)
								, null);
				} else {
					DependencyService.Get<INavigator> ().NavigateBack (true);
				}
			});
			
			BtnAddCarClicked
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe ();
			
			BtnAddDriverCliked
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe ();

			BtnAddPolicyCliked
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe ();

//			BtnOKCliked
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (action => {
//					bool isSucces = ValidateFields ();
//					if(isSucces)
//					{
//						UpdateDataToServer();
//					}
//				});
			
//			LoadDataCommand = (IReactiveCommand<object>)ReactiveCommand
//				.CreateAsyncObservable (obj => 
//					Observable.Start (() => {
//						if(obj != null && !string.IsNullOrEmpty(obj.ToString()))
//						{
//							SponsorId = obj.ToString();
//							SponsorRegistration data = SponsorRegistrationManager.Get(_sponsorId);
//							return data;
//						}
//						SponsorId = string.Empty;
//						return null;
//						
//			}));
//
//			LoadDataCommand
////				.Where (x => x != null)
//				.Subscribe (data => {
//					
//			});

			GetContractCommand = ReactiveCommand.CreateAsyncObservable (
				obj => {
					if (obj == null) {
						return null;
					}
					//
					RxApp.MainThreadScheduler.Schedule (PopupManager.ShowLoading);
					SponsorId = obj as string;
					return Observable.Start (() => GetInputData (SponsorId));
				});
			GetContractCommand.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (data => {
				PopupManager.HideLoading ();
				if (data != null) {
					LoadData (data);
					// TODO: edit later
					IsEnableButton = false;
					Mode = ViewMode.Modify;
				}
			});

			GetContractCommand.ThrownExceptions.Subscribe (ex => {
				IsEnableButton = false;
				Mode = ViewMode.Modify;
			});
				

//			LoadDataCommand
//				.Where(x => x!=null)
//				.ObserveOn (RxApp.TaskpoolScheduler)
//				.Subscribe ((obj) => {
//					SponsorId = obj as string;
//					if(!string.IsNullOrEmpty(SponsorId))
//					{
//						Sponsor sponsor = SponsorManager.Get(SponsorId);
//						SponsorRegistration data = SponsorRegistrationManager.Get (SponsorId);
//						if(data == null)
//						{
//							if(sponsor.Type == SponsorType.Insurer)
//							{
//								ViewStatus = (SponsorStatus)sponsor.Status;
//								if(ViewStatus == SponsorStatus.Modify)
//								{
//									LoadDataFromServer();
//								}
////								else if(ViewStatus == SponsorStatus.View)
////								{
////									//TODO load from DB.
////									LoadDataFromLocal();
////								}
//							}
//						}
//						else
//						{
//							ViewStatus = (SponsorStatus)sponsor.Status;
//							LoadData(data);
//						}
//					}
//			});
//			
//			SaveDataCommand
//				.ObserveOn (RxApp.TaskpoolScheduler)
//				.Subscribe (x =>
//					{
//						bool isSucces = ValidateFields ();
//						if(isSucces)
//						{
//							UpdateDataToServer();
//						}
//					});

			UpdateContractCommand = ReactiveCommand.CreateAsyncObservable (Observable.Return (ValidateFields ())
				, obj => {
				PopupManager.ShowLoading ();
				var sponsorRestration = GetInputData ();
				return Observable.Start (() => SponsorRegistrationManager.UpdateSponsorRegistration (sponsorRestration));
			});
			
			UpdateContractCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (b => {
				if (b) {
					Sponsor sponsor = SponsorManager.Get (SponsorId);
					sponsor.Status = SponsorStatus.Registered;
					SponsorManager.Save (sponsor);
					
					var sponsorRegistration = GetInputData ();
					SponsorRegistrationManager.Save (sponsorRegistration, SponsorId);

					PopupManager.HideLoading ();
					PopupManager.ShowAlert ("Alert", "Update success", DialogIcon.Info);
					IsEnableButton = false;
				} else {
					PopupManager.HideLoading ();
					PopupManager.ShowAlert ("Alert", "Update failed", DialogIcon.Warning);
				}
				Mode = ViewMode.Modify;
			});

			CheckInsuranceChange
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (obj => IsEnableButton = CheckInsuranceChanged ());
		}

		private SponsorRegistration GetInputData (string sponsorId)
		{
			if (string.IsNullOrEmpty (SponsorId)) {
				return null;	
			}
			//
			Sponsor sponsor = SponsorManager.Get (SponsorId);
			SponsorRegistration data = SponsorRegistrationManager.Get (SponsorId);
			if (data == null) {
				if (sponsor.Type == SponsorType.Insurer) {
					if (sponsor.Status == SponsorStatus.Registered) {
						data = SponsorRegistrationManager.GetSponsorRegistration (sponsorId);
					}
				}
			}
			Mode = sponsor.Status == SponsorStatus.Registered ? ViewMode.Modify : ViewMode.Modify;
			return data;
		}


		public void ResetData ()
		{
			Mode = ViewMode.Modify;

			Car = string.Empty;
			CarNo = string.Empty;

			DriverInsurer = string.Empty;
			DriverNo = string.Empty;

			PolicyInsurer = string.Empty;
			PolicyExpiry = string.Empty;
		}

		private void LoadDataFromLocal ()
		{
			SponsorRegistration data = SponsorRegistrationManager.Get (SponsorId);
			RxApp.MainThreadScheduler.Schedule (() => LoadData (data));
//			RxApp.MainThreadScheduler.Schedule(()=>
		}

		//		private async void LoadDataFromServer()
		//		{
		//			RxApp.MainThreadScheduler.Schedule (PopupManager.ShowLoading);
		//			GetSponsorRegistrationResponse response = await SponsorRegistrationManager.GetAsync (SponsorId);
		//			RxApp.MainThreadScheduler.Schedule (() => {
		//				PopupManager.HideLoading();
		//				if(response.IsSuccess)
		//				{
		//					if(response.SponsorRegistration != null)
		//					{
		//						//TODO: save to DB:
		//						SponsorRegistrationManager.Save(response.SponsorRegistration);
		//						LoadData(response.SponsorRegistration);
		//					}
		//
		//					IsEnableButton = false;
		//					ViewStatus = SponsorStatus.Modify;
		//
		//				}
		//				else
		//				{
		//					IsEnableButton = false;
		//					ViewStatus = SponsorStatus.View;
		//					PopupManager.ShowAlert("Alert", response.ErrorMessage, DialogType.Error);
		//				}
		////				LoadData (data);
		//			});
		//		}

		private void LoadData (SponsorRegistration data)
		{
			if (data == null)
				return;
			var car = data.CarRegistration;
			var driver = data.DriverLicense;
			var insurance = data.Insurance;
			if (car != null) {
				Car = car.CarModel;
				CarNo = car.CarNo;

			}

			if (driver != null) {
				DriverInsurer = driver.Insurer;
				DriverNo = driver.LicenseNo;
			}

			if (insurance != null) {
				PolicyInsurer = insurance.Insurer;
				PolicyNo = insurance.PolicyNo;
				if (DateTime.MinValue.CompareTo (insurance.Expiry) < 0) {
					PolicyExpiry = insurance.Expiry.ToString ("yy-MM-dd");
				}
			}
		}

		//		private async void UpdateDataToServer()
		//		{
		//
		//			RxApp.MainThreadScheduler.Schedule (() => PopupManager.ShowLoading ());
		//			SponsorRegistration dataInsurance = GetInputData ();
		//			UpdateSponsorRegistrationResponse response = await SponsorRegistrationManager.UpdateSponsorRegistration (dataInsurance);
		//
		//			RxApp.MainThreadScheduler.Schedule (() => {
		//				PopupManager.HideLoading ();
		//				if (response.IsSuccess) {
		//					IsEnableButton = false;
		//					SponsorRegistrationManager.Save (dataInsurance);
		//
		//					ViewStatus = SponsorStatus.View;
		//
		//					//TODO: save status of sponsor
		//					Sponsor sponsor = SponsorManager.Get(SponsorId);
		//					sponsor.Status = ViewStatus;
		//					SponsorManager.Save(sponsor);
		//
		//
		//					PopupManager.ShowAlert ("Alert", "Update success",DialogType.Info);
		//				} else {
		//					PopupManager.ShowAlert ("Alert", response.ErrorMessage,DialogType.Error);
		//				}
		//			});
		//		}

		public SponsorRegistration GetInputData ()
		{
			SponsorRegistration data = new SponsorRegistration ();
			CarRegistration car = new CarRegistration () {
				SponsorId = SponsorId,
				CarNo = CarNo,
				CarModel = Car
			};

			DriverLicense driver = new DriverLicense () {
				SponsorId = SponsorId,
				LicenseNo = DriverNo,
				Insurer = DriverInsurer,
			};

			Insurance insurance = new Insurance () {
				SponsorId = SponsorId,
				Insurer = PolicyInsurer,
				PolicyNo = PolicyNo,
				Expiry = GetInsuranceExpiry (PolicyExpiry)
			};

			data.CarRegistration = car;
			data.DriverLicense = driver;
			data.Insurance = insurance;

			return data;

		}

		private bool CheckInsuranceChanged ()
		{
			if (Mode == ViewMode.View) {
				return false;
			}

			SponsorRegistration local = SponsorRegistrationManager.Get (_sponsorId);
			if (local == null) {
				return false;
			}

			var car = local.CarRegistration;
			var driver = local.DriverLicense;
			var insurance = local.Insurance;
			bool checkExpiry = !insurance.Expiry.CompareDateTo (GetInsuranceExpiry (PolicyExpiry))
			                   && !DateTime.MinValue.CompareDateTo (insurance.Expiry);

			bool isChanged = car.CarModel != Car
			                 || car.CarNo != CarNo
			                 || driver.Insurer != DriverInsurer
			                 || driver.LicenseNo != DriverNo
			                 || insurance.Insurer != PolicyInsurer
			                 || insurance.PolicyNo != PolicyNo
			                 || checkExpiry;

			return isChanged;

		}

		private DateTime GetInsuranceExpiry (string expiry)
		{
			if (string.IsNullOrEmpty (expiry)) {
				return DateTime.MinValue;
			} else {
				DateTime result;
				bool isSuccess = DateTime.TryParseExact (expiry, "yy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out result);
				return result;
			}
		}

		private bool ValidateFields ()
		{
//			IsBusy = true;
			bool isAllPassed = true;
			Match match;

			if (!string.IsNullOrEmpty (CarNo)) {
				match = Regex.Match (CarNo, @"^([0-9]{12})$");
				if (!match.Success) {
					CarNoError = "Must be 12 numbers";
					isAllPassed = false;
				} else {
					CarNoError = string.Empty;
				}
			} else {
				CarNoError = string.Empty;
			}

			if (!string.IsNullOrEmpty (PolicyNo)) {
				match = Regex.Match (PolicyNo, @"^([0-9]{12})$");
				if (!match.Success) {
					PolicyNoError = "Must be 12 numbers";
					isAllPassed = false;
				} else {
					PolicyNoError = string.Empty;
				}
			} else {
				PolicyNoError = string.Empty;
			}

			if (!string.IsNullOrEmpty (PolicyExpiry)) {
				match = Regex.Match (PolicyExpiry, @"^([0-9]{2})-([0-9]{2})-([0-9]{2})$");
				if (!match.Success) {
					PolicyExpiryError = "Must be yy-MM-dd format";
					isAllPassed = false;
				} else {
					PolicyExpiryError = string.Empty;
				}
			} else {
				PolicyExpiryError = string.Empty;
			}

			if (!string.IsNullOrEmpty (DriverNo)) {
				match = Regex.Match (DriverNo, @"^([0-9]{12})$");
				if (!match.Success) {
					DriverNoError = "Must be 12 numbers";
					isAllPassed = false;
				} else {
					DriverNoError = string.Empty;
				}
			} else {
				DriverNoError = string.Empty;
			}
				
			if (isAllPassed == false)
				IsAllPass = !IsAllPass;
			
			return isAllPassed;
//			IsBusy = false;
		}
	}
}
