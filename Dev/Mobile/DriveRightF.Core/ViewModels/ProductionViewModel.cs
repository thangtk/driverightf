﻿using System;
using DriveRightF.Core.Enums;

namespace DriveRightF.Core
{
	public class ProductionViewModel : BaseHandlerHeaderViewModel
	{
		#region implemented abstract members of BaseHandlerHeaderViewModel

		public override int ScreenToNotify {
			get {
				return (int)Screen.ProductIntroduction;
			}
		}

		public override void UpdateHeaderFooter ()
		{
//			HeaderViewModel.Title = Translator.Translate ("HELP_HEADER");
//			HeaderViewModel.IsShowBtnBack = true;
		}

		#endregion

		public ProductionViewModel () : base()
		{
		}
	}
}

