﻿using System;
using DriveRightF.Core.Models;
using Unicorn.Core;
using ReactiveUI;
using DriveRightF.Core.Enums;

namespace DriveRightF.Core
{
	public class EarningViewModel : BaseAutoRequestViewModel<Earning>
	{
		public EarningViewModel ()
		{
		}

		#region implemented abstract members of BaseAutoRequestViewModel

		public override int ScreenToNotify {
			get {
				return (int)Screen.Earnings;
			}
		}

		public override BaseAutoRequestManager<Earning> Manager {
			get {
				return DependencyService.Get<BaseAutoRequestManager<Earning>> ();
			}
		}
		protected override void Initalize (object param)
		{
			base.Initalize (param);
			HeaderTitle = "Earnings";
			HeaderLogo = "logo_earning.png";
		}
		#endregion
	}
}

