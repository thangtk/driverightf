﻿using System;
using DriveRightF.Core.Managers;
using Unicorn.Core;
using DriveRightF.Core.ViewModels;

namespace DriveRightF.Core
{
	public class AwardTabViewModel : HomeListContentViewModel<Award>
	{
		public AwardManager AwardManager {
			get{
				return DependencyService.Get<AwardManager> ();
			}
		}
		public AwardTabViewModel () : base()
		{
//			LoadData ();
			NoDataContent = Translator.Translate("AWARD_NO_DATA");
		}

		public void LoadData()
		{
			var awards = AwardManager.GetAwardsList (isUsed:false);
			awards.Sort (new SortAwards ());
			awards.Reverse ();

			DataSources = awards;
		}
	}
}

