﻿using System;
using Unicorn.Core;
using DriveRightF.Core.Constants;
using DriveRightF.Core.Managers;
using DriveRightF.Core.ViewModels;

namespace DriveRightF.Core
{
	public class AwardHomeContentViewModel : HomeListContentViewModel<Award>
	{
		public AwardManager AwardManager {
			get{
				return DependencyService.Get<AwardManager> ();
			}
		}
		public AwardTabViewModel AwardTabViewModel {
			get;
			set;
		}

		public AwardTabUsedViewModel AwardTabUsedViewModel {
			get;
			set;
		}

		public AwardHomeContentViewModel (BannerViewModel bannerViewModel) : base()
		{
			BannerViewModel = bannerViewModel;
			AwardTabUsedViewModel = new AwardTabUsedViewModel ();
			AwardTabViewModel = new AwardTabViewModel ();
			InitMessage ();
			LoadData ();
		}

		private void LoadData()
		{
			//TODO: sort awards.
			var awards = AwardManager.GetAwardsList ();
			awards.Sort (new SortAwards ());
			awards.Reverse ();

			BannerViewModel.BannerValue = awards.Count.ToString();
			AwardTabUsedViewModel.LoadData ();
			AwardTabViewModel.LoadData ();
		}

		private void InitMessage()
		{
			MessagingCenter.Subscribe<Award> (this, MessageConstant.CARD_GAMBLE, x => {
				try{
					AwardManager.Delete(x);
				}catch(Exception e){
					(new ErrorHandler()).Handle(e);
				}
				LoadData();
			});


			MessagingCenter.Subscribe<Award> (this, MessageConstant.CARD_TAKE_MONEY, x => {
				try{
					AwardManager.Delete(x);
				}catch(Exception e){
					(new ErrorHandler()).Handle(e);
				}
				LoadData();
			});

			MessagingCenter.Subscribe<Award> (this, MessageConstant.AWARD_IS_USED, aa => {

				LoadData();
			});
		}
	}
}

