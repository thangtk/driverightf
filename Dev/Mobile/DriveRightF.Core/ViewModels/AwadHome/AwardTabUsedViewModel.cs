﻿using System;
using DriveRightF.Core.Managers;
using Unicorn.Core;
using DriveRightF.Core.ViewModels;

namespace DriveRightF.Core
{
	public class AwardTabUsedViewModel : HomeListContentViewModel<Award>
	{
		public AwardManager AwardManager {
			get{
				return DependencyService.Get<AwardManager> ();
			}
		}
		public AwardTabUsedViewModel () : base()
		{
//			LoadData ();
		}

		public void LoadData()
		{
			var awards = AwardManager.GetAwardsList (isUsed:true);
			awards.Sort (new SortAwards ());
			awards.Reverse ();

			DataSources = awards;
		}
	}
}

