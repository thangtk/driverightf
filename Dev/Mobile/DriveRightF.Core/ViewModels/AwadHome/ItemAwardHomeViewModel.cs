﻿using System;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using DriveRightF.Core.ViewModels;

namespace DriveRightF.Core
{
	public class ItemAwardHomeViewModel : BaseViewModel
	{
		private Award _award;
		private string _expdays;
		private string _nameAward;
		private string _iconAward;

		public Award Award
		{
			get {
				return _award;
			}
			set {
				this.RaiseAndSetIfChanged (ref _award, value);
			}
		}

		public string ExpDays
		{
			get {
				return _expdays;
			}
			set {
				this.RaiseAndSetIfChanged (ref _expdays, value);
			}
		}


		public string IconAward
		{
			get {
				return _iconAward;
			}
			set {
				this.RaiseAndSetIfChanged (ref _iconAward, value);
			}
		}

		public string NameAward
		{
			get {
				return _nameAward;
			}
			set {
				this.RaiseAndSetIfChanged (ref _nameAward, value);
			}
		}

		public ItemAwardHomeViewModel () : base ()
		{
			this.WhenAnyValue (x => x.Award)
				.Where (x => x != null)
				.Subscribe (x => {
					IconAward = x.IconURL;
					NameAward = x.Name;
					int day = x.Expiry.Date.Subtract(DateTime.Now.Date).Days;
					ExpDays = string.Format ("{0}: {1} {2}", Translator.Translate ("AWARD_HOME_EXP"), day, day > 1 ? Translator.Translate ("ACCOUNT_DAYS") : Translator.Translate ("ACCOUNT_DAY"));

			});
		}

		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get {
				throw new NotImplementedException ();
			}
		}

		#endregion
	}
}

