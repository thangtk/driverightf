﻿using System;
using Unicorn.Core;
using DriveRightF.Core.Constants;
using DriveRightF.Core.Managers;
using DriveRightF.Core.ViewModels;

namespace DriveRightF.Core
{
	public class AwardHomeViewModel : BaseHomeListViewModel<Award>
	{

		public AwardHomeContentViewModel AwardContentViewModel {
			get;
			set;
		}
		public AwardHomeViewModel () : base()
		{
			BannerViewModel = new BannerViewModel ()
			{
				BannerTile = Translator.Translate("TAB_AWARDS"),
				BannerIcon = "award_ico.png",
				BannerBackground = "account_banner.png",
			};

			AwardContentViewModel = new AwardHomeContentViewModel (BannerViewModel);
		}

		#region implemented abstract members of BaseHandlerHeaderViewModel
		public override void UpdateHeaderFooter ()
		{
			HeaderViewModel.IsShowBtnBack = true;
			HeaderViewModel.Title = Translator.Translate("TAB_AWARDS");

			TabBarViewModel.IsHidden = true;
		}
		#endregion
	}
}

