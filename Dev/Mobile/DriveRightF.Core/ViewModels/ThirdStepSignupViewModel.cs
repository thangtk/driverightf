﻿using System;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using DriveRightF.Core.Enums;
using System.Reactive.Linq;
using Unicorn.Core;
using DriveRightF.Core.Managers;
using DriveRightF.Core.Models;
using System.Collections.Generic;
using Unicorn.Core.UI;

namespace DriveRightF.Core
{
	public class ThirdStepSignupViewModel : BaseViewModel
	{
		private object _infoContentView;
		private string _phoneNumber;

		public SignupViewModel Step1 { get; set;}

		public SecondStepSignupViewModel Step2 { get; set;}


		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get {
				return (int)Screen.Signup;
			}
		}

		#endregion

//		private string _name;
//
//		public string Name {
//			get { return _name; } 
//			set {
//				this.RaiseAndSetIfChanged (ref _name, value);
//			}
//		}
//
//		private string _password;
//
//		public string Password {
//			get { return _password; } 
//			set {
//				this.RaiseAndSetIfChanged (ref _password, value);
//			}
//		}
//
//		private string _accountType;
//
//		public string AccountType {
//			get { return _accountType; } 
//			set {
//				this.RaiseAndSetIfChanged (ref _accountType, value);
//			}
//		}
//
//		private string _accountNumber;
//
//		public string AccountNumber {
//			get { return _accountNumber; } 
//			set {
//				this.RaiseAndSetIfChanged (ref _accountNumber, value);
//			}
//		}

//		private string _lbaccountNumber;
//		public string LbAccountNumber {
//			get { return _lbaccountNumber; } 
//			set {
//				this.RaiseAndSetIfChanged (ref _lbaccountNumber, value);
//			}
//		}

		public IReactiveCommand<object> ThirdStepNextCommand {
			get;
			set;
		}

		public IReactiveCommand<object> ThirdStepDeclineCommand {
			get;
			set;
		}

		public IReactiveCommand<object> GetInfoFormCommand {
			get;
			set;
		}

		public IReactiveCommand<object> BindInfoContentViewCommand {
			get;
			set;
		}


//		public object InfoContentView {
//			get { return _infoContentView; }
//			set { this.RaiseAndSetIfChanged (ref _infoContentView, value); }
//		}

		public ThirdStepSignupViewModel ()
		{
		}

		protected override void InitCommands ()
		{
			base.InitCommands ();

			GetInfoFormCommand = ReactiveCommand.Create ();
			GetInfoFormCommand
				.Where(x => x != null)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe (x => PopupManager.ShowPopup (Translator.Translate ("SIGN_UP_PAYMENT_METHOD"), x, new List<DialogButton> () {
					new DialogButton () {
						Text = Translator.Translate ("BUTTON_CANCEL"),
						OnClicked = () => {
						}
					},
					new DialogButton () {
						Text = Translator.Translate ("BUTTON_VERIFY"),
						OnClicked = UpdatePaymentData,
						IsBold = true
					}
				}));
			

			ThirdStepNextCommand = ReactiveCommand.Create ();
			ThirdStepNextCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					if(string.IsNullOrEmpty( PaymentAccount)){
						PopupManager.ShowAlert (Translator.Translate("ALERT"), Translator.Translate("PAYMENT_METHOD_EMPTY_WRONG"),Unicorn.Core.Enums.DialogIcon.None, false, null, Translator.Translate("BUTTON_OK"));
						return;
					}

					//
					// TODO: save phone number
//					var manager = new ProfileManager();
//					var profile = manager.GetProfile();
//					profile.PaymentAccount = PaymentAccount;
//					profile.PaymentType = PaymentType;
//					profile.PaymentName = PaymentName;
//					manager.Save(profile);
					//
					Profile proff = new Profile(){
						PhoneNumber = Step1.PhoneNumber,
						NumberPlate = Step2.IsNoThank ?  "" : Step2.PlateNumber,
						PaymentName = PaymentName,
						PaymentAccount = PaymentAccount,
						PaymentType = PaymentType,
					};
					DependencyService.Get<ProfileManager>().Save(proff);
					Observable.Start(()=>{
						PopupManager.ShowLoading();
					},RxApp.MainThreadScheduler)
						.Delay(TimeSpan.FromMilliseconds(100))
						.ObserveOn(RxApp.MainThreadScheduler)
						.Subscribe(x1 =>{
						Navigator.Navigate ((int)Screen.Home);
						PopupManager.HideLoading();
					});
				});

			ThirdStepDeclineCommand = ReactiveCommand.Create ();
			ThirdStepDeclineCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					Profile proff = new Profile(){
						PhoneNumber = Step1.PhoneNumber,
						NumberPlate = Step2.IsNoThank ?  "" : Step2.PlateNumber,
					};
					DependencyService.Get<ProfileManager>().Save(proff);
					Observable.Start(()=>{
						PopupManager.ShowLoading();
					},RxApp.MainThreadScheduler).Delay(TimeSpan.FromMilliseconds(100))
						.ObserveOn(RxApp.MainThreadScheduler)
						.Subscribe(x2 =>{
						Navigator.Navigate ((int)Screen.Home);
						PopupManager.HideLoading();
					});


				});


//			BindInfoContentViewCommand = ReactiveCommand.Create ();
//			BindInfoContentViewCommand
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (view => 
//					InfoContentView = view);
		}
			
		private bool ValidateInfoField ()
		{
			//TODO: add validation in here
			return true;
		}

		void UpdatePaymentData ()
		{
			PaymentAccount = PaymentViewModel.PaymentAccount;
			PaymentName = PaymentViewModel.PaymentName;
			PaymentType = PaymentViewModel.PaymentType;
		}

		private string _paymentAccount;

		public string PaymentAccount {
			get { return _paymentAccount; } 
			set {
				this.RaiseAndSetIfChanged (ref _paymentAccount, value);
			}
		}

		private string _paymentName;

		public string PaymentName {
			get { return _paymentName; }
			set { this.RaiseAndSetIfChanged (ref _paymentName, value); }
		}

		private string _paymentType;

		public string PaymentType {
			get { return _paymentType; }
			set { this.RaiseAndSetIfChanged (ref _paymentType, value); }
		}

		private PaymentPopupViewModel _paymentViewModel;

		public PaymentPopupViewModel PaymentViewModel {
			get { 
				if (_paymentViewModel == null) {
					_paymentViewModel = new PaymentPopupViewModel (){ 
						PaymentName = this.PaymentName,
						PaymentType = this.PaymentType ?? Translator.Translate("ALIPAY"),
						PaymentAccount = this.PaymentAccount
					};
				}
				return _paymentViewModel;
			}
			set { this.RaiseAndSetIfChanged (ref _paymentViewModel, value); }
		}
	}
}

