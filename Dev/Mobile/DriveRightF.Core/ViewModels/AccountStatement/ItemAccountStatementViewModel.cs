﻿using System;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using DriveRightF.Core.ViewModels;
using DriveRightF.Core.Enums;
using DriveRightF.Core.Models;


namespace DriveRightF.Core
{
	public class ItemAccountStatementViewModel: BaseViewModel
	{
		private AccountTransaction _item;
		private string _transactionType;
		private string _startTime;
		private string _transactionValue;

		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify
		{
			get
			{
				return (int)Screen.Notification;
			}
		}

		public AccountTransaction Item
		{
			get
			{
				return _item;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref _item, value);
			}
		}

		public string Type
		{
			get
			{
				return _transactionType;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref _transactionType, value);
			}
		}

		public string StartTime
		{
			get
			{
				return _startTime;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref _startTime, value);
			}
		}

		public string TransactionValue
		{
			get
			{
				return _transactionValue;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref _transactionValue, value);
			}
		}

		#endregion

		public ItemAccountStatementViewModel() : base()
		{
			this.WhenAnyValue(x => x.Item)
				.Where(x => x != null)
				.Subscribe(x =>
			{
					
				//Type = GetTypeTransaction(Item.TransactionType);
				Type = Item.Description;
				//StartTime = Item.StartTime.ToString("MM/dd/yyyy");
				StartTime = Item.Time.ToString("MM/dd/yyyy");
				TransactionValue = Item.Value > 0 ? string.Format("+{0} {1}", Item.Value, Item.Unit) : string.Format("{0} {1}", Item.Value, Item.Unit);

			});
		}

		public static string GetTypeTransaction(TransactionType type)
		{
			switch (type)
			{
				case TransactionType.Awards:
					return "AWARDS";
				case TransactionType.Gamble:
					return "GAMBLE";
				case TransactionType.TakeCash:
					return "TAKE CASH";
				case TransactionType.WidthDraw:
					return "WIDTHDRAW";
			}
			return string.Empty;
		}
	}
}

