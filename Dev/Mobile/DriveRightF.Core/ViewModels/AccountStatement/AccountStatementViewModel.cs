﻿using System;
using DriveRightF.Core.ViewModels;
using System.Collections.Generic;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using DriveRightF.Core.Enums;
using DriveRightF.Core.Managers;
using Unicorn.Core;
using DriveRightF.Core.Models;
using System.Globalization;

namespace DriveRightF.Core
{
	public class AccountStatementViewModel: BaseHandlerHeaderViewModel
	{
		private List<AccountTransaction> _items;
		private string _totalStartCash, _totalStartCard, _totalEndCash, _totalEndCard;
		private string _startBalance, _endBalance, _nameCard, _nameCash;
		private string _transactionHistory;
		private DateTime _date;

		private IReactiveCommand<object> _loadDataCommand;

		public IReactiveCommand<object> LoadDataCommand {
			get {
				return _loadDataCommand;
			}
			set{ 
				_loadDataCommand = value;
			}
		}

		#region implemented abstract members of BaseViewModel

		#region implemented abstract members of BaseHandlerHeaderViewModel

		public override void UpdateHeaderFooter ()
		{
			HeaderViewModel.Title = Translator.Translate("ACCOUNT_STATEMENT_HEADER");
			HeaderViewModel.IsShowBtnBack = true;

			TabBarViewModel.IsHidden = true;
		}

		#endregion

		public override int ScreenToNotify {
			get {
				return (int)Screen.Notification;
			}
		}

		public List<AccountTransaction> Items {
			get
			{
				return _items;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _items, value);
			}
		}

		public string NameCash {
			get
			{
				return _nameCash;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _nameCash, value);
			}
		}

		public DateTime Date {
			get {
				return _date;
			}
			set {
				this.RaiseAndSetIfChanged (ref _date, value);
			}
		}

		public string NameCard {
			get
			{
				return _nameCard;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _nameCard, value);
			}
		}

		public string StartBalance {
			get
			{
				return _startBalance;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _startBalance, value);
			}
		}

		public string EndBalance {
			get
			{
				return _endBalance;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _endBalance, value);
			}
		}

		public string TotalEndCash {
			get
			{
				return _totalEndCash;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _totalEndCash, value);
			}
		}

		public string TotalStartCash {
			get
			{
				return _totalStartCash;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _totalStartCash, value);
			}
		}


		public string TotalEndCard {
			get
			{
				return _totalEndCard;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _totalEndCard, value);
			}
		}

		public string TotalStartCard
		{
			get
			{
				return _totalStartCard;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _totalStartCard, value);
			}
		}

		public string TransactionHistory
		{
			get
			{
				return _transactionHistory;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _transactionHistory, value);
			}
		}

		private AccountManager AccountManager {
			get { 
				return DependencyService.Get<AccountManager> ();
			}
		}

		#endregion

		protected override void InitCommands ()
		{		
			LoadDataCommand = ReactiveCommand.Create ();

			LoadDataCommand
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe (obj => {
					LoadData((string)obj);
					LoadTransactionHistory((string)obj);
				});
		}

		public AccountStatementViewModel ():base()
		{			
			//LoadData ();
		}

		/// <summary>
		/// Loads the data.
		/// </summary>
		/// <param name="month">Month (format: yyyy-MM)</param>
		private void LoadData(string month){

			NameCash = Translator.Translate("ACCOUNT_STATEMENT_CASH");
			NameCard = Translator.Translate("ACCOUNT_STATEMENT_CARD");
			StartBalance = Translator.Translate("ACCOUNT_STATEMENT_START_BALANCE");	
			EndBalance =  Translator.Translate("ACCOUNT_STATEMENT_END_BALANCE");
			TransactionHistory = Translator.Translate ("ACCOUNT_STATEMENT_TRANSACTION_HISTORY");
			SetDateTitle (month);

			//TODO: retrive actual account ID
			Account account = AccountManager.GetMonthlyAccountStatement ("1", month);
			if (account != null && account.Id > 0) {
				TotalEndCard = account.EndCard.ToString();
				TotalEndCash = account.EndBalance.ToString();
				TotalStartCard = account.StartCard.ToString();
				TotalStartCash = account.StartBalance.ToString();
			}
		}

		/// <summary>
		/// Sets the date title.
		/// </summary>
		/// <param name="month">Month (format yyyy-MM)</param>
		private void SetDateTitle(string month){
			DateTime pDate = DateTime.Now;
			if (DateTime.TryParseExact (month, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out pDate)) {
				Date = pDate;
			}
		}

		private void LoadTransactionHistory(string month)
		{
			List<AccountTransaction> transactions = AccountManager.GetAccountTransactions ("1", month);
			if( transactions != null)
				Items = transactions;
		}

		/// <summary>
		/// Dummy data
		/// </summary>
		private void LoadData()
		{
			NameCash = Translator.Translate("ACCOUNT_STATEMENT_CASH");
			NameCard = Translator.Translate("ACCOUNT_STATEMENT_CARD");
			StartBalance = Translator.Translate("ACCOUNT_STATEMENT_START_BALANCE");	
			EndBalance =  Translator.Translate("ACCOUNT_STATEMENT_END_BALANCE");
			TransactionHistory = Translator.Translate ("ACCOUNT_STATEMENT_TRANSACTION_HISTORY");

			TotalEndCard = "0";
			TotalEndCash = "120";
			TotalStartCard = "10";
			TotalStartCash = "100";
		}
	}
}


