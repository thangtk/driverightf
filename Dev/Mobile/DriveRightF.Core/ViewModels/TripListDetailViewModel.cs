﻿using System;
using DriveRightF.Core.Models;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using System.Reactive.Linq;
using System.Collections.Generic;
using DriveRightF.Core.Managers;
using DriveRightF.Core.Enums;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using DriveRightF.Core;

namespace DriveRight.Core.ViewModels
{
	public partial class TripListDetailViewModel: BaseHandlerHeaderViewModel
	{
		private Trip _trip;

		private TripManager TripManager {
			get {
				return DependencyService.Get<TripManager> ();
			}
		}

		private TripDetail _tripDetail;
		private TripDetailManager _manager;
		private IReactiveCommand<Trip> _loadTripCommand;
		private IReactiveCommand<object> _btnBackClick;
		//		private string _tripId;
		//		public string TripId {
		//			get
		//			{
		//				return _tripId;
		//			}
		//			set
		//			{
		//				this.RaiseAndSetIfChanged (ref _tripId, value);
		//				//InitData ();
		//			}
		//		}
		public Trip Trip {
			get {
				return _trip;
			}
			set {
				this.RaiseAndSetIfChanged (ref _trip, value);
				//InitData ();
			}
		}

		public override int ScreenToNotify {
			get {
				return (int)Screen.TripDetail;
			}
		}

		public IReactiveCommand<Trip> LoadTripCommand {
			get {
				return _loadTripCommand;
			}
			set { 
				_loadTripCommand = value;
			}
		}

		public IReactiveCommand<object> BtnBackClick {
			get {
				return _btnBackClick;
			}
			set { 
				_btnBackClick = value;
			}
		}

		public TripListDetailViewModel () : base ()
		{

		}

		public override void UpdateHeaderFooter ()
		{
			HeaderViewModel.Title = Translator.Translate ("TRIP_DETAILS_TITLE");
			HeaderViewModel.IsShowBtnBack = true;

			TabBarViewModel.IsHidden = true;

		}

		//		public void ResetValue()
		//		{
		//			Score = "--";
		//		}

		protected override void InitCommands ()
		{
			this.WhenAnyValue (vm => vm.TripId).Where (x => !string.IsNullOrEmpty(x)).Subscribe (x => LoadTripCommand.Execute (x));
			LoadTripCommand = ReactiveCommand.CreateAsyncObservable (obj => Observable.Start (() => {
				return TripManager.GetTripDetail (obj.ToString ());
			}));
//			LoadTripCommand
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (action => {
//					if (action != null)
//						((Action)action).Invoke();
//			});

			LoadTripCommand.Subscribe (x => {
				Trip = x;
			});

			BtnBackClick = ReactiveCommand.Create ();
			BtnBackClick
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (action => {
				if (action != null)
					((Action)action).Invoke ();
				DependencyService.Get<INavigator> ().NavigateBack ();
			});

			this.WhenAnyValue (vm => vm.Trip)
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Where (i => i != null)
				.Subscribe (x => {
//					HeaderLogo = "logo_trip.png";
//					HeaderTitle = Trip.StartTime.ToLocalTime().ToString ("M") + "," + Trip.StartTime.ToLocalTime().ToString (" hh:mmtt").ToLower () + " Trip";
				StartTimeString = Trip.StartTime.ToLocalTime ().ToString ("HH:mm");
				Score = Math.Round (Trip.Score, 1).ToString ();
				TimeSpan duration = TimeSpan.FromMilliseconds (Trip.Duration);
				EndTimeString = Trip.EndTime.ToLocalTime ().ToString ("HH:mm");
				StartTimeAmOrPm = Trip.StartTime.ToLocalTime ().ToString ("tt").ToLower ();
				EndTimeAmOrPm = Trip.EndTime.ToLocalTime ().ToString ("tt").ToLower ();

				Duration = string.Format ("{0}:{1}", (int)duration.TotalHours, duration.ToString (@"mm"));

				Distance = Math.Round (Trip.Distance, 1).ToString () + Translator.Translate ("TRIPS_STATEMENT_KM");

				Trip.AverageSpeed = Trip.Distance / (double)duration.TotalHours;
				AverageSpeed = Math.Round (Trip.AverageSpeed, 1).ToString () + Translator.Translate ("TRIPS_STATEMENT_KM/H");
				// Shouldn't use score as string
				SmoothScore = Math.Round (Trip.SmoothScore, 0).ToString ();
				SpeedScore = Math.Round (Trip.SpeedScore, 0).ToString ();
				DistractionScore = Math.Round (Trip.DistractionScore, 0).ToString ();
				TimeOfDayScore = Math.Round (Trip.TimeOfDayScore, 0).ToString ();
			});
		}

		public List<string> GetWebViewScripts ()
		{
			if (Trip == null || Trip.Coordinates == null) {
				return null;
			}
			var scripts = new List<string> ();
			scripts.Add (@"javascript:refresh();");

//			_tripDetail = await (new TripDetailManager ()).GetTripDetailFromServer (_trip.TripId);
//			List<GPS> trip = _tripDetail.Coordinates;

//			List<GPS> trip = TripManager.GetTripDetail (_trip.TripId);
			List<GPS> tripGPS = Trip.Coordinates;
//			Console.WriteLine (tripx.Count);
//			List<GPS> trip = new List<GPS> ();
//			for (int i = 0; i < tripx.Count && i < tripx.Count - 250; i++) {
//				trip.Add (tripx [i]);
//			}

			if (tripGPS == null || tripGPS.Count == 0)
				return scripts;

			double latitude = 0;
			double longitude = 0;

			string lat = "";
			string lng = "";

			double sumLat = 0;
			double sumLong = 0;

			tripGPS.ForEach (coordinate => {
				GPSConverter.bd_encrypt (coordinate.Latitude, coordinate.Longitude, out latitude, out longitude);
				NormalizeCoordinate (latitude, longitude, out lat, out lng);
				scripts.Add (string.Format (@"javascript:addCoordinate({0},{1});", lat, lng));

				sumLat += latitude;
				sumLong += longitude;
			});

			// Calculate the center of the trip
			sumLat /= scripts.Count;
			sumLong /= scripts.Count;
			NormalizeCoordinate (sumLat, sumLong, out lat, out lng);
			scripts.Add (string.Format ("javascript:centerAt({0},{1});", sumLat, sumLong));

			// Add start point A
			GPSConverter.bd_encrypt (tripGPS [0].Latitude, tripGPS [0].Longitude, out latitude, out longitude);
			NormalizeCoordinate (latitude, longitude, out lat, out lng);
			scripts.Add (string.Format (@"javascript:addMark({0},{1},'{2}',{3});", lat, lng, "Start", 1));

			// Add end point B
			GPSConverter.bd_encrypt (tripGPS [tripGPS.Count - 1].Latitude, tripGPS [tripGPS.Count - 1].Longitude, out latitude, out longitude);
			NormalizeCoordinate (latitude, longitude, out lat, out lng);
			scripts.Add (string.Format (@"javascript:addMark({0},{1},'{2}',{3});", lat, lng, "End", 2));

			// Show map
			scripts.Add (@"javascript:showMap(true);");
			Console.WriteLine (">>> Show map");

			return scripts;

//			double cLat = 0;
//			double cLng = 0;
//			double convertedlat = 0;
//			double convertedlng = 0;
//
//			string script = string.Empty;
//			var scripts = new List<string>();
//
//			if (_trip == null)
//			{
//				return scripts;
//			}
//
//			string refreshMap = "javascript:refresh();";
//			scripts.Add(refreshMap);
//
//			if (_trip.Coordinators.Count >= 2)
//			{
//				// Add start A marker
//				script = "javascript:addMark({0},{1},'{2}',{3});";
//				GPSConverter.ConvertToCorrectedGPS(_trip.Coordinators[0].Lat, _trip.Coordinators[0].Long, out convertedlat, out convertedlng);
//				scripts.Add(string.Format(script, convertedlat.ToString().Replace(',', '.'), convertedlng.ToString().Replace(',', '.'), Translator.Translate("TRIP_START"), 1));
//				// Add end B marker
//				GPSConverter.ConvertToCorrectedGPS(_trip.Coordinators[_trip.Coordinators.Count - 1].Lat, _trip.Coordinators[_trip.Coordinators.Count - 1].Long, out convertedlat, out convertedlng);
//				scripts.Add(string.Format(script, convertedlat.ToString().Replace(',', '.'), convertedlng.ToString().Replace(',', '.'), Translator.Translate("TRIP_END"), 2));
//
//				foreach (var item in _trip.Coordinators)
//				{
//					GPSConverter.ConvertToCorrectedGPS(item.Lat, item.Long, out convertedlat, out convertedlng);
//					string lat = convertedlat.ToString().Replace(',', '.');
//					string lng = convertedlng.ToString().Replace(',', '.');
//
//					scripts.Add(string.Format("javascript:addCoordinate({0},{1});", lat, lng));
//					cLat += convertedlat;
//					cLng += convertedlng;
//				}
//
//				if (_trip.EventProcessed || _trip.Events != null && _trip.Events.Count > 0)
//				{
//					if (_trip.Events != null && _trip.Events.Count > 0)
//					{
//						foreach (var item in _trip.Events)
//						{
//							long timeStamp = (long)item.TimeStamp;
//							DateTime date = new DateTime(timeStamp + 1000L);
//
//							GPSConverter.ConvertToCorrectedGPS(item.Lat, item.Long, out convertedlat, out convertedlng);
//							string lat = convertedlat.ToString().Replace(',', '.');
//							string lng = convertedlng.ToString().Replace(',', '.');
//
//							string windowContent = string.Format("<div class=\"popup\"><form><h3>{0} <div id=\"eventId\" hidden=\"true\">{1}</div></h3><ul><li>{4}: {5}</li><li>{6}: {7}</li><li>{8}: {9}</li></ul>" +
//								"<p>{2}</p><textarea class = \"feedback\" rows=\"4\">{10}</textarea><input onclick=\"formSubmit();\" class=\"submitButon\" type=\"button\" value=\"{3}\" /></form></div>"
//								, Translator.Translate("TRIPDETAIL_EVENT"), item.EventId
//								, Translator.Translate("TRIPDETAIL_FEEDBACK")
//								, Translator.Translate("TRIPDETAIL_SEND")
//								, Translator.Translate("TRIPDETAIL_LATITUDE"), lat
//								, Translator.Translate("TRIPDETAIL_LONGITUDE"), lng
//								, Translator.Translate("TRIPDETAIL_TIMESTAMP"), date.ToLocalTime().ToString(Translator.GetFormat("TIME_FORMAT_24"))
//								, (item.EventFeedback == null) ? "" : item.EventFeedback);
//
//							scripts.Add(string.Format("javascript:addMarkEvent({0},{1},'{2}',{3});", lat, lng, windowContent, item.EventId));
//							cLat += convertedlat;
//							cLng += convertedlng;
//						}
//					}
//				}
//
//				string centerLat = (cLat / _trip.Coordinators.Count).ToString().Replace(',', '.');
//				string centerLng = (cLng / _trip.Coordinators.Count).ToString().Replace(',', '.');
//				scripts.Add(string.Format("javascript:centerAt({0},{1});", centerLat, centerLng));
//			}
//			scripts.Add(string.Format("javascript:showMap({0})", (_trip.EventProcessed ? "1" : "0")));
//
//			return scripts;
		}
		//
		//		public void InitData()
		//		{
		//			HeaderTitle= _trip.StartTime.ToString ("M") + "," + _trip.StartTime.ToString (" hh:mmtt").ToLower() + " Trip";
		//			StartTimeString = _trip.StartTime.ToString ("hh:mm");
		//			Score = _trip.Score.ToString ();
		//			EndTimeString = _trip.EndTime.ToString ("hh:mm");
		//			StartTimeAmOrPm = _trip.StartTime.ToString ("tt").ToLower();
		//			EndTimeAmOrPm = _trip.EndTime.ToString ("tt").ToLower();
		//
		//			TimeSpan duration = _trip.EndTime.Subtract (
		//				_trip.StartTime);
		//			Duration = string.Format ("{0}:{1}", (int)duration.TotalHours, duration.ToString(@"mm"));
		//			//Duaration = duration.ToString (@"hh\:mm");
		//
		//			Distance = _trip.Distance.ToString () + "Km";
		//			AverageSpeed = _trip.AverageSpeed.ToString() + "Km/h";
		//			SmoothScore = _trip.SmoothScore.ToString ();
		//			SpeedScore = _trip.SpeedScore.ToString ();
		//			DistractionScore = _trip.DistractionScore.ToString ();
		//			TimeOfDayScore = _trip.TimeOfDayScore.ToString ();
		//		}
		private void NormalizeCoordinate (double latitude, double longitude, out string lat, out string lng)
		{
			lat = latitude.ToString ().Replace (',', '.');
			lng = longitude.ToString ().Replace (',', '.');
		}
	}
}

