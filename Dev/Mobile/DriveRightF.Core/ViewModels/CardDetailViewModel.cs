﻿using System;
using ReactiveUI;
using System.Drawing;
using System.Reactive.Linq;
using Unicorn.Core;
using DriveRightF.Core.Enums;
using Unicorn.Core.Navigator;
using DriveRightF.Core.Managers;
using DriveRightF.Core.Models;
using DriveRightF.Core.Constants;

namespace DriveRightF.Core.ViewModels
{
	public class CardDetailViewModel : BaseHandlerHeaderViewModel
	{
		private DateTime _expireDate;
		private string _sponsorBy;
		private Award _award;

		public Award Award {
			get
			{
				return _award;
			}
			set
			{
				_award = value;
				SponsorBy = value.Sponsor;
				ExpireDate = value.Expiry;
			}
		}

		public DateTime ExpireDate {
			get
			{
				return _expireDate;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _expireDate, value);
			}
		}

		public string SponsorBy {
			get
			{
				return _sponsorBy;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _sponsorBy, value);
			}
		}

		private IReactiveCommand<object> _btnGambleClick;

		public IReactiveCommand<object> BtnGambleClick {
			get
			{
				return _btnGambleClick;
			}
			private set
			{
				_btnGambleClick = value;
			}
		}

		private IReactiveCommand<object> _btnTakeMoney;

		public IReactiveCommand<object> BtnTakeMoney {
			get
			{
				return _btnTakeMoney;
			}
			private set
			{
				_btnTakeMoney = value;
			}
		}

		private AccountManager AccountManager {
			get { 
				return DependencyService.Get<AccountManager> ();
			}
		}

		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get {
				return (int)Screen.CardDetail;
			}
		}
		public override void UpdateHeaderFooter()
		{
			HeaderViewModel.IsShowBtnBack = true;
			HeaderViewModel.Title = Translator.Translate("CARD_DETAILS_HEADER");

			TabBarViewModel.IsHidden = true;
		}

		#endregion

		public CardDetailViewModel() : base()
		{
			InitCommand ();
		}

		private void InitCommand()
		{
			BtnGambleClick = ReactiveCommand.Create ();
			BtnGambleClick.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (action => {
				if(action != null)
					((Action)action).Invoke ();
			});

			BtnTakeMoney = ReactiveCommand.Create ();
			BtnTakeMoney.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (action => {
					if(action != null)
					{
						((Action)action).Invoke ();
						UpdateAccount();
					
						MessagingCenter.Send<Award>(Award, MessageConstant.CARD_TAKE_MONEY);
					}
				});
		}

		/// <summary>
		/// Updates the account for take money
		/// </summary>
		private void UpdateAccount()
		{
			Account account = AccountManager.GetCurrentAccount ("1");
			if (account != null) {
				account.CashEarned = (float)Math.Round (account.CashEarned + 3.99f, 2);
				account.EndCard = account.EndCard - 1;
				account.EndBalance = (float)Math.Round (account.EndBalance + 3.99f, 2);

				AccountManager.SaveAccount (account);

				//Create cash transaction history
				AccountTransaction cashTransaction = new AccountTransaction {
					AccountId = "1",
					Time = DateTime.Now,
					Description = Translator.Translate("ACCOUNT_STATEMENT_TAKE_MONEY"),
					Type =  Translator.Translate("ACCOUNT_STATEMENT_TAKE_MONEY"),
					Value = 3.99f,
					Unit = Translator.Translate ("RMB")
				};
				AccountManager.SaveAccountTransaction (cashTransaction);

				//Create card transaction history
				AccountTransaction cardTransaction = new AccountTransaction {
					AccountId = "1",
					Time = DateTime.Now,
					Description =  Translator.Translate("ACCOUNT_STATEMENT_TAKE_MONEY"),
					Type =  Translator.Translate("ACCOUNT_STATEMENT_TAKE_MONEY"),
					Value = -1,
					Unit = Translator.Translate("ACCOUNT_STATEMENT_CASH")
				};
				AccountManager.SaveAccountTransaction (cardTransaction);
			}

		}
	}
}

