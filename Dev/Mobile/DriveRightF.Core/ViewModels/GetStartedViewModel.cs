﻿using System;
using DriveRightF.Core;
using ReactiveUI;
using System.Reactive.Linq;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using DriveRightF.Core.Enums;

namespace DriveRightF.Core.ViewModels
{
	public class GetStartedViewModel : BaseViewModel
	{
		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify
		{
			get
			{
				return (int)Screen.GetStart;
			}
		}

		#endregion

		private IReactiveCommand<object> _onNavigateToLoginCommand = ReactiveCommand.Create();

		public IReactiveCommand<object> OnNavigateToLoginCommand
		{
			get
			{
				return _onNavigateToLoginCommand;
			}
			set
			{ 
				_onNavigateToLoginCommand = value;
			}
		}

		public GetStartedViewModel() : base()
		{
			
		}

		protected override void InitCommands()
		{
			OnNavigateToLoginCommand.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(obj =>
			{
				try
				{
					DependencyService.Get<INavigator>().Navigate((int)Screen.Login, new object[0]);
				}
				catch(Exception ex)
				{
					PopupManager.ShowAlert(Translator.Translate("ALERT"), ex.Message, Unicorn.Core.Enums.DialogIcon.None, true, null, Translator.Translate("BUTTON_OK"));
				}
			});
		}
	}
}

