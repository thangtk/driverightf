﻿using System;
using DriveRightF.Core;
using ReactiveUI;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using Unicorn.Core;
using DriveRightF.Core.Enums;
using Unicorn.Core.Navigator;
using System.Threading.Tasks;
using DriveRightF.Core.Managers;
using System.Diagnostics;
using DriveRightF.Core.Models;
using DriveRightF.Core.Constants;
using DriveRightF.Core.Storages;

namespace DriveRightF.Core.ViewModels
{
	public class SplashViewModel : BaseViewModel
	{
		public IReactiveCommand<object> _loadComponentCommand = ReactiveCommand.Create ();

		public IReactiveCommand<object> LoadComponentCommand {
			get {
				return _loadComponentCommand;
			}
			set{ 
				_loadComponentCommand = value;
			}
		}

		private UserManager UserManager
		{
			get {
				return DependencyService.Get<UserManager> ();
			}
		}

		private IPolaraxSDK PolaraxSDK {
			get { return DependencyService.Get<IPolaraxSDK> ();}
		}

		public SplashViewModel() : base()
		{
			
		}

		public override int ScreenToNotify {
			get {
				return (int)Screen.Splash;
			}
		}

		protected override void InitCommands()
		{
			LoadComponentCommand
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Select (obj => {
					return UserManager.CheckLogin();
				})
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
				isLogin => {
						LoadComponents();
						Navigate(isLogin);
				});
		}

		public void LoadComponents()
		{
			//Load stuff
//			DependencyService.Get<DriveRightRepository> ();
//			var perkManager = new PerkManager();
//
//			if (perkManager.GetAvailablePerk (1) == null)
//				for (int i = 0; i < 20; i++) {
//					var perk = new AvailablePerk () {
//						PerkId = (i + 1).ToString (),
//						Icon = i % 2 == 0 ? "award_coffee.png" : "award_dimond.png"
//					};
//					perkManager.SaveAvailablePerk (perk);
//				}
//			else {
//			}
		}

		public async void Navigate(bool isLogin)
		{
			#if DEBUG
//			await Task.Delay(2000);
			#endif

			if (isLogin) {
//				Unicorn.Core.Debugger.StartTracking();
				DependencyService.Get<INavigator> ().Navigate ((int)Screen.Home);
				//RxApp.MainThreadScheduler.Schedule (() => PolaraxSDK.StartService (x => Console.WriteLine (x.ErrorMessage)));
			}
			else
				DependencyService.Get<INavigator> ().Navigate ((int)Screen.Signup);
			
		}
	}
}

