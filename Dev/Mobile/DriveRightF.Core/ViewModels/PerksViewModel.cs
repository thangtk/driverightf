﻿using System;
using DriveRightF.Core;
using System.Reactive.Linq;
using System.Linq;
using ReactiveUI;
using System.Reactive.Concurrency;
using DriveRightF.Core.Models;
using System.Collections.Generic;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using System.Timers;
using DriveRightF.Core.Managers;
using System.Threading.Tasks;
using DriveRightF.Core.Constants;
using DriveRightF.Core.Enums;

namespace DriveRightF.Core.ViewModels
{
	public class PerksViewModel : BaseAutoRequestViewModel<Perk>
	{
		public PerksViewModel ()
		{
		}

		#region implemented abstract members of BaseAutoRequestViewModel

		public override BaseAutoRequestManager<Perk> Manager {
			get {
				return DependencyService.Get<BaseAutoRequestManager<Perk>> ();
			}
		}

		public override int ScreenToNotify {
			get {
				return (int)Screen.Perks;
			}
		}
		protected override void InitCommands ()
		{
			base.InitCommands ();
		}
		protected override void Initalize (object param)
		{
			base.Initalize (param);
			HeaderTitle = "Perks";
			HeaderLogo = "logo_perk.png";
		}
		#endregion

	}

}

