﻿using System;
using DriveRightF.Core.ViewModels;
using ReactiveUI;

namespace DriveRightF.Core
{
	public class PaymentPopupViewModel : BaseViewModel
	{
		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get {
				return -1;
			}
		}

		#endregion

		public PaymentPopupViewModel ()
		{
		}

		private string _accountType;

		public string PaymentType {
			get { return _accountType; }
			set { this.RaiseAndSetIfChanged (ref _accountType, value); }
		}

		private string _accountName;

		public string PaymentName {
			get { return _accountName; }
			set { this.RaiseAndSetIfChanged (ref _accountName, value); }
		}

		private string _accountNumber;

		public string PaymentAccount {
			get { return _accountNumber; }
			set { this.RaiseAndSetIfChanged (ref _accountNumber, value); }
		}
	}
}

