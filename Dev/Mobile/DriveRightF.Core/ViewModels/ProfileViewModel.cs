﻿using System;
using System.Collections.Generic;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DriveRightF.Core.Constants;
using DriveRightF.Core.Enums;
using DriveRightF.Core.Managers;
using DriveRightF.Core.Models;
using ReactiveUI;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using Unicorn.Core.UI;
using System.Reactive.Disposables;

namespace DriveRightF.Core.ViewModels
{
	public partial class ProfileViewModel: BaseHandlerHeaderViewModel
	{
		private object _iconData;

		private CaptureImageViewModel _captureImageViewModel;

		public CaptureImageViewModel CaptureImageViewModel {
			get {
				return _captureImageViewModel;
			}
			set {
				_captureImageViewModel = value;
				InitCommandChild ();
			}
		}

		#region implemented abstract members of BaseHandlerHeaderViewModel

		public override int ScreenToNotify {
			get {
				return -1; //(int)Screen.Home;
			}
		}

		public override void UpdateHeaderFooter ()
		{
			HeaderViewModel.Title = Translator.Translate ("PROFILE_HEADER");
			HeaderViewModel.IsShowBtnBack = false;
			HeaderViewModel.IsShowBtnInfo = false;
			HeaderViewModel.IsShowBtnShare = false;
			HeaderViewModel.IsShowBtnNotification = false;

			TabBarViewModel.IsHidden = false;
			TabBarViewModel.ItemSelected = 1;
		}

		#endregion

		public ProfileViewModel () : base ()
		{
			InitData ();
		}

		public ProfileViewModel (CaptureImageViewModel children) : base ()
		{
			InitData ();
		}

		//		private void InitChildModels ()
		//		{
		//			Console.WriteLine ("InitChildModels");
		//			NameModel = new UTextBoxViewModel () {
		//				Title = "Name",
		//				CurrentScreen = (int) Screen.Profile,
		//			};
		//
		//			SocialModel = new UTextBoxViewModel () {
		//				Title = "Social security No.",
		//				CurrentScreen = (int) Screen.Profile,
		//			};
		//
		//			PhoneNumberModel = new UTextBoxViewModel () {
		//				Title = "Phone number",
		//				Validations = new List<Validation>
		//				{
		//					new Validation
		//					{
		//						Type = ValidationType.PhoneNumber,
		//					},
		//				},
		//
		//				KeyboardType = KeyboardType.Phone,
		//				CurrentScreen = (int) Screen.Profile,
		//			};
		//
		//			PasswordModel = new UTextBoxViewModel () {
		//				Title = "Password",
		//				CurrentScreen = (int) Screen.Profile,
		//			};
		//
		//			WechatModel = new UTextBoxViewModel () {
		//				Title = "Wechat",
		//				CurrentScreen = (int)Screen.Profile,
		//			};
		//
		//			DOBModel = new UDatePickerViewModel () {
		//				Title = "Date of birth",
		//				CurrentScreen = (int)Screen.Profile,
		//			};
		//
		//		}

		public void InitData ()
		{
			ScreenTitle = Translator.Translate ("PROFILE_HEADER");
			AnyFieldChanged = false;
		}

		private void InitCommandChild ()
		{
			MyDebugger.ShowCost ("InitCommandChild - in");
			CaptureImageViewModel
				.CancelButtonCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (obj => {
				IsPictureLoading = false;
			});
			CaptureImageViewModel
				.DoneButtonCommand
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Select<object,object> (obj => {
				return CaptureImageViewModel.CropResizeImage ();
			})
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (obj => {
				IsPictureLoading = false;
				if (obj != null) {
					IconData = obj;
				}
			});
			MyDebugger.ShowCost ("InitCommandChild - out");
		}

		protected override void InitCommands ()
		{
//			Debugger.ShowCost ("InitCommands - in");
			OnBackClicked
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				if (AnyFieldChanged) {
					PopupManager.ShowConfirm (Translator.Translate ("CONFIRM"), Translator.Translate ("LOST_INFORMATION"), 
						() => {
							DependencyService.Get<INavigator> ().NavigateBack (true);
							SetProperties (ProfileManager.Profile);
							ResetPaymentViewModel ();
							ResetProfileImage ();
							AnyFieldChanged = false;
//							ResetImage
							//TODO: reset image: iconData + iconurl.
						},
						null, Translator.Translate ("BUTTON_OK"), Translator.Translate ("BUTTON_CANCEL"), showTitle: true);
				} else {
					DependencyService.Get<INavigator> ().NavigateBack (true);
				}

			});

			OnCaptureClicked
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (obj => {
				Action act = obj as Action;
				if (act != null) {
					act.Invoke ();
				}
				ImagePicker.PickImage (iconDatas => {
					//IconData = bytesIcon;
					//IsPictureLoading = false;

					if (iconDatas != null) {
						RxApp.MainThreadScheduler.Schedule (() => {
							CaptureImageViewModel.ImageData = iconDatas;
							CaptureImageViewModel.Visible = true;
							//Navigator.Navigate((int)Screen.CaptureImage,bytesIcon);
						});
					}

				}, () => {
					IsPictureLoading = true;
				});
			});

			// ------------------
			// Setup for Update Profile command
			UpdateProfileCommand = ReactiveCommand.CreateAsyncObservable (
				obj => {
					System.Diagnostics.Debug.WriteLine ("Update profile >>> ");
					bool isValid = ValidateFields ();
					if (!isValid) {
						return Observable.Return (false);
					}
					//
					PopupManager.ShowLoading ();
					return Observable.Start (() => {
//						Thread.Sleep(3000);

						//TODO: warning save and update image.
						var profile = GetProfileData ();
						return ProfileManager.UpdateProfile(profile,IconData);
					});
				});
			//			UpdateProfileCommand.IsExecuting.ToProperty (this, vm => vm.IsBusy, false, RxApp.MainThreadScheduler);
			UpdateProfileCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Where (b => b)
				.Subscribe (b => {
				MessagingCenter.Send (this, MessageConstant.PROFILE_UPDATE);
				PopupManager.HideLoading ();
				PopupManager.ShowToast (Translator.Translate ("UPDATE_SUCCESS"), ToastLength.Long);
				AnyFieldChanged = false;
			});

			ChangePhoneNumber = ReactiveCommand.Create ();
			ChangePhoneNumber
				.Where (x => x != null)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => PopupManager.ShowPopup (Translator.Translate ("NEW_PHONE_NUMBER"), x, new List<DialogButton> () {
			
				new DialogButton () {
					Text = Translator.Translate ("BUTTON_CANCEL"),
					OnClicked = () => {
						TempPhoneNo = string.Empty;
					}
				},
				new DialogButton () {
					Text = Translator.Translate ("BUTTON_GET_SMS"),
					OnClicked = () => {
						if (ValidatePhoneNumber ()) {
							GetSMSCodeCommand.Execute (null);
						} else {
							PopupManager.ShowAlert (Translator.Translate ("ALERT"), Translator.Translate ("PHONE_NUMBER_EMPTY_WRONG"), Unicorn.Core.Enums.DialogIcon.None, false, null, Translator.Translate ("BUTTON_OK"));
						}
					},
					IsBold = true
				}
			
			}));

			ChangeNumberPlate = ReactiveCommand.Create ();
			ChangeNumberPlate
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => PopupManager.ShowAlert (Translator.Translate ("ALERT"), Translator.Translate ("FUNCTION_COMMING_SOON"), Unicorn.Core.Enums.DialogIcon.None, false, null, Translator.Translate ("BUTTON_OK")));

			ChangeAccountNumber = ReactiveCommand.Create ();
			ChangeAccountNumber
				.Where (x => x != null)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => PopupManager.ShowPopup (Translator.Translate ("SIGN_UP_PAYMENT_METHOD"), x, new List<DialogButton> () {
				new DialogButton () {
					Text = Translator.Translate ("BUTTON_CANCEL"),
					OnClicked = ResetPaymentViewModel
				},
				new DialogButton () {
					Text = Translator.Translate ("BUTTON_VERIFY"),
					OnClicked = UpdatePaymentData,
					IsBold = true
				}
			
			}));


			//(x => PopupManager.ShowAlert ("Demo", "Change Account number is processing... Close if you cannot wait any more (-_-)"));

			VerifySMSCode = ReactiveCommand.Create ();
			VerifySMSCode.Subscribe (x => PopupManager.ShowPopup (Translator.Translate ("ENTER_SMS_CODE"), x, new List<DialogButton> () {
				new DialogButton () {
					Text = Translator.Translate ("BUTTON_CANCEL"),
					OnClicked = () => {
						SMSCode = string.Empty;
						TempPhoneNo = string.Empty;
					}
				},
				new DialogButton () {
					Text = Translator.Translate ("BUTTON_RESEND"),
					OnClicked = () => {
						SMSCode = string.Empty;
						TempPhoneNo = string.Empty;
						GetSMSCodeCommand.Execute (false);
					},
					CloseAfterClick = false
				},
				new DialogButton () {
					Text = Translator.Translate ("BUTTON_CONTINUE"),
					OnClicked = async () => {
						// Demo
						PopupManager.ShowLoading ();
						if (ValidateSMSCode ()) {
							PhoneNumber = TempPhoneNo;
							PopupManager.ShowToast (Translator.Translate ("PHONE_NUMBER_HAS_CHANGED"), ToastLength.Short);
						} else {
							PopupManager.ShowAlert (Translator.Translate ("ERROR"), Translator.Translate ("SMS_VERIFY_WRONG"), Unicorn.Core.Enums.DialogIcon.None, false, null, Translator.Translate ("BUTTON_OK"));
						}
						TempPhoneNo = string.Empty;
						SMSCode = string.Empty;
						PopupManager.HideLoading ();
						// Save and change data
//						_profile.PhoneNumber = PhoneNumber;
//						ProfileManager.Save(_profile);

						//

					}, IsBold = true
				},
			}));

			GetSMSCodeCommand = ReactiveCommand.CreateAsyncTask (async obj => {
//				PopupManager.ShowLoading();
				// obj parameter determine Verify form will be shown or not
				return await GetSMSCodeAsync (obj == null || (bool)obj);
			});
			GetSMSCodeCommand.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (b => PopupManager.HideLoading ());

			// Setup for Get profile command
			// For more simpler should implement as TripStatementViewModel
			// but I wanna try other usages
			GetProfileCommand = ReactiveCommand.CreateAsyncObservable (obj => {
				PopupManager.ShowLoading();
				ErrorReportType = ErrorReportType.None; // Turn off report error by dialog
				//
				var obs = Observable.Create<Profile> (o => {
					o.OnNext(ProfileManager.GetProfile());
					o.OnNext(ProfileManager.GetProfileFromService());
					o.OnCompleted();
					return UpdateUI;
				});
				//
				return obs.SubscribeOn(RxApp.TaskpoolScheduler);
			});
			// NOTE: Command have not OnCompleted and OnError
			GetProfileCommand
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(SetProperties); // , ex => UpdateUI(), () => UpdateUI()
			GetProfileCommand.ThrownExceptions.Subscribe (ex => UpdateUI ());
//			Debugger.ShowCost ("InitCommands - out");
		}

		// Profile profile
		private void UpdateUI ()
		{
			MessagingCenter.Send (this, MessageConstant.PROFILE_UPDATE);
			// Rem by KT - use observable instead
//			SetProperties (profile);
			ObserveFieldChanged ();
			PopupManager.HideLoading ();
			AnyFieldChanged = false;
			ErrorReportType = ErrorReportType.Dialog;
		}

		private void ResetPaymentViewModel ()
		{
			if (_paymentViewModel != null) {
				_paymentViewModel.PaymentAccount = PaymentAccount;
				_paymentViewModel.PaymentName = PaymentName;
				_paymentViewModel.PaymentType = PaymentType;
			}
		}

		private void ResetProfileImage ()
		{
			IconData = null;
		}

		private void SetProperties (Profile profile)
		{
			if (profile == null) {
				return;
			}

			Name = profile.Name;
			PhoneNumber = profile.PhoneNumber;
			NumberPlate = profile.NumberPlate;
			PaymentAccount = profile.PaymentAccount;
			PaymentName = profile.PaymentName;
			PaymentType = profile.PaymentType;
			PaymentAccount = profile.PaymentAccount;

			ImagePath = string.Empty;
			const string defaultImage = "icon_unknown_user.png";
			ImagePath = string.IsNullOrEmpty (profile.Image) ? defaultImage : profile.Image;
		}

		private async Task<bool> GetSMSCodeAsync (bool showVerifySMSForm = true)
		{
//			await Task.Delay(3000);
			return showVerifySMSForm;
		}

		void UpdatePaymentData ()
		{
			PaymentAccount = PaymentViewModel.PaymentAccount;
			PaymentName = PaymentViewModel.PaymentName;
			PaymentType = PaymentViewModel.PaymentType;
		}

		private Profile GetProfileData ()
		{
			//TODO: get profile from DB and update some field from UI!
			Profile profile =  ProfileManager.GetProfile();

			profile.Name = Name;
			profile.PhoneNumber = PhoneNumber;
			profile.NumberPlate = NumberPlate;
			profile.PaymentAccount = PaymentAccount;
			profile.PaymentType = PaymentType;
			profile.PaymentName = PaymentName;


//			_profile.Image = "";
			profile.ImageUrl = "";

			return profile;
		}

		private bool ValidateFields ()
		{
			// SKIP validate in Demo
			return true;
			//
//			bool isAllPass = true;
//			isAllPass = NameModel.IsValid && PhoneNumberModel.IsValid && WechatModel.IsValid && SocialModel.IsValid;
//
//			if (!isAllPass) {
//				IsAllPass = !IsAllPass;
//			}
//			return isAllPass;

		}

		public void ObserveFieldChanged ()
		{
			this.WhenAnyValue (x => x.Name, x => x.PhoneNumber, x => x.NumberPlate,
				x => x.PaymentName, x => x.PaymentType, x => x.PaymentAccount)
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (t => {
				Profile profile = ProfileManager.Profile;
				this.AnyFieldChanged = 
						t.Item1 != profile.Name ||
				t.Item2 != profile.PhoneNumber ||
				string.IsNullOrEmpty (t.Item3) != string.IsNullOrEmpty (profile.NumberPlate) ||
				t.Item4 != profile.PaymentName ||
				t.Item5 != profile.PaymentType ||
				t.Item6 != profile.PaymentAccount;
			});
			this.WhenAnyValue (x => x.IconData)
				.Where(x => x != null)
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (x => 
					{
						AnyFieldChanged = true;
					});
		}

		#region New requirement - 21/04/2015

		private string _name = "";

		public string Name {
			get {
				return _name;
			} 
			set {
				this.RaiseAndSetIfChanged (ref _name, value);
			}
		}

		private string _phoneNumber = "";

		public string PhoneNumber {
			get {
				return _phoneNumber;
			} 
			set {
				this.RaiseAndSetIfChanged (ref _phoneNumber, value);
			}
		}

		private string _numberPlate = "";

		public string NumberPlate {
			get {
				return _numberPlate;
			} 
			set {
				this.RaiseAndSetIfChanged (ref _numberPlate, value);
			}
		}

		private string _paymentAccount = "";

		public string PaymentAccount {
			get {
				return _paymentAccount;
			} 
			set {
				this.RaiseAndSetIfChanged (ref _paymentAccount, value);
			}
		}

		private string _paymentName = "";

		public string PaymentName {
			get {
				return _paymentName;
			}
			set {
				this.RaiseAndSetIfChanged (ref _paymentName, value);
			}
		}

		private string _paymentType = "";

		public string PaymentType {
			get {
				return _paymentType;
			}
			set {
				this.RaiseAndSetIfChanged (ref _paymentType, value);
			}
		}

		private bool _anyFieldChanged;

		public bool AnyFieldChanged {
			get {
				return _anyFieldChanged;
			} 
			set {
				this.RaiseAndSetIfChanged (ref _anyFieldChanged, value);
			}
		}

		public IReactiveCommand<object> ChangePhoneNumber {
			get;
			set;
		}

		public IReactiveCommand<object> ChangeNumberPlate {
			get;
			set;
		}

		public IReactiveCommand<object> ChangeAccountNumber {
			get;
			set;
		}

		public IReactiveCommand<object> VerifySMSCode {
			get;
			set;
		}

		public IReactiveCommand<bool> GetSMSCodeCommand {
			get;
			set;
		}

		private string _tempPhoneNo;

		public string TempPhoneNo {
			get {
				return _tempPhoneNo;
			} 
			set {
				this.RaiseAndSetIfChanged (ref _tempPhoneNo, value);
			}
		}

//		private Profile _profile = null;
////
//		public Profile Profile {
//			get {
//				return _profile;
//			}
//			set {
//				_profile = value;
//			}
//		}

		private PaymentPopupViewModel _paymentViewModel;

		public PaymentPopupViewModel PaymentViewModel {
			get { 
				if (_paymentViewModel == null) {
					_paymentViewModel = new PaymentPopupViewModel () { 
						PaymentName = this.PaymentName,
						PaymentType = this.PaymentType,
						PaymentAccount = this.PaymentAccount
					};
				}
				return _paymentViewModel;
			}
			set {
				this.RaiseAndSetIfChanged (ref _paymentViewModel, value);
			}
		}

		#endregion

		private bool ValidateSMSCode ()
		{
			if (!String.IsNullOrEmpty (SMSCode)) {
				//				#if DEBUG
				if (SMSCode == "12345") {
					return true;
				}
				//				#endif
			}
			return false;
		}

		private bool ValidatePhoneNumber ()
		{
			//TODO: add validation in here
			if (TempPhoneNo == "12345") {
				return true;
			}
			if (!String.IsNullOrEmpty (TempPhoneNo)) {
				Match match = Regex.Match (TempPhoneNo, @"^[+. ]?\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4,6})$");
				return match.Success;
			}
			return false;
		}
	}
}

