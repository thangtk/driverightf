﻿using System;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using System.Reactive.Linq;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using DriveRightF.Core.Models;
using System.Collections.Generic;
using DriveRightF.Core.Managers;
using DriveRightF.Core.Enums;

namespace DriveRightF.Core.ViewModels
{
	public class EarningDetailViewModel : BaseViewModel
	{
		private int _progress;
		private string _name;
		private string _earningId;
		private string _icon;
		private string _image;
		private string _iconUrl;
		private string _imageUrl;
		private string _description;
		private	string _briefDesc;
		private string _startTime;
		private string _endTime;
		private string _updateTime;
		private List<Tuple<string, bool>> _iconList;
		private string _archieve;
		private string _get;
		private EarningStatus _status;
		private Earning _earning;

		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get {
				return (int)Screen.EarningDetail;
			}
		}

		#endregion
		private IReactiveCommand<object> _btnBackClicked;
		private IReactiveCommand<object> _loadEarningCommand;
		private IReactiveCommand<object> _loadPerkIconCommand;
		private IReactiveCommand<object> _saveEarningCommand;
		public IReactiveCommand<object> BtnBackClicked {
			get {
				return _btnBackClicked;
			}
			set{ 
				_btnBackClicked = value;
			}
		}

		public IReactiveCommand<object> LoadEarningCommand {
			get {
				return _loadEarningCommand;
			}
			set{ 
				_loadEarningCommand = value;
			}
		}

		public IReactiveCommand<object> LoadPerkIconCommand {
			get {
				return _loadPerkIconCommand;
			}
			set{ 
				_loadPerkIconCommand = value;
			}
		}

		public IReactiveCommand<object> SaveEarningCommand {
			get {
				return _saveEarningCommand;
			}
			set{ 
				_saveEarningCommand = value;
			}
		}

		public string Name {
			get { return _name ?? "";}
			set { this.RaiseAndSetIfChanged (ref _name, value);}
		}

		public string EarningId {
			get { return _earningId;}
			set { this.RaiseAndSetIfChanged (ref _earningId, value);}
		}

		public string Icon {
			get { return _icon ?? "";}
			set { this.RaiseAndSetIfChanged (ref _icon, value);}
		}

		public string IconUrl {
			get { return _iconUrl ?? "";} 
			set { this.RaiseAndSetIfChanged (ref _iconUrl, value);}
		}

		public string Image {
			get { return _image ?? "";}
			set { this.RaiseAndSetIfChanged (ref _image, value);}
		}

		public string ImageUrl {
			get { return _imageUrl ?? "";}
			set { this.RaiseAndSetIfChanged (ref _imageUrl, value);}
		}

		public string Description {
			get { return _description ?? "";}
			set { this.RaiseAndSetIfChanged (ref _description, value);}
		}

		public string StartTime
		{
			get { return _startTime == DateTime.MinValue.ToString("yy-MM-dd") ? "" : _startTime; }
			set { this.RaiseAndSetIfChanged(ref _startTime, value);}
		}

		public string EndTime {
			get { return _endTime ?? "";}
			set { this.RaiseAndSetIfChanged (ref _endTime, value);}
		}

		public string UpdateTime {
			get { return _updateTime ?? "";}
			set { this.RaiseAndSetIfChanged (ref _updateTime, value);}
		}

		public List<Tuple<string,bool>> IconList {
			get { return _iconList;}
			set { this.RaiseAndSetIfChanged (ref _iconList, value);}
		}

		public EarningStatus Status {
			get { return _status == null ? EarningStatus.New : _status;}
			set { this.RaiseAndSetIfChanged (ref _status, value);}
		}

		public int Progress {
			get { return _progress == null ? 0 : _progress;}
			set { this.RaiseAndSetIfChanged (ref _progress, value);}
		}

		public string Archieve {
			get { return _archieve ?? "";}
			set { this.RaiseAndSetIfChanged (ref _archieve, value);}
		}

		public string Get {
			get { return _get ?? "";}
			set { this.RaiseAndSetIfChanged (ref _get, value);}
		}

		public string BriefDesc {
			get { return _briefDesc ?? "";}
			set { this.RaiseAndSetIfChanged (ref _briefDesc, value);}
		}

		public EarningManager EarningManager {
			get { return DependencyService.Get<EarningManager> ();}
		}

		public PerkManager PerkManager {
			get { return DependencyService.Get<PerkManager> ();}
		}

		public EarningDetailViewModel () : base()
		{
		}

		protected override void InitCommands()
		{
			BtnBackClicked = ReactiveCommand.Create ();
			LoadEarningCommand = ReactiveCommand.Create ();
			LoadPerkIconCommand = ReactiveCommand.Create ();
			SaveEarningCommand = ReactiveCommand.Create ();
//			LoadIconCommand = ReactiveCommand.Create ();
//			LoadImageCommand = ReactiveCommand.Create ();

			BtnBackClicked
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(_ => DependencyService.Get<INavigator>().NavigateBack(true));

			LoadEarningCommand
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Select (earningId => {
					return EarningManager.GetById(earningId.ToString());
				})
				.ObserveOn(RxApp.TaskpoolScheduler)
				.Subscribe(earning =>
					LoadEarning(earning));

			LoadPerkIconCommand
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Select (data => {
					var listTuple = (Tuple<List<string>, List<string>>)data;
					var iconList = new List<Tuple<string, bool>>();

					//use for add gainedPerk on top
					int count = 0;
					//

					foreach(var perkString in listTuple.Item1)
					{
						var perk = PerkManager.GetAvailablePerk(Convert.ToInt32(perkString));
						if(perk != null)
						{
							var icon = perk.Icon;
							if(listTuple.Item2.Contains(perkString))
								iconList.Insert(count++, Tuple.Create<string, bool>(icon, true));
							else
								iconList.Add(Tuple.Create<string, bool>(icon, false));
						}
						else
						{
							iconList.Insert(iconList.Count, Tuple.Create<string, bool>("", false));
						}
					}
					return iconList;})
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(iconList => {
					IconList = (List<Tuple<string, bool>>)iconList;
				});

//			LoadIconCommand
//				.Where (x => !String.IsNullOrEmpty ((string)x))
//				.ObserveOn (RxApp.TaskpoolScheduler)
//				.Select (url => {
//					if(!String.IsNullOrEmpty(_earning.IconURL))
//					{
//						var iconUrl =  ImageUtil.DownloadImage (_earning.IconURL, "icon_" + Name + EarningId).Result;
//						_earning.Icon = iconUrl;
//						EarningManager.Save(_earning);
//						return iconUrl;
//					}
//						return null;
//				})
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (imagePath => 
//					Icon = imagePath);
//
//			LoadImageCommand
//				.Where(x => !String.IsNullOrEmpty((string)x))
//				.ObserveOn (RxApp.TaskpoolScheduler)
//				.Select (url => {
//					var imageUrl = ImageUtil.DownloadImage ((string)url, "image_" + Name + EarningId).Result;
//					_earning.Image = imageUrl;
//					EarningManager.Save(_earning);
//					return imageUrl;
//				})
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (imagePath => 
//					Image = imagePath);

			SaveEarningCommand
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (earning => {
					EarningManager.Save((Earning)earning);
				});

		}

		private void LoadEarning(Earning earning)
		{
			_earning = earning;
			Name = earning.Name ;
			EarningId = earning.EarningId;
			Progress = 0;
			Progress = earning.Progress;
			Description = earning.Description;
			StartTime = earning.StartTime.ToLocalTime ().ToString ("yy-MM-dd");
			EndTime = earning.EndTime.ToLocalTime ().ToString ("yy-MM-dd");
			UpdateTime = earning.UpdateTime.ToLocalTime ().ToString ("yy-MM-dd");
			Status = earning.Status;
			Archieve = earning.Archieve;
			Get = earning.Get;
			BriefDesc = earning.BriefDesc;

			var imageManager = DependencyService.Get<CachedImageManager>();
			var image = imageManager.GetImage (new CachedImage () { LocalPath = earning.Icon, URL = earning.IconURL }, earning, "Icon");

			if (image.Cached)
				Icon = image.LocalPath;
			else {
				IconUrl = earning.IconURL;
			}
				

			image = imageManager.GetImage (new CachedImage () { LocalPath = earning.Image, URL = earning.ImageURL }, earning, "Image");

			if (image.Cached)
				Image = image.LocalPath;
			else {
				ImageUrl = earning.IconURL;
			}

			var perks = new List<string> (earning.AvailablePerks.Trim(' ').Split (new char[1] { ',' }));
			var gainedPerk = new List<string> (earning.GainedPerks.Trim(' ').Split (new char[1] { ',' }));

			if (perks.Count > 10)
				perks = perks.GetRange (0, 10);
			if (gainedPerk.Count > 10)
				gainedPerk = gainedPerk.GetRange (0, 10);

			LoadPerkIconCommand.Execute(Tuple.Create<List<string>, List<string>>(perks,gainedPerk));
		}
	}
}

