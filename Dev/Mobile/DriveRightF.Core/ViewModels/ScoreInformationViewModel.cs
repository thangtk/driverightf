﻿using System;
using DriveRightF.Core.Enums;

namespace DriveRightF.Core
{
	public class ScoreInformationViewModel : BaseHandlerHeaderViewModel
	{
		#region implemented abstract members of BaseHandlerHeaderViewModel

		public override int ScreenToNotify {
			get {
				return (int)Screen.TripStatementInfo;
			}
		}

		public override void UpdateHeaderFooter ()
		{
			HeaderViewModel.Title = Translator.Translate ("SCORE_INFORMATION");
			HeaderViewModel.IsShowBtnBack = true;

			TabBarViewModel.IsHidden = true;

		}

		#endregion

		public ScoreInformationViewModel () : base()
		{
		}
	}
}

