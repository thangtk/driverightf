﻿using System;
using System.Reactive.Linq;
using DriveRightF.Core;
using DriveRightF.Core.Enums;
using DriveRightF.Core.Managers;
using DriveRightF.Core.Models;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using Unicorn.Core;
using Unicorn.Core.Navigator;


namespace DriveRight.Core.ViewModels
{
	public partial class AwardDetailViewModel : BaseHandlerHeaderViewModel
    {
		public override int ScreenToNotify {
			get {
				return (int)Screen.AwardDetail;
			}
		}

		public override void UpdateHeaderFooter ()
		{
			HeaderViewModel.IsShowBtnBack = true;
			HeaderViewModel.Title = Translator.Translate("AWARD_DETAILS_HEADER");

			TabBarViewModel.IsHidden = true;
		}

		public AwardDetailViewModel() : base()
		{
			
		}

		protected override void Initalize ()
		{
			CheckBoxIsUseLabel = Translator.Translate("AWARD_DETAILS_USE_PERK");
			SponsoredByText = Translator.Translate("AWARD_DETAILS_SPONSOR_BY");
			ExpDateText = Translator.Translate("AWARD_DETAILS_EXPIRY_DATE");

			UpdateHeaderFooter ();

		}
		protected override void InitCommands()
        {
			LoadPerkCommand
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe (obj => LoadPerk ((Award)obj));

			SavePerkCommand
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (perk => SavePerk ((Award)perk));

            BtnBackClicked
                .ObserveOn(RxApp.MainThreadScheduler)
                .Subscribe(obj =>
                DependencyService.Get<INavigator> ().NavigateBack (true));
        }

		private void SavePerk(Award perk)
		{
//			PerkManager.Save (perk);
		}

		private void LoadPerk(Award award)
		{
		

			TxtAwardTitle = award.Name;
			TxtDescription = award.Description;

			TxtSponsoredName = "";
			TxtAwardExpireDate = "";


			this.SponsoredByText = award.Sponsor;
			this.ExpDateText = award.Expiry.ToLocalTime().ToString ("dd/MM/yyyy");
//			BriefDesc = perk.BriefDesc;

			var imageManager = DependencyService.Get<CachedImageManager>();
//			var image = imageManager.GetImage (new CachedImage () { LocalPath = perk.Icon, URL = perk.IconURL }, perk, "Icon");

//			if (image.Cached)
//				AwardIcon = image.LocalPath;
//			else {
//				IconUrl = perk.IconURL;
//			}
//
//			image = imageManager.GetImage (new CachedImage () { LocalPath = perk.Image, URL = perk.ImageURL }, perk, "Image");
//
//			if (image.Cached)
//				AwardBackgroundPicture = image.LocalPath;
//			else {
//				ImageUrl = perk.IconURL;
//			}

			ImageUrl = award.ImageURL;

//			AwardIcon = perk.Icon;
			//for Used raise anyway
//			AwardBackgroundPicture = perk.Image;
			Used = !Used;
			Used = award.Used;
			AwardQRNumber = award.QRCode;
			AwardIdLocal = award.Id.ToString();

			PerkCurrent = ValidatePerk (award);
		}

		private Award ValidatePerk(Award award)
		{

			if (award.Expiry == DateTime.MinValue)
				IsIndefinite = true;
			return award;
		}
    }
}
