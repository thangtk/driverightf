﻿using System;
using DriveRightF.Core.ViewModels;
using DriveRightF.Core.Enums;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

namespace DriveRightF.Core
{
	public class ItemNotificationViewModel : BaseViewModel
	{
		private Notification _item;
		private string _description;
		private string _updateTime;
		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get {
				return (int)Screen.Notification;
			}
		}

		public Notification Item {
			get
			{
				return _item;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _item, value);
			}
		}

		public string Description {
			get
			{
				return _description;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _description, value);
			}
		}

		public string UpdateTime {
			get
			{
				return _updateTime;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _updateTime, value);
			}
		}

		#endregion

		public ItemNotificationViewModel () : base()
		{
			this.WhenAnyValue (x => x.Item)
				.Where(x => x!= null)
			.Subscribe (x => {
					Description = Item.Description;
					UpdateTime = Item.UpdateTime.ToString("MM/dd/yyyy");
			});
		}
	}
}

