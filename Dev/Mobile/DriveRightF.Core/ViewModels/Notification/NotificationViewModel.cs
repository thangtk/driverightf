﻿using System;
using DriveRightF.Core.ViewModels;
using System.Collections.Generic;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using DriveRightF.Core.Enums;
using Unicorn.Core;
using Unicorn.Core.Navigator;

namespace DriveRightF.Core
{
	public class NotificationViewModel : BaseViewModel
	{
		private List<Notification> _items;
		private string _nameScreen, _iconHeader;
		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get {
				return (int)Screen.Notification;
			}
		}

		public List<Notification> Items {
			get
			{
				return _items;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _items, value);
			}
		}

		public string NameScreen {
			get
			{
				return _nameScreen;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _nameScreen, value);
			}
		}

		public string IconHeader {
			get
			{
				return _iconHeader;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _iconHeader, value);
			}
		}

		#endregion

		public IReactiveCommand<object> BtnBackClick { get; set; }

		void InitData ()
		{
			NameScreen = Translator.Translate ("NOTIFICATION");
			Items = new System.Collections.Generic.List<Notification> () {
				new Notification () {
					Description = Translator.Translate ("NOTIFICATION_DESC_1"),
					UpdateTime = DateTime.UtcNow.AddDays (-10),
				},
				new Notification () {
					Description = Translator.Translate ("NOTIFICATION_DESC_2"),
					UpdateTime = DateTime.UtcNow.AddDays (-19),
				},
				new Notification () {
					Description = Translator.Translate ("NOTIFICATION_DESC_2"),
					UpdateTime = DateTime.UtcNow.AddDays (-19),
				},
				new Notification () {
					Description = Translator.Translate ("NOTIFICATION_DESC_2"),
					UpdateTime = DateTime.UtcNow.AddDays (-19),
				}
			};
		}

		public NotificationViewModel ():base()
		{
			InitData ();
		}

		protected override void InitCommands ()
		{
			base.InitCommands ();

			BtnBackClick = ReactiveCommand.Create ();
			BtnBackClick
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (obj => DependencyService.Get<INavigator> ().NavigateBack ());
		}
	}
}

