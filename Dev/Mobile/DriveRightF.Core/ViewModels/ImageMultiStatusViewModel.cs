﻿using System;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using Unicorn.Core.Bases;
using Unicorn.Core;
using DriveRightF.Core.Storages;
using DriveRightF.Core.Enums;

namespace DriveRightF.Core
{
	public class ImageMultiStatusViewModel : BaseViewModel
	{
		private int _screenToNotify;
		public override int ScreenToNotify {
			get {
				return _screenToNotify;
			}
		}

		public ImageMultiStatusViewModel (int screenId) : base()
		{
			_screenToNotify = screenId;
		}

		private string _icon = string.Empty;
		public string Icon {
			get{ 
				return _icon;
			}
			set { 
				this.RaiseAndSetIfChanged (ref _icon, value);
			}
		}

		private string _iconUrl = string.Empty;
		public string IconURl {
			get { return _iconUrl;}
			set { this.RaiseAndSetIfChanged (ref _iconUrl, value);}
		}

		private string _statusIcon = string.Empty;
		public string StatusIcon {
			get{ 
				return _statusIcon;
			}
			private set { 
				this.RaiseAndSetIfChanged (ref _statusIcon, value);
			}
		}

		private ItemStatus _status;
		public ItemStatus Status {
			get{ 
				return _status;
			}
			set { 
				this.RaiseAndSetIfChanged (ref _status, value);
			}
		}

		private bool _isGrayStyle;
		public bool IsGrayStyle {
			get{ 
				return _isGrayStyle;
			}
			set{ 
				this.RaiseAndSetIfChanged(ref _isGrayStyle, value);
			}
		}

		protected override void InitCommands(){
			this.WhenAnyValue (vm => vm.Status)
				.Subscribe (s => {
					string statusIcon = "";
					switch(s){
					case ItemStatus.New:
						statusIcon = "icon_new.png";
						break;
					case ItemStatus.Modified:
						statusIcon = "icon_modified.png";
						break;
					case ItemStatus.Deleted:
						// For demo
						statusIcon = "icon_deleted.png";
						break;
					}
					StatusIcon = statusIcon;
				});
		}

//		public void RemoveItemStatus(IEntity model){
//			Status = ItemStatus.None;
//			DependencyService.Get<DriveRightRepository> ().Save (model);
//		}
	}
}

