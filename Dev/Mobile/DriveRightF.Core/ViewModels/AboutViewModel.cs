﻿using System;
using DriveRightF.Core.Enums;

namespace DriveRightF.Core
{
	public class AboutViewModel : BaseHandlerHeaderViewModel
	{
		#region implemented abstract members of BaseHandlerHeaderViewModel

		public override int ScreenToNotify {
			get {
				return (int)Screen.About;
			}
		}

		public override void UpdateHeaderFooter ()
		{
//			HeaderViewModel.Title = Translator.Translate ("ABOUT_HEADER");
//			HeaderViewModel.IsShowBtnBack = true;
		}

		#endregion

		public AboutViewModel () : base()
		{
		}
	}
}

