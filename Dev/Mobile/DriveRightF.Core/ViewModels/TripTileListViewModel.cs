﻿using System;
using System.Collections.Generic;
using Unicorn.Core;
using DriveRightF.Core.Managers;
using DriveRightF.Core.Models;
using DriveRightF.Core.Enums;
using ReactiveUI;
using System.Reactive.Linq;
using System.Linq;

namespace DriveRightF.Core.ViewModels
{
	public class TripTileListViewModel : BaseListTileVM<TripTileViewModel>
	{
		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get
			{
				return (int)Screen.Home;
			}
		}

		public override void UpdateHeaderFooter ()
		{
			HeaderViewModel.IsShowBtnBack = false;
			HeaderViewModel.Title = Translator.Translate("DRIVE_RIGHT_HEADER");

			TabBarViewModel.IsHidden = true;
		}


		#endregion

		public TripStatementManager TripStatementManager { 
			get { return DependencyService.Get<TripStatementManager> (); } 
		}

		public TripTileListViewModel() : base()
		{
//			List<TripMonth> trips = TripStatementManager.GetTripStatementsList ();
//
//			ConvertToTripListVM (trips);
//			NormalizeData (TileViewModels);
//
//			int i = 0;
//			int j = 0;
//			for (i = 0; i < TileViewModels.Count; i++, j++)
//			{
//				TileViewModels [i].Colors = _colorsList [j];
//
//				if (j + 1 == _colorsList.Count) 
//					j = -1;
//			}
			CreateDefaultData();
			GetDataFromDb.Execute (null);
		}

		protected override void InitCommands ()
		{
			base.InitCommands ();
			//
			GetDataFromDb = ReactiveCommand.CreateAsyncObservable (obj => {
				return Observable.Start (() => {
					var data = TripStatementManager.GetTripStatementsList();
					if(data != null){
						UpdateTripSummary(data);
					}
					return data;
				});
			});
			GetDataFromDb.Subscribe (x => {
				GetDataFromServer.Execute(null);
			});

			GetDataFromServer = ReactiveCommand.CreateAsyncObservable (obj => {
				return Observable.Start (() => {
					var data = TripStatementManager.SyncTripStatement();
					if (data != null && data.Count > 0) {
						UpdateTripSummary(data);
					}
					return data;
				});
			});
		}

		private const int MIN_ITEMS_COUNT = 15;

		private void CreateDefaultData(){
			TileViewModels = new List<TripTileViewModel> ();
			// Big tile
			TileViewModels.Add (new TripTileViewModel (){ 
				TileType = AwardType.AwardLarge,
				DateKey = new DateTime(9999,01,01),
				Trip = new TripMonth(){Month = new DateTime(9999,01,01)}
			});
			// Small tiles
			for (int i = 1; i < MIN_ITEMS_COUNT; i++) {
				TripMonth tm = null;
				var keyMonth = DateTime.Today.AddMonths (-i + 1);
				if (keyMonth.Year == DateTime.Today.Year) {
					tm = new TripMonth (){ Month = keyMonth };
				}
				//
				TileViewModels.Add (new TripTileViewModel (){ 
					TileType = AwardType.AwardSmall,
					DateKey = keyMonth,
					Trip = tm
				});
			}
		}

		private void UpdateTripSummary(List<TripMonth> data){
			if (data == null || data.Count == 0)
				return;
			//
			data.OrderByDescending(x => x.Month);
			var lastItem = data [data.Count - 1];
			DateTime smallestMonth;
			if (IsOverallStatement (lastItem)) {
				smallestMonth = new DateTime (DateTime.Today.Year, 1, 1);
			} else {
				smallestMonth = lastItem.Month;
			}

			for (int i = 0; i < TileViewModels.Count; i++) {
				int index = data.FindIndex (x => TileViewModels[i].DateKey.MonthDiff (x.Month)==0);
				if (index >= 0) {
					TileViewModels [i].Trip = data [index];
				} else if (TileViewModels [i].DateKey.MonthDiff (smallestMonth) < 0) {
					TileViewModels [i].Trip = null;
				} 
			}
		}

		private bool IsOverallStatement(TripMonth tm){
			return tm != null && tm.Month.Year == 9999 && tm.Month.Month == 1;
		}

		private void NormalizeData(List<TripTileViewModel> tiles)
		{
			int numberMissingTiles = 15 - tiles.Count;

			if (numberMissingTiles > 0) 
				AddMissingData (tiles, numberMissingTiles);
			else if (numberMissingTiles < 0)
			{
				numberMissingTiles = (int)((Math.Abs (numberMissingTiles)) % 3f);
				AddMissingData (tiles, numberMissingTiles);
			}
		}

		private void AddMissingData(List<TripTileViewModel> tiles, int numberMissingTiles)
		{
			for (int i = 0; i < numberMissingTiles; i++)
			{
				TripTileViewModel viewmodel = new TripTileViewModel () {
					Trip = new TripMonth(),
					TileType = AwardType.AwardSmall
				};
				tiles.Add (viewmodel);
			}
		}

		public IReactiveCommand<List<TripMonth>> GetDataFromDb { get; set; }

		public IReactiveCommand<List<TripMonth>> GetDataFromServer { get; set; }
	}
}

