﻿using System;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
[assembly: Unicorn.Core.Dependency.Dependency (typeof (DriveRightF.Core.HeaderViewModel))]
namespace DriveRightF.Core
{
	public class HeaderViewModel : BaseViewModel
	{
		private string _title;
		private string _iconProfile;
		private string _iconNotification;
		private bool _isShowBtnBack = false;
		private bool _isEnableBtnBack = true;
		private bool _isNavigateToHome = false;
		private bool _isShowBtnInfo = false;
		private bool _isShowBtnShare = false;
		private bool _isShowBtnNotification = false;

		private IReactiveCommand<object> _btnNotificationCommand =  ReactiveCommand.Create ();

		public IReactiveCommand<object> BtnNotificationCommand
		{
			get {
				return _btnNotificationCommand;
			}
			set{ 
				_btnNotificationCommand = value;
			}
		}

		private IReactiveCommand<object> _btnInfoClickCommand = ReactiveCommand.Create();

		public IReactiveCommand<object> BtnInfoClickCommand {
			get {
				return _btnInfoClickCommand;
			}
			set {
				_btnInfoClickCommand = value;
			}
		}

		private IReactiveCommand<object> _btnShareClickCommand = ReactiveCommand.Create();

		public IReactiveCommand<object> BtnShareClickCommand {
			get {
				return _btnShareClickCommand;
			}
			set {
				_btnShareClickCommand = value;
			}
		}

		public Action BtnInfoClickAction { get; set; }
		public Action BtnShareClickAction { get; set; }

		public bool IsShowBtnBack {
			get { return _isShowBtnBack; }

			set {
				this.RaiseAndSetIfChanged (ref _isShowBtnBack, value);
			}
		}

		public bool IsShowBtnInfo {
			get {
				return _isShowBtnInfo;
			}
			set {
				this.RaiseAndSetIfChanged (ref _isShowBtnInfo, value);
			}
		}

		public bool IsShowBtnShare {
			get {
				return _isShowBtnShare;
			}
			set {
				this.RaiseAndSetIfChanged (ref _isShowBtnShare, value);
			}
		}

		public bool IsShowBtnNotification {
			get {
				return _isShowBtnNotification;
			}
			set {
				this.RaiseAndSetIfChanged (ref _isShowBtnNotification, value);
			}
		}

		public bool IsEnableBtnBack {
			get {
				return _isEnableBtnBack;
			}
			set {
				this.RaiseAndSetIfChanged (ref _isEnableBtnBack, value);
			}
		}

		public bool IsNavigateToHome {
			get {
				return _isNavigateToHome;
			}
			set {
				_isNavigateToHome = value;
			}
		}

		public string IconProfile {
		
			get { return _iconProfile; }

			set {
				this.RaiseAndSetIfChanged (ref _iconProfile, value);
			}
		}

		public string IconNotification {
		
			get { return _iconNotification; }

			set {
				this.RaiseAndSetIfChanged (ref _iconNotification, value);
			}
		}

		public string Title {

			get { return _title; }

			set {
				this.RaiseAndSetIfChanged (ref _title, value);
			}
		}

		private int _numOfNewNotification;

		public int NumOfNewNotification {
			get { return _numOfNewNotification; } 
			set {
				this.RaiseAndSetIfChanged (ref _numOfNewNotification, value);
			}
		}

		public HeaderViewModel ()
		{
			BtnNotificationCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					if(x != null && x is Action)
					{
						(x as Action).Invoke();
					}
			});

			BtnInfoClickCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					if (BtnInfoClickAction != null)
						BtnInfoClickAction.Invoke ();
			});

			BtnShareClickCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					if (BtnShareClickAction != null)
						BtnShareClickAction.Invoke ();
				});
		}

		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get {
				throw new NotImplementedException ();
			}
		}

		#endregion
	}
}

