﻿using System;
using Unicorn.Core;
using ReactiveUI;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using DriveRightF.Core.Constants;

namespace DriveRightF.Core.ViewModels
{
	/// <summary>
	/// TODO: Refactor later
	/// </summary>
	public abstract class BaseViewModel : UBaseViewModel
	{
		private bool _isBlockUI = false;
		private bool _isBlockTabBar = false;

		public ErrorHandler ErrorHandler {
			get{ 
				return DependencyService.Get<ErrorHandler> ();
			}
		}

		public abstract int ScreenToNotify { get; }

		private ErrorReportType _errorDisplayingType = ErrorReportType.Dialog;
		protected ErrorReportType ErrorReportType {
			get { 
				return _errorDisplayingType;
			}
			set{ 
				_errorDisplayingType = value;
			}
		}

		//protected ICoinView CoinAnimationView {
		//	get { 
		//		return DependencyService.Get<ICoinView> ();
		//	}
		//}

		public bool IsBlockUI {
			get {
				return _isBlockUI;
			}
			set {
				_isBlockUI = value;
				MessagingCenter.Send<BaseViewModel> (this, MessageConstant.SCREEN_BLOCK);
			}
		}

		public bool IsBlockTabBar {
			get {
				return _isBlockTabBar;
			}
			set {
				_isBlockTabBar = value;
				MessagingCenter.Send<BaseViewModel> (this, MessageConstant.TAB_BAR_BLOCK);
			}
		}

		public BaseViewModel () : base()
		{
			InitViewModel ();
		}

//		public BaseViewModel(object param) : base()
//		{
//			InitViewModel (param);
//		}

		private void InitViewModel(){
			Initalize ();
			InitCommands ();
			HandleExReactiveCommands ();
			//
//			this.WhenAnyValue (vm => vm.IsBusy)
//				.Skip(1)
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (b => {
//					if(b){
//						PopupManager.ShowLoading();
//					} else{
//						PopupManager.HideLoading();
//					}
//				});
		}

		/// <summary>
		/// It's invoked before InitCommands
		/// </summary>
		protected virtual void Initalize(){
		}

		protected virtual void InitCommands (){}

		private void HandleExReactiveCommands(){
			var properties = this.GetType ().GetProperties ().Where(p => typeof(IReactiveCommand).IsAssignableFrom(p.PropertyType));
			if (properties == null) {
				return;
			}
			foreach (var p in properties) {
				var command = (IReactiveCommand)p.GetValue (this) ;
				if (command == null)
					continue;
				command.ThrownExceptions.Subscribe (ex => {
					switch (ErrorReportType) {
					case ErrorReportType.Dialog:
						ErrorHandler.Handle (ex, ScreenToNotify);
						break;
					case ErrorReportType.None:
						ErrorHandler.Handle (ex, "ReactiveCommand");
						break;
						// TODO: other case
					}
				});

			}
		}

		protected bool _isBusy;

		public bool IsBusy {
			get { return _isBusy; } 
			set {
				this.RaiseAndSetIfChanged (ref _isBusy, value);
			}
		}
	}
}

