﻿using System;
using Unicorn.Core.Translation;
using Unicorn.Core;
using System.Collections.Generic;
using ReactiveUI;
using System.Drawing;

namespace DriveRightF.Core
{
	public abstract class BaseMetroViewModel<T> : BaseHandlerHeaderViewModel where T: ITile
	{
		protected List<string> _colorsList = ColorUtils.GetColorList ();

		public ITranslator Translator {get {return DependencyService.Get<ITranslator>();}}

		public BaseMetroViewModel()
		{
		}

		private List<T> _tileViewModel;

		public List<T> TileViewModels {
			get { return _tileViewModel; } 
			set
			{
				this.RaiseAndSetIfChanged (ref _tileViewModel, value);
			}
		}

		private int _currentIndex;

		public int CurrentIndex {
			get { return _currentIndex; }
			set { this.RaiseAndSetIfChanged (ref _currentIndex, value); }
		}

	}
}

