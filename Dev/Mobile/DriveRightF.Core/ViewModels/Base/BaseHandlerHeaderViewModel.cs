﻿using System;
using DriveRightF.Core.ViewModels;
using Unicorn.Core;
using Unicorn.Core.Translation;

namespace DriveRightF.Core
{
	public abstract class BaseHandlerHeaderViewModel : BaseViewModel
	{
		public HeaderViewModel HeaderViewModel {
			get{ 
				return DependencyService.Get<HeaderViewModel> ();
			}
		}

		public UTabBarViewModel TabBarViewModel {
			get{ 
				return DependencyService.Get<UTabBarViewModel> ();
			}
		}

		public BaseHandlerHeaderViewModel () : base()
		{
			UpdateHeaderFooter ();
		}
		public override int ScreenToNotify { get { return -1; } }
		public abstract void UpdateHeaderFooter ();
	}
}

