﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using Unicorn.Core.Translation;
using Unicorn.Core;

namespace DriveRightF.Core
{
	public abstract class BaseListTileVM<T> : BaseHandlerHeaderViewModel where T: ITile
	{
		protected List<string> _colorsList = ColorUtils.GetColorList ();

		public ITranslator Translator {get {return DependencyService.Get<ITranslator>();}}

		public BaseListTileVM()
		{
		}

		private List<T> _tileViewModel;

		public List<T> TileViewModels {
			get { return _tileViewModel; } 
			set
			{
				this.RaiseAndSetIfChanged (ref _tileViewModel, value);
			}
		}

		private int _currentIndex;

		public int CurrentIndex {
			get { return _currentIndex; }
			set { this.RaiseAndSetIfChanged (ref _currentIndex, value); }
		}

//		public Size ContentSize { get; set; }

		public void ComputeTileFrame(Size contentSize)
		{
			// TODO: Compute tiles's frame
			int smallWidth = contentSize.Width / 3;
			int largeWidth = contentSize.Width * 2 / 3;

			int currentX = 0;
			int currentY = 0;

			int direction = 0;	// 0: left to right, 1: right to left
			float delta = 0;

			for (int i = 0; i < TileViewModels.Count; i++)
			{
				switch (i)
				{
					case 0:
						TileViewModels [i].Size = new Size ((int)(largeWidth - 1), (int)(largeWidth - 1));
						TileViewModels [i].Position = new Point (1, currentY + smallWidth + 1);
						break;
					case 1:
						TileViewModels [i].Size = new Size ((int)(smallWidth), (int)(smallWidth - 0.5f));
						TileViewModels [i].Position = new Point (1, currentY + 1);
						break;
					case 2:
						TileViewModels [i].Size = new Size ((int)(smallWidth - 0.5f), (int)(smallWidth - 0.5f));
						TileViewModels [i].Position = new Point (smallWidth + 2, currentY + 1);
						break;
					case 3:
						TileViewModels [i].Size = new Size ((int)(smallWidth - 0.5f), (int)(smallWidth - 0.5f));
						TileViewModels [i].Position = new Point (2 * smallWidth + 2, currentY + 1);
						break;
					case 4:
						TileViewModels [i].Size = new Size ((int)(smallWidth - 0.5f), (int)(smallWidth - 0.5f));
						TileViewModels [i].Position = new Point (2 * smallWidth + 2, currentY + smallWidth + 1);
						break;
					case 5:
						TileViewModels [i].Size = new Size ((int)(smallWidth - 0.5f), (int)(smallWidth));
						TileViewModels [i].Position = new Point (2 * smallWidth + 2, currentY + 2 * smallWidth + 1);

						currentX = 2 * smallWidth + 2;
						currentY += 3 * smallWidth + 2;

						direction = 1;
						break;
					default:
						if (currentX < smallWidth)
						{
							currentX = 1;
							delta = 0.5f;
						}
						else if (currentX < 2 * smallWidth)
						{
							currentX = contentSize.Width / 3 + 1;
							delta = 0f;
						}
						else
						{
							currentX = contentSize.Width * 2 / 3 + 1;
							delta = 0.5f;
						}

						TileViewModels [i].Size = new Size ((int)(smallWidth - delta), (int)(smallWidth - 0.5f));
						TileViewModels [i].Position = new Point (currentX, (int)(currentY + 0.5f));

						if ((Math.Round((double)(currentX / smallWidth)) == 0f && direction == 1) || (Math.Round((double)(currentX / smallWidth)) == 2f && direction == 0))
						{
							currentY += (int)(smallWidth + 0.5f);
							direction = 1 - direction;
						}
						else
						{
							if (direction == 0) 
								currentX += smallWidth;
							else if (direction == 1) 
								currentX -= smallWidth;
						}
						break;
				}
			}
			UpdateColors();
		}

		public void UpdateColors ()
		{
			int i = 0;
			int j = 0;
			for (i = 0; i < TileViewModels.Count; i++, j++) {
				TileViewModels [i].Colors = _colorsList [j];
				if (j + 1 == _colorsList.Count)
					j = -1;
			}
		}
	}
}

