﻿using System;
using System.Collections.Generic;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

namespace DriveRightF.Core
{
	public abstract class BaseHomeListViewModel<TCellModel> : BaseHandlerHeaderViewModel
	{
		private List<TCellModel> _dataSources = new List<TCellModel>();
//		public TBannerViewModel BannerViewModel {
//			get;
//			set;
//		}

		public BannerViewModel BannerViewModel {
			get;
			set;
		}

		public List<TCellModel> DataSources {
			get {
				return _dataSources;
			}
			set {
				this.RaiseAndSetIfChanged (ref _dataSources, value);
			}
		}

		public BaseHomeListViewModel ()
		{
//			BannerViewModel = new TBannerViewModel ();
		}


	}
}

