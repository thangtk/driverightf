﻿using System;
using DriveRightF.Core.Enums;
using ReactiveUI;
using System.Reactive.Linq;
using DriveRightF.Core.Models;
using Unicorn.Core;
using DriveRightF.Core.Managers;
using System.Reactive.Disposables;
using Unicorn.Core.Services.Base;
using System.Diagnostics;

namespace DriveRightF.Core.ViewModels
{
	public class TripStatementViewModel : BaseHandlerHeaderViewModel
	{
		#region implemented abstract members of BaseViewModel

		public TripStatementManager TripStatementManager
		{
			get {
				return DependencyService.Get<TripStatementManager> ();
			}

		}

		public override int ScreenToNotify {
			get
			{
//				return (int)Screen.TripStatement;
				return -1;
			}
		}

		#endregion

		public override void UpdateHeaderFooter()
		{
			HeaderViewModel.IsShowBtnBack = true;
			HeaderViewModel.IsShowBtnInfo = true;
			HeaderViewModel.IsShowBtnShare = true;
			HeaderViewModel.Title = Translator.Translate("TRIPS_STATEMENT_HEADER");

			TabBarViewModel.IsHidden = false;
			TabBarViewModel.ItemSelected = -1;
		}

		public TripStatementViewModel() :base()
		{
			ErrorReportType = ErrorReportType.None;
		}

		#region Properties and commands

		// Smooth, speed, distance, time of delay

		private DateTime _month;

		public DateTime Month {
			get	{ return _month; }
			set	{ this.RaiseAndSetIfChanged (ref _month, value); }
		}

		private double _smoothScore;

		public double SmoothScore {
			get { return _smoothScore; }
			set { this.RaiseAndSetIfChanged (ref _smoothScore, value); }
		}

		private double _speedScore;

		public double SpeedScore {
			get { return _speedScore; }
			set { this.RaiseAndSetIfChanged (ref _speedScore, value); }
		}

		private double _distanceScore;

		public double DistanceScore {
			get { return _distanceScore; }
			set { this.RaiseAndSetIfChanged (ref _distanceScore, value); }
		}

		private double _timeOfDelay;

		public double TimeOfDelay {
			get { return _timeOfDelay; }
			set { this.RaiseAndSetIfChanged (ref _timeOfDelay, value); }
		}

		private double _TotalScore;

		public double TotalScore {
			get { return _TotalScore; }
			set { this.RaiseAndSetIfChanged (ref _TotalScore, value); }
		}

		private string _totalDistance;

		public string TotalDistance {
			get { return _totalDistance ?? "--"; }
			set { this.RaiseAndSetIfChanged (ref _totalDistance, value); }
		}

		private string _totalDuration;

		public string TotalDuration {
			get { return _totalDuration ?? "--"; }
			set { this.RaiseAndSetIfChanged (ref _totalDuration, value); }
		}

		private string _noOfTrips;

		public string NoOfTrips {
			get { return _noOfTrips ?? "--"; }
			set { this.RaiseAndSetIfChanged (ref _noOfTrips, value); }
		}

		private string _avgDistance;

		public string AvgDistance {
			get { return _avgDistance ?? "--"; }
			set { this.RaiseAndSetIfChanged (ref _avgDistance, value); }
		}

		private string _avgDuration;

		public string AvgDuration {
			get { return _avgDuration ?? "--"; }
			set { this.RaiseAndSetIfChanged (ref _avgDuration, value); }
		}

		private string _avgSpeed;

		public string AvgSpeed {
			get { return _avgSpeed ?? "--"; }
			set { this.RaiseAndSetIfChanged (ref _avgSpeed, value); }
		}

		private IReactiveCommand<object> _infoCommand;

		public IReactiveCommand<object> BtnInfoClickCommand {
			get { return _infoCommand; }
			set { this.RaiseAndSetIfChanged (ref _infoCommand, value); }
		}

		private IReactiveCommand<object> _btnDetailClickCommand;

		public IReactiveCommand<object> BtnDetailClickCommand {
			get	{ return _btnDetailClickCommand; }
			set	{ this.RaiseAndSetIfChanged (ref _btnDetailClickCommand, value); }
		}

		#endregion

//		public IReactiveCommand<TripMonth> GetDataFromDb { get; set; }

		public IReactiveCommand<TripMonth> LoadDataCommand { get; set; }

		protected override void InitCommands()
		{
			base.InitCommands ();

			this.WhenAnyValue (vm => vm.Month)
				.Skip(1)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe (x => {
					ResetToDefault();
					LoadDataCommand.Execute (x);
				});
			// Load data
			LoadDataCommand = ReactiveCommand.CreateAsyncObservable (obj => {
				PopupManager.ShowLoading("",Translator.Translate ("SYNCING_DATA")); // Loading with message has not implemted yet. Please wait next version
				return Observable.Start(() => TripStatementManager.GetTripStatementFromServer((DateTime)obj));
			});
			LoadDataCommand.ObserveOn(RxApp.MainThreadScheduler).Subscribe (x => {
				LoadLocalData();
				PopupManager.HideLoading();
			});
			LoadDataCommand.ThrownExceptions.ObserveOn(RxApp.MainThreadScheduler).Subscribe (ex => {
				string message;
				if (ex is ServiceException) {
					message = (ex as ServiceException).ErrorMessage;
				} else {
					message = ex.Message;
				}
				//
				PopupManager.HideLoading();
				PopupManager.ShowAlert (Translator.Translate ("ERROR"), message, Unicorn.Core.Enums.DialogIcon.None, true,LoadLocalData,Translator.Translate("BUTTON_OK"));
			});
			//
			BtnDetailClickCommand = ReactiveCommand.Create ();
			BtnDetailClickCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (action => {
					if (action != null) 
						((Action)action).Invoke ();
			});
		}

		/// <summary>
		/// Load data from database and set them to properties
		/// </summary>
		private void LoadLocalData(){
			// This version block UI so user cannot change Month property before completion
			var data = TripStatementManager.GetTripStatementLocal(Month);
			if(data != null){
				SetTripStatementData(data);
			}
		}

		private void ResetToDefault ()
		{
			TotalScore = 0;
			SmoothScore = 0;
			SpeedScore = 0;
			DistanceScore = 0;
			TimeOfDelay = 0;
			TotalDistance = "--" + Translator.Translate ("TRIPS_STATEMENT_KM");
			TotalDuration = "--" + Translator.Translate ("TRIPS_STATEMENT_H");
			NoOfTrips = "--";
			AvgDistance = "--" + Translator.Translate ("TRIPS_STATEMENT_KM");
			AvgSpeed = "--" + Translator.Translate ("TRIPS_STATEMENT_KM/H");
			AvgDuration = "--" + Translator.Translate ("MINUTES");
		}

		private void SetTripStatementData (TripMonth trip)
		{
			System.Diagnostics.Debug.WriteLine ("SetTripStatementData >>> Trip month: " + trip.Month.ToString("yyyy-MMM") + " >>> Month: " + Month.ToString("yyyy-MMM"));
			if (trip != null) {
				if (Month.MonthDiff (trip.Month) != 0) {
					return;
				}
//				Month = trip.Month;
				TotalScore = Math.Round( trip.Score,0);
				SmoothScore =  Math.Round(trip.SmoothScore,0);
				SpeedScore = Math.Round(trip.SpeedScore,0);
				DistanceScore = Math.Round(trip.DistractionScore,0);
				TimeOfDelay = Math.Round(trip.TimeOfDayScore, 0);
				TotalDistance = Math.Round(trip.TotalDistance, 1).ToString () + Translator.Translate ("TRIPS_STATEMENT_KM");
				TotalDuration = Math.Round (trip.TotalDuration / 3600000f, 1).ToString () + Translator.Translate ("TRIPS_STATEMENT_H");
				NoOfTrips = trip.TripCount.ToString ();
				AvgDistance = Math.Round (trip.AverageDistance, 1).ToString () + Translator.Translate ("TRIPS_STATEMENT_KM");
				AvgSpeed = Math.Round (trip.AverageSpeed, 1).ToString () + Translator.Translate ("TRIPS_STATEMENT_KM/H");
				if (trip.AverageDuration > 3600000f)
					AvgDuration = Math.Round (trip.AverageDuration / 3600000f, 1).ToString () + Translator.Translate ("TRIPS_STATEMENT_H");
				else
					AvgDuration = Math.Round (trip.AverageDuration / 60000, 1).ToString () + Translator.Translate ("MINUTES");
			} else {
				ResetToDefault ();
			}
		}
	}
}

