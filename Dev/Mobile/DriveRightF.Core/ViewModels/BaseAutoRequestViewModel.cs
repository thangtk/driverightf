﻿using System;
using Unicorn.Core;
using DriveRightF.Core.Enums;
using ReactiveUI;
using Unicorn.Core.Bases;
using System.Collections.Generic;
using System.Reactive.Linq;
using DriveRightF.Core.ViewModels;

namespace DriveRightF.Core
{
	public abstract class BaseAutoRequestViewModel<T> : BaseViewModel where T: BaseModel, new()
	{
		private string _headerTitle;
		private string _headerLogo;

		public string HeaderTitle {
			get { return _headerTitle;}
			set { this.RaiseAndSetIfChanged (ref _headerTitle, value);}
		}
		public string HeaderLogo {
			get { return _headerLogo;}
			set { this.RaiseAndSetIfChanged (ref _headerLogo, value);}
		}

		public BaseAutoRequestViewModel () : base()
		{
			
		}

		public virtual string Name {
			get { 
				return "ViewModel Name";
			}
		}

		public abstract BaseAutoRequestManager<T> Manager { get; }

		private List<T> _cachedData;

		private ViewState _state = ViewState.None;

		public ViewState State {
			get { return _state; }
			set { 
				this.RaiseAndSetIfChanged (ref _state, value);
			}
		}

		private bool _manualRequest = false;

//		public bool AutoRequestSuccess { get; set; }

		private List<T> _newData;

		public List<T> NewData {
			get { 
				return _newData;
			}
			set { 
				this.RaiseAndSetIfChanged (ref _newData, value);
			}
		}

		private bool _requesting;

		public bool Requesting {
			get { 
				return _requesting;
			}
			set { 
				this.RaiseAndSetIfChanged (ref _requesting, value);
			}
		}

		private bool _hasNewData;

		public bool HasNewData {
			get {
				return _hasNewData;
			}
			set {
				this.RaiseAndSetIfChanged (ref _hasNewData, value);
				if (value == false && NewData != null && NewData.Count > 0) {
					// After update UI
					// Add data to Model List???
					NewData.Clear ();
				}
			}
		}

		private bool _contentViewHidden;

		public bool ContentViewHidden {
			get { 
				return _contentViewHidden;
			}
			set { 
				this.RaiseAndSetIfChanged (ref _contentViewHidden, value);
			}
		}

		private bool _noDataViewHidden;

		public bool NoDataViewHidden {
			get { 
				return _noDataViewHidden;
			}
			set { 
				this.RaiseAndSetIfChanged (ref _noDataViewHidden, value);
			}
		}

		private bool _requestingViewHidden;

		public bool RequestingViewHidden {
			get { 
				return _requestingViewHidden;
			}
			set { 
				this.RaiseAndSetIfChanged (ref _requestingViewHidden, value);
			}
		}

		private IReactiveCommand<bool> _checkAutoRequest;

		private IReactiveCommand<List<T>> _requestNewData;

		private IReactiveCommand<List<T>> _loadDataFromDb;

		private IReactiveCommand<object> _appendNewdata;

		private IReactiveCommand<object> _backCommand;

		public IReactiveCommand<bool> CheckAutoRequest {
			get {
				return _checkAutoRequest;
			}
			set {
				_checkAutoRequest = value;
			}
		}

		public IReactiveCommand<List<T>> RequestNewData {
			get {
				return _requestNewData;
			}
			set {
				_requestNewData = value;
			}
		}
		public IReactiveCommand<List<T>> LoadDataFromDb {
			get {
				return _loadDataFromDb;
			}
			set {
				_loadDataFromDb = value;
			}
		}
		public IReactiveCommand<object> AppendNewdata {
			get {
				return _appendNewdata;
			}
			set {
				_appendNewdata = value;
			}
		}
		public IReactiveCommand<object> BackCommand {
			get {
				return _backCommand;
			}
			set {
				_backCommand = value;
			}
		}
		private List<T> _dataList;

		public List<T> DataList {
			get { 
				return _dataList;
			}
			set { 
				this.RaiseAndSetIfChanged (ref _dataList, value);
				System.Diagnostics.Debug.WriteLine ("DataList >>> RaiseAndSetIfChanged");
			}
		}

		protected override void InitCommands ()
		{
			LoadDataFromDb = ReactiveCommand.CreateAsyncObservable (obj => Observable.Start (() => Manager.GetDataFromDb ()));
			CheckAutoRequest = ReactiveCommand.CreateAsyncObservable (obj => Observable.Start (() => Manager.CheckAutoRequest ()));
			RequestNewData = ReactiveCommand.CreateAsyncObservable (obj => {
				_manualRequest = true.Equals (obj);
				Requesting = true;
				// TODO:
				return Observable.Start (() => Manager.GetDataFromServer ());
			});

			LoadDataFromDb
				.Subscribe (
				data => UpdateData (data));
			//
			CheckAutoRequest
				.Where (b => b)
				.Subscribe (b => RequestNewData.Execute (false));
			//
			RequestNewData.Subscribe (data => {
				Requesting = false;
				_manualRequest = false;
//				AutoRequestSuccess = true;
				//
				UpdateData (data);
			});
			// Combine exception handling
			RequestNewData.ThrownExceptions.Subscribe (ex => {
				Requesting = false;
				_manualRequest = false;
			});
			//
//			this.WhenAnyValue (vm => vm.DataList, vm => vm.Requesting, (t, r) => {
//				if ((t != null && t.Count > 0)) {
//					return ViewState.DataDisplaying;
//				} else if (r && !_manualRequest) {
//					return ViewState.Requesting;
//				} else {
//					return ViewState.NoDataFound;
//				}
//			}).Subscribe (s => State = s);
			//
//			this.WhenAnyValue(vm => vm.DataList).Subscribe(x => System.Diagnostics.Debug.WriteLine ("DataList >>> WhenAnyValue"));
			this.ObservableForProperty(vm => vm.DataList).Subscribe(x => {
				if ((x.Value != null && x.Value.Count > 0)) {
					State = ViewState.DataDisplaying;
					ErrorReportType = ErrorReportType.None;
				} else{
					ErrorReportType = ErrorReportType.Dialog;
					if (Requesting && !_manualRequest) {
						State = ViewState.Requesting;
					} else {
						State = ViewState.NoDataFound;
					}
				}
			});

			this.ObservableForProperty(vm => vm.Requesting).Subscribe(x => {
				if ((DataList != null && DataList.Count > 0)) {
					State = ViewState.DataDisplaying;
				} else if (x.Value && !_manualRequest) {
					State = ViewState.Requesting;
				} else {
					State = ViewState.NoDataFound;
				}
			});

//			this.WhenAny(vm => vm.DataList, x => x.Value).Subscribe(x => System.Diagnostics.Debug.WriteLine ("DataList >>> WhenAny"));
			//
			this.WhenAnyValue (vm => vm.State)
				.Subscribe (s => {
				System.Diagnostics.Debug.WriteLine (">>> Status: " + s.ToString ());
				switch (s) {
				case ViewState.None:
					ContentViewHidden = true;
					NoDataViewHidden = true;
					RequestingViewHidden = true;
					break;

				case ViewState.DataDisplaying:
					ContentViewHidden = false;
					NoDataViewHidden = true;
					RequestingViewHidden = true;
					break;

				case ViewState.NoDataFound:
					ContentViewHidden = true;
					NoDataViewHidden = false;
					RequestingViewHidden = true;
					break;

				case ViewState.Requesting:
					ContentViewHidden = true;
					NoDataViewHidden = true;
					RequestingViewHidden = false;
					break;
				}
			});

			AppendNewdata = ReactiveCommand.Create ();
			AppendNewdata
				.Where (b => true.Equals (b))
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (obj => {
				NewData = _cachedData;
				_cachedData = null;
			});
			//
			BackCommand = ReactiveCommand.Create ();
			BackCommand.Subscribe (obj => Navigator.NavigateBack ());
		}

		private void UpdateData (List<T> data)
		{
			bool exist = data != null && data.Count > 0;
			if (exist) {
				if (DataList == null || DataList.Count == 0) {
					DataList = data;
				} else {
					_cachedData = data;
					HasNewData = exist;
				}
			}
		}

		public void LoadData ()
		{
			ResetBeforeLoadData ();
			//
			LoadDataFromDb.Execute (null);
			DoAutoRequest ();
		}

		public void DoAutoRequest(){
			if (!Requesting) {
				CheckAutoRequest.Execute (null);
			}
		}

		protected virtual void ResetBeforeLoadData (){}
	}
}
