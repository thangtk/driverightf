﻿using System;
using ReactiveUI;
using System.Collections.Generic;
using System.Drawing;
using DriveRightF.Core.Enums;
using DriveRightF.Core.Models;
using System.Reactive.Linq;
using System.Reactive.Concurrency;

namespace DriveRightF.Core.ViewModels
{
	public class TripTileViewModel : BaseViewModel, ITile
	{
		private string _image;
		private string _overallImage;
		private string _colors;
		private AwardType _type;

		private TripMonth _trip;

		public Size Size { get; set; }

		public Point Position { get; set; }

//		public Color BackgroundColor { get; set; }

		public TripMonth Trip {
			get
			{
				return _trip;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _trip, value);
			}
		}

		public string Image {
			get
			{
				return _image;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _image, value);
			}
		}

		public string OverallImage {
			get
			{
				return _overallImage;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _overallImage, value);
			}
		}

		public string Colors {
			get
			{
				return _colors;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _colors, value);
			}
		}

		public AwardType TileType {
			get
			{
				return _type;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _type, value);
			}
		}

		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get
			{
				return (int)Screen.HomeTrips;
			}
		}

		#endregion

		public TripTileViewModel() : base ()
		{
			TileType = AwardType.AwardSmall;
//			this.WhenAnyValue (x => Trip)
//				.Where (x => x != null)
//				.Subscribe (x => {
//					Image = x.ImageUrl;
//			});
		}

		public DateTime DateKey { get; set; }
	}
}

