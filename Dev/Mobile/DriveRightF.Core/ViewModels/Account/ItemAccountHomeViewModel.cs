﻿using System;
using ReactiveUI;
using System.Collections.Generic;
using System.Drawing;
using DriveRightF.Core.Models;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Globalization;

namespace DriveRightF.Core.ViewModels
{
	public class ItemAccountHomeViewModel : BaseViewModel
	{
		private Account _account;
		private string _cardsEarnedValue;
		private string _cardsEarnedTitle;
		private string _cashsEarnedTitle;
		private string _cashsEarnedValue;
		private DateTime _month;

		public string CardsEarnedValue {
			get
			{
				return _cardsEarnedValue;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _cardsEarnedValue, value);
			}
		}

		public string CashsEarnedValue {
			get
			{
				return _cashsEarnedValue;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _cashsEarnedValue, value);
			}
		}

		#region Add property for V4

		public string CardsEarnTitle {
			get
			{
				return _cardsEarnedTitle;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _cardsEarnedTitle, value);
			}
		}

		public string CashsEarnTitle {
			get
			{
				return _cashsEarnedTitle;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _cashsEarnedTitle, value);
			}
		}

		public Account Account {
			get
			{
				return _account;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _account, value);
			}
		}

		public DateTime Month {
			get
			{
				return _month;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _month, value);
			}
		} 

		#endregion


		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get
			{
				throw new NotImplementedException ();
			}
		}

		#endregion

		public ItemAccountHomeViewModel() : base ()
		{
			//TODO: add for V4
			CardsEarnTitle = Translator.Translate ("ACCOUNT_CARDS_EARNED");
			CashsEarnTitle = Translator.Translate ("ACCOUNT_CASH_EARNED");

//			this.WhenAnyValue (x => x.Account)
//				.Where (x => x != null)
//				.ObserveOn (RxApp.TaskpoolScheduler)
//				.Subscribe (x => {
//					Month = DateTime.ParseExact(x.Month, "yyyy-MM", CultureInfo.InvariantCulture);
//					CardsEarnedValue = x.CardEarned.ToString();
//					CashsEarnedValue = x.CashEarned.ToString();
//				});
		}
	}
}

