﻿using System;
using ReactiveUI;
using System.Collections.Generic;
using System.Drawing;

namespace DriveRightF.Core.ViewModels
{
	public class BigItemAccountViewModel : ItemAccountViewModel
	{
		private string _balanceValue;

		public string BalanceValue {
			get
			{
				return _balanceValue;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _balanceValue, value);
			}
		}

		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get
			{
				throw new NotImplementedException ();
			}
		}

		#endregion

		public BigItemAccountViewModel() : base ()
		{
		}
	}
}

