﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReactiveUI;
using DriveRightF.Core.Models;
using DriveRightF.Core.Managers;
using Unicorn.Core;
using Unicorn.Core.UI;

namespace  DriveRight.Core.ViewModels
{
	public partial class AccountDetailViewModel
	{
		private float _balanceCashValue;
		private float _premiumAccountValue;
		private float _withDrawCashValue;
		private float _premiumDiscountValue;
		private float _withDrawCashPercent;
		private float _premiumDiscountPercent;
		private string _cashInputValue;

		private IReactiveCommand<object> _btnPayClick;

		private IReactiveCommand<object> _btnGetDiscountClick;

		private IReactiveCommand<object> _loadDataCommand;

		private IReactiveCommand<object> _loadAccountData;

		public IReactiveCommand<object> LoadAccountData {
			get
			{
				return _loadAccountData;
			}
			set
			{
				_loadAccountData = value;
			}
		}

		public IReactiveCommand<object> LoadDataCommand {
			get {
				return _loadDataCommand;
			}
			set{ 
				_loadDataCommand = value;
			}
		}

		public IReactiveCommand<object> BtnPayClick {
			get
			{
				return _btnPayClick;
			}
			private set
			{
				this.RaiseAndSetIfChanged (ref _btnPayClick, value);
			}
		} 

		public IReactiveCommand<object> BtnGetDiscountClick {
			get
			{
				return _btnGetDiscountClick;
			}
			private set
			{
				this.RaiseAndSetIfChanged (ref _btnGetDiscountClick, value);
			}
		} 

		public float WithDrawCashPercent {
			get {
				return _withDrawCashPercent;
			}
			set {
				this.RaiseAndSetIfChanged (ref _withDrawCashPercent, value);
			}
		}

		public float PremiumDiscountPercent {
			get {
				return _premiumDiscountPercent;
			}
			set {
				this.RaiseAndSetIfChanged (ref _premiumDiscountPercent, value);
			}
		}

		public float BalanceCashValue {
			get {
				return _balanceCashValue;
			}
			set {
				this.RaiseAndSetIfChanged (ref _balanceCashValue, value);
			}
		}


		public float PremiumAccountValue {
			get {
				return _premiumAccountValue;
			}
			set {
				this.RaiseAndSetIfChanged (ref _premiumAccountValue, value);
			}
		}


		public float WithDrawCashValue {
			get {
				return _withDrawCashValue;
			}
			set {
				this.RaiseAndSetIfChanged (ref _withDrawCashValue, value);
			}
		}

		public string CashInputValue
		{
			get {
				return _cashInputValue;
			}
			set {
				this.RaiseAndSetIfChanged (ref _cashInputValue, value);
			}
		}


		public float PremiumDiscountValue {
			get {
				return _premiumDiscountValue;
			}
			set {
				this.RaiseAndSetIfChanged (ref _premiumDiscountValue, value);
			}
		}

		public PopupManager PopupManager{
			get { 
				return DependencyService.Get<PopupManager> ();
			}
		}
	}
}

