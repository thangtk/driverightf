﻿using System;
using DriveRightF.Core.Models;
using Unicorn.Core;
using DriveRightF.Core.Constants;
using DriveRightF.Core.Managers;
using DriveRight.Core.ViewModels;

namespace DriveRightF.Core
{
	public class AccountContentViewModel : HomeListContentViewModel<Account>
	{
		
		private AccountManager AccountManager
		{
			get {
				return DependencyService.Get<AccountManager> ();
			}
		}

		public AccountContentViewModel ( BannerViewModel bannerViewModel) : base()
		{
			//TODO: get data to BannerViewmodel and source.
			NoDataContent = Translator.Translate("ACCOUNT_NO_DATA");
			BannerViewModel = bannerViewModel;

			ReloadBalance ();

			InitMessage ();
		}

		void InitMessage ()
		{
			// subscribe from Card Detail ViewModel
			MessagingCenter.Subscribe<Award> (this, MessageConstant.CARD_TAKE_MONEY, x =>  {
				ReloadBalance ();
			});
			MessagingCenter.Subscribe<Award> (this, MessageConstant.CARD_GAMBLE, x =>  {
				ReloadBalance ();
			});
			// subscribe from Account Detail ViewModel
			MessagingCenter.Subscribe<AccountDetailViewModel> (this, MessageConstant.CARD_WITH_DRAW_CASH, x =>  {
				ReloadBalance ();
			});
		}

		public void ReloadBalance(){
			Account currentAccount = AccountManager.GetCurrentAccount ("1");
			BannerViewModel.BannerValue = currentAccount.EndBalance.ToString();
			DataSources = AccountManager.GetAccounts ("1");
		}
	}
}

