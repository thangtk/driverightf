﻿using System;
using System.Collections.Generic;
using DriveRightF.Core.Managers;
using Unicorn.Core;
using DriveRightF.Core.Models;
using System.Globalization;
using DriveRight.Core.ViewModels;
using DriveRightF.Core.Constants;

namespace DriveRightF.Core.ViewModels
{
	public class AccountListViewModel : BaseListTileVM<ItemAccountViewModel>
	{
		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get
			{
				throw new NotImplementedException ();
			}
		}

		private AccountManager AccountManager {
			get { 
				return DependencyService.Get<AccountManager> ();
			}
		}

		public override void UpdateHeaderFooter ()
		{
			HeaderViewModel.IsShowBtnBack = false;
			HeaderViewModel.Title = Translator.Translate("DRIVE_RIGHT_HEADER");

			TabBarViewModel.IsHidden = true;
		}

		#endregion

		public AccountListViewModel() :base()
		{
			// subscribe from Card Detail ViewModel
			MessagingCenter.Subscribe<Award> (this, MessageConstant.CARD_TAKE_MONEY, x => {
				ReloadBalance();
			});
			MessagingCenter.Subscribe<Award> (this, MessageConstant.CARD_GAMBLE, x => {
				ReloadBalance();
			});

			// subscribe from Account Detail ViewModel
			MessagingCenter.Subscribe<AccountDetailViewModel> (this, MessageConstant.CARD_WITH_DRAW_CASH, x => {
				ReloadBalance();
			});
		}

		private void ReloadBalance(){
			Account currentAccount = AccountManager.GetCurrentAccount ("1");
			if(TileViewModels != null && TileViewModels.Count > 1)
			{
				(TileViewModels[0] as BigItemAccountViewModel).BalanceValue = currentAccount.EndBalance.ToString();
				(TileViewModels[1] as SmallItemAccountViewModel).CardsEarnedValue = currentAccount.CardEarned.ToString();
				(TileViewModels[1] as SmallItemAccountViewModel).CashsEarnedValue = currentAccount.CashEarned.ToString();
			}
		}

		protected override void Initalize ()
		{
			TileViewModels = new List<ItemAccountViewModel> ();
			LoadViewModel ();
		}

		public void LoadViewModel()
		{
//			TileViewModels.Clear ();
			Account currentAccount = AccountManager.GetCurrentAccount ("1");
			if (currentAccount != null) {
				TileViewModels.Add (new BigItemAccountViewModel (){
					Month = DateTime.ParseExact(currentAccount.Month, "yyyy-MM", CultureInfo.InvariantCulture),
					BalanceValue = currentAccount.EndBalance.ToString()
				});
			}
			//TODO: retrieve actual account id
			List<Account> accounts = AccountManager.GetAccounts ("1");

			foreach (Account account in accounts) 
			{				
				TileViewModels.Add (new SmallItemAccountViewModel (){
					Month = DateTime.ParseExact(account.Month, "yyyy-MM", CultureInfo.InvariantCulture),
					CardsEarnedValue = account.CardEarned.ToString(),
					CashsEarnedValue = account.CashEarned.ToString()
				});
			}

			//			TileViewModels = new List<ItemAccountViewModel> () {
			//				new BigItemAccountViewModel () { Month = DateTime.UtcNow, Image = "KFC.png",BalanceValue = "230.9"},
			//				new SmallItemAccountViewModel () { Month = DateTime.UtcNow, CardsEarnedValue = "10", CashsEarnedValue = "123" },
			//				new SmallItemAccountViewModel () { Month = DateTime.UtcNow.AddMonths(-1), CardsEarnedValue = "10", CashsEarnedValue = "123"},
			//				new SmallItemAccountViewModel () { Month = DateTime.UtcNow.AddMonths(-2), CardsEarnedValue = "10", CashsEarnedValue = "123"},
			//				new SmallItemAccountViewModel () { Month = DateTime.UtcNow.AddMonths(-3), CardsEarnedValue = "10", CashsEarnedValue = "123" },
			//				new SmallItemAccountViewModel () { Month = DateTime.UtcNow.AddMonths(-4), CardsEarnedValue = "10", CashsEarnedValue = "123" },
			//				new SmallItemAccountViewModel () { Month = DateTime.UtcNow.AddMonths(-5), CardsEarnedValue = "10", CashsEarnedValue = "123" },
			//				new SmallItemAccountViewModel () { Month = DateTime.UtcNow.AddMonths(-6), CardsEarnedValue = "10", CashsEarnedValue = "123" }
			//			};

			NormalizeData (TileViewModels);

			int i = 0;
			int j = 0;
			for (i = 0; i < TileViewModels.Count; i++, j++)
			{
				TileViewModels [i].Colors = _colorsList [j];

				if (j + 1 == _colorsList.Count) 
					j = -1;
			}
		}

		public void InitViewModel ()
		{
			
		}

		private void NormalizeData(List<ItemAccountViewModel> tiles)
		{
			int numberMissingTiles = 15 - tiles.Count;

			if (numberMissingTiles > 0) 
				AddMissingData (tiles, numberMissingTiles);
			else if (numberMissingTiles < 0)
			{
				numberMissingTiles = (int)((Math.Abs (numberMissingTiles)) % 3f);
				AddMissingData (tiles, numberMissingTiles);
			}
		}

		private void AddMissingData(List<ItemAccountViewModel> tiles, int numberMissingTiles)
		{
			for (int i = 0; i < numberMissingTiles; i++)
			{
				SmallItemAccountViewModel viewmodel = new SmallItemAccountViewModel () {
					//CurrentMonth = DateTime.UtcNow.AddMonths(-1), CardsEarnedValue = "10", CashsEarnedValue = "123"
					//Month = DateTime.UtcNow.AddMonths(-1), 
					CardsEarnedValue = "", 
					CashsEarnedValue = ""
				};
				tiles.Add (viewmodel);
			}
		}
	}
}

