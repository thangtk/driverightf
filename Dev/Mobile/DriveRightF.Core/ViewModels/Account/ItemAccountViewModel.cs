﻿using System;
using DriveRightF.Core.ViewModels;
using System.Drawing;
using ReactiveUI;
using DriveRightF.Core.Models;

namespace DriveRightF.Core
{
	public abstract class ItemAccountViewModel : BaseViewModel, ITile
	{
		private string _colors;
		private string _image;
		private DateTime _month;
		//private Account _account;

		public Size Size { get; set; }

		public Point Position { get; set; }

//		public Color BackgroundColor { get; set; }
	
		public string Image {
			get
			{
				return _image;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _image, value);
			}
		}

		public string Colors {
			get
			{
				return _colors;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _colors, value);
			}
		}


		public DateTime Month {
			get
			{
				return _month;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _month, value);
			}
		} 

		public ItemAccountViewModel ()
		{			
		}
	}
}

