﻿using System;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using DriveRightF.Core.Managers;
using Unicorn.Core;
using DriveRightF.Core.Models;
using DriveRightF.Core;
using DriveRightF.Core.Constants;
using System.Text.RegularExpressions;

namespace  DriveRight.Core.ViewModels
{
	public partial class AccountDetailViewModel : BaseHandlerHeaderViewModel
	{
		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify
		{
			get
			{
				throw new NotImplementedException ();
			}
		}

		public override void UpdateHeaderFooter()
		{
			HeaderViewModel.Title = Translator.Translate("PAY_CASH_HEADER"); 
			HeaderViewModel.IsShowBtnBack = true;

			TabBarViewModel.IsHidden = true;
		}

		#endregion

		private AccountManager AccountManager
		{
			get
			{ 
				return DependencyService.Get<AccountManager>();
			}
		}

		private ProfileManager ProfileManager
		{
			get
			{ 
				return DependencyService.Get<ProfileManager>();
			}
		}


		public AccountDetailViewModel() : base()
		{
			InitCommand();
			//DummyData ();
			InitMessages();
		}

		public bool IsValidEditingCash(string value)
		{
			bool isValid = Regex.Match (value, "^[0-9]*[.]?[0-9]*$").Success;
			return isValid;
		}

		public void HandleEndEditingCash(string value)
		{
			if(!IsValidEditingCash(value))
			{
				PopupManager.ShowAlert ("", Translator.Translate ("WRONG_FORMAT"), showTitle: false, submitText: Translator.Translate("BUTTON_OK"));
				CashInputValue = "0.00";
				return;
			}

			if ( string.IsNullOrEmpty(value) )
			{
				CashInputValue = "0.00";
			}
			else if((float)Convert.ToDouble(value) > BalanceCashValue)
			{
				WithDrawCashValue = BalanceCashValue;
				WithDrawCashPercent = 100;
				CashInputValue = string.Format ("{0:N2} ",WithDrawCashValue);
			}
			else if(value != "")
			{
				WithDrawCashPercent = ((float)Convert.ToDouble(value) * 100f) / BalanceCashValue;
				CashInputValue = string.Format ("{0:N2} ",Convert.ToDouble(value));
//				CashInputValue = value;
			}
		}

		private void InitMessages()
		{
			MessagingCenter.Subscribe<Award>(this, MessageConstant.CARD_TAKE_MONEY, x =>
			{
				ChangeAccountData();
			});
			MessagingCenter.Subscribe<Award>(this, MessageConstant.CARD_GAMBLE, x =>
			{
				RxApp.MainThreadScheduler.Schedule(() =>
				{
					ChangeAccountData();
				});
			});
		}

		private void InitCommand()
		{
			LoadDataCommand = ReactiveCommand.Create();

			LoadDataCommand
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(obj =>
			{
				ChangeAccountData();
			});

			LoadAccountData = ReactiveCommand.Create();
			LoadAccountData
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(obj =>
			{
				ChangeAccountData();
			});

			this
				.WhenAnyValue(vm => vm.WithDrawCashPercent)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(v =>
			{
				WithDrawCashValue = (float)Math.Round((WithDrawCashPercent * BalanceCashValue) / 100, 2);
				PremiumDiscountValue = (float)Math.Round((BalanceCashValue - WithDrawCashValue) * 1.5f, 2);
				PremiumDiscountPercent = (float)Math.Round(100 - WithDrawCashPercent, 0);
			});

			BtnPayClick = ReactiveCommand.Create();
			BtnPayClick.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(action =>
			{
				if(action != null)
				{
					if(!CheckProfilePayment())
					{
						//show popup dialog

						PopupManager.ShowConfirm(Translator.Translate("ALERT"), Translator.Translate("IN_ORDER_TO_PAY"), 
							() =>
							{
								((Action<bool>)action).Invoke(true);
							},
							null, Translator.Translate("BUTTON_PROCEED"), Translator.Translate("BUTTON_CANCEL"), showTitle: true);
					}
						//TODO: else, process payment
					else
					{
						if(this.WithDrawCashValue <= 0)
						{
							PopupManager.ShowAlert(Translator.Translate("ALERT"), Translator.Translate("CASH_GRATER_THAN"), Unicorn.Core.Enums.DialogIcon.None, false, null, Translator.Translate("BUTTON_OK"));
							return;
						}
						UpdateAccount(this.WithDrawCashValue);
//						ChangeAccountData ();
//						this.WithDrawCashPercent = 0;
						MessagingCenter.Send(this, MessageConstant.CARD_WITH_DRAW_CASH);
						((Action<bool>)action).Invoke(false);
					}
				}
			});

			BtnGetDiscountClick = ReactiveCommand.Create();
			BtnGetDiscountClick.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(action =>
					DependencyService.Get<IApplication> ().NavigateToLocationSetting (GetDiscountWebUrl ()));
		}

		//TODO: need to verify logic (check payment name, payment password?)ß
		private bool CheckProfilePayment()
		{
			return !(string.IsNullOrEmpty(ProfileManager.Profile.PaymentAccount));
		}

		private string GetDiscountWebUrl()
		{
//			return string.Format("http://unicorn.vn:8011/Home/Index/{0}", ProfileManager.Profile.PhoneNumber);
			return "http://zensur.cloudapp.net/";
		}

		private void ChangeAccountData()
		{
			Account account = AccountManager.GetCurrentAccount("1");
			BalanceCashValue = account.EndBalance;
			PremiumAccountValue = (float)Math.Round(BalanceCashValue * 1.5f, 2);
			PremiumDiscountValue = PremiumAccountValue;
		}

		/// <summary>
		/// Updates the account for Withdraw Cash
		/// </summary>
		private void UpdateAccount(float amount)
		{
			Account account = AccountManager.GetCurrentAccount("1");
			if(account != null)
			{
				account.EndBalance = (float)Math.Round(account.EndBalance - amount, 2);

				AccountManager.SaveAccount(account);

				//Create cash transaction history
				AccountTransaction cashTransaction = new AccountTransaction {
					AccountId = "1",
					Time = DateTime.Now,
					Description = Translator.Translate("ACCOUNT_STATEMENT_WITHDRAW_CASH"),
					Type = Translator.Translate("ACCOUNT_STATEMENT_WITHDRAW"),
					Value = -amount,
					Unit = Translator.Translate("RMB")
				};
				AccountManager.SaveAccountTransaction(cashTransaction);

			}

		}

		private void DummyData()
		{
			BalanceCashValue = 100;
			PremiumAccountValue = 150;
		}
	}

}

