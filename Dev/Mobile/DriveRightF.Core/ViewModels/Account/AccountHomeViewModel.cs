﻿using System;
using DriveRightF.Core.Models;
using Unicorn.Core;
using DriveRightF.Core.Constants;
using DriveRightF.Core.Managers;
using DriveRight.Core.ViewModels;

namespace DriveRightF.Core
{
	public class AccountHomeViewModel : BaseHomeListViewModel<Account>
	{
		private AccountManager AccountManager
		{
			get {
				return DependencyService.Get<AccountManager> ();
			}
		}
		public AccountContentViewModel AccountContentViewModel {
			get;
			private set;
		}

		public AccountHomeViewModel () : base()
		{
			//TODO: get data to BannerViewmodel and source.

			BannerViewModel = new BannerViewModel () {
				BannerTile = Translator.Translate ("ACCOUNT_BALANCE"),
				BannerIcon = "yan_ico.png",
				BannerBackground = "account_banner.png",
			};

			AccountContentViewModel = new AccountContentViewModel (BannerViewModel);

		}

		#region implemented abstract members of BaseHandlerHeaderViewModel
		public override void UpdateHeaderFooter ()
		{
			HeaderViewModel.IsShowBtnBack = true;
			HeaderViewModel.Title = Translator.Translate("TAB_ACCOUNT");

			TabBarViewModel.IsHidden = true;
		}
		#endregion
	}
}

