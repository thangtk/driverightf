﻿using DriveRightF.Core.Enums;
using DriveRightF.Core.Managers;
using DriveRightF.Core.Models;
using ReactiveUI;
using Unicorn.Core;

namespace DriveRightF.Core.ViewModels
{
	public partial class SponsorRegistrationViewModel
	{
		private bool _isBusy;
		private bool _isEnableButton = false;
		private string _car;
		private string _carNo;
		private string _driverInsurer;
		private string _driverNo;
		private string _policyExpiry;
		private string _policyInsurer;
		private string _policyNo;

		private string _carError;
		private string _carNoError;
		private string _driverInsureError;
		private string _driverNoError;
		private string _policyExpiryError;
		private string _policyInsurerError;
		private string _policyNoError;
		private string _sponsorId = string.Empty;

		private string _carTitle;
		private string _carNoTitle;
		private string _driverInsurerTitle;
		private string _driverNoTitle;
		private string _policyExpiryTitle;
		private string _policyInsurerTitle;
		private string _policyNoTitle;
		private string _headerTitle;
		private string _headerLogo;

		private ViewMode _mode = ViewMode.Modify;

//		private IReactiveCommand<object> _loadDataCommand = ReactiveCommand.Create();
//		private IReactiveCommand<object> _saveDataCommand = ReactiveCommand.Create();
		private IReactiveCommand<object> _btnBackCliked = ReactiveCommand.Create();
		private IReactiveCommand<object> _btnAddCarClicked = ReactiveCommand.Create();
		private IReactiveCommand<object> _btnAddDriverCliked = ReactiveCommand.Create();
		private IReactiveCommand<object> _btnAddPolicyCliked = ReactiveCommand.Create();
		private IReactiveCommand<object> _checkInsuranceChange = ReactiveCommand.Create();
//		private IReactiveCommand<object> _btnOKCliked = ReactiveCommand.Create();

		public IReactiveCommand<SponsorRegistration> GetContractCommand { get; set; }
		public IReactiveCommand<bool> UpdateContractCommand { get; set; }

		#region IReactiveCommand
//		public IReactiveCommand<object> LoadDataCommand 
//		{
//			get {
//				return _loadDataCommand;
//			}
//			set{ 
//				_loadDataCommand = value;
//			}
//		}
//		public IReactiveCommand<object> SaveDataCommand 
//		{
//			get {
//				return _saveDataCommand;
//			}
//			set{ 
//				_saveDataCommand = value;
//			}
//		}
		public IReactiveCommand<object> BtnBackCliked 
		{
			get {
				return _btnBackCliked;
			}
			set{ 
				_btnBackCliked = value;
			}
		}
		public IReactiveCommand<object> BtnAddCarClicked 
		{
			get {
				return _btnAddCarClicked;
			}
			set{ 
				_btnAddCarClicked = value;
			}
		}
		public IReactiveCommand<object> BtnAddDriverCliked 
		{
			get {
				return _btnAddDriverCliked;
			}
			set{ 
				_btnAddDriverCliked = value;
			}
		}
		public IReactiveCommand<object> BtnAddPolicyCliked 
		{
			get {
				return _btnAddPolicyCliked;
			}
			set{ 
				_btnAddPolicyCliked = value;
			}
		}

		public IReactiveCommand<object> CheckInsuranceChange {
			get {
				return _checkInsuranceChange;
			}
			set {
				_checkInsuranceChange = value;
			}
		}

//		public IReactiveCommand<object> BtnOKCliked 
//		{
//			get {
//				return _btnOKCliked;
//			}
//			set{ 
//				_btnOKCliked = value;
//			}
//		}

		#endregion
		private bool _isAllPass = true;

		public override int ScreenToNotify {
			get {
				return (int)Screen.SponsorRegistration;
			}
		}

		public SponsorRegistrationManager SponsorRegistrationManager{
			get { 
				return DependencyService.Get<SponsorRegistrationManager> ();
			}
		}

		public SponsorManager SponsorManager{
			get { 
				return DependencyService.Get<SponsorManager> ();
			}
		}

		public ViewMode Mode
		{
			get {
				return _mode;
			}
			set {
				this.RaiseAndSetIfChanged (ref _mode, value);
			}
		}

		public string SponsorId
		{
			get {
				return _sponsorId;
			}
			set {
				_sponsorId = value;
			}
		}

		public bool IsEnableButton {
			get {
				return _isEnableButton;
			}
			set {
				this.RaiseAndSetIfChanged (ref _isEnableButton, value);
			}
		}

		public bool IsAllPass {
			get {
				return _isAllPass;
			}
			set {
				this.RaiseAndSetIfChanged (ref _isAllPass, value);
			}
		}

		public string Car {
			get { return _car;}
			set { 
				this.RaiseAndSetIfChanged (ref _car, value);
			}
		}

		public string CarNo {
			get { return _carNo;}
			set { 
				this.RaiseAndSetIfChanged (ref _carNo, value);
			}
		}

		public string DriverInsurer {
			get { return _driverInsurer;}
			set { 
				this.RaiseAndSetIfChanged (ref _driverInsurer, value);
			}
		}

		public string DriverNo {
			get { return _driverNo;}
			set { 
				this.RaiseAndSetIfChanged (ref _driverNo, value);
			}
		}

		public string PolicyExpiry {
			get { return _policyExpiry;}
			set { 
				this.RaiseAndSetIfChanged (ref _policyExpiry, value);
			}
		}

		public string PolicyInsurer {
			get { return _policyInsurer;}
			set { 
				this.RaiseAndSetIfChanged (ref _policyInsurer, value);
			}
		}

		public string PolicyNo {
			get { return _policyNo;}
			set { 
				this.RaiseAndSetIfChanged (ref _policyNo, value);
			}
		}

		public string CarTitle {
			get { return _carTitle;}
			set { 
				this.RaiseAndSetIfChanged (ref _carTitle, value);
			}
		}

		public string CarNoTitle {
			get { return _carNoTitle;}
			set { 
				this.RaiseAndSetIfChanged (ref _carNoTitle, value);
			}
		}

		public string HeaderTitle {
			get { return _headerTitle;}
			set { 
				this.RaiseAndSetIfChanged (ref _headerTitle, value);
			}
		}

		public string HeaderLogo {
			get { return _headerLogo;}
			set { 
				this.RaiseAndSetIfChanged (ref _headerLogo, value);
			}
		}

		public string DriverInsurerTitle {
			get { return _driverInsurerTitle;}
			set { 
				this.RaiseAndSetIfChanged (ref _driverInsurerTitle, value);
			}
		}

		public string DriverNoTitle {
			get { return _driverNoTitle;}
			set { 
				this.RaiseAndSetIfChanged (ref _driverNoTitle, value);
			}
		}

		public string PolicyExpiryTitle {
			get { return _policyExpiryTitle;}
			set { 
				this.RaiseAndSetIfChanged (ref _policyExpiryTitle, value);
			}
		}

		public string PolicyInsurerTitle {
			get { return _policyInsurerTitle;}
			set { 
				this.RaiseAndSetIfChanged (ref _policyInsurerTitle, value);
			}
		}

		public string PolicyNoTitle {
			get { return _policyNoTitle;}
			set { 
				this.RaiseAndSetIfChanged (ref _policyNoTitle, value);
			}
		}

		public string CarError {
			get { return _carError;}
			set { 
				this.RaiseAndSetIfChanged (ref _carError, value);
			}
		}

		public string CarNoError {
			get { return _carNoError;}
			set { 
				this.RaiseAndSetIfChanged (ref _carNoError, value);
			}
		}

		public string DriverInsurerError {
			get { return _driverInsureError;}
			set { 
				this.RaiseAndSetIfChanged (ref _driverInsureError, value);
			}
		}

		public string DriverNoError {
			get { return _driverNoError;}
			set { 
				this.RaiseAndSetIfChanged (ref _driverNoError, value);
			}
		}

		public string PolicyExpiryError {
			get { return _policyExpiryError;}
			set { 
				this.RaiseAndSetIfChanged (ref _policyExpiryError, value);
			}
		}

		public string PolicyInsurerError {
			get { return _policyInsurerError;}
			set { 
				this.RaiseAndSetIfChanged (ref _policyInsurerError, value);
			}
		}

		public string PolicyNoError {
			get { return _policyNoError;}
			set { 
				this.RaiseAndSetIfChanged (ref _policyNoError, value);
			}
		}

		public bool IsBusy{
			get { return _isBusy;}
			set { 
				if (value) 
					PopupManager.ShowLoading ();
				else
					PopupManager.HideLoading ();
				_isBusy = value;
			}
		}

	}
}

