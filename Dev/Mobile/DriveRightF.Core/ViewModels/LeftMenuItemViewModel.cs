﻿using System;
using DriveRightF.Core.ViewModels;
using System.Reactive.Linq;
using System.Linq;
using ReactiveUI;
using System.Reactive.Concurrency;
using DriveRightF.Core.Enums;
using Unicorn.Core;
using Unicorn.Core.Navigator;

namespace DriveRightF.Core.ViewModels
{
	public class LeftMenuItemViewModel : BaseViewModel
	{
		public override int ScreenToNotify {
			get {
				return (int)Screen.Home;
			}
		}

		private LeftMenuItem _item;

		public LeftMenuItem Item {
			get {
				return _item;
			}
			set {
				this.RaiseAndSetIfChanged (ref _item, value);
			}
		}

		private IReactiveCommand<object> _itemViewCellClick;
		private IReactiveCommand<object> _loadData;
		public IReactiveCommand<object> ItemViewCellClick {
			get {
				return _itemViewCellClick;
			}
			set{ 
				_itemViewCellClick = value;
			}
		}

		public IReactiveCommand<object> LoadData {
			get {
				return _loadData;
			}
			set{ 
				_loadData = value;
			}
		}

		public LeftMenuItemViewModel () : base()
		{
			
		}

		protected override void InitCommands()
		{
			ItemViewCellClick = ReactiveCommand.Create ();
			ItemViewCellClick
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (obj => {
					if (Item.Screen != null)
						DependencyService.Get<INavigator>().Navigate((int)Item.Screen);
				}
			);
		}
	}
}

