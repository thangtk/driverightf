﻿using System;
using System.Reactive.Linq;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using DriveRightF.Core.Managers;
using System.Threading.Tasks;
using DriveRightF.Core.Enums;
using DriveRightF.Core.Models;
using System.Reactive.Concurrency;

namespace DriveRightF.Core.ViewModels
{
	public class EarningItemViewModel : BaseViewModel
	{
		public override int ScreenToNotify {
			get {
				return (int)Screen.Earnings;
			}
		}

		private EarningStatus _status = EarningStatus.None;
		public EarningStatus Status {
			get{ 
				return _status;
			}
			set{ 
				this.RaiseAndSetIfChanged (ref _status, value);
			}
		}

//		private SponsorType _type;
//		public SponsorType Type {
//			get{ 
//				return _type;
//			}
//			set{ 
//				this.RaiseAndSetIfChanged (ref _type, value);
//			}
//		}

		private Earning _item;
		public Earning Item {
			get{ 
				return _item;
			}

			set{ 
				this.RaiseAndSetIfChanged (ref _item, value);
			}
		}

		private ImageMultiStatusViewModel _itemIconViewModel;
		public ImageMultiStatusViewModel ItemIconViewModel {
			get{ 
				return _itemIconViewModel;
			}
			set{ 
				this.RaiseAndSetIfChanged (ref _itemIconViewModel, value);
			}
		}

		private string _name;

		public string Name {
			get{ 
				return _name ?? string.Empty;
			}
			set{ 
				this.RaiseAndSetIfChanged (ref _name, value);
			}
		}

		private IReactiveCommand<object> _itemClickCommand;
		private IReactiveCommand<object> _addClickCommand;
		private IReactiveCommand<object> _removeItemStatusCommand;
		public IReactiveCommand<object> ItemClickCommand {
			get {
				return _itemClickCommand;
			}
			set{ 
				_itemClickCommand = value;
			}
		}

		public IReactiveCommand<object> AddClickCommand {
			get {
				return _addClickCommand;
			}
			set{ 
				_addClickCommand = value;
			}
		}

		public IReactiveCommand<object> RemoveItemStatusCommand {
			get {
				return _removeItemStatusCommand;
			}
			set{ 
				_removeItemStatusCommand = value;
			}
		}

		public EarningItemViewModel () : base ()
		{

		}

		protected override void InitCommands()
		{
			this.WhenAnyValue (vm => vm.Item)
				.Where(i => i != null)
				.Subscribe (item => {
					Name = item.Name;
					Status = item.Status;
					RxApp.TaskpoolScheduler.Schedule(()=>{
						
						var imageManager = DependencyService.Get<CachedImageManager>();
						var image = imageManager.GetImage (new CachedImage () { LocalPath = item.Icon, URL = item.IconURL }, item, "Icon");

						var icon = "";
						var iconUrl = "";
						if (image.Cached)
							icon = image.LocalPath;
						else {
							iconUrl = item.IconURL;
						}

//						ItemIconViewModel = new ImageMultiStatusViewModel((int)Screen.Sponsors){
//							Icon = image.LocalPath,
//							IconURl = image.URL,
//							Status = item.ItemStatus
//						};

						if(ItemIconViewModel != null)
						{
							ItemIconViewModel.Icon = icon;
							ItemIconViewModel.IconURl = iconUrl;
							ItemIconViewModel.Status = item.ItemStatus;
						}

					});

				});

			AddClickCommand = ReactiveCommand.Create ();
			AddClickCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (action => 
					PopupManager.ShowConfirm ("Add earnings?", "You sure you want to add these earnings?", 
						async () => {
							bool result = await AddEarningToService ();

							Item.Status = EarningStatus.InProgress;
							Item.Progress = 0;
							DependencyService.Get<EarningManager> ().Save(Item);

							if (action != null && result == true)
								(action as Action).Invoke ();

							DependencyService.Get<INavigator>().Navigate((int)Screen.EarningDetail, Item.EarningId);
						}, 
						() => {
					}));

			ItemClickCommand = ReactiveCommand.Create ();
			ItemClickCommand
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (earningId => DependencyService.Get<INavigator> ().Navigate ((int)Screen.EarningDetail, earningId));

			RemoveItemStatusCommand = ReactiveCommand.Create ();
			RemoveItemStatusCommand
				.Subscribe (_ => {
					ItemIconViewModel.Status = ItemStatus.None;
					Item.ItemStatus = ItemStatus.None;
					DependencyService.Get<EarningManager> ().Save(Item);
				});
		}

		private async Task<bool> AddEarningToService()
		{
			PopupManager.ShowLoading();
//			bool result = await DependencyService.Get<EarningManager> ().AddEarnings ();
			Task.Delay(4000);
			PopupManager.HideLoading();

			return true;
		}
	}
}

