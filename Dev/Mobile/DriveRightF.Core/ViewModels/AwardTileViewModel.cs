﻿using System;
using ReactiveUI;
using System.Collections.Generic;
using System.Drawing;
using DriveRightF.Core.Enums;

namespace DriveRightF.Core.ViewModels
{
	public class AwardTileViewModel : BaseViewModel, ITile
	{
		private string _image;
		private string _colors;
		private bool _isUsed;
		private AwardType _type;
		private Award _award;

		public Size Size { get; set; }

		public Point Position { get; set; }

//		public Color BackgroundColor { get; set; }

		public Award Award {
			get
			{
				return _award;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _award, value);
			}
		}

		public string Image {
			get
			{
				return _image;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _image, value);
			}
		}

		public string Colors {
			get
			{
				return _colors;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _colors, value);
			}
		}

		public AwardType Type {
			get
			{
				return _type;
			}
			set
			{
				_type = value;
			}
		}

		public bool IsUsed {
			get
			{
				return _isUsed;
			}
			set
			{
				this.RaiseAndSetIfChanged (ref _isUsed, value);
			}
		}

		#region implemented abstract members of BaseViewModel

		public override int ScreenToNotify {
			get
			{
				return (int)Screen.AwardDetail;
			}
		}

		#endregion

		public AwardTileViewModel() : base ()
		{

		}
	}
}

