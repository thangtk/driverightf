﻿using System;
using System.Collections.Generic;
using DriveRightF.Core.Enums;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using DriveRightF.Core.Models;
using Unicorn.Core;
using DriveRightF.Core.Managers;

namespace DriveRightF.Core.ViewModels
{
	public class LeftMenuViewModel : BaseViewModel
	{
		public const string DEFAULT_IMAGE = "icon_unknown_user.png";

		public override int ScreenToNotify {
			get {
				return (int)Screen.Home;
			}
		}

		private List<LeftMenuItem> _items;
		private string _image;

		public List<LeftMenuItem> Items {
			get {
				return _items;
			}
			set {
				this.RaiseAndSetIfChanged (ref _items, value);
			}
		}

		public string Image {
			get {
				return _image;
			}
			set {
				this.RaiseAndSetIfChanged (ref _image, value);
			}
		}

		public LeftMenuViewModel () : base()
		{
			InitData ();
		}

		private void InitData()
		{
			_items = new List<LeftMenuItem> () {
				new LeftMenuItem () { Title = "Profile", Icon = "logo_profile.png", Screen = Screen.Profile },
				new LeftMenuItem () { Title = "Perks", Icon = "logo_perk.png", Screen = Screen.Perks },
				new LeftMenuItem () { Title = "Earnings", Icon = "logo_earning.png", Screen = Screen.Earnings },
				new LeftMenuItem () { Title = "Sponsors", Icon = "logo_sponsor.png", Screen = Screen.Sponsors },
				new LeftMenuItem () { Title = "Trips", Icon = "logo_trip.png", Screen = Screen.Trips }
			};

//			User user = DependencyService.Get<UserManager> ().GetUser ();
//			user.Image = String.Empty;
//			DependencyService.Get<UserManager> ().Save (user);

			ReloadImage ();
		}

		public void ReloadImage()
		{
			Image = DependencyService.Get<UserManager> ().GetImage (DEFAULT_IMAGE);
		}

		#region implemented abstract members of BaseViewModel

		protected override void InitCommands ()
		{
			
		}

		#endregion
	}
}

