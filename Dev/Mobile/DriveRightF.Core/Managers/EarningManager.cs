﻿using System;
using DriveRightF.Core.Models;
using System.Collections.Generic;
using System.Threading;
using Unicorn.Core;
using System.Globalization;
using DriveRightF.Core.Constants;
using System.Text;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.Core.Managers.EarningManager))]
namespace DriveRightF.Core.Managers
{
	public class EarningManager : BaseAutoRequestManager<Earning>
	{
		public EarningManager ()
		{
			// TEST
//			Repository.DeleteAll<Earning>();
		}

		public Earning GetById(string earingId)
		{
			return Repository.Get<Earning> (x => x.EarningId == earingId);
		}

		#region implemented abstract members of BaseAutoRequestManager

		public override List<Earning> GetDataFromServer ()
		{
			var data = ServiceManager.GetChallenges ();
			if(data != null)
			{	// Save data into db
				FilterItems(data);
				SaveData(data);
				// Update request time
				UpdateLastTimeRequest();
			}
			//
			return data;
//			Debugger.ShowThreadInfo ("Begin request");
//			Thread.Sleep (3000);
//			// TEST
//			var random = new Random();
//			StringBuilder availablePerks = new StringBuilder("");
//			StringBuilder gainedPerks = new StringBuilder ("");
//			int gainedNumber = random.Next (3, 9);
//			string next = random.Next (1, 20).ToString ();
//			for (int i = 0; i < 10; i++) {
//				while (availablePerks.ToString ().Contains (next))
//					next = random.Next (1, 20).ToString ();
//				availablePerks.Append (next);
//				availablePerks.Append (",");
//
//			}
//
//			for (int i = 0; i < gainedNumber; i++) {
//				next = availablePerks.ToString ().Split (new char [1]{','}) [random.Next (10)];
//				while (gainedPerks.ToString ().Contains (next))
//					next = random.Next (1, 20).ToString ();
//				gainedPerks.Append (random.Next (1, 20).ToString ());
//				gainedPerks.Append (",");
//			}
//
//			var responseData = new List<Earning> ();
//			int count = DateTime.Now.Millisecond % 4;
//			for (int i = 1; i <= count; i++) {
//				responseData.Add (new Earning () {
//					Name = "Earning " + ++_count,
//					Description = "Earning " + _count,
//					Icon = "award_dimond.png",
//					IconURL = "http://xamarin.com/resources/design/home/devices.png",
//					Image = "coffee_snob.jpg",
//					Get = "Xxxxx",
//					StartTime = DateTime.Today.AddDays(-10),
//					Archieve = "Xxxxxxxx",
//					Progress = i % 2== 0 ? 0 : new Random().Next(100),
//					Status = i % 2 == 0 ? EarningStatus.New : EarningStatus.InProgress,
//					ItemStatus = ItemStatus.New,
//					EarningId = (_count + 1).ToString(),
//					AvailablePerks = availablePerks.ToString(),
//					GainedPerks = gainedPerks.ToString(),
//				});
//			}
//			// TEST
//			if(count == 1){
//				throw new Exception ("Error occurs while requesting to server");
//			}
//			//
//			bool success = true; // Fake
//			if(success){
//				// Save data into db
//				SaveData(responseData);
//				// Update request time
//				UpdateLastTimeRequest();
//			} else {
//				// QUESTION: How to nofify error???
//				return null;
//			}
//			//
//			Debugger.ShowThreadInfo ("End request");
//			//
//			return responseData;
		}

		public override string LastUpdatedKey {
			get {
				return ConfigKey.EARNING_LAST_UPDATED;
			}
		}

		public override long AllowTime {
			get {
				return AutoRequestTime.REQUEST_TIME_EARNING;
			}
		}

		#endregion
	}
}

