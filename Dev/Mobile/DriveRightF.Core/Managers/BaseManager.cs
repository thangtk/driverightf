﻿using System;
using Unicorn.Core.Storages;
using Unicorn.Core;
using DriveRightF.Core.Storages;
using DriveRightF.Core.Managers;
using DriveRightF.Core.Services;
using System.Collections.Generic;
using Unicorn.Core.Bases;
using System.Globalization;
using System.Linq;

namespace DriveRightF.Core
{
	public class BaseManager
	{
		public BaseManager ()
		{

		}

		public SQLiteRepository Repository {
			get {
				return DependencyService.Get<SQLiteRepository> ();
			}
		}

		public ConfigManager ConfigManager{
			get { 
				return DependencyService.Get<ConfigManager> ();
			}
		}

		public DriveRightFServiceManager ServiceManager{
			get { 
				return DependencyService.Get<DriveRightFServiceManager> ();
			}
		}

		public ProfileManager ProfileManager{
			get { 
				return DependencyService.Get<ProfileManager> ();
			}
		}

		/// <summary>
		/// Just get modified or new items base on Modified field
		/// TODO: refactor later
		/// </summary>
		/// <param name="items">Items.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public void FilterItems<T>(List<T> items) where T: BaseModel, ICheckChangeModel, new()
		{
			if (items == null || items.Count == 0) {
				return;
			}
			// Get all local items from db
			var localItems = Repository.GetAll<T> ();
			if (localItems == null || localItems.Count == 0) {
				foreach(var i in items){
					i.ItemStatus = ItemStatus.New;
				}
				return;
			}
			//
			foreach (var i in items) {
				var localItem = localItems.FirstOrDefault (o => o.ServerId == i.ServerId);
				if (localItem == null) {
					i.ItemStatus = ItemStatus.New;
				} else if (CompareTime(i.Modified, localItem.Modified) > 0) {
					i.ItemStatus = ItemStatus.Modified;
					i.Id = localItem.Id;
				} else {
					i.ItemStatus = ItemStatus.None;
				}
			}
			// Keep modified or new items only
			items.RemoveAll (i => i.ItemStatus == ItemStatus.None);
		}

		/// <summary>
		/// Ignore milliseconds and kind of time
		/// </summary>
		/// <returns>The time.</returns>
		/// <param name="d1">D1.</param>
		/// <param name="d2">D2.</param>
		private int CompareTime(DateTime d1, DateTime d2){
			d1 = new DateTime (d1.Year, d1.Month, d1.Day, d1.Hour, d1.Minute, d1.Second);
			d2 = new DateTime (d2.Year, d2.Month, d2.Day, d2.Hour, d2.Minute, d2.Second);
			return DateTime.Compare (d1, d2);
		}
	}

	public abstract class BaseAutoRequestManager<T> : BaseManager where T: BaseModel, new()
	{
		protected int _count;
		public const string TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
		public abstract string LastUpdatedKey { get; }
		public abstract long AllowTime { get; }

		public void Save(T model){
			if (model != null) {
				Repository.Save (model);
			}
		}

		public virtual List<T> GetDataFromDb (){
			var temp = Repository.GetAll<T> ();
			_count = temp == null ? 0 : temp.Count;
			return temp;
		}
		public abstract List<T> GetDataFromServer();
		public virtual void SaveData (List<T> data){
			if (data != null & data.Count > 0) {
				Repository.Save (data);
			}
		}

		public virtual bool CheckAutoRequest ()
		{
			MyDebugger.ShowThreadInfo ("Check Auto request");
			// Check No data => always allow auto request
			var data = GetDataFromDb();
			if (data == null || data.Count == 0) {
				return true;
			}
			var lastUpdated = ConfigManager.GetValue (LastUpdatedKey);
			if (string.IsNullOrEmpty (lastUpdated)) {
				return true;
			}
			//
			System.Diagnostics.Debug.WriteLine (">>> Last time request: " + lastUpdated);
			//
			DateTime d = DateTime.ParseExact (lastUpdated, TIME_FORMAT, CultureInfo.InvariantCulture);
			return DateTime.Now.Subtract (d).TotalMilliseconds > AllowTime;
		}

		public virtual void UpdateLastTimeRequest ()
		{
			ConfigManager.SaveData( LastUpdatedKey, DateTime.Now.ToString(TIME_FORMAT));
		}
	}
}

