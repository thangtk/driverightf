﻿using System;
using DriveRightF.Core.Models;
using System.Collections.Generic;
using System.Linq;
using Unicorn.Core;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.Core.Managers.TripStatementManager))]
namespace DriveRightF.Core.Managers
{
	public class TripStatementManager : BaseManager
	{
		/// <summary>
		/// Gets the trip statements list. Overall month: 9999-01
		/// </summary>
		/// <returns>The trip statements list.</returns>
		public List<TripMonth> GetTripStatementsList()
		{
			var list = Repository
				.GetTableQuery<TripMonth> ()
				.OrderByDescending (trip => trip.Month).ToList ();
			return list;
		}


		public TripMonth GetTripStatementLocal(DateTime month)
		{
			return Repository.Get<TripMonth>(x => (x.Month.Year == month.Year && x.Month.Month == month.Month));
		}

		public TripMonth GetTripStatementFromServer(DateTime month)
		{
			TripMonth responseData = null;
			if (month.Year == 9999 && month.Month == 1) {
				responseData = ServiceManager.GetTripSummary6Month ();
			} else {
				responseData = ServiceManager.GetTripMonthDetail(month);
			}
			//
			if (responseData != null) {
				UpdateTripStatement (responseData);
			}
			return responseData;
		}

		/// <summary>
		/// Get list of monthly trip statement from server
		/// Update into database
		/// Return ordered data
		/// </summary>
		/// <returns>The trip statement.</returns>
		public List<TripMonth> SyncTripStatement(){
			// TODO:
			// Get list of monthly trip statements from server
			// Fake data -> image get randomly from a list of image (resource)
			// Order
			// Update to db: insert + update
			var data = ServiceManager.GetTripMonthSummary();
			List<TripMonth> tripMonths = null;
			if (data != null) {
				tripMonths = new List<TripMonth> ();
				data.ForEach (x => tripMonths.Add (new TripMonth (){ 
					Month = x.Month,
					Score = (float)x.Score
				}));
				FakeImages (tripMonths);
				tripMonths.OrderByDescending (x => x.Month);
				UpdateTripStatementByMonth(tripMonths);
			}
			return tripMonths;
		}

		private void FakeImages(List<TripMonth> data){
			string[] images = new string[]{"City/Beijing.jpg", "City/Hongkong.jpg","City/Shanghai.jpg","City/Xiamen.jpg" };
			Random r = new Random ();
//			string img = images [r.Next (3)];
//			System.Diagnostics.Debug.WriteLine (">>> Random image >>> " + img);
			data.ForEach(x => x.ImageUrl = images [r.Next (3)]);
		}

		private void UpdateTripStatementByMonth(List<TripMonth> newData){
			if (newData == null) {
				return;
			}
			//
			var currentData = Repository.GetAll<TripMonth> ();
			if (currentData != null && currentData.Count != 0) {
				newData.ForEach (x => {
					var item = currentData.Find (a => a.Month.MonthDiff (x.Month) == 0);
					if (item != null) {
						// FAKE: skip update image
						if (string.IsNullOrEmpty (item.ImageUrl)) {
							item.ImageUrl = x.ImageUrl;
						} else {
							x.ImageUrl = item.ImageUrl;
						}
						item.Score = x.Score;
					} else {
						currentData.Add (x);
					}
				});
			} else {
				currentData = newData;
			}
			//
			Repository.Save (currentData);
		}

		private void UpdateTripStatement(TripMonth data){
			// TODO: Test
			var x = Repository.GetList<TripMonth> (d => d.Month.MonthDiff (data.Month)==0).FirstOrDefault ();
			if (x != null) {
				data.Id = x.Id;
				// Update all properties except image
				// FAKE: skip update image
				if(!string.IsNullOrEmpty(x.ImageUrl)){
					data.ImageUrl = x.ImageUrl;
				}
				Repository.Save (data);
			}
		}

		public float GetSummaryTripScore(){
			DateTime d = new DateTime(9999,01,01);
			var x = GetTripStatementLocal(d);
			if (x == null) {
				// TODO: edit later
//				x = GetTripStatementFromServer(d);
//				if (x == null) {
//					return 0;
//				}
				return 0;
			}
			return (float)Math.Round (x.Score, 0);
		}
	}
}

