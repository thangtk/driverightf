﻿using System;
using DriveRightF.Core.Models;
using System.Collections.Generic;
using Unicorn.Core;
using System.Threading;
using DriveRightF.Core.Enums;
using DriveRightF.Core.Constants;
using DriveRightF.Core.Services;
using DriveRightF.Services.Responses;
using DriveRightF.Services.Requests;
using System.Globalization;
using System.Linq;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.Core.Managers.AccountManager))]
namespace DriveRightF.Core.Managers
{
	public class AccountManager : BaseManager
	{		
		/// <summary>
		/// Gets the account information by Account and month for Account Statement
		/// </summary>
		/// <returns>The monthly account.</returns>
		/// <param name="accountId">Account identifier.</param>
		/// <param name="month">Month: format yyyy-MM</param>
		public Account GetMonthlyAccountStatement(string accountId, string month)
		{			
//			return Repository.Get<Account> (x => x.AccountId.Equals (accountId) && x.Month.Equals (month));
			return Repository.Get<Account> (x => x.Month.Equals (month));
		}

		/// <summary>
		/// Gets the monthly accounts.
		/// </summary>
		/// <returns>The accounts.</returns>
		/// <param name="accountid">Accountid.</param>
		public List<Account> GetAccounts(string accountid)
		{
			//return Repository.GetList<Account> (x => x.AccountId.Equals (accountid));
			return Repository.GetTableQuery<Account> ()
//				.Where(x => x.AccountId == accountid)
				.OrderByDescending (x => x.Month).ToList();
		}

		/// <summary>
		/// Gets the history transactions for Account Statement based on month
		/// </summary>
		/// <returns>The account transactions.</returns>
		/// <param name="accountId">Account identifier.</param>
		/// <param name="month">Month: format yyyy-MM</param>
		public List<AccountTransaction> GetAccountTransactions(string accountId, string month)
		{
			//return Repository.GetTableQuery<AccountTransaction> ().Where(x => (x.Time.ToString("yyyy-MM") == month)).OrderByDescending (x => x.Time).ToList();
			return Repository.GetList<AccountTransaction> (x => x.Time.ToString("yyyy-MM") == month && x.AccountId == accountId).OrderByDescending(x=>x.Time).ToList();
		}

		/// <summary>
		/// Saved account.
		/// </summary>
		/// <returns>The monthly account.</returns>
		/// <param name="account">Account.</param>
		public int SaveAccount(Account account)
		{
			return Repository.Save<Account> (account);
		}

		/// <summary>
		/// Save the account transaction.
		/// </summary>
		/// <returns>The account transaction.</returns>
		/// <param name="transaction">Transaction.</param>
		public int SaveAccountTransaction(AccountTransaction transaction)
		{
			return Repository.Save<AccountTransaction> (transaction);
		}

		/// <summary>
		/// Gets the lastest account (Overall).
		/// </summary>
		/// <returns>The current account.</returns>
		/// <param name="accountId">Account identifier.</param>
		public Account GetLastestAccount(string accountId)
		{
			return Repository.GetTableQuery<Account> ().OrderByDescending (x => x.Month).FirstOrDefault ();
		}

		/// <summary>
		/// Gets the current account or create new account for current month if not exist
		/// </summary>
		/// <returns>The current account.</returns>
		/// <param name="accountId">Account identifier.</param>
		public Account GetCurrentAccount(string accountId)
		{
			Account lastestAccount = GetLastestAccount (accountId);
			if (lastestAccount == null) {
				return null;
			}
			string month = DateTime.Now.ToString ("yyyy-MM");
			if (!string.IsNullOrEmpty (lastestAccount.Month) && lastestAccount.Month.CompareTo (month)<0) {
				//create new monthly account
				Account currentAccount = new Account () {
					AccountId = accountId,
					CardEarned = 0,
					CashEarned = 0,
					EndBalance = lastestAccount.EndBalance,
					EndCard = lastestAccount.EndCard,
					StartBalance = lastestAccount.EndBalance,
					StartCard = lastestAccount.StartCard,
					Month = month
				};
				SaveAccount (currentAccount);
				return currentAccount;
			}
			return lastestAccount;
		}

		// TODO: confirm
		public double GetCurrentBalance(){
			var currentAccount = GetCurrentAccount ("1"); // QUESTION: what is account id?
			if(currentAccount == null) return 0;
			return Math.Round(currentAccount.EndBalance, 0); // TODO confirm
		}
	}
}

