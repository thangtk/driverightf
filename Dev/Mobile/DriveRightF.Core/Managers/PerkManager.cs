﻿using System;
using DriveRightF.Core.Models;
using System.Collections.Generic;
using Unicorn.Core;
using System.Threading;
using DriveRightF.Core.Enums;
using DriveRightF.Core.Constants;
using DriveRightF.Core.Services;
using DriveRightF.Services.Responses;
using DriveRightF.Services.Requests;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.Core.Managers.PerkManager))]
namespace DriveRightF.Core.Managers
{
	public class PerkManager : BaseAutoRequestManager<Perk>
	{
		public PerkManager ()
		{
			// TEST
//			Repository.DeleteAll<Perk>();
		}

		#region implemented abstract members of BaseAutoRequestManager

		public override List<Perk> GetDataFromServer ()
		{
			var data = ServiceManager.GetPerks ();
			if(data != null){
				// Save data into db
				FilterItems(data);
				SaveData(data);
				// Update request time
				UpdateLastTimeRequest();
			}
			//
			return data;
		}

		public override string LastUpdatedKey {
			get {
				return ConfigKey.PERK_LAST_UPDATED;
			}
		}

		public override long AllowTime {
			get {
				return AutoRequestTime.REQUEST_TIME_PERK;
			}
		}

		#endregion

		public Perk GetPerk(string perkId)
		{
			// PerkId is id in local
//			return Repository.Get<Perk>(perkId);


			return Repository.Get<Perk>(x => x.PerkId == perkId.ToString());

		}
		public AvailablePerk GetAvailablePerk(int perkId)
		{
			return Repository.Get<AvailablePerk> (x => x.PerkId == perkId.ToString ());
		}

		public void SaveAvailablePerk(AvailablePerk perk)
		{
			Repository.Save (perk);
		}
	}
}

