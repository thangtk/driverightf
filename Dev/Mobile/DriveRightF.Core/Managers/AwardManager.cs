﻿using System;
using System.Collections.Generic;
using System.Linq;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.Core.Managers.AwardManager))]
namespace DriveRightF.Core.Managers
{
	public class AwardManager : BaseManager
	{
		public List<Award> GetAwardsList()
		{
			var listAll = Repository
				.GetTableQuery<Award> ().ToList();
			var list = listAll
//				.Where (award => (award.Type =="IDA" || award.Used == false || award.Used == null))
				.Where (award => award.Expiry >= DateTime.Now)
				.OrderBy (award => award.Expiry).ToList ();
			return list;
		}

		public List<Award> GetAwardsList(bool isUsed)
		{
			var listAll = Repository
				.GetTableQuery<Award> ().ToList();
			var list = listAll
				.Where (award => (award.Used == isUsed))
				.Where (award => award.Expiry >= DateTime.Now)
				.OrderBy (award => award.Expiry).ToList ();
			return list;
		}

		public Award GetAward(int awardId)
		{
			return Repository.Get<Award> (awardId);	
		}
		public void Delete(Award award)
		{
			Repository.Delete<Award> (award.Id);	
		}

		public void Save(Award item)
		{
			Repository.Save<Award> (item);
		}

		// TODO: confirm
		public int GetNumOfAwards(){
			var x = Repository.GetList<Award>(a => true); // !a.Used && a.Expiry < DateTime.Now
			if (x == null)
				return 0;
			return x.Count;
		}
	}
}

