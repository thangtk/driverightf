﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DriveRightF.Core.Models;
using System.Linq;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.Core.Managers.TripDetailManager))]
namespace DriveRightF.Core.Managers
{
	public class TripDetailManager : BaseManager
	{
		public TripDetailManager ()
		{
		}

		public async Task<Trip> GetTripDetailFromServer(string tripId)
		{
			Console.WriteLine ("Begin request");
			Trip trip = ServiceManager.GetTripDetail (tripId);
			Console.WriteLine ("End request");

			return trip;
		}

		public List<GPS> GetTripDetail(string tripId)
		{
			return ServiceManager.GetTripDetail (tripId).Coordinates;
		}

		public List<GPS> GetGPSFromLocal(string tripId)
		{
			var list = Repository.GetTableQuery<GPS> ()
				.Where (coor => coor.TripId == tripId).ToList ();
			return list;
		}
	}
}

