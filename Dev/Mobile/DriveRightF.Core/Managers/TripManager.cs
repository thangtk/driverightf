﻿using System;
using System.Collections.Generic;
using DriveRightF.Core.Models;
using DriveRightF.Core.Services;
using System.Linq;
using System.Threading.Tasks;
using DriveRightF.Core.Constants;
using System.Globalization;
using Unicorn.Core;
using System.Threading;
using DriveRightF.Services.Responses;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.Core.Managers.TripManager))]
namespace DriveRightF.Core.Managers
{
	public class TripManager : BaseManager
	{
		public const string MONTH_FORMAT = "yyyy-MM";
		public const string TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

		public TripManager ()
		{
			// TEST
//			Repository.DeleteAll<Trip>();
		}

		public void Save(List<TripSummary> trips)
		{
			if (trips == null || trips.Count == 0) {
				return;
			}
			//
			Repository.Save<TripSummary> (trips);
		}

		public List<Trip> GetTrip()
		{
			return Repository.GetAll<Trip> ();
		}

		public List<TripSummary> GetTripsLocal(DateTime month)
		{
			List<TripSummary> trips = Repository.GetList<TripSummary> (x => x.StartTime.MonthDiff(month) == 0);
//			System.Diagnostics.Debug.WriteLine (">>> Local trip count: >>>> " + trips.Count + " >>> " + month.ToString("Y"));
			return trips;
		}

		public List<Trip> Sort(List<Trip> trips)
		{
			if (trips != null && trips.Count > 0) {
				return trips.OrderByDescending (x => x.UpdatedTime).ToList ();
			}
			return trips;
		}

		public List<TripSummary> GetTripFromServer(DateTime month){
			// Replace by demo thread
			var data = ServiceManager.GetTripByMonth (month);
			if(data != null){
				// Save data into db
				FilterItems<TripSummary>(data);
				Save(data);
				// Update request time
				UpdateLastTimeRequest(month);
			}
			//
			return data;
//			Thread.Sleep(1000);
//			return null;
		}

		public bool CheckAutoRequest(DateTime month){
//			Debugger.ShowThreadInfo ("Check Auto request >>> " + month.ToString("Y"));
			// Check No data => always allow auto request
			var trips = GetTripsLocal(month); // GetTripsLocalAsync (month).Result;
			if (trips == null || trips.Count == 0) {
				return true;
			}
			var lastUpdated = ConfigManager.GetValue (GetMonthKeyConfig (month));
			if (string.IsNullOrEmpty (lastUpdated)) {
				return true;
			}
			//
//			System.Diagnostics.Debug.WriteLine (">>> Last time request: " + lastUpdated);
			//
			DateTime d = DateTime.ParseExact (lastUpdated, TIME_FORMAT, CultureInfo.InvariantCulture);
			return DateTime.Now.Subtract (d).TotalMilliseconds > AutoRequestTime.REQUEST_TIME_TRIP;
		}

		private string GetMonthKeyConfig(DateTime month){
			return month.ToString(MONTH_FORMAT) +  "_last_updated";
		}

		public void UpdateLastTimeRequest(DateTime month){
			string key = GetMonthKeyConfig(month);
			ConfigManager.SaveData( key, DateTime.Now.ToString(TIME_FORMAT));
		}

		public Trip GetTripDetail(string tripId)
		{
			return ServiceManager.GetTripDetail (tripId);
		}
	}
}

