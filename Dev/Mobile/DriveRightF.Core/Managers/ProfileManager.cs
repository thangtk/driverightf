﻿using System;
using DriveRightF.Core.Models;
using System.Linq;
using System.Threading.Tasks;
using Unicorn.Core;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.Core.Managers.ProfileManager))]
namespace DriveRightF.Core.Managers
{
	public class ProfileManager: BaseManager
	{
		private IImageManager ImageManager{
			get { 
				return DependencyService.Get<IImageManager> ();
			}
		}

		private Profile _profile;
		public Profile Profile
		{
			get
			{
				if (_profile == null) {
					_profile = Repository.Get<Profile> ();
				}
				return _profile;
			}
		}

		public Profile GetProfile()
		{
			Profile profileLocal = Repository.GetAll<Profile> ().FirstOrDefault ();
			return profileLocal;
		}

		public Profile GetProfileFromService()
		{
			var profile = ServiceManager.GetProfile ();
			SaveProfileService (profile);
			return profile;
		}

		public Profile GetProfileFromService(string numberPlate)
		{
			var profile = ServiceManager.GetProfile ();
			profile.NumberPlate = numberPlate;
			Save (profile);
			return profile;
		}

		public bool VerifyPaymentMethod(string type,string name,string number,string password){
			return true;
		}

		public void Save(Profile proff){
			if (proff == null)
				return;
			Profile profile = Repository.GetAll<Profile> ().FirstOrDefault ();

			if (profile != null) {
				proff.Id = profile.Id;
			}

			Repository.Save<Profile>(proff);
			_profile = proff;
		}

		private void SaveProfileService(Profile profileService)
		{
			if (profileService == null)
				return;
			Profile profile = Repository.GetAll<Profile> ().FirstOrDefault ();

			if (profile != null && profileService != null) {
				profileService.Id = profile.Id;
				profileService.NumberPlate = profile.NumberPlate;
				profileService.PaymentAccount = profile.PaymentAccount;
				profileService.PaymentName = profile.PaymentName;
				profileService.PaymentPassword = profile.PaymentPassword;
				profileService.PaymentType = profile.PaymentType;
			}

			Repository.Save<Profile>(profileService);
			_profile = profileService;
		}

		public bool UpdateProfile(Profile profile, object imageData){
			// REM for demo
			//			var profileId = ServiceManager.UpdateProfile (profile);
			// main thread or not???

			//TODO: remove image after update raw image success.
			// How to check service call failed?? remove image
			string imgLocal = ImageManager.SaveImage(imageData);
			if (!String.IsNullOrEmpty (imgLocal)) {

				try
				{
					string imageUrl = ServiceManager.UpdateRawImageProfile (profile.ProfileID, imgLocal);
					//TODO: check to delete image if service not throw exception.
					if (!string.IsNullOrEmpty (imageUrl)) {
						ImageManager.DeleteImage(profile.Image);
						profile.Image = imgLocal;
						profile.ImageUrl = imageUrl;
					} 
					else
					{
						ImageManager.DeleteImage(imgLocal);
					}
				}
				catch(Exception e) {
					//TODO: check remove image, and continue throw exception
					ImageManager.DeleteImage(imgLocal);
					throw e;
				}
			}

			//TODO: condition save profile, if don't throw exception.

			ServiceManager.UpdateProfile (profile);
			_profile = profile;
			Save (profile);

			return true; // It's always true
		}

		public void ClearAllData()
		{
			//			_db.CreateTable<Account> ();
			//			_db.CreateTable<Config> ();
			//			_db.CreateTable<Award> ();
			//			_db.CreateTable<Notification> ();
			//			_db.CreateTable<Profile> ();
			//			_db.CreateTable<TripMonth> ();
			//			_db.CreateTable<Trip> ();
			//			_db.CreateTable<TripSummary> ();
			//			_db.CreateTable<AccountTransaction> ();
			//			_db.CreateTable<GPS> ();

//			Repository.DeleteAll<Account> ();
			Repository.DeleteAll<Config> ();
//			Repository.DeleteAll<Award> ();
			Repository.DeleteAll<Notification> ();
			Repository.DeleteAll<Profile> ();
			Repository.DeleteAll<TripMonth> ();
			Repository.DeleteAll<Trip> ();
			Repository.DeleteAll<TripSummary> ();
			Repository.DeleteAll<AccountTransaction> ();
			Repository.DeleteAll<GPS> ();


		}
	}
}

