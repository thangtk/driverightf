﻿	using System;
using System.Threading.Tasks;
using Unicorn.Core.Bases;

namespace DriveRightF.Core
{
	public abstract class CachedImageManager : BaseManager
	{
		protected abstract bool ImageExists(string localPath);
		protected abstract Task<string> DownloadImageAsync(string url);

		public CachedImage GetImage(CachedImage cachedImage, BaseModel model, string propertyName)
		{
			if (!string.IsNullOrEmpty(cachedImage.LocalPath)) { 
				// try to get image by local path
				if (ImageExists(cachedImage.LocalPath))
				{
					cachedImage.Cached = true;
					return cachedImage;
				}
				else {
					cachedImage.LocalPath = string.Empty;
				}
			}
			// Download image from URL asynchronously
			if(model != null)
				CacheImageAsync(cachedImage.URL, model, propertyName);
			cachedImage.Cached = false;
			return cachedImage;
		}

		protected async void CacheImageAsync (string url, BaseModel model, string propertyName){
			var localPath = await DownloadImageAsync (url);
			if (localPath == null)
				return;
			if (model != null) {
				var p = model.GetType ().GetProperty (propertyName);
				if (p != null) {
					p.SetValue (model, localPath);
					Repository.Save (model);
				}
			}
		}
	}

	public class CachedImage {
		public string LocalPath { get; set; }
		public string URL { get; set; }

		public bool Cached { get; set; }
	}
}

