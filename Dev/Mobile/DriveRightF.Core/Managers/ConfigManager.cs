﻿using System;
using DriveRightF.Core.Models;
using System.Linq;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.Core.Managers.ConfigManager))]
namespace DriveRightF.Core.Managers
{
	public class ConfigManager : BaseManager
	{
		/// <summary>
		/// Get config object by key
		/// Not found: return null
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		private Config GetData(string key) { 
			return Repository.GetList<Config> (c => c.Key == key).FirstOrDefault (); //GetTableQuery<Config> ().Where (c => c.Key == key).FirstOrDefault();
		}

		public string GetValue(string key, string defaultValue = ""){
			var c = GetData (key);
			return c == null ? defaultValue : c.Value;
		}

		/// <summary>
		/// Insert new record or update record same key
		/// </summary>
		/// <returns><c>true</c>, if data was saved, <c>false</c> otherwise.</returns>
		/// <param name="key">Key.</param>
		/// <param name="value">Value.</param>
		public bool SaveData(string key, string value) {
			value = value ?? string.Empty;

			var config = GetData (key);
			if (config != null) {
				config.Value = value;
				return Repository.Save<Config> (config) > 0;
			}
			// New config
			int id = Repository.Save<Config> (new Config (){ 
				Key = key,
				Value = value
			});
			return id > 0;
		}

		/// <summary>
		/// Insert new record or update record same key
		/// </summary>
		/// <returns><c>true</c>, if data was saved, <c>false</c> otherwise.</returns>
		/// <param name="config">Config.</param>
		public bool SaveData(Config config) {
			var existConfig = GetData (config.Key);
			if (existConfig != null) {
				existConfig.Value = config.Value;
				return Repository.Save<Config> (existConfig) > 0;
			}
			// New config
			int id = Repository.Save<Config> (config);
			return id > 0;
		}
	}
}

