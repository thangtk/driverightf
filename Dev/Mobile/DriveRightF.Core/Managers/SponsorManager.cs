﻿using System;
using DriveRightF.Core.Models;
using System.Collections.Generic;
using Unicorn.Core;
using System.Threading;
using DriveRightF.Core.Enums;
using DriveRightF.Core.Constants;
using System.Threading.Tasks;
using DriveRightF.Core.Services;
using DriveRightF.Services.Responses;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.Core.Managers.SponsorManager))]
namespace DriveRightF.Core.Managers
{
	public class SponsorManager : BaseAutoRequestManager<Sponsor>
	{
		private bool _isFirstTime = true;
		private int _itemCount = 0;
		public SponsorManager ()
		{
			// TEST
//			Repository.DeleteAll<Sponsor>();
		}

		#region implemented abstract members of BaseAutoRequestManager

		public Sponsor Get(string sponsorId)
		{
			Sponsor sponsor = Repository.Get<Sponsor> (x => x.SponsorID == sponsorId);
			return sponsor;
		}

		public override List<Sponsor> GetDataFromServer ()
		{
//			return null;
			var data = ServiceManager.GetSponsors ();
			if(data != null){
				// Save data into db
				FilterItems(data);
				SaveData(data);
				// Update request time
				UpdateLastTimeRequest();
			}
			//
			return data;
//			Debugger.ShowThreadInfo ("Begin request");
//			Thread.Sleep (3000);
//			// TEST
//			var responseData = new List<Sponsor> ();
//			int countDB = Repository.GetAll<Sponsor> ().Count;
//			int count = DateTime.Now.Millisecond % 5;
//			count = 20;
////			for (int i = 1; i <= count; i++) {
////				responseData.Add (new Sponsor () {
////					Name = "Sponsor " + ++_count,
////					Icon = "award_thunderbolt.png",
////					Status = i%2== 0 ? SponsorStatus.Modify:SponsorStatus.View,
////					Type = i%2==0? SponsorType.Insurer : SponsorType.None,
////					SponsorID = (countDB + i).ToString(),
////					ItemStatus = ItemStatus.New
////				});
////			}
//			//
//			bool success = true; // Fake
//			if(success){
//				// Save data into db
//				SaveData(responseData);
//				// Update request time
//				UpdateLastTimeRequest();
//			} else {
//				// QUESTION: How to nofify error???
//				return null;
//			}
//			//
//			Debugger.ShowThreadInfo ("End request");
//			//
//			return responseData;
		}

		public override string LastUpdatedKey {
			get {
				return ConfigKey.SPONSOR_LAST_UPDATED;
			}
		}

		public override long AllowTime {
			get {
				return AutoRequestTime.REQUEST_TIME_SPONSOR;
			}
		}

		#endregion
	}
}

