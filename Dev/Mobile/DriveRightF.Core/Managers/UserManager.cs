﻿using System;
using DriveRightF.Core.Models;
using DriveRightF.Core.Constants;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.Core.Managers.UserManager))]
namespace DriveRightF.Core.Managers
{
	public class UserManager : BaseManager
	{
		public UserManager () : base()
		{
		}

		public string GetImage(string defaultImage)
		{
			return ConfigManager.GetValue (ConfigKey.PROFILE_IMAGE, defaultImage);
		}

		public User GetUser()
		{
			int age = 0;
			int.TryParse (ConfigManager.GetValue (ConfigKey.PROFILE_AGE), out age);

			return new User () {
				Name = ConfigManager.GetValue (ConfigKey.PROFILE_NAME),
				PhoneNumber = ConfigManager.GetValue (ConfigKey.PROFILE_PHONE_NUMBER),
				SocialSecurityNo = ConfigManager.GetValue (ConfigKey.PROFILE_SOCIAL_SECURITY_NUMBER),
				Image = ConfigManager.GetValue(ConfigKey.PROFILE_IMAGE),
				ImageURL = ConfigManager.GetValue(ConfigKey.PROFILE_IMAGE_URL),
				Age = age,
				DOB = ConfigManager.GetValue(ConfigKey.PROFILE_DOB),
				Password = ConfigManager.GetValue (ConfigKey.PROFILE_PASSWORD),
				WeChat = ConfigManager.GetValue (ConfigKey.PROFILE_WECHAT)
			};
		}

		public void Save(User user)
		{
			if (user == null)
				return;
				
			ConfigManager.SaveData (ConfigKey.PROFILE_NAME, user.Name);
			ConfigManager.SaveData (ConfigKey.PROFILE_PHONE_NUMBER, user.PhoneNumber);
			ConfigManager.SaveData (ConfigKey.PROFILE_SOCIAL_SECURITY_NUMBER, user.SocialSecurityNo);
			ConfigManager.SaveData (ConfigKey.PROFILE_IMAGE, user.Image);
			ConfigManager.SaveData (ConfigKey.PROFILE_IMAGE_URL, user.ImageURL);
			ConfigManager.SaveData (ConfigKey.PROFILE_AGE, user.Age.ToString());
			ConfigManager.SaveData (ConfigKey.PROFILE_DOB, user.DOB.ToString());
			ConfigManager.SaveData (ConfigKey.PROFILE_PASSWORD, user.Password);
			ConfigManager.SaveData (ConfigKey.PROFILE_WECHAT, user.WeChat);
		}

		public bool CheckLogin()
		{

			var prof = ProfileManager.Profile;
			if (prof == null) {
				return false;
			}
			return true;
		}

		public bool Login(string customerId){
            var userId = ServiceManager.Login(customerId);

            // TEST
            //var userId = "3ee38768-d17a-4dcc-b9fa-76d7c31acc33";
			if (!string.IsNullOrEmpty (userId)) {
                //ConfigManager.SaveData (ConfigKey.APP_USER_ID, userId);
                Profile profile = new Profile();
                profile.ProfileID = userId;
                profile.PhoneNumber = customerId;
                ProfileManager.Save(profile);
				ServiceManager.UserId = userId; // bonus chapter 2
				return true;
			}
			return false;
		}

		public bool Register(string customerId){
			var userId = ServiceManager.Register (customerId);
			if (!string.IsNullOrEmpty (userId)) {
				ConfigManager.SaveData (ConfigKey.APP_USER_ID, userId);
				ServiceManager.UserId = userId; // bonus chapter 2
				return true;
			}
			return false;
		}

//		public async Task<UpdateProfileResponse> UpdateProfile(User user)
//		{
//			DeviceInfo deviceInfo = DependencyService.Get<DeviceManager> ().GetDeviceInfo ();
//			UpdateProfileRequest request = new UpdateProfileRequest () {
////				DeviceInfo = deviceInfo,
//				User = user
//			};
//			UpdateProfileResponse response = await ServiceManager.UpdateProfile(request);
//
//			return response;
//		}

		public User GetProfile(){
			// REPLACE BY DEMO DATA
//			var user = ServiceManager.GetProfile ();
//			if (user != null) {
//				Save (user);
////				ConfigManager.SaveData (ConfigKey.PROFILE_EXIST, "1");
//			}
//			ConfigManager.SaveData (ConfigKey.PROFILE_SYNCED, "1");
//			return user;
			System.Threading.Thread.Sleep(5000);
			return null;
		}

		/// <summary>
		/// Fake with func param
		/// TODO: pass image data as param
		/// </summary>
		/// <returns><c>true</c>, if profile was updated, <c>false</c> otherwise.</returns>
		/// <param name="profile">Profile.</param>
		/// <param name="saveImageFunc">Save image func.</param>
		public bool UpdateProfile(User profile, Func<string> saveImageFunc){
			// REM for demo
//			var profileId = ServiceManager.UpdateProfile (profile);
			// main thread or not???
			string x = saveImageFunc.Invoke ();
			if(!String.IsNullOrEmpty(x))
				profile.Image = x;
			Save (profile);
			return true; // It's always true
		}

		public bool ExistProfile(){
			return !string.IsNullOrEmpty (ConfigManager.GetValue (ConfigKey.PROFILE_SYNCED));
		}

		/// <summary>
		/// Gets the customer reference. It will use for start SDK service.
		/// </summary>
		/// <returns>The customer reference.</returns>

		public string GetCustomerRef()
		{
			return  ConfigManager.GetValue (ConfigKey.CUSTOMER_REF);
		}

		public void SaveCustomerRef(string customerRef)
		{
			ConfigManager.SaveData (ConfigKey.CUSTOMER_REF, customerRef);
		}

	}
}

