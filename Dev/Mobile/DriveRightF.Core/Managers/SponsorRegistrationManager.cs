﻿using System;
using System.Threading.Tasks;
using DriveRightF.Core.Services;
using DriveRightF.Core.Models;
using DriveRightF.Services.Responses;
using DriveRightF.Services.Requests;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.Core.Managers.SponsorRegistrationManager))]
namespace DriveRightF.Core.Managers
{
	public class SponsorRegistrationManager : BaseManager
	{
		public SponsorRegistration GetSponsorRegistration(string sponsorId){
			var result = ServiceManager.GetSponsorRegistration (sponsorId);
			if (result != null) {
				// Save data into database
				SaveInsurance(result.Insurance);
				SaveDriver (result.DriverLicense);
				SaveCar (result.CarRegistration);
			}
			return result;
		}

		public bool UpdateSponsorRegistration(SponsorRegistration contract){
			ServiceManager.UpdateSponsorRegistration(contract);
			return true;
		}

//		public async Task<UpdateSponsorRegistrationResponse> UpdateSponsorRegistration( SponsorRegistration dataRequest)
//		{
//			UpdateSponsorRegistrationRequest request = new UpdateSponsorRegistrationRequest () {
//				SponsorRegistration = dataRequest
//			};
//
////			UpdateSponsorRegistrationResponse response = await ServiceManager.UpdateSponsorRegistration (request);
//			// TODO: update
//			return new UpdateSponsorRegistrationResponse(){
//				IsSuccess = true
//			};
//		}

		public void Save(SponsorRegistration data, string sponsorId = "")
		{
			if (data != null) {
				var car = data.CarRegistration;
				var driver = data.DriverLicense;
				var insurance = data.Insurance;

				if (!string.IsNullOrEmpty (sponsorId)) {
					car.SponsorId = sponsorId;
					driver.SponsorId = sponsorId;
					insurance.SponsorId = sponsorId;
				}

				if (car != null) {
					SaveCar (car);
				}

				if (driver != null) {
					SaveDriver (driver);
				}

				if (insurance != null) {
					SaveInsurance (insurance);
				}
			}
		}

		public SponsorRegistration Get(string sponsorId)
		{
			var car = GetCar (sponsorId);
			var driver = GetDriver (sponsorId);
			var insurance = GetInsurance (sponsorId);

			if (car == null && driver == null && insurance == null) {
				return null;
			} else {
				return new SponsorRegistration (){ 
					CarRegistration = car,
					DriverLicense = driver,
					Insurance = insurance,
				};
			}
		}

//		public async Task<GetSponsorRegistrationResponse> GetAsync(string sponsorId)
//		{
//			GetSponsorRegistrationRequest request = new GetSponsorRegistrationRequest()
//			{
//				SponsorId = sponsorId
//			};
//
////			GetSponsorRegistrationResponse response = await ServiceManager.GetSponsorRegistration (request);
//			// TODO:
//			return new GetSponsorRegistrationResponse(){
//				
//			};
//		}

		public void SaveCar(CarRegistration car)
		{
			var carLocal = GetCar (car.SponsorId);
			if (carLocal != null) {
				car.Id = carLocal.Id;
			}
			Repository.Save<CarRegistration> (car);
		}

		public void SaveDriver(DriverLicense driver)
		{
			var driverLocal = GetDriver (driver.SponsorId);
			if (driverLocal != null) {
				driver.Id = driverLocal.Id;
			}
			Repository.Save<DriverLicense> (driver);
		}

		public void SaveInsurance(Insurance insurance)
		{
			var insuranceLocal = GetInsurance (insurance.SponsorId);
			if (insuranceLocal != null) {
				insurance.Id = insuranceLocal.Id;
			}
			Repository.Save<Insurance> (insurance);
		}

		public CarRegistration GetCar(string sponsorId)
		{
			return Repository.Get<CarRegistration> (x => x.SponsorId == sponsorId);
		}

		public DriverLicense GetDriver(string sponsorId)
		{
			return Repository.Get<DriverLicense> (x => x.SponsorId == sponsorId);
		}

		public Insurance GetInsurance(string sponsorId)
		{
			return Repository.Get<Insurance> (x => x.SponsorId == sponsorId);
		}
			
	}
}

