﻿using System;
using System.Drawing;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.Core.ScoreManager))]
namespace DriveRightF.Core
{
	public class ScoreManager
	{

		public Color GetColorByScore(float score)
		{
			Color c;
			if(score < 0 || score > 100)
			{
				throw new Exception ("Score must be between 0 and 100");
			}
			//
			if(score < 60)
			{
				c = Color.FromArgb(247, 107, 107); // Color.FromArgb(242, 129, 149);
			}
			else if(score < 80)
			{
				c = Color.FromArgb(255, 189, 82); // Color.FromArgb (255, 143, 69);
			}
			else if(score <= 100)
			{
				c = Color.FromArgb(158, 220, 107); // Color.FromArgb (115, 206, 235);
			}
			else
			{
				c = Color.Black;
			}
			//
			return c;
		}

	}
}

