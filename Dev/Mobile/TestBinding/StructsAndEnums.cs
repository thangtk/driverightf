﻿using System;

namespace TestBinding
{
	public enum TestBindingStatus
	{
		NO_INTERNET, // NO 3G, NO WIFI

		INVALID_CUSTOMER_ID, // CUSTOMERID with spaces, too long customerId

		SYSTEM_ERROR,

		NETWORK_TIMEOUT,

		START_SUCCESS
	}
}

