//
//  TestDelegate.h
//  TestBinding
//
//  Created by unicorn-kyhomac on 3/31/15.
//  Copyright (c) 2015 unicorn-kyhomac. All rights reserved.
//
#import "PolaraxResponse.h"

typedef void (^handlePolaraxResponse)(TestReponse * response);

@protocol TestDelegate <NSObject>
// list of methods and properties
+(void)LogTestDelegate;
+(void)versionDelegate;
+(void)handleBlock: (handlePolaraxResponse) handleBlock;
@end

