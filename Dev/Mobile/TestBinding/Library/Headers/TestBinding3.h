//
//  TestBinding3.h
//  TestBinding
//
//  Created by unicorn-kyhomac on 3/31/15.
//  Copyright (c) 2015 unicorn-kyhomac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TestDelegate.h"

@interface TestBinding3 : NSObject<TestDelegate>

@end
