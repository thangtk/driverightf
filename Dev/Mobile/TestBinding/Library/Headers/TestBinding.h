//
//  TestBinding.h
//  TestBinding
//
//  Created by unicorn-kyhomac on 3/27/15.
//  Copyright (c) 2015 unicorn-kyhomac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TestBinding : NSObject
+(void)writeTest;
+(void)writeTest: (NSString *) msgTest;
+(NSString *)getTest;

@end
