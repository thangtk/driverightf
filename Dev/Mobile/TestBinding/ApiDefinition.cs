﻿using System;

using UIKit;
using Foundation;
using ObjCRuntime;
using CoreGraphics;

namespace TestBinding
{
	// The first step to creating a binding is to add your native library ("libNativeLibrary.a")
	// to the project by right-clicking (or Control-clicking) the folder containing this source
	// file and clicking "Add files..." and then simply select the native library (or libraries)
	// that you want to bind.
	//
	// When you do that, you'll notice that MonoDevelop generates a code-behind file for each
	// native library which will contain a [LinkWith] attribute. MonoDevelop auto-detects the
	// architectures that the native library supports and fills in that information for you,
	// however, it cannot auto-detect any Frameworks or other system libraries that the
	// native library may depend on, so you'll need to fill in that information yourself.
	//
	// Once you've done that, you're ready to move on to binding the API...
	//
	//
	// Here is where you'd define your API definition for the native Objective-C library.
	//
	// For example, to bind the following Objective-C class:
	//
	//     @interface Widget : NSObject {
	//     }
	//
	// The C# binding would look like this:
	//
	//     [BaseType (typeof (NSObject))]
	//     interface Widget {
	//     }
	//
	// To bind Objective-C properties, such as:
	//
	//     @property (nonatomic, readwrite, assign) CGPoint center;
	//
	// You would add a property definition in the C# interface like so:
	//
	//     [Export ("center")]
	//     CGPoint Center { get; set; }
	//
	// To bind an Objective-C method, such as:
	//
	//     -(void) doSomething:(NSObject *)object atIndex:(NSInteger)index;
	//
	// You would add a method definition to the C# interface like so:
	//
	//     [Export ("doSomething:atIndex:")]
	//     void DoSomething (NSObject object, int index);
	//
	// Objective-C "constructors" such as:
	//
	//     -(id)initWithElmo:(ElmoMuppet *)elmo;
	//
	// Can be bound as:
	//
	//     [Export ("initWithElmo:")]
	//     IntPtr Constructor (ElmoMuppet elmo);
	//
	// For more information, see http://developer.xamarin.com/guides/ios/advanced_topics/binding_objective-c/
	//



	[Model]
	[BaseType (typeof (NSObject))]
	interface TestDelegate
	{
		[Static]
		[Export ("LogTestDelegate")]
		void LogTestDelegate();

		[Static]
		[Export ("versionDelegate")]
		void versionDelegate();

		[Static]
		[Export ("handleBlock:")]
		void handleBlock(handleResponse response);

	}

//	[BaseType (typeof (NSObject))]
//	interface TestBinding {
//
//		[Static]
//		[Export ("writeTest")]
//		void WriteTest();
//
//		[Static]
//		[Export ("writeTest:")]
//		void WriteTest (string msgTest);
//
//		[Static]
//		[Export ("getTest")]
//		string GetTest ();
//	}

	[BaseType (typeof (NSObject))]
	interface TestBinding1 : TestDelegate
	{
		[Static]
		[Export ("methodTestBinding1")]
		void methodTestBinding1();
	}

	[BaseType (typeof (NSObject))]
	interface TestBinding2 : TestDelegate
	{
		[Static]
		[Export ("methodTestBinding2")]
		void methodTestBinding2();
	}

	[BaseType (typeof (NSObject))]
	interface TestBinding3 : TestDelegate
	{
	}

	[BaseType (typeof (NSObject))]
	interface TestReponse {

		[Export ("statusCode")]
		TestBindingStatus StatusCode {get;set;}

		[Export ("statusMessage")]
		string StatusMessage {get;set;}

		[Static,Export ("getResponse:")]
		TestReponse getResponse( TestBindingStatus status);
	}

	delegate void handleResponse(TestReponse response);
}

