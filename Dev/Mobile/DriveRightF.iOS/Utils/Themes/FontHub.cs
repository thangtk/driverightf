﻿using System;
using UIKit;
using Foundation;

namespace DriveRightF.iOS
{
	public static class FontHub
	{
		public const string HelveticaHvCn = "HelveticaNeueLTStd-HvCn";
		public const string HelveticaMedium = "HelveticaNeueLTStd-Md";
		public const string HelveticaThin = "HelveticaNeueLTStd-Th";
		public const string HelveticaLt = "HelveticaNeueLTStd-Lt";
		public const string HelveticaCn = "HelveticaNeueLTStd-Cn";

		public static UIFont GetFont(string fontName, float fontSize)
		{
			return UIFont.FromName(fontName, fontSize);
		}

		public static readonly UIFont HelveticaHvCn13 = UIFont.FromName("HelveticaNeueLTStd-HvCn", 13f);
		public static readonly UIFont HelveticaHvCn15 = UIFont.FromName("HelveticaNeueLTStd-HvCn", 15f);
		public static readonly UIFont HelveticaHvCn17 = UIFont.FromName("HelveticaNeueLTStd-HvCn", 17f);
		public static readonly UIFont HelveticaHvCn18 = UIFont.FromName("HelveticaNeueLTStd-HvCn", 18f);
		public static readonly UIFont HelveticaHvCn20 = UIFont.FromName("HelveticaNeueLTStd-HvCn", 20f);
		public static readonly UIFont HelveticaHvCn22 = UIFont.FromName("HelveticaNeueLTStd-HvCn", 22f);
		public static readonly UIFont HelveticaHvCn28 = UIFont.FromName("HelveticaNeueLTStd-HvCn", 28f);
		public static readonly UIFont HelveticaHvCn30 = UIFont.FromName("HelveticaNeueLTStd-HvCn", 30f);

		public static readonly UIFont HelveticaMedium11 = UIFont.FromName("HelveticaNeueLTStd-Md", 11f);
		public static readonly UIFont HelveticaMedium13 = UIFont.FromName("HelveticaNeueLTStd-Md", 13f);
		public static readonly UIFont HelveticaMedium15 = UIFont.FromName("HelveticaNeueLTStd-Md", 15f);
		public static readonly UIFont HelveticaMedium17 = UIFont.FromName("HelveticaNeueLTStd-Md", 17f);
		public static readonly UIFont HelveticaMedium22 = UIFont.FromName("HelveticaNeueLTStd-Md", 22f);

		public static readonly UIFont HelveticaThin11 = UIFont.FromName("HelveticaNeueLTStd-Th", 11f);
		public static readonly UIFont HelveticaThin13 = UIFont.FromName("HelveticaNeueLTStd-Th", 13f);
		public static readonly UIFont HelveticaThin15 = UIFont.FromName("HelveticaNeueLTStd-Th", 15f);
		public static readonly UIFont HelveticaThin17 = UIFont.FromName("HelveticaNeueLTStd-Th", 17f);
		public static readonly UIFont HelveticaThin20 = UIFont.FromName("HelveticaNeueLTStd-Th", 20f);

		public static readonly UIFont HelveticaLt8 = UIFont.FromName("HelveticaNeueLTStd-Lt", 8f);
		public static readonly UIFont HelveticaLt11 = UIFont.FromName("HelveticaNeueLTStd-Lt", 11f);
		public static readonly UIFont HelveticaLt13 = UIFont.FromName("HelveticaNeueLTStd-Lt", 13f);
		public static readonly UIFont HelveticaLt15 = UIFont.FromName("HelveticaNeueLTStd-Lt", 15f);
		public static readonly UIFont HelveticaLt16 = UIFont.FromName("HelveticaNeueLTStd-Lt", 16f);
		public static readonly UIFont HelveticaLt17 = UIFont.FromName("HelveticaNeueLTStd-Lt", 17f);
		public static readonly UIFont HelveticaLt20 = UIFont.FromName("HelveticaNeueLTStd-Lt", 20f);

		public static readonly UIFont HelveticaCn8 = UIFont.FromName("HelveticaNeueLTStd-Cn", 8f);
		public static readonly UIFont HelveticaCn14 = UIFont.FromName("HelveticaNeueLTStd-Cn", 14f);
		public static readonly UIFont HelveticaCn17 = UIFont.FromName("HelveticaNeueLTStd-Cn", 17f);
		public static readonly UIFont HelveticaCn20 = UIFont.FromName("HelveticaNeueLTStd-Cn", 20f);
		public static readonly UIFont HelveticaCn25 = UIFont.FromName("HelveticaNeueLTStd-Cn", 25f);

		//		public static UIFont HeaderFont(float fontSize = 17f)
		//		{
		//			NSArray languages = (NSArray)NSUserDefaults.StandardUserDefaults.ValueForKey(new NSString("AppleLanguages"));
		//			NSString language = languages.GetItem<NSString>(0);
		//
		//			switch(language)
		//			{
		//			case "vi":
		//				return UIFont.FromName("HelveticaNeueLTStd-Md", fontSize);
		//			default:
		//				return UIFont.FromName("HelveticaNeueLTStd-Md", fontSize);
		//			}
		//		}
		//
		//		public static UIFont ButtonFont(float fontSize = 16f)
		//		{
		//			NSArray languages = (NSArray)NSUserDefaults.StandardUserDefaults.ValueForKey(new NSString("AppleLanguages"));
		//			NSString language = languages.GetItem<NSString>(0);
		//
		//			switch(language)
		//			{
		//			case "vi":
		//				return UIFont.FromName("Roboto-Light", fontSize);
		//			default:
		//				return UIFont.FromName("Roboto-Light", fontSize);
		//			}
		//		}
		//
		//		public static UIFont CoachmaskFont(float fontSize)
		//		{
		//			return UIFont.FromName("Roboto-Light", fontSize);
		//		}
		//
		//		public static UIFont CaptionGetStartedFont(float fontSize = 30f)
		//		{
		//			return UIFont.FromName("Roboto-Light", fontSize);
		//		}
	}
}

