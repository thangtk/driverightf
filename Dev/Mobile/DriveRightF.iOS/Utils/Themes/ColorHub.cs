﻿using System;
using UIKit;
using Foundation;

namespace DriveRightF.iOS
{
	public static class ColorHub
	{
		public static UIColor ColorFromHex(string hex)
		{
			if (String.IsNullOrEmpty (hex)) return UIColor.Clear;

			int[] color = new int[3];

			switch (hex.Length)
			{
				case 3:
					color [0] = Convert.ToInt32 (hex.Substring (0, 1), 16);
					color [1] = Convert.ToInt32 (hex.Substring (1, 1), 16);
					color [2] = Convert.ToInt32 (hex.Substring (2, 1), 16);
					break;
				case 6:
					color [0] = Convert.ToInt32 (hex.Substring (0, 2), 16);
					color [1] = Convert.ToInt32 (hex.Substring (2, 2), 16);
					color [2] = Convert.ToInt32 (hex.Substring (4, 2), 16);
					break;
				default:
					break;
			}

			return UIColor.FromRGB (color [0], color [1], color [2]);
		}
	}
}

