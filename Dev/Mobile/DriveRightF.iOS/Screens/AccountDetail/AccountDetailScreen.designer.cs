// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("AccountDetailScreen")]
	partial class AccountDetailScreen
	{
		[Outlet]
		UIKit.UIButton btnGetDiscount { get; set; }

		[Outlet]
		UIKit.UIButton btnPay { get; set; }

		[Outlet]
		UIKit.UILabel lblCash1 { get; set; }

		[Outlet]
		UIKit.UILabel lblCash2 { get; set; }

		[Outlet]
		UIKit.UILabel lblPayCash { get; set; }

		[Outlet]
		UIKit.UILabel lblPremiumAccount { get; set; }

		[Outlet]
		UIKit.UILabel lblPremiumDiscount { get; set; }

		[Outlet]
		UIKit.UILabel lblRMB { get; set; }

		[Outlet]
		UIKit.UILabel lblValueOfCash1 { get; set; }

		[Outlet]
		UIKit.UILabel lblValueOfCash2 { get; set; }

		[Outlet]
		UIKit.UILabel lblValueOfPremiumAccount { get; set; }

		[Outlet]
		UIKit.UILabel lblValueOfPremiumDiscount { get; set; }

		[Outlet]
		UIKit.UISlider sldCash { get; set; }

		[Outlet]
		UIKit.UISlider sldPremiumDiscount { get; set; }

		[Outlet]
		UIKit.UITextField txtValueOfCash2 { get; set; }

		[Outlet]
		UIKit.UIView viewContent { get; set; }

		[Outlet]
		UIKit.UIView viewPaycash { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnGetDiscount != null) {
				btnGetDiscount.Dispose ();
				btnGetDiscount = null;
			}

			if (btnPay != null) {
				btnPay.Dispose ();
				btnPay = null;
			}

			if (lblCash1 != null) {
				lblCash1.Dispose ();
				lblCash1 = null;
			}

			if (lblCash2 != null) {
				lblCash2.Dispose ();
				lblCash2 = null;
			}

			if (lblPayCash != null) {
				lblPayCash.Dispose ();
				lblPayCash = null;
			}

			if (lblPremiumAccount != null) {
				lblPremiumAccount.Dispose ();
				lblPremiumAccount = null;
			}

			if (lblPremiumDiscount != null) {
				lblPremiumDiscount.Dispose ();
				lblPremiumDiscount = null;
			}

			if (lblRMB != null) {
				lblRMB.Dispose ();
				lblRMB = null;
			}

			if (lblValueOfCash1 != null) {
				lblValueOfCash1.Dispose ();
				lblValueOfCash1 = null;
			}

			if (lblValueOfCash2 != null) {
				lblValueOfCash2.Dispose ();
				lblValueOfCash2 = null;
			}

			if (lblValueOfPremiumAccount != null) {
				lblValueOfPremiumAccount.Dispose ();
				lblValueOfPremiumAccount = null;
			}

			if (lblValueOfPremiumDiscount != null) {
				lblValueOfPremiumDiscount.Dispose ();
				lblValueOfPremiumDiscount = null;
			}

			if (sldCash != null) {
				sldCash.Dispose ();
				sldCash = null;
			}

			if (sldPremiumDiscount != null) {
				sldPremiumDiscount.Dispose ();
				sldPremiumDiscount = null;
			}

			if (txtValueOfCash2 != null) {
				txtValueOfCash2.Dispose ();
				txtValueOfCash2 = null;
			}

			if (viewPaycash != null) {
				viewPaycash.Dispose ();
				viewPaycash = null;
			}

			if (viewContent != null) {
				viewContent.Dispose ();
				viewContent = null;
			}
		}
	}
}
