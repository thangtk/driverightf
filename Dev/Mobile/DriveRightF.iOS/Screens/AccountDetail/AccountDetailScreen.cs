﻿using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRight.Core.ViewModels;
using CoreGraphics;
using DriveRightF.Core.ViewModels;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using DriveRightF.Core.Enums;
using Unicorn.Core;

using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using Unicorn.Core.UI;
using Unicorn.Core.Navigator;
using System.Threading;
using System.Threading.Tasks;
using CoreAnimation;
using Unicorn.Core.Enums;

namespace DriveRightF.iOS
{
	public partial class AccountDetailScreen :BaseHandlerHeaderViewController<AccountDetailViewModel>
	{
		private GlanceLoadingView _loadingView;
		public AccountDetailScreen() 
		{
		}
			
		public override void ViewDidLoad()
		{
			base.ViewDidLoad ();
			InitFont ();
			InitControl ();
			ViewModel = new AccountDetailViewModel ();
			CreateBindingsAsync ();
			InitBindCommands ();
			InitTranslator ();
		}

		private void InitControl()
		{
			txtValueOfCash2.ShouldBeginEditing += (textField) => { 

				//TODO: fix temp. prevent click button and slide processing
				sldCash.UserInteractionEnabled = false;
				btnPay.UserInteractionEnabled = false;

				ViewModel.WithDrawCashPercent = 0;
//				textField.Text = string.Empty;
				ViewModel.CashInputValue = string.Empty;
				return true; 
			};
			txtValueOfCash2.ShouldEndEditing += (textField) => { 

				//TODO: Temp prevent click button and slide processing
				sldCash.UserInteractionEnabled = true;
				btnPay.UserInteractionEnabled = true;


				//TODO check valid number of text value.
				// Need move to viewmodel.

				ViewModel.HandleEndEditingCash(textField.Text);

				return true; 
			};
			txtValueOfCash2.ShouldReturn += (textField) => { 
				textField.ResignFirstResponder ();
				return true; 
			};

			txtValueOfCash2.KeyboardType = UIKeyboardType.DecimalPad;
		
			View.AddGestureRecognizer(new UITapGestureRecognizer (() =>
			{
				View.EndEditing(true);
			}));
		}

		private void CreateBindingsAsync ()
		{

			RxApp.TaskpoolScheduler.Schedule(()=>{
				InitBindings ();
			});
		}


		private void InitBindings()
		{

			ViewModel.WhenAnyValue (vm => vm.BalanceCashValue).Where (x => x != null).ObserveOn (RxApp.MainThreadScheduler).Subscribe (x => lblValueOfCash1.Text = x.ToString());
			ViewModel.WhenAnyValue (vm => vm.PremiumAccountValue).Where (x => x != null).ObserveOn (RxApp.MainThreadScheduler).Subscribe (x => lblValueOfPremiumAccount.Text = x.ToString());
			ViewModel.WhenAnyValue (vm => vm.PremiumDiscountPercent).Where (x => x != null).ObserveOn (RxApp.MainThreadScheduler).Subscribe (x => sldPremiumDiscount.Value = x);
			ViewModel.WhenAnyValue (vm => vm.PremiumDiscountValue).ObserveOn (RxApp.MainThreadScheduler).Subscribe  (v => lblValueOfPremiumDiscount.Text = string.Format ("{0:N2} {1}",v,  Translator.Translate("RMB")));			

			ViewModel.WhenAnyValue (vm => vm.WithDrawCashPercent)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Where (x => x != sldCash.Value)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => sldCash.Value = x);

			ViewModel.WhenAnyValue (vm => vm.CashInputValue)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => 
					{
						//						txtValueOfCash2.Text = string.Format ("{0:N2}", x);
						txtValueOfCash2.Text = x;
					});

			ViewModel.WhenAnyValue (vm => vm.WithDrawCashValue)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Where(x => sldCash.UserInteractionEnabled)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
					v => {
						txtValueOfCash2.Text = string.Format ("{0:N2}", v);
						lblRMB.Text = Translator.Translate("RMB");
					});

		}

		private void InitBindCommands()
		{
			btnPay.TouchUpInside += (object sender, EventArgs e) => 
				ViewModel.BtnPayClick.Execute (new Action<bool> (b => {
					if (b) DependencyService.Get<IHomeTabNavigator> ().CurrentTab.Navigate((int)Screen.Profile);
				else
				{
					AddLoadingView ();
				}
			}));

			btnGetDiscount.TouchUpInside += (sender, e) => {
				View.EndEditing(true);
				ViewModel.BtnGetDiscountClick.Execute(null);
			};
			ViewModel.LoadDataCommand.Execute (null);
			// Chieu V => VM

			sldCash.ValueChanged += (object sender, EventArgs e) => 
			{
				ViewModel.WithDrawCashPercent = sldCash.Value;
			};
		}

//		private void InitBinding()
//		{
//			ViewModel = new AccountDetailViewModel ();
//
//			this.OneWayBind (ViewModel, vm => vm.BalanceCashValue, v => v.lblValueOfCash1.Text);
//			this.OneWayBind (ViewModel, vm => vm.PremiumAccountValue, v => v.lblValueOfPremiumAccount.Text);
//			this.OneWayBind (ViewModel, vm => vm.PremiumAccountValue, v => v.lblValueOfPremiumAccount.Text);
//			this.OneWayBind (ViewModel, vm => vm.PremiumDiscountPercent, v => v.sldPremiumDiscount.Value);
//
//			ViewModel.WhenAnyValue (vm => vm.WithDrawCashValue)
//			.Where(x => sldCash.UserInteractionEnabled)
//			.Subscribe (
//	
//				v => {
//						txtValueOfCash2.Text = string.Format ("{0:N2} ", v);
//						lblRMB.Text = Translator.Translate("RMB");
//				});
//
//			ViewModel.WhenAnyValue (vm => vm.PremiumDiscountValue)
//				.Subscribe (v => lblValueOfPremiumDiscount.Text = string.Format ("{0:N2} {1}",v,  Translator.Translate("RMB")));			
//			
//			// Chieu V => VM
//			sldCash.ValueChanged += (object sender, EventArgs e) => 
//			{
//				ViewModel.WithDrawCashPercent = sldCash.Value;
//			};
//
//			// Chieu VM => V
//
//			ViewModel.WhenAnyValue (vm => vm.WithDrawCashPercent)
//				.Where (x => x != sldCash.Value)
//				.Subscribe (x => {
//				sldCash.Value = x;
//			});
//
//			btnPay.TouchUpInside += (object sender, EventArgs e) => 
//			ViewModel.BtnPayClick.Execute (new Action<bool> (b => {
//				if (b) DependencyService.Get<INavigator> ().Navigate ((int)Screen.Profile);
//				else
//				{
//					AddLoadingView ();
//				}
//			}));
//		
//			btnGetDiscount.TouchUpInside += (sender, e) => {
//				View.EndEditing(true);
//				ViewModel.BtnGetDiscountClick.Execute(null);
//			};
//			ViewModel.LoadDataCommand.Execute (null);
//		}

		private void InitFont()
		{
//			lblCash1.Font = FontHub.HelveticaCn17;
//			lblPremiumAccount.Font = FontHub.HelveticaCn17;
//			lblValueOfCash1.Font = FontHub.HelveticaHvCn17;
//			lblValueOfPremiumAccount.Font = FontHub.HelveticaHvCn17;

//			lblCash2.Font = FontHub.HelveticaCn17;
//			lblValueOfCash2.Font = FontHub.HelveticaCn17;
//			lblRMB.Font = FontHub.HelveticaCn17;
//			txtValueOfCash2.Font = FontHub.HelveticaCn17;
//			lblPremiumDiscount.Font = FontHub.HelveticaCn17;
//			lblValueOfPremiumDiscount.Font = FontHub.HelveticaCn17;

//			lblPayCash.Font = FontHub.HelveticaHvCn17;

//			btnPay.Font = FontHub.HelveticaLt15;
//			btnGetDiscount.Font = FontHub.HelveticaLt15;

//			sldCash.SetThumbImage (UIImage.FromFile ("icon_slider.png"), UIControlState.Normal);
//			sldPremiumDiscount.SetThumbImage (UIImage.FromFile ("icon_slider.png"), UIControlState.Normal);

			sldPremiumDiscount.UserInteractionEnabled = false;
		}

		private void InitTranslator()
		{
			lblPayCash.Text = Translator.Translate ("PAY_CASH");
			lblCash1.Text = Translator.Translate ("PAY_CASH_CASH");
			lblCash2.Text = Translator.Translate ("PAY_CASH_CASH");
			lblPremiumDiscount.Text = Translator.Translate ("PAY_CASH_PREMIUM_ACCOUNT");
			lblPremiumAccount.Text = Translator.Translate ("PAY_CASH_PREMIUM_ACCOUNT");
			btnPay.SetTitle (Translator.Translate ("BUTTON_PAY"), UIControlState.Normal);
			btnGetDiscount.SetTitle (Translator.Translate ("BUTTON_GET_DISOUNT"), UIControlState.Normal);
		}

		private void AddLoadingView ()
		{
			_loadingView = new GlanceLoadingView (View.Frame);
			_loadingView.LabelText = Translator.Translate("TRANSFERING");
			_loadingView.StartAnimating ();
			this.View.AddSubview (_loadingView);

			CountTime ();
		}

		private async void CountTime(int duration = 2000)
		{
			CALayer layer = new CALayer ();
			layer.BackgroundColor = UIColor.Black.CGColor;
			layer.Bounds = new CGRect (0,0, View.Frame.Width,View.Frame.Height);
			layer.Opacity = 0.2f;
			layer.Position = View.Center;
			View.Layer.AddSublayer (layer);

			await Task.Delay (duration);
			_loadingView.Hidden = true;
			if (ViewModel != null)
			{
				if (ViewModel.BalanceCashValue == 0) txtValueOfCash2.Text = "0.00";
				ViewModel.WithDrawCashPercent = 0;
			
				ViewModel.LoadAccountData.Execute (null);
				ViewModel.PopupManager.ShowToast(Translator.Translate("TRANSFER_SUCCESSFUL"), ToastLength.Long);
			}
			layer.RemoveFromSuperLayer ();
		}
	}
}
