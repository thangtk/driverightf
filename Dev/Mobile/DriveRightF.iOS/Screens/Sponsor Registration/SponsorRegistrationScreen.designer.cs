// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("SponsorRegistrationScreen")]
	partial class SponsorRegistrationScreen
	{
		[Outlet]
		UIKit.UIButton btnAddCar { get; set; }

		[Outlet]
		UIKit.UIButton btnAddDriver { get; set; }

		[Outlet]
		UIKit.UIButton btnAddPolicy { get; set; }

		[Outlet]
		UIKit.UIButton btnOk { get; set; }

		[Outlet]
		DriveRightF.Core.UI.UTextBox drtbCar { get; set; }

		[Outlet]
		DriveRightF.Core.UI.UTextBox drtbCarNo { get; set; }

		[Outlet]
		DriveRightF.Core.UI.UTextBox drtbDriverInsurer { get; set; }

		[Outlet]
		DriveRightF.Core.UI.UTextBox drtbDriverNo { get; set; }

		[Outlet]
		DriveRightF.Core.UDatePicker drtbPolicyExpiry { get; set; }

		[Outlet]
		DriveRightF.Core.UI.UTextBox drtbPolicyInsurer { get; set; }

		[Outlet]
		DriveRightF.Core.UI.UTextBox drtbPolicyNo { get; set; }

		[Outlet]
		DriveRightF.iOS.TitleHeaderView headerView { get; set; }

		[Outlet]
		UIKit.UILabel lblCar { get; set; }

		[Outlet]
		UIKit.UILabel lblDriver { get; set; }

		[Outlet]
		UIKit.UILabel lblPolicy { get; set; }

		[Outlet]
		UIKit.UIScrollView svScrollView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnAddCar != null) {
				btnAddCar.Dispose ();
				btnAddCar = null;
			}

			if (btnAddDriver != null) {
				btnAddDriver.Dispose ();
				btnAddDriver = null;
			}

			if (btnAddPolicy != null) {
				btnAddPolicy.Dispose ();
				btnAddPolicy = null;
			}

			if (btnOk != null) {
				btnOk.Dispose ();
				btnOk = null;
			}

			if (drtbCar != null) {
				drtbCar.Dispose ();
				drtbCar = null;
			}

			if (drtbCarNo != null) {
				drtbCarNo.Dispose ();
				drtbCarNo = null;
			}

			if (drtbDriverInsurer != null) {
				drtbDriverInsurer.Dispose ();
				drtbDriverInsurer = null;
			}

			if (drtbDriverNo != null) {
				drtbDriverNo.Dispose ();
				drtbDriverNo = null;
			}

			if (drtbPolicyInsurer != null) {
				drtbPolicyInsurer.Dispose ();
				drtbPolicyInsurer = null;
			}

			if (drtbPolicyExpiry != null) {
				drtbPolicyExpiry.Dispose ();
				drtbPolicyExpiry = null;
			}

			if (drtbPolicyNo != null) {
				drtbPolicyNo.Dispose ();
				drtbPolicyNo = null;
			}

			if (headerView != null) {
				headerView.Dispose ();
				headerView = null;
			}

			if (lblCar != null) {
				lblCar.Dispose ();
				lblCar = null;
			}

			if (lblDriver != null) {
				lblDriver.Dispose ();
				lblDriver = null;
			}

			if (lblPolicy != null) {
				lblPolicy.Dispose ();
				lblPolicy = null;
			}

			if (svScrollView != null) {
				svScrollView.Dispose ();
				svScrollView = null;
			}
		}
	}
}
