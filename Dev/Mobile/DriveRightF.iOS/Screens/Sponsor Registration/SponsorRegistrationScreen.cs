﻿
using System;
using System.Drawing;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using CoreGraphics;
using System.Reactive.Linq;
using DriveRightF.Core.UI;
using DriveRightF.Core.Enums;

namespace DriveRightF.iOS
{
	public partial class SponsorRegistrationScreen : DriveRightBaseViewController<SponsorRegistrationViewModel>
	{
		public SponsorRegistrationScreen () : base ("SponsorRegistrationScreen", null)
		{
		}
			
		private string _sponsorId;
		public override void LoadData (params object[] objs)
		{
			base.LoadData (objs); 
			if (objs != null && objs [0] != null) {
				_sponsorId = objs [0].ToString();
				if (ViewModel != null) {
					ViewModel.ResetData ();
					ViewModel.GetContractCommand.Execute (_sponsorId);
				}
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			ViewModel = new SponsorRegistrationViewModel ();
			ViewModel.Mode = ViewMode.Modify;
			ViewModel.SponsorId = _sponsorId;
			ViewModel.GetContractCommand.Execute (_sponsorId);

			InitControls ();
			InitBindings ();
		}
		private void InitControls()
		{
			lblPolicy.Font = FontHub.RobotoLight14;
			lblCar.Font = FontHub.RobotoLight14;
			lblDriver.Font = FontHub.RobotoLight14;

			svScrollView.PagingEnabled = false;
//			svScrollView.ContentSize = new CGSize(svScrollView.Frame.Width, svScrollView.Frame.Height);

//			HandleValueChanged ();

		}

//		private void HandleValueChanged()
//		{
//			foreach (UIView view in svScrollView) {
//				if (view is DriveRightTextBox) {
//					(view as DriveRightTextBox).ValueChanged += (object sender, string e) => 
//					{						ViewModel.IsEnableButton = true;
//					};
//
//				}
//			}
//
//		}

		private void InitBindings()
		{
			#region Binding Title
			//TODO Binding Title

			this.Bind(
				ViewModel,
				vm => vm.CarTitle,
				v => v.drtbCar.Title);

			this.OneWayBind(
				ViewModel,
				vm => vm.CarNoTitle,
				v => v.drtbCarNo.Title);

			this.OneWayBind(
				ViewModel,
				vm => vm.PolicyExpiryTitle,
				v => v.drtbPolicyExpiry.Title);

			this.OneWayBind(
				ViewModel,
				vm => vm.PolicyInsurerTitle,
				v => v.drtbPolicyInsurer.Title);

			this.OneWayBind(
				ViewModel,
				vm => vm.PolicyNoTitle,
				v => v.drtbPolicyNo.Title);
			this.OneWayBind(
				ViewModel,
				vm => vm.DriverNoTitle,
				v => v.drtbDriverNo.Title);

			this.OneWayBind(
				ViewModel,
				vm => vm.DriverInsurerTitle,
				v => v.drtbDriverInsurer.Title);

			this.OneWayBind(
				ViewModel,
				vm => vm.HeaderTitle,
				v => v.headerView.Title);

			ViewModel
				.WhenAnyValue(vm => vm.HeaderLogo)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(x => headerView.LogoImage = UIImage.FromFile(x));
			#endregion

			#region Binding Text
			this.Bind(
				ViewModel,
				vm => vm.Car,
				v => v.drtbCar.Text);
			this.Bind(
				ViewModel,
				vm => vm.CarNo,
				v => v.drtbCarNo.Text);
			this.Bind(
				ViewModel,
				vm => vm.DriverInsurer,
				v => v.drtbDriverInsurer.Text);
			this.Bind(
				ViewModel,
				vm => vm.DriverNo,
				v => v.drtbDriverNo.Text);
			this.Bind(
				ViewModel,
				vm => vm.PolicyExpiry,
				v => v.drtbPolicyExpiry.Text);
			this.Bind(
				ViewModel,
				vm => vm.PolicyInsurer,
				v => v.drtbPolicyInsurer.Text);
			this.Bind(
				ViewModel,
				vm => vm.PolicyNo,
				v => v.drtbPolicyNo.Text);

			this.BindCommand (
				ViewModel,
				vm => vm.UpdateContractCommand,
				v => v.btnOk);
			#endregion

			headerView.BackClicked	= new Action<object, EventArgs> (delegate(object arg1, EventArgs arg2) {
				View.EndEditing(true);
				ViewModel.BtnBackCliked.Execute (null);
			});

			ViewModel.WhenAnyValue (x => x.Car, x => x.CarNo, x => x.DriverInsurer, x => x.DriverNo, x => x.PolicyExpiry, x => x.PolicyNo, x => x.PolicyInsurer)
				.ObserveOn(RxApp.TaskpoolScheduler)
				.Subscribe(_ => ViewModel.CheckInsuranceChange.Execute(null));

			ViewModel.WhenAnyValue (vm => vm.IsAllPass)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => UpdateScrollView ());
			
			ViewModel
				.WhenAnyValue (vm => vm.CarError)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => drtbCar.Error = x);
			ViewModel
				.WhenAnyValue (vm => vm.CarNoError)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => drtbCarNo.Error = x);
			ViewModel
				.WhenAnyValue (vm => vm.DriverInsurerError)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => drtbDriverInsurer.Error = x);
			ViewModel
				.WhenAnyValue (vm => vm.DriverNoError)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => drtbDriverNo.Error = x);
			ViewModel
				.WhenAnyValue (vm => vm.PolicyExpiryError)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => drtbPolicyExpiry.Error = x);
			ViewModel
				.WhenAnyValue (vm => vm.PolicyInsurerError)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => drtbPolicyInsurer.Error = x);
			ViewModel
				.WhenAnyValue (vm => vm.PolicyNoError)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => drtbPolicyNo.Error = x);

			ViewModel
				.WhenAnyValue (vm => vm.IsEnableButton)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => 
					{
						Console.WriteLine(">>>> X: {0}", x.ToString());
						btnOk.Enabled = x;
					});
			ViewModel
				.WhenAnyValue (vm => vm.Mode)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => HandleDisableScreen (x == ViewMode.View));
		}

		private void UpdateScrollView()
		{
			svScrollView.ContentSize = new CGSize(svScrollView.Frame.Width, (svScrollView.Frame.Y + drtbCarNo.Frame.Y + drtbCarNo.Frame.Height));
		}

		private void HandleDisableScreen(bool isDisible)
		{
//			foreach (UIView view in svScrollView) {
//				if (view is UTextbox) {
//					(view as UTextbox).IsEnable = !isDisible;
//				} else if (view is UIButton){
//					(view as UIButton).Enabled = !isDisible;
//				}
//			}
//			btnOk.Enabled = !isDisible && ViewModel.IsEnableButton;
		}
	}
}

