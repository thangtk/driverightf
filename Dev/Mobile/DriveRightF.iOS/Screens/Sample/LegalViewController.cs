﻿
using System;
using System.Drawing;

using Foundation;
using UIKit;
using Unicorn.Core.UI;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using DriveRightF.Core;

namespace DriveRightF.iOS
{
	public partial class LegalViewController : BaseHandlerHeaderViewController<LegalViewModel>
	{
		public LegalViewController () : base ("LegalViewController", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			ViewModel = new LegalViewModel ();
			InitControl ();
		}

		private void InitControl()
		{
			btnBack.TouchUpInside += (sender, e) => DependencyService.Get<INavigator> ().NavigateBack ();
			InitTranslator ();
		}

		private void InitTranslator()
		{
			lblTitle.Text = Translator.Translate ("LEGAL_HEADER");
			lblInfoHeader.Text = Translator.Translate("LEGAL_INFO_HEADER");
			txtLegal.Text = Translator.Translate("LEGAL_INFO");
		}
	}
}

