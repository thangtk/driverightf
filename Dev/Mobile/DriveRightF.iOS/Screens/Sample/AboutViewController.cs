﻿
using System;
using System.Drawing;

using Foundation;
using UIKit;
using Unicorn.Core.UI;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using DriveRightF.Core;
using DriveRightF.Core.Enums;

namespace DriveRightF.iOS
{
	public partial class AboutViewController : BaseHandlerHeaderViewController<AboutViewModel>
	{
		public AboutViewController() : base("AboutViewController", null)
		{
		}

		public override void LoadData(params object[] objs)
		{
			base.LoadData(objs);
		}

		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			ViewModel = new AboutViewModel ();
			InitControl();
		}

		private void InitControl()
		{
			lblAppVersion.Text = string.Format("V {0}", NSBundle.MainBundle.InfoDictionary ["CFBundleVersion"]);

			tapProductIntroduction.AddTarget(() =>
			{
				DependencyService.Get<INavigator>().Navigate((int)Screen.ProductIntroduction, new object[0]);
		
			});

			tapDisclaimer.AddTarget(() =>
			{
				DependencyService.Get<INavigator>().Navigate((int)Screen.Disclaimer, new object[0]);
			});

			btnBack.TouchUpInside += (sender, e) => DependencyService.Get<INavigator>().NavigateBack();
			InitTranslator();
		}

		private void InitTranslator()
		{
			lblTitle.Text = Translator.Translate("ABOUT_HEADER");
			lblProductIntroduction.Text = Translator.Translate("ABOUT_PRODUCT_INTRODUCTION");
			lblDisclaimer.Text = Translator.Translate("ABOUT_DISCLAIMER");
		}
	}
}

