﻿
using System;
using System.Drawing;

using Foundation;
using UIKit;
using Unicorn.Core.UI;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using DriveRightF.Core.Enums;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Linq;
using ReactiveUI;

namespace DriveRightF.iOS
{
	public partial class SettingViewController : BaseHandlerHeaderViewController<SettingViewModel>
	{
		protected IPolaraxSDK PolaraxSDK
		{
			get {
				return DependencyService.Get<IPolaraxSDK> ();
			}
		}

		public SettingViewController () : base ("SettingViewController", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			ViewModel = new SettingViewModel ();

			InitData ();

			InitControls ();

			RxApp.TaskpoolScheduler.Schedule (InitBinding);
		}

		void InitData ()
		{
			chkWifiOnly.ValueChanged += (object sender, EventArgs e) => 
			ViewModel.SetWifiUploadOnly (chkWifiOnly.On);
		}

		void InitControls ()
		{
			lblWifi.Font = FontHub.HelveticaLt15;

			btnBack.TouchUpInside += (sender, e) => DependencyService.Get<INavigator> ().NavigateBack ();

			InitTranslator ();
		}

		private void InitBinding()
		{
			ViewModel.WhenAnyValue(x => x.IsWifiUploadOnly)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(x => chkWifiOnly.SetState (x, true));
		}

		private void InitTranslator()
		{
			lblTitle.Text = Translator.Translate ("SETTING_HEADER");
			lblWifi.Text = Translator.Translate ("WIFI_ONLY");
		}
	}
}

