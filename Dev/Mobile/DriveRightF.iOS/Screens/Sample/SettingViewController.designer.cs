// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("SettingViewController")]
	partial class SettingViewController
	{
		[Outlet]
		UIKit.UIButton btnBack { get; set; }

		[Outlet]
		UIKit.UISwitch chkWifiOnly { get; set; }

		[Outlet]
		UIKit.UILabel lblTitle { get; set; }

		[Outlet]
		UIKit.UILabel lblWifi { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (chkWifiOnly != null) {
				chkWifiOnly.Dispose ();
				chkWifiOnly = null;
			}

			if (lblWifi != null) {
				lblWifi.Dispose ();
				lblWifi = null;
			}

			if (btnBack != null) {
				btnBack.Dispose ();
				btnBack = null;
			}

			if (lblTitle != null) {
				lblTitle.Dispose ();
				lblTitle = null;
			}
		}
	}
}
