﻿
using System;
using System.Drawing;

using Foundation;
using UIKit;
using Unicorn.Core.UI;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using DriveRightF.Core;

namespace DriveRightF.iOS
{
	public partial class ProductionViewController : BaseHandlerHeaderViewController<ProductionViewModel>
	{
		public ProductionViewController() : base("ProductionViewController", null)
		{
		}

		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			ViewModel = new ProductionViewModel ();
			InitControl();
		}

		private void InitControl()
		{
			btnBack.TouchUpInside += (sender, e) => DependencyService.Get<INavigator>().NavigateBack();
			InitTranslator();
		}

		private void InitTranslator()
		{
			lblTitle.Text = Translator.Translate("PRODUCTION_HEADER");
			txtDisclaime.Text = Translator.Translate("PRODUCTION_INFO");
		}
	}
}

