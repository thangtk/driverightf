// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("AboutViewController")]
	partial class AboutViewController
	{
		[Outlet]
		UIKit.UIButton btnBack { get; set; }

		[Outlet]
		UIKit.UILabel lblAppVersion { get; set; }

		[Outlet]
		UIKit.UILabel lblDisclaimer { get; set; }

		[Outlet]
		UIKit.UILabel lblProductIntroduction { get; set; }

		[Outlet]
		UIKit.UILabel lblTitle { get; set; }

		[Outlet]
		UIKit.UITapGestureRecognizer tapDisclaimer { get; set; }

		[Outlet]
		UIKit.UITapGestureRecognizer tapProductIntroduction { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnBack != null) {
				btnBack.Dispose ();
				btnBack = null;
			}

			if (lblDisclaimer != null) {
				lblDisclaimer.Dispose ();
				lblDisclaimer = null;
			}

			if (lblProductIntroduction != null) {
				lblProductIntroduction.Dispose ();
				lblProductIntroduction = null;
			}

			if (lblTitle != null) {
				lblTitle.Dispose ();
				lblTitle = null;
			}

			if (tapDisclaimer != null) {
				tapDisclaimer.Dispose ();
				tapDisclaimer = null;
			}

			if (tapProductIntroduction != null) {
				tapProductIntroduction.Dispose ();
				tapProductIntroduction = null;
			}

			if (lblAppVersion != null) {
				lblAppVersion.Dispose ();
				lblAppVersion = null;
			}
		}
	}
}
