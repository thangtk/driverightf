// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("LoginScreen")]
	partial class LoginScreen
	{
		[Outlet]
		UIKit.UIButton btnGo { get; set; }

		[Outlet]
		UIKit.UILabel lblWrongNumber { get; set; }

		[Outlet]
		UIKit.UIScrollView scrlMain { get; set; }

		[Outlet]
		UIKit.UITextField txtPhoneNumber { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnGo != null) {
				btnGo.Dispose ();
				btnGo = null;
			}

			if (lblWrongNumber != null) {
				lblWrongNumber.Dispose ();
				lblWrongNumber = null;
			}

			if (txtPhoneNumber != null) {
				txtPhoneNumber.Dispose ();
				txtPhoneNumber = null;
			}

			if (scrlMain != null) {
				scrlMain.Dispose ();
				scrlMain = null;
			}
		}
	}
}
