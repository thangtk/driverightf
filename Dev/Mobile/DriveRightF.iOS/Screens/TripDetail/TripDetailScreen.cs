﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using CoreGraphics;
using DriveRight.Core.iOS;
using System.IO;
using DriveRightF.Core.Models;
using DriveRight.Core.ViewModels;
using ReactiveUI;
using System.Reactive.Linq;
using System.Collections.Generic;
using System.Threading;
using Unicorn.Core.iOS;
using DriveRightF.Core.Managers;
using Unicorn.Core;
using System.Threading.Tasks;
using System.Reactive.Concurrency;
using System.Text.RegularExpressions;

namespace DriveRightF.iOS
{
	public partial class TripDetailScreen : BaseHandlerHeaderViewController<TripListDetailViewModel>
	{
		private string _fileName;
		private iOSMapConfig _mapConfig;
		private bool _mapIsLoad;
		private TripSummary _trip;
		private GlanceLoadingView _loadingView;
		private NetworkStatus _internetStatus;
		private SwipeControlView _swipe;
		private bool _isBack = false;
		private bool _menuOpen = false;

		//		private TripDetailManager _tripDetailManager {
		//			get	{ return DependencyService.Get<TripDetailManager>(); }
		//		}

		public TripDetailScreen () : base ("TripDetailScreen", null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			if (ViewModel == null)
				ViewModel = new TripListDetailViewModel ();
			InitView ();

			InitTranslator ();
			RxApp.TaskpoolScheduler.Schedule (InitBinding);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			_swipe.IsVisible = false;
		}

		private void AddLoadingView ()
		{
			_loadingView = new GlanceLoadingView (wbvMap.Frame);
			_loadingView.LabelText = Translator.Translate ("LOADING");
			_loadingView.StartAnimating ();
			this.View.AddSubview (_loadingView);
		}

		public override void LoadData (params object[] objs)
		{
			base.LoadData (objs);



			ViewModel.HeaderViewModel.IsShowBtnInfo = false;

//			_isBack = false;

			if (ViewModel == null) {
				ViewModel = new TripListDetailViewModel ();
			}

			_trip = objs [0] as TripSummary;
			ViewModel.TripId = _trip.TripId;
//			ViewModel.LoadTripCommand.Execute (null);

			LoadLocalData ();
			_internetStatus = Reachability.InternetConnectionStatus ();

			LoadWebView ();
		}

		void LoadLocalData ()
		{
			lblStartTime.Text = _trip.StartTime.ToLocalTime ().ToString ("HH:mm");
			lblEndTime.Text	= _trip.StartTime.AddMilliseconds (_trip.Duration).ToLocalTime ().ToString ("HH:mm");
			SetScore (Math.Round (_trip.Score, 1).ToString ());
		}

		private void LoadWebView ()
		{
			if (wbvMap == null)
				return;
			wbvMap.LoadRequest (NSUrlRequest.FromUrl (NSUrl.FromString ("about:blank")));

			lblNoInternet.Hidden = false;
			lblNoInternet.Text = "";
			_loadingView.Hidden = true;

			if (_internetStatus != NetworkStatus.NotReachable) {
//				LoadLocalData();

				wbvMap.LoadHtmlString (File.ReadAllText (_fileName), NSUrl.FromFilename (_fileName));
			} else {
				_loadingView.Hidden = true;
				lblNoInternet.Hidden = false;
				lblNoInternet.Text = Translator.Translate ("NO_INTERNET_CONNECTION");
				ResetValue ();
			}
		}

		private void ResetValue ()
		{
//			lblStartTime.Text = "--:--";
//			lblEndTime.Text = "--:--";
//			lblScoreVariable.Text = "---";

			lblValueOfAverageSpeed.Text = "----";
			lblValueOfDistance.Text = "----";
			lblValueOfDuration.Text = "----";
//			lblValueOfSmooth.Text = "----";
//			lblValueOfSpeed.Text = "----";
//			lblValueOfDistraction.Text = "----";
//			lblValueOfTimeOfDay.Text = "----";
		}

		private void InitTranslator ()
		{
			lblStart.Text = Translator.Translate ("TRIP_DETAILS_START").ToUpper ();
			lblScore.Text = Translator.Translate ("TRIP_DETAILS_SCORE").ToUpper ();
			lblEnd.Text = Translator.Translate ("TRIP_DETAILS_STOP").ToUpper ();
			lblDistance.Text = Translator.Translate ("TRIP_DETAILS_DISTANCE");
			lblDuration.Text = Translator.Translate ("TRIP_DETAILS_DURATION");
			lblAverageSpeed.Text = Translator.Translate ("TRIP_DETAILS_AVERAGE");
			lblSmoothScore.Text = Translator.Translate ("TRIP_DETAILS_SMOOTH");
			lblDistractionScore.Text = Translator.Translate ("TRIP_DETAILS_DISTRACTION");
			lblSpeedScore.Text = Translator.Translate ("TRIP_DETAILS_SPEED");
			lblTimeOfDayScore.Text = Translator.Translate ("TRIP_DETAILS_TIME_OF_DAY");

		}

		private void InitView ()
		{
			wbvMap.Alpha = 1;
			wbvMap.LoadError += MapView_WebError;
			wbvMap.LoadFinished += MapView_LoadFinished;
			wbvMap.ShouldStartLoad += MapView_ShouldStartLoad;
			wbvMap.ScrollView.ScrollEnabled = false;

			AddLoadingView ();

			_mapConfig = new iOSMapConfig ();
			_fileName = _mapConfig.FilePath;
			//LoadWebView ();

			SetFont ();

			wbvMap.AddGestureRecognizer (new UITapGestureRecognizer (() => wbvMap.EndEditing (false)));

			//add swipe menu
			_swipe = new SwipeControlView (new CGRect (0, UIScreen.MainScreen.Bounds.Height - 94f, 320, 180), vSwipeContent);
			_swipe.SwipeButtonView = btnControlMenu;

			View.AddSubview (_swipe);
			View.BringSubviewToFront (vPanel);

		}

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();

			ResizeControl ();
		}

		private void ResizeControl ()
		{
		}

		private void ReloadMap ()
		{
			wbvMap.EvaluateJavascript (@"javascript:refresh();");
			wbvMap.EvaluateJavascript (@"javascript:showMap(true);");
		}

		private void SetFont ()
		{

			lblDistance.Font = FontHub.HelveticaLt13;
			lblDuration.Font = FontHub.HelveticaLt13;
			lblAverageSpeed.Font = FontHub.HelveticaLt13;
			lblSmoothScore.Font = FontHub.HelveticaLt13;
			lblSpeedScore.Font = FontHub.HelveticaLt13;
			lblDistractionScore.Font = FontHub.HelveticaLt13;
			lblTimeOfDayScore.Font = FontHub.HelveticaLt13;

			lblValueOfDistance.Font = FontHub.HelveticaHvCn17;
			lblValueOfDuration.Font = FontHub.HelveticaHvCn17;
			lblValueOfAverageSpeed.Font = FontHub.HelveticaHvCn17;
//			lblValueOfSmooth.Font = FontHub.HelveticaMedium11;
//			lblValueOfSpeed.Font = FontHub.HelveticaMedium11;
//			lblValueOfDistraction.Font = FontHub.HelveticaMedium11;
//			lblValueOfTimeOfDay.Font = FontHub.HelveticaMedium11;

			lblStart.Font = FontHub.HelveticaLt13;
			lblEnd.Font = FontHub.HelveticaLt13;
			lblScore.Font = FontHub.HelveticaLt13;

			lblStartTime.Font = FontHub.HelveticaHvCn20;
			lblScoreVariable.Font = FontHub.HelveticaHvCn20;
			lblEndTime.Font = FontHub.HelveticaHvCn20;	
			lblNoInternet.Font = FontHub.HelveticaMedium17;

			progressTimeOfDelayView.TextColor = UIColor.Black;
			progressSmoothView.TextColor = UIColor.Black;
			progressSpeedView.TextColor = UIColor.Black;
			progressDistractionView.TextColor = UIColor.Black;

			progressTimeOfDelayView.LabelTextFont = FontHub.HelveticaHvCn20;
			progressSmoothView.LabelTextFont = FontHub.HelveticaHvCn20;
			progressSpeedView.LabelTextFont = FontHub.HelveticaHvCn20;
			progressDistractionView.LabelTextFont = FontHub.HelveticaHvCn20;
		}

		public void InitBinding ()
		{
//			headerView.BackClicked = new Action<object, EventArgs> (delegate(object arg1, EventArgs arg2) {
//				 ViewModel.BtnBackClick.Execute (new Action(delegate {
//					ReloadMap();
//					_swipe.IsVisible = false;
//				}));
//				_isBack = true;
//			});

//			ViewModel
//				.WhenAnyValue (vm => vm.HeaderTitle)
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (x => headerView.Title = x);

//			ViewModel
//				.WhenAnyValue (vm => vm.HeaderLogo)
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (x => headerView.LogoImage = UIImage.FromFile(x));
//			
//			ViewModel
//				.WhenAnyValue (vm => vm.HeaderTitle)
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (x => headerView.Title = x);
			ViewModel
				.WhenAnyValue (vm => vm.Distance)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => lblValueOfDistance.Text = x);

			ViewModel
				.WhenAnyValue (vm => vm.StartTimeString)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => lblStartTime.Text = x);

//			ViewModel
//				.WhenAnyValue (vm => vm.StartTimeAmOrPm)
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (x => lblEndTime.Text = x);

			ViewModel
				.WhenAnyValue (vm => vm.Score)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				SetScore (x);



			});

			ViewModel
				.WhenAnyValue (vm => vm.EndTimeString)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => lblEndTime.Text = x);

//			ViewModel
//				.WhenAnyValue (vm => vm.EndTimeAmOrPm)
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (x => lblStartTime.Text = x);

			ViewModel
				.WhenAnyValue (vm => vm.Duration)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => lblValueOfDuration.Text = x);

			ViewModel
				.WhenAnyValue (vm => vm.AverageSpeed)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => lblValueOfAverageSpeed.Text = x);

			ViewModel
				.WhenAnyValue (vm => vm.SmoothScore)
				.Where (x => string.IsNullOrEmpty (x) == false)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				SetScore (progressSmoothView, int.Parse (x));
			});

			ViewModel
				.WhenAnyValue (vm => vm.SpeedScore)
				.Where (x => string.IsNullOrEmpty (x) == false)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				SetScore (progressSpeedView, int.Parse (x));
			});
			

			ViewModel
				.WhenAnyValue (vm => vm.DistractionScore)
				.Where (x => string.IsNullOrEmpty (x) == false)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				SetScore (progressDistractionView, int.Parse (x));
			});

			ViewModel
				.WhenAnyValue (vm => vm.TimeOfDayScore)
				.Where (x => string.IsNullOrEmpty (x) == false)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				SetScore (progressTimeOfDelayView, int.Parse (x));
			});
			
			ViewModel
				.WhenAnyValue (vm => vm.Trip)
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Select (obj => ViewModel.GetWebViewScripts ())
				.Where (obj => obj != null)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (scripts => {
				foreach (string script in scripts) {
					wbvMap.EvaluateJavascript (script);
				}
							
				_loadingView.Hidden = true;
				lblNoInternet.Hidden = true;
			});

		}

		void SetScore (string x)
		{
			lblScoreVariable.Text = x;
			Regex regex = new Regex ("^[0-9]{1,3}([.][0-9]{1,3})?$");
			if (regex.IsMatch (lblScoreVariable.Text)) {
				double score = double.Parse (lblScoreVariable.Text);
				lblScoreVariable.TextColor = ColorHub.ColorFromHex (ScoreUtils.GetScoreColor ((float)score));
			} else {
				lblScoreVariable.TextColor = UIColor.Black;
			}
		}

		private void SetScore (ProgressScoreView sv, int score)
		{
			sv.SetValue (score, true);
//			sv.StartAnimating(500);
		}

		public void MapView_WebError (object sender, UIWebErrorArgs e)
		{
			Console.WriteLine ("Error: {0}", e.Error);
		}

		public void MapView_LoadFinished (object sender, EventArgs e)
		{
//			if (!_mapIsLoad != null) {
//				//                mapView.LoadHtmlString(File.ReadAllText(_fileName),NSUrl.FromFilename(_fileName));
//				InvokeOnMainThread (delegate {
//					RenderMap ();
//				});
//				_mapIsLoad = true;
//			}
		}

		public bool MapView_ShouldStartLoad (UIWebView webView, NSUrlRequest request, UIWebViewNavigationType navigationType)
		{
			return true;
		}

		public void RenderMap ()
		{
//			Observable.Start(()=>{})
			InvokeInBackground (() => {
				List<string> scripts = ViewModel.GetWebViewScripts ();
				InvokeOnMainThread (() => {
					foreach (string script in scripts) {
						wbvMap.EvaluateJavascript (script);
					}

					_loadingView.Hidden = true;
					lblNoInternet.Hidden = true;
				});
			});

		}
	}
}

