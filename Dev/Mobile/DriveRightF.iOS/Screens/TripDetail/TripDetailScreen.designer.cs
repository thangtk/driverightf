// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("TripDetailScreen")]
	partial class TripDetailScreen
	{
		[Outlet]
		UIKit.UIButton btnControlMenu { get; set; }

		[Outlet]
		UIKit.UILabel lblAverageSpeed { get; set; }

		[Outlet]
		UIKit.UILabel lblDistance { get; set; }

		[Outlet]
		UIKit.UILabel lblDistractionScore { get; set; }

		[Outlet]
		UIKit.UILabel lblDuration { get; set; }

		[Outlet]
		UIKit.UILabel lblEnd { get; set; }

		[Outlet]
		UIKit.UILabel lblEndTime { get; set; }

		[Outlet]
		UIKit.UILabel lblNoInternet { get; set; }

		[Outlet]
		UIKit.UILabel lblScore { get; set; }

		[Outlet]
		UIKit.UILabel lblScoreVariable { get; set; }

		[Outlet]
		UIKit.UILabel lblSmoothScore { get; set; }

		[Outlet]
		UIKit.UILabel lblSpeedScore { get; set; }

		[Outlet]
		UIKit.UILabel lblStart { get; set; }

		[Outlet]
		UIKit.UILabel lblStartTime { get; set; }

		[Outlet]
		UIKit.UILabel lblTimeOfDayScore { get; set; }

		[Outlet]
		UIKit.UILabel lblValueOfAverageSpeed { get; set; }

		[Outlet]
		UIKit.UILabel lblValueOfDistance { get; set; }

		[Outlet]
		UIKit.UILabel lblValueOfDuration { get; set; }

		[Outlet]
		DriveRightF.iOS.ProgressScoreView progressDistractionView { get; set; }

		[Outlet]
		DriveRightF.iOS.ProgressScoreView progressSmoothView { get; set; }

		[Outlet]
		DriveRightF.iOS.ProgressScoreView progressSpeedView { get; set; }

		[Outlet]
		DriveRightF.iOS.ProgressScoreView progressTimeOfDelayView { get; set; }

		[Outlet]
		UIKit.UIView vPanel { get; set; }

		[Outlet]
		UIKit.UIView vSwipeContent { get; set; }

		[Outlet]
		UIKit.UIView vSwipeMenu { get; set; }

		[Outlet]
		UIKit.UIWebView wbvMap { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnControlMenu != null) {
				btnControlMenu.Dispose ();
				btnControlMenu = null;
			}

			if (lblAverageSpeed != null) {
				lblAverageSpeed.Dispose ();
				lblAverageSpeed = null;
			}

			if (lblDistance != null) {
				lblDistance.Dispose ();
				lblDistance = null;
			}

			if (lblDistractionScore != null) {
				lblDistractionScore.Dispose ();
				lblDistractionScore = null;
			}

			if (lblDuration != null) {
				lblDuration.Dispose ();
				lblDuration = null;
			}

			if (lblEnd != null) {
				lblEnd.Dispose ();
				lblEnd = null;
			}

			if (lblEndTime != null) {
				lblEndTime.Dispose ();
				lblEndTime = null;
			}

			if (lblNoInternet != null) {
				lblNoInternet.Dispose ();
				lblNoInternet = null;
			}

			if (lblScore != null) {
				lblScore.Dispose ();
				lblScore = null;
			}

			if (lblScoreVariable != null) {
				lblScoreVariable.Dispose ();
				lblScoreVariable = null;
			}

			if (lblSmoothScore != null) {
				lblSmoothScore.Dispose ();
				lblSmoothScore = null;
			}

			if (lblSpeedScore != null) {
				lblSpeedScore.Dispose ();
				lblSpeedScore = null;
			}

			if (lblStart != null) {
				lblStart.Dispose ();
				lblStart = null;
			}

			if (lblStartTime != null) {
				lblStartTime.Dispose ();
				lblStartTime = null;
			}

			if (lblTimeOfDayScore != null) {
				lblTimeOfDayScore.Dispose ();
				lblTimeOfDayScore = null;
			}

			if (lblValueOfAverageSpeed != null) {
				lblValueOfAverageSpeed.Dispose ();
				lblValueOfAverageSpeed = null;
			}

			if (lblValueOfDistance != null) {
				lblValueOfDistance.Dispose ();
				lblValueOfDistance = null;
			}

			if (lblValueOfDuration != null) {
				lblValueOfDuration.Dispose ();
				lblValueOfDuration = null;
			}

			if (progressDistractionView != null) {
				progressDistractionView.Dispose ();
				progressDistractionView = null;
			}

			if (progressSmoothView != null) {
				progressSmoothView.Dispose ();
				progressSmoothView = null;
			}

			if (progressSpeedView != null) {
				progressSpeedView.Dispose ();
				progressSpeedView = null;
			}

			if (progressTimeOfDelayView != null) {
				progressTimeOfDelayView.Dispose ();
				progressTimeOfDelayView = null;
			}

			if (vSwipeContent != null) {
				vSwipeContent.Dispose ();
				vSwipeContent = null;
			}

			if (vSwipeMenu != null) {
				vSwipeMenu.Dispose ();
				vSwipeMenu = null;
			}

			if (wbvMap != null) {
				wbvMap.Dispose ();
				wbvMap = null;
			}

			if (vPanel != null) {
				vPanel.Dispose ();
				vPanel = null;
			}
		}
	}
}
