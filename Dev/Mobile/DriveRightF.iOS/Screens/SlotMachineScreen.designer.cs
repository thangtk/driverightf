// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("SlotMachineScreen")]
	partial class SlotMachineScreen
	{
		[Outlet]
		UIKit.UIImageView img11 { get; set; }

		[Outlet]
		UIKit.UIImageView img12 { get; set; }

		[Outlet]
		UIKit.UIImageView img13 { get; set; }

		[Outlet]
		UIKit.UIImageView img14 { get; set; }

		[Outlet]
		UIKit.UIImageView img21 { get; set; }

		[Outlet]
		UIKit.UIImageView img22 { get; set; }

		[Outlet]
		UIKit.UIImageView img23 { get; set; }

		[Outlet]
		UIKit.UIImageView img24 { get; set; }

		[Outlet]
		UIKit.UIImageView img31 { get; set; }

		[Outlet]
		UIKit.UIImageView img32 { get; set; }

		[Outlet]
		UIKit.UIImageView img33 { get; set; }

		[Outlet]
		UIKit.UIImageView img34 { get; set; }

		[Outlet]
		UIKit.UIView vColumn1 { get; set; }

		[Outlet]
		UIKit.UIView vColumn2 { get; set; }

		[Outlet]
		UIKit.UIView vColumn3 { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (img11 != null) {
				img11.Dispose ();
				img11 = null;
			}

			if (img12 != null) {
				img12.Dispose ();
				img12 = null;
			}

			if (img13 != null) {
				img13.Dispose ();
				img13 = null;
			}

			if (img14 != null) {
				img14.Dispose ();
				img14 = null;
			}

			if (img21 != null) {
				img21.Dispose ();
				img21 = null;
			}

			if (img22 != null) {
				img22.Dispose ();
				img22 = null;
			}

			if (img23 != null) {
				img23.Dispose ();
				img23 = null;
			}

			if (img24 != null) {
				img24.Dispose ();
				img24 = null;
			}

			if (img31 != null) {
				img31.Dispose ();
				img31 = null;
			}

			if (img32 != null) {
				img32.Dispose ();
				img32 = null;
			}

			if (img33 != null) {
				img33.Dispose ();
				img33 = null;
			}

			if (img34 != null) {
				img34.Dispose ();
				img34 = null;
			}

			if (vColumn1 != null) {
				vColumn1.Dispose ();
				vColumn1 = null;
			}

			if (vColumn2 != null) {
				vColumn2.Dispose ();
				vColumn2 = null;
			}

			if (vColumn3 != null) {
				vColumn3.Dispose ();
				vColumn3 = null;
			}
		}
	}
}
