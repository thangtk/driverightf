﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using System.Collections.Generic;
using System.Linq;
using DriveRightF.Core.Enums;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

namespace DriveRightF.iOS
{
	public partial class AccountStatementScreen : BaseHandlerHeaderViewController<AccountStatementViewModel>
	{
		private AccountStatementTableSource _tableSources;

		public AccountStatementScreen() : base("AccountStatementScreen", null)
		{
		}

		public override void LoadData(params object[] objs)
		{
			ViewModel = new AccountStatementViewModel ();
			base.LoadData(objs);

			if(objs != null && objs.Length > 0) ViewModel.LoadDataCommand.Execute(objs [0]);
		}

		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			
			InitControls();
			CreateBindingsAsync();

			lblEndBalance.Text = Translator.Translate("ACCOUNT_STATEMENT_END_BALANCE");
		}

		private void InitControls()
		{
			
//			lblHeader.Font = FontHub.HelveticaThin15;
//			lblStartBalance.Font = FontHub.HelveticaLt15;
//			lblEndBalance.Font = FontHub.HelveticaLt15;
//
//			lblCashTitle.Font = FontHub.HelveticaLt15;
//			lblCardTitle.Font = FontHub.HelveticaLt15;

//			lblCardEnd.Font = FontHub.HelveticaHvCn17;
//			lblCardStart.Font = FontHub.HelveticaHvCn17;
//			lblCashEnd.Font = FontHub.HelveticaHvCn17;
//			lblCashStart.Font = FontHub.HelveticaHvCn17;
////
//			lblTransactionHistory.Font = FontHub.HelveticaLt11;
//			lblDate.Font = FontHub.HelveticaLt11;

			_tableSources = new AccountStatementTableSource ();
//
			tbTransactions.Source = _tableSources;
		}

		private void CreateBindingsAsync()
		{
			RxApp.TaskpoolScheduler.Schedule(() =>
			{
				InitBindings();
			});
		}

		private void InitBindings()
		{
			this.WhenAnyValue(v => v.ViewModel)
				.Where(x => x != null)
				.Subscribe(k =>
			{

				ViewModel.WhenAnyValue(vm => vm.StartBalance).Where(x => x != null).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x => lblStartBalance.Text = x);
				ViewModel.WhenAnyValue(vm => vm.TotalStartCard).Where(x => x != null).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x => lblCardStart.Text = x);
				ViewModel.WhenAnyValue(vm => vm.TotalEndCard).Where(x => x != null).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x => lblCardEnd.Text = x);
				ViewModel.WhenAnyValue(vm => vm.TotalStartCash).Where(x => x != null).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x => lblCashStart.Text = x);
				ViewModel.WhenAnyValue(vm => vm.TotalEndCash).Where(x => x != null).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x => lblCashEnd.Text = x);
				ViewModel.WhenAnyValue(vm => vm.NameCard).Where(x => x != null).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x => lblCardTitle.Text = x);
				ViewModel.WhenAnyValue(vm => vm.NameCash).Where(x => x != null).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x => lblCashTitle.Text = x);
				ViewModel.WhenAnyValue(vm => vm.TransactionHistory).Where(x => x != null).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x => lblTransactionHistory.Text = x);

				ViewModel.WhenAnyValue(x => x.Date)
					.Where(x => x != null)
					.ObserveOn(RxApp.MainThreadScheduler)
					.Subscribe(x =>
				{
					lblDate.Text = x.ToString("MMMMM - yyyy");
				});

				ViewModel.WhenAnyValue(vm => vm.Items)
				//.Where (x => x != null && x.Count > 0)
					.Where(x => x != null)
					.ObserveOn(RxApp.MainThreadScheduler)
					.Subscribe(items =>
				{
					_tableSources.Items = items;
					tbTransactions.ReloadData();
				});
			});
		}
		//		private void InitBindings ()
		//		{
		//			this.WhenAnyValue (v => v.ViewModel)
		//				.Where (x => x != null)
		//				.Subscribe (k => {
		//				this.OneWayBind (ViewModel, vm => vm.StartBalance, v => v.lblStartBalance.Text);
		//				this.OneWayBind (ViewModel, vm => vm.TotalStartCard, v => v.lblCardStart.Text);
		//				this.OneWayBind (ViewModel, vm => vm.TotalEndCard, v => v.lblCardEnd.Text);
		//				this.OneWayBind (ViewModel, vm => vm.TotalStartCash, v => v.lblCashStart.Text);
		//				this.OneWayBind (ViewModel, vm => vm.TotalEndCash, v => v.lblCashEnd.Text);
		//				this.OneWayBind (ViewModel, vm => vm.NameCard, v => v.lblCardTitle.Text);
		//				this.OneWayBind (ViewModel, vm => vm.NameCash, v => v.lblCashTitle.Text);
		//				this.OneWayBind (ViewModel, vm => vm.TransactionHistory, v => v.lblTransactionHistory.Text);
		//				//this.OneWayBind (ViewModel, vm => vm.Date.ToString("MMMMM - yyyy"), v => v.lblDate.Text);
		//
		//				ViewModel.WhenAnyValue (x => x.Date)
		//							.Where (x => x != null)
		//							.ObserveOn (RxApp.MainThreadScheduler)
		//							.Subscribe (x => {
		//							lblDate.Text = x.ToString ("MMMMM - yyyy").ToUpper();
		//				});
		//
		////						ViewModel.Items = new List<Transaction> () {
		////							new Transaction () {
		////								Value = 10f,
		////								Unit = "CARD",
		////								StartTime = DateTime.UtcNow.AddDays (-1),
		////								TransactionType = TransactionType.Awards
		////							},
		////							new Transaction () {
		////								Value = 10f,
		////								Unit = "RMB",
		////								StartTime = DateTime.UtcNow.AddDays (-1),
		////								TransactionType = TransactionType.Gamble
		////							},
		////							new Transaction () {
		////								Value = -10f,
		////								Unit = "RMB",
		////								StartTime = DateTime.UtcNow.AddDays (-1),
		////								TransactionType = TransactionType.WidthDraw
		////							},
		////							new Transaction () {
		////								Value = -10f,
		////								Unit = "RMB",
		////								StartTime = DateTime.UtcNow.AddDays (-1),
		////								TransactionType = TransactionType.TakeCash
		////							},
		////							new Transaction () {
		////								Value = 10.5f,
		////								Unit = "CARD",
		////								StartTime = DateTime.UtcNow.AddDays (-1),
		////								TransactionType = TransactionType.TakeCash
		////							},
		////						};
		//
		//				ViewModel.WhenAnyValue (vm => vm.Items)
		//				//.Where (x => x != null && x.Count > 0)
		//							.Where (x => x != null)
		//							.ObserveOn (RxApp.MainThreadScheduler)
		//							.Subscribe (items => {
		//					_tableSources.Items = items;
		//					tbTransactions.ReloadData ();
		//				});
		//			});
		//
		//		}

	}
}

