﻿using System;
using DriveRightF.Core;
using DriveRightF.Core.Models;

namespace DriveRightF.iOS
{
	public class AccountStatementTableSource :DriveRightTableViewSource<AccountTransaction, ItemAccountStatementViewCell>
	{
		public AccountStatementTableSource () : base()
		{
		}
	}
}
