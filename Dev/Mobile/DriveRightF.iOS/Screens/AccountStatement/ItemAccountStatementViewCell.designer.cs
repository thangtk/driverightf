// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("ItemAccountStatementViewCell")]
	partial class ItemAccountStatementViewCell
	{
		[Outlet]
		UIKit.UIImageView imgTransaction { get; set; }

		[Outlet]
		UIKit.UILabel lblStartTime { get; set; }

		[Outlet]
		UIKit.UILabel lblTransactionType { get; set; }

		[Outlet]
		UIKit.UILabel lblTransactionValue { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (lblStartTime != null) {
				lblStartTime.Dispose ();
				lblStartTime = null;
			}

			if (lblTransactionType != null) {
				lblTransactionType.Dispose ();
				lblTransactionType = null;
			}

			if (lblTransactionValue != null) {
				lblTransactionValue.Dispose ();
				lblTransactionValue = null;
			}

			if (imgTransaction != null) {
				imgTransaction.Dispose ();
				imgTransaction = null;
			}
		}
	}
}
