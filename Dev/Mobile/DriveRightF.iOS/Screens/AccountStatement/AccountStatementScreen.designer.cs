// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("AccountStatementScreen")]
	partial class AccountStatementScreen
	{
		[Outlet]
		UIKit.UILabel lblCardEnd { get; set; }

		[Outlet]
		UIKit.UILabel lblCardStart { get; set; }

		[Outlet]
		UIKit.UILabel lblCardTitle { get; set; }

		[Outlet]
		UIKit.UILabel lblCashEnd { get; set; }

		[Outlet]
		UIKit.UILabel lblCashStart { get; set; }

		[Outlet]
		UIKit.UILabel lblCashTitle { get; set; }

		[Outlet]
		UIKit.UILabel lblDate { get; set; }

		[Outlet]
		UIKit.UILabel lblEndBalance { get; set; }

		[Outlet]
		UIKit.UILabel lblStartBalance { get; set; }

		[Outlet]
		UIKit.UILabel lblTransactionHistory { get; set; }

		[Outlet]
		UIKit.UITableView tbTransactions { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (lblCardEnd != null) {
				lblCardEnd.Dispose ();
				lblCardEnd = null;
			}

			if (lblCardStart != null) {
				lblCardStart.Dispose ();
				lblCardStart = null;
			}

			if (lblCardTitle != null) {
				lblCardTitle.Dispose ();
				lblCardTitle = null;
			}

			if (lblCashEnd != null) {
				lblCashEnd.Dispose ();
				lblCashEnd = null;
			}

			if (lblCashStart != null) {
				lblCashStart.Dispose ();
				lblCashStart = null;
			}

			if (lblCashTitle != null) {
				lblCashTitle.Dispose ();
				lblCashTitle = null;
			}

			if (lblDate != null) {
				lblDate.Dispose ();
				lblDate = null;
			}

			if (lblEndBalance != null) {
				lblEndBalance.Dispose ();
				lblEndBalance = null;
			}

			if (lblStartBalance != null) {
				lblStartBalance.Dispose ();
				lblStartBalance = null;
			}

			if (lblTransactionHistory != null) {
				lblTransactionHistory.Dispose ();
				lblTransactionHistory = null;
			}

			if (tbTransactions != null) {
				tbTransactions.Dispose ();
				tbTransactions = null;
			}
		}
	}
}
