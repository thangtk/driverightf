﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using DriveRightF.Core.Models;

namespace DriveRightF.iOS
{
	public partial class ItemAccountStatementViewCell : DriveRightTableViewCell<AccountTransaction, ItemAccountStatementViewModel>
	{
		public static readonly UINib Nib = UINib.FromName("ItemAccountStatementViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("ItemAccountStatementViewCell");

		public ItemAccountStatementViewCell(IntPtr handle) : base(handle)
		{
		}

		public static ItemAccountStatementViewCell Create()
		{
			return (ItemAccountStatementViewCell)Nib.Instantiate(null, null) [0];
		}

		public override void UpdateCell(AccountTransaction item, int index, int count)
		{
			ViewModel.Item = item;
//			vLine.Hidden = index == 0;
		}

		public override void Initialize()
		{
			InitControls();
			InitBindings();
		}

		private void InitControls()
		{
//			FontHub.HelveticaHvCn17;
//			lblTransactionValue.Font = FontHub.HelveticaHvCn22;
//			lblStartTime.Font = FontHub.HelveticaThin13;
//			lblTransactionType.Font = FontHub.HelveticaLt15;
		}

		private void InitBindings()
		{
			ViewModel = new ItemAccountStatementViewModel ();

//			this.OneWayBind (ViewModel, vm => vm.StartTime, v => v.lblStartTime.Text);
//			this.OneWayBind (ViewModel, vm => vm.Type, v => v.lblTransactionType.Text);

			ViewModel.WhenAnyValue(vm => vm.StartTime).Where(x => x != null).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x => lblStartTime.Text = x);

			ViewModel.WhenAnyValue(vm => vm.Type)
				.Where(x => x != null)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(x =>
			{
				lblTransactionType.Text = x;
			});


			ViewModel.WhenAnyValue(x => x.TransactionValue)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(value =>
			{
				lblTransactionValue.Text = value;
				lblTransactionValue.TextColor = ViewModel.Item.Value < 0 ? UIColor.FromRGB(241, 94, 142) : UIColor.FromRGB(159, 212, 108);
			});


		}
	}
}

