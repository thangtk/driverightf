﻿using System;
using System.Drawing;
using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using CoreGraphics;
using ReactiveUI;
using DriveRightF.Core.iOS;

namespace DriveRightF.iOS
{
	public partial class GetStartedScreen : DriveRightBaseViewController<GetStartedViewModel>
	{
		public GetStartedScreen () : base ("GetStartedScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			ViewModel = new GetStartedViewModel ();
			InitView ();
			InitBinding ();
			SetFont ();

//			var progressView = new RadialProgressView (new CGRect (View.Center.X - 30, View.Center.Y - 30, 60, 60)) {
//				Value = 0.1f,
//			};
//			View.AddSubview (progressView);
		}

		private void InitView ()
		{
			CGSize contentSize = _svBackground.ContentSize;
			contentSize.Width = _backgroundView.Frame.Width;
			contentSize.Height = View.Frame.Height;
			_svBackground.ContentSize = contentSize;
			_svBackground.AddSubview (_backgroundView);
			_svBackground.DecelerationEnded += OnDecelerationEnded;
			_svBackground.BackgroundColor = UIColor.FromPatternImage (UIImage.FromFile ("bg.png"));

			CGPoint contentOffset = _svBackground.ContentOffset;
			contentOffset.X = 0;
			contentOffset.Y = 0;
			_svBackground.SetContentOffset (contentOffset, false);
			_svBackground.Scrolled += OnScrolled;

			_btnGetStarted.Font = FontHub.HelveticaMedium15;
		}

		private void InitBinding ()
		{
			this.BindCommand (ViewModel, vm => vm.OnNavigateToLoginCommand, v => v._btnGetStarted);
		}

		private void SetFont ()
		{
			_lblMission.Font = FontHub.HelveticaMedium17;
			_lblGoal.Font = FontHub.HelveticaMedium17;
			_lblUnicorn.Font = FontHub.HelveticaMedium17;
			_txtDescriptionMission.Font = FontHub.HelveticaMedium13;
			_txtDescriptionGoal.Font = FontHub.HelveticaMedium13;
			_txtDescriptionUnicorn.Font = FontHub.HelveticaMedium13;
		}

		void OnScrolled (object sender, EventArgs e)
		{
			// Check the content off set of scroll view. If Y value less than 0, set Y value by 0
			CGPoint contentOffSet = _svBackground.ContentOffset;
			if (contentOffSet.Y != 0)
			{	
				contentOffSet.Y = 0;
				_svBackground.SetContentOffset (contentOffSet, false);
			}
		}

		void OnDecelerationEnded (object sender, EventArgs e)
		{
			_pageControl.CurrentPage = (int)(_svBackground.ContentOffset.X / _svBackground.Frame.Width);
		}
	}
}

