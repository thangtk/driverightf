﻿
using System;

using Foundation;
using UIKit;
using System.Collections.Generic;
using System.Timers;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using ReactiveUI.Legacy;
using System.Reactive.Linq;
using DriveRightF.Core.Models;
using CoreGraphics;
using Unicorn.Core;
using DriveRightF.Core.Managers;

namespace DriveRightF.iOS
{
	public partial class PerksScreen : BaseAutoRequestListScreen<Perk>
	{
		private PerkTableViewSource _tableSource = new PerkTableViewSource ();

		private string _perkId = string.Empty;
		public PerksScreen(): base()
		{
			MessagingCenter.Subscribe<PerkItemViewModel> (this, "Click", (sender) => {
				// do something whenever the "Hi" message is sent
				_perkId = sender.Item.PerkId;
			});
		}

		#region implemented abstract members of BaseAutoRequestListScreen

		public override void InitViewModel ()
		{
			ViewModel = new PerksViewModel ();
		}

		public override void ReloadView ()
		{
			base.ReloadView ();
			if (ViewModel != null) {
				if (!string.IsNullOrEmpty (_perkId)) {
					int index = _tableSource.Items.FindIndex (x => x.PerkId == _perkId);
					_tableSource.Items [index] = DependencyService.Get<PerkManager> ().GetPerk ((_perkId));
					ReloadRow (index);
					_perkId = string.Empty;
				}
			}
		}

		public override void InitControls ()
		{
//			_tblPerkList.AddSubview (_refreshControl);
//			_tblPerkList.Source = _tableSource;

			base.InitControls ();

			btnNewData.Alpha = 0;
			View.BringSubviewToFront (btnNewData);
		}

		protected override UITableView ContentTableView {
			get {
				return _tblPerkList;
			}
		}

		protected override DRTableViewSource<Perk> ContentTableSource {
			get {
				return _tableSource;
			}
		}

		#endregion

		public override void InitBindings ()
		{
			base.InitBindings ();
			//
//			this.BindCommand (ViewModel, vm => vm.BackCommand, v => v.btnBack);

			btnNewData.OnClick += (sender, e) => {
				if (ViewModel != null)
					ViewModel.AppendNewdata.Execute(true);
				btnNewData.Hide();
			};

			this.OneWayBind(
				ViewModel,
				vm => vm.HeaderTitle,
				v => v.headerView.Title);

			ViewModel.WhenAnyValue (x => x.HeaderLogo)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				headerView.LogoImage = UIImage.FromBundle (x);	
			});
		}
	}
}

