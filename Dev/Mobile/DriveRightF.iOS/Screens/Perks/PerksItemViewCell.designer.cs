// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("PerksItemViewCell")]
	partial class PerksItemViewCell
	{
		[Outlet]
		DriveRightF.iOS.ImageMutilStatus ItemIcon { get; set; }

		[Outlet]
		UIKit.UILabel lblPerkItem { get; set; }

		[Outlet]
		UIKit.UIView viewCell { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ItemIcon != null) {
				ItemIcon.Dispose ();
				ItemIcon = null;
			}

			if (lblPerkItem != null) {
				lblPerkItem.Dispose ();
				lblPerkItem = null;
			}

			if (viewCell != null) {
				viewCell.Dispose ();
				viewCell = null;
			}
		}
	}
}
