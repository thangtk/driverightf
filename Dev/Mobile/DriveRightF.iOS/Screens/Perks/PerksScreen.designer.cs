// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("PerksScreen")]
	partial class PerksScreen
	{
		[Outlet]
		UIKit.UITableView _tblPerkList { get; set; }

		[Outlet]
		DriveRightF.iOS.ButtonNewData btnNewData { get; set; }

		[Outlet]
		DriveRightF.iOS.TitleHeaderView headerView { get; set; }

		[Outlet]
		UIKit.UIActivityIndicatorView indicatorView { get; set; }

		[Outlet]
		UIKit.UIView vLoading { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnNewData != null) {
				btnNewData.Dispose ();
				btnNewData = null;
			}

			if (headerView != null) {
				headerView.Dispose ();
				headerView = null;
			}

			if (indicatorView != null) {
				indicatorView.Dispose ();
				indicatorView = null;
			}

			if (vLoading != null) {
				vLoading.Dispose ();
				vLoading = null;
			}

			if (_tblPerkList != null) {
				_tblPerkList.Dispose ();
				_tblPerkList = null;
			}
		}
	}
}
