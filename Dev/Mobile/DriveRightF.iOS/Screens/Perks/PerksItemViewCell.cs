﻿   
using System;

using System.Reactive.Linq;
using CoreGraphics;
using DriveRightF.Core;
using DriveRightF.Core.iOS;
using DriveRightF.Core.Models;
using DriveRightF.Core.ViewModels;
using Foundation;
using ReactiveUI;
using UIKit;
using Unicorn.Core.UI;
using DriveRightF.Core.Enums;

namespace DriveRightF.iOS
{
	public partial class PerksItemViewCell : 
	DriveRightTableViewCell<Perk,PerkItemViewModel>
	//UBaseTableViewCell<PerkItemViewModel>
	{
		public PerksItemViewCell():base()
		{
		}

		public PerksItemViewCell (IntPtr handle) : base (handle)
		{
		}

		#region implemented abstract members of DriveRightTableViewCell

		public override void UpdateCell (Perk item, int index)
		{
			ViewModel.Item = item;
//			ItemIcon.StatusImage.Image = UIImage.FromFile (item.Icon);
		}

		public override void Initialize ()
		{
			InitControls ();
			InitBindings ();
		}

		#endregion

		private void InitControls()
		{
			UITapGestureRecognizer tapCell = new UITapGestureRecognizer (() => {
				ViewModel.ItemClickCommand.Execute(null);
				ViewModel.RemoveItemStatusCommand.Execute(null);
			});
			this.AddGestureRecognizer (tapCell);

			lblPerkItem.Font = FontHub.RobotoRegular17;
			//Warning: need release gesture.
		}

		private void InitBindings()
		{
			ViewModel = new PerkItemViewModel ();

			ItemIcon.ViewModel = new ImageMultiStatusViewModel((int) Screen.Perks)
			{
			};

			ViewModel.ItemIconViewModel = ItemIcon.ViewModel;

			//TODO
			ViewModel.WhenAnyValue (v => v.Name).ObserveOn (RxApp.MainThreadScheduler).Subscribe (x => {
				lblPerkItem.Text = x;
			});


		}
	}
}

