﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using CoreGraphics;
using System.Collections.Generic;
using Unicorn.Core.Navigator;
using CoreAnimation;
using Unicorn.Core.iOS;

namespace DriveRightF.iOS
{
	public partial class BaseAutoLayoutScreen_AL<T> : BaseNavigableViewController<T>,ITransitionDelegate where T: BaseViewModel
	{
		public const float HEADER_HEIGHT = 60;
		public const float FOOTER_HEIGHT = 44;

//		public virtual float HEIGHT_HEADER
//		{
//			get{
//				return 64f;
//			}
//		}
//
//		public virtual float HEIGHT_TAB_BAR
//		{
//			get{
//				return 44f;
//			}
//		}

		//------------------

		private UIView _headerView;
//		private UIView _contentView;
		private UIView _tabView;
		private UIView _underGroundView;
		private UIView _rightMenu;
		private UIView _maskSuperView;

		private float _heightHeader = 64;
		private float _heightTabBar;

		CGPoint _lastPositon ;

		private const float MAX_ALPHA = 0.5f;

		public virtual float HEIGHT_HEADER
		{
			get{
				return _heightHeader;
			}
			set {
				_heightHeader = value;
				heightHeader.Constant = value;
				paddingTopContent.Constant = value;

			}
		}

		public virtual float HEIGHT_TAB_BAR
		{
			get{
				return _heightTabBar;
			}
			set {
				_heightTabBar = value;
				heightTabBar.Constant = value;
			}
		}

		public virtual float SPACE_MENU
		{
			get {
				return 60;
			}
		}

		public UIView HeaderView
		{
			get {
				return _headerView;
			}
			set {
				if (_headerView != null) {
					_headerView.RemoveFromSuperview ();
				}
				_headerView = value;
				FixSupperView(vTest, _headerView);
			}
		}

//		public UIView ContentView
//		{
//			get {
//				return _contentView;
//			}
//			set {
//				if (_contentView != null) {
//					_contentView.RemoveFromSuperview ();
//				}
//				_contentView = value;
//				FixSupperView(vPage, _contentView);
//			}
//		}
			
		public UIView TabView
		{
			get {
				return _tabView;
			}
			set {
				if (_tabView != null) {
					_tabView.RemoveFromSuperview ();
				}
				_tabView = value;
				_tabView.FixSupperView (vTab);
//				FixSupperView(vTab, _tabView);
			}
		}

		public UIView UnderGroundView
		{
			get {
				return _underGroundView;
			}
			set {
				if (_underGroundView != null) {
					_underGroundView.RemoveFromSuperview ();
				}
				_underGroundView = value;
				FixSupperView(vUnderGround, _underGroundView);
			}
		}

		public UIView RightMenu
		{
			get {
				return _rightMenu;
			}
			set {
				if (_rightMenu != null) {
					_rightMenu.RemoveFromSuperview ();
				}
				_rightMenu = value;
				FixSupperView(vRightMenu, _rightMenu);
			}
		}

		public UIView MaskSupperView
		{
			get {
				return _maskSuperView;
			}
			set {
				if (_maskSuperView != null) {
					_maskSuperView.RemoveFromSuperview ();
				}
				_maskSuperView = value;
				_maskSuperView.UserInteractionEnabled = false;
				FixSupperView(View, _maskSuperView);
			}
		}

		public BaseAutoLayoutScreen_AL () : base ("BaseAutoLayoutScreen_AL", null)
		{
		}		

		protected void FixSupperView(UIView supperView, UIView childView)
		{
			childView.TranslatesAutoresizingMaskIntoConstraints = false;
			supperView.BackgroundColor = UIColor.Clear;
			supperView.Add (childView);
			NSLayoutConstraint[] constraints = {
				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Top,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Top,1,0),
				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Bottom,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Bottom,1,0),
				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Trailing,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Trailing,1,0),
				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Leading,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Leading,1,0)
			};
			supperView.AddConstraints (constraints);
		}

		private void AnimationSepareted (bool isOpen)
		{
//			bool isOpen = paddingTopHeader.Constant < 0;
			paddingTopHeader.Constant = isOpen ? 0 : -HEIGHT_HEADER;
			paddingTopContent.Constant = isOpen ? HEIGHT_HEADER : vContent.Frame.Height;
			paddingBottomContent.Constant = isOpen ? 0 : -vContent.Frame.Height;
			UIView.Animate (0.5, () =>  {
				View.LayoutIfNeeded ();
			}, () =>  {
				vOverLay.UserInteractionEnabled = !isOpen;
			});
		}

		public void OpenView()
		{
			AnimationSepareted(true);
		}

		public void CloseView()
		{
			AnimationSepareted(false);
		}

		public void AnimationTabBar(bool isHidden)
		{
			HEIGHT_TAB_BAR = isHidden ? 0 : FOOTER_HEIGHT;
			//			markRightMenu.Alpha = isOpen ? 0 : MAX_ALPHA;
			UIView.Animate (0.5, () => {
				View.LayoutIfNeeded ();
			});
		}

		private void AnimationMenu(bool isOpen)
		{
			paddingLeftMenuRight.Constant = isOpen ? SPACE_MENU : View.Frame.Width;
//			markRightMenu.Alpha = isOpen ? 0 : MAX_ALPHA;
			UIView.Animate (0.5, () => {
				vMaskRightMenu.Alpha = !isOpen ? 0 : MAX_ALPHA;
				View.LayoutIfNeeded ();
			});
		}

		public void OpenMenu()
		{
			AnimationMenu(true);
		}

		public void CloseMenu()
		{
			AnimationMenu(false);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			ContainerView = vPage;

			HEIGHT_TAB_BAR = 44;
			HEIGHT_HEADER = 64;

			InitControls ();				
		}

		private void InitControls()
		{
			vOverLay.UserInteractionEnabled = false;

			tapOverlay.AddTarget (() => {
				OpenView();
			});

			panMenu.AddTarget (() => HandlePanMenu(panMenu));
			tapMaskMenu.AddTarget (() => {
				CloseMenu();
			});
				
		}

		private void HandlePanMenu(UIPanGestureRecognizer recognizer)
		{
			CGPoint touchPosition = recognizer.LocationInView(View);
			if (recognizer.State == UIGestureRecognizerState.Began) {
				//TODO: set origin location
				_lastPositon = recognizer.LocationInView (View);
			} else if (recognizer.State == UIGestureRecognizerState.Changed) {
				nfloat detal = touchPosition.X - _lastPositon.X;

				var xMove = paddingLeftMenuRight.Constant + detal;
				var alpha = (xMove - View.Frame.Width)* MAX_ALPHA/(SPACE_MENU - View.Frame.Width);
				if (xMove >=SPACE_MENU ) {
					paddingLeftMenuRight.Constant = xMove;
					vMaskRightMenu.Alpha = alpha;

				}
			} else if (recognizer.State == UIGestureRecognizerState.Cancelled || recognizer.State == UIGestureRecognizerState.Ended) {
				float velocity = (float)recognizer.VelocityInView (View).X;
				AnimationMenu (velocity < 0);
			}
			_lastPositon = touchPosition;
		}
	}
}

