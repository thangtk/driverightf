// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("SplashScreen")]
	partial class SplashScreen
	{
		[Outlet]
		UIKit.UILabel lblDesc { get; set; }

		[Outlet]
		UIKit.UIView viewFakeLoad { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (viewFakeLoad != null) {
				viewFakeLoad.Dispose ();
				viewFakeLoad = null;
			}

			if (lblDesc != null) {
				lblDesc.Dispose ();
				lblDesc = null;
			}
		}
	}
}
