﻿using System;
using Foundation;
using UIKit;
using DriveRightF.Core.Models;
using DriveRightF.Core;
using DriveRight.Core.ViewModels;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using CoreGraphics;
using DriveRightF.Core.Enums;
using ReactiveUI;
using System.Reactive.Linq;
using ZXing;
using ZXing.Common;
using Unicorn.Core.UI;
using DriveRightF.Core.iOS;
using Unicorn.Core.iOS;

namespace DriveRightF.iOS
{
	public partial class PerkDetailScreen : DriveRightBaseViewController<PerkDetailViewModel>
	{
		private string _perkId;
		private Perk _perk;
		private UILabel lblUsedDescription;
		private UICheckBox checkBoxIsUsed;
		private UILabel lblIsUsed;
		private iOSImageViewer _imageViewer;
		private UIActivityIndicatorView _imageLoadingView;


		public PerkDetailScreen (string perkId)
		{
			_perkId = perkId;
		}

		public override void LoadData (params object[] objs)
		{
			base.LoadData (objs);
			_perkId = (objs [0].ToString ());
			if (ViewModel != null) {
				ViewModel.LoadPerkCommand.Execute (_perkId);
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			ViewModel = new PerkDetailViewModel ();
			ViewModel.LoadPerkCommand.Execute (_perkId);

			_imageLoadingView = new UIActivityIndicatorView (new CGRect (0, 0, btnQRCode.Frame.Width, btnQRCode.Frame.Height));
			_imageLoadingView.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray;
			_imageLoadingView.Center = new CGPoint (btnQRCode.Frame.Width / 2, btnQRCode.Frame.Height / 2);
			_imageLoadingView.BackgroundColor = UIColor.LightGray.ColorWithAlpha (0.5f);

			_imageLoadingView.StartAnimating ();

			lblAwardTitle.Font = FontHub.RobotoBold20;
			lblDescription.Font = FontHub.RobotoLight15;
			lblSponsoredBy.Font = FontHub.RobotoLight15;
			lblExpDate.Font = FontHub.RobotoLight15;
		
			InitializeControls ();
			InitHandler ();
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);


		}

		private void InitHandler ()
		{
			ViewModel
				.WhenAnyValue (vm => vm.CheckBoxIsUseLabel)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
				x => checkBoxIsUsed.Label = x);
			ViewModel
				.WhenAnyValue (vm => vm.SponsoredByText)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
				x => lblSponsoredBy.Text = x);
			ViewModel
				.WhenAnyValue (vm => vm.ExpDateText)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
				x => lblExpDate.Text = x);
			

			ViewModel
				.WhenAnyValue (vm => vm.PerkCurrent)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (perk => {
				_perk = perk;
			});

			ViewModel
				.WhenAnyValue (vm => vm.IsIndefinite)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
				x => lblExpiryText.Hidden = x);

			ViewModel
				.WhenAnyValue (vm => vm.TxtAwardTitle)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
				x => lblAwardTitle.Text = x);

			ViewModel
				.WhenAnyValue (vm => vm.TxtDescription)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
				x => {
					lblUsedDescription.Text = x;
						lblUsedDescription.Frame = new CGRect (0, btnQRCode.Hidden ? 20f : btnQRCode.Frame.Bottom + 20f, View.Frame.Width - 10f, x.Length / 2.4f + 20f);
					checkBoxIsUsed.Frame = new CGRect (0, lblUsedDescription.Frame.Bottom + 5f, scrollViewContent.Frame.Width, 30);

					var scrollViewFrame = scrollViewContent.Frame;
					scrollViewFrame.Height = UIScreen.MainScreen.Bounds.Height - scrollViewFrame.Y - 5f;
					scrollViewContent.Frame = scrollViewFrame;

					scrollViewContent.ContentSize = new CGSize (tblViewContent.ContentSize.Width, 
						checkBoxIsUsed.Frame.Bottom + (DeviceUtil.IsiPhone5 ? 30f : 120f));
				});

//			ViewModel
//				.WhenAnyValue (vm => vm.BriefDesc)
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (
//				x => 
//					lblDescription.Text = x);

			ViewModel
				.WhenAnyValue (vm => vm.Used)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
				x => {
					checkBoxIsUsed.IsChecked = x;
					if (x)
						headerView.MainImage = GrayScaleViewUtil.ConvertToGrayScale (headerView.MainImage);
					else
						headerView.MainImage = UIImage.FromFile (ViewModel.AwardIcon);
				});

			ViewModel
				.WhenAnyValue (vm => vm.TxtAwardExpireDate)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
				x => lblExpiryText.Text = x);

			ViewModel
				.WhenAnyValue (vm => vm.TxtSponsoredName)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
				x => lblSponsoredText.Text = x);
			
			ViewModel
				.WhenAnyValue (vm => vm.AwardIcon)
				.Where (x => !String.IsNullOrEmpty (x))
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
				x => {
					headerView.MainImage = UIImage.FromFile (x);
				});

			ViewModel
				.WhenAnyValue (vm => vm.IconUrl)
				.Where (x => !String.IsNullOrEmpty (x))
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
				x => {
					headerView.MainImage = UIImage.LoadFromData (NSData.FromUrl (new NSUrl (x)));
				});
			
			ViewModel
				.WhenAnyValue (vm => vm.AwardBackgroundPicture)
				.Where (x => !String.IsNullOrEmpty (x))
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
				x => imgPicture.Image = UIImage.FromFile (x));

			ViewModel
				.WhenAnyValue (vm => vm.ImageUrl)
				.Where (x => !String.IsNullOrEmpty (x))
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
				x => imgPicture.Image = UIImage.LoadFromData (NSData.FromUrl (new NSUrl (x)))
			);

			ViewModel
				.WhenAnyValue (vm => vm.AwardQRNumber)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Select (x => {
				if (String.IsNullOrEmpty (x)) {
					btnQRCode.Hidden = true;
					return null;
				}
				btnQRCode.Hidden = false;
				btnQRCode.AddSubview (_imageLoadingView);
				return x;
			})
				.Where (x => !String.IsNullOrEmpty (x))
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Select (x => {
				var writer = new ZXing.BarcodeWriter () {
					Format = BarcodeFormat.QR_CODE,
					Options = new EncodingOptions {
						Height = 150,
						Width = 150,
					}
				};	

				#if DEBUG
				for (int i = 0; i < int.MaxValue / 5; i++) {
				}
				#endif

				return  writer.Write (x);
			})
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				_imageLoadingView.RemoveFromSuperview ();
				btnQRCode.SetBackgroundImage (x, UIControlState.Normal);
			});
		}

		protected void InitializeControls ()
		{
			btnQRCode.TouchUpInside += (object sender, EventArgs e) => {
				var image = ((UIButton)sender).CurrentBackgroundImage;
				if (image != null) {
					_imageViewer = new iOSImageViewer (image, View.Frame);
					_imageViewer.Show ();
				}
			};

			checkBoxIsUsed = new UICheckBox ();
			checkBoxIsUsed.Font = UIFont.SystemFontOfSize (20);
		
			checkBoxIsUsed.TextColor = UIColor.FromRGB (154, 154, 154);
			checkBoxIsUsed.OnTouch += OnIsUsedCheckChanged;

			lblUsedDescription = new UILabel (CGRect.Empty);
			lblUsedDescription.TextColor = UIColor.FromRGB (154, 154, 154);
			lblUsedDescription.Font = UIFont.SystemFontOfSize (14);
			lblUsedDescription.TextAlignment = UITextAlignment.Justified;
			lblUsedDescription.Lines = 30;

			scrollViewContent.AddSubviews (checkBoxIsUsed, lblUsedDescription);
		}

		private void OnIsUsedCheckChanged (object sender, EventArgs e)
		{
			if (checkBoxIsUsed.IsChecked == true) {
				headerView.MainImage = GrayScaleViewUtil.ConvertToGrayScale (headerView.MainImage);
				_perk.Used = true;
			} else {
				headerView.MainImage = UIImage.FromFile (_perk.Icon);
				_perk.Used = false;
			}
			ViewModel.SavePerkCommand.Execute (_perk);
		}

		public void OnLoadperkDetailCompleted (Perk perk)
		{
			_perk = perk;
		}
	}

	public class perkTableSource : UITableViewDataSource
	{
		private string[] _data;
		private string _cellId = "perkCell";

		public perkTableSource (string data)
		{
			string[] str = data.Split (new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
			_data = str;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return _data.Length;
		}

		//		public override nint QRCodeOfSections(UITableView tableView)
		//		{
		//			return 1;
		//		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (_cellId);
			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Default, null);
			}

			cell.BackgroundColor = UIColor.Clear;
			cell.ImageView.Image = new UIImage ("bullet.png");
			cell.TextLabel.Text = _data [indexPath.Row];
			cell.TextLabel.Font = FontHub.RobotoLight12;
			cell.TextLabel.TextColor = UIColor.Gray;

			return cell;
		}
	}
}

