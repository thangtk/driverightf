﻿using System;
using System.Drawing;
using Foundation;
using UIKit;
using DriveRightF.Core.ViewModels;
using DriveRightF.Core;
using ReactiveUI;
using System.Reactive.Linq;
using CoreGraphics;

namespace DriveRightF.iOS
{
	public partial class LoginScreen : DriveRightBaseViewController<LoginViewModel>
	{

		private UIView _activeview;            
		private float _scrollAmount = 0.0f;   
		private float _bottom = 0.0f;          
		private float _offset = 60f;          
		private bool _moveViewUp = false;

		public LoginScreen () : base ("LoginScreen", null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			ViewModel = new LoginViewModel ();

			InitView ();
			InitBinding ();

		}

		public override float OffsetKeyboard {
			get {
				return -200f;
			}
		}

		private void InitView()
		{
			btnGo.Font = FontHub.RobotoRegular35;
			lblWrongNumber.Hidden = true;
			txtPhoneNumber.Layer.CornerRadius = 10;

			View.AddGestureRecognizer (new UITapGestureRecognizer (() => {
				View.EndEditing(false);
			}));
		}
			
		private void InitBinding()
		{
			this.BindCommand(
				ViewModel,
				vm => vm.FormatValidate,
				v => v.btnGo);

			this.Bind (
				ViewModel,
				vm => vm.LoginText,
				v => v.txtPhoneNumber.Text);
		
			this.WhenAnyValue (v => v.txtPhoneNumber.Text).
			ObserveOn (RxApp.MainThreadScheduler).
			Subscribe (x => {
				ViewModel.LoginText = x;
			});

			this.Bind (
				ViewModel,
				vm => vm.ErrorLabelHidden,
				v => v.lblWrongNumber.Hidden);

			ViewModel.WhenAnyValue (vm => vm.ErrorLabelHidden).ObserveOn (RxApp.MainThreadScheduler).Subscribe (x => {
				lblWrongNumber.Hidden = x;
			});
		}
			
		public override void ReloadView(){
		}
		public override void LoadData(params object[] objs){
		}
		public override bool IsShouldCache(){
			return true;
		}

	}
}

