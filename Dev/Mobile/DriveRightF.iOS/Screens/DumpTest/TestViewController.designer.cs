// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("TestViewController")]
	partial class TestViewController
	{
		[Outlet]
		UIKit.NSLayoutConstraint constraint1 { get; set; }

		[Outlet]
		UIKit.UIView vContent { get; set; }

		[Outlet]
		UIKit.UIView vHeader { get; set; }

		[Outlet]
		UIKit.UIView view { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (vContent != null) {
				vContent.Dispose ();
				vContent = null;
			}

			if (vHeader != null) {
				vHeader.Dispose ();
				vHeader = null;
			}

			if (constraint1 != null) {
				constraint1.Dispose ();
				constraint1 = null;
			}

			if (view != null) {
				view.Dispose ();
				view = null;
			}
		}
	}
}
