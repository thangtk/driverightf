﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using CoreGraphics;

namespace DriveRightF.iOS
{
	public partial class TestTabScreen :  DriveRightBaseViewController<SplashViewModel>
	{
		private UIView _viewReplace;
		public TestTabScreen () : base ("TestTabScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

//			TabDetail1Screen tab1Screen = new TabDetail1Screen();
//			contentView.AddSubview(tab1Screen.View);
//			View.BringSubviewToFront (tabBar);
//			contentView.RemoveFromSuperview();
//			this.Add(contentView);
//			separateView.AddViewGroup(header,vBottom);
			tabBar.SelectedItem = tab1;
			contentView.ClipsToBounds = true;
//			// Perform any additional setup after loading the view, typically from a nib.
			tabBar.ItemSelected += (object sender, UITabBarItemEventArgs e) => 
			{
				

				CGRect frameOrg = contentView.Frame;
				if(e.Item == tab1)
				{
					tabBar.SelectedImageTintColor = UIColor.White;
//					this.View.Frame;
					if(_viewReplace != null)
					{
						_viewReplace.RemoveFromSuperview();
					}

					TabDetail1Screen tab1Screen = new TabDetail1Screen();
					_viewReplace = tab1Screen.View;
					_viewReplace.RemoveFromSuperview();
					contentView.Add(_viewReplace);
//					View.BringSubviewToFront (tabBar);
//					View.BringSubviewToFront(header);
//					contentView.AddSubview(tab1Screen.View);
//					contentView.RemoveFromSuperview();
//					this.Add(contentView);
				}
				else if(e.Item == tab2)
				{
					tabBar.SelectedImageTintColor = UIColor.Yellow;
					if(_viewReplace != null)
					{
						_viewReplace.RemoveFromSuperview();
					}
					TabDetail2Screen tab1Screen = new TabDetail2Screen();
					_viewReplace = tab1Screen.View;
					_viewReplace.RemoveFromSuperview();
					contentView.Add(_viewReplace);
//					View.BringSubviewToFront (tabBar);
//					View.BringSubviewToFront(header);
//					contentView.AddSubview(tab1Screen.View);
//					contentView.RemoveFromSuperview();
//					this.Add(contentView);
				}
			};

		}
	}
}

