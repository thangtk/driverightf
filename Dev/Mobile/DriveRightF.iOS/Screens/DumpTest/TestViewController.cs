﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;

namespace DriveRightF.iOS
{
	public partial class TestViewController : DriveRightBaseViewController<SplashViewModel>
	{
		public TestViewController () : base ("TestViewController", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
//			separateView.Frame = View.Frame;
//			separateView.AddViewGroup (vHeader, vContent);
			// Perform any additional setup after loading the view, typically from a nib.

			view.Frame = new CoreGraphics.CGRect (0, 0, 100, 100);
		}
	}
}

