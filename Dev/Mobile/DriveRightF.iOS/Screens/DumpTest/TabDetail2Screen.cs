﻿
using System;

using Foundation;
using UIKit;

namespace DriveRightF.iOS
{
	public partial class TabDetail2Screen : UIViewController
	{
		public TabDetail2Screen () : base ("TabDetail2Screen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.
		}
	}
}

