// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("TestTabScreen")]
	partial class TestTabScreen
	{
		[Outlet]
		UIKit.UIView contentView { get; set; }

		[Outlet]
		UIKit.UIView header { get; set; }

		[Outlet]
		Foundation.NSObject separateView { get; set; }

		[Outlet]
		UIKit.UITabBarItem tab1 { get; set; }

		[Outlet]
		UIKit.UITabBarItem tab2 { get; set; }

		[Outlet]
		UIKit.UITabBar tabBar { get; set; }

		[Outlet]
		UIKit.UIView vBottom { get; set; }

		[Outlet]
		UIKit.UIView vUnderGround { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (contentView != null) {
				contentView.Dispose ();
				contentView = null;
			}

			if (header != null) {
				header.Dispose ();
				header = null;
			}

			if (tab1 != null) {
				tab1.Dispose ();
				tab1 = null;
			}

			if (tab2 != null) {
				tab2.Dispose ();
				tab2 = null;
			}

			if (tabBar != null) {
				tabBar.Dispose ();
				tabBar = null;
			}

			if (vBottom != null) {
				vBottom.Dispose ();
				vBottom = null;
			}

			if (vUnderGround != null) {
				vUnderGround.Dispose ();
				vUnderGround = null;
			}

			if (separateView != null) {
				separateView.Dispose ();
				separateView = null;
			}
		}
	}
}
