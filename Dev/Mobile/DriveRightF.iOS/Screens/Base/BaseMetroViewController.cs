﻿using System;
using DriveRightF.Core;
using UIKit;
using Unicorn.Core;
using CoreGraphics;
using System.Drawing;
using System.Collections.Generic;
using Unicorn.Core.UI;
using Foundation;
using ReactiveUI;
using System.Reactive.Linq;

namespace DriveRightF.iOS
{
	public abstract class BaseMetroViewController<TModel,TView>: BaseHandlerHeaderViewController<BaseMetroViewModel<TModel>>   
		where TModel: ITile
		where TView : UBaseView

	{
		static int _id = 0;
//		protected UIView _contentView;

//		private UIScrollView _scrollView;

		protected abstract UIScrollView ScrollView { get;}
		protected abstract List<TView> ListView { get;}
		protected abstract UIView ContainerView { get;}

		protected abstract void CreateViewModel();

		protected abstract void ApplyView (TModel model, TView view, bool isBigTile);

		protected BaseMetroViewController (string nibNameOrNull, NSBundle nibBundleOrNull)
			:base(nibNameOrNull,nibBundleOrNull)
		{
			_id++;
			Debugger.ShowCost ("++++++++++++++++  --- Construct ---");
		}

		protected BaseMetroViewController (IntPtr handle)
			:base(handle)
		{
		}

		protected BaseMetroViewController (NSObjectFlag f)
			:base(f)
		{
		}

		protected BaseMetroViewController ():base()
		{

		}

		protected BaseMetroViewController (NSCoder c):base(c)
		{

		}

		public override void LoadView(){
			base.LoadView ();
			Debugger.ShowCost ("++++++++++++++++  --- LoadView ---");
		}
		public override void ViewDidLoad()
		{
			Debugger.ShowCost ("++++++++++++++++  --- reset time ---");
			base.ViewDidLoad ();
			//
			InitView ();

			CreateViewModel();
			Debugger.ShowCost ("++++++++++++++++ Base Metro screen - Create ViewModel");
			Observable.Start (()=>{
				AddTiles ();
			},RxApp.TaskpoolScheduler);


			Debugger.ShowCost ("++++++++++++++++ Base Metro screen - ViewDidLoad");
		}

		private void AddTiles()
		{
			for (int i = 0; i < ViewModel.TileViewModels.Count; i++) {
				ApplyView (ViewModel.TileViewModels [i], ListView [i],i==0);
			}

		}

		private void UpdateContentHeight(float height){
		}

		private void InitView()
		{
			View.BackgroundColor = UIColor.White;
			System.Diagnostics.Debug.WriteLine ("++++++++++++++++  InitView");
		}
		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();
			Debugger.ShowCost ("++++++++++++++++ end Base Metro screen - ViewWillLayoutSubviews - " + _id);
		}
		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();
			Debugger.ShowCost ("++++++++++++++++ Base Metro screen - ViewDidLayoutSubviews - " + _id);
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			//
			Debugger.ShowCost ("++++++++++++++++ Base Metro screen - ViewDidAppear - " + _id);
		}
	}
}

