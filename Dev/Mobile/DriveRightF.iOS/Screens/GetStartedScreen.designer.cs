// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("GetStartedScreen")]
	partial class GetStartedScreen
	{
		[Outlet]
		UIKit.UIView _backgroundView { get; set; }

		[Outlet]
		UIKit.UIButton _btnGetStarted { get; set; }

		[Outlet]
		UIKit.UIImageView _imgBackground { get; set; }

		[Outlet]
		UIKit.UILabel _lblGoal { get; set; }

		[Outlet]
		UIKit.UILabel _lblMission { get; set; }

		[Outlet]
		UIKit.UILabel _lblUnicorn { get; set; }

		[Outlet]
		UIKit.UIPageControl _pageControl { get; set; }

		[Outlet]
		UIKit.UIScrollView _svBackground { get; set; }

		[Outlet]
		UIKit.UITextView _txtDescriptionGoal { get; set; }

		[Outlet]
		UIKit.UITextView _txtDescriptionMission { get; set; }

		[Outlet]
		UIKit.UITextView _txtDescriptionUnicorn { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (_backgroundView != null) {
				_backgroundView.Dispose ();
				_backgroundView = null;
			}

			if (_btnGetStarted != null) {
				_btnGetStarted.Dispose ();
				_btnGetStarted = null;
			}

			if (_lblGoal != null) {
				_lblGoal.Dispose ();
				_lblGoal = null;
			}

			if (_lblMission != null) {
				_lblMission.Dispose ();
				_lblMission = null;
			}

			if (_lblUnicorn != null) {
				_lblUnicorn.Dispose ();
				_lblUnicorn = null;
			}

			if (_pageControl != null) {
				_pageControl.Dispose ();
				_pageControl = null;
			}

			if (_svBackground != null) {
				_svBackground.Dispose ();
				_svBackground = null;
			}

			if (_txtDescriptionGoal != null) {
				_txtDescriptionGoal.Dispose ();
				_txtDescriptionGoal = null;
			}

			if (_txtDescriptionMission != null) {
				_txtDescriptionMission.Dispose ();
				_txtDescriptionMission = null;
			}

			if (_txtDescriptionUnicorn != null) {
				_txtDescriptionUnicorn.Dispose ();
				_txtDescriptionUnicorn = null;
			}

			if (_imgBackground != null) {
				_imgBackground.Dispose ();
				_imgBackground = null;
			}
		}
	}
}
