﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using DriveRightF.Core.Enums;

namespace DriveRightF.iOS
{
	public partial class MenuHomeScreen : BaseHandlerHeaderViewController<MenuViewModel>
	{
		public MenuHomeScreen () : base ("MenuHomeScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			ViewModel = new MenuViewModel ();

			InitControls ();
			InitTranslator ();
		}

		private void InitTranslator()
		{
			lblSetting.Text = Translator.Translate ("BUTTON_SETTING");
			lblAbout.Text = Translator.Translate ("BUTTON_ABOUT");
			lblHelp.Text = Translator.Translate ("BUTTON_HELP");
			lblLegal.Text = Translator.Translate ("BUTTON_LEGAL");
		}

		private void InitControls()
		{
			tapSetting.AddTarget (() => DependencyService.Get<INavigator> ().Navigate ((int)Screen.Setting));

			tapAbout.AddTarget (() => DependencyService.Get<INavigator> ().Navigate ((int)Screen.About));

			tapHelp.AddTarget (() => DependencyService.Get<INavigator> ().Navigate ((int)Screen.Help));

			tapLegal.AddTarget (() => DependencyService.Get<INavigator> ().Navigate ((int)Screen.Legal));
		}
	}
}

