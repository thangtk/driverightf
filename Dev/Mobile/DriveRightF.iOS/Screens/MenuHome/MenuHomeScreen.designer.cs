// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("MenuHomeScreen")]
	partial class MenuHomeScreen
	{
		[Outlet]
		UIKit.UILabel lblAbout { get; set; }

		[Outlet]
		UIKit.UILabel lblHelp { get; set; }

		[Outlet]
		UIKit.UILabel lblLegal { get; set; }

		[Outlet]
		UIKit.UILabel lblSetting { get; set; }

		[Outlet]
		UIKit.UITapGestureRecognizer tapAbout { get; set; }

		[Outlet]
		UIKit.UITapGestureRecognizer tapHelp { get; set; }

		[Outlet]
		UIKit.UITapGestureRecognizer tapLegal { get; set; }

		[Outlet]
		UIKit.UITapGestureRecognizer tapSetting { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (tapSetting != null) {
				tapSetting.Dispose ();
				tapSetting = null;
			}

			if (tapAbout != null) {
				tapAbout.Dispose ();
				tapAbout = null;
			}

			if (tapHelp != null) {
				tapHelp.Dispose ();
				tapHelp = null;
			}

			if (tapLegal != null) {
				tapLegal.Dispose ();
				tapLegal = null;
			}

			if (lblSetting != null) {
				lblSetting.Dispose ();
				lblSetting = null;
			}

			if (lblHelp != null) {
				lblHelp.Dispose ();
				lblHelp = null;
			}

			if (lblAbout != null) {
				lblAbout.Dispose ();
				lblAbout = null;
			}

			if (lblLegal != null) {
				lblLegal.Dispose ();
				lblLegal = null;
			}
		}
	}
}
