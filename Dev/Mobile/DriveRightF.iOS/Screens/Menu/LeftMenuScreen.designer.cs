// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("LeftMenuScreen")]
	partial class LeftMenuScreen
	{
		[Outlet]
		UIKit.UIImageView imgIcon { get; set; }

		[Outlet]
		UIKit.UITableView tblMenu { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (imgIcon != null) {
				imgIcon.Dispose ();
				imgIcon = null;
			}

			if (tblMenu != null) {
				tblMenu.Dispose ();
				tblMenu = null;
			}
		}
	}
}
