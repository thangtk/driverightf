﻿
using System;
using System.Collections.Generic;

using System.Drawing;
using DriveRightF.Core;
using Foundation;
using ReactiveUI;
using UIKit;
using DriveRightF.Core.ViewModels;
using DriveRightF.Core.Models;
using System.Reactive.Linq;

namespace DriveRightF.iOS
{
	public partial class LeftMenuScreen : DriveRightBaseViewController<LeftMenuViewModel>
	{
		List<LeftMenuItem> _items;

		public LeftMenuScreen () : base ("LeftMenuScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			InitBinding ();
			InitView ();
		}

		private void InitBinding()
		{
			ViewModel = new LeftMenuViewModel ();

			this.Bind (ViewModel, vm => vm.Items, v => v._items);

			ViewModel.WhenAnyValue (vm => vm.Image)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (image => {
					if (!String.IsNullOrEmpty(image) && UIImage.FromFile (image) != null)
						imgIcon.Image = UIImage.FromFile (image);
					else
						imgIcon.Image = UIImage.FromFile (LeftMenuViewModel.DEFAULT_IMAGE);
			});
		}

		private void InitView()
		{
			tblMenu.Source = new MenuTableViewSource (_items);
		}

		public override void ReloadView ()
		{
			base.ReloadView ();

			if (ViewModel != null)
				ViewModel.ReloadImage ();
		}
	}
}

