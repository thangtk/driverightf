﻿using System;
using System.Collections.Generic;
using DriveRightF.Core.iOS;
using Foundation;
using ObjCRuntime;
using UIKit;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using DriveRightF.Core.Enums;
using DriveRightF.Core;

namespace DriveRightF.iOS
{
	public class MenuTableViewSource : UITableViewSource
	{
		private List<LeftMenuItem> _source;

		public MenuTableViewSource (List<LeftMenuItem> source)
		{
			source.Add (new LeftMenuItem () { Title = "Legal", Icon = "", Screen = null });
			_source = source;
		}

		public override UITableViewCell GetCell (UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell ("MenuItemViewCell");
			if (cell == null) {
				var views = NSBundle.MainBundle.LoadNib ("MenuItemViewCell", tableView, null);
				cell = Runtime.GetNSObject (views.ValueAt (0)) as MenuItemViewCell;

				if (_source != null && _source.Count != 0) {
					(cell as MenuItemViewCell).Item = _source [indexPath.Row];
				}
			}
			return cell;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return _source.Count;
		}
	}
}

