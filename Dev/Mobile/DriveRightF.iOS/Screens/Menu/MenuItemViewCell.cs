﻿
using System;
using System.Drawing;

using Foundation;
using UIKit;
using Unicorn.Core.UI;
using DriveRightF.Core;
using ReactiveUI;
using System.Reactive.Linq;
using Unicorn.Core;
using DriveRightF.Core.Enums;
using Unicorn.Core.Navigator;
using DriveRightF.Core.ViewModels;

namespace DriveRightF.iOS
{
	public partial class MenuItemViewCell : UBaseTableViewCell<LeftMenuItemViewModel>
	{
		public static readonly UINib Nib = UINib.FromName ("MenuItemViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("MenuItemViewCell");

		private LeftMenuItem _item;

		public LeftMenuItem Item {
			get {
				return _item;
			}
			set {
				_item = value;
				if (ViewModel != null)
					ViewModel.Item = _item;
				InitView ();
			}
		}

		public MenuItemViewCell () : base ()
		{
			InitBinding ();
		}

		public MenuItemViewCell (IntPtr handle) : base (handle)
		{
			InitBinding ();
		}

		public static MenuItemViewCell Create ()
		{
			return (MenuItemViewCell)Nib.Instantiate (null, null) [0];
		}

		private void InitView()
		{
			lblTitle.Font = FontHub.RobotoRegular22;

			if (Item.Title == "Legal")
				ShowLegalLabel ();
			else
				lblTitle.Text = Item.Title;

			imgIcon.Image = UIImage.FromFile (Item.Icon);
		}

		private void InitBinding()
		{
			ViewModel = new LeftMenuItemViewModel ();

			this.Bind (ViewModel, vm => vm.Item, v => v.Item);

			AddGestureRecognizer (new UITapGestureRecognizer (() => {
				ViewModel.ItemViewCellClick.Execute (null);
			}));
		}

		private void ShowLegalLabel()
		{
			lblLegal.TextColor = UIColor.White;
			lblLegal.Alpha = 1;
			lblLegal.Font = FontHub.RobotoRegular22;
			lblLegal.BackgroundColor = UIColor.FromRGB (36, 36, 36);
		}
	}
}

