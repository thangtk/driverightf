﻿using System;
using DriveRightF.Core;
using System.Drawing;
using CoreGraphics;
using DriveRightF.Core.Enums;
using UIKit;
using Unicorn.Core;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Threading;
using Unicorn.Core.iOS;
using System.Threading.Tasks;

namespace DriveRightF.iOS
{
	public abstract class BaseMetroScreen<T> : BaseHandlerHeaderViewController<BaseListTileVM<T>> where T: ITile
	{
		static int _id = 0;
		protected UIView _contentView;

		private UIScrollView _scrollView;

		protected abstract void CreateViewModel();
		protected abstract UIView CreatTileView (T tile, bool isBigTile);

		protected BaseMetroScreen(){
//			Debugger.StartTracking ();
//			Debugger.ShowCost ("++++++++++++++++  --- reset time ---");
			_id++;
			MyDebugger.ShowCost ("++++++++++++++++ BaseMetroScreen  --- Constructor ---");
		}
		public override void LoadView(){
			base.LoadView ();
//			Debugger.ShowCost ("++++++++++++++++  --- LoadView ---");
		}
		public override void ViewDidLoad()
		{
			MyDebugger.ShowCost ("++++++++++++++++ Base Metro screen - ViewDidLoad 0");
			base.ViewDidLoad ();
			//
			InitView ();
			CreateViewModel();
			AddTiles ();
			MyDebugger.ShowCost ("++++++++++++++++ Base Metro screen - ViewDidLoad 4");
		}

		private void AddTiles()
		{
			var contentSize = new Size ((int)View.Frame.Width, (int)View.Frame.Height);
			ViewModel.ComputeTileFrame (contentSize);
			float rows = (float)ViewModel.TileViewModels.Count / 3f + 1f;
			UpdateContentHeight (rows * (float)View.Frame.Width / 3f - (rows - 2f));
			//
			AddTilesDelay();
		}

		private async void AddTilesDelay(){
			foreach (var tile in ViewModel.TileViewModels) {
//				await Task.Delay(10);
				UIView tileView = CreatTileView (tile, ViewModel.TileViewModels.IndexOf (tile) == 0);
//				tileView.Scale(true);
				_contentView.AddSubview(tileView);
			}
		}

		private void UpdateContentHeight(float height){
			var frame = _contentView.Frame;
			frame.Height = height;
			_contentView.Frame = frame;
			_scrollView.ContentSize = _contentView.Frame.Size;
		}

		private void InitView()
		{
			View.BackgroundColor = UIColor.White;
			var contentFrame = new CGRect (0, 0, View.Frame.Width, View.Frame.Height - HomeScreen.HEADER_HEIGHT - HomeScreen.FOOTER_HEIGHT);
			_scrollView = new UIScrollView (contentFrame);
			_scrollView.ShowsVerticalScrollIndicator = false;
			_contentView = new UIView (contentFrame);
			_scrollView.AddSubview (_contentView);
			View.AddSubview (_scrollView);
			System.Diagnostics.Debug.WriteLine ("++++++++++++++++  InitView");
		}
		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();
//			Debugger.ShowCost ("++++++++++++++++ Base Metro screen - ViewWillLayoutSubviews - " + _id);
		}
		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();
//			Debugger.ShowCost ("++++++++++++++++ Base Metro screen - ViewDidLayoutSubviews - " + _id);
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			//
			MyDebugger.ShowCost ("++++++++++++++++ Base Metro screen - ViewDidAppear - " + _id);
		}
	}
}

