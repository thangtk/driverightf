﻿
using System;

using Foundation;
using UIKit;
using System.Collections.Generic;
using CoreGraphics;
using ReactiveUI;
using System.Reactive.Linq;
using System.Timers;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;


namespace DriveRightF.iOS
{
	public partial class SlotMachineScreen : DriveRightBaseViewController<HomeViewModel>
	{
		Timer _timer;

		public SlotMachineScreen () : base ("SlotMachineScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			int count = 0;
		
			vColumn1.BackgroundColor = UIColor.FromPatternImage (UIImage.FromFile("imagegloss.png"));
			List<UIImageView> firstColumnSubviews = new List<UIImageView> () {
				img11,
				img12,
				img13,
				img14
			};

			List<UIImage> imageList = new List<UIImage> (){
				UIImage.FromFile("bar.png"),
				UIImage.FromFile("bell.png"),
				UIImage.FromFile("cherry.png"),
				UIImage.FromFile("orange.png"),
				UIImage.FromFile("seven.png"),
				UIImage.FromFile("watermelon.png"),
			};

			int imageIndicator = 0;
			for (int i = 0; i < firstColumnSubviews.Count; i++) {
				firstColumnSubviews [i].Image = imageList [i];
				imageIndicator++;
			}
				
			_timer = new Timer (10);


			_timer.Elapsed += (sender, e) => {
				InvokeOnMainThread (() => {
					count++;
					foreach (UIImageView view in firstColumnSubviews) {
						var frame = view.Frame;
						nfloat value = 0f;
						if(count > 500 && count >= 800)
							value = 5f;
						else if(count > 800)
							value = 1f;
						frame.Y += 20f - value;
						if (frame.Y >= 270)
						{
							Console.WriteLine(imageIndicator % 6);
							view.Image = imageList[(imageIndicator++) % 6];
							frame.Y = -90 + (frame.Y - 270);
						}
						view.Frame = frame;
					}
					if (count == 1001) {
						_timer.Stop ();
						var min = firstColumnSubviews [0].Frame.Y;
						for (int i = 1; i < firstColumnSubviews.Count; i++) {
							var temp = firstColumnSubviews [i].Frame.Y;
							if (Math.Abs (temp) < Math.Abs (min))
								min = temp;
						}
						
						foreach (UIImageView view in firstColumnSubviews) {
							UIView.Animate (
								1.5,
								() => {
									var frame = view.Frame;
									frame.Y -= (nfloat)min;
									view.Frame = frame;
									//									view.Transform = CGAffineTransform.MakeTranslation(0, -(nfloat)min);
								},
								() => {
								});
						}	
					}
				});
			};
//			_timer.Start ();

			View.AddGestureRecognizer (new UIPanGestureRecognizer (new Action<UIPanGestureRecognizer> (delegate(UIPanGestureRecognizer recognizer) {
				if (recognizer.State == UIGestureRecognizerState.Ended && recognizer.VelocityInView(View).Y > 3) {
					InvokeOnMainThread (() => {
						count = 0;
						if(!_timer.Enabled)
							_timer.Start ();
					});
				}
			})));
		}
	}
}

