﻿using System;
using System.Collections.Generic;
using DriveRightF.Core.Models;
using UIKit;
using Foundation;
using ObjCRuntime;
using Unicorn.Core;

namespace DriveRightF.iOS
{
	public class TripTableSource : UITableViewSource
	{
		private List<TripSummary> _items = new List<TripSummary> ();
		public List<TripSummary> Items { 
			get{ 
				return _items;
			}
			set { 
				_items = value;
			}
		}

		public TripTableSource ()
		{
		}
			
		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return Items.Count;
		}

		public override bool ShouldHighlightRow (UITableView tableView, NSIndexPath rowIndexPath)
		{
			return false;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
//			Debugger.StartTracking ();
			var cell = tableView.DequeueReusableCell (TripTableViewCell.Key) as TripTableViewCell;
			if (cell == null) {
				cell = TripTableViewCell.Create ();
			} else {
//				System.Diagnostics.Debug.WriteLine ("Reuseable cell");
			}
			int index = indexPath.Row;
			if (Items != null && Items.Count > index) {
				cell.UpdateCell (Items[index], index);
			}
//			Debugger.ShowCost ("Get cell");
			return cell;
		}

		public override nfloat GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			return 60f;
		}
	}
}

