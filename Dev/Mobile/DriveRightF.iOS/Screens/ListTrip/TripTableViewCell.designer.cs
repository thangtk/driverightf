// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("TripTableViewCell")]
	partial class TripTableViewCell
	{
		[Outlet]
		UIKit.UILabel lblDistance { get; set; }

		[Outlet]
		UIKit.UILabel lblDuration { get; set; }

		[Outlet]
		UIKit.UILabel lblScore { get; set; }

		[Outlet]
		UIKit.UILabel lblStartTime { get; set; }

		[Outlet]
		DriveRightF.iOS.ProgressScoreView scoreView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (lblDistance != null) {
				lblDistance.Dispose ();
				lblDistance = null;
			}

			if (lblDuration != null) {
				lblDuration.Dispose ();
				lblDuration = null;
			}

			if (lblScore != null) {
				lblScore.Dispose ();
				lblScore = null;
			}

			if (lblStartTime != null) {
				lblStartTime.Dispose ();
				lblStartTime = null;
			}

			if (scoreView != null) {
				scoreView.Dispose ();
				scoreView = null;
			}
		}
	}
}
