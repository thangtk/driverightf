﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core.iOS;
using DriveRightF.Core.Models;
using ReactiveUI;
using Unicorn.Core.UI;
using DriveRightF.Core.ViewModels;
using System.Reactive.Linq;
using Unicorn.Core;
using System.Threading.Tasks;

namespace DriveRightF.iOS
{
	public partial class ListTripItemViewCell : UBaseTableViewCell<TripItemViewModel>
	{
		public static readonly UINib Nib = UINib.FromName ("ListTripItemViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("ListTripItemViewCell");

		private ListTripItemViewCell (IntPtr handle) : base (handle)
		{
		}

		private void Initialize ()
		{
//			Debugger.StartTracking();
			InitControls ();
//			Debugger.ShowCost ("InitControls");
			InitBindings ();
//			Debugger.ShowCost ("InitBindings");
		}

		public static ListTripItemViewCell Create ()
		{
			var c = (ListTripItemViewCell)Nib.Instantiate (null, null) [0];
			c.Initialize ();
			return c;
		}

		private void InitBindings ()
		{
			ViewModel = new TripItemViewModel ();
			//
			this.AddGestureRecognizer (new UITapGestureRecognizer (() => {
				if (ViewModel != null) {
					ViewModel.ItemClickCommand.Execute (null);
				}
			}));
			// Performance problem
//			this.OneWayBind (ViewModel, vm => vm.StartDay, v => v.lblStartDay.Text);
//			this.OneWayBind (ViewModel, vm => vm.StartTime, v => v.lblStartTime.Text);
//			this.OneWayBind (ViewModel, vm => vm.Duration, v => v.lblDuration.Text);
//			this.OneWayBind (ViewModel, vm => vm.Distance, v => v.lblDistance.Text);
//			this.OneWayBind (ViewModel, vm => vm.Score, v => v.lblScore.Text);
//			lblStartDay.Text = ViewModel.StartDay;
//			lblStartTime.Text = ViewModel.StartTime;
//			lblDuration.Text = ViewModel.Duration;
//			lblDistance.Text = ViewModel.Distance;
//			lblScore.Text = ViewModel.Score;

			AddGestureRecognizer (new UITapGestureRecognizer (() => {
				ViewModel.ItemClickCommand.Execute("11");
			}));
		}

		private void InitControls ()
		{
			lblDistance.Font = FontHub.RobotoLight12;
			lblDuration.Font = FontHub.RobotoLight12;
			lblScore.Font = FontHub.RobotoLight12;
			lblStartDay.Font = FontHub.RobotoLight12;
			lblStartTime.Font = FontHub.RobotoLight12;
		}

		public void UpdateCell (Trip item, int index)
		{
			ViewModel.Item = item;
			if (index % 2 == 0)
				this.BackgroundColor = UIColor.FromRGB (218, 218, 218);
			else
				this.BackgroundColor = UIColor.White;
			//
			UpdateUI ();
		}

		private void UpdateUI ()
		{
			lblStartDay.Text = ViewModel.StartDay;
			lblStartTime.Text = ViewModel.StartTime;
			lblDuration.Text = ViewModel.Duration;
			lblDistance.Text = ViewModel.Distance;
			lblScore.Text = ViewModel.Score;
		}
	}
}

