﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using CoreGraphics;
using ReactiveUI;
using Unicorn.Core;
using System.Linq;
using DriveRightF.Core.Models;
using System.Reactive.Linq;
using System.Reactive.Concurrency;

namespace DriveRightF.iOS
{
	public partial class ListTripScreen : BaseHandlerHeaderViewController<ListTripViewModel>
	{
		TripMonth _trip;

		public ListTripScreen () : base ("ListTripScreen", null)
		{
// 			MessagingCenter.Subscribe<TripItemViewModel> (this, "Click", (sender) => {
//				// do something whenever the "Hi" message is sent
//				_tripId = sender.Item.TripId;
//			});
		}

		public override void LoadData(params object[] objs)
		{
			base.LoadData (objs);

			if (objs != null && objs.Length > 0)
			{
//				System.Threading.Thread.Sleep(5000);

				_trip = objs [0] as TripMonth;
				if (_trip.Month.Date.CompareTo (new DateTime (9999, 1, 1)) >= 0) {
					ViewModel.CurrentMonth = Convert.ToDateTime (DateTime.Today.ToString ("yyyy-MM"));				
				} else {
					ViewModel.CurrentMonth = _trip.Month;
				}
				scrollView.Subviews.All (v => {
					v.RemoveFromSuperview();
					v.Dispose();
					return true;
				});
//				NSTimer.CreateScheduledTimer (TimeSpan.FromMilliseconds (10), (timer) => {
//					RxApp.MainThreadScheduler.Schedule(()=>{
						AddSubContentViewsDelay();
//					});
//				});

			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			UIApplication.SharedApplication.SetStatusBarHidden (false, false);
			ViewModel = new ListTripViewModel ();
			InitControls ();
			CreateBindingsAsync ();

			// Init data
//			ViewModel.CurrentMonth = DateTime.Today;
//			AddSubContentViewsDelay ();
		}

//		public override void ViewDidLayoutSubviews ()
//		{
//			base.ViewDidLayoutSubviews ();
//			if (_isFirstTime == false) {
//				_isFirstTime = true;
//				AddSubContentViewsDelay ();
//			}
//			InitControls ();
//			InitBinding ();
//		}

		protected void InitControls ()
		{
//			monthNavigatorView.BackgroundColor = UIColor.FromRGB (241, 236, 243);

//			View.SetNeedsLayout();
//			scrollView.LayoutIfNeeded ();
//			scrollView.ContentSize = new CGSize(scrollView.Frame.Width*3, scrollView.Frame.Height);
			scrollView.ContentSize = new CGSize (scrollView.Frame.Width * 3, scrollView.Frame.Height - 200f); // Not allow vertical scrolling
			scrollView.PagingEnabled = true;
			scrollView.ShowsVerticalScrollIndicator = false;
			scrollView.ShowsHorizontalScrollIndicator = false;
			scrollView.ContentOffset = new CGPoint (scrollView.Frame.Width, 0);
			scrollView.ScrollEnabled = true;
			scrollView.Bounces = false;

			scrollView.ScrollAnimationEnded += ScrollEnded;
			scrollView.DecelerationEnded += ScrollEnded;
			scrollView.Scrolled += OnScrollStarted; 

			btnNext.TouchDown += (s, e) => SlideToLeft ();
			btnPrevious.TouchDown += (s, e) => SlideToRight ();

			lblMonth.Font = FontHub.GetFont(FontHub.HelveticaLt, 15f); // FontHub.HelveticaMedium15;
		}

		public override void ReloadView ()
		{
			base.ReloadView ();
//			if(ViewModel != null && ViewModel.CurrentMonth.MonthDiff (DateTime.Today) != 0){
//				// Reset view
				
//			}
		}

		private void CreateBindingsAsync ()
		{

			RxApp.TaskpoolScheduler.Schedule(()=>{
				InitBindings ();
			});
		}


		public void InitBindings ()
		{
//			this.BindCommand (ViewModel, vm => vm.BackCommand, v => v.btnBack);
//			headerView.BackClicked = (s, e) => ViewModel.BackCommand.Execute (null);
//			this.OneWayBind (ViewModel, vm => vm.MonthTitle, v => v.lblMonth.Text);
//			System.Threading.Thread.Sleep(5000);
			ViewModel.WhenAnyValue (vm => vm.MonthTitle)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (b => lblMonth.Text = b);

			ViewModel.WhenAnyValue (vm => vm.EnableNextButton)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (b => SetButtonEnable (btnNext, b));

			ViewModel.WhenAnyValue (vm => vm.EnablePreviousButton)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (b => SetButtonEnable (btnPrevious, b));
		}

		private void SetButtonEnable (UIButton btn, bool enable)
		{
			btn.Enabled = enable;
			btn.Layer.Opacity = enable ? 1.0f : 0.5f;
		}

		private void OnScrollStarted (object s, EventArgs e)
		{
			var enableScroll = DateTime.Today.MonthDiff (ViewModel.CurrentMonth) > 0; // Don't use EnableNextButton prop here
			var offset = scrollView.ContentOffset;
			if (!enableScroll && offset.X > scrollView.Frame.Width) {
				offset.X = scrollView.Frame.Width;
				scrollView.SetContentOffset (offset, false);
				ViewModel.Scrolling = false;
			} else {
				ViewModel.Scrolling = true;
			}
		}

		public void SlideToLeft ()
		{
			// TODO: review
			DateTime tripMonth = ViewModel.CurrentMonth;
			if (tripMonth.Year != DateTime.UtcNow.Year || tripMonth.Month != DateTime.UtcNow.Month) {
				CGPoint offset = scrollView.ContentOffset;
				offset.X += scrollView.Frame.Width;

				if (offset.X <= scrollView.ContentSize.Width) {
					scrollView.SetContentOffset (offset, true);
				}
			}
		}

		public void SlideToRight ()
		{
			CGPoint offset = scrollView.ContentOffset;
			offset.X = 0;
			scrollView.SetContentOffset (offset, true);
		}

		private void ScrollEnded (object sender, EventArgs e)
		{
			var width = scrollView.Frame.Width;
			int changedMonth = 0;
			if (scrollView.ContentOffset.X == 0) {
				changedMonth = -1;

				//TODO: remove lastView by maxX not by  2*width
				UIView lastView = GetLastView ();

				foreach (UIView view in scrollView.Subviews) {
					
					var frame = view.Frame;
//					if (view.Frame.X == 2 * width) {
//					if (view.Frame.X == maxX) {
					if(view == lastView){
						view.RemoveFromSuperview();
					} else {
						view.Frame = new CGRect (frame.X + width, frame.Y, frame.Width, frame.Height);
					}
				}
			} else if (scrollView.ContentOffset.X == 2 * width) {
				changedMonth = 1;

				//TODO: remove lastView by minX not by  frame.X = 0
				UIView firstView = GetFirstView ();

				foreach (UIView view in scrollView.Subviews) {
					var frame = view.Frame;
//					if (view.Frame.X == 0) {
//					if (view.Frame.X == minX) {
					if (view == firstView) {
						//TODO remove fistview.
						view.RemoveFromSuperview();
					} else {
						view.Frame = new CGRect (frame.X - width, frame.Y, frame.Width, frame.Height);
					}
				}
			}
			//
			if (changedMonth != 0) {
				ViewModel.CurrentMonth = ViewModel.CurrentMonth.AddMonths (changedMonth);
				// Replace view
				// TODO: Skip month in future
				var offsetX = changedMonth == -1 ? 0 : 2 * width;
				var v = scrollView.Subviews.FirstOrDefault (x => x.Frame.X == offsetX);
				// Case 2: Replace with new view
				CGRect frame;
					frame = new CGRect (offsetX, 0, scrollView.Frame.Width, scrollView.Frame.Height);				
				//
				var vm = ViewModel.GetMonthlyViewModel (ViewModel.CurrentMonth.AddMonths (changedMonth));
				var newView = new ListTripContentView (frame, vm);
				scrollView.AddSubview (newView);
				//
				scrollView.ContentOffset = new CGPoint (width, scrollView.ContentOffset.Y);

				// Case 1: Change viewmodel only
				// Cannot remove binding which associated with previous ViewModel
//				if (v == null) {
//					var frame = new CGRect (offsetX, 0, scrollView.Frame.Width, scrollView.Frame.Height);	
//					v = new ListTripContentView (frame);
//					scrollView.AddSubview (v);
//				}
//				//
//				var vm = ViewModel.GetMonthlyViewModel (ViewModel.CurrentMonth.AddMonths (changedMonth));
//				(v as ListTripContentView).SetViewModelDelay (vm);
//				//
//				scrollView.ContentOffset = new CGPoint (width, scrollView.ContentOffset.Y);
			}
			//
			ViewModel.Scrolling = false;
		}

		private UIView GetFirstView()
		{
			var a = scrollView.Subviews.Min (x => x.Frame.X);
			return scrollView.Subviews.FirstOrDefault (x => x.Frame.X == a);
		}

		private UIView GetLastView()
		{
			var a = scrollView.Subviews.Max (x => x.Frame.X);
			return scrollView.Subviews.FirstOrDefault (x => x.Frame.X == a);
		}

		public override void BeforeBack(){
			if(ViewModel !=null){
				ViewModel.BackCommand.Execute (null);
			}
		}
		private async void AddSubContentViewsDelay(){
			System.Threading.Tasks.Task.Delay(1);
			DateTime d = ViewModel.CurrentMonth;
			var vm1 = ViewModel.GetMonthlyViewModel (d.AddMonths (-1));
			var vm2 = ViewModel.GetMonthlyViewModel (d);

			// view 2 create first (center view)
			View.SetNeedsLayout();
			View.LayoutIfNeeded ();

			var width = scrollView.Frame.Width;
			scrollView.ContentOffset = new CGPoint (width, scrollView.ContentOffset.Y);

			var view2 = new ListTripContentView (new CGRect (scrollView.Frame.Width, 0, scrollView.Frame.Width, scrollView.Frame.Height), vm2);
			var view1 = new ListTripContentView (new CGRect (0, 0, scrollView.Frame.Width, scrollView.Frame.Height), vm1);
			scrollView.AddSubview (view1);
			scrollView.AddSubview (view2);
			var vm3 = ViewModel.GetMonthlyViewModel (d.AddMonths (1));
			var view3 = new ListTripContentView (new CGRect (scrollView.Frame.Width * 2, 0, scrollView.Frame.Width, scrollView.Frame.Height ),vm3);
			scrollView.AddSubview (view3);

			//TODO: reset state of scroll is end sroll.
			ViewModel.Scrolling = false;
		}

	}
}
