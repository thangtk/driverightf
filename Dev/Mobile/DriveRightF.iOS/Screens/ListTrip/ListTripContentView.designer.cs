// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("ListTripContentView")]
	partial class ListTripContentView
	{
		[Outlet]
		UIKit.UILabel lblDateTime { get; set; }

		[Outlet]
		UIKit.UILabel lblDistance { get; set; }

		[Outlet]
		UIKit.UILabel lblDuration { get; set; }

		[Outlet]
		UIKit.UILabel lblScore { get; set; }

		[Outlet]
		UIKit.UITableView tblListTrip { get; set; }

		[Outlet]
		UIKit.UIView tripsView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (lblDateTime != null) {
				lblDateTime.Dispose ();
				lblDateTime = null;
			}

			if (lblDistance != null) {
				lblDistance.Dispose ();
				lblDistance = null;
			}

			if (lblDuration != null) {
				lblDuration.Dispose ();
				lblDuration = null;
			}

			if (lblScore != null) {
				lblScore.Dispose ();
				lblScore = null;
			}

			if (tblListTrip != null) {
				tblListTrip.Dispose ();
				tblListTrip = null;
			}

			if (tripsView != null) {
				tripsView.Dispose ();
				tripsView = null;
			}
		}
	}
}
