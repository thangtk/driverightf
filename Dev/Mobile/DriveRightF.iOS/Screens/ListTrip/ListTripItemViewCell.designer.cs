// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("ListTripItemViewCell")]
	partial class ListTripItemViewCell
	{
		[Outlet]
		UIKit.UILabel lblDistance { get; set; }

		[Outlet]
		UIKit.UILabel lblDuration { get; set; }

		[Outlet]
		UIKit.UILabel lblNoData { get; set; }

		[Outlet]
		UIKit.UILabel lblNoTrip { get; set; }

		[Outlet]
		UIKit.UILabel lblScore { get; set; }

		[Outlet]
		UIKit.UILabel lblStartDay { get; set; }

		[Outlet]
		UIKit.UILabel lblStartTime { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (lblDistance != null) {
				lblDistance.Dispose ();
				lblDistance = null;
			}

			if (lblDuration != null) {
				lblDuration.Dispose ();
				lblDuration = null;
			}

			if (lblNoData != null) {
				lblNoData.Dispose ();
				lblNoData = null;
			}

			if (lblScore != null) {
				lblScore.Dispose ();
				lblScore = null;
			}

			if (lblStartDay != null) {
				lblStartDay.Dispose ();
				lblStartDay = null;
			}

			if (lblStartTime != null) {
				lblStartTime.Dispose ();
				lblStartTime = null;
			}

			if (lblNoTrip != null) {
				lblNoTrip.Dispose ();
				lblNoTrip = null;
			}
		}
	}
}
