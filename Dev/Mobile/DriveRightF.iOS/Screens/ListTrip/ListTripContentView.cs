﻿
using System;

using Foundation;
using UIKit;
using Unicorn.Core.UI;
using DriveRightF.Core.ViewModels;
using CoreGraphics;
using ObjCRuntime;
using ReactiveUI;
using System.Reactive.Linq;
using System.Collections.Generic;
using DriveRightF.Core.Models;
using System.Reactive.Concurrency;
using Unicorn.Core;
using System.Threading.Tasks;
using System.Linq;

namespace DriveRightF.iOS
{
	//[Register ("ListTripContentView")]
	public partial class ListTripContentView : UBaseView<MonthlyTripModel>
	{
		private UITableView _tblListTrip;
		private readonly TripTableSource _tableSource = new TripTableSource ();
		private readonly UIRefreshControl _refreshControl = new UIRefreshControl ();
		private bool _addedRefreshControlEvent = false;
		private LoadingControlView _noDataView;
		private GlanceLoadingView _loadingView;
		private static int id = 0;

		public ListTripContentView (IntPtr handle) : base (handle)
		{
		}

		public ListTripContentView (CGRect frame) : base (frame)
		{
//			LoadNib ();
			InitControls ();
			id++;
		}

		public ListTripContentView (CGRect frame, MonthlyTripModel viewModel) : base (frame)
		{
//			LoadNib ();
			InitControls ();
			SetViewModelDelay (viewModel);
			id++;
		}

		public async void SetViewModelDelay (MonthlyTripModel viewModel)
		{
//			ResetView ();
			await System.Threading.Tasks.Task.Delay(1);
			ViewModel = viewModel;
			if (ViewModel != null) {
				InitBindings ();
				ViewModel.LoadData ();
			}
		}

//		private void LoadNib ()
//		{
//			var nibObjects = NSBundle.MainBundle.LoadNib ("ListTripContentView", this, null);
//			var v = Runtime.GetNSObject ((nibObjects.ValueAt (0))) as UIView;
//			AddSubview (v);
//		}
			
		private void InitControls ()
		{
//			lblDateTime.Font = FontHub.RobotoLight12;
//			lblDuration.Font = FontHub.RobotoLight12;
//			lblDistance.Font = FontHub.RobotoLight12;
//			lblScore.Font = FontHub.RobotoLight12;

//			this.BackgroundColor = UIColor.FromRGB (233, 233, 233);
			_refreshControl.TintColor = UIColor.FromRGB(255, 180, 0);
//			tblListTrip.Frame = new CGRect (tblListTrip.Frame.X, tblListTrip.Frame.Y, tblListTrip.Frame.Width, Frame.Height - 36);
			_tblListTrip = new UITableView(new CGRect(0,0, Frame.Width, Frame.Height));
			_tblListTrip.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			_tblListTrip.Source = _tableSource;
			_tblListTrip.ReloadData ();
			_tblListTrip.AddSubview (_refreshControl);
			_tblListTrip.BackgroundColor = UIColor.White; // UIColor.FromRGB (233, 233, 233);
			this.AddSubview (_tblListTrip);
			//
			this.SetNeedsLayout();
			this.LayoutIfNeeded ();
			AddNoDataView();
			AddLoadingView ();
			//
			DefaultContentVisibility();
		}

		private void DefaultContentVisibility(){
			_tblListTrip.Hidden = true;
			_noDataView.Hidden = true;
			_loadingView.Hidden = false;
		}

		private void AddNoDataView(){
			_noDataView = new LoadingControlView (new CGRect (0, 0, Frame.Width, Frame.Height));
			_noDataView.StartText = Translator.Translate("NO_TRIP_FOUND");
			this.AddSubview (_noDataView);
		}

		private void AddLoadingView(){
			_loadingView = new GlanceLoadingView (new CGRect (0, 0, Frame.Width, Frame.Height));
			_loadingView.LabelText = Translator.Translate("WAITING");
			_loadingView.StartAnimating();
			this.AddSubview (_loadingView);
		}

		public void InitBindings ()
		{
			if (ViewModel == null) {
				return;
			}
			if (!_addedRefreshControlEvent) {
				_refreshControl.ValueChanged +=	(o, e) => ViewModel.RequestNewData.Execute (true);
				_noDataView.ValueChanged += (o, e) => ViewModel.RequestNewData.Execute (true);
				//
				_addedRefreshControlEvent = true;
			}
			//
			//this.OneWayBind (ViewModel, vm => vm.ContentViewHidden, v => v._tblListTrip.Hidden);

			ViewModel.WhenAnyValue (vm => vm.ContentViewHidden)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (b => _tblListTrip.Hidden = b);
			
			ViewModel.WhenAnyValue (vm => vm.NoDataHidden)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (b => _noDataView.Hidden = b);
			ViewModel.WhenAnyValue (vm => vm.RequestingViewHidden).Subscribe (b => _loadingView.Hidden = b);
			//
			ViewModel.WhenAnyValue (vm => vm.Requesting)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (b => {
					if (!b) {
						_refreshControl.EndRefreshing ();
						_noDataView.EndRefreshing ();
					}
				});
			//
			ViewModel.WhenAnyValue (vm => vm.TripList)
				.Where (x => x != null && x.Count > 0)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (trips => UpdateUI (trips));
			//
			ViewModel.WhenAnyValue (vm => vm.NewData)
				.Where (x => x != null && x.Count > 0)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (trips => {
//					_tableSource.Items.InsertRange (0, trips);
//					tblListTrip.ReloadData ();
					UpdateUI(trips);
					ViewModel.MergeData();
				});
		}

		private async void UpdateUI(List<TripSummary> data)
		{
			data = data.Select(x=>x).ToList();

			if (data != null && data.Count > 0) {
				await Task.Factory.StartNew (() => {
					List<TripSummary> newItems = new List<TripSummary> ();
					for (int i = 0; i < data.Count; i++) {
//					Console.WriteLine(">>>>> i: " + i);
						var existItem = _tableSource.Items.Find (s => s.TripId == data [i].TripId);
						if (existItem == null) {
							newItems.Add (data [i]);
						} else {
							int x = _tableSource.Items.IndexOf (existItem);
//						Console.WriteLine(">>>>> x: " + x);
							_tableSource.Items [x] = data [i];
						}
					}
					_tableSource.Items.InsertRange (0, newItems);
				});

//			NSIndexPath[] index = CreateIndexPaths (newItems.Count);
//			tblListTrip.InsertRows (index, UITableViewRowAnimation.None);
				//
				_tblListTrip.ReloadData ();
			} else {
				return;
			}
		}

		/// <summary>
		/// For test performance
		/// </summary>
		/// <param name="data">Data.</param>
		private void InsertRows(List<TripSummary> data)
		{
			if(data == null || data.Count == 0){
				return;
			}
			// Case 1: slow lvl 2: 2 - 75
//			Debugger.StartTracking();
//			this.EndEditing (true);
//			tblListTrip.BeginUpdates ();
//			_tableSource.Items.InsertRange (0, data);
//
//			NSIndexPath[] index = CreateIndexPaths (data.Count);
//			tblListTrip.InsertRows (index, UITableViewRowAnimation.Left);
//			tblListTrip.EndUpdates ();
//			Debugger.ShowCost ("Append data");

			// Case 2: slow lvl 3: 90
//			Debugger.StartTracking();
//			_tableSource.Items.InsertRange (0, data);
//
//			NSIndexPath[] index = CreateIndexPaths (data.Count);
//			tblListTrip.InsertRows (index, UITableViewRowAnimation.Left);
//			tblListTrip.ReloadRows(tblListTrip.IndexPathsForVisibleRows, UITableViewRowAnimation.Fade);
			//			Debugger.ShowCost ("Append data");

			// Case 3: very fast : 0.4
//			Debugger.StartTracking();
//			_tableSource.Items.InsertRange (0, data);
//			tblListTrip.ReloadData ();
//			Debugger.ShowCost (ViewModel.Name + " >>> Append data");
		}

		private NSIndexPath[] CreateIndexPaths(int count)
		{
			NSIndexPath[] x = new NSIndexPath[count];
			for (int i = 0; i < count; i++) {
				x[i] = NSIndexPath.FromRowSection (i, 0);
			}
			return x;
		}

		/// <summary>
		/// Use when Change ViewModel - but not now
		/// </summary>
		private void ResetView(){
			// Clear table data
			_tableSource.Items.Clear();
			_tblListTrip.ReloadData ();
			DefaultContentVisibility ();
		}
	}
}

