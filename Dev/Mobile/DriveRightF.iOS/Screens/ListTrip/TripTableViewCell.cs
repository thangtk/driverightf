﻿
using System;

using Foundation;
using UIKit;
using Unicorn.Core.UI;
using DriveRightF.Core.ViewModels;
using DriveRightF.Core.Models;
using Unicorn.Core;
using DriveRightF.Core.Enums;

namespace DriveRightF.iOS
{
	public partial class TripTableViewCell : UBaseTableViewCell<TripItemViewModel>
	{
		public static readonly UINib Nib = UINib.FromName ("TripTableViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("TripTableViewCell");

		public TripTableViewCell (IntPtr handle) : base (handle)
		{
		}

		private void Initialize ()
		{
			InitControls ();
			InitBindings ();
		}

		public static TripTableViewCell Create ()
		{
			var c = (TripTableViewCell)Nib.Instantiate (null, null) [0];
			c.Initialize ();
			return c;
		}

		private void InitBindings ()
		{
			ViewModel = new TripItemViewModel ();

			AddGestureRecognizer (new UITapGestureRecognizer (() => ViewModel.ItemClickCommand.Execute (new Action (delegate {

				DependencyService.Get<IHomeTabNavigator> ().CurrentTab.Navigate ((int)Screen.TripDetail, new object[] {
					ViewModel.Item
				});
			}))));
		}

		private void InitControls ()
		{
//			scoreView.Layer.CornerRadius = scoreView.Frame.Width/2;
			scoreView.TextColor = UIColor.Black;
			scoreView.LabelTextFont = FontHub.GetFont(FontHub.HelveticaHvCn, 16f);
		}

		public void UpdateCell (TripSummary item, int index)
		{
			ViewModel.Item = item;

			UpdateUI ();
		}

		private void UpdateUI ()
		{
			lblStartTime.Text = ViewModel.StartTime;
			lblDuration.Text = ViewModel.Duration;
			lblDistance.Text = ViewModel.Distance;
//			lblScore.Text = ViewModel.Score;
//			var c = ViewModel.ScoreColor;
//			scoreView.BackgroundColor = UIColor.FromRGB (c.R, c.G, c.B);
			scoreView.SetValue((int)ViewModel.Item.Score, true);
		}

	}
}

