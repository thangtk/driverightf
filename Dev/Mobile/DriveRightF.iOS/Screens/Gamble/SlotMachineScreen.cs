﻿
using System;

using System.Collections.Generic;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using AudioToolbox;
using AVFoundation;
using CoreGraphics;
using DriveRightF.Core;
using DriveRightF.Core.Enums;
using DriveRightF.Core.ViewModels;
using Foundation;
using ReactiveUI;
using UIKit;
using Unicorn.Core;


namespace DriveRightF.iOS
{
	public partial class SlotMachineScreen : DriveRightBaseViewController<SlotMachineViewModel>
	{
		private Award _award;
		private bool _isFinish = false;
		List<bool> _spinInProress;
		List<UIImage> _imageList;
		List<UIImageView> columnList;
		int _countIndex;
		int _currentColumn;
		int _columnCount = 0;
		UILabel _layerView;
		AVAudioPlayer _player = AVAudioPlayer.FromUrl (NSUrl.FromFilename ("shiny-ding.mp3"));
		nfloat _width = UIScreen.MainScreen.Bounds.Width;
		nfloat _height = UIScreen.MainScreen.Bounds.Height;
		List<UIImageView> _firstColumnSubviews;
		List<UIImageView> _secondColumnSubviews;
		List<UIImageView> _thirdColumnSubviews;
		List<int> _itemCountList;
		List<CGRect> _backupFrame;
		List<System.Timers.Timer> _timerList;


		public int Duration {
			get { return _countIndex / 100; }
			set { _countIndex = value * 100; }
		}

		public nfloat MaxScaleRatio {
			get;
			set;
		}

		public nfloat MinScaleRatio {
			get;
			set;
		}

		public SlotMachineScreen () : base ("SlotMachineScreen", null)
		{
			Initialize ();
		}

		private void Initialize ()
		{
			InitParams ();
			ViewModel = new SlotMachineViewModel ();
			CreateBindingsAsync ();
			InitView ();
		}

		private void InitParams ()
		{
			Duration = 6;
			MaxScaleRatio = 1f;
			MinScaleRatio = 0f;
		}

		private void CreateBindingsAsync ()
		{
			RxApp.TaskpoolScheduler.Schedule(()=>{
				InitBindings ();
			});
		}

		private void InitBindings ()
		{
			
//			this.Bind (ViewModel, vm => vm.Sponsor, v => v.lblSponsorBy.Text);
//			this.Bind (ViewModel, vm => vm.Expiry, v => v.lblExpiresIn.Text);

			ViewModel.WhenAnyValue (vm => vm.Sponsor).Where (x => x != null).ObserveOn (RxApp.MainThreadScheduler).Subscribe (x => lblSponsorBy.Text = x.ToString());
			ViewModel.WhenAnyValue (vm => vm.Expiry).Where (x => x != null).ObserveOn (RxApp.MainThreadScheduler).Subscribe (x => lblExpiresIn.Text = x.ToString());
		}

		private void InitView ()
		{
		}

		public override void LoadData (params object[] objs)
		{
			base.LoadData (objs);

			if (objs != null && objs.Length > 0)
			{
				_award = objs [0] as Award;
				if (ViewModel != null) ViewModel.SetAwardData (_award);
				_isFinish = false;
			}
		}

		public override void ReloadView ()
		{
			base.ReloadView ();

			//vContainer.Superview.Transform = CGAffineTransform.MakeIdentity ();

			if (!_isFinish) {
				vContainer.Hidden = false;

				_layerView.Text = Translator.Translate ("SPIN_THREE_COLUMN");
				_layerView.TextColor = ColorHub.ColorFromHex ("ff7316");
				View.AddSubview (_layerView);
				_itemCountList = new List<int> (6) { 0, 0, 0, 0, 0, 0 };
				_spinInProress = new List<bool> (3) { false, false, false };
				_columnCount = 0;

		
				ViewModel.LoadDataCommand.Execute (null);
			}
		}


		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
			
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

//			SetFont ();

			_itemCountList = new List<int> (6) { 0, 0, 0, 0, 0, 0 };
			_timerList = new List<System.Timers.Timer>(){ new System.Timers.Timer(), new System.Timers.Timer(), new System.Timers.Timer()};


			//init column list
			columnList = new List<UIImageView> () {
				vColumn1,
				vColumn2,
				vColumn3
			};

			//init image for columns in slot machine
//			_imageList = new List<UIImage> () {
//				UIImage.FromFile ("orange.png"),
//				UIImage.FromFile ("watermelon.png"),
//				UIImage.FromFile ("seven.png"),
//				UIImage.FromFile ("bar.png"),
//				UIImage.FromFile ("cherry.png"),
//				UIImage.FromFile ("bell.png"),
//			};

			_imageList = new List<UIImage> () {
				UIImage.FromFile ("bar.png"),
				UIImage.FromFile ("seven.png"),
				UIImage.FromFile ("watermelon.png"),
				UIImage.FromFile ("orange.png"),
				UIImage.FromFile ("cherry.png"),
				UIImage.FromFile ("banana.png"),
			};

			//set back ground image for each column
			var image = UIImage.FromFile ("imagegloss.png");
			vColumn1.Image = image;
			vColumn2.Image = image;
			vColumn3.Image = image;
			vColumn1.ClipsToBounds = true;
			vColumn2.ClipsToBounds = true;
			vColumn3.ClipsToBounds = true;

			//creat lists
			_firstColumnSubviews = new List<UIImageView> () {
				img14,
				img11,
				img12,
				img13,
			};

			_secondColumnSubviews = new List<UIImageView> () {
				img24,
				img21,
				img22,
				img23,
			};

			_thirdColumnSubviews = new List<UIImageView> () {
				img34,
				img31,
				img32,
				img33,
			};

			_backupFrame = new List<CGRect> (){ 
				img14.Frame,
				img11.Frame,
				img12.Frame,
				img13.Frame,
			};

			//set image for each element of columns
			for (int i = 0; i < _firstColumnSubviews.Count; i++) {
				_firstColumnSubviews [i].Image = _imageList [3-i];
				_secondColumnSubviews [i].Image = _imageList [3-i];
				_thirdColumnSubviews [i].Image = _imageList [3-i];
			}

			//set first scale
			for (int i = 1; i < _firstColumnSubviews.Count; i++) {
				var scaleRatio = (nfloat)(MaxScaleRatio - MinScaleRatio * (Math.Abs (_firstColumnSubviews [i].Center.Y - vColumn1.Frame.Height / 2f) / (vColumn1.Frame.Height / 2f)));
				_firstColumnSubviews [i].Transform = CGAffineTransform.MakeScale (scaleRatio, scaleRatio);
				scaleRatio = (nfloat)(MaxScaleRatio - MinScaleRatio * (Math.Abs (_secondColumnSubviews [i].Center.Y - vColumn2.Frame.Height / 2f) / (vColumn2.Frame.Height / 2f)));
				_secondColumnSubviews [i].Transform = CGAffineTransform.MakeScale (scaleRatio, scaleRatio);
				scaleRatio = (nfloat)(MaxScaleRatio - MinScaleRatio * (Math.Abs (_thirdColumnSubviews [i].Center.Y - vColumn3.Frame.Height / 2f) / (vColumn3.Frame.Height / 2f)));
				_thirdColumnSubviews [i].Transform = CGAffineTransform.MakeScale (scaleRatio, scaleRatio);
			}

			_spinInProress = new List<bool> (3) { false, false, false };

			//setup swipe recognizers
			View.AddGestureRecognizer (new UIPanGestureRecognizer (OnPanGestureRecognizer));

			//Init Layer View
			InitLayerView ();
			InitTranslator ();
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);

			foreach (var timer in _timerList) {
				if (timer != null)
					timer.Stop ();
			}

			//set image for each element of columns
			for (int i = 0; i < _firstColumnSubviews.Count; i++) {
				_firstColumnSubviews [i].Frame = _backupFrame [i];
				_secondColumnSubviews [i].Frame = _backupFrame [i];
				_thirdColumnSubviews [i].Frame = _backupFrame [i];

				_firstColumnSubviews [i].Image = _imageList [3-i];
				_secondColumnSubviews [i].Image = _imageList [3-i];
				_thirdColumnSubviews [i].Image = _imageList [3-i];
			}

			for (int i = 1; i < _firstColumnSubviews.Count; i++) {
				var scaleRatio = (nfloat)(MaxScaleRatio - MinScaleRatio * (Math.Abs (_firstColumnSubviews [i].Center.Y - vColumn1.Frame.Height / 2f) / (vColumn1.Frame.Height / 2f)));
				_firstColumnSubviews [i].Transform = CGAffineTransform.MakeScale (scaleRatio, scaleRatio);
				scaleRatio = (nfloat)(MaxScaleRatio - MinScaleRatio * (Math.Abs (_secondColumnSubviews [i].Center.Y - vColumn2.Frame.Height / 2f) / (vColumn2.Frame.Height / 2f)));
				_secondColumnSubviews [i].Transform = CGAffineTransform.MakeScale (scaleRatio, scaleRatio);
				scaleRatio = (nfloat)(MaxScaleRatio - MinScaleRatio * (Math.Abs (_thirdColumnSubviews [i].Center.Y - vColumn3.Frame.Height / 2f) / (vColumn3.Frame.Height / 2f)));
				_thirdColumnSubviews [i].Transform = CGAffineTransform.MakeScale (scaleRatio, scaleRatio);
			}
		}

		private void InitTranslator ()
		{
			lblSponsor.Text = Translator.Translate("CARD_DETAILS_SPONSOR_BY");
			lblExpire.Text = Translator.Translate("CARD_DETAILS_EXPIRES_IN");
		}

		private void SetFont()
		{
			lblSponsor.Font = FontHub.HelveticaHvCn18;
			lblExpire.Font = FontHub.HelveticaHvCn18;
			lblSponsorBy.Font = FontHub.HelveticaCn14;
			lblExpiresIn.Font = FontHub.HelveticaCn14;
		}

		private void InitLayerView ()
		{
			_layerView = new UILabel (vContainer.Frame);
			_layerView.Text = Translator.Translate ("SPIN_THREE_COLUMN");
			_layerView.TextColor = UIColor.DarkTextColor.ColorWithAlpha(0.5f);
			_layerView.TextAlignment = UITextAlignment.Center;
			_layerView.Font = FontHub.HelveticaMedium22;
			View.AddSubview (_layerView);

			UIView.Animate (
				1, 
				0,
				UIViewAnimationOptions.Autoreverse | UIViewAnimationOptions.Repeat,
				() => {
					_layerView.Alpha = 0.7f;
					_layerView.Transform = CGAffineTransform.MakeScale (0.98f, 0.98f);
				},
				() => {
				});
		}

		private void StartAnimating (int indicator, int column, List<UIImageView> imageViewList)
		{
			UIView parentView = columnList [column];
			int imageIndicator = 4;
			int count = 0;

//			System.Timers.Timer timer = new System.Timers.Timer (10);
			_timerList[column] = new System.Timers.Timer (10);
			nfloat height = parentView.Frame.Height;
			nfloat offset = 10f;
			nfloat imageSize = imageViewList [0].Frame.Height;
//			var accelerate = (((imageSize + offset) * _imageList.Count * velocity + (indicator - 2) * 90f) - velocity * _countIndex) / (_countIndex * _countIndex);

			_timerList[column].Elapsed += (sender, e) => InvokeOnMainThread (() => {
				count++;
				if (count == _countIndex) {
					_timerList[column].Stop ();
					var min = imageViewList [0].Center.Y - height / 2f;
					for (int i = 1; i < imageViewList.Count; i++) {
						var temp = imageViewList [i].Center.Y - height / 2f;
						if (Math.Abs (temp) < Math.Abs (min))
							min = temp;
					}
					foreach (UIImageView view in imageViewList) {
						if (view.Center.Y - min > 250)
						{	
							UIView.Animate(0.5, 
								() => view.Alpha = 0,
								() => {
									view.Center = new CGPoint (view.Center.X, view.Center.Y - min);
									view.Alpha = 1;
								});
						}
						else
						{
							UIView.Animate (1, 
								() => {
//							if (view.Frame.Bottom > 0f && view.Frame.Bottom < height) {
//								var scaleRatio = (nfloat)(MaxScaleRatio - MinScaleRatio * (Math.Abs (view.Center.Y - height / 2f) / (height / 2f)));
//								view.Transform = CGAffineTransform.MakeScale (scaleRatio, scaleRatio);
//							}
									view.Center = new CGPoint (view.Center.X, view.Center.Y - min);
								}, () => {
								}
							);
						}
					}
					RxApp.TaskpoolScheduler.Schedule(_ => {
						Task.Delay(2000).Wait();
						_columnCount++;
						RxApp.MainThreadScheduler.Schedule (EndGamble);
					});
				}
				foreach (UIImageView view in imageViewList) {
					nfloat value = 25f;
					if (count > 378)
						value = (540f + (indicator - 3f) * 90f - 30f + (nfloat)(new Random (DateTime.Now.Millisecond).Next (30, 60))) / (_countIndex - 378f);
					else if (count > 216)
						value = 10f;
					//						value = accelerate * (2 * count - 1) + velocity;
					if (view.Frame.Bottom > 0f && view.Frame.Bottom < height) {
						var scaleRatio = (nfloat)(MaxScaleRatio - MinScaleRatio * (Math.Abs (view.Center.Y + value - height / 2f) / (height / 2f)));
						view.Transform = CGAffineTransform.MakeScale (scaleRatio, scaleRatio);
					}
					view.Center = new CGPoint (view.Center.X, view.Center.Y + value);
					if (view.Center.Y >= height + imageSize / 2f + offset) {
						imageIndicator++;
						view.Image = _imageList [(imageIndicator) % 6];
						view.Center = new CGPoint (view.Center.X, -90 + imageSize / 2f + (view.Center.Y - (height + imageSize / 2f + offset)));
					}
				}
			});
			_timerList[column].Start ();
		}

		private void OnPanGestureRecognizer (UIPanGestureRecognizer recognizer)
		{
			if (recognizer.State == UIGestureRecognizerState.Began) {
				if (recognizer.LocationInView (View).X < _width / 3f)
					_currentColumn = 0;
				else if (recognizer.LocationInView (View).X < _width * 2f / 3f)
					_currentColumn = 1;
				else
					_currentColumn = 2;
				DisableButtonBack ();
//				DisableTabBar ();
			} else if (recognizer.State == UIGestureRecognizerState.Ended && recognizer.VelocityInView (View).Y > 0) {


				_layerView.RemoveFromSuperview ();
				int indicator = 1;
				switch (_currentColumn) {
				case 0:
					if (_spinInProress [_currentColumn] == false) {
						_spinInProress [_currentColumn] = true;
						indicator = new Random ().Next (1, 7);
//						indicator = 1;
						StartAnimating (indicator, _currentColumn, _firstColumnSubviews);
					}
					break;
				case 1:
					if (_spinInProress [_currentColumn] == false) {
						_spinInProress [_currentColumn] = true;
						indicator = new Random ().Next (1, 7);
//						indicator = 2;
						StartAnimating (indicator, _currentColumn, _secondColumnSubviews);
					}
					break;
				case 2:
					if (_spinInProress [_currentColumn] == false) {
						_spinInProress [_currentColumn] = true;
						indicator = new Random ().Next (1, 7);
//						indicator = 3;
						StartAnimating (indicator, _currentColumn, _thirdColumnSubviews);
					}
					break;
				}
				_itemCountList [indicator - 1]++;
			}
		}

		private void EndGamble ()
		{
			if (_columnCount == 3 && DependencyService.Get<IHomeTabNavigator> ().CurrentTab.CurrentScreen == (int)Screen.Gamble) {
				float amount = 1;
				lblNotify.Text = string.Format ("{0} {1} {2}", Translator.Translate ("YOU_WIN"), "1", Translator.Translate("RMB"));	
				foreach (var item in _itemCountList) {
					if (item == 3) {
						if (_itemCountList.IndexOf (item) == 1) {
							lblNotify.Text = string.Format ("{0} {1} {2}", Translator.Translate ("YOU_WIN"), "999", Translator.Translate("RMB"));
							amount = 999;
						} else {
							lblNotify.Text = string.Format ("{0} {1} {2}", Translator.Translate ("YOU_WIN"), "99", Translator.Translate("RMB"));
							amount = 99;
						}
						break;
					} else if (item == 2) {
						lblNotify.Text = string.Format ("{0} {1} {2}", Translator.Translate ("YOU_WIN"), "9", Translator.Translate("RMB"));
						amount = 9;
						break;
					} 
				}

				ViewModel.UpdateAwardCommand.Execute (_award);
				ViewModel.UpdateAccountCommand.Execute (amount);
			
//				//TODO: sendmessage to remove cardaward!
//				MessagingCenter.Send<object>(this,"Gamble");

				//TODO : block ui and animation;

//				ViewModel.IsBlockUI = true;

				// Disable coins fly animation for new UI
//				DependencyService.Get<ICoinView>().AnimateCoins(5, () =>
//					RxApp.MainThreadScheduler.Schedule (() => {
//					ViewModel.IsBlockUI = false;
//
//					Sleep();
//				}));

				vContainer.ClipsToBounds = true;

//				UIView.Animate (
//					0.5, 
//					2,
//					UIViewAnimationOptions.TransitionNone,
//					() => {
//						var frame = vContainer.Superview.Frame;
//						frame.Height = 0f;
//						vContainer.Frame = frame;
//
//						vContainer.Superview.Transform = CGAffineTransform.MakeScale(0.0001f, 0.0001f);
//
//
////						foreach(var view in columnList)
////						{
////							var frame = view.Frame;
////							frame.Height = 0f;
////							view.Frame = frame;		
////
////							view.Transform = CGAffineTransform.MakeScale(1f, 0.0001f);
////						}
//					},
//					() => {
//					});
				vContainer.Hidden = true;
				_columnCount = 0;
				_isFinish = true;
				EnableButtonBack ();
//				EnableTabBar ();
				ViewModel.NavigateToHomeCommand.Execute (true);
			}
		}

		private async void Sleep()
		{
			await Task.Delay (4000);

			if (_layerView != null)
			{
				_layerView.RemoveFromSuperview ();
			}

//			DependencyService.Get<IHomeTabNavigator> ().CurrentTab.Navigate ((int)Screen.HomeAward);
		}

		private void DisableButtonBack()
		{
			ViewModel.IsEnableButtonBack = false;
		}

		private void EnableButtonBack()
		{
			ViewModel.IsEnableButtonBack = true;
		}

		private void DisableTabBar()
		{
			ViewModel.IsBlockTabBar = false;
		}

		private void EnableTabBar()
		{
			ViewModel.IsBlockTabBar = true;
		}
	}
}

