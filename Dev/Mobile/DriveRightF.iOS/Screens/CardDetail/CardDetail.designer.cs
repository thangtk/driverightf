// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("CardDetail")]
	partial class CardDetail
	{
		[Outlet]
		UIKit.UIButton btnGamble { get; set; }

		[Outlet]
		UIKit.UIButton btnTakeMoney { get; set; }

		[Outlet]
		UIKit.UILabel lbl999 { get; set; }

		[Outlet]
		UIKit.UILabel lblDateExpires { get; set; }

		[Outlet]
		UIKit.UILabel lblDoYouWant { get; set; }

		[Outlet]
		UIKit.UILabel lblExpires { get; set; }

		[Outlet]
		UIKit.UILabel lblForAChance { get; set; }

		[Outlet]
		UIKit.UIButton lblGameble { get; set; }

		[Outlet]
		UIKit.UILabel lblGolden { get; set; }

		[Outlet]
		UIKit.UILabel lblRMB { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnGamble != null) {
				btnGamble.Dispose ();
				btnGamble = null;
			}

			if (btnTakeMoney != null) {
				btnTakeMoney.Dispose ();
				btnTakeMoney = null;
			}

			if (lbl999 != null) {
				lbl999.Dispose ();
				lbl999 = null;
			}

			if (lblDateExpires != null) {
				lblDateExpires.Dispose ();
				lblDateExpires = null;
			}

			if (lblDoYouWant != null) {
				lblDoYouWant.Dispose ();
				lblDoYouWant = null;
			}

			if (lblExpires != null) {
				lblExpires.Dispose ();
				lblExpires = null;
			}

			if (lblForAChance != null) {
				lblForAChance.Dispose ();
				lblForAChance = null;
			}

			if (lblGameble != null) {
				lblGameble.Dispose ();
				lblGameble = null;
			}

			if (lblGolden != null) {
				lblGolden.Dispose ();
				lblGolden = null;
			}

			if (lblRMB != null) {
				lblRMB.Dispose ();
				lblRMB = null;
			}
		}
	}
}
