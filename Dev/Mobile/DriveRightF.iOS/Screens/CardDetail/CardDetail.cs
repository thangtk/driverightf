﻿
using System;
using System.Drawing;

using AVFoundation;
using CoreGraphics;
using DriveRightF.Core;
using DriveRightF.Core.Enums;
using DriveRightF.Core.Managers;
using DriveRightF.Core.ViewModels;
using Foundation;
using ReactiveUI;
using UIKit;
using Unicorn.Core;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

namespace DriveRightF.iOS
{
	public partial class CardDetail : BaseHandlerHeaderViewController<CardDetailViewModel>
	{
//		nfloat _width = UIScreen.MainScreen.Bounds.Width;
//		nfloat _height = UIScreen.MainScreen.Bounds.Height;
//		AVAudioPlayer _player = AVAudioPlayer.FromUrl (NSUrl.FromFilename ("coinsflying.mp3"));
		private Award _award;

		public CardDetail () 
		{
		}


		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
		
//			SetFont ();
			ViewModel = new CardDetailViewModel ();
			InitView ();
			InitBindings ();
			InitBindCommands ();
			InitTranslator ();
		}

		public override void LoadData(params object[] objs)
		{
			base.LoadData (objs);

			if (objs != null && objs.Length > 0)
			{
				_award = objs [0] as Award;
				if (ViewModel != null) ViewModel.Award = _award;
			}
		}

		private void CreateBindingsAsync ()
		{
			RxApp.TaskpoolScheduler.Schedule(()=>{
				InitBindings ();
			});
		}
		private void InitBindCommands ()
		{

			btnGamble.TouchUpInside += (object sender, EventArgs e) => ViewModel.BtnGambleClick.Execute (
				new Action(delegate{
					DependencyService.Get<IHomeTabNavigator>().CurrentTab.Navigate((int)Screen.Gamble, new object[] {_award});
				}));

			btnTakeMoney.TouchUpInside += (object sender, EventArgs e) => ViewModel.BtnTakeMoney.Execute(new Action(async () => {
				//TODO: need to move Viewmodel
				ViewModel.IsBlockUI = true;
//				DependencyService.Get<ICoinView> ().AnimateCoins (5, () => RxApp.MainThreadScheduler.Schedule (() => {
//					ViewModel.IsBlockUI = false;
//					Console.WriteLine ("Animation Finish");
//					DependencyService.Get<IHomeTabNavigator> ().CurrentTab.Navigate ((int)Screen.HomeAward);
//				}));
				await System.Threading.Tasks.Task.Delay(1000);
				ViewModel.IsBlockUI = false;
				Console.WriteLine ("Animation Finish");
				DependencyService.Get<IHomeTabNavigator> ().CurrentTab.Navigate ((int)Screen.HomeAward);

				_award.Used = true;
				DependencyService.Get<AwardManager> ().Save (_award);
			}));

			//			this.BindCommand (ViewModel, vm => vm.BtnGambleClick, v => v.btnGamble, () => new Action(delegate{
			//				DependencyService.Get<IHomeNavigator>().Navigate((int)Screen.Gamble, new object[] {_award});
			//			}));
			//
			//			this.BindCommand (ViewModel, vm => vm.BtnTakeMoney, v => v.btnTakeMoney, () => new Action(delegate{	
			//				//TODO: need to move Viewmodel
			//				ViewModel.IsBlockUI = true;
			//				DependencyService.Get<ICoinView>().AnimateCoins(5,() =>
			//				{
			//					RxApp.MainThreadScheduler.Schedule(() =>
			//					{
			//						ViewModel.IsBlockUI = false;
			//						Console.WriteLine("Animation Finish");
			//						DependencyService.Get<IHomeNavigator> ().Navigate ((int)Screen.HomeAward);
			//					});
			//				});
			//				_award.Used = true;
			//				DependencyService.Get<AwardManager> ().Save (_award);
			//			}));
		}

		private void InitView()
		{
		}

		private void InitBindings()
		{

			//			this.OneWayBind (ViewModel, vm => vm.SponsorBy, v => v.lblGolden.Text);
			ViewModel.WhenAnyValue (vm => vm.SponsorBy)
				.Where(x => x !=null)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => lblGolden.Text = x.ToString ());
			ViewModel.WhenAnyValue (vm => vm.ExpireDate)
				.Where(x => x !=null)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (date => lblDateExpires.Text = date.ToString ("dd/MM/yyyy"));

		}

		private void InitTranslator()
		{
			lblExpires.Text = Translator.Translate ("CARD_DETAILS_EXPIRES_IN");
			lblGolden.Text = Translator.Translate ("CARD_DETAILS_GOLDEN_LION");
			lblForAChance.Text = Translator.Translate ("CARD_DETAILS_CHANCE");
			lblRMB.Text = Translator.Translate ("RMB") + "?";

			lblDoYouWant.Text = Translator.Translate ("CARD_DETAILS_999");
			btnTakeMoney.SetTitle (Translator.Translate ("BUTTON_TAKE_MONEY"), UIControlState.Normal);
			btnGamble.SetTitle (Translator.Translate ("BUTTON_GAMBLE"), UIControlState.Normal);
		
		}

		private void SetFont()
		{
			lblExpires.Font = FontHub.HelveticaHvCn18;
			lblGolden.Font = FontHub.HelveticaCn14;
			lblDateExpires.Font = FontHub.HelveticaCn14;

			lblDoYouWant.Font = FontHub.HelveticaHvCn17;
			lblForAChance.Font = FontHub.HelveticaHvCn17;
			lblRMB.Font = FontHub.HelveticaHvCn17;
			lbl999.Font = FontHub.HelveticaHvCn28;

			btnTakeMoney.Font = FontHub.HelveticaLt15;
			btnGamble.Font = FontHub.HelveticaLt15;
		}
	}
}

