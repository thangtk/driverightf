﻿using System;
using DriveRightF.Core.Enums;
using DriveRightF.Core;

namespace DriveRightF.iOS
{
	public class AccountTabScreen: BaseTabItemScreen
	{
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.Navigate ((int)Screen.HomeAccount);
		}
		protected override void InitData(){
			base.InitData ();

		}
		public override void TabClick (){
			if (CurrentScreen != (int)Screen.HomeAccount) {
				this.NavigateBack ((int)Screen.HomeAccount,true);
			}
		}
	}
}

