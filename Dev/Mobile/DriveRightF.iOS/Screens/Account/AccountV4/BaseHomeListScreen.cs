﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using DriveRight.Core.ViewModels;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using Unicorn.Core.iOS;
using CoreGraphics;

namespace DriveRightF.iOS
{
	public abstract partial  class BaseHomeListScreen<TCellModel,TCellView> : DriveRightBaseViewController<BaseHomeListViewModel<TCellModel>>
		where TCellView:  UITableViewCell, IDriveRightTableViewCell<TCellModel>
	{

		public UIView ContentView
		{
			get {
				return vContent;
			}
		}

		public BannerView BannerView
		{
			get {
				return vBanner;
			}
		}

		public BaseHomeListScreen () : base ("BaseHomeListScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public abstract void CreatViewModel();

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			UpdateControls();
			CreatViewModel();
			vBanner.CreateViewModel (ViewModel.BannerViewModel);

			CreateViewContent();

		}

		public abstract void CreateViewContent();

		public virtual void UpdateControls()
		{
		}
	}
}

