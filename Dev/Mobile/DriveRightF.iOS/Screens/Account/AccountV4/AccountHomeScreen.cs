﻿using System;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using DriveRightF.Core.Models;
using Unicorn.Core;
using DriveRightF.Core.Enums;
using Unicorn.Core.iOS;

namespace DriveRightF.iOS
{
	public class AccountHomeScreen : BaseHomeListScreen<Account,ItemAccountViewCell>
	{
		public AccountHomeScreen () : base()
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();


		}

		#region implemented abstract members of BaseHomeListScreen

		public override void CreatViewModel ()
		{
			ViewModel = new AccountHomeViewModel ();
		}
			

		public override void CreateViewContent ()
		{
			AccountContentViewModel model = (ViewModel as AccountHomeViewModel).AccountContentViewModel;
			AccountContentView contentView = new AccountContentView (model);
			contentView.FixSupperView (ContentView);
		}

		#endregion

		public override void UpdateControls ()
		{
			BannerView.OnClick += (object sender, EventArgs e) => {
				DependencyService.Get<IHomeTabNavigator> ().CurrentTab.Navigate ((int)Screen.AccountDetail, new object[] {});
			};
		}
	}
}

