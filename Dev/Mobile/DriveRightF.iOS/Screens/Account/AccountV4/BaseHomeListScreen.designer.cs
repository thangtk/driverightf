// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("BaseHomeListScreen")]
	partial class BaseHomeListScreen<TCellModel,TCellView>
	{
		[Outlet]
		UIKit.UITableView tbView { get; set; }

		[Outlet]
		DriveRightF.iOS.BannerView vBanner { get; set; }

		[Outlet]
		UIKit.UIView vContent { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (tbView != null) {
				tbView.Dispose ();
				tbView = null;
			}

			if (vBanner != null) {
				vBanner.Dispose ();
				vBanner = null;
			}

			if (vContent != null) {
				vContent.Dispose ();
				vContent = null;
			}
		}
	}
}
