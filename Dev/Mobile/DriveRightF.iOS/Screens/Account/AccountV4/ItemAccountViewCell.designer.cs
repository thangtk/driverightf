// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("ItemAccountViewCell")]
	partial class ItemAccountViewCell
	{
		[Outlet]
		UIKit.UILabel lblMonthEarn { get; set; }

		[Outlet]
		UIKit.UILabel lblTitleCardEarn { get; set; }

		[Outlet]
		UIKit.UILabel lblTitleCashEarn { get; set; }

		[Outlet]
		UIKit.UILabel lblValueCardEarn { get; set; }

		[Outlet]
		UIKit.UILabel lblValueCashEarn { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (lblTitleCardEarn != null) {
				lblTitleCardEarn.Dispose ();
				lblTitleCardEarn = null;
			}

			if (lblTitleCashEarn != null) {
				lblTitleCashEarn.Dispose ();
				lblTitleCashEarn = null;
			}

			if (lblValueCardEarn != null) {
				lblValueCardEarn.Dispose ();
				lblValueCardEarn = null;
			}

			if (lblValueCashEarn != null) {
				lblValueCashEarn.Dispose ();
				lblValueCashEarn = null;
			}

			if (lblMonthEarn != null) {
				lblMonthEarn.Dispose ();
				lblMonthEarn = null;
			}
		}
	}
}
