﻿using System;
using DriveRightF.Core.Models;
using DriveRightF.Core;

namespace DriveRightF.iOS
{
	public class AccountContentView : HomeListContentView<Account, ItemAccountViewCell>
	{
		public AccountContentView (AccountContentViewModel model):base(model)
		{
		}

		public override void UpdateUI ()
		{
			TableView.RowHeight = 70;
			RefreshChanged += (object sender, EventArgs e) => 
			{
				if(ViewModel != null)
				{
					(ViewModel as AccountContentViewModel).ReloadBalance();
				}
			};
		}
	}
}

