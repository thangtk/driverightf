﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using DriveRightF.Core.Models;
using Unicorn.Core;
using DriveRightF.Core.Enums;
using System.Globalization;

namespace DriveRightF.iOS
{
	public partial class ItemAccountViewCell : DriveRightTableViewCell<Account,ItemAccountHomeViewModel> 
	{
		public static readonly UINib Nib = UINib.FromName ("ItemAccountViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("ItemAccountViewCell");

		public ItemAccountViewCell (IntPtr handle) : base (handle)
		{
		}

		public static ItemAccountViewCell Create ()
		{
			return (ItemAccountViewCell)Nib.Instantiate (null, null) [0];
		}

		#region implemented abstract members of DriveRightTableViewCell

		public override void UpdateCell (Account item, int index, int count)
		{
			//TODO don't need binding, load direct data to view.
			ViewModel.Account = item;
		}

		public override void Initialize ()
		{
			InitControls();
			ViewModel = new ItemAccountHomeViewModel ();
			CreateBindingsAsync ();

			AddGestureRecognizer (new UITapGestureRecognizer (() => {
				if( ViewModel.Month > DateTime.MinValue){
						//TODO: retrive month data from ViewModel
					DependencyService.Get<IHomeTabNavigator>().CurrentTab.Navigate((int)Screen.AccountStatement, new object[] {ViewModel.Month.ToString("yyyy-MM")});
				}
			}));
		}

		private void InitControls(){

//			lblDescription.Font = FontHub.HelveticaLt13;
//			lblUpdateTime.Font = FontHub.HelveticaThin11;
		}


		private void CreateBindingsAsync ()
		{
			RxApp.TaskpoolScheduler.Schedule(()=>{
				InitBindings ();
			});
		}

		private void InitBindings()
		{
//			ViewModel.WhenAnyValue (vm => vm.CardsEarnedValue)
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (x => {
//					lblValueCardEarn.Text = x;
//				});
//
//			ViewModel.WhenAnyValue (vm => vm.CashsEarnedValue)
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (x => {
//					lblValueCashEarn.Text = x;
//				});
//
			ViewModel.WhenAnyValue (vm => vm.CardsEarnTitle)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					lblTitleCardEarn.Text = x;
			});

			ViewModel.WhenAnyValue (vm => vm.CashsEarnTitle)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					lblTitleCashEarn.Text = x;
			});

//			ViewModel.WhenAnyValue (vm => vm.Month)
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (x => {
//					lblMonthEarn.Text  = string.Format("{0}.",x.ToString("MMM"));
//			});

			ViewModel.WhenAnyValue (vm => vm.Account)
				.Where (x => x != null)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					DateTime month = DateTime.ParseExact(x.Month, "yyyy-MM", CultureInfo.InvariantCulture);
					ViewModel.Month = month;

					lblMonthEarn.Text  = string.Format("{0}.",month.ToString("MMM"));
					lblValueCardEarn.Text = x.CardEarned.ToString();
					lblValueCashEarn.Text = x.CashEarned.ToString();
			});
		}

		#endregion
	}
}

