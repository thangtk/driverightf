﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;
using CoreAnimation;
using ObjCRuntime;
using Unicorn.Core.UI;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using DriveRightF.Core.Enums;
using System.Reactive.Linq;
using System.Reactive.Concurrency;

namespace DriveRightF.iOS
{
	[Register("SmallItemAccountView")]
	public sealed partial class SmallItemAccountView : UBaseView<SmallItemAccountViewModel>
	{
		private string _title;
		private int _expireDate;
		private float _money;
		private string _name;
		private string _backgroundColor;
		private string _image;

		public event EventHandler OnClick;

		public SmallItemAccountView(SmallItemAccountViewModel vm) : base()
		{
			InitView();
			ViewModel = vm;
			SetColor (vm.Colors, null);
			if( vm.Month == DateTime.MinValue ){
				lblMonth.Text = "";
				lblExpire.Hidden = true;
			}
			else
			{
				lblMonth.Text = vm.Month.ToString("MMMM");

				bool isCurrentMonth = vm.Month.Year == DateTime.UtcNow.Year && vm.Month.Month == DateTime.UtcNow.Month;
				lblExpire.Hidden = !isCurrentMonth;
				if(isCurrentMonth)
				{
					ViewModel.ExpireDate = DateTime.DaysInMonth(vm.Month.Year,vm.Month.Month) - DateTime.UtcNow.Day;
				}
			}
			SetFont ();
//			InitBinding ();
			RxApp.TaskpoolScheduler.Schedule (HandleBinding);
//			HandleBinding();
			InitEvent ();
		}

		public SmallItemAccountView(string color) : base()
		{
			InitView();
			SetColor (color, null);
			SetFont ();
			InitBinding ();
			InitEvent ();
		}

		public SmallItemAccountView(string color,string image) : base()
		{
			InitView();
			SetColor (color, image);
			SetFont ();
			InitBinding ();
			InitEvent ();
		}

		public SmallItemAccountView() : base()
		{
			InitView();
			SetFont ();
			InitBinding ();
			InitEvent ();
		}

		public SmallItemAccountView(IntPtr h): base(h)
		{
			InitView ();
			SetFont ();
			InitBinding ();
			InitEvent ();
		}

		private void InitEvent()
		{
			//TODO: remove multi touch of view
			this.ExclusiveTouch = true;

			AddGestureRecognizer (new UITapGestureRecognizer (() => {
				if( this.OnClick != null)
					this.OnClick (this, new EventArgs ());
			}
			));
		}

		private void FixSupperView(UIView supperView, UIView childView)
		{
//			childView.TranslatesAutoresizingMaskIntoConstraints = false;
			supperView.Add (childView);
			var frame = supperView.Frame;
			childView.Frame = new CGRect (0,0,frame.Width,frame.Height);
//			NSLayoutConstraint[] constraints = {
//				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Top,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Top,1,0),
//				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Bottom,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Bottom,1,0),
//				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Trailing,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Trailing,1,0),
//				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Leading,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Leading,1,0)
//			};
//			supperView.AddConstraints (constraints);
		}

		private void InitBinding()
		{

			this.WhenAnyValue (x => x.ViewModel)
				.Where (vm => vm != null)
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (vm => HandleBinding ());

		}

		private void HandleBinding()
		{
			ViewModel.WhenAnyValue (x => x.Month)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					if( x == DateTime.MinValue ){
						lblMonth.Text = "";
						lblExpire.Hidden = true;
					}
					else
					{
						lblMonth.Text = x.ToString("MMMM");

						bool isCurrentMonth = x.Year == DateTime.UtcNow.Year && x.Month == DateTime.UtcNow.Month;
						lblExpire.Hidden = !isCurrentMonth;
						if(isCurrentMonth)
						{
							ViewModel.ExpireDate = DateTime.DaysInMonth(x.Year,x.Month) - DateTime.UtcNow.Day;
						}
					}
			});

			ViewModel.WhenAnyValue (vm => vm.CardsEarnedValue)

				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				lblCardEarned.Text = string.IsNullOrEmpty(x) ? "" : string.Format("{0}: {1}", Translator.Translate("ACCOUNT_CARDS_EARNED"),x);
				});

			ViewModel.WhenAnyValue (vm => vm.CashsEarnedValue)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				lblCashEarned.Text = string.IsNullOrEmpty(x) ? "" : string.Format("{0}: {1}", Translator.Translate("ACCOUNT_CASH_EARNED"),x);
				});

			ViewModel.WhenAnyValue (vm => vm.ExpireDate)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				lblExpire.Text = string.Format ("{0} {1}", x, x > 1 ? Translator.Translate("ACCOUNT_DAYS") : Translator.Translate("ACCOUNT_DAY"));
				});

			ViewModel.WhenAnyValue (vm => vm.Image)
				.Where(x => !string.IsNullOrEmpty(x))
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => backgroundImage.Image = UIImage.FromFile (x));

			ViewModel.WhenAnyValue (vm => vm.Colors, vm => vm.Image)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => SetColor(x.Item1, x.Item2));
		}

		private void SetColor(string color, string image)
		{
			viewBackGround.BackgroundColor = ColorHub.ColorFromHex (color);

			if (!String.IsNullOrEmpty (image))
			{	
				backgroundImage.Image = UIImage.FromFile (image);
				viewBackGround.Alpha = 0.85f;
			}
			else viewBackGround.Alpha = 1f;
		}

		private void InitView()
		{
			var arr = NSBundle.MainBundle.LoadNib("SmallItemAccountView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			FixSupperView (this, v);
		}

		private void SetFont()
		{
			lblExpire.Font = FontHub.HelveticaLt13;
			lblMonth.Font = FontHub.HelveticaLt15;
			lblCardEarned.Font = FontHub.HelveticaLt11;
			lblCashEarned.Font = FontHub.HelveticaLt11;
		}
	}
}
