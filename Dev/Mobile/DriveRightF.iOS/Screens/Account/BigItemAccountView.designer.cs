// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	partial class BigItemAccountView
	{
		[Outlet]
		UIKit.UIImageView backgroundImage { get; set; }

		[Outlet]
		UIKit.UILabel lblBlance { get; set; }

		[Outlet]
		UIKit.UIView viewBackground { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (backgroundImage != null) {
				backgroundImage.Dispose ();
				backgroundImage = null;
			}

			if (lblBlance != null) {
				lblBlance.Dispose ();
				lblBlance = null;
			}

			if (viewBackground != null) {
				viewBackground.Dispose ();
				viewBackground = null;
			}
		}
	}
}
