// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	partial class SmallItemAccountView
	{
		[Outlet]
		UIKit.UIImageView backgroundImage { get; set; }

		[Outlet]
		UIKit.UILabel lblCardEarned { get; set; }

		[Outlet]
		UIKit.UILabel lblCashEarned { get; set; }

		[Outlet]
		UIKit.UILabel lblExpire { get; set; }

		[Outlet]
		UIKit.UILabel lblMonth { get; set; }

		[Outlet]
		UIKit.UILabel lblName { get; set; }

		[Outlet]
		UIKit.UIView viewBackGround { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (backgroundImage != null) {
				backgroundImage.Dispose ();
				backgroundImage = null;
			}

			if (lblCardEarned != null) {
				lblCardEarned.Dispose ();
				lblCardEarned = null;
			}

			if (lblCashEarned != null) {
				lblCashEarned.Dispose ();
				lblCashEarned = null;
			}

			if (lblExpire != null) {
				lblExpire.Dispose ();
				lblExpire = null;
			}

			if (lblMonth != null) {
				lblMonth.Dispose ();
				lblMonth = null;
			}

			if (lblName != null) {
				lblName.Dispose ();
				lblName = null;
			}

			if (viewBackGround != null) {
				viewBackGround.Dispose ();
				viewBackGround = null;
			}
		}
	}
}
