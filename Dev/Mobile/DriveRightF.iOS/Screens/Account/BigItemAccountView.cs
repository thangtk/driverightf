﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;
using CoreAnimation;
using ObjCRuntime;
using Unicorn.Core.UI;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using DriveRightF.Core.Enums;
using System.Reactive.Linq;
using System.Reactive.Concurrency;

namespace DriveRightF.iOS
{
	[Register("BigItemAccountView")]
	public sealed partial class BigItemAccountView : UBaseView<BigItemAccountViewModel>
	{
		public event EventHandler OnClick;

		private void InitEvent()
		{
			//TODO: remove multi touch of view.
			this.ExclusiveTouch = true;

			AddGestureRecognizer (new UITapGestureRecognizer (() => {
				if( this.OnClick != null)
					this.OnClick (this, new EventArgs ());
			}
			));
		}

		public BigItemAccountView(BigItemAccountViewModel vm) : base()
		{
			InitView();
			ViewModel = vm;
			SetColor (vm.Colors ,"img_treasure_chest.png");
			ViewModel.Image = "img_treasure_chest.png";
			SetFont ();
//			InitBinding ();
			RxApp.TaskpoolScheduler.Schedule(HandleBinding);
//			HandleBinding();
			InitEvent ();
		}

		public BigItemAccountView(string color,string image) : base()
		{
			InitView();
			SetColor (color, image);
			ViewModel.Image = image;
			SetFont ();
			InitBinding ();
			InitEvent ();
		}

		public BigItemAccountView() : base()
		{
			InitView();
			SetFont ();
			InitBinding ();
			InitEvent ();
		}

		public BigItemAccountView(IntPtr h): base(h)
		{
			InitView ();
			SetFont ();
			InitBinding ();
			InitEvent ();
		}

		private void InitBinding()
		{

			this.WhenAnyValue (x => x.ViewModel)
				.Where (vm => vm != null)
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (vm => HandleBinding ());

		}

		private void HandleBinding()
		{
			ViewModel.WhenAnyValue (x => x.BalanceValue)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				lblBlance.Text = string.Format("{0}: {1}", Translator.Translate("ACCOUNT_BALANCE"), x);
				});

			ViewModel.WhenAnyValue (vm => vm.Image)
				.Where(x => !string.IsNullOrEmpty(x))
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => backgroundImage.Image = UIImage.FromFile (x));

			ViewModel.WhenAnyValue (vm => vm.Colors, vm => vm.Image)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => SetColor(x.Item1, x.Item2));
		}

		private void SetColor(string color, string image)
		{
			viewBackground.BackgroundColor = ColorHub.ColorFromHex (color);

			if (!String.IsNullOrEmpty (image)) {	
				backgroundImage.Image = UIImage.FromFile (image);
				viewBackground.Alpha = 0.85f;
			} else {
				viewBackground.Alpha = 1f;
			}
		}

		private void InitView()
		{
			var arr = NSBundle.MainBundle.LoadNib("BigItemAccountView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			FixSupperView (this, v);
		}

		private void SetFont()
		{
			lblBlance.Font = FontHub.HelveticaHvCn22;
		}

		private void FixSupperView(UIView supperView, UIView childView)
		{
//			childView.TranslatesAutoresizingMaskIntoConstraints = false;
			supperView.Add (childView);
			var frame = supperView.Frame;
			childView.Frame = new CGRect (0,0,frame.Width,frame.Height);
//			NSLayoutConstraint[] constraints = {
//				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Top,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Top,1,0),
//				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Bottom,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Bottom,1,0),
//				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Trailing,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Trailing,1,0),
//				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Leading,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Leading,1,0)
//			};
//			supperView.AddConstraints (constraints);
		}
	}
}
