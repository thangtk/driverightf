﻿
using System;

using UIKit;
using DriveRightF.Core;
using CoreGraphics;
using DriveRightF.Core.ViewModels;
using System.Drawing;
using DriveRightF.Core.Enums;
using Unicorn.Core;

namespace DriveRightF.iOS
{
	public class AccountScreen : BaseMetroScreen<ItemAccountViewModel>
	{		
//		public AccountScreen() : base ("AccountScreen", null)
//		{
//		}

		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void LoadData (params object[] objs)
		{
			base.LoadData (objs);
		}

		#region implemented abstract members of BaseMetroScreen

		protected override void CreateViewModel ()
		{
			ViewModel = new AccountListViewModel ();
		}

		protected override UIView CreatTileView (ItemAccountViewModel tile, bool isBigTile)
		{
			UIView tileView;
			if (tile == null) {
				return null;
			}
			if (isBigTile) {
				tileView = new BigItemAccountView( tile as BigItemAccountViewModel)
				{
					Frame = new CGRect (tile.Position.X, tile.Position.Y, tile.Size.Width, tile.Size.Height),
				};

				((BigItemAccountView)tileView).OnClick += (sender, e) => 
					DependencyService.Get<IHomeTabNavigator> ().CurrentTab.Navigate ((int)Screen.AccountDetail, new object[] {}, new object[] {tileView});
			} else {
				var vm = tile as SmallItemAccountViewModel;
				tileView = new SmallItemAccountView(vm)
				{
					Frame = new CGRect (tile.Position.X, tile.Position.Y, tile.Size.Width, tile.Size.Height),
				};

				if( vm.Month > DateTime.MinValue){
					((SmallItemAccountView)tileView).OnClick += (sender, e) => {
						//TODO: retrive month data from ViewModel
						DependencyService.Get<IHomeTabNavigator>().CurrentTab.Navigate((int)Screen.AccountStatement, new object[] {vm.Month.ToString("yyyy-MM")},new object[] { tileView });
					};
				}
			}
			return tileView;
		}

		#endregion

//		public override void ViewDidLoad()
//		{
//			base.ViewDidLoad ();
//			ViewModel = new AccountListViewModel ();
//			SetData ();
//			InitView ();
//		}

//		private void BindingData()
//		{
//			ViewModel.whe
//		}

//		private void SetData()
//		{
//			ViewModel.ContentSize = new Size ((int)View.Frame.Width, (int)View.Frame.Height);
//			ViewModel.ComputeTileFrame (ViewModel.TileViewModels);
//
//			ViewModel.TileViewModels.ForEach (tile => {
//				int index = ViewModel.TileViewModels.IndexOf (tile);
//				UIView cellView;
//				if(index == 0)
//				{
//					cellView = new BigItemAccountView()
//					{
//						Frame = new CGRect (tile.Position.X, tile.Position.Y, tile.Size.Width, tile.Size.Height),
//						ViewModel = tile as BigItemAccountViewModel
//					};
//
//					((BigItemAccountView)cellView).OnClick += (sender, e) => {
//						//TODO: retrive month data from ViewModel
//
//						DependencyService.Get<IHomeNavigator>().Navigate((int)Screen.AccountDetail, new object[] {},new object[] { cellView.Frame });
//					};
//
//				}
//				else
//				{
//					cellView = new SmallItemAccountView()
//					{
//						Frame = new CGRect (tile.Position.X, tile.Position.Y, tile.Size.Width, tile.Size.Height),
//						ViewModel = tile as SmallItemAccountViewModel
//					};
//
//					if( tile.Month > DateTime.MinValue){
//						((SmallItemAccountView)cellView).OnClick += (sender, e) => {
//							//TODO: retrive month data from ViewModel
//
//							DependencyService.Get<IHomeNavigator>().Navigate((int)Screen.AccountStatement, new object[] {tile.Month.ToString("yyyy-MM")},new object[] { cellView.Frame });
//						};
//					}
//				}
//
//				contentView.AddSubview (cellView);
//			});
//
//			float rows = (float)ViewModel.TileViewModels.Count / 3f + 1f;
//			contentView.Frame = new CGRect (0, 0, contentView.Frame.Width, rows * (float)View.Frame.Width / 3f);
//
//			scrollView.ContentSize = contentView.Frame.Size;
//		}

//		private void InitView()
//		{
//			CGSize contentSize = scrollView.ContentSize;
//			contentSize.Width = View.Frame.Width;
//			contentSize.Height = contentView.Frame.Height;
//			scrollView.ContentSize = contentSize;
//			scrollView.AddSubview (contentView);
//
//			CGPoint contentOffset = scrollView.ContentOffset;
//			contentOffset.X = 0;
//			contentOffset.Y = 0;
//			scrollView.SetContentOffset (contentOffset, false);
//
//			View.AddSubview (scrollView);
//		}
	}
}

