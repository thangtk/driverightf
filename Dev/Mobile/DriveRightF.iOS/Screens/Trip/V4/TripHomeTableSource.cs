﻿using System;
using DriveRightF.Core;
using DriveRightF.Core.Models;

namespace DriveRightF.iOS
{
	//TODO: update height of first cell.
	public class TripHomeTableSource : DriveRightTableViewSource<TripMonth,ItemTripHomeViewCell>
	{
		public override nfloat GetHeightForRow (UIKit.UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			if (indexPath.Row == 0 || indexPath.Row == Items.Count - 1) {
				return 97f;
			} 
			return 90;
		}
	}
}

