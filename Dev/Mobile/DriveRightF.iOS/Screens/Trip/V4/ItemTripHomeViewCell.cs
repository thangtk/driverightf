﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.Models;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using Unicorn.Core;
using DriveRightF.Core.Enums;

namespace DriveRightF.iOS
{
	public partial class ItemTripHomeViewCell : DriveRightTableViewCell<TripMonth,ItemTripHomeViewModel> 
	{
		public static readonly UINib Nib = UINib.FromName ("ItemTripHomeViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("ItemTripHomeViewCell");

		private nfloat _top;
		private nfloat _bottom;

		public ItemTripHomeViewCell (IntPtr handle) : base (handle)
		{
		}

		public static ItemTripHomeViewCell Create ()
		{
			return (ItemTripHomeViewCell)Nib.Instantiate (null, null) [0];
		}

		#region implemented abstract members of DriveRightTableViewCell

		public override void UpdateCell (TripMonth item, int index, int count)
		{
			//TODO: update height for first and last cell
			constraintTopContent.Constant = index == 0 ? _top + 7f : _top;
			constrainBottomContent.Constant = index == count - 1 ? _bottom - 7f : _bottom;
			

			ViewModel.Trip = item;
		}

		public override void Initialize ()
		{
			_top = constraintTopContent.Constant;
			_bottom = constrainBottomContent.Constant;

			InitControls();
			ViewModel = new ItemTripHomeViewModel ();
			CreateBindingsAsync ();

			AddGestureRecognizer (new UITapGestureRecognizer (() => {
				if( ViewModel.Month > DateTime.MinValue){
//					//TODO: retrive month data from ViewModel
					DependencyService.Get<IHomeTabNavigator> ().CurrentTab.Navigate ((int)Screen.TripStatement, objs: new object[] { ViewModel.Trip});
					;
				}
			}));
		}

		private void InitControls(){

			//			lblDescription.Font = FontHub.HelveticaLt13;
			//			lblUpdateTime.Font = FontHub.HelveticaThin11;
		}


		private void CreateBindingsAsync ()
		{
			RxApp.TaskpoolScheduler.Schedule(()=>{
				InitBindings ();
			});
		}

		private void InitBindings()
		{
			ViewModel.WhenAnyValue (vm => vm.ScoreStatus)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					vContent.BackgroundColor = ColorHub.ColorFromHex(ScoreUtils.GetScoreColor(x));
				});


			ViewModel.WhenAnyValue (vm => vm.TripScore)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					//TODO: load image here.
					if(imgTripBackground.Image == null)
						imgTripBackground.Image = UIImage.FromFile("trip_car_bg.png");
					
					lblTripScore.Text = Math.Round(x, 0).ToString();
				});

			ViewModel.WhenAnyValue (vm => vm.Month)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					lblTripMonth.Text  = string.Format("{0}.",x.ToString("MMM"));
				});
		}

		#endregion
	}
}

