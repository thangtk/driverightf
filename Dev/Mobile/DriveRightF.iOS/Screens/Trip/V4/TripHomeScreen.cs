﻿using System;
using DriveRightF.Core.Models;
using DriveRightF.Core;
using Unicorn.Core;
using DriveRightF.Core.Enums;
using Unicorn.Core.iOS;

namespace DriveRightF.iOS
{
	public class TripHomeScreen: BaseHomeListScreen<TripMonth,ItemTripHomeViewCell>
	{
		public TripHomeScreen (): base()
		{
		}

		#region implemented abstract members of BaseHomeListScreen

		public override void CreatViewModel ()
		{
			ViewModel = new TripHomeViewModel ();
		}

		#endregion
//
		public override void UpdateControls ()
		{
			BannerView.OnClick += (object sender, EventArgs e) => {
				DependencyService.Get<IHomeTabNavigator> ().CurrentTab.Navigate ((int)Screen.TripStatement, objs: new object[] { new TripMonth(){Month = new DateTime(9999,01,01)}});
			};
		}

		#region implemented abstract members of BaseHomeListScreen

		public override void CreateViewContent ()
		{
			TripHomeContentViewModel model = (ViewModel as TripHomeViewModel).TripContentViewModel;
			TripHomeContentView contentView = new TripHomeContentView (model);
			contentView.FixSupperView (ContentView);
		}

		#endregion
	}
}

