// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("ItemTripHomeViewCell")]
	partial class ItemTripHomeViewCell
	{
		[Outlet]
		UIKit.NSLayoutConstraint constrainBottomContent { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint constraintTopContent { get; set; }

		[Outlet]
		UIKit.UIImageView imgTripBackground { get; set; }

		[Outlet]
		UIKit.UILabel lblTripMonth { get; set; }

		[Outlet]
		UIKit.UILabel lblTripScore { get; set; }

		[Outlet]
		UIKit.UIView vContent { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (constrainBottomContent != null) {
				constrainBottomContent.Dispose ();
				constrainBottomContent = null;
			}

			if (constraintTopContent != null) {
				constraintTopContent.Dispose ();
				constraintTopContent = null;
			}

			if (lblTripMonth != null) {
				lblTripMonth.Dispose ();
				lblTripMonth = null;
			}

			if (lblTripScore != null) {
				lblTripScore.Dispose ();
				lblTripScore = null;
			}

			if (vContent != null) {
				vContent.Dispose ();
				vContent = null;
			}

			if (imgTripBackground != null) {
				imgTripBackground.Dispose ();
				imgTripBackground = null;
			}
		}
	}
}
