﻿using System;
using DriveRightF.Core.Models;
using DriveRightF.Core;

namespace DriveRightF.iOS
{
	public class TripHomeContentView : HomeListContentView<TripMonth, ItemTripHomeViewCell>
	{
		public TripHomeContentView (TripHomeContentViewModel model):base(model)
		{
		}

		public override void InitTableSource ()
		{
			_tableSource = new TripHomeTableSource ();
		}
	}
}


