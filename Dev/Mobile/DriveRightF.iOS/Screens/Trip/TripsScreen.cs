﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRight.Core.ViewModels;
using CoreGraphics;
using DriveRightF.Core.ViewModels;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using DriveRightF.Core.Enums;
using Unicorn.Core;

namespace DriveRightF.iOS
{
	public partial class TripsScreen : BaseMetroScreen<TripTileViewModel>//BaseHandlerHeaderViewController<TripTileListViewModel>
	{
		#region implemented abstract members of BaseMetroScreen
		protected override void CreateViewModel ()
		{
			ViewModel = new TripTileListViewModel ();
		}
		protected override UIView CreatTileView (TripTileViewModel tile, bool isBigTile)
		{
			var tileView = new TripTile (tile) {
				Frame = new CGRect (tile.Position.X, tile.Position.Y, tile.Size.Width, tile.Size.Height),
			};
			//tileView.ViewModel = tile;

			if (isBigTile)
				tileView.Type = AwardType.AwardLarge;
			else
				tileView.Type = AwardType.AwardSmall;

			tileView.OnClick += (sender, e) => {
//				if(tileView.Trip.Score > 0)
				if(tileView.Trip != null)
					DependencyService.Get<IHomeTabNavigator> ().CurrentTab.Navigate ((int)Screen.TripStatement, objs: new object[] { tileView.Trip }, transitionParam: new object[] {
						tileView
					});
			};
			//
			return tileView;
		}
		#endregion
//		public TripsScreen() : base ("TripsScreen", null)
//		{
//		}
//
//		public override void ViewDidLoad()
//		{
//			base.ViewDidLoad ();
//			
//			InitView ();
//
//			ViewModel = new TripTileListViewModel ();
//
//			SetData ();
//		}
//
//		private void SetData()
//		{
//			ViewModel.ContentSize = new Size ((int)View.Frame.Width, (int)View.Frame.Height);
//			ViewModel.ComputeTileFrame (ViewModel.TileViewModels);
//
//			ViewModel.TileViewModels.ForEach (tile => {
//				int index = ViewModel.TileViewModels.IndexOf (tile);
//
//				var trip = new TripTile (tile) {
//					Frame = new CGRect (tile.Position.X, tile.Position.Y, tile.Size.Width, tile.Size.Height),
//				};
//				trip.ViewModel = tile;
//
//				if (index == 0)
//					trip.Type = AwardType.AwardLarge;
//				else
//					trip.Type = AwardType.AwardSmall;
//
//				trip.OnClick += (sender, e) => DependencyService.Get<IHomeNavigator> ().Navigate ((int)Screen.TripStatement, objs: new object[] { trip.Trip }, transitionParam: new object[] {
//					trip.Frame
//				});
//
//				contentView.AddSubview (trip);
//			});
//
//			float rows = (float)ViewModel.TileViewModels.Count / 3f + 1f;
//			contentView.Frame = new CGRect (0, 0, contentView.Frame.Width, rows * (float)View.Frame.Width / 3f);
//
//			scrollView.ContentSize = contentView.Frame.Size;
//		}
//
//		private void InitView()
//		{
//			CGSize contentSize = scrollView.ContentSize;
//			contentSize.Width = View.Frame.Width;
//			contentSize.Height = contentView.Frame.Height;
//			scrollView.ContentSize = contentSize;
//			scrollView.AddSubview (contentView);
//
//			CGPoint contentOffset = scrollView.ContentOffset;
//			contentOffset.X = 0;
//			contentOffset.Y = 0;
//			scrollView.SetContentOffset (contentOffset, false);
//
//			View.AddSubview (scrollView);
//		}
	}
}

