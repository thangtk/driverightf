﻿using System;
using DriveRightF.Core.Enums;
using DriveRightF.Core;

namespace DriveRightF.iOS
{
	public class TripTabScreen: BaseTabItemScreen
	{
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.Navigate ((int)Screen.HomeTrips);
		}
		protected override void InitData(){
			base.InitData ();

		}
		public override void TabClick (){
			if (CurrentScreen != (int)Screen.HomeTrips) {
				this.NavigateBack ((int)Screen.HomeTrips,true);
			}
		}
	}
}

