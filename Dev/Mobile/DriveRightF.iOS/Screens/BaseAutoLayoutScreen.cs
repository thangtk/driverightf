﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using CoreGraphics;
using System.Collections.Generic;
using Unicorn.Core.Navigator;
using CoreAnimation;

namespace DriveRightF.iOS
{
	public partial class BaseAutoLayoutScreen<T> : BaseNavigableViewController<T>,ITransitionDelegate where T: BaseViewModel
	{
		private UIView _headerView;
		//		private UIView _contentView;
		private UIView _tabView;
		private UIView _underGroundView;
		private UIView _rightMenu;
		private UIView _maskSupperView;

		private float _heightHeader;
		private float _heightTabBar;

		CGPoint _lastPositon ;

		private const float MAX_ALPHA = 0.5f;

		public virtual float HEIGHT_HEADER
		{
			get{
				return _heightHeader;
			}
			set {
				_heightHeader = value;

			}
		}

		public virtual float HEIGHT_TAB_BAR
		{
			get{
				return _heightTabBar;
			}
			set {
				_heightTabBar = value;
			}
		}

		public virtual float SPACE_MENU
		{
			get {
				return 60;
			}
		}

		public virtual bool IsEnableTabBar
		{
			set {
				if (vTab != null)
					vTab.UserInteractionEnabled = value;
			}
		}

		public UIView HeaderView
		{
			get {
				return _headerView;
			}
			set {
				if (_headerView != null) {
					((DHeaderView)_headerView).RemoveFromSuperview ();
				}
				_headerView = value;
				var frame = vTest.Frame;
				((DHeaderView)_headerView).Frame = new CGRect (0, 0, frame.Width, frame.Height);
				vTest.Add (((DHeaderView)_headerView).RootView);
//				FixSupperView(vTest, _headerView);
			}
		}

		public UIView MaskSupperView
		{
			get {
				return _maskSupperView;
			}
			set {
				if (_maskSupperView != null) {
					_maskSupperView.RemoveFromSuperview ();
				}
				_maskSupperView = value;
				_maskSupperView.UserInteractionEnabled = false;
				var frame = View.Frame;
				_maskSupperView.Frame = new CGRect (0, 0, frame.Width, frame.Height);
				View.Add (_maskSupperView);
//				FixSupperView(View, _maskSupperView);
			}
		}

		public UIView TabView
		{
			get {
				return _tabView;
			}
			set {
				if (_tabView != null) {
					((DTabBar)_tabView).RootView.RemoveFromSuperview ();
				}
				_tabView = value;
				vTab.Add(((DTabBar)_tabView).RootView);
				var frame = vTab.Frame;
				((DTabBar)_tabView).RootView.Frame = new CGRect (0, 0, frame.Width, frame.Height);
				vTab.Add (((DTabBar)_tabView).RootView);
//				FixSupperView(vTab, ((DTabBar)_tabView).RootView);
			}
		}

		public UIView UnderGroundView
		{
			get {
				return _underGroundView;
			}
			set {
				if (_underGroundView != null) {
					_underGroundView.RemoveFromSuperview ();
				}
				_underGroundView = value;
				var frame = vUnderGround.Frame;
				_underGroundView.Frame = new CGRect (0, 0, frame.Width, frame.Height);
				vUnderGround.Add (_underGroundView);
//				FixSupperView(vUnderGround, _underGroundView);
			}
		}

		public UIView RightMenu
		{
			get {
				return _rightMenu;
			}
			set {
				if (_rightMenu != null) {
					_rightMenu.RemoveFromSuperview ();
				}
				_rightMenu = value;
				var frame = vRightMenu.Frame;
				_rightMenu.Frame = new CGRect (0, 0, frame.Width, frame.Height);
				vRightMenu.Add (_rightMenu);
//				FixSupperView(vRightMenu, _rightMenu);
			}
		}

		public BaseAutoLayoutScreen () : base ("BaseAutoLayoutScreen", null)
		{

		}		

//		protected void FixSupperView(UIView supperView, UIView childView)
//		{
//			childView.TranslatesAutoresizingMaskIntoConstraints = false;
//			supperView.BackgroundColor = UIColor.Clear;
//			supperView.Add (childView);
////			NSLayoutConstraint[] constraints = {
////				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Top,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Top,1,0),
////				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Bottom,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Bottom,1,0),
////				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Trailing,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Trailing,1,0),
////				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Leading,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Leading,1,0)
////			};
////			supperView.AddConstraints (constraints);
//		}

		private void AnimationSepareted (bool isOpen)
		{
			//			bool isOpen = paddingTopHeader.Constant < 0;
//			paddingTopHeader.Constant = isOpen ? 0 : -HEIGHT_HEADER;
//			paddingTopContent.Constant = isOpen ? HEIGHT_HEADER : vContent.Frame.Height;
//			paddingBottomContent.Constant = isOpen ? 0 : -vContent.Frame.Height;

			var frameH = vTest.Frame;
			frameH.Y = isOpen ? 0 : -(HEIGHT_HEADER + 4) ;
			var frameTop = vContent.Frame;
			frameTop.Y = isOpen ? HEIGHT_HEADER : (View.Frame.Height - HEIGHT_HEADER);

			var frameOverlay = vOverLay.Frame;
			frameOverlay.Y = isOpen ? HEIGHT_HEADER : (View.Frame.Height - HEIGHT_HEADER);
			UIView.Animate (0.5, () =>  {
				vContent.Frame = frameTop;
				vTest.Frame = frameH;
				vOverLay.Frame = frameOverlay;
				View.LayoutIfNeeded ();
			}, () =>  {
				vOverLay.UserInteractionEnabled = !isOpen;
			});
		}

		public void OpenView()
		{
			AnimationSepareted(true);
		}

		public void CloseView()
		{
			AnimationSepareted(false);
		}

		private void AnimationMenu(bool isOpen)
		{
//			paddingLeftMenuRight.Constant = isOpen ? SPACE_MENU : View.Frame.Width;
			//			markRightMenu.Alpha = isOpen ? 0 : MAX_ALPHA;
			UIView.Animate (0.5, () => {
				var frame = vRightMenu.Frame;
				frame.X = isOpen ? SPACE_MENU : View.Frame.Width ;
				vRightMenu.Frame = frame;
				vMaskRightMenu.Alpha = !isOpen ? 0 : MAX_ALPHA;
				View.LayoutIfNeeded ();
			});
		}

		public void OpenMenu()
		{
			AnimationMenu(true);
		}

		public void CloseMenu()
		{
			AnimationMenu(false);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			ContainerView = vPage;
			HEIGHT_TAB_BAR = FOOTER_HEIGHT;
			HEIGHT_HEADER = HEADER_HEIGHT;

			InitControls ();				
		}

		public const float HEADER_HEIGHT = 64;
		public const float FOOTER_HEIGHT = 44;

		private void InitControls()
		{
			vOverLay.UserInteractionEnabled = false;
			//			vMaskSupperView.UserInteractionEnabled = false;

			tapOverlay.AddTarget (() => {

//				vOverLay.Frame;
//				vPage.Frame;

				OpenView();
			});

			panMenu.AddTarget (() => HandlePanMenu(panMenu));
			tapMaskMenu.AddTarget (() => {
				CloseMenu();
			});

		}
		CGRect _rightMenRect;
		private void HandlePanMenu(UIPanGestureRecognizer recognizer)
		{
			CGPoint touchPosition = recognizer.LocationInView(View);
			if (recognizer.State == UIGestureRecognizerState.Began) {
				//TODO: set origin location
				_lastPositon = recognizer.LocationInView (View);
				_rightMenRect = vRightMenu.Frame;
			} else if (recognizer.State == UIGestureRecognizerState.Changed) {
				nfloat detal = touchPosition.X - _lastPositon.X;

				var xMove = _rightMenRect.X + detal;
				var alpha = (xMove - View.Frame.Width)* MAX_ALPHA/(SPACE_MENU - View.Frame.Width);
				if (xMove >=SPACE_MENU ) {
//					paddingLeftMenuRight.Constant = xMove;
					var frame = vRightMenu.Frame;
					frame.X = xMove;
					vRightMenu.Frame = frame;
					_rightMenRect = frame;
					vMaskRightMenu.Alpha = alpha;

				}
			} else if (recognizer.State == UIGestureRecognizerState.Cancelled || recognizer.State == UIGestureRecognizerState.Ended) {
				float velocity = (float)recognizer.VelocityInView (View).X;
				AnimationMenu (velocity < 0);
			}
			_lastPositon = touchPosition;
		}
	}

}

