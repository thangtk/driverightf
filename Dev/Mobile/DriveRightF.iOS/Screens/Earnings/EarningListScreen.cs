﻿
using System;
using System.Collections.Generic;

using System.Drawing;
using DriveRightF.Core;
using DriveRightF.Core.Models;
using Foundation;
using UIKit;
using CoreGraphics;
using ReactiveUI;
using DriveRightF.Core.ViewModels;
using System.Reactive.Linq;

namespace DriveRightF.iOS
{
	public partial class EarningListScreen : BaseAutoRequestListScreen<Earning>//DriveRightBaseViewController<EarningsViewModel>
	{
		private EarningsTableViewSource _tableSource = new EarningsTableViewSource();

		#region implemented abstract members of BaseAutoRequestListScreen

		public override void InitViewModel ()
		{
			ViewModel = new EarningViewModel ();
		}

		public override void InitControls ()
		{
//			tblEarningList.Source = _tableSource;
//			tblEarningList.AddSubview (_refreshControl);
			base.InitControls();

			btnNewData.Alpha = 0;
			View.BringSubviewToFront (btnNewData);
		}

		protected override void AddNoDataView ()
		{
			base.AddNoDataView ();

			View.BringSubviewToFront (btnNewData);
		}

		protected override UITableView ContentTableView {
			get {
				return tblEarningList;
			}
		}

		protected override DRTableViewSource<Earning> ContentTableSource {
			get {
				return _tableSource;
			}
		}

		#endregion

		public override void InitBindings ()
		{
			base.InitBindings ();

//			this.BindCommand(ViewModel, vm => vm.BackCommand, v => v.btnBack);

			btnNewData.OnClick += (sender, e) => {
				if (ViewModel != null)
					ViewModel.AppendNewdata.Execute(true);
				btnNewData.Hide();
			};

			this.OneWayBind(
				ViewModel,
				vm => vm.HeaderTitle,
				v => v.headerView.Title);

			ViewModel.WhenAnyValue (x => x.HeaderLogo)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					headerView.LogoImage = UIImage.FromBundle (x);	
				});
			
		}
	}
}

