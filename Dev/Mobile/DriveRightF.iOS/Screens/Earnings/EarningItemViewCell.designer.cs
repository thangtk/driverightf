// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("EarningItemViewCell")]
	partial class EarningItemViewCell
	{
		[Outlet]
		DriveRightF.iOS.ImageMutilStatus ItemIcon { get; set; }

		[Outlet]
		UIKit.UILabel lblDescription { get; set; }

		[Outlet]
		UIKit.UILabel lblNoEarning { get; set; }

		[Outlet]
		UIKit.UIView viewCell { get; set; }

		[Outlet]
		UIKit.UIActivityIndicatorView viewLoading { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ItemIcon != null) {
				ItemIcon.Dispose ();
				ItemIcon = null;
			}

			if (lblDescription != null) {
				lblDescription.Dispose ();
				lblDescription = null;
			}

			if (lblNoEarning != null) {
				lblNoEarning.Dispose ();
				lblNoEarning = null;
			}

			if (viewCell != null) {
				viewCell.Dispose ();
				viewCell = null;
			}

			if (viewLoading != null) {
				viewLoading.Dispose ();
				viewLoading = null;
			}
		}
	}
}
