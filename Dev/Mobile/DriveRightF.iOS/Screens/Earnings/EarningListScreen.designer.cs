// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("EarningListScreen")]
	partial class EarningListScreen
	{
		[Outlet]
		DriveRightF.iOS.ButtonNewData btnNewData { get; set; }

		[Outlet]
		DriveRightF.iOS.TitleHeaderView headerView { get; set; }

		[Outlet]
		UIKit.UILabel lblNoEarning { get; set; }

		[Outlet]
		UIKit.UITableView tblEarningList { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnNewData != null) {
				btnNewData.Dispose ();
				btnNewData = null;
			}

			if (headerView != null) {
				headerView.Dispose ();
				headerView = null;
			}

			if (lblNoEarning != null) {
				lblNoEarning.Dispose ();
				lblNoEarning = null;
			}

			if (tblEarningList != null) {
				tblEarningList.Dispose ();
				tblEarningList = null;
			}
		}
	}
}
