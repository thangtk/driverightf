﻿
using System;
using System.Drawing;

using System.Threading.Tasks;
using CoreGraphics;
using DriveRightF.Core;
using DriveRightF.Core.Models;
using DriveRightF.Core.ViewModels;
using Foundation;
using ReactiveUI;
using UIKit;
using Unicorn.Core.UI;
using DriveRightF.Core.Enums;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

namespace DriveRightF.iOS
{
	public partial class EarningItemViewCell : DriveRightTableViewCell<Earning,EarningItemViewModel>
	{

		private UIButton _btnAdd;
		private RadialProgressView _progressVew;
		private UIImageView _imgDone;

		private EarningItemViewCell (IntPtr handle) : base (handle)
		{
		}

		#region implemented abstract members of DriveRightTableViewCell
		public override void UpdateCell (Earning item, int index)
		{
			ViewModel.Item = item;
		}
		public override void Initialize ()
		{
			InitControls ();
			InitBindings ();
		}


		private void InitBindings()
		{
			ViewModel = new EarningItemViewModel ();

			ItemIcon.ViewModel = new ImageMultiStatusViewModel ((int)Screen.Earnings) {
			};
			ViewModel.ItemIconViewModel = ItemIcon.ViewModel;

			var gr = new UITapGestureRecognizer (()=> 
				{
					if(ViewModel != null)
					{
						ViewModel.RemoveItemStatusCommand.Execute(null);
						Earning item = ViewModel.Item;
						if (item.Status != EarningStatus.New) {
							ViewModel.ItemClickCommand.Execute (item.EarningId);
						}
					}
				});
			this.AddGestureRecognizer (gr);

			this.OneWayBind (ViewModel, vm => vm.Name, v => v.lblDescription.Text);
			ViewModel.WhenAnyValue (vm => vm.Status)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe (AddStatusView);

		}

		private void AddStatusView(EarningStatus status)
		{
			switch (status) {
			case EarningStatus.New:
				AddButton ();
				break;
			case EarningStatus.InProgress:
				AddRadialProgress ((int)ViewModel.Item.Progress);
				break;
			case EarningStatus.Done:
				AddDoneImage ();
				break;
			default:
				break;
			}
		}

		private void InitControls(){

			lblDescription.Font = FontHub.RobotoRegular17;
		}
		#endregion

		private void AddRadialProgress(int progress)
		{
			if (_progressVew == null) 
			{
				_progressVew = new RadialProgressView (new CGPoint (260, 15), 45);
				_progressVew.SetValue (progress, true);
				viewCell.AddSubview (_progressVew);
			}

			_progressVew.Hidden = false;

			if (_btnAdd != null) {
				_btnAdd.Hidden = true;
			}

			if (_imgDone != null) {
				_imgDone.Hidden = true;
			}

		}

		private void AddButton()
		{
			if (_btnAdd == null) {
				_btnAdd = new UIButton () {
					Frame = new CGRect (260, 26, 45, 22)
				};

				_btnAdd.SetImage (UIImage.FromFile("button_add2.png"), UIControlState.Normal);
				_btnAdd.BackgroundColor = UIColor.Clear;
				if (ViewModel != null) {
					Action action = new Action (delegate {
						_btnAdd.RemoveFromSuperview ();
						AddRadialProgress (0);
					});

					_btnAdd.TouchUpInside += (sender, e) => {
						ViewModel.RemoveItemStatusCommand.Execute(null);
						ViewModel.AddClickCommand.Execute (action);
					};
				}

				viewCell.AddSubview (_btnAdd);
			}

			_btnAdd.Hidden = false;

			if (_progressVew != null) {
				_progressVew.Hidden = true;
			}

			if (_imgDone != null) {
				_imgDone.Hidden = true;
			}

		}

		private void AddDoneImage()
		{
			if (_imgDone == null) {
				_imgDone = new UIImageView () {
					Frame = new CGRect (260, 15, 45, 45),
					Image = UIImage.FromFile ("button_v.png")
				};
				viewCell.AddSubview (_imgDone);
			}

			_imgDone.Hidden = false;

			if (_progressVew != null) {
				_progressVew.Hidden = true;
			}

			if (_btnAdd != null) {
				_btnAdd.Hidden = true;
			}
		}
	}
}

