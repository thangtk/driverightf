// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("ProfileScreenX")]
	partial class ProfileScreenX
	{
		[Outlet]
		UIKit.UIButton btnAccount { get; set; }

		[Outlet]
		UIKit.UIButton btnCapture { get; set; }

		[Outlet]
		UIKit.UIButton btnEditName { get; set; }

		[Outlet]
		UIKit.UIButton btnNumberPlate { get; set; }

		[Outlet]
		UIKit.UIButton btnPhone { get; set; }

		[Outlet]
		UIKit.UIButton btnSave { get; set; }

		[Outlet]
		UIKit.UIView circleImageBackground { get; set; }

		[Outlet]
		UIKit.UIView imgContainer { get; set; }

		[Outlet]
		UIKit.UIImageView imgPortrait { get; set; }

		[Outlet]
		UIKit.UILabel lblName { get; set; }

		[Outlet]
		UIKit.UILabel lblNumberPlate { get; set; }

		[Outlet]
		UIKit.UILabel lblPaymentAccount { get; set; }

		[Outlet]
		UIKit.UILabel lblPhoneNumber { get; set; }

		[Outlet]
		UIKit.UIActivityIndicatorView loadingImage { get; set; }

		[Outlet]
		UIKit.UIScrollView scrollView { get; set; }

		[Outlet]
		UIKit.UITextField tbxAccountNumber { get; set; }

		[Outlet]
		UIKit.UITextField tbxName { get; set; }

		[Outlet]
		UIKit.UITextField tbxNumberPlate { get; set; }

		[Outlet]
		UIKit.UITextField tbxPhoneNo { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnAccount != null) {
				btnAccount.Dispose ();
				btnAccount = null;
			}

			if (btnCapture != null) {
				btnCapture.Dispose ();
				btnCapture = null;
			}

			if (btnEditName != null) {
				btnEditName.Dispose ();
				btnEditName = null;
			}

			if (btnNumberPlate != null) {
				btnNumberPlate.Dispose ();
				btnNumberPlate = null;
			}

			if (btnPhone != null) {
				btnPhone.Dispose ();
				btnPhone = null;
			}

			if (btnSave != null) {
				btnSave.Dispose ();
				btnSave = null;
			}

			if (circleImageBackground != null) {
				circleImageBackground.Dispose ();
				circleImageBackground = null;
			}

			if (imgContainer != null) {
				imgContainer.Dispose ();
				imgContainer = null;
			}

			if (imgPortrait != null) {
				imgPortrait.Dispose ();
				imgPortrait = null;
			}

			if (lblName != null) {
				lblName.Dispose ();
				lblName = null;
			}

			if (lblNumberPlate != null) {
				lblNumberPlate.Dispose ();
				lblNumberPlate = null;
			}

			if (lblPaymentAccount != null) {
				lblPaymentAccount.Dispose ();
				lblPaymentAccount = null;
			}

			if (lblPhoneNumber != null) {
				lblPhoneNumber.Dispose ();
				lblPhoneNumber = null;
			}

			if (loadingImage != null) {
				loadingImage.Dispose ();
				loadingImage = null;
			}

			if (scrollView != null) {
				scrollView.Dispose ();
				scrollView = null;
			}

			if (tbxAccountNumber != null) {
				tbxAccountNumber.Dispose ();
				tbxAccountNumber = null;
			}

			if (tbxName != null) {
				tbxName.Dispose ();
				tbxName = null;
			}

			if (tbxNumberPlate != null) {
				tbxNumberPlate.Dispose ();
				tbxNumberPlate = null;
			}

			if (tbxPhoneNo != null) {
				tbxPhoneNo.Dispose ();
				tbxPhoneNo = null;
			}
		}
	}
}
