﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using System.Linq;
using CoreGraphics;
using ReactiveUI;
using Unicorn.Core;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using Unicorn.Core.Enums;
using Unicorn.Core.iOS;
using DriveRightF.Core.Models;
using DriveRightF.Core.Managers;
using Unicorn.Core.UI;
using CoreAnimation;

namespace DriveRightF.iOS
{
	public partial class ProfileScreenX : BaseHandlerHeaderViewController<ProfileViewModel>
	{

		public ProfileScreenX() : base("ProfileScreenX", null)
		{
		}

		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void LoadData(params object[] objs)
		{
			base.LoadData(objs);

//			if(ViewModel == null) ViewModel = new ProfileViewModel ();
		}

		public override void ViewDidLoad()
		{
			MyDebugger.ShowCost("ViewDidLoad - in");
			base.ViewDidLoad();
			
			// Perform any additional setup after loading the view, typically from a nib.
			ViewModel = new ProfileViewModel ();
			Initialize();
			MyDebugger.ShowCost("ViewDidLoad - in 2");
			ViewModel.GetProfileCommand.Execute(null);
			MyDebugger.ShowCost("ViewDidLoad - out");
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			MyDebugger.ShowCost("ViewDidAppear - out");
		}

		private void InitControls()
		{
			this.View.TranslatesAutoresizingMaskIntoConstraints = true;
			// Responding of UIButton in scrollview
//			scrollView.DelaysContentTouches = false;
			//

			tbxName.AddHideButtonToKeyboard(KeyboardType.Default);
			tbxNumberPlate.AddHideButtonToKeyboard(KeyboardType.Default);
			tbxPhoneNo.AddHideButtonToKeyboard(KeyboardType.Phone);
			tbxAccountNumber.AddHideButtonToKeyboard(KeyboardType.Number);
			tbxPhoneNo.Enabled = false;
			tbxAccountNumber.Enabled = false;
			SetButtonEnabled(btnSave, false);

//			CAGradientLayer gradient = new CAGradientLayer ();
//			gradient.Frame = imgContainer.Bounds;
//			gradient.NeedsDisplayOnBoundsChange = true;
//			gradient.MasksToBounds = true;
//			gradient.Colors = new CGColor[]{ UIColor.FromRGB(1, 102, 176).CGColor, UIColor.FromRGB(1, 159, 218).CGColor };
//			imgContainer.Layer.InsertSublayer(gradient, 0);

			imgContainer.BackgroundColor = UIColor.FromPatternImage(UIImage.FromBundle("profile_bg.png"));

			// Round button
			btnSave.Layer.CornerRadius = 4f;
			btnSave.Layer.MasksToBounds = true;

			tbxPhoneNo.ShouldChangeCharacters = (textField, range, replacementString) =>
			{
				var newLength = textField.Text.Length + replacementString.Length - range.Length;
				return newLength <= 15;
			};

			tbxAccountNumber.ShouldChangeCharacters = (textField, range, replacementString) =>
			{
				var newLength = textField.Text.Length + replacementString.Length - range.Length;
				return newLength <= 15;
			};

			tbxName.ShouldChangeCharacters = (textField, range, replacementString) =>
			{
				var newLength = textField.Text.Length + replacementString.Length - range.Length;
				return newLength <= 30;
			};

			tbxNumberPlate.ShouldChangeCharacters = (textField, range, replacementString) =>
			{
				var newLength = textField.Text.Length + replacementString.Length - range.Length;
				return newLength <= 15;
			};

		}

		private void SetTexts()
		{
			lblName.Text = Translator.Translate("PROFILE_NAME");
			lblNumberPlate.Text = Translator.Translate("PROFILE_NUMBER_PLATE");
			lblPaymentAccount.Text = Translator.Translate("PROFILE_PAYMENT_ACCOUNT");
			lblPhoneNumber.Text = Translator.Translate("PROFILE_PHONE_NUMBER");
			btnSave.SetTitle(Translator.Translate("BUTTON_SAVE"), UIControlState.Normal);
		}

		public override void ViewDidLayoutSubviews()
		{
			base.ViewDidLayoutSubviews();
			// Circle image
			imgPortrait.Layer.CornerRadius = imgPortrait.Frame.Width / 2;
			imgPortrait.ClipsToBounds = true;
			circleImageBackground.Layer.CornerRadius = circleImageBackground.Frame.Width / 2;
		}

		private void Initialize()
		{
			UIApplication.SharedApplication.SetStatusBarHidden(false, false);
			InitControls();
			CreateBindingsAsync();
			SetTexts();
			InitBindCommands();
//			AddCaptureImageView ();
		}

		private void CreateCaptureImageView()
		{
			//TODO: update frame from window view.

			_captureImage = new CaptureImageView ( View.Window.Frame);
			View.Window.AddSubview(_captureImage);
			_captureImage.Hidden = true;

			ViewModel.CaptureImageViewModel = _captureImage.ViewModel;

//			var frame = View.Frame;
//			frame.Y = -frame.Height;
//			_captureImage.Frame = frame;

			var frame = View.Window.Frame;
			frame.Y = -frame.Height;
			_captureImage.Frame = frame;
			View.Window.BringSubviewToFront(_captureImage);
		}

		CaptureImageView _captureImage;

		private void InitBindCommands()
		{
			btnEditName.TouchUpInside += (sender, e) => tbxName.BecomeFirstResponder();
			btnCapture.TouchUpInside += (object sender, EventArgs e) =>
			{
				View.EndEditing(true);
				if(_captureImage == null)
				{
					CreateCaptureImageView();
				}
				_captureImage.Hidden = false;
				ViewModel.OnCaptureClicked.Execute(null);
			};
			btnSave.TouchUpInside += (sender, e) =>
			{
				View.EndEditing(true);
				ViewModel.UpdateProfileCommand.Execute(null);
			};
			btnPhone.TouchUpInside += (sender, e) =>
			{
				try {
					View.EndEditing(true);
					var contentView = CreatePhonePopupContent(text =>
					{
						ViewModel.TempPhoneNo = text;
					}, UIKeyboardType.PhonePad);
					ViewModel.ChangePhoneNumber.Execute(contentView);
				} catch (Exception ex) {
					(new PopupManager()).ShowAlert("Error", ex.ToString());
				}
			};
			//
			btnNumberPlate.TouchUpInside += (sender, e) =>
			{
				View.EndEditing(true);
				ViewModel.ChangeNumberPlate.Execute(null);
			};
			//
			btnAccount.TouchUpInside += (sender, e) =>
			{
				View.EndEditing(true);
				var contentView = new PaymentPopupContent (new CGRect (0, 0, 300, 230),ViewModel.PaymentViewModel);
//				contentView.ViewModel = ViewModel.PaymentViewModel;
				ViewModel.ChangeAccountNumber.Execute(contentView);
			};

		}

		/// <summary>
		/// Will invoke on another thread
		/// NOTE: Bind & BindCommand will udpate on the same thread with registration process
		/// Use WhenAny series instead
		/// RegisterTextChangedEvent to update ViewModel's properties
		/// </summary>
		private void InitBindings()
		{
			//TODO: warning don't check condition null, some case ui will don't reload
			ViewModel.WhenAnyValue(vm => vm.Name).Where(x => x != null).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x => tbxName.Text = x);
			ViewModel.WhenAnyValue(vm => vm.PhoneNumber).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x => tbxPhoneNo.Text = x);
			ViewModel.WhenAnyValue(vm => vm.NumberPlate).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x => 
			{
				tbxNumberPlate.Text = x;
			});
			ViewModel.WhenAnyValue(vm => vm.PaymentAccount)
//				.Where(x => x != null)
				.ObserveOn(RxApp.MainThreadScheduler).Subscribe(x => tbxAccountNumber.Text = x);
			//
			RegisterTextChangedEvent();
			//
			ViewModel.WhenAnyValue(vm => vm.ImagePath).Where(x => !string.IsNullOrEmpty(x)).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x =>
			{
				var img = UIImage.FromFile(x);
				if(img == null)
				{
					img = UIImage.FromBundle("icon_unknown_user.png");
					// default image
				}
				imgPortrait.Image = img;
			});
			ViewModel.WhenAnyValue(vm => vm.IconUrl).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x =>
			{
				if(!string.IsNullOrEmpty(x))
				{
					UIImage image = UIImage.LoadFromData(NSData.FromUrl(new NSUrl (x)));
					if(image != null)
					{
						imgPortrait.Image = image;
					}
				}
//				else imgPortrait.Image = null;
			});
			ViewModel.WhenAnyValue(vm => vm.IconData).Where(x => x != null).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x =>
			{
				imgPortrait.Image = x as UIImage;
			});
			ViewModel.WhenAnyValue(vm => vm.IsPictureLoading).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x =>
			{
				loadingImage.Hidden = !x;
			});
			ViewModel.WhenAnyValue(vm => vm.AnyFieldChanged).ObserveOn(RxApp.MainThreadScheduler).Subscribe(b => SetButtonEnabled(btnSave, b));

			// on action complete
			ViewModel.WhenAnyObservable(vm => vm.GetSMSCodeCommand)
				.Where(b => b)
				.Subscribe(x =>
			{
				var contentView = CreatePhonePopupContent(sms => 
				{
					ViewModel.SMSCode = sms;
				});
				ViewModel.VerifySMSCode.Execute(contentView);
			});
		}

		/// <summary>
		/// This maybe finish after Load data so every binding should use WhenAny command series
		/// </summary>
		private void CreateBindingsAsync()
		{
			RxApp.TaskpoolScheduler.Schedule(() =>
			{
				InitBindings();
			});
		}

		private void SetButtonEnabled(UIButton btn, bool enable)
		{
			btn.Enabled = enable;
			btn.Layer.Opacity = enable ? 1f : 0.5f;
		}

		private UIView CreatePhonePopupContent(Action<string> textChanged, UIKeyboardType keyboardType = UIKeyboardType.NumberPad)
		{
			var width = this.View.Frame.Width * 0.9f;
			nfloat height = 70f;
			UIView view = new UIView (new CGRect (0, 0, width, height));
			view.BackgroundColor = UIColor.FromRGB(255, 255, 255); // Same color with dialog
			nfloat padding = 16f;
			UITextField tbx = new UITextField (new CGRect (padding, 15f, width - padding * 2, 40)) {
				BackgroundColor = UIColor.FromRGB(239, 239, 239),
				LeftViewMode = UITextFieldViewMode.Always,
				LeftView = new UIView (new CGRect (0, 0, 15, 5)),
				KeyboardType = keyboardType
			};
			tbx.Layer.CornerRadius = 2;
			tbx.EditingChanged += (object sender, EventArgs e) =>
			{
				if(textChanged != null)
				{
					textChanged.Invoke((sender as UITextField).Text);
				}
			};

			tbx.ShouldChangeCharacters = (textField, range, replacementString) =>
			{
				var newLength = textField.Text.Length + replacementString.Length - range.Length;
				return newLength <= 15;
			};

			view.AddSubview(tbx);
			return view;
		}

		private void RegisterTextChangedEvent()
		{
			NSNotificationCenter.DefaultCenter.AddObserver(UITextField.TextFieldTextDidChangeNotification, notification =>
			{
				OnTextChanged(notification.Object);
			});
		}

		private void OnTextChanged(object sender)
		{
			if(sender == null || ViewModel == null) return;
			//
			if(sender.Equals(tbxName))
			{
				ViewModel.Name = tbxName.Text;
			}
			else if(sender.Equals(tbxPhoneNo))
			{
				ViewModel.PhoneNumber = tbxPhoneNo.Text;
			}
			else if(sender.Equals(tbxNumberPlate))
			{
				ViewModel.NumberPlate = tbxNumberPlate.Text;
			}
			else if(sender.Equals(tbxAccountNumber))
			{
				ViewModel.PaymentAccount = tbxAccountNumber.Text;
			}
		}
	}
}

