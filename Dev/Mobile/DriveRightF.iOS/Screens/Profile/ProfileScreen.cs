﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using CoreGraphics;
using ReactiveUI;
using Unicorn.Core;
using System.Reactive.Linq;
using DriveRightF.Core.UI;
using System.Reactive.Concurrency;
using System.Threading.Tasks;
using Unicorn.Core.Enums;

namespace DriveRightF.iOS
{
	public partial class ProfileScreen : DriveRightBaseViewController<ProfileViewModel>
	{
		public ProfileScreen () : base ("ProfileScreen", null)
		{

		}

		public override void LoadData (params object[] objs)
		{
			base.LoadData (objs);
//			if (ViewModel != null) {
//				ViewModel.InitData ();
//			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			UIApplication.SharedApplication.SetStatusBarHidden (false, false);

			Debugger.ShowCost (">>>>>>>>>>>>>> Create CaptureImage");
			InitControls ();
			Debugger.ShowCost (">>>>>>>>>>>>>>begin Profile InitControls");

			RxApp.TaskpoolScheduler.Schedule (()=>{
				ViewModel = new ProfileViewModel ();
				InitBindingValue ();
				RxApp.MainThreadScheduler.Schedule(()=>
					{
						InitBindCommand();
						AddCaptureImageView();
						btnOk.Enabled = false;
					});
			});

			Debugger.ShowCost (">>>>>>>>>>>>>> Profile init binding");

		}

		private void AddCaptureImageView()
		{
			_captureImage = new CaptureImageView (this.View.Bounds);
			this.View.AddSubview(_captureImage);
			_captureImage.Hidden = true;

			ViewModel.CaptureImageViewModel = _captureImage.ViewModel;

			var frame = View.Frame;
			frame.Y = -frame.Height;
			_captureImage.Frame = frame;
			this.View.BringSubviewToFront (_captureImage);
		}

		CaptureImageView _captureImage;

		private void InitControls()
		{

			headerView.LogoImage = UIImage.FromFile ("logo_profile.png");
			lblPicture.Font = FontHub.RobotoLight14;
			lblPerson.Font = FontHub.RobotoLight14;

			drtbPhoneNumber.KeyboardType = KeyboardType.Number;
			drtbWechat.KeyboardType = KeyboardType.Email;

			viewLoading.Hidden = true;
			loadImage.Hidden = true;
			loadImage.StartAnimating();
			loadImage.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray;

		}

		private void InitBindCommand()
		{
			this.btnAddPicture.TouchDown += delegate {
				_captureImage.Hidden = false;
			};
			headerView.BackClicked	= new Action<object, EventArgs> ((object arg1, EventArgs arg2) => {
				View.EndEditing(true);
				ViewModel.BtnBackCliked.Execute (null);});

			this.BindCommand (
				ViewModel,
				vm => vm.UpdateProfileCommand,
				v => v.btnOk);
			this.BindCommand (
				ViewModel,
				vm => vm.BtnAddPictureCliked,
				v => v.btnAddPicture,
				() => new Action(delegate {
				}));
		}

		private void InitBindingValue()
		{
			//TODO Binding Title
			ViewModel.WhenAnyValue (x => x.ScreenTitle)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => headerView.Title = x);

			ViewModel.WhenAnyValue (vm => vm.IsAllPass)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => UpdateScrollView ());

			ViewModel
				.WhenAnyValue (vm => vm.Icon)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					if(!string.IsNullOrEmpty(x))
					{
//						DependencyService.Get<Unicorn.Core.UI.PopupManager>().ShowAlert("", x, Unicorn.Core.Enums.DialogType.Info);
						UIImage image = UIImage.FromFile(x);
						if(image != null)
						{
							RxApp.MainThreadScheduler.Schedule(() => imgPicture.Image = image);
						}
					}
					else{
//						DependencyService.Get<Unicorn.Core.UI.PopupManager>().ShowAlert("", "null", Unicorn.Core.Enums.DialogType.Info);
						imgPicture.Image = null;
					}
			});
			ViewModel.WhenAnyValue (x => x.NameModel.Text, x => x.SocialModel.Text, x => x.DOBModel.Text, x => x.PhoneNumberModel.Text, x => x.WechatModel.Text)
				.ObserveOn(RxApp.TaskpoolScheduler)
				.Subscribe(_ => ViewModel.CheckUserChanged.Execute(null));

			ViewModel
				.WhenAnyValue (vm => vm.IconUrl)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					if(!string.IsNullOrEmpty(x))
					{
						UIImage image = UIImage.LoadFromData(NSData.FromUrl(new NSUrl(x)));
						if(image != null)
						{
							imgPicture.Image = image;
						}
					}
					else
						imgPicture.Image = null;

				});

			ViewModel
				.WhenAnyValue (vm => vm.IconData)
				.Where(x => x != null)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					imgPicture.Image = x as UIImage;
					ViewModel.IsEnableButton = true;
				});

			ViewModel.WhenAnyValue (vm => vm.IsPictureLoading)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					loadImage.Hidden = !x;
					viewLoading.Hidden = !x;
			});
			
			ViewModel
				.WhenAnyValue (vm => vm.IsEnableButton)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => 
					{
						btnOk.Enabled = x;
					});

			//TODO: binding childs model.
			ViewModel
				.WhenAnyValue (vm => vm.NameModel)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => drtbName.ViewModel = x);
			ViewModel
				.WhenAnyValue (vm => vm.SocialModel)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => drtbSocial.ViewModel = x);
			ViewModel
				.WhenAnyValue (vm => vm.PhoneNumberModel)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => drtbPhoneNumber.ViewModel = x);
			ViewModel
				.WhenAnyValue (vm => vm.WechatModel)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => drtbWechat.ViewModel = x);
			ViewModel
				.WhenAnyValue (vm => vm.DOBModel)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => dpDOB.ViewModel = x);

		}
		private void UpdateScrollView()
		{
			svScrollView.ContentSize = new CGSize (svScrollView.Frame.Width, (svScrollView.Frame.Y + drtbWechat.Frame.Y + drtbWechat.Frame.Height));
		}
	}
}

