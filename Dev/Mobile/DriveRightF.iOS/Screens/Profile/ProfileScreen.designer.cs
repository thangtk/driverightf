// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("ProfileScreen")]
	partial class ProfileScreen
	{
		[Outlet]
		UIKit.UIButton btnAddPerson { get; set; }

		[Outlet]
		UIKit.UIButton btnAddPicture { get; set; }

		[Outlet]
		UIKit.UIButton btnOk { get; set; }

		[Outlet]
		DriveRightF.Core.UDatePicker dpDOB { get; set; }

		[Outlet]
		DriveRightF.Core.UI.UTextBox drtbName { get; set; }

		[Outlet]
		DriveRightF.Core.UI.UTextBox drtbPhoneNumber { get; set; }

		[Outlet]
		DriveRightF.Core.UI.UTextBox drtbSocial { get; set; }

		[Outlet]
		DriveRightF.Core.UI.UTextBox drtbWechat { get; set; }

		[Outlet]
		DriveRightF.iOS.TitleHeaderView headerView { get; set; }

		[Outlet]
		UIKit.UIImageView imgPicture { get; set; }

		[Outlet]
		UIKit.UILabel lblPerson { get; set; }

		[Outlet]
		UIKit.UILabel lblPicture { get; set; }

		[Outlet]
		UIKit.UIActivityIndicatorView loadImage { get; set; }

		[Outlet]
		UIKit.UIScrollView svScrollView { get; set; }

		[Outlet]
		UIKit.UIView viewLoading { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnAddPerson != null) {
				btnAddPerson.Dispose ();
				btnAddPerson = null;
			}

			if (btnAddPicture != null) {
				btnAddPicture.Dispose ();
				btnAddPicture = null;
			}

			if (btnOk != null) {
				btnOk.Dispose ();
				btnOk = null;
			}

			if (dpDOB != null) {
				dpDOB.Dispose ();
				dpDOB = null;
			}

			if (drtbName != null) {
				drtbName.Dispose ();
				drtbName = null;
			}

			if (drtbPhoneNumber != null) {
				drtbPhoneNumber.Dispose ();
				drtbPhoneNumber = null;
			}

			if (drtbSocial != null) {
				drtbSocial.Dispose ();
				drtbSocial = null;
			}

			if (drtbWechat != null) {
				drtbWechat.Dispose ();
				drtbWechat = null;
			}

			if (headerView != null) {
				headerView.Dispose ();
				headerView = null;
			}

			if (imgPicture != null) {
				imgPicture.Dispose ();
				imgPicture = null;
			}

			if (lblPerson != null) {
				lblPerson.Dispose ();
				lblPerson = null;
			}

			if (lblPicture != null) {
				lblPicture.Dispose ();
				lblPicture = null;
			}

			if (loadImage != null) {
				loadImage.Dispose ();
				loadImage = null;
			}

			if (svScrollView != null) {
				svScrollView.Dispose ();
				svScrollView = null;
			}

			if (viewLoading != null) {
				viewLoading.Dispose ();
				viewLoading = null;
			}
		}
	}
}
