﻿using System;
using DriveRightF.Core.Enums;
using Unicorn.Core.Navigator;
using DriveRightF.Core.Constants;
using DriveRightF.Core;

namespace DriveRightF.iOS
{
	public class ProfileTabScreen : BaseTabItemScreen
	{
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.Navigate ((int)Screen.Profile);
		}
		protected override void InitData(){
			base.InitData ();

			//			ViewModel = new AwardTabViewModel ();
			//			CoinViewModel 
		}
		public override void TabClick (){
			if (CurrentScreen != (int)Screen.Profile) {
				this.NavigateBack ((int)Screen.Profile,true);
			}
		}
	}
}

