// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("TripStatementScreen")]
	partial class TripStatementScreen
	{
		[Outlet]
		UIKit.UILabel avgDistance { get; set; }

		[Outlet]
		UIKit.UILabel avgDuration { get; set; }

		[Outlet]
		UIKit.UILabel avgSpeed { get; set; }

		[Outlet]
		UIKit.UIButton btnCanel { get; set; }

		[Outlet]
		UIKit.UIButton btnDetail { get; set; }

		[Outlet]
		UIKit.UIButton btnDetailInfo { get; set; }

		[Outlet]
		UIKit.UIButton btnSwipeCancel { get; set; }

		[Outlet]
		DriveRightF.iOS.ProgressScoreView distractionScore { get; set; }

		[Outlet]
		UIKit.UILabel lblAvgDistance { get; set; }

		[Outlet]
		UIKit.UILabel lblAvgDuration { get; set; }

		[Outlet]
		UIKit.UILabel lblAvgSpeed { get; set; }

		[Outlet]
		UIKit.UILabel lblDistraction { get; set; }

		[Outlet]
		UIKit.UILabel lblMessages { get; set; }

		[Outlet]
		UIKit.UILabel lblMonth { get; set; }

		[Outlet]
		UIKit.UILabel lblNoOfTrips { get; set; }

		[Outlet]
		UIKit.UILabel lblQQ { get; set; }

		[Outlet]
		UIKit.UILabel lblSmooth { get; set; }

		[Outlet]
		UIKit.UILabel lblSpeed { get; set; }

		[Outlet]
		UIKit.UILabel lblTimeOfDelay { get; set; }

		[Outlet]
		UIKit.UILabel lblTotalDistance { get; set; }

		[Outlet]
		UIKit.UILabel lblTotalDuration { get; set; }

		[Outlet]
		UIKit.UILabel lblTotalScore { get; set; }

		[Outlet]
		UIKit.UILabel lblWechat { get; set; }

		[Outlet]
		UIKit.UILabel lblWechatquan { get; set; }

		[Outlet]
		UIKit.UILabel lblWeibo { get; set; }

		[Outlet]
		UIKit.UILabel noOfTrips { get; set; }

		[Outlet]
		UIKit.UIScrollView scrollView { get; set; }

		[Outlet]
		DriveRightF.iOS.ProgressScoreView smoothScore { get; set; }

		[Outlet]
		DriveRightF.iOS.ProgressScoreView speedScore { get; set; }

		[Outlet]
		UIKit.UIView swipeView { get; set; }

		[Outlet]
		DriveRightF.iOS.ProgressScoreView timeOfDay { get; set; }

		[Outlet]
		UIKit.UILabel totalDistance { get; set; }

		[Outlet]
		UIKit.UILabel totalDuration { get; set; }

		[Outlet]
		DriveRightF.iOS.ProgressScoreView totalScore { get; set; }

		[Outlet]
		DriveRightF.iOS.TotalScoreBar totalScoreView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (avgDistance != null) {
				avgDistance.Dispose ();
				avgDistance = null;
			}

			if (avgDuration != null) {
				avgDuration.Dispose ();
				avgDuration = null;
			}

			if (avgSpeed != null) {
				avgSpeed.Dispose ();
				avgSpeed = null;
			}

			if (btnDetail != null) {
				btnDetail.Dispose ();
				btnDetail = null;
			}

			if (btnDetailInfo != null) {
				btnDetailInfo.Dispose ();
				btnDetailInfo = null;
			}

			if (btnSwipeCancel != null) {
				btnSwipeCancel.Dispose ();
				btnSwipeCancel = null;
			}

			if (distractionScore != null) {
				distractionScore.Dispose ();
				distractionScore = null;
			}

			if (lblAvgDistance != null) {
				lblAvgDistance.Dispose ();
				lblAvgDistance = null;
			}

			if (lblAvgDuration != null) {
				lblAvgDuration.Dispose ();
				lblAvgDuration = null;
			}

			if (lblAvgSpeed != null) {
				lblAvgSpeed.Dispose ();
				lblAvgSpeed = null;
			}

			if (lblDistraction != null) {
				lblDistraction.Dispose ();
				lblDistraction = null;
			}

			if (lblMonth != null) {
				lblMonth.Dispose ();
				lblMonth = null;
			}

			if (lblNoOfTrips != null) {
				lblNoOfTrips.Dispose ();
				lblNoOfTrips = null;
			}

			if (lblSmooth != null) {
				lblSmooth.Dispose ();
				lblSmooth = null;
			}

			if (lblSpeed != null) {
				lblSpeed.Dispose ();
				lblSpeed = null;
			}

			if (lblTimeOfDelay != null) {
				lblTimeOfDelay.Dispose ();
				lblTimeOfDelay = null;
			}

			if (lblTotalDistance != null) {
				lblTotalDistance.Dispose ();
				lblTotalDistance = null;
			}

			if (lblTotalDuration != null) {
				lblTotalDuration.Dispose ();
				lblTotalDuration = null;
			}

			if (lblTotalScore != null) {
				lblTotalScore.Dispose ();
				lblTotalScore = null;
			}

			if (noOfTrips != null) {
				noOfTrips.Dispose ();
				noOfTrips = null;
			}

			if (scrollView != null) {
				scrollView.Dispose ();
				scrollView = null;
			}

			if (smoothScore != null) {
				smoothScore.Dispose ();
				smoothScore = null;
			}

			if (speedScore != null) {
				speedScore.Dispose ();
				speedScore = null;
			}

			if (swipeView != null) {
				swipeView.Dispose ();
				swipeView = null;
			}

			if (timeOfDay != null) {
				timeOfDay.Dispose ();
				timeOfDay = null;
			}

			if (totalDistance != null) {
				totalDistance.Dispose ();
				totalDistance = null;
			}

			if (totalDuration != null) {
				totalDuration.Dispose ();
				totalDuration = null;
			}

			if (lblQQ != null) {
				lblQQ.Dispose ();
				lblQQ = null;
			}

			if (totalScore != null) {
				totalScore.Dispose ();
				totalScore = null;
			}

			if (lblMessages != null) {
				lblMessages.Dispose ();
				lblMessages = null;
			}

			if (btnCanel != null) {
				btnCanel.Dispose ();
				btnCanel = null;
			}

			if (totalScoreView != null) {
				totalScoreView.Dispose ();
				totalScoreView = null;
			}

			if (lblWeibo != null) {
				lblWeibo.Dispose ();
				lblWeibo = null;
			}

			if (lblWechat != null) {
				lblWechat.Dispose ();
				lblWechat = null;
			}

			if (lblWechatquan != null) {
				lblWechatquan.Dispose ();
				lblWechatquan = null;
			}
		}
	}
}
