// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("ScoreInformationViewController")]
	partial class ScoreInformationViewController
	{
		[Outlet]
		UIKit.UITextView txtInfor { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (txtInfor != null) {
				txtInfor.Dispose ();
				txtInfor = null;
			}
		}
	}
}
