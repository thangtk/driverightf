﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;
using ObjCRuntime;
using Unicorn.Core.iOS;
using CoreAnimation;

namespace DriveRightF.iOS
{
	[Register("ScoreView")]
	public partial class ScoreView : UIView
	{
		const double ANGLE = Math.PI * 1 / 2;

		public ScoreView(CGRect frame) : base(frame)
		{
			InitViews ();
		}

		public ScoreView(IntPtr handler) : base(handler)
		{
			InitViews ();
		}

		private void InitViews(){
			var arr = NSBundle.MainBundle.LoadNib("ScoreView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			v.Frame = new CGRect (CGPoint.Empty, Frame.Size);
			AddSubview (v);
			//
			ResetViews();
		}

		public void ResetViews(){
			seekView.Transform = CGAffineTransform.MakeRotation ((nfloat)(-ANGLE / 2));
		}

		private bool _isAnimating = false;
		public void BeginAnimation(double duration = 2){
			if (_isAnimating)
				return;
			//
//			System.Diagnostics.Debug.WriteLine("Score animation start: " + Score.ToString());
//			ResetViews ();
			if (Score == 0) {
				return;
			}
			_isAnimating = true;
//			double rate = Score / 100;
			UIView.Animate (duration, 0, UIViewAnimationOptions.CurveLinear,
				() => {
//					seekView.Transform = CGAffineTransform.MakeRotation((nfloat)(-ANGLE/2 + ANGLE*rate));
					UpdateSeekView (Score);
				},
				() => {
//					seekView.Transform = CGAffineTransform.MakeRotation((nfloat)(-ANGLE/2 + ANGLE*rate));
					UpdateSeekView (Score);
					_isAnimating = false;
				}
			);
		}

		private double _score;
		public double Score {
			get{ 
				return _score;
			}
			set{ 
				_score = value;
				lblScore.Text = value.ToString ();
			}
		}

		public void SetScoreWithAnimation(double score, bool animate = false, double duration = 0){
			Score = score;
			if (animate) {
				BeginAnimation (duration);
			}
		}

		private void UpdateSeekView(double score){
			double rate = score / 100;
			seekView.Transform = CGAffineTransform.MakeRotation((nfloat)(-ANGLE/2 + ANGLE*rate));
		}

		public string Name {
			get{ 
				return lblName.Text;
			}
			set{ 
				lblName.Text = value;
			}
		}
	}
}

