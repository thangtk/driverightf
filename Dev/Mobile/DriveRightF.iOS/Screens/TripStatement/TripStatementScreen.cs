﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using ReactiveUI;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using DriveRightF.Core.Models;
using DriveRightF.Core.ViewModels;
using Unicorn.Core;
using DriveRightF.Core.Enums;
using System.Timers;
using Unicorn.Core.Navigator;
using CoreGraphics;
using System.Diagnostics;

namespace DriveRightF.iOS
{
	public partial class TripStatementScreen : BaseHandlerHeaderViewController<TripStatementViewModel>
	{
		private TripMonth _trip;
		private SwipeControlView _swipeView;

		public TripStatementScreen () : base ("TripStatementScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void LoadData (params object[] objs)
		{
			base.LoadData (objs);
			//
			if (objs != null && objs.Length > 0) {
				_trip = objs [0] as TripMonth;
				if (ViewModel != null && _trip != null) {
					ViewModel.Month = _trip.Month;
					UpdateMonthLable (_trip.Month);
				}
			}
			System.Diagnostics.Debug.WriteLine ("xxxxxxxxxxxxxxxxxxxxxxxxxxx >>> LoadData");
		}

		public override void ReloadView ()
		{
			base.ReloadView ();

			RxApp.MainThreadScheduler.Schedule (() => {
				if (ViewModel != null) {
					ViewModel.HeaderViewModel.IsShowBtnInfo = true;
					ViewModel.HeaderViewModel.IsShowBtnShare = true;
				}

				if (_swipeView != null)
				{
					_swipeView.IsVisible = false;
					View.Window.AddSubview(_swipeView);
					View.Window.SendSubviewToBack(_swipeView);
				}
			});
		}

		private void UpdateMonthLable (DateTime xz)
		{
			if (xz.Date.CompareTo (new DateTime (9999, 1, 1)) >= 0)
				lblMonth.Text = Translator.Translate ("TRIPS_STATEMENT_OVERALL");
			else
				lblMonth.Text = Convert.ToDateTime (xz).ToString ("MMMM - yyyy");
		}

		private void ResetViewState ()
		{
			totalScore.SetValue (0, true);
			smoothScore.SetValue (0, true);
			distractionScore.SetValue (0, true);
			speedScore.SetValue (0, true);
			timeOfDay.SetValue (0, true);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			InitControls ();

			ViewModel = new TripStatementViewModel ();
			InitBindCommands ();
			RxApp.TaskpoolScheduler.Schedule (() => {
				InitBindings ();
			});

			lblMonth.Font = DependencyService.Get<FontManager> ().GetMainFont (15);

			distractionScore.TextColor = UIColor.White;
			smoothScore.TextColor = UIColor.White;
			speedScore.TextColor = UIColor.White;
			timeOfDay.TextColor = UIColor.White;
			totalScore.TextColor = UIColor.White;

			distractionScore.LabelTextFont = FontHub.HelveticaHvCn20;
			smoothScore.LabelTextFont = FontHub.HelveticaHvCn20;
			speedScore.LabelTextFont = FontHub.HelveticaHvCn20;
			timeOfDay.LabelTextFont = FontHub.HelveticaHvCn20;
			totalScore.LabelTextFont = FontHub.HelveticaHvCn30;

			totalScore.LoadingBackgroundColor = UIColor.White;
			distractionScore.LoadingBackgroundColor = UIColor.White;
			smoothScore.LoadingBackgroundColor = UIColor.White;
			speedScore.LoadingBackgroundColor = UIColor.White;
			timeOfDay.LoadingBackgroundColor = UIColor.White;

			_swipeView = new SwipeControlView (
				new CGRect (0, UIScreen.MainScreen.Bounds.Height - 30, 320, swipeView.Frame.Height),
				swipeView) {
				ShowButtonSwipe = false,
				AnimationDuration = 0.25f,
			};
			_swipeView.AnimationHideEnded += delegate 
			{ 
				View.Window.SendSubviewToBack(_swipeView);
			};
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);

			_swipeView.RemoveFromSuperview ();
//			ResetViewState ();
		}

		private void InitControls ()
		{
			InitTranslator ();

//			smoothScore.Name = Translator.Translate("TRIPS_STATEMENT_SMOOTH");
//			speedScore.Name = Translator.Translate("TRIPS_STATEMENT_SPEED");
//			distractionScore.Name = Translator.Translate("TRIPS_STATEMENT_DISTRACTION");
//			timeOfDelay.Name = Translator.Translate("TRIPS_STATEMENT_TIME_OF_DAY");
		}

		private void InitTranslator ()
		{
			lblSmooth.Text = Translator.Translate ("TRIPS_STATEMENT_SMOOTH");
			lblSpeed.Text = Translator.Translate ("TRIPS_STATEMENT_SPEED");
			lblDistraction.Text = Translator.Translate ("TRIPS_STATEMENT_DISTRACTION");
			lblTimeOfDelay.Text = Translator.Translate ("TRIPS_STATEMENT_TIME_OF_DAY");
			lblTotalScore.Text = Translator.Translate ("TRIPS_STATEMENT_TOTAL_SCORE");
			totalDistance.Text = Translator.Translate ("TRIPS_STATEMENT_TOTAL_DISTANCE");
			totalDuration.Text = Translator.Translate ("TRIPS_STATEMENT_TOTAL_DURATION");
			noOfTrips.Text = Translator.Translate ("TRIPS_STATEMENT_TOTAL_NO_OF_TRIPS");
			avgDistance.Text = Translator.Translate ("TRIPS_STATEMENT_TOTAL_AVG_DIS_TRIP");
			avgDuration.Text = Translator.Translate ("TRIPS_STATEMENT_TOTAL_AVG_DUR_TRIP");
			avgSpeed.Text = Translator.Translate ("TRIPS_STATEMENT_TOTAL_AVG_SPEED");

			lblWechat.Text = Translator.Translate("TRIPS_STATEMENT_WE_CHAT");
			lblWechatquan.Text = Translator.Translate("TRIPS_STATEMENT_WECHAT_QUAN");
			lblWeibo.Text = Translator.Translate("TRIPS_STATEMENT_WEIBO");
			lblMessages.Text = Translator.Translate("TRIPS_STATEMENT_MESSAGES");
			btnSwipeCancel.SetTitle(Translator.Translate("BUTTON_CANCEL"), UIControlState.Normal);
		}

		private void SetSubScore (ProgressScoreView sv, int score)
		{
			Debug.WriteLine(">>>> SetSubScore");
			bool animated = score != 0;
			sv.SetValue (score, !animated);
			if (animated) {
				sv.StartAnimating (100);
			}
		}

		private void InitBindCommands ()
		{
			btnDetailInfo.TouchUpInside += delegate {
				DependencyService.Get<IHomeTabNavigator> ().CurrentTab.Navigate ((int)Screen.Trips, new object[] { _trip });
				_swipeView.RemoveFromSuperview ();
			};

			btnDetail.TouchUpInside += (sender, e) => {
				DependencyService.Get<IHomeTabNavigator> ().CurrentTab.Navigate ((int)Screen.Trips, new object[] { _trip });
				_swipeView.RemoveFromSuperview ();
			};

			btnSwipeCancel.TouchUpInside += (sender, e) => {
				_swipeView.IsVisible = false;
			};

			ViewModel.HeaderViewModel.BtnInfoClickAction = () => {
				DependencyService.Get<IHomeTabNavigator> ().CurrentTab.Navigate ((int)Screen.TripStatementInfo);
				ViewModel.HeaderViewModel.IsShowBtnInfo = false;
				ViewModel.HeaderViewModel.IsShowBtnShare = false;
				_swipeView.RemoveFromSuperview ();
			};

			ViewModel.HeaderViewModel.BtnShareClickAction = () => {
				View.Window.BringSubviewToFront(_swipeView);
				_swipeView.IsVisible = true;
			};
		}

		private void InitBindings ()
		{
//			ViewModel = new TripStatementViewModel ();

			ViewModel.WhenAnyValue (xz => xz.TotalScore)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (xz => SetSubScore (totalScore, Convert.ToInt32 (xz)));
			ViewModel.WhenAnyValue (xz => xz.SmoothScore)
				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (xz => smoothScore.Score = xz);
				.Subscribe (xz => SetSubScore (smoothScore, Convert.ToInt32 (xz)));
			ViewModel.WhenAnyValue (xz => xz.SpeedScore)
				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (xz => speedScore.Score = xz);
				.Subscribe (xz => SetSubScore (speedScore, Convert.ToInt32 (xz)));
			ViewModel.WhenAnyValue (xz => xz.DistanceScore)
				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (xz => distractionScore.Score = xz);
				.Subscribe (xz => SetSubScore (distractionScore, Convert.ToInt32 (xz)));
			ViewModel.WhenAnyValue (xz => xz.TimeOfDelay)
				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (xz => timeOfDelay.Score = xz);
				.Subscribe (xz => SetSubScore (timeOfDay, Convert.ToInt32 (xz)));

//			// Binding text
//			this.OneWayBind (ViewModel, vm => vm.TotalDistance, v => v.lblTotalDistance.Text);
//			this.OneWayBind (ViewModel, vm => vm.TotalDuration, v => v.lblTotalDuration.Text);
//			this.OneWayBind (ViewModel, vm => vm.NoOfTrips, v => v.lblNoOfTrips.Text);
//			this.OneWayBind (ViewModel, vm => vm.AvgDistance, v => v.lblAvgDistance.Text);
//			this.OneWayBind (ViewModel, vm => vm.AvgDuration, v => v.lblAvgDuration.Text);
//			this.OneWayBind (ViewModel, vm => vm.AvgSpeed, v => v.lblAvgSpeed.Text);

			ViewModel.WhenAnyValue (vm => vm.TotalDistance).ObserveOn (RxApp.MainThreadScheduler).Subscribe (x => lblTotalDistance.Text = x);
			ViewModel.WhenAnyValue (vm => vm.TotalDuration).ObserveOn (RxApp.MainThreadScheduler).Subscribe (x => lblTotalDuration.Text = x);
			ViewModel.WhenAnyValue (vm => vm.NoOfTrips).ObserveOn (RxApp.MainThreadScheduler).Subscribe (x => lblNoOfTrips.Text = x);
			ViewModel.WhenAnyValue (vm => vm.AvgDistance).ObserveOn (RxApp.MainThreadScheduler).Subscribe (x => lblAvgDistance.Text = x);
			ViewModel.WhenAnyValue (vm => vm.AvgDuration).ObserveOn (RxApp.MainThreadScheduler).Subscribe (x => lblAvgDuration.Text = x);
			ViewModel.WhenAnyValue (vm => vm.AvgSpeed).ObserveOn (RxApp.MainThreadScheduler).Subscribe (x => lblAvgSpeed.Text = x);
		}
	}

}

