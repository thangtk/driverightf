﻿
using System;

using Foundation;
using UIKit;
using Unicorn.Core.UI;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;

namespace DriveRightF.iOS
{
	public partial class ScoreInformationViewController : BaseHandlerHeaderViewController<ScoreInformationViewModel>
	{
		public ScoreInformationViewController () : base ("ScoreInformationViewController", null)
		{
		}

		#region implemented abstract members of UBaseViewController

		public override void ReloadView ()
		{
			base.ReloadView();
		}

		public override void LoadData (params object[] objs)
		{
			base.LoadData();
		}

		public override bool IsShouldCache ()
		{
			return false;
		}

		#endregion

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			ViewModel = new ScoreInformationViewModel ();

			txtInfor.Text = Translator.Translate("SCORE_INFO");
		}
	}
}

