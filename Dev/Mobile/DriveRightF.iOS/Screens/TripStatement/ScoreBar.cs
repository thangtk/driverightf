﻿using System;
using UIKit;
using CoreGraphics;
using Unicorn.Core.iOS;
using System.Reactive.Linq;
using Foundation;

namespace DriveRightF.iOS
{
	[Register ("TotalScoreBar")]
	public class TotalScoreBar : UIView
	{
		UIImageView _seekView;

		public TotalScoreBar(CGRect frame) : base(frame)
		{
			InitViews ();
		}

		public TotalScoreBar(IntPtr handler) : base(handler)
		{
			InitViews ();
		}

		private void InitViews(){
			// Score bar
//			ScoreBar sb = new ScoreBar(new CGRect(0, Frame.Height/2, Frame.Width, Frame.Height/2));
			UIImageView sb = new UIImageView(new CGRect(0, Frame.Height/2, Frame.Width, Frame.Height/2));
			sb.Image = UIImage.FromBundle ("img_gradient_horizontal.png");
			this.AddSubview (sb);

			// Seek image
			_seekView = new UIImageView(new CGRect(-Frame.Height/2, 0, Frame.Height/2, Frame.Height/2));
			ResetSeekViewPosition ();
			_seekView.Image = UIImage.FromBundle ("icon_arrow_down.png");
			this.AddSubview (_seekView);
		}

		public void ResetSeekViewPosition(){
			_seekView.Center = new CGPoint (-Frame.Height / 2, 0);
		}

		public void BeginAnimation(double duration = 2){
//			_seekView.Center = new CGPoint (-Frame.Height / 2, 0);
			ResetSeekViewPosition();

			UIView.Animate (duration, 0, UIViewAnimationOptions.CurveLinear,
				() => {
					_seekView.Center = _seekPoint;
				},
				() => {
					_seekView.Center = _seekPoint;
				}
			);
		}

		CGPoint _seekPoint = new CGPoint();

		public double Score {
			set{ 
				var offsetX = Frame.Width * value / 100;
				_seekPoint.X = (nfloat)offsetX;
				_seekPoint.Y = _seekView.Center.Y;
			}
		}
	}

	public class ScoreBar : UIView
	{
		public ScoreBar (CGRect frame) : base(frame)
		{
			Layer.CornerRadius = frame.Height / 2;
			Layer.MasksToBounds = true;
		}

		public override void Draw (CGRect rect)
		{
			base.Draw (rect);
			//
			CGContext ctx = UIGraphics.GetCurrentContext();
			CGColorSpace space = CGColorSpace.CreateDeviceRGB ();
			CGGradient gradient = new CGGradient (space, new CGColor[]{UIColor.Red.CGColor, UIColor.Blue.CGColor, UIColor.Yellow.CGColor});
			CGPoint startPoint = new CGPoint (0,0);
			CGPoint endPoint = new CGPoint (rect.Width, 0);
			ctx.DrawLinearGradient (gradient, startPoint, endPoint, CGGradientDrawingOptions.DrawsBeforeStartLocation);
		}
	}
}

