﻿using System;
using DriveRightF.Core;

namespace DriveRightF.iOS
{
	public class NotificationTableSource : DriveRightTableViewSource<Notification, ItemNotificationViewCell>
	{
		public override bool AnimationEnabled {
			get { return base.AnimationEnabled; }
			set { base.AnimationEnabled = value; }
		}
		
		public NotificationTableSource () : base()
		{
		}
	}
}

