﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

namespace DriveRightF.iOS
{
	public partial class ItemNotificationViewCell : DriveRightTableViewCell<Notification, ItemNotificationViewModel> 
	{
		public static readonly UINib Nib = UINib.FromName ("ItemNotificationViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("ItemNotificationViewCell");

		public ItemNotificationViewCell (IntPtr handle) : base (handle)
		{
		}

		public static ItemNotificationViewCell Create ()
		{
			return (ItemNotificationViewCell)Nib.Instantiate (null, null) [0];
		}

		public override void UpdateCell (Notification item, int index, int count)
		{
			ViewModel.Item = item;
			vLine.Hidden = index == 0;
		}
		public override void Initialize ()
		{
			InitControls ();
			ViewModel = new ItemNotificationViewModel ();
			CreateBindingsAsync ();
		}

		private void InitControls(){

			lblDescription.Font = FontHub.HelveticaLt13;
			lblUpdateTime.Font = FontHub.HelveticaThin11;
		}

		private void CreateBindingsAsync ()
		{

			RxApp.TaskpoolScheduler.Schedule(()=>{
				InitBindings ();
			});
		}

		private void InitBindings()
		{

			ViewModel.WhenAnyValue (vm => vm.Description)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (description => {
					lblDescription.Text = description;
					lblDescription.SizeToFit();
			});

			ViewModel.WhenAnyValue (vm => vm.UpdateTime)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				lblUpdateTime.Text = x;
				//lblUpdateTime.SizeToFit();
			});

		//	this.OneWayBind (ViewModel, vm => vm.UpdateTime, v => v.lblUpdateTime.Text);

		}
	}
}

