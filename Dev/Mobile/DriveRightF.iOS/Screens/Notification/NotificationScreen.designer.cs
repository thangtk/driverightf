// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("NotificationScreen")]
	partial class NotificationScreen
	{
		[Outlet]
		UIKit.UIButton btnBack { get; set; }

		[Outlet]
		UIKit.UILabel lblHeader { get; set; }

		[Outlet]
		UIKit.UITableView tbNotifications { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (tbNotifications != null) {
				tbNotifications.Dispose ();
				tbNotifications = null;
			}

			if (lblHeader != null) {
				lblHeader.Dispose ();
				lblHeader = null;
			}

			if (btnBack != null) {
				btnBack.Dispose ();
				btnBack = null;
			}
		}
	}
}
