﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using CoreAnimation;
using CoreGraphics;

namespace DriveRightF.iOS
{
	public partial class NotificationScreen : DriveRightBaseViewController<NotificationViewModel>
	{
		private NotificationTableSource _tableSources;
		public NotificationScreen () : base ("NotificationScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			UIApplication.SharedApplication.StatusBarStyle = UIStatusBarStyle.Default;
			InitControls ();
			InitBindings ();
			// Perform any additional setup after loading the view, typically from a nib.
		}

		private void InitControls()
		{
			_tableSources = new NotificationTableSource ();
			_tableSources.AnimationEnabled = false;
			tbNotifications.Source = _tableSources;

			btnBack.TouchUpInside += (object sender, EventArgs e) => ViewModel.BtnBackClick.Execute(null);
		}

		private void InitBindings()
		{

			ViewModel = new NotificationViewModel ();
			ViewModel.NameScreen = Translator.Translate("NOTIFICATION");
//			ViewModel.Items = new System.Collections.Generic.List<Notification> () {
//				new Notification () {
//					Description = Translator.Translate("NOTIFICATION_DESC_1"),
//					UpdateTime = DateTime.UtcNow.AddDays (-10),
//				},
//				new Notification () {
//					Description =  Translator.Translate("NOTIFICATION_DESC_2"),
//					UpdateTime = DateTime.UtcNow.AddDays (-19),
//				}
//
//			};
//			Observable.Start (()=>{
				
//			},RxApp.TaskpoolScheduler);
			this.OneWayBind (ViewModel, vm => vm.NameScreen, v =>v.lblHeader.Text);
			RxApp.TaskpoolScheduler.Schedule (() => {
				ViewModel.WhenAnyValue (vm => vm.Items)
					.Where (x => x != null && x.Count > 0)
					.ObserveOn (RxApp.MainThreadScheduler)
					.Subscribe (items => {
						_tableSources.Items = items;
						tbNotifications.ReloadData();
					});
			});
		}

	}
}

