// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("ItemNotificationViewCell")]
	partial class ItemNotificationViewCell
	{
		[Outlet]
		UIKit.UILabel lblDescription { get; set; }

		[Outlet]
		UIKit.UILabel lblUpdateTime { get; set; }

		[Outlet]
		UIKit.UIView vLine { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (lblDescription != null) {
				lblDescription.Dispose ();
				lblDescription = null;
			}

			if (lblUpdateTime != null) {
				lblUpdateTime.Dispose ();
				lblUpdateTime = null;
			}

			if (vLine != null) {
				vLine.Dispose ();
				vLine = null;
			}
		}
	}
}
