// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("AwardDetailScreen")]
	partial class AwardDetailScreen
	{
		[Outlet]
		UIKit.UIButton btnQRCode { get; set; }

		[Outlet]
		UIKit.UIView contentView { get; set; }

		[Outlet]
		UIKit.UIImageView imgIcon { get; set; }

		[Outlet]
		UIKit.UIImageView imgPicture { get; set; }

		[Outlet]
		UIKit.UILabel lblAwardTitle { get; set; }

		[Outlet]
		UIKit.UILabel lblDescription { get; set; }

		[Outlet]
		UIKit.UILabel lblExpDate { get; set; }

		[Outlet]
		UIKit.UILabel lblExpiryText { get; set; }

		[Outlet]
		UIKit.UILabel lblSponsoredText { get; set; }

		[Outlet]
		UIKit.UIView overlay { get; set; }

		[Outlet]
		UIKit.UIScrollView scrollViewContent { get; set; }

		[Outlet]
		UIKit.UITableView tblViewContent { get; set; }

		[Outlet]
		UIKit.UIButton tnQRCode { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnQRCode != null) {
				btnQRCode.Dispose ();
				btnQRCode = null;
			}

			if (contentView != null) {
				contentView.Dispose ();
				contentView = null;
			}

			if (imgIcon != null) {
				imgIcon.Dispose ();
				imgIcon = null;
			}

			if (imgPicture != null) {
				imgPicture.Dispose ();
				imgPicture = null;
			}

			if (lblAwardTitle != null) {
				lblAwardTitle.Dispose ();
				lblAwardTitle = null;
			}

			if (lblDescription != null) {
				lblDescription.Dispose ();
				lblDescription = null;
			}

			if (lblExpDate != null) {
				lblExpDate.Dispose ();
				lblExpDate = null;
			}

			if (lblExpiryText != null) {
				lblExpiryText.Dispose ();
				lblExpiryText = null;
			}

			if (lblSponsoredText != null) {
				lblSponsoredText.Dispose ();
				lblSponsoredText = null;
			}

			if (overlay != null) {
				overlay.Dispose ();
				overlay = null;
			}

			if (scrollViewContent != null) {
				scrollViewContent.Dispose ();
				scrollViewContent = null;
			}

			if (tblViewContent != null) {
				tblViewContent.Dispose ();
				tblViewContent = null;
			}

			if (tnQRCode != null) {
				tnQRCode.Dispose ();
				tnQRCode = null;
			}
		}
	}
}
