﻿using System;
using Foundation;
using UIKit;
using DriveRightF.Core.Models;
using DriveRightF.Core;
using DriveRight.Core.ViewModels;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using CoreGraphics;
using DriveRightF.Core.Enums;
using ReactiveUI;
using System.Reactive.Linq;
using ZXing;
using ZXing.Common;
using Unicorn.Core.UI;
using DriveRightF.Core.iOS;
using Unicorn.Core.iOS;
using DriveRightF.Core.Managers;
using System.Reactive.Concurrency;
using DriveRightF.Core.Constants;

namespace DriveRightF.iOS
{
	public partial class AwardDetailScreen : BaseHandlerHeaderViewController<AwardDetailViewModel>
	{
		private string _awardId;
		private Award _award;
		private UILabel lblUsedDescription;
		private UICheckBox checkBoxIsUsed;
		private UILabel lblIsUsed;
		private iOSImageViewer _imageViewer;
		private UIActivityIndicatorView _imageLoadingView;

		public AwardDetailScreen ()
		{
		}

		public override void LoadData (params object[] objs)
		{
			base.LoadData (objs);
			var award = objs [0] as Award;
			_awardId = award.Id.ToString ();
			_award = award;
			_imageLoadingView = new UIActivityIndicatorView (new CGRect (0, 0, tnQRCode.Frame.Width, tnQRCode.Frame.Height));
			_imageLoadingView.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray;
			_imageLoadingView.Center = new CGPoint (tnQRCode.Frame.Width / 2, tnQRCode.Frame.Height / 2);
			_imageLoadingView.BackgroundColor = UIColor.Clear;
			_imageLoadingView.StartAnimating ();
			if (ViewModel != null) {
				ViewModel.LoadPerkCommand.Execute (_award);
			}

		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			ViewModel = new AwardDetailViewModel ();
//			ViewModel.LoadPerkCommand.Execute (_awardId);

//			lblAwardTitle.Font = FontHub.HelveticaMedium17;
//			lblDescription.Font = FontHub.HelveticaLt15;
//			lblSponsoredBy.Font = FontHub.HelveticaLt15;
//			lblExpDate.Font = FontHub.HelveticaLt15;

			InitializeControls ();
			RxApp.TaskpoolScheduler.Schedule (()=>{
				InitHandler ();
			});

			InitTranslator ();
//			SetFont ();
		}

		private void InitHandler ()
		{
			ViewModel
				.WhenAnyValue (vm => vm.CheckBoxIsUseLabel)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
				x => checkBoxIsUsed.Label = x);

			ViewModel
				.WhenAnyValue (vm => vm.ExpDateText)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
					x => lblExpiryText.Text = x);

			ViewModel
				.WhenAnyValue (vm => vm.SponsoredByText)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => lblSponsoredText.Text = x);

//			ViewModel
//				.WhenAnyValue (vm => vm.TxtAwardTitle)
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (
//				x => lblAwardTitle.Text = x);

			ViewModel
				.WhenAnyValue (vm => vm.TxtDescription)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
				x => {
					lblUsedDescription.Text = x;
					UpdateFrameOfControls (tnQRCode.Hidden);
				});

			ViewModel
				.WhenAnyValue (vm => vm.Used)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
				x => {
					checkBoxIsUsed.IsChecked = x;
				});

			ViewModel
				.WhenAnyValue (vm => vm.ImageUrl)
				.Where (x => !String.IsNullOrEmpty (x))
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => imgPicture.Image = UIImage.FromFile(x));
			
			ViewModel
				.WhenAnyValue (vm => vm.AwardBackgroundPicture)
				.Where (x => !String.IsNullOrEmpty (x))
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (
				x => imgPicture.Image = UIImage.FromFile (x));

			ViewModel
				.WhenAnyValue (vm => vm.AwardQRNumber)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Select (x => {
				if (String.IsNullOrEmpty (x)) {
					tnQRCode.Hidden = true;
					UpdateFrameOfControls(true);
					return null;
				}
				else
				{
					UpdateFrameOfControls(false);
					tnQRCode.Hidden = false;
					tnQRCode.AddSubview (_imageLoadingView);
					return x;
				}
			})
				.Where (x => !String.IsNullOrEmpty (x))
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Select (x => {
				var writer = new BarcodeWriter {
					Format = BarcodeFormat.QR_CODE,
					Options = new EncodingOptions {
						Height = 150,
						Width = 150,
					}
				};

				return  writer.Write (x);
			})
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				_imageLoadingView.RemoveFromSuperview ();
				tnQRCode.SetBackgroundImage (x, UIControlState.Normal);
			});
		}

		protected void InitializeControls ()
		{
			scrollViewContent.AddSubview (contentView);

			CGPoint contentOffset = scrollViewContent.ContentOffset;
			contentOffset.X = 0;
			contentOffset.Y = 0;
			scrollViewContent.SetContentOffset (contentOffset, false);

			tnQRCode.TouchUpInside += delegate(object sender, EventArgs e) {
				var image = ((UIButton)sender).CurrentBackgroundImage;
				if (image != null)
				{
					_imageViewer = new iOSImageViewer (image, UIScreen.MainScreen.Bounds);
					_imageViewer.Show ();
				}
			};

			checkBoxIsUsed = new UICheckBox ();
			checkBoxIsUsed.Font = UIFont.SystemFontOfSize (15);
		
			checkBoxIsUsed.TextColor = UIColor.FromRGB (154, 154, 154);
			checkBoxIsUsed.OnTouch += OnIsUsedCheckChanged;

			lblUsedDescription = new UILabel (CGRect.Empty);
			lblUsedDescription.TextColor = UIColor.FromRGB (154, 154, 154);
			lblUsedDescription.Font = UIFont.SystemFontOfSize (14);
			lblUsedDescription.TextAlignment = UITextAlignment.Justified;
			lblUsedDescription.Lines = 30;

			contentView.AddSubviews (checkBoxIsUsed, lblUsedDescription);
		}

		private void UpdateFrameOfControls(bool hidden)
		{
			lblUsedDescription.Frame = new CGRect (0, hidden == true ? 20f : tnQRCode.Frame.Bottom + 1f, View.Frame.Width - 10f, lblUsedDescription.Text.Length / 2.4f + 20f);
			checkBoxIsUsed.Frame = new CGRect (0, lblUsedDescription.Frame.Bottom + 5f, scrollViewContent.Frame.Width, 25);

			CGSize contentSize = scrollViewContent.ContentSize;
			contentSize.Width = contentView.Frame.Width;
			contentSize.Height = checkBoxIsUsed.Frame.Bottom + 20f;
			scrollViewContent.ContentSize = contentSize;
		}

		private void OnIsUsedCheckChanged (object sender, EventArgs e)
		{
			_award.Used = checkBoxIsUsed.IsChecked;
			DependencyService.Get<AwardManager> ().Save (_award);

			MessagingCenter.Send<Award>(_award,MessageConstant.AWARD_IS_USED);
		}

		private void InitTranslator()
		{
//			lblSponsoredBy.Text = Translator.Translate("AWARD_DETAILS_SPONSOR_BY");
			lblExpDate.Text = Translator.Translate("AWARD_DETAILS_EXPIRY_DATE");
		}

		private void SetFont()
		{
//			lblSponsoredBy.Font = FontHub.HelveticaHvCn18;
//			lblExpDate.Font = FontHub.HelveticaHvCn18;
//			lblSponsoredText.Font = FontHub.HelveticaCn14;
//			lblExpiryText.Font = FontHub.HelveticaCn14;
		}
	}
}

