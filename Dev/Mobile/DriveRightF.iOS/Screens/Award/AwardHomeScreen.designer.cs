// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("AwardHomeScreen")]
	partial class AwardHomeScreen
	{
		[Outlet]
		UIKit.UIView _containerView { get; set; }

		[Outlet]
		UIKit.UIScrollView _scrollView { get; set; }

		[Outlet]
		DriveRightF.iOS.AwardSquareItem bigItem { get; set; }

		[Outlet]
		DriveRightF.iOS.AwardSquareItem item1 { get; set; }

		[Outlet]
		DriveRightF.iOS.AwardSquareItem item10 { get; set; }

		[Outlet]
		DriveRightF.iOS.AwardSquareItem item11 { get; set; }

		[Outlet]
		DriveRightF.iOS.AwardSquareItem item12 { get; set; }

		[Outlet]
		DriveRightF.iOS.AwardSquareItem item13 { get; set; }

		[Outlet]
		DriveRightF.iOS.AwardSquareItem item14 { get; set; }

		[Outlet]
		DriveRightF.iOS.AwardSquareItem item2 { get; set; }

		[Outlet]
		DriveRightF.iOS.AwardSquareItem item3 { get; set; }

		[Outlet]
		DriveRightF.iOS.AwardSquareItem item4 { get; set; }

		[Outlet]
		DriveRightF.iOS.AwardSquareItem item5 { get; set; }

		[Outlet]
		DriveRightF.iOS.AwardSquareItem item6 { get; set; }

		[Outlet]
		DriveRightF.iOS.AwardSquareItem item7 { get; set; }

		[Outlet]
		DriveRightF.iOS.AwardSquareItem item8 { get; set; }

		[Outlet]
		DriveRightF.iOS.AwardSquareItem item9 { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (_containerView != null) {
				_containerView.Dispose ();
				_containerView = null;
			}

			if (_scrollView != null) {
				_scrollView.Dispose ();
				_scrollView = null;
			}

			if (bigItem != null) {
				bigItem.Dispose ();
				bigItem = null;
			}

			if (item1 != null) {
				item1.Dispose ();
				item1 = null;
			}

			if (item10 != null) {
				item10.Dispose ();
				item10 = null;
			}

			if (item11 != null) {
				item11.Dispose ();
				item11 = null;
			}

			if (item12 != null) {
				item12.Dispose ();
				item12 = null;
			}

			if (item13 != null) {
				item13.Dispose ();
				item13 = null;
			}

			if (item14 != null) {
				item14.Dispose ();
				item14 = null;
			}

			if (item2 != null) {
				item2.Dispose ();
				item2 = null;
			}

			if (item3 != null) {
				item3.Dispose ();
				item3 = null;
			}

			if (item4 != null) {
				item4.Dispose ();
				item4 = null;
			}

			if (item5 != null) {
				item5.Dispose ();
				item5 = null;
			}

			if (item6 != null) {
				item6.Dispose ();
				item6 = null;
			}

			if (item7 != null) {
				item7.Dispose ();
				item7 = null;
			}

			if (item8 != null) {
				item8.Dispose ();
				item8 = null;
			}

			if (item9 != null) {
				item9.Dispose ();
				item9 = null;
			}
		}
	}
}
