﻿using System;
using DriveRightF.Core;
using CoreGraphics;
using DriveRightF.Core.ViewModels;
using System.Drawing;
using DriveRightF.Core.Enums;
using Unicorn.Core;
using System.Collections.Generic;
using ReactiveUI;
using System.Reactive.Linq;
using System.Linq;
using UIKit;
using System.Reactive.Concurrency;
using System.Threading.Tasks;

namespace DriveRightF.iOS
{
	public class SortAwardsViewItem : IComparer<AwardSquareItem>
	{
		#region IComparer implementation

		public int Compare(AwardSquareItem x, AwardSquareItem y)
		{
			AwardSquareItem a = x as AwardSquareItem;
			AwardSquareItem b = y as AwardSquareItem;

			return new SortAwardsViewModel ().Compare(a.ViewModel, b.ViewModel);

		}

		#endregion
	}

	public class AwardScreen : BaseMetroScreen<AwardTileViewModel> // BaseHandlerHeaderViewController<AwardListViewModel>
	{
		private List<AwardSquareItem> _cells = new List<AwardSquareItem> ();

		private List<AwardSquareItem> Cells
		{
			get
			{

				return _cells;
			}
		}

		#region implemented abstract members of BaseMetroScreen

		protected override void CreateViewModel()
		{
			ViewModel = new AwardListViewModel ();
			Task.Delay (30).Wait ();
			InitBindings ();
//			RxApp.TaskpoolScheduler.Schedule(InitBindings);
//			RxApp.MainThreadScheduler.Schedule (() => {
//				ViewModel = new AwardListViewModel ();
//				InitBindings();
//			});
		}

		private void InitBindings()
		{
			(ViewModel as AwardListViewModel).WhenAnyValue(v => v.RemoveCell).Where(x => x != null).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x =>
			{
				AwardSquareItem cell = Cells [x.Index];
				Cells.RemoveAt(x.Index);

				cell.RemoveFromSuperview();

				cell = (AwardSquareItem)CreatTileView(ViewModel.TileViewModels.Last(), false);
				_contentView.Add(cell);
				ReUpdateCellUI();
			});
			(ViewModel as AwardListViewModel).WhenAnyValue(v => v.LastestCell).Where(x => x != null).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x =>
			{
				AwardSquareItem cell = Cells [x.Index];
				for(int i = x.Index; i < x.NextIndex; i++)
				{
					Cells [i] = Cells [i + 1];
				}
				Cells [x.NextIndex] = cell;
				ReUpdateCellUI();
			});
			(ViewModel as AwardListViewModel).WhenAnyValue(v => v.UpdateCell).Where(x => x != null)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(x =>
			{
				Cells.Sort(new SortAwardsViewItem ());
				Cells.Reverse();
				ReUpdateCellUI();
			});
		}

		private void ReUpdateCellUI()
		{
			var contentSize = new Size ((int)View.Frame.Width, (int)View.Frame.Height);
			ViewModel.ComputeTileFrame(contentSize);
			for(int i = 0; i < ViewModel.TileViewModels.Count; i++)
			{

				AwardTileViewModel tile = ViewModel.TileViewModels [i];

				Cells [i].IsUsed = tile.Award.Used;
				Cells [i].Frame = new CGRect (tile.Position.X, tile.Position.Y, tile.Size.Width, tile.Size.Height);

				if(i == 0)
				{
					if(Cells [i].Type == AwardType.CardSmall) Cells [i].Type = AwardType.CardLarge;
					else if(Cells [i].Type == AwardType.AwardSmall) Cells [i].Type = AwardType.AwardLarge;
				}
				else
				{
					if(Cells [i].Type == AwardType.AwardLarge) Cells [i].Type = AwardType.AwardSmall;
					else if(Cells [i].Type == AwardType.CardLarge) Cells [i].Type = AwardType.CardSmall;
				}

			}

			(ViewModel as AwardListViewModel).UpdateColors();
		}

		protected override UIView CreatTileView(AwardTileViewModel tile, bool isBigTile)
		{
			var award = new AwardSquareItem (tile) {
				Frame = new CGRect (tile.Position.X, tile.Position.Y, tile.Size.Width, tile.Size.Height),
				IsUsed = tile.Award.Used,
			};
//			award.ViewModel = tile;
			if(isBigTile)
			{
				if(tile.Award.Type == "IDA") award.Type = AwardType.AwardLarge;
				else award.Type = AwardType.CardLarge;
			}
			else
			{
				if(tile.Award.Type == "IDA") award.Type = AwardType.AwardSmall;
				else award.Type = AwardType.CardSmall;
			}
			award.OnClick += (sender, e) =>
			{
				//TODO: send index click, how to check index of award in list award
				ViewModel.CurrentIndex = Cells.FindIndex(x => x == award);
				if(award.ViewModel != null && award.ViewModel.Award != null && !string.IsNullOrEmpty(award.ViewModel.Award.Name))
				{
					switch (award.Type)
					{
						case AwardType.AwardLarge:
						case AwardType.AwardSmall:
						Navigator.Navigate((int)Screen.AwardDetail, objs: new object[] {
								award.ViewModel.Award
							}, transitionParam: new object[] {
								award
							});
							break;
						case AwardType.CardLarge:
						case AwardType.CardSmall:
						Navigator.Navigate((int)Screen.CardDetail, objs: new object[] {
								award.ViewModel.Award
							}, transitionParam: new object[] {
								award
							});
							break;
						default:
							break;
					}
				}
			};
			_cells.Add(award);
			return award;
		}

		#endregion

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			MyDebugger.ShowCost("Award Screen >>> ViewDidAppear - done");
		}
	}
}

