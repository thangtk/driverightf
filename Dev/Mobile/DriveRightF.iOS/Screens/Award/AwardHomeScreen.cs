﻿
using System;
using System.Drawing;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using System.Collections.Generic;
using DriveRightF.Core.Enums;

namespace DriveRightF.iOS
{
	public partial class AwardHomeScreen : BaseMetroViewController<AwardTileViewModel,AwardSquareItem>
	{
		private List<AwardSquareItem> _listView;
		public AwardHomeScreen () : base ("AwardHomeScreen", null)
		{

		}

		public override void LoadView(){
			base.LoadView();
			_scrollView.ContentSize = _containerView.Frame.Size;
		}

		public override void ViewDidLoad()
		{
			_listView = new List<AwardSquareItem> (15);
			_listView.Add (bigItem);
			_listView.Add (item1);
			_listView.Add (item2);
			_listView.Add (item3);
			_listView.Add (item4);
			_listView.Add (item5);
			_listView.Add (item6);
			_listView.Add (item7);
			_listView.Add (item8);
			_listView.Add (item9);
			_listView.Add (item10);
			_listView.Add (item11);
			_listView.Add (item12);
			_listView.Add (item13);
			_listView.Add (item14);
			_scrollView.ContentSize = _containerView.Frame.Size;
			base.ViewDidLoad ();
//			((AwardHomeViewModel)ViewModel).UpdateColors ();
		}

		protected override UIScrollView ScrollView { get{ return _scrollView; }}
		protected override List<AwardSquareItem> ListView { get{ return _listView; }}
		protected override UIView ContainerView { get{ return _containerView; }}

		protected override void CreateViewModel(){
			ViewModel = new AwardHomeViewModel ();
//			InitBindings ();
		}

		protected override void ApplyView (AwardTileViewModel model, AwardSquareItem view, bool isBigTile){
			view.ViewModel = model;
//			view.BackgroundColor = ColorHub.ColorFromHex(model.Colors);
//			view.Alpha = 0.5f;
//			view.
			if (model.Award != null) {
				view.Award = model.Award;			}
			if (isBigTile) {
				if (model.Award.Type == "IDA")
					view.Type = AwardType.AwardLarge;
				else
					view.Type = AwardType.CardLarge;
			} else {
				if (model.Award.Type == "IDA")
					view.Type = AwardType.AwardSmall;
				else
					view.Type = AwardType.CardSmall;
			}
		}

	}
}

