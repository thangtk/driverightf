﻿using System;
using DriveRightF.Core.Enums;
using Unicorn.Core.Navigator;
using DriveRightF.Core.Constants;
using DriveRightF.Core;

namespace DriveRightF.iOS
{
	public class AwardTabScreen : BaseTabItemScreen
	{
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.Navigate ((int)Screen.HomeAward);
		}
		protected override void InitData(){
			base.InitData ();

//			ViewModel = new AwardTabViewModel ();
//			CoinViewModel 
		}
		public override void TabClick (){
			if (CurrentScreen != (int)Screen.HomeAward) {
				this.NavigateBack ((int)Screen.HomeAward,true);
			}
		}
	}
}

