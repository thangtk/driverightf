// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	partial class AwardHomeContentView
	{
		[Outlet]
		DriveRightF.iOS.AwardTabBar vTabAward { get; set; }

		[Outlet]
		UIKit.UIView vTabContent { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (vTabAward != null) {
				vTabAward.Dispose ();
				vTabAward = null;
			}

			if (vTabContent != null) {
				vTabContent.Dispose ();
				vTabContent = null;
			}
		}
	}
}
