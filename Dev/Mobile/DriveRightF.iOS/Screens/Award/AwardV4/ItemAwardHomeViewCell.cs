﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using Unicorn.Core;
using ReactiveUI;
using DriveRightF.Core.Enums;
using Unicorn.Core.Navigator;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

namespace DriveRightF.iOS
{
	public partial class ItemAwardHomeViewCell : DriveRightTableViewCell<Award,ItemAwardHomeViewModel>
	{
		public static readonly UINib Nib = UINib.FromName("ItemAwardHomeViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("ItemAwardHomeViewCell");

		public ItemAwardHomeViewCell(IntPtr handle) : base(handle)
		{
		}

		public static ItemAwardHomeViewCell Create()
		{
			return (ItemAwardHomeViewCell)Nib.Instantiate(null, null) [0];
		}

		#region implemented abstract members of DriveRightTableViewCell

		public override void UpdateCell(Award item, int index, int count)
		{
			ViewModel.Award = item;
		}

		public override void Initialize()
		{
			InitControls();
			ViewModel = new ItemAwardHomeViewModel ();
			CreateBindingsAsync();

			AddGestureRecognizer(new UITapGestureRecognizer (() =>
			{
				if(ViewModel.Award.Type == "IDA")
				{
					DependencyService.Get<IHomeTabNavigator>().CurrentTab.Navigate((int)Screen.AwardDetail, objs: new object[] {
						ViewModel.Award
					});				
				}
				else
				{
					DependencyService.Get<IHomeTabNavigator>().CurrentTab.Navigate((int)Screen.CardDetail, objs: new object[] {
						ViewModel.Award
					});				
				}
			}));
		}


		private void InitControls()
		{

			//			lblDescription.Font = FontHub.HelveticaLt13;
			//			lblUpdateTime.Font = FontHub.HelveticaThin11;
		}


		private void CreateBindingsAsync()
		{
			RxApp.TaskpoolScheduler.Schedule(() =>
			{
				InitBindings();
			});
		}

		private void InitBindings()
		{
			ViewModel.WhenAnyValue(x => x.ExpDays)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(x =>
			{
				lblAwardExpire.Text = x;
			});

			ViewModel.WhenAnyValue(x => x.NameAward)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(x =>
			{
				lblAwardName.Text = x;
			});
			
			ViewModel.WhenAnyValue(x => x.IconAward)
				.Where(x => !string.IsNullOrEmpty(x))
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(x =>
			{
				imgAwardIcon.Image = UIImage.FromFile(x);
			});

		}


		#endregion
	}
}

