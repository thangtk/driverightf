﻿using System;
using DriveRightF.Core;
using Unicorn.Core.iOS;

namespace DriveRightF.iOS
{
	public class AwardHomeScreen : BaseHomeListScreen<Award,ItemAwardHomeViewCell>
	{
		public AwardHomeScreen () : base()
		{
		}

		#region implemented abstract members of BaseHomeListScreen

		public override void CreatViewModel ()
		{
			ViewModel = new AwardHomeViewModel ();
		}

		public override void CreateViewContent ()
		{
			AwardHomeContentViewModel model = (ViewModel as AwardHomeViewModel).AwardContentViewModel;
			AwardHomeContentView contentView = new AwardHomeContentView (model);
			contentView.FixSupperView (ContentView);
		}

		#endregion
	}
}

