﻿using System;
using DriveRightF.Core;

namespace DriveRightF.iOS
{
	public class AwardHomeUsedTabView : HomeListContentView<Award, ItemAwardHomeViewCell>
	{
		public AwardHomeUsedTabView (AwardTabUsedViewModel viewModel):base(viewModel)
		{
		}

		public override void UpdateUI ()
		{
			TableView.RowHeight = 70;
		}
	}
}

