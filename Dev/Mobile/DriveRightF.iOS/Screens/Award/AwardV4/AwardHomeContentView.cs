﻿using System;
using DriveRightF.Core;
using Foundation;
using System.Collections.Generic;
using ObjCRuntime;
using UIKit;
using Unicorn.Core.iOS;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

namespace DriveRightF.iOS
{
	[Register("AwardHomeContentView")]
	public partial class AwardHomeContentView : DriveRightBaseView<AwardHomeContentViewModel>
//	HomeListContentView<Award, ItemAwardHomeViewCell>
	{
		private AwardHomeTabView _tabNewView;
		private AwardHomeUsedTabView _tabUsedView;
		public AwardHomeContentView (AwardHomeContentViewModel model)
		{
			LoadNib ();

			ViewModel = model;
			InitControls ();
			HandleBinding ();
		}


		private void LoadNib ()
		{
			var nibObjects = NSBundle.MainBundle.LoadNib ("AwardHomeContentView", this, null);
			var v = Runtime.GetNSObject ((nibObjects.ValueAt (0))) as UIView;
			v.FixSupperView (this);
		}

		private void HandleBinding()
		{
			
		}

		private void CreateTabContent ()
		{
			var usedViewModel = (ViewModel as AwardHomeContentViewModel).AwardTabUsedViewModel;
			_tabUsedView = new AwardHomeUsedTabView (usedViewModel);
			_tabUsedView.FixSupperView (vTabContent);
			var newViewModel = (ViewModel as AwardHomeContentViewModel).AwardTabViewModel;
			_tabNewView = new AwardHomeTabView (newViewModel);
			_tabNewView.FixSupperView (vTabContent);
		}

		private void InitControls()
		{
			RxApp.MainThreadScheduler.Schedule (() => CreateTabContent ());
//			CreateTabContent ();


			AwardTabBarViewModel tabBarViewModel = new AwardTabBarViewModel () {
				ItemSelected = 0,
			};
			vTabAward.ViewModel = tabBarViewModel;
			List<AwardTabItem> items = new List<AwardTabItem> {
				new AwardTabItem (tabBarViewModel.ItemModels[0]),
				new AwardTabItem (tabBarViewModel.ItemModels[1]),		
			};

			vTabAward.AddTabItems (items);

			vTabAward.OnTabSelected += (object sender, int e) => 
			{
				OnSelectedTab(e);
			};
		}

		public void OnSelectedTab(int index)
		{
			switch(index)
			{
			case 0:
				_tabNewView.Hidden = false;
				_tabUsedView.Hidden = true;
				break;
			case 1:
				_tabNewView.Hidden = true;
				_tabUsedView.Hidden = false;
				break;
			}
		}
	}
}


