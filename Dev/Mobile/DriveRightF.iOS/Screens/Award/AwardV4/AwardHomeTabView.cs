﻿using System;
using DriveRightF.Core;

namespace DriveRightF.iOS
{
	public class AwardHomeTabView : HomeListContentView<Award, ItemAwardHomeViewCell>
	{
		public AwardHomeTabView (AwardTabViewModel viewModel) : base(viewModel)
		{
		}

		public override void UpdateUI ()
		{
			TableView.RowHeight = 70;
		}
	}
}

