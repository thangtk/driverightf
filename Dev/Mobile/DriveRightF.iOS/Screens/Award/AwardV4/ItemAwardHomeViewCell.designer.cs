// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("ItemAwardHomeViewCell")]
	partial class ItemAwardHomeViewCell
	{
		[Outlet]
		UIKit.UIImageView imgAwardIcon { get; set; }

		[Outlet]
		UIKit.UILabel lblAwardExpire { get; set; }

		[Outlet]
		UIKit.UILabel lblAwardName { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (imgAwardIcon != null) {
				imgAwardIcon.Dispose ();
				imgAwardIcon = null;
			}

			if (lblAwardName != null) {
				lblAwardName.Dispose ();
				lblAwardName = null;
			}

			if (lblAwardExpire != null) {
				lblAwardExpire.Dispose ();
				lblAwardExpire = null;
			}
		}
	}
}
