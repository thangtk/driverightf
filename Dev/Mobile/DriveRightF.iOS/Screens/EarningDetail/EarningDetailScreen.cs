﻿using System;
using System.Drawing;
using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRight.Core.ViewModels;
using CoreGraphics;
using System.Collections.Generic;
using ReactiveUI;
using DriveRightF.Core.ViewModels;
using System.Reactive.Linq;
using DriveRightF.Core.iOS;
using Unicorn.Core.iOS;

namespace DriveRightF.iOS
{
	public partial class EarningDetailScreen : DriveRightBaseViewController<EarningDetailViewModel>
	{
		private bool _isFirstTime = true;
		private RadialProgressView _progress;
		private List<UIImageView> _listAwards;
		private List<Tuple<string, bool>> _iconList;
		private string _earningId;
		private List<UIImageView> _listAwardLayer;

		public EarningDetailScreen ()
		{
		}

		public EarningDetailScreen (string earningId)
		{
			_earningId = earningId;
		}

		public override void LoadData (params object[] objs)
		{
			base.LoadData (objs);
			_earningId = objs [0].ToString ();
			if (ViewModel != null) {
				ViewModel.LoadEarningCommand.Execute (_earningId);
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			ViewModel = new EarningDetailViewModel ();
			ViewModel.LoadEarningCommand.Execute (_earningId);

			_listAwards = new List<UIImageView> () {
				viewAward1, viewAward2, viewAward3, viewAward4, viewAward5, 
				viewAward6, viewAward7, viewAward8, viewAward9, viewAward10
			};
				
			_listAwardLayer = new List<UIImageView> ();

			InitCommand ();
		}

		private void InitCommand ()
		{
			ViewModel
				.WhenAnyValue (vm => vm.IconList) 
				.Where (x => x != null)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (iconList => {
				for (int i = 0; i < iconList.Count; i++) {
					if (iconList [i].Item2) {
						_listAwards [i].Image = (UIImage.FromFile (iconList [i].Item1));
//							_listAwardLayer[i].Transform = CGAffineTransform.MakeIdentity();
//							UIView.Animate(
//								10,
//								0.7,
//								UIViewAnimationOptions.CurveEaseInOut,
//								() => {
//									_listAwardLayer[i].Transform = CGAffineTransform.MakeRotation((nfloat) Math.PI * 799);
//								},
//								() => {
//								});
					} else {
							
						_listAwards [i].Image = (GrayScaleViewUtil.ConvertToGrayScale (UIImage.FromFile (iconList [i].Item1)));
					}
					UIView.Animate (
						0.2,
						0.2 + i * 0.1f,
						UIViewAnimationOptions.CurveEaseInOut,
						() => {
							_listAwards [i].Transform = CGAffineTransform.MakeIdentity ();
						},
						() => {
						});
				}
			});
	

			ViewModel
				.WhenAnyValue (vm => vm.Icon)
				.Where (x => !String.IsNullOrEmpty (x))
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x =>
					headerView.MainImage = UIImage.FromFile (x));
			ViewModel
				.WhenAnyValue (vm => vm.Image)
				.Where (x => !String.IsNullOrEmpty (x))
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => imgCover.Image = UIImage.FromFile (x));
			ViewModel
				.WhenAnyValue (vm => vm.IconUrl)
				.Where (x => !String.IsNullOrEmpty (x))
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Select (iconUrl => {
				return UIImage.LoadFromData (NSData.FromUrl (new NSUrl (iconUrl)));
			}).ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (image => headerView.MainImage = image);
			ViewModel
				.WhenAnyValue (vm => vm.ImageUrl)
				.Where (x => !String.IsNullOrEmpty (x))
				.ObserveOn (RxApp.MainThreadScheduler)
				.Select (imageUrl => {
				return UIImage.LoadFromData (NSData.FromUrl (new NSUrl (imageUrl)));
			}).ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (image => imgCover.Image = image);
			ViewModel
				.WhenAnyValue (vm => vm.Archieve)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => lblAchieve.Text = x);
			ViewModel
				.WhenAnyValue (vm => vm.Get)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => lblGet.Text = x);
			ViewModel
				.WhenAnyValue (vm => vm.StartTime)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => lblBegin.Text = x);
			ViewModel
				.WhenAnyValue (vm => vm.Progress)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				_progress.SetValue (x);
			});
			ViewModel
				.WhenAnyValue (vm => vm.Description)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => lblDescription.Text = x);
			ViewModel
				.WhenAnyValue (vm => vm.Name)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => lblTitle.Text = x);
		}

		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();

			if (_isFirstTime) {
				InitViews ();
				_isFirstTime = false;
			}
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			_progress.StartAnimating ();
		}

		private void InitViews ()
		{
			FrameExtension.ChangeFrameAttributes (
				viewProgress, 
				x: 300 - (float)viewProgress.Frame.Height,
				width: (float)viewProgress.Frame.Height);
			viewProgress.Layer.CornerRadius = viewProgress.Frame.Height / 2;

			SetFont ();
			RepositionAwards ();

			_progress = new RadialProgressView (new CGPoint (5, 5), viewProgress.Frame.Width - 10);
			_progress.FillStroke = 10;

			_progress.LabelTextFont = FontHub.RobotoBold20;

			viewProgress.AddSubview (_progress);

			foreach (var image in _listAwards) {
				var layerImage = new UIImageView (new CGRect (new CGPoint (0, 0), new CGSize (image.Frame.Width, image.Frame.Height)));
				layerImage.Center = image.Center;
				layerImage.BackgroundColor = UIColor.Black.ColorWithAlpha (0.2f);
				layerImage.Layer.CornerRadius = image.Layer.CornerRadius;


				_listAwardLayer.Add (layerImage);
				image.Superview.AddSubview (layerImage);
				image.Superview.BringSubviewToFront (layerImage);
				image.Superview.BringSubviewToFront (image);
			}

			foreach (var image in _listAwards)
				image.Transform = CGAffineTransform.MakeScale (0.01f, 0.01f);
		}

		private void SetFont ()
		{
			lblTitle.Font = FontHub.RobotoBold17;
			lblDescription.Font = DeviceUtil.IsiPhone5 ? FontHub.RobotoRegular15 : FontHub.RobotoRegular12;
			lblAchieve.Font = FontHub.RobotoRegular15;
			lblArchieveTitle.Font = FontHub.RobotoRegular15;
			lblGet.Font = FontHub.RobotoRegular15;
			lblGetTitle.Font = FontHub.RobotoRegular15;
			lblBegin.Font = FontHub.RobotoRegular15;
			lblBeginTitle.Font = FontHub.RobotoRegular15;

			lblProgress.Font = FontHub.MyriadProRegular22;
			lblPrePerked.Font = FontHub.MyriadProRegular15;
		}

		private void RepositionAwards ()
		{
			if (DeviceUtil.IsiPhone5)
				for (int i = 0; i < 5; i++) {
					FrameExtension.ChangeFrameAttributes (view: _listAwards [i], y: (float)_listAwards [i].Frame.Y + 10);
				}
			else
				for (int i = 5; i < 10; i++) {
					FrameExtension.ChangeFrameAttributes (view: _listAwards [i], y: (float)_listAwards [i].Frame.Y + 3);
				}
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);

			foreach (var image in _listAwards)
				image.Transform = CGAffineTransform.MakeScale (0.01f, 0.01f);
		}
	}
}

