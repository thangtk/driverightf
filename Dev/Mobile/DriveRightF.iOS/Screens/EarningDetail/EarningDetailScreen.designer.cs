// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("EarningDetailScreen")]
	partial class EarningDetailScreen
	{
		[Outlet]
		DriveRightF.iOS.ImageHeaderView headerView { get; set; }

		[Outlet]
		UIKit.UIImageView imgCover { get; set; }

		[Outlet]
		UIKit.UILabel lblAchieve { get; set; }

		[Outlet]
		UIKit.UILabel lblArchieveTitle { get; set; }

		[Outlet]
		UIKit.UILabel lblBegin { get; set; }

		[Outlet]
		UIKit.UILabel lblBeginTitle { get; set; }

		[Outlet]
		UIKit.UILabel lblDescription { get; set; }

		[Outlet]
		UIKit.UILabel lblGet { get; set; }

		[Outlet]
		UIKit.UILabel lblGetTitle { get; set; }

		[Outlet]
		UIKit.UILabel lblPrePerked { get; set; }

		[Outlet]
		UIKit.UILabel lblProgress { get; set; }

		[Outlet]
		UIKit.UILabel lblTitle { get; set; }

		[Outlet]
		UIKit.UIImageView viewAward1 { get; set; }

		[Outlet]
		UIKit.UIImageView viewAward10 { get; set; }

		[Outlet]
		UIKit.UIImageView viewAward2 { get; set; }

		[Outlet]
		UIKit.UIImageView viewAward3 { get; set; }

		[Outlet]
		UIKit.UIImageView viewAward4 { get; set; }

		[Outlet]
		UIKit.UIImageView viewAward5 { get; set; }

		[Outlet]
		UIKit.UIImageView viewAward6 { get; set; }

		[Outlet]
		UIKit.UIImageView viewAward7 { get; set; }

		[Outlet]
		UIKit.UIImageView viewAward8 { get; set; }

		[Outlet]
		UIKit.UIImageView viewAward9 { get; set; }

		[Outlet]
		UIKit.UIView viewProgress { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (headerView != null) {
				headerView.Dispose ();
				headerView = null;
			}

			if (imgCover != null) {
				imgCover.Dispose ();
				imgCover = null;
			}

			if (lblAchieve != null) {
				lblAchieve.Dispose ();
				lblAchieve = null;
			}

			if (lblArchieveTitle != null) {
				lblArchieveTitle.Dispose ();
				lblArchieveTitle = null;
			}

			if (lblBegin != null) {
				lblBegin.Dispose ();
				lblBegin = null;
			}

			if (lblBeginTitle != null) {
				lblBeginTitle.Dispose ();
				lblBeginTitle = null;
			}

			if (lblDescription != null) {
				lblDescription.Dispose ();
				lblDescription = null;
			}

			if (lblGet != null) {
				lblGet.Dispose ();
				lblGet = null;
			}

			if (lblGetTitle != null) {
				lblGetTitle.Dispose ();
				lblGetTitle = null;
			}

			if (lblPrePerked != null) {
				lblPrePerked.Dispose ();
				lblPrePerked = null;
			}

			if (lblProgress != null) {
				lblProgress.Dispose ();
				lblProgress = null;
			}

			if (lblTitle != null) {
				lblTitle.Dispose ();
				lblTitle = null;
			}

			if (viewAward1 != null) {
				viewAward1.Dispose ();
				viewAward1 = null;
			}

			if (viewAward10 != null) {
				viewAward10.Dispose ();
				viewAward10 = null;
			}

			if (viewAward2 != null) {
				viewAward2.Dispose ();
				viewAward2 = null;
			}

			if (viewAward3 != null) {
				viewAward3.Dispose ();
				viewAward3 = null;
			}

			if (viewAward4 != null) {
				viewAward4.Dispose ();
				viewAward4 = null;
			}

			if (viewAward5 != null) {
				viewAward5.Dispose ();
				viewAward5 = null;
			}

			if (viewAward6 != null) {
				viewAward6.Dispose ();
				viewAward6 = null;
			}

			if (viewAward7 != null) {
				viewAward7.Dispose ();
				viewAward7 = null;
			}

			if (viewAward8 != null) {
				viewAward8.Dispose ();
				viewAward8 = null;
			}

			if (viewAward9 != null) {
				viewAward9.Dispose ();
				viewAward9 = null;
			}

			if (viewProgress != null) {
				viewProgress.Dispose ();
				viewProgress = null;
			}
		}
	}
}
