﻿using System;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using Unicorn.Core.Bases;
using ReactiveUI;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using UIKit;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using System.Linq;
using System.Threading.Tasks;

namespace DriveRightF.iOS
{
	public abstract class BaseAutoRequestListScreen <T> : DriveRightBaseViewController<BaseAutoRequestViewModel<T>> where T: BaseModel, ICheckChangeModel, new()
	{
		protected LoadingControlView _noDataView;
		protected GlanceLoadingView _loadingView;
		protected readonly UIRefreshControl _refreshControl = new UIRefreshControl ();
		protected virtual CGRect ContentFrame { 
			get { 
				if (ContentTableView != null) {
					return ContentTableView.Frame;
				}
				return this.View.Frame;
			}
		}
		protected abstract UITableView ContentTableView { get; }
		protected abstract DRTableViewSource<T> ContentTableSource  { get; }

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			UIApplication.SharedApplication.SetStatusBarHidden (false, false);
			//
			Initialize();
			RxApp.TaskpoolScheduler.Schedule (() => {
				InitViewModel();
				RxApp.MainThreadScheduler.Schedule(() =>
					{
						InitBindings ();
						ViewModel.LoadData ();
					});
			});

		}

		public abstract void InitViewModel ();

		private void Initialize()
		{
			InitControls ();
			AddNoDataView ();
			AddLoadingView ();
		}

		public virtual void InitControls (){
			ContentTableView.AddSubview (_refreshControl);
			ContentTableView.Source = ContentTableSource;

		}

		protected virtual void AddNoDataView ()
		{
			_noDataView = new LoadingControlView (ContentFrame);
			_noDataView.StartText = "No Data Found";
			_noDataView.Hidden = true;
			this.View.AddSubview (_noDataView);
		}

		protected virtual void AddLoadingView ()
		{
			_loadingView = new GlanceLoadingView (ContentFrame);
			_loadingView.LabelText = "Loading...";
			_loadingView.StartAnimating ();
			_loadingView.Hidden = true;
			this.View.AddSubview (_loadingView);
		}

		/// <summary>
		/// Need to add:
		/// this.OneWayBind (ViewModel, vm => vm.ContentViewHidden, v => v.ContentView.Hidden);
		/// </summary>
		public virtual void InitBindings ()
		{
			if (ViewModel == null) {
				return;
			}
			//
			_refreshControl.ValueChanged += (s,e) => ViewModel.RequestNewData.Execute(true);
			_noDataView.ValueChanged += (s,e) => ViewModel.RequestNewData.Execute(true);
			//
			this.OneWayBind (ViewModel, vm => vm.ContentViewHidden, v => v.ContentTableView.Hidden);
			//			this.OneWayBind (ViewModel, vm => vm.NoDataHidden, v => v._noDataView.Hidden);
			//			this.OneWayBind (ViewModel, vm => vm.RequestingViewHidden, v => v._loadingView.Hidden);

			ViewModel.WhenAnyValue (vm => vm.NoDataViewHidden)
				.Subscribe (b => 
					{
						_noDataView.Hidden = b;
//						if (_isFirstTime == false)
//							_noDataView.Hidden = b;
//						else
//							_isFirstTime = true;
					});
			ViewModel.WhenAnyValue (vm => vm.RequestingViewHidden)
				.Subscribe (b =>
					{
						_loadingView.Hidden = b;
					});
			//
			ViewModel.WhenAnyValue (vm => vm.Requesting)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (b => {
					if (!b) {
						_refreshControl.EndRefreshing ();
						_noDataView.EndRefreshing ();
					}
				});
			//
			ViewModel.WhenAnyValue (vm => vm.DataList)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (data => {
					UpdateUI(data);
				});
			//
			ViewModel.WhenAnyValue (vm => vm.NewData)
				.Where (x => x != null && x.Count > 0)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (data => {
					AppendDataToUI(data);
				});
			//
			ViewModel.WhenAnyValue (vm => vm.HasNewData)
				.Where (b => b)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (ShowNewDataNotification);
		}

		protected virtual void ShowNewDataNotification(bool hasNewData){
			// NEW REQUIREMENT: update ui immediately
			ViewModel.AppendNewdata.Execute(hasNewData);
		}

		/// <summary>
		/// TODO: try to run asynchronously except ReloadRows method
		/// </summary>
		/// <param name="data">Data.</param>
		public virtual void UpdateUI (List<T> data){
			if (data == null || data.Count == 0) return;
			//
			List<T> newItems = new List<T> ();
			foreach (var i in data) {
				var existItem = ContentTableSource.Items.Find (s => s.ServerId == i.ServerId);
				if (existItem == null) {
					newItems.Add (i);
				} else {
					int x = ContentTableSource.Items.IndexOf (existItem);
					ContentTableSource.Items [x] = i;
				}
			}
			ContentTableSource.Items.InsertRange (0, newItems);
			NSIndexPath[] index = CreateIndexPaths (newItems.Count);
			ContentTableView.InsertRows (index, UITableViewRowAnimation.None);
			//
			ContentTableView.ReloadRows(ContentTableView.IndexPathsForVisibleRows, UITableViewRowAnimation.None);
		}

		private NSIndexPath[] CreateIndexPaths(int count)
		{
			List<NSIndexPath> rowsToAdd = new List<NSIndexPath> ();
			for (int i = 0; i < count; i++) {
				rowsToAdd.Add(NSIndexPath.FromRowSection (i, 0));
			}

			return rowsToAdd.ToArray();
		}

		private void AppendDataToUI(List<T> data){
			UpdateUI(data);
			ViewModel.HasNewData = false;
		}

		public override void LoadData (params object[] objs)
		{
			if (ViewModel != null) {
				ViewModel.DoAutoRequest ();
			}
		}

		protected void ReloadRow(int index)
		{
			View.EndEditing (true);
			ContentTableView.BeginUpdates ();
			ContentTableView.ReloadRows (new NSIndexPath[]
				{
					NSIndexPath.FromRowSection (index, 0)
				}, UITableViewRowAnimation.None);
			ContentTableView.EndUpdates ();
		}
	}
}

