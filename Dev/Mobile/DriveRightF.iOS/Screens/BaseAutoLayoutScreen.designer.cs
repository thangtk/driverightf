// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("BaseAutoLayoutScreen")]
	partial class BaseAutoLayoutScreen<T>
	{
//		[Outlet]
//		UIKit.NSLayoutConstraint heightHeader { get; set; }
//
//		[Outlet]
//		UIKit.NSLayoutConstraint heightTabBar { get; set; }
//
//		[Outlet]
//		UIKit.NSLayoutConstraint paddingBottomContent { get; set; }
//
//		[Outlet]
//		UIKit.NSLayoutConstraint paddingLeftMenuRight { get; set; }
//
//		[Outlet]
//		UIKit.NSLayoutConstraint paddingTopContent { get; set; }
//
//		[Outlet]
//		UIKit.NSLayoutConstraint paddingTopHeader { get; set; }

		[Outlet]
		UIKit.UIPanGestureRecognizer panMenu { get; set; }

		[Outlet]
		UIKit.UITapGestureRecognizer tabHeader { get; set; }

		[Outlet]
		UIKit.UITapGestureRecognizer tapMaskMenu { get; set; }

		[Outlet]
		UIKit.UITapGestureRecognizer tapMenu { get; set; }

		[Outlet]
		UIKit.UITapGestureRecognizer tapOverlay { get; set; }

		[Outlet]
		UIKit.UIView vContent { get; set; }

		[Outlet]
		UIKit.UIView vMaskRightMenu { get; set; }

		[Outlet]
		UIKit.UIView vOverLay { get; set; }

		[Outlet]
		UIKit.UIView vOverLaymenu { get; set; }

		[Outlet]
		UIKit.UIView vPage { get; set; }

		[Outlet]
		UIKit.UIView vRightMenu { get; set; }

		[Outlet]
		UIKit.UIView vTab { get; set; }

		[Outlet]
		UIKit.UIView vTest { get; set; }

		[Outlet]
		UIKit.UIView vUnderGround { get; set; }

		[Action ("btnCloseMenu:")]
		partial void btnCloseMenu (Foundation.NSObject sender);

		[Action ("btnOpenMenu:")]
		partial void btnOpenMenu (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (vMaskRightMenu != null) {
				vMaskRightMenu.Dispose ();
				vMaskRightMenu = null;
			}



			if (panMenu != null) {
				panMenu.Dispose ();
				panMenu = null;
			}

			if (tabHeader != null) {
				tabHeader.Dispose ();
				tabHeader = null;
			}

			if (tapMaskMenu != null) {
				tapMaskMenu.Dispose ();
				tapMaskMenu = null;
			}

			if (tapMenu != null) {
				tapMenu.Dispose ();
				tapMenu = null;
			}

			if (tapOverlay != null) {
				tapOverlay.Dispose ();
				tapOverlay = null;
			}

			if (vContent != null) {
				vContent.Dispose ();
				vContent = null;
			}

			if (vOverLay != null) {
				vOverLay.Dispose ();
				vOverLay = null;
			}

			if (vOverLaymenu != null) {
				vOverLaymenu.Dispose ();
				vOverLaymenu = null;
			}

			if (vPage != null) {
				vPage.Dispose ();
				vPage = null;
			}

			if (vRightMenu != null) {
				vRightMenu.Dispose ();
				vRightMenu = null;
			}

			if (vTab != null) {
				vTab.Dispose ();
				vTab = null;
			}

			if (vTest != null) {
				vTest.Dispose ();
				vTest = null;
			}

			if (vUnderGround != null) {
				vUnderGround.Dispose ();
				vUnderGround = null;
			}
		}
	}
}
