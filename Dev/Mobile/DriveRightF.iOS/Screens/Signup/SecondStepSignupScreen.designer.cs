// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("SecondStepSignupScreen")]
	partial class SecondStepSignupScreen
	{
		[Outlet]
		UIKit.UIButton btnCamera { get; set; }

		[Outlet]
		UIKit.UIButton btnDecline { get; set; }

		[Outlet]
		UIKit.UIButton btnNext { get; set; }

		[Outlet]
		UIKit.UIImageView imgBackground { get; set; }

		[Outlet]
		UIKit.UITextView lblDescription { get; set; }

		[Outlet]
		UIKit.UITextField txtPlateNumber { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnCamera != null) {
				btnCamera.Dispose ();
				btnCamera = null;
			}

			if (btnDecline != null) {
				btnDecline.Dispose ();
				btnDecline = null;
			}

			if (btnNext != null) {
				btnNext.Dispose ();
				btnNext = null;
			}

			if (lblDescription != null) {
				lblDescription.Dispose ();
				lblDescription = null;
			}

			if (txtPlateNumber != null) {
				txtPlateNumber.Dispose ();
				txtPlateNumber = null;
			}

			if (imgBackground != null) {
				imgBackground.Dispose ();
				imgBackground = null;
			}
		}
	}
}
