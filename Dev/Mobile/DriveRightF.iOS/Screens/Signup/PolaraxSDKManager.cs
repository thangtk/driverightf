﻿using System;
using DriveRightF.Core;
using PolaraxSDKBinding;
using DriveRightF.Core.Managers;
using Unicorn.Core;
using DriveRightF.Core.Models;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.iOS.PolaraxSDKManager))]
namespace DriveRightF.iOS
{
	public class PolaraxSDKManager : IPolaraxSDK
	{
		private UserManager UserManager {
			get { return DependencyService.Get<UserManager> ();}
		}


		#region IPolaraxSDK implementation
		public void StartService (string customerId, Action<SDKResponse> result)
		{
			// TEST
			return;
			PolaraxSDK.Init (customerId);
			PolaraxSDK.StartService(x =>
				{
					SDKResponse response = new SDKResponse();
					if(x == null || x.StatusCode == PolaraxSDKStatus.START_SUCCESS)
					{
						response.Response = "success";
					}
					else
					{
						response.Response = "failed";
						response.ErrorMessage = x.StatusMessage;
					}

					if(result != null)
					{
						result(response);
					}
				});
		}



		public void StartService (Action<SDKResponse> response)
		{
			// TEST
			return;

			string customerRef = UserManager.GetCustomerRef();
			if (!string.IsNullOrEmpty (customerRef)) {
				StartService (customerRef, response);
			}
		}
		#endregion
		
	}
}

