﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using System.Linq;
using CoreGraphics;
using ReactiveUI;
using Unicorn.Core;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using Unicorn.Core.Enums;
using Unicorn.Core.iOS;
using DriveRightF.Core.Models;
using DriveRightF.Core.Managers;
using Unicorn.Core.UI;

namespace DriveRightF.iOS
{
	public partial class ThirdStepSignupScreen : DriveRightBaseViewController<ThirdStepSignupViewModel>
	{
		private UIPickerView _pickerView;
		private UIToolbar _toolBar;

		public ThirdStepSignupScreen() : base("ThirdStepSignupScreen", null)
		{
			ViewModel = new ThirdStepSignupViewModel ();
		}

		private void Initialize()
		{
			if(ViewModel == null)
			{
				ViewModel = new ThirdStepSignupViewModel ();
			}
			CreateBindingsAsync();
			InitBindCommands ();
			InitView();
		}

		public override void LoadData(params object[] objs)
		{
			base.LoadData(objs);
			ViewModel.Step1 = (SignupViewModel)objs [0];
			ViewModel.Step2 = (SecondStepSignupViewModel)objs [1];
		}

		private void CreateBindingsAsync ()
		{
			RxApp.TaskpoolScheduler.Schedule(()=>{
				InitBindings ();
			});
		}

		private void InitBindings()
		{
			ViewModel.WhenAnyValue (vm => vm.PaymentAccount).Where (x => x != null).ObserveOn (RxApp.MainThreadScheduler).Subscribe (x => lbAccountNumber.Text = x);

		}

		private void InitBindCommands ()
		{
			btnNext.TouchUpInside += (sender, e) =>
			{
				View.EndEditing(true);
				ViewModel.ThirdStepNextCommand.Execute(null);
			};
			btnDecline.TouchUpInside += (sender, e) =>
			{
				View.EndEditing(true);
				ViewModel.ThirdStepDeclineCommand.Execute(null);
			};
			btnInfoPopup.TouchUpInside += (sender, e) =>
			{
				View.EndEditing(true);
				var contentView = new PaymentPopupContent (new CGRect (0, 0, 300, 230),ViewModel.PaymentViewModel);
//				contentView.ViewModel = ViewModel.PaymentViewModel;
				ViewModel.GetInfoFormCommand.Execute(contentView);
			};
		}

		private void InitView()
		{
//			btnInfoPopup.TouchUpInside += (sender, e) => View.EndEditing (true);
//
//			Console.WriteLine (">>>>>>>>>>>>>>> AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
//			_pickerView = new UIPickerView ();
//
//			_toolBar = new UIToolbar ();
//			_toolBar.BarStyle = UIBarStyle.Black;
//			_toolBar.Translucent = true;
//			_toolBar.SizeToFit ();
//			// Create a 'done' button for the toolbar and add it to the toolbar
//
//			PickerModel pickerModel = new PickerModel (new List<string> () { "Alipay", "Wechat" });
//			pickerModel.ValueChange += (sender, e) => {
//				txtAccountType.Text = e.SelectedValue;
//				ViewModel.AccountType = e.SelectedValue;
//			};
//
//			_pickerView.Model = pickerModel;
//			UIBarButtonItem doneButton = new UIBarButtonItem ("DONE", UIBarButtonItemStyle.Done, (s, e) => {
//				txtAccountType.ResignFirstResponder ();
//			});
//			
//			_toolBar.SetItems (new UIBarButtonItem[] {
//				new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace),
//				doneButton
//			}, true);
//
//			txtAccountType.InputView = _pickerView;
//			txtAccountType.Text = "Alipay";
//			txtAccountType.LeftView = new UIView (new CGRect (0, 0, 15, 0));
//			txtAccountType.LeftViewMode = UITextFieldViewMode.Always;
//
//			txtName.LeftViewMode = UITextFieldViewMode.Always;
//			txtName.LeftView = new UIView (new CGRect (0, 0, 15, 0));
//
//			txtAccountNumber.LeftViewMode = UITextFieldViewMode.Always;
//			txtAccountNumber.LeftView = new UIView (new CGRect (0, 0, 15, 0));
//
//			ViewModel.AccountType = "Alipay";
//			txtAccountType.InputAccessoryView = _toolBar;
//			txtAccountType.AccessibilityFrame = new CGRect (0, -10, 200, 64);
//
//			var image = new UIImageView (new CGRect (-10f, 0f, 14, 9));
//			image.Image = UIImage.FromFile ("icon_dropdown@2x.png");
//			txtAccountType.RightViewMode = UITextFieldViewMode.Always;
//			var view = new UIView (new CGRect (0, 0, 14, 9));
//			view.AddSubview (image);
//			txtAccountType.RightView = view;

			lblName.Text = Translator.Translate("SIGN_UP_NAME");
			lblNumber.Text = Translator.Translate("SIGN_UP_ACCOUNT_NUMBER");

			View.AddGestureRecognizer(new UITapGestureRecognizer (() =>
			{
				View.EndEditing(true);
			}));
		}

		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			Initialize();
			InitTranslator();
		}

		private void InitTranslator()
		{
			btnNext.SetTitle(Translator.Translate("BUTTON_NEXT"), UIControlState.Normal);
			btnDecline.SetTitle(Translator.Translate("BUTTON_NO_THANK_YOU"), UIControlState.Normal);
			lblName.Text = Translator.Translate("SIGN_UP_NAME");
			lblNumber.Text = Translator.Translate("SIGN_UP_ACCOUNT_NUMBER");

			lblDescription.Font = FontHub.HelveticaLt15;
			lblDescription.TextColor = UIColor.White;

			var str = Translator.Translate("SIGN_UP_DESCRIPTION_3");
			var aliPayStr = Translator.Translate("ALIPAY");
			var weChatStr = Translator.Translate("WECHAT");
			var attributedString = new NSMutableAttributedString (str);

			var btTitle = new UIStringAttributes { Font = FontHub.HelveticaLt15, ForegroundColor = UIColor.White };
			attributedString.AddAttributes(btTitle.Dictionary, new NSRange (0, str.Length));

			var alitTitle = new UIStringAttributes { Font = FontHub.HelveticaMedium15, ForegroundColor = UIColor.White };
			var wechatTitle = new UIStringAttributes { Font = FontHub.HelveticaMedium15, ForegroundColor = UIColor.White };

			try
			{
				
				attributedString.AddAttributes(alitTitle.Dictionary, new NSRange (str.IndexOf(aliPayStr), aliPayStr.Length));
				attributedString.AddAttributes(wechatTitle.Dictionary, new NSRange (str.IndexOf(weChatStr), weChatStr.Length));
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
			}

			lblDescription.AttributedText = attributedString;		
			lblDescription.TextAlignment = UITextAlignment.Center;
		}
	}
}

