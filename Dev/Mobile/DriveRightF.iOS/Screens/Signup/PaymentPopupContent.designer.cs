// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	partial class PaymentPopupContent
	{
		[Outlet]
		UIKit.UILabel lblAccountNumber { get; set; }

		[Outlet]
		UIKit.UILabel lblName { get; set; }

		[Outlet]
		UIKit.UITextField txtAccountNumber { get; set; }

		[Outlet]
		UIKit.UITextField txtAccountType { get; set; }

		[Outlet]
		UIKit.UITextField txtName { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (txtAccountNumber != null) {
				txtAccountNumber.Dispose ();
				txtAccountNumber = null;
			}

			if (txtAccountType != null) {
				txtAccountType.Dispose ();
				txtAccountType = null;
			}

			if (txtName != null) {
				txtName.Dispose ();
				txtName = null;
			}

			if (lblName != null) {
				lblName.Dispose ();
				lblName = null;
			}

			if (lblAccountNumber != null) {
				lblAccountNumber.Dispose ();
				lblAccountNumber = null;
			}
		}
	}
}
