﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using System.Linq;
using CoreGraphics;
using ReactiveUI;
using Unicorn.Core;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using Unicorn.Core.Enums;
using Unicorn.Core.iOS;
using DriveRightF.Core.Models;
using DriveRightF.Core.Managers;
using Unicorn.Core.UI;

namespace DriveRightF.iOS
{
	public partial class SecondStepSignupScreen : DriveRightBaseViewController<SecondStepSignupViewModel>
	{
		public SecondStepSignupScreen() : base("SecondStepSignupScreen", null)
		{
			ViewModel = new SecondStepSignupViewModel ();
		}

		private void Initialize()
		{
			InitView();

			if(ViewModel == null)
			{
				ViewModel = new SecondStepSignupViewModel ();
			}

			CreateBindingsAsync();
			InitBindCommands ();

		}

		public override void LoadData(params object[] objs)
		{
			base.LoadData(objs);
			ViewModel.Step1 = (SignupViewModel)objs [0];
		}

		private void CreateBindingsAsync ()
		{
			RxApp.TaskpoolScheduler.Schedule(()=>{
				InitBindings ();
			});
		}

		private void InitBindCommands ()
		{
			btnNext.TouchUpInside += (sender, e) => {
				View.EndEditing(true);
				ViewModel.SecondStepNextCommand.Execute(null);
			};

			btnDecline.TouchUpInside += (sender, e) => {
				View.EndEditing(true);
				ViewModel.SecondStepDeclineCommand.Execute(null);
			};
			btnCamera.TouchUpInside += (sender, e) => {
				View.EndEditing(true);
				ViewModel.GetPlateNumberByOCRCommand.Execute(null);
			};
			txtPlateNumber.EditingChanged += (sender, e) => {
				ViewModel.PlateNumber = txtPlateNumber.Text;
			};
		}

		private void InitBindings()
		{
			ViewModel.WhenAnyValue (vm => vm.PlateNumber).Where (x => x != null).ObserveOn (RxApp.MainThreadScheduler).Subscribe (x => txtPlateNumber.Text = x);
//			this.Bind(
//				ViewModel,
//				vm => vm.PlateNumber,
//				v => v.txtPlateNumber.Text);
		}

		private void InitView()
		{
			txtPlateNumber.LeftViewMode = UITextFieldViewMode.Always;
			txtPlateNumber.LeftView = new UIView (new CGRect (0, 0, 15, 5));

			imgBackground.Image = BlurViewUtil.BlurImage (UIImage.FromFile ("splash_bg.png"));

			btnCamera.TouchUpInside += (sender, e) => View.EndEditing(true);

			View.AddGestureRecognizer(new UITapGestureRecognizer (() =>
			{
				View.EndEditing(true);
			}));

			btnDecline.Layer.BorderColor = UIColor.White.CGColor;
			btnDecline.Layer.BorderWidth = 1f;

//			btnNext.Layer.BorderColor = UIColor.White.CGColor;
//			btnNext.Layer.BorderWidth = 1f;
		}

		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			Initialize();
			InitTranslator();

		}

		private void InitTranslator()
		{
			btnNext.SetTitle(Translator.Translate("BUTTON_NEXT"), UIControlState.Normal);
			btnDecline.SetTitle(Translator.Translate("BUTTON_NO_THANK_YOU"), UIControlState.Normal);
			lblDescription.Text = Translator.Translate ("SIGN_UP_DESCRIPTION_2");

//			lblDescription.Font = FontHub.HelveticaLt15;
//			lblDescription.TextColor = UIColor.White;
//
//			var str = Translator.Translate("SIGN_UP_DESCRIPTION_2");
//			var attributedString = new NSMutableAttributedString (str);
//
//			var btTitle = new UIStringAttributes { Font = FontHub.HelveticaLt15, ForegroundColor = UIColor.White };			
//			var atTitle = new UIStringAttributes { Font = FontHub.HelveticaMedium15, ForegroundColor = UIColor.White };
//
//			try
//			{
//				attributedString.AddAttributes(btTitle.Dictionary, new NSRange (0, str.Length));
//				attributedString.AddAttributes(atTitle.Dictionary, new NSRange (str.Substring(0, str.Length - 1).LastIndexOf('.') + 1, str.Length - str.Substring(0, str.Length - 1).LastIndexOf('.')-1));				
//			}
//			catch(Exception ex)
//			{
//				Console.WriteLine(ex.Message);
//			}
//
//			lblDescription.AttributedText = attributedString;		
//			lblDescription.TextAlignment = UITextAlignment.Left;
		}
	}
}

