// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("SignupScreen")]
	partial class SignupScreen
	{
		[Outlet]
		UIKit.UIButton btnSMS { get; set; }

		[Outlet]
		UIKit.UIImageView imgBackground { get; set; }

		[Outlet]
		UIKit.UILabel lblDescription1 { get; set; }

		[Outlet]
		UIKit.UILabel lblSignUp { get; set; }

		[Outlet]
		UIKit.UITextField txtPhoneNumber { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnSMS != null) {
				btnSMS.Dispose ();
				btnSMS = null;
			}

			if (lblDescription1 != null) {
				lblDescription1.Dispose ();
				lblDescription1 = null;
			}

			if (lblSignUp != null) {
				lblSignUp.Dispose ();
				lblSignUp = null;
			}

			if (txtPhoneNumber != null) {
				txtPhoneNumber.Dispose ();
				txtPhoneNumber = null;
			}

			if (imgBackground != null) {
				imgBackground.Dispose ();
				imgBackground = null;
			}
		}
	}
}
