﻿
using System;

using UIKit;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using CoreGraphics;
using ReactiveUI;
using System.Reactive.Concurrency;
using Unicorn.Core.iOS;

namespace DriveRightF.iOS
{
	public partial class SignupScreen : DriveRightBaseViewController<SignupViewModel>
	{
		private UIView _contentView;
//		private UITextField _txtView;

		public override float OffsetKeyboard
		{
			get
			{
				return -40;
			}
		}

		public SignupScreen() : base("SignupScreen", null)
		{
		}

		private void Initialize()
		{
			ViewModel = new SignupViewModel ();

			CreateBindingsAsync();
			InitView();
			InitBindCommands ();
			InitTranslator();

			lblDescription1.Font = FontHub.HelveticaCn14;
		}

		private void InitTranslator()
		{
			lblSignUp.Text = Translator.Translate("SIGN_UP_NOW");
			txtPhoneNumber.Placeholder = Translator.Translate("PLACEHOLDER_PHONENUMBER");
			lblDescription1.Text = Translator.Translate("SIGN_UP_DESCRIPTION_1");
			btnSMS.SetTitle(Translator.Translate("BUTTON_GET_SMS"), UIControlState.Normal);
		}

		private void CreateBindingsAsync ()
		{
			RxApp.TaskpoolScheduler.Schedule(()=>{
				InitBindings ();
			});
		}

		private void InitBindings()
		{

//			this.Bind(
//				ViewModel,
//				vm => vm.SMSCode,
//				v => v._txtView.Text);
//
////			this.Bind(
////				ViewModel,
////				vm => vm.PhoneNumber,
////				v => v.txtPhoneNumber.Text);
////
//		//	ViewModel.WhenAnyValue (vm => vm.SMSCode).Where (x => x != null).ObserveOn (RxApp.MainThreadScheduler).Subscribe (x => _txtView.Text = x);
//			ViewModel.WhenAnyValue (vm => vm.PhoneNumber).Where (x => x != null).ObserveOn (RxApp.MainThreadScheduler).Subscribe (x => txtPhoneNumber.Text = x);
//
		}

		private void InitBindCommands ()
		{
			btnSMS.TouchUpInside += (sender, e) =>
			{
				View.EndEditing(true);
				var contentView = CreatePhonePopupContent(null);
				ViewModel.GetSMSCommand.Execute(contentView);
			};
			txtPhoneNumber.EditingChanged += (sender, e) => {
				ViewModel.PhoneNumber = txtPhoneNumber.Text;
			};

//			_txtView.EditingChanged += (sender, e) => {
//				ViewModel.PhoneNumber = _txtView.Text;
//			};
		}
		private void InitView()
		{
			var height = UIScreen.MainScreen.Bounds.Height;
			var width = UIScreen.MainScreen.Bounds.Width;

			imgBackground.Image = BlurViewUtil.BlurImage (UIImage.FromFile ("splash_bg.png"));

//			_contentView = new UIView (new CGRect (0, 0, width * 0.7f, 40));
//
//			_txtView = new UITextField (new CGRect (0.05f * width, 5, 0.6f * width, 30)) {
//				BackgroundColor = UIColor.White,
//				LeftViewMode = UITextFieldViewMode.Always,
//				LeftView = new UIView (new CGRect (0, 0, 15, 5)),
//			};
//			_txtView.Layer.CornerRadius = 2;
//			_txtView.EditingChanged += (object sender, EventArgs e) => {
//				ViewModel.SMSCode = (sender as UITextField).Text;
//			};
//			_contentView.AddSubview (_txtView);

//			ViewModel.BindSMSCodeContentViewCommand.Execute ((object)_contentView);

//			#if DEBUG
//			txtPhoneNumber.Text = "(094)-936-8209";
//			ViewModel.PhoneNumber = "(094)-936-8209";
//			_txtView.Text = "12345";
//			ViewModel.SMSCode = "12345";
//			#endif
			txtPhoneNumber.ShouldChangeCharacters = (textField, range, replacementString) =>
			{
				var newLength = textField.Text.Length + replacementString.Length - range.Length;
				return newLength <= 15;
			};

			txtPhoneNumber.LeftViewMode = UITextFieldViewMode.Always;
			txtPhoneNumber.LeftView = new UIView (new CGRect (0, 0, 15, txtPhoneNumber.Frame.Height));

			// Perform any additional setup after loading the view, typically from a nib.
			View.AddGestureRecognizer(new UITapGestureRecognizer (() =>
			{
				View.EndEditing(true);
			}));
		}

		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			Initialize();

		}

		private UIView CreatePhonePopupContent(Action<string> textChanged, UIKeyboardType keyboardType = UIKeyboardType.NumberPad)
		{
			var width = this.View.Frame.Width * 0.9f;
			nfloat height = 70f;
			UIView view = new UIView (new CGRect (0, 0, width, height));
			view.BackgroundColor = UIColor.FromRGB(255, 255, 255); // Same color with dialog
			nfloat padding = 16f;
			UITextField tbx = new UITextField (new CGRect (padding, 15f, width - padding * 2, 40)) {
				BackgroundColor = UIColor.FromRGB(239, 239, 239),
				LeftViewMode = UITextFieldViewMode.Always,
				LeftView = new UIView (new CGRect (0, 0, 15, 5)),
				KeyboardType = keyboardType
			};
			tbx.Layer.CornerRadius = 2f;
			tbx.EditingChanged += (object sender, EventArgs e) =>
			{
				ViewModel.SMSCode = (sender as UITextField).Text;
				if(textChanged != null)
				{
					textChanged.Invoke((sender as UITextField).Text);
				}
			};
			view.AddSubview(tbx);
			return view;
		}
	}
}

