﻿using System;
using UIKit;
using Foundation;
using Unicorn.Core.UI;
using System.Collections.Generic;
using CoreGraphics;
using ObjCRuntime;
using DriveRightF.Core;
using ReactiveUI;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using Unicorn.Core;

namespace DriveRightF.iOS
{
	[Register("PaymentPopupContent")]
	public partial class PaymentPopupContent : UBaseView<PaymentPopupViewModel>
	{
		UIPickerView _pickerView;
		private bool _touchFistTime = true;
		private List<string> _sourceModel = new List<string>();


		//TODO contructor with viewmodel
		public PaymentPopupContent(CGRect frame, PaymentPopupViewModel model) : base(frame)
		{
			ViewModel = model;
			InitViews();
		}

		public PaymentPopupContent(IntPtr handler) : base(handler)
		{
			InitViews();
		}

		private void InitViews()
		{
			var arr = NSBundle.MainBundle.LoadNib("PaymentPopupContent", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			v.Frame = new CGRect (CGPoint.Empty, Frame.Size);
			AddSubview(v);
			//
			InitControls();
			//TODO apply binding with thread pool
			RxApp.TaskpoolScheduler.Schedule (Bindings);
		}

		private void InitControls()
		{
			_pickerView = new UIPickerView ();

			var _toolBar = new UIToolbar ();
			_toolBar.BarStyle = UIBarStyle.Black;
			_toolBar.Translucent = true;
			_toolBar.SizeToFit();
			// Create a 'done' button for the toolbar and add it to the toolbar

			lblName.Text = Translator.Translate("SIGN_UP_NAME");
			lblAccountNumber.Text = Translator.Translate("SIGN_UP_ACCOUNT_NUMBER");

			_sourceModel = new List<string> () {
				Translator.Translate ("ALIPAY"),
				Translator.Translate ("WECHAT")
			};
			PickerModel pickerModel = new PickerModel (_sourceModel);
			pickerModel.ValueChange += (sender, e) =>
			{
				txtAccountType.Text = e.SelectedValue;
				if(ViewModel != null && !ViewModel.PaymentType.Equals(e.SelectedValue))
				{
					MyDebugger.ShowThreadInfo("Picker value changed");
					ViewModel.PaymentType = e.SelectedValue;
				}
			};

			_pickerView.Model = pickerModel;
			UIBarButtonItem doneButton = new UIBarButtonItem (Translator.Translate("BUTTON_DONE"), UIBarButtonItemStyle.Done, (s, e) =>
			{
				txtAccountType.ResignFirstResponder();
			});

			_toolBar.SetItems(new UIBarButtonItem[] {
				new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace),
				doneButton
			}, true);


			txtAccountType.InputView = _pickerView;
			txtAccountType.LeftView = new UIView (new CGRect (0, 0, 15, 0));
			txtAccountType.LeftViewMode = UITextFieldViewMode.Always;

			txtName.LeftViewMode = UITextFieldViewMode.Always;
			txtName.LeftView = new UIView (new CGRect (0, 0, 15, 0));

			txtAccountNumber.LeftViewMode = UITextFieldViewMode.Always;
			txtAccountNumber.LeftView = new UIView (new CGRect (0, 0, 15, 0));
			txtAccountNumber.KeyboardType = UIKeyboardType.NumberPad;

			txtAccountNumber.ShouldChangeCharacters = (textField, range, replacementString) =>
			{
				var newLength = textField.Text.Length + replacementString.Length - range.Length;
				return newLength <= 20;
			};

			txtName.ShouldChangeCharacters = (textField, range, replacementString) =>
			{
				var newLength = textField.Text.Length + replacementString.Length - range.Length;
				return newLength <= 30;
			};
				
			txtAccountType.InputAccessoryView = _toolBar;
			txtAccountType.AccessibilityFrame = new CGRect (0, -10, 200, 64);

			var image = new UIImageView (new CGRect (-10f, 0f, 14, 9));
			image.Image = UIImage.FromFile("icon_dropdown@2x.png");
			txtAccountType.RightViewMode = UITextFieldViewMode.Always;
			var view = new UIView (new CGRect (0, 0, 14, 9));
			view.UserInteractionEnabled = false;
			view.AddSubview(image);
			txtAccountType.RightView = view;

			//TODO: apply first item alipay when touch pickerview.
			txtAccountType.ShouldBeginEditing += (textField) =>
			{
				if(_touchFistTime)
				{
					_touchFistTime = false;
					//TODO: LoadData to accoutType.
					if(string.IsNullOrEmpty(ViewModel.PaymentType))
					{
						ViewModel.PaymentType = _sourceModel[0];
					}
				}
				return true;
			};

			txtAccountType.EditingChanged += (object sender, EventArgs e) => 
			{
				ViewModel.PaymentType = txtAccountType.Text;
			};
			txtName.EditingChanged += (object sender, EventArgs e) => 
			{
				ViewModel.PaymentName = txtName.Text;
			};
			txtAccountNumber.EditingChanged += (object sender, EventArgs e) => 
			{
				ViewModel.PaymentAccount = txtAccountNumber.Text;
			};
		}

		private void Bindings()
		{
			ViewModel.WhenAnyValue(vm => vm.PaymentType)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(x =>
			{

					txtAccountType.Text = x;

				var currentIndex = (_pickerView.Model as PickerModel).values.FindIndex(v => string.Equals(v, x, StringComparison.CurrentCultureIgnoreCase));
				if(currentIndex >= 0 && currentIndex != _pickerView.SelectedRowInComponent(0))
				{
					_pickerView.Select(currentIndex, 0, false);
				}

			});

			ViewModel.WhenAnyValue(vm => vm.PaymentName)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(x =>
				{
					txtName.Text = x;
				});
			ViewModel.WhenAnyValue(vm => vm.PaymentAccount)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(x =>
				{
					txtAccountNumber.Text = x;
				});
		}
	}

	public class PickerModel : UIPickerViewModel
	{
		public List<string> values;

		public event EventHandler<PickerChangedEventArgs> ValueChange;

		public PickerModel(List<string> values)
		{
			this.values = values;
		}

		public override nint GetComponentCount(UIPickerView picker)
		{
			return 1;
		}
		//
		public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
		{
			return values.Count;
		}
		//
		public override string GetTitle(UIPickerView pickerView, nint row, nint component)
		{
			return values [(int)row].ToString();
		}
		//
		public override nfloat GetRowHeight(UIPickerView pickerView, nint component)
		{
			return 40f;
		}
		//
		public override void Selected(UIPickerView pickerView, nint row, nint component)
		{
			if(this.ValueChange != null)
			{
				this.ValueChange(this, new PickerChangedEventArgs{ SelectedValue = values [(int)row] });
			}
		}

	}

	public class PickerChangedEventArgs : EventArgs
	{
		public string SelectedValue
		{
			get;
			set;
		}
	}
}

