// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("ThirdStepSignupScreen")]
	partial class ThirdStepSignupScreen
	{
		[Outlet]
		UIKit.UIButton btnDecline { get; set; }

		[Outlet]
		UIKit.UIButton btnInfoPopup { get; set; }

		[Outlet]
		UIKit.UIButton btnNext { get; set; }

		[Outlet]
		UIKit.UILabel lbAccountNumber { get; set; }

		[Outlet]
		UIKit.UITextView lblDescription { get; set; }

		[Outlet]
		UIKit.UILabel lblName { get; set; }

		[Outlet]
		UIKit.UILabel lblNumber { get; set; }

		[Outlet]
		UIKit.UILabel lblPaymentMethod { get; set; }

		[Outlet]
		UIKit.UITextField txtAccountNumber { get; set; }

		[Outlet]
		UIKit.UITextField txtAccountType { get; set; }

		[Outlet]
		UIKit.UITextField txtName { get; set; }

		[Outlet]
		UIKit.UITextField txtPassword { get; set; }

		[Outlet]
		UIKit.UIView vInfoForm { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (lblPaymentMethod != null) {
				lblPaymentMethod.Dispose ();
				lblPaymentMethod = null;
			}

			if (lblName != null) {
				lblName.Dispose ();
				lblName = null;
			}

			if (lblNumber != null) {
				lblNumber.Dispose ();
				lblNumber = null;
			}

			if (btnDecline != null) {
				btnDecline.Dispose ();
				btnDecline = null;
			}

			if (btnInfoPopup != null) {
				btnInfoPopup.Dispose ();
				btnInfoPopup = null;
			}

			if (btnNext != null) {
				btnNext.Dispose ();
				btnNext = null;
			}

			if (lbAccountNumber != null) {
				lbAccountNumber.Dispose ();
				lbAccountNumber = null;
			}

			if (lblDescription != null) {
				lblDescription.Dispose ();
				lblDescription = null;
			}

			if (txtAccountNumber != null) {
				txtAccountNumber.Dispose ();
				txtAccountNumber = null;
			}

			if (txtAccountType != null) {
				txtAccountType.Dispose ();
				txtAccountType = null;
			}

			if (txtName != null) {
				txtName.Dispose ();
				txtName = null;
			}

			if (txtPassword != null) {
				txtPassword.Dispose ();
				txtPassword = null;
			}

			if (vInfoForm != null) {
				vInfoForm.Dispose ();
				vInfoForm = null;
			}
		}
	}
}
