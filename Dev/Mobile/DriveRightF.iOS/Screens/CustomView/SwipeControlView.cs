﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;
using CoreAnimation;

namespace DriveRightF.iOS
{
	public partial class SwipeControlView : UIView
	{
		private UIButton _btnSwipe;
		private CGPoint _lastTouch;
		private nfloat _originalViewTop;
		private bool _isVisible;
		private UIView _contentView;
		private UIView _layerView;
		private nfloat _layerFrameTop;
		private SwipeViewState _swipeViewState;

		public event EventHandler AnimationShowEnded;
		public event EventHandler AnimationHideEnded;

		public nfloat OriginalViewTop {
			get {
				return _originalViewTop;
			}
			set {
				_originalViewTop = value;
			}
		}

		public bool ShowButtonSwipe {
			set { _btnSwipe.Alpha = value ? 1 : 0; }
		}

		public double DistanceRatio {
			get;
			set;
		}

		public double AnimationDuration {
			get;
			set;
		}

		public nfloat TopOffset {
			get;
			set;
		}

		public nfloat NaturalOffset {
			get;
			set;
		}

		public UIImage SwipeButtonImage {
			get;
			set;
		}

		public UIImage OpenedSwipeButtonImage {
			get;
			set;
		}

		public bool IsSwipeEnabled {
			get;
			set;
		}

		private SwipeViewState SwipeViewState {
			set {
				if (_swipeViewState != value) {
					_swipeViewState = value;
					if (value == SwipeViewState.DownsideUp) {
					} else {
					}
				}
			}
		}

		public UIButton SwipeButtonView {
			get { return _btnSwipe; }
			set { 
				_btnSwipe = value;
				ReaddSwipeButton ();
			}
		}

		public UIView ContentView {
			get { return _contentView; }
			set { 
				_contentView = value;
				ReAddContentView ();
			}
		}

		public nfloat MaxLayerViewAlpha {
			get;
			set;
		}

		public UIColor LayerBackGroundColor {
			get;
			set;
		}

		public bool IsVisible {
			get { return _isVisible; }
			set {
				if (!value)
					HideView ();
				else
					ShowView ();
			}
		}

		public nfloat LayerFrameTop {
			get { return _layerFrameTop; }
			set { 
				_layerFrameTop = value;
				RelayoutLayer ();
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DriveRightF.iOS.SwipeControlView"/> class.
		/// </summary>
		/// <param name="frame"> Should set y < 15f.</param>
		/// <param name="contentView">Content view.</param>
		public SwipeControlView (CGRect frame, UIView contentView = null, SwipeViewState swipeViewState = SwipeViewState.DownsideUp) : base (frame)
		{
			//Frame = new CGRect (frame.X, frame.Y - TopOffset, frame.Width, frame.Height);
			InitParams ();
			SwipeViewState = swipeViewState;
			InitViews ();
			ContentView = contentView;
		}

		private void InitParams ()
		{
			IsSwipeEnabled = false;
			LayerBackGroundColor = UIColor.Black;
			NaturalOffset = 15f;
			AnimationDuration = 0.15;
			DistanceRatio = 0.2;
			TopOffset = 30f;
			_originalViewTop = Frame.Top;
			MaxLayerViewAlpha = 0.4f;
			LayerFrameTop = 0;
			SwipeButtonImage = UIImage.FromFile ("arrow_tab_up.png");
			OpenedSwipeButtonImage = UIImage.FromFile ("arrow_tab_down.png");
			SwipeViewState = SwipeViewState.UpsideDown;
		}

		private void InitViews ()
		{
			_layerView = new UIView ();

			_btnSwipe = new UIButton (new CGRect (0, TopOffset - 8f, 80, 15));
			ReaddSwipeButton ();

			AddGestureRecognizer (new UIPanGestureRecognizer (OnPanGestureRecognizer));
		}

		private void ReaddSwipeButton ()
		{
			if (_btnSwipe != null)
				_btnSwipe.RemoveFromSuperview ();

			_btnSwipe.Layer.CornerRadius = 7.5f;

			if (_swipeViewState == SwipeViewState.DownsideUp) {
				_btnSwipe.Frame = new CGRect (0, TopOffset - _btnSwipe.Frame.Height, _btnSwipe.Frame.Width, _btnSwipe.Frame.Height);
				_btnSwipe.Center = new CGPoint (Frame.Width / 2f, _btnSwipe.Center.Y);
			} else {
				_btnSwipe.Frame = new CGRect (0, Frame.Height - TopOffset, _btnSwipe.Frame.Width, _btnSwipe.Frame.Height);
				_btnSwipe.Center = new CGPoint (Frame.Width / 2f, _btnSwipe.Center.Y);
			}

			_btnSwipe.TouchUpInside += (sender, e) => {
				if (!_isVisible)
					ShowView ();
				else
					HideView ();
			};
			AddSubview (_btnSwipe);
		}

		private void RelayoutLayer ()
		{
			if (Superview != null && _layerView != null) {
				_layerView.RemoveFromSuperview ();

				if (_swipeViewState == SwipeViewState.DownsideUp)
					_layerView.Frame = new CGRect (Frame.X, LayerFrameTop, Frame.Width, Frame.Top + TopOffset - LayerFrameTop);
				else
					_layerView.Frame = new CGRect (Frame.X, Frame.Bottom, Frame.Width, UIScreen.MainScreen.Bounds.Height - Frame.Bottom);
				
				_layerView.BackgroundColor = LayerBackGroundColor;
				_layerView.Alpha = 0;

				_layerView.AddGestureRecognizer (new UITapGestureRecognizer (new Action<UITapGestureRecognizer> (delegate(UITapGestureRecognizer obj) {
					if (obj.State == UIGestureRecognizerState.Ended)
						HideView ();
				})));

				Superview.AddSubview (_layerView);
				Superview.BringSubviewToFront (this);
			}
		}

		private void ReAddContentView ()
		{
			if (_contentView != null) {
				_contentView.RemoveFromSuperview ();

				if (_swipeViewState == SwipeViewState.DownsideUp) {
					var frame = Frame;
					frame.Height = _contentView.Frame.Height;
					Frame = frame;

					_contentView.Frame = new CGRect (0.5f * (Frame.Width - _contentView.Frame.Width), TopOffset, _contentView.Frame.Width, _contentView.Frame.Height);
				
					AddSubview (_contentView);
					SendSubviewToBack (_contentView);
				} else {
					var frame = Frame;
					frame.Height = _contentView.Frame.Height - NaturalOffset;
					Frame = frame;

					_contentView.Frame = new CGRect (0.5f * (Frame.Width - _contentView.Frame.Width), Frame.Height - TopOffset - _contentView.Frame.Height, _contentView.Frame.Width, _contentView.Frame.Height);

					AddSubview (_contentView);
					SendSubviewToBack (_contentView);
				}
				//RelayoutLayer ();
			}
		}

		private void OnPanGestureRecognizer (UIPanGestureRecognizer recognizer)
		{
			if (!IsSwipeEnabled)
				return;
			
			var touch = recognizer.LocationInView (Superview);

			if (recognizer.State == UIGestureRecognizerState.Began) {
				_lastTouch = touch;
			} else if (recognizer.State == UIGestureRecognizerState.Changed) {

				var offset = touch.Y - _lastTouch.Y;

				if (_swipeViewState == SwipeViewState.DownsideUp) {
					if (Frame.Y + offset <= _originalViewTop
					    && Frame.Bottom + offset >= _originalViewTop + TopOffset) {
						var frame = Frame;
						frame.Y += offset;
						Frame = frame;
						_layerView.Alpha = MaxLayerViewAlpha * Math.Abs ((float)(frame.Y - _originalViewTop)) / Frame.Height;
					}
				} else {
					if (Frame.Y + offset >= _originalViewTop
					    && Frame.Top + offset <= _originalViewTop + Frame.Height) {
						var frame = Frame;
						frame.Y += offset;
						Frame = frame;				
						_layerView.Alpha = MaxLayerViewAlpha * Math.Abs ((float)(frame.Y - _originalViewTop)) / Frame.Height;
					}
				}
			} else if (recognizer.State == UIGestureRecognizerState.Ended) {
				if (_swipeViewState == SwipeViewState.DownsideUp) {
					if (_originalViewTop - Frame.Top < DistanceRatio * Frame.Height)
						HideView ();
					else if (_originalViewTop - Frame.Top > (1 - DistanceRatio) * Frame.Height)
						ShowView ();
					else {
						if (recognizer.VelocityInView (Superview).Y > 0)
							HideView ();
						else
							ShowView ();
					}
				} else {
					if (Frame.Top - OriginalViewTop < DistanceRatio * Frame.Height)
						HideView ();
					else if (Frame.Top - OriginalViewTop > (1 - DistanceRatio) * Frame.Height)
						ShowView ();
					else {
						if (recognizer.VelocityInView (Superview).Y < 0)
							HideView ();
						else
							ShowView ();
					}
				}
			}

			_lastTouch = touch;
		}

		private void HideView ()
		{
			if (!_isVisible)
				return;

			_isVisible = false;
			
			UIView.Animate (AnimationDuration,
				() => {
					var frame = Frame;
					frame.Y = _originalViewTop;
					Frame = frame;

					_layerView.Alpha = 0f;
				},
				() => {
					InvokeOnMainThread (() => {
					_btnSwipe.SetImage (SwipeButtonImage, UIControlState.Normal);
					if (AnimationHideEnded != null)
						AnimationHideEnded.Invoke (this, null);
					});
				}
			);
		}

		private void ShowView ()
		{
			if (_isVisible)
				return;
				
			_isVisible = true;
			
			var frame = _btnSwipe.Frame;

			UIView.Animate (AnimationDuration,
				() => {
					if (_swipeViewState == SwipeViewState.DownsideUp) {
						frame = Frame;
						frame.Y = _originalViewTop - Frame.Height + TopOffset - NaturalOffset;
						Frame = frame;
					} else {
						frame = Frame;
						frame.Y = _originalViewTop + Frame.Height + NaturalOffset;
						Frame = frame;
					}
					_layerView.Alpha = MaxLayerViewAlpha;
				},
				() => {
					InvokeOnMainThread (() => {
						UIView.Animate (
							0.2,
							() => {
								if (_swipeViewState == SwipeViewState.DownsideUp) {
									frame = Frame;
									frame.Y += NaturalOffset;
									Frame = frame;
								} else {
									frame = Frame;
									frame.Y -= NaturalOffset;
									Frame = frame;
								}
							},
							() => {
							});
						
					});

					_btnSwipe.SetImage (OpenedSwipeButtonImage, UIControlState.Normal);

					if (AnimationShowEnded != null)
						AnimationShowEnded.Invoke (this, null);
				});
		}

		public override void MovedToSuperview ()
		{
			base.MovedToSuperview ();
			RelayoutLayer ();
		}
	}

	public enum SwipeViewState
	{
		UpsideDown,
		DownsideUp,
	}
}

