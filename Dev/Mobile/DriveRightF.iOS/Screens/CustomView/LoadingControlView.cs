﻿
using System;

using Foundation;
using UIKit;
using CoreGraphics;
using Unicorn.Core.UI;
using System.ComponentModel;

namespace DriveRightF.iOS
{
	public partial class LoadingControlView : UIScrollView
	{
		private UIRefreshControl _refreshControl;
		private UILabel _startLabel;
		public event EventHandler ValueChanged;

		public string StartText {
			get { return _startLabel.Text;}
			set { _startLabel.Text = value;}
		}

		public bool StartTextHidden {
			get { return _startLabel.Hidden;}
			set { _startLabel.Hidden = value;}
		}

		public LoadingControlView (CGRect frame) : base(frame)
		{
			InitControls ();
		}
			
		public LoadingControlView (NSCoder coder) : base(coder)
		{
			InitControls ();
		}

		public LoadingControlView (NSObjectFlag t) : base(t)
		{
			InitControls ();
		}

		public LoadingControlView (IntPtr handle) : base(handle)
		{
			InitControls ();
		}

		private void InitControls()
		{
			ContentSize = new CGSize (Frame.Width, Frame.Height + 5f);
			ShowsVerticalScrollIndicator = false;
			ShowsHorizontalScrollIndicator = false;

			_startLabel = new UILabel (new CGRect (0, 0, Frame.Width, 30f));
			_startLabel.Center = new CGPoint (Frame.Width / 2f, Frame.Height / 2f - 100f);
			_startLabel.BackgroundColor = UIColor.Clear;
			_startLabel.TextColor = UIColor.DarkGray;
			_startLabel.TextAlignment = UITextAlignment.Center;
			AddSubview (_startLabel);

			_refreshControl = new UIRefreshControl () {
				BackgroundColor = UIColor.White,
				TintColor = UIColor.FromRGB (255, 180, 0)
			};
			AddSubview(_refreshControl);

			_refreshControl.ValueChanged += (object sender, EventArgs e) => 
			{
				if(ValueChanged != null)
				{
					ValueChanged(sender,e);
				}
			};
		}

//		public override void SetNeedsLayout ()
//		{
//			ContentSize = new CGSize (Frame.Width, Frame.Height + 5f);
//			_startLabel.Frame =new CGRect (0, 0, Frame.Width, 30f);
//			_startLabel.Center = new CGPoint (Frame.Width / 2f, Frame.Height / 2f - 100f);
//			base.SetNeedsLayout ();
//		}

		public void EndRefreshing()
		{
			_refreshControl.EndRefreshing ();
		}
	}
}

