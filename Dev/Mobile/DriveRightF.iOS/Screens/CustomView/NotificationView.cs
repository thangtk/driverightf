﻿
using System;

using Foundation;
using UIKit;
using CoreGraphics;
using ObjCRuntime;

namespace DriveRightF.iOS
{
	[Register("NotificationView")]
	public partial class NotificationView : UIView
	{
		public NotificationView(CGRect frame) : base(frame)
		{
			Initialize ();
		}

		public NotificationView(IntPtr handler) : base(handler)
		{
			Initialize ();
		}

		private void Initialize(){
			var arr = NSBundle.MainBundle.LoadNib("NotificationView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			v.Frame = new CGRect (CGPoint.Empty, Frame.Size);
			this.AddSubview (v);
			//
			InitView();
		}

		private void InitView(){
			lblCount.Layer.CornerRadius = lblCount.Bounds.Width/2;
			lblCount.Layer.MasksToBounds = true;
			//
			var tgr = new UITapGestureRecognizer(()=> {
				Count = 0;
				if(OnTapped != null){
					OnTapped.Invoke();
				}
			});
			this.AddGestureRecognizer (tgr);
		}

		public event Action OnTapped;
		private int _count;

		public int Count {
			get { return _count; } 
			set {
				_count = value;
				lblCount.Text = _count.ToString ();
				lblCount.Hidden = _count <= 0;
			}
		}
	}
}

