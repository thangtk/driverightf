﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;
using ObjCRuntime;

namespace DriveRightF.iOS
{
	[Register("ButtonNewData")]
	public partial class ButtonNewData : UIView
	{
		private const double ANIMATE_DURATION = 0.4F;

		public event EventHandler OnClick;

		public ButtonNewData (CGRect frame) : base (frame)
		{
			InitView ();
			InitEvents ();
		}

		public ButtonNewData(IntPtr h) : base (h)
		{
			InitView ();
			InitEvents ();
		}

		private void InitView()
		{
			var arr = NSBundle.MainBundle.LoadNib("ButtonNewData", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			v.Frame = new CGRect (CGPoint.Empty, Frame.Size);
			v.Layer.CornerRadius = Frame.Height / 2;
			v.BackgroundColor = UIColor.FromRGB (237, 238, 242);

			AddSubview (v);

			lblTitle.Font = FontHub.HelveticaMedium13;
		}

		private void InitEvents()
		{
			AddGestureRecognizer(new UITapGestureRecognizer(() => {
				OnClickHandler (new EventArgs ());
			}));
		}

		protected virtual void OnClickHandler(EventArgs e)
		{
			EventHandler handler = OnClick;
			if (handler != null)
				handler (this, e);
		}

		public void Show()
		{
			UIView.Animate (ANIMATE_DURATION,
				() => {
					Frame = new CGRect(Frame.X, 73, Frame.Width, Frame.Height);
					Alpha = 1;
				}
			);
		}

		public void Hide()
		{
			UIView.Animate (ANIMATE_DURATION,
				() => {
					Frame = new CGRect(Frame.X, 39, Frame.Width, Frame.Height);
					Alpha = 0;
				}
			);
		}
	}
}

