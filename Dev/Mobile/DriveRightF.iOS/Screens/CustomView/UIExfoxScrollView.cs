﻿using System;
using UIKit;
using Foundation;
using Unicorn.Core;
using DriveRightF.Core;
using CoreGraphics;
using ObjCRuntime;
using ReactiveUI;
using System.Reactive.Linq;
using System.ComponentModel;
using Unicorn.Core.UI;
using DriveRightF.Core.iOS;
using System.Threading.Tasks;
using Unicorn.Core.iOS;
using System.Collections.Generic;

namespace DriveRightF.iOS
{
	[Register("UIExfoxScrollView")]
	public partial class UIExfoxScrollView : UIScrollView
	{
		List<UIView> subviewList = new List<UIView> ();
		CGPoint startPosition;
		CGPoint startContentOffset;

		public UIExfoxScrollView (IntPtr handle) : base (handle) { }

		public UIExfoxScrollView ()
		{
		}

		public override void SubviewAdded (UIView uiview)
		{
			base.SubviewAdded (uiview);
			int i = 0;
			for (; i < subviewList.Count; i++) {
				if (uiview.Frame.Y > subviewList [i].Frame.Y)
					break;
			}

			subviewList.Insert (i, uiview);
		}

		public override void TouchesBegan (Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			startPosition = (touches.AnyObject as UITouch).LocationInView (this);
			startContentOffset = ContentOffset;
		}

		public override void TouchesMoved (Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesMoved (touches, evt);

			var touch = (touches.AnyObject as UITouch).LocationInView (this);
			var verticalOffset = touch.Y - startPosition.Y;

			if (touch.Y < startPosition.Y) {
				foreach (var view in subviewList) {
					if (view.Frame.Y > startContentOffset.Y) {
						
					}
				}
			}
		}

		public override void TouchesEnded (Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
		}
	}
}

