﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;
using CoreAnimation;
using ObjCRuntime;
using Unicorn.Core.UI;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using DriveRightF.Core.Enums;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using DriveRightF.Core.Models;

namespace DriveRightF.iOS
{
	[Register ("TripTile")]
	public sealed partial class TripTile : UBaseView<TripTileViewModel>
	{
		private string _backgroundColor;
		private string _image;
		private string _overallImage;
		private AwardType _type;
		private TripMonth _trip;

		public event EventHandler OnClick;

		public TripMonth Trip {
			get
			{
				return _trip;
			}
			set
			{
				_trip = value;
			}
		}

		public string Image {
			set
			{
				_image = value;
				if (ViewModel != null) ViewModel.Image = value;
			}
		}

		public string OverallImage {
			set
			{
				_overallImage = value;
				if (ViewModel != null) ViewModel.OverallImage = value;
			}
		}

		public string BackgroundColor {
			set
			{
				_backgroundColor = value;
				if (ViewModel != null) ViewModel.Colors = value;
			}
		}

		public AwardType Type {
			get
			{
				return _type;
			}
			set
			{
				_type = value;

				switch (_type)
				{
				// Overall
					case AwardType.AwardLarge:
						//lblMoney.Hidden = true;
						lblLocation.Font = FontHub.HelveticaHvCn22;
						//lblMonth.Font = FontHub.HelveticaHvCn22;
						lblMonth.Hidden = true;
						overallImage.Hidden = false;
						lblScore.Font = FontHub.HelveticaHvCn22;
						break;
				// Monthly
					case AwardType.AwardSmall:
						//lblName.Text = "";
						//lblMoney.Hidden = true;
						overallImage.Hidden = true;
						break;
					default:
						break;
				}
			}
		}

		public TripTile() : base ()
		{
			InitView ();
			SetFont ();
			InitBinding ();
			InitEvent ();
		}

		public TripTile(TripTileViewModel vm) : base ()
		{
			InitView ();
			ViewModel = vm;
			SetColor (vm.Colors, vm.Trip == null ? null : vm.Trip.ImageUrl);
			if (vm.Trip != null) {
				SetTrip (vm.Trip);
			}
			SetFont ();
//			InitBinding ();
			RxApp.TaskpoolScheduler.Schedule(InitBinding);
			InitEvent ();
		}

		public TripTile(string color,string image) : base ()
		{
			InitView ();
			SetColor (color, image);
			SetFont ();
			InitBinding ();
			InitEvent ();
		}
		public TripTile(IntPtr h) : base (h)
		{
			InitView ();
			SetFont ();
			InitBinding ();
			InitEvent ();
		}

		private void InitEvent()
		{
			//TODO: remove multi touch of view
			this.ExclusiveTouch = true;

			AddGestureRecognizer (new UITapGestureRecognizer (() => {
				if( this.OnClick != null)
					this.OnClick (this, new EventArgs ());
			}));
		}

		private void InitBinding()
		{
			this.WhenAnyValue(x => x.ViewModel)
				.Where(vm => vm != null)
				.ObserveOn(RxApp.TaskpoolScheduler)
				.Subscribe (vm => HandleBinding ());
		}

		private void HandleBinding()
		{
			ViewModel.WhenAnyValue (vm => vm.Trip)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (SetTrip);

			ViewModel.WhenAnyValue (vm => vm.Colors, vm => vm.Trip.ImageUrl)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => SetColor (x.Item1, x.Item2));
		}

		private void SetColor(string color, string image){
			viewBackground.BackgroundColor = ColorHub.ColorFromHex (color);
			if (!String.IsNullOrEmpty (image)) 
			{	
				backgroundImage.Image = UIImage.FromFile (image);
				viewBackground.Alpha = 0.85f;
			}
			else
				viewBackground.Alpha = 1f;
		}

		void ClearData ()
		{
			// clear data
			lblMonth.Text = string.Empty;
			lblLocation.Text = string.Empty;
			lblScore.Text = string.Empty;
			backgroundImage.Image = null;
		}

		private void SetTrip(TripMonth trip){
			Trip = trip;
			if (trip == null) {
				ClearData ();
				return;
			}
			//
			lblMonth.Text = trip.Month.CompareTo(DateTime.MinValue) > 0 ? trip.Month.ToString ("yyyy-MMMM") : string.Empty;
			lblLocation.Text = trip.City ?? string.Empty;
			if( trip.Score > 0)
				lblScore.Text = string.Format ("{0}", Math.Round(trip.Score, 0));

			if (!String.IsNullOrEmpty (trip.ImageUrl))
				backgroundImage.Image = UIImage.FromFile (trip.ImageUrl);
		}

		private void InitView()
		{
			var arr = NSBundle.MainBundle.LoadNib ("TripTile", this, null);
			var v = Runtime.GetNSObject (arr.ValueAt (0)) as UIView;
			v.Frame = new CGRect (0, 0, Frame.Width, Frame.Height);
			AddSubview (v);
		}

		private void SetFont()
		{
			lblMonth.Font = FontHub.HelveticaLt17;
			lblLocation.Font = FontHub.HelveticaLt13;
			lblScore.Font = FontHub.HelveticaHvCn20; 
		}
	}
}
