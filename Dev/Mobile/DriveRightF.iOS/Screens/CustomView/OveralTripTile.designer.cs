// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	partial class OveralTripTile
	{
		[Outlet]
		UIKit.UIImageView imgArrowUp { get; set; }

		[Outlet]
		UIKit.UILabel lblScore { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (imgArrowUp != null) {
				imgArrowUp.Dispose ();
				imgArrowUp = null;
			}

			if (lblScore != null) {
				lblScore.Dispose ();
				lblScore = null;
			}
		}
	}
}
