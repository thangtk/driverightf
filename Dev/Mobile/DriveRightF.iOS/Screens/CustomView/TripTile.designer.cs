// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	partial class TripTile
	{
		[Outlet]
		UIKit.UIImageView backgroundImage { get; set; }

		[Outlet]
		UIKit.UILabel lblLocation { get; set; }

		[Outlet]
		UIKit.UILabel lblMonth { get; set; }

		[Outlet]
		UIKit.UILabel lblScore { get; set; }

		[Outlet]
		UIKit.UIImageView overallImage { get; set; }

		[Outlet]
		UIKit.UIView viewBackground { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (backgroundImage != null) {
				backgroundImage.Dispose ();
				backgroundImage = null;
			}

			if (lblLocation != null) {
				lblLocation.Dispose ();
				lblLocation = null;
			}

			if (lblMonth != null) {
				lblMonth.Dispose ();
				lblMonth = null;
			}

			if (lblScore != null) {
				lblScore.Dispose ();
				lblScore = null;
			}

			if (overallImage != null) {
				overallImage.Dispose ();
				overallImage = null;
			}

			if (viewBackground != null) {
				viewBackground.Dispose ();
				viewBackground = null;
			}
		}
	}
}
