﻿using System;
using UIKit;
using DriveRightF.Core.Models;
using Foundation;
using ObjCRuntime;
using Unicorn.Core.iOS;

namespace DriveRightF.iOS
{
	[Register ("LoadingControlView_AL")]
	public partial class LoadingControlView_AL : UIView
	{
		private UIRefreshControl _refreshControl;
		public event EventHandler ValueChanged;

		public string TextContent {
			get { return lblContent.Text;}
			set { lblContent.Text = value;}
		}

		public LoadingControlView_AL () 
		{
			InitView ();
			InitControls ();
		}

		public LoadingControlView_AL (IntPtr handle) : base(handle)
		{
			InitView ();
			InitControls ();
		}

		private void InitControls()
		{
			_refreshControl = new UIRefreshControl () {
				BackgroundColor = UIColor.White,
				TintColor = UIColor.FromRGB (255, 180, 0)
			};

			scrollView.AddSubview(_refreshControl);

			_refreshControl.ValueChanged += (object sender, EventArgs e) => 
			{
				if(ValueChanged != null)
				{
					ValueChanged(sender,e);
				}
			};
		}

		private void InitView ()
		{
			var arr = NSBundle.MainBundle.LoadNib ("LoadingControlView_AL", this, null);
			var v = Runtime.GetNSObject (arr.ValueAt (0)) as UIView;
			v.FixSupperView (this);
		}

		public void EndRefreshing()
		{
			_refreshControl.EndRefreshing ();
		}
	}
}

