// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	partial class BannerView
	{
		[Outlet]
		UIKit.UIImageView imgBannerBackground { get; set; }

		[Outlet]
		UIKit.UIImageView imgBannerIcon { get; set; }

		[Outlet]
		UIKit.UILabel lblBannerTitle { get; set; }

		[Outlet]
		UIKit.UILabel lblBannerValue { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (imgBannerBackground != null) {
				imgBannerBackground.Dispose ();
				imgBannerBackground = null;
			}

			if (imgBannerIcon != null) {
				imgBannerIcon.Dispose ();
				imgBannerIcon = null;
			}

			if (lblBannerTitle != null) {
				lblBannerTitle.Dispose ();
				lblBannerTitle = null;
			}

			if (lblBannerValue != null) {
				lblBannerValue.Dispose ();
				lblBannerValue = null;
			}
		}
	}
}
