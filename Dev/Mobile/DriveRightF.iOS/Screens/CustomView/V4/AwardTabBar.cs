﻿using System;
using UIKit;
using Foundation;
using ObjCRuntime;
using System.Collections.Generic;
using DriveRightF.Core;
using Unicorn.Core.iOS;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

namespace DriveRightF.iOS
{
	[Register("AwardTabBar")]
	public partial class AwardTabBar : BaseTabBar<AwardTabBarViewModel, AwardTabItem>
	{
		#region implemented abstract members of BaseTabBar

		public override UIView ContentView {
			get {
				return vContent;
			}
		}

		#endregion

		private UIView _rootView;
		private const float LINE_PADDING = 10f;
		public AwardTabBar ()
		{
			InitializeView();
		}

		public AwardTabBar(IntPtr handle) : base(handle)
		{
			InitializeView();
		}

		private void InitializeView()
		{
			LoadNib();
		
			InitBinding();
		}

		private void UpdateUILineAnimation ()
		{
			constraintWidthLine.Constant = WidthTabItem - 2 * LINE_PADDING;
			constraintLeftLine.Constant = LINE_PADDING;
		}

		private void AnimationLine(int index)
		{
			constraintLeftLine.Constant = LINE_PADDING + index * WidthTabItem;
			UIView.Animate (0.3f, () => this.LayoutIfNeeded());
		}

		public override void AddTabItems (List<AwardTabItem> items)
		{
			base.AddTabItems (items);
			CreateLineMiddle ();
			UpdateUILineAnimation ();
		}

		private void CreateLineMiddle()
		{
			for (int i = 0; i < TabItems.Count - 1; i++) {
				CreateLineMiddle (i);
			}
		}

		private void CreateLineMiddle(int index)
		{
			UIView vLineMiddle = new UIView () {
				BackgroundColor = UIColor.FromRGB(241,241,241),
			};

			nfloat widthLine = 1;

			vLineMiddle.TranslatesAutoresizingMaskIntoConstraints = false;
			vLineMiddle.AddConstraint(NSLayoutConstraint.Create(vLineMiddle, NSLayoutAttribute.Width,NSLayoutRelation.Equal,null,NSLayoutAttribute.NoAttribute,0,widthLine));

			_rootView.Add (vLineMiddle);
			NSLayoutConstraint[] constraints = {
				NSLayoutConstraint.Create(vLineMiddle, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _rootView, NSLayoutAttribute.Top, 1, 10),
				NSLayoutConstraint.Create(vLineMiddle, NSLayoutAttribute.Leading, NSLayoutRelation.Equal, _rootView, NSLayoutAttribute.Leading, 1, WidthTabItem * (index + 1) - widthLine/2 ),
				NSLayoutConstraint.Create(vLineMiddle, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _rootView, NSLayoutAttribute.Bottom, 1, -10),
			};

			_rootView.AddConstraints (constraints);
		}

		private void InitBinding()
		{
			this.WhenAnyValue (x => x.ViewModel)
				.Where (x => x != null)
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (x => {
					HandleBinding ();
			});
		}

		private void HandleBinding ()
		{
			ViewModel.WhenAnyValue (v => v.ItemSelected)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (v => AnimationLine (v));
		}

		private void LoadNib()
		{
			var nibObjects = NSBundle.MainBundle.LoadNib("AwardTabBar", this, null);
			_rootView = Runtime.GetNSObject((nibObjects.ValueAt(0))) as UIView;
			_rootView.FixSupperView (this);
		}
	}
}

