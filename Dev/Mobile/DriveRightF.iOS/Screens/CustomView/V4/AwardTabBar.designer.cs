// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	partial class AwardTabBar
	{
		[Outlet]
		UIKit.NSLayoutConstraint constraintLeftLine { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint constraintWidthLine { get; set; }

		[Outlet]
		UIKit.UIView vContent { get; set; }

		[Outlet]
		UIKit.UIView vLineAnimate { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (constraintLeftLine != null) {
				constraintLeftLine.Dispose ();
				constraintLeftLine = null;
			}

			if (constraintWidthLine != null) {
				constraintWidthLine.Dispose ();
				constraintWidthLine = null;
			}

			if (vLineAnimate != null) {
				vLineAnimate.Dispose ();
				vLineAnimate = null;
			}

			if (vContent != null) {
				vContent.Dispose ();
				vContent = null;
			}
		}
	}
}
