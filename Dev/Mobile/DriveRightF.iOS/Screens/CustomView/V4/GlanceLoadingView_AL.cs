﻿using System;
using UIKit;
using Unicorn.Core.UI;
using Foundation;
using ObjCRuntime;
using Unicorn.Core.iOS;

namespace DriveRightF.iOS
{
	[Register ("GlanceLoadingView_AL")]
	public partial class GlanceLoadingView_AL : UBaseView
	{
		public string LoadingText {
			get { return lblLoading.Text;}
			set { lblLoading.Text = value;}
		}
			
		public GlanceLoadingView_AL () : base()
		{
			InitView ();
		}

		public GlanceLoadingView_AL (IntPtr handle) : base(handle)
		{
			InitView ();
		}

		private void InitView ()
		{
			var arr = NSBundle.MainBundle.LoadNib ("GlanceLoadingView_AL", this, null);
			var v = Runtime.GetNSObject (arr.ValueAt (0)) as UIView;
			v.FixSupperView (this);
		}

		public void StartAnimating()
		{
//			_indicatorView.StartAnimating ();
			indicator.StartAnimating();
		}

		public void StopAnimating()
		{
//			_indicatorView.StopAnimating ();
			indicator.StopAnimating();
		}
	}
}