﻿using System;
using DriveRightF.Core;
using Foundation;
using ObjCRuntime;
using UIKit;
using Unicorn.Core.iOS;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Linq;

namespace DriveRightF.iOS
{
	[Register("AwardTabItem")]
	public partial class AwardTabItem : BaseTabItem<UTabItemViewModel>
	{
		private UIView _rootView;

		public AwardTabItem (UTabItemViewModel viewModel) : base()
		{
			InitializeView ();

			ViewModel = viewModel;

			lblTabText.Text = viewModel.NameTab;
			lblTabText.TextColor = viewModel.IsSelected ? UIColor.FromRGB(255, 180, 0) : UIColor.FromRGB(153, 153, 153);

			RxApp.TaskpoolScheduler.Schedule (() => HandleBinding());
		}
		public AwardTabItem(IntPtr handle) : base(handle)
		{
			InitializeView();
		}

		private void InitializeView()
		{
			LoadNib ();
			InitBinding ();
		}

		private void LoadNib()
		{
			var nibObjects = NSBundle.MainBundle.LoadNib("AwardTabItem", this, null);
			_rootView = Runtime.GetNSObject((nibObjects.ValueAt(0))) as UIView;
			_rootView.FixSupperView (this);
		}
			

		private void InitBinding ()
		{
//			this.WhenAnyValue (x => x.ViewModel)
//				.Where (vm => vm != null)
//				.ObserveOn (RxApp.TaskpoolScheduler)
//				.Subscribe (vm => HandleBinding ());

		}

		private void HandleBinding ()
		{
			ViewModel.WhenAnyValue (x => x.IsSelected)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					lblTabText.TextColor = x ? UIColor.FromRGB(255, 180, 0) : UIColor.FromRGB(153, 153, 153);
				});

			ViewModel.WhenAnyValue (x => x.NameTab)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					lblTabText.Text = x;
				});
		}
	}
}

