// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	partial class HomeListContentView<TCellModel, TCellView>
	{
		[Outlet]
		DriveRightF.iOS.GlanceLoadingView_AL vLoading { get; set; }

		[Outlet]
		DriveRightF.iOS.LoadingControlView_AL vRefresh { get; set; }

		[Outlet]
		UIKit.UITableView vTable { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (vLoading != null) {
				vLoading.Dispose ();
				vLoading = null;
			}

			if (vRefresh != null) {
				vRefresh.Dispose ();
				vRefresh = null;
			}

			if (vTable != null) {
				vTable.Dispose ();
				vTable = null;
			}
		}
	}
}
