﻿using System;
using Unicorn.Core.UI;
using DriveRightF.Core;
using ReactiveUI;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using Foundation;
using UIKit;
using ObjCRuntime;
using Unicorn.Core.iOS;

namespace DriveRightF.iOS
{
	[Register ("BannerView")]
	public partial class BannerView : UBaseView<BannerViewModel>
	{
		public event EventHandler OnClick;
		private UIView _rootView;

		public BannerView (BannerViewModel viewModel): base()
		{
			ViewModel = viewModel;
			InitView ();
			InitControls ();
			InitBinding ();

		}

		public BannerView (): base()
		{
			InitView ();
			InitControls ();
//			InitBinding();
		}
		public BannerView (IntPtr h) : base (h)
		{
			InitView ();
			InitControls ();
//			InitBinding ();
		}

		private void InitView ()
		{
			var arr = NSBundle.MainBundle.LoadNib ("BannerView", this, null);
			_rootView = Runtime.GetNSObject (arr.ValueAt (0)) as UIView;
			_rootView.FixSupperView (this);
		}

		public void CreateViewModel(BannerViewModel viewModel)
		{
			//TODO: update all value in here.
			ViewModel = viewModel;
			imgBannerBackground.Image = UIImage.FromFile(viewModel.BannerBackground);
			imgBannerIcon.Image = UIImage.FromFile(viewModel.BannerIcon);
			lblBannerValue.Text = string.IsNullOrEmpty(viewModel.BannerValue) ? "--" : viewModel.BannerValue;
			lblBannerTitle.Text = viewModel.BannerTile;
			InitBinding ();
		}

		private void InitControls()
		{
			InitFonts ();
			InitEvents();
		}

		private void InitFonts()
		{
			//TODO: inti fonts
			//			lblBannerTitle
		}

		public void InitEvents()
		{
			//TODO: handle click, touch here.
			this.AddGestureRecognizer (new UITapGestureRecognizer (() =>
				{
					if(OnClick != null)
						this.OnClick (this, new EventArgs ());
				}));
		}
			
		public void HandleBinding()
		{
			ViewModel.WhenAnyValue(x => x.BannerBackground)
				.Where(x => !string.IsNullOrEmpty(x))
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe( x =>{
					imgBannerBackground.Image = UIImage.FromFile(x);
				});

			ViewModel.WhenAnyValue (x => x.BannerIcon)
				.Where(x => !string.IsNullOrEmpty(x))
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					imgBannerIcon.Image = UIImage.FromFile(x);
				});

			ViewModel.WhenAnyValue (x => x.BannerValue)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					lblBannerValue.Text = string.IsNullOrEmpty(x) ? "--" : x;
				});

			ViewModel.WhenAnyValue (x => x.BannerTile)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					lblBannerTitle.Text = x;
				});
		}

		public void InitBinding()
		{
			RxApp.TaskpoolScheduler.Schedule (() => 
				this.WhenAnyValue (x => x.ViewModel).Where (x => x != null).Subscribe (x => HandleBinding ()));
		}
	}
}

