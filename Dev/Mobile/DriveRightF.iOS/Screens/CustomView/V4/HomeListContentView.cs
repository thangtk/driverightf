﻿using System;
using UIKit;
using Foundation;
using Unicorn.Core.UI;
using ObjCRuntime;
using Unicorn.Core.iOS;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using DriveRightF.Core;

namespace DriveRightF.iOS
{
	[Register ("HomeListContentView")]
	public partial class HomeListContentView<TCellModel, TCellView> : UBaseView<HomeListContentViewModel<TCellModel>>
		where TCellView:  UITableViewCell, IDriveRightTableViewCell<TCellModel>
	{
		protected DriveRightTableViewSource<TCellModel,TCellView> _tableSource;
		protected UIRefreshControl _refreshControl;
		public event EventHandler RefreshChanged;

		protected UITableView TableView
		{
			get {
				return vTable;
			}
		}

		public HomeListContentView (HomeListContentViewModel<TCellModel> viewModel) : base()
		{
			ViewModel = viewModel;
			InitView ();
			InitControls ();
			RxApp.TaskpoolScheduler.Schedule (() => InitBinding ());
		}
		public HomeListContentView () : base()
		{
			InitView ();
		}

		public HomeListContentView (IntPtr handle) : base(handle)
		{
			InitView ();
		}

		private void InitView ()
		{
			var arr = NSBundle.MainBundle.LoadNib ("HomeListContentView", this, null);
			var v = Runtime.GetNSObject (arr.ValueAt (0)) as UIView;
			v.FixSupperView (this);
		}

		private void HandleRefreshControl ()
		{
			_refreshControl = new UIRefreshControl () {
				BackgroundColor = UIColor.White,
				TintColor = UIColor.FromRGB (255, 180, 0)
			};
			vTable.Add (_refreshControl);
			vRefresh.ValueChanged += (object sender, EventArgs e) =>  {
				//TODO: call service.
				if(RefreshChanged != null)
				{
					RefreshChanged(sender,e);
				}
				vRefresh.EndRefreshing ();
			};
			_refreshControl.ValueChanged += (object sender, EventArgs e) =>  {
				//TODO: call service.
				if(RefreshChanged != null)
				{
					RefreshChanged(sender,e);
				}
				_refreshControl.EndRefreshing ();
			};
		}

		private void InitControls()
		{
			InitTableSource ();
			vTable.Source = _tableSource;

			HandleRefreshControl ();

			UpdateUI ();
		}

		public virtual void UpdateUI()
		{
		}

		public virtual void InitTableSource()
		{
			_tableSource = new DriveRightTableViewSource<TCellModel, TCellView> ();
		}

		private void DefaultContentVisibility(){
			vTable.Hidden = true;
			vRefresh.Hidden = false;
		}

		private void InitBinding()
		{
			//TODO: reload tableView
			ViewModel.WhenAnyValue(x => x.DataSources)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(x => {
					_tableSource.Items = x;
					vTable.ReloadData();
				});

			ViewModel.WhenAnyValue( x => x.NoDataContent)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(x => {
					vRefresh.TextContent = x;
				});
			ViewModel.WhenAnyValue (x => x.State)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					if(x == DriveRightF.Core.Enums.ViewState.NoDataFound)
					{
						vTable.Hidden = true;
						vLoading.Hidden = true;
						vRefresh.Hidden = false;
					}
					else if (x == DriveRightF.Core.Enums.ViewState.DataDisplaying)
					{
						vTable.Hidden = false;
						vLoading.Hidden = true;
						vRefresh.Hidden = true;
					}
			});
			//TODO: reload bannerView.
		}

	}
}

