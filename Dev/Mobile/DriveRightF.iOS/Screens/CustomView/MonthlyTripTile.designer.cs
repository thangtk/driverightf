// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	partial class MonthlyTripTile
	{
		[Outlet]
		UIKit.UIImageView imgBcakground { get; set; }

		[Outlet]
		UIKit.UILabel lblLocal { get; set; }

		[Outlet]
		UIKit.UILabel lblMonth { get; set; }

		[Outlet]
		UIKit.UILabel lblScore { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (imgBcakground != null) {
				imgBcakground.Dispose ();
				imgBcakground = null;
			}

			if (lblLocal != null) {
				lblLocal.Dispose ();
				lblLocal = null;
			}

			if (lblMonth != null) {
				lblMonth.Dispose ();
				lblMonth = null;
			}

			if (lblScore != null) {
				lblScore.Dispose ();
				lblScore = null;
			}
		}
	}
}
