﻿using System;
using UIKit;
using Foundation;
using Unicorn.Core;
using DriveRightF.Core;
using CoreGraphics;
using ObjCRuntime;
using ReactiveUI;
using System.Reactive.Linq;
using System.ComponentModel;
using Unicorn.Core.UI;
using DriveRightF.Core.iOS;
using System.Threading.Tasks;
using Unicorn.Core.iOS;

namespace DriveRightF.iOS
{
	[Register("ImageMutilStatus"), DesignTimeVisible(true)]
	public partial class ImageMutilStatus : UBaseView<ImageMultiStatusViewModel>
	{
		private bool _animated = false;
		public ImageMutilStatus (IntPtr handle) : base (handle) { }

		public ImageMutilStatus (CGRect frame) : base(frame)
		{
			Initialize ();
		}

		public ImageMutilStatus (CGRect frame, ImageMultiStatusViewModel viewModel) : base(frame)
		{
			Initialize ();
			SetViewModel(viewModel);
		}

		private void Initialize(){
			LoadNib ();
			InitControls ();
			InitBinding ();
		}

		private void LoadNib(){
			var nibObjects = NSBundle.MainBundle.LoadNib ("ImageMutilStatus", this, null);
			var v = Runtime.GetNSObject ((nibObjects.ValueAt (0))) as UIView;
			v.Frame = new CGRect(0,0, Frame.Width, Frame.Height); // Very important
			v.LayoutIfNeeded();
			this.AddSubview (v);
		}

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			Initialize ();
		}

		public async void SetViewModel(ImageMultiStatusViewModel viewModel){
			await Task.Delay (1);
			ViewModel = viewModel;
			if (ViewModel != null) {
				InitBinding ();
			}
		}

		private void InitControls(){
		}

		private void InitBinding ()
		{
//			var LoadIconCommand = ReactiveCommand.Create ();
//			ViewModel.WhenAnyValue (vm => vm.Icon, vm => vm.IsGrayStyle)
//				.ObserveOn (RxApp.TaskpoolScheduler)
//				.Select (url => {
//					return ImageUtil.DownloadImage ("http://xamarin.com/resources/design/home/devices.png", "icon_");
//				})
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (imagePath => {
//						Icon.Image = UIImage.FromFile(imagePath.Result);
//					});
			this.WhenAnyValue (view => view.ViewModel)
				.Where (vm => vm != null)
				.Subscribe (vm => SubcribleBinding ());
			
		}

		private void SubcribleBinding()
		{
			ViewModel.WhenAnyValue (vm => vm.Icon, vm => vm.IsGrayStyle)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Where (x => !String.IsNullOrEmpty(x.Item1))
				.Subscribe (x => {
					var img = UIImage.FromBundle(x.Item1);
					if(img != null){
						if(x.Item2){
							// Item2 represents for Gray style
							img = GrayScaleViewUtil.ConvertToGrayScale (img);
						}
						Icon.Image = img;
					}
				});

			ViewModel.WhenAnyValue (vm => vm.IconURl, vm => vm.IsGrayStyle)
				.Where (x => !String.IsNullOrEmpty(x.Item1))
				.ObserveOn(RxApp.TaskpoolScheduler)
				.Select(x => {
					try {
						var data = NSData.FromUrl(new NSUrl(x.Item1));
						UIImage img = null;
						if(data != null){
							img = UIImage.LoadFromData(data);
						}
						//
						if(img != null){
							if(x.Item2){
								// Item2 represents for Gray style
								img = GrayScaleViewUtil.ConvertToGrayScale (img);
							}
							return img;
						}
					} catch (Exception ex) {
						(new ErrorHandler()).Handle(ex, "Multiple status image");
					}
					return null;
				})
//				.Where(x => x != null)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe (img => {
					Icon.Image = img ?? UIImage.FromBundle("award_coffee.png");
				});

			ViewModel.WhenAnyValue (vm => vm.StatusIcon)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Where (x => x != null)
				.Subscribe (icon => {
					var img = UIImage.FromBundle(icon);
					if(img == null){
						StatusIcon.Hidden = true;						
					} else {
						StatusIcon.Image = img;	
						StatusIcon.Hidden = false;
						// TODO: Animate
						StatusIcon.Scale (0.5, 3, 1, 1.5f);
						//						if(!_animated){
						//							StatusIcon.Scale (0.5, 3, 1, 1.5f);
						//							_animated = true;
						//						}
					}
				});
		}
	}
}

