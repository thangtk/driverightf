﻿using System;
using Foundation;
using DriveRightF.Core;
using UIKit;
using CoreGraphics;
using ObjCRuntime;
using ReactiveUI;
using System.Reactive.Linq;
using Unicorn.Core;
using DriveRightF.Core.Enums;
using Unicorn.Core.Navigator;
using System.Reactive.Concurrency;
using DriveRightF.Core.Managers;

namespace DriveRightF.iOS
{
	[Register ("DMenu")]
	public partial class DMenu: DriveRightBaseView<MenuViewModel>
	{
		private UIView _rootView;
		//		public event EventHandler<string> OnBtnSettingTouch;
		//		public event EventHandler<string> OnBtnAboutTouch;
		//		public event EventHandler<string> OnBtnHelpTouch;
		//		public event EventHandler<string> OnBtnLegalTouch;

		public DMenu() : base ()
		{
			InitializeView ();
		}

		public DMenu(IntPtr h) : base (h)
		{
			InitializeView ();
		}

		public DMenu(CGRect frame) : base (frame)
		{
			InitializeView ();
		}

		private void LoadNib()
		{
			var nibObjects = NSBundle.MainBundle.LoadNib ("DMenu", this, null);
			_rootView = Runtime.GetNSObject ((nibObjects.ValueAt (0))) as UIView;
			FixSupperView (this, _rootView);
		}

		private void InitializeView()
		{
			LoadNib ();
			InitControls ();
			InitTranslator ();
//			RxApp.TaskpoolScheduler.Schedule (InitBinding);
		}

		private void FixSupperView(UIView supperView, UIView childView)
		{
			childView.TranslatesAutoresizingMaskIntoConstraints = false;
			supperView.Add (childView);
			NSLayoutConstraint[] constraints = {
				NSLayoutConstraint.Create (childView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create (childView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create (childView, NSLayoutAttribute.Trailing, NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Trailing, 1, 0),
				NSLayoutConstraint.Create (childView, NSLayoutAttribute.Leading, NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Leading, 1, 0)
			};
			supperView.AddConstraints (constraints);
		}

		private void InitTranslator()
		{
			lblSetting.Text = Translator.Translate ("BUTTON_SETTING");
			lblAbout.Text = Translator.Translate ("BUTTON_ABOUT");
			lblHelp.Text = Translator.Translate ("BUTTON_HELP");
			lblLegal.Text = Translator.Translate ("BUTTON_LEGAL");
		}

		private void InitControls()
		{
			tapSetting.AddTarget (() => DependencyService.Get<INavigator> ().Navigate ((int)Screen.Setting));

			tapAbout.AddTarget (() => DependencyService.Get<INavigator> ().Navigate ((int)Screen.About));

			tapHelp.AddTarget (() => DependencyService.Get<INavigator> ().Navigate ((int)Screen.Help));

			tapLegal.AddTarget (() => DependencyService.Get<INavigator> ().Navigate ((int)Screen.Legal));
		}

//		private void InitBinding()
//		{
//			this.WhenAnyValue (x => x.ViewModel)
//				.Where (vm => vm != null)
//				.ObserveOn (RxApp.TaskpoolScheduler)
//				.Subscribe (vm => HandleBinding ());
//		}
//
//		private void HandleBinding()
//		{
//			ViewModel.LoadDataCommand.Execute (null);
//		}
	}
}

