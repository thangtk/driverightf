﻿using System;
using Unicorn.Core.UI;
using DriveRightF.Core.Models;
using DriveRightF.Core.ViewModels;
using Foundation;
using DriveRightF.Core.Enums;
using DriveRightF.Core;
using CoreAnimation;
using UIKit;
using ReactiveUI;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using CoreGraphics;
using ObjCRuntime;
using Unicorn.Core;

namespace DriveRightF.iOS
{
	[Register("AwardSquareItem")]
	public sealed partial class AwardSquareItem : UBaseView<AwardTileViewModel>
	{
		private string _backgroundColor;
		private string _image;
		private AwardType _type;
		//		private Award _award;
		private CALayer _layer;

		public event EventHandler OnClick;

		//		public Award Award {
		//			get {
		//				return _award;
		//			}
		//			set {
		//				_award = value;
		//			}
		//		}

		public string Image
		{
			set
			{
				_image = value;
				if(ViewModel != null) ViewModel.Image = value;
			}
		}

		//		public string BackgroundColor {
		//			set {
		//				_backgroundColor = value;
		//				if (ViewModel != null)
		//					ViewModel.Colors = value;
		//			}
		//		}

		public bool IsUsed
		{
			get
			{
				return !imgUsed.Hidden;
			}
			set
			{
				imgUsed.Hidden = !value;
				lblExpire.Hidden = value;
			}
		}

		public AwardType Type
		{
			get
			{
				return _type;
			}
			set
			{
				SetFont();
				_type = value;
				switch (_type)
				{
					case AwardType.AwardLarge:
						lblMoney.Hidden = true;
						lblExpire.Font = FontHub.HelveticaHvCn22;
						lblName.Font = FontHub.HelveticaHvCn22;
						lblTitle.Font = FontHub.HelveticaHvCn22;
						break;
					case AwardType.AwardSmall:
						lblName.Hidden = true;
						lblMoney.Hidden = true;
						lblName.Font = FontHub.HelveticaThin17;
						lblTitle.Font = FontHub.HelveticaThin17;
						break;
					case AwardType.CardLarge:
						lblExpire.Font = FontHub.HelveticaHvCn22;
						lblName.Font = FontHub.HelveticaHvCn22;
						lblTitle.Font = FontHub.HelveticaHvCn22;
						lblMoney.Font = FontHub.HelveticaHvCn20; 
						break;
					case AwardType.CardSmall:
						lblName.Hidden = true;
						lblTitle.Font = FontHub.HelveticaThin17;
						lblExpire.Font = FontHub.HelveticaThin13;
//					lblMoney.Font = FontHub.HelveticaHvCn20; 
						break;
					default:
						break;
				}
			}
		}

		public AwardSquareItem(AwardTileViewModel model) : base()
		{
			InitView();
			ViewModel = model;
			if(ViewModel.Award != null)
			{
				SetAward(ViewModel.Award);
				SetColor(ViewModel.Colors, ViewModel.Award.IconURL);
			}
			else
			{
				SetColor(ViewModel.Colors, string.Empty);
			}
//			this.Award = ViewModel.Award;
			RxApp.TaskpoolScheduler.Schedule(HandleBinding);
			InitEvent();
		}

		public AwardSquareItem(IntPtr h) : base(h)
		{
			InitView();
			InitBinding();
			InitEvent();
		}

		private void InitView()
		{
			var arr = NSBundle.MainBundle.LoadNib("AwardSquareItem", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			var frame = Frame;
			v.Frame = new CGRect (0, 0, frame.Width, frame.Height);
			Add(v);
			SetFont();

//			var label = new UILabel (new CGRect (-4, 5, 25, 20));
//			label.Text = Translator.Translate("AWARD_USED");
//			label.TextColor = UIColor.DarkTextColor;
//			label.TextAlignment = UITextAlignment.Center;
//			label.Font = FontHub.HelveticaCn8;
//			label.Transform = CGAffineTransform.MakeRotation((nfloat)Math.PI / 5);
//			imgUsed.AddSubview(label);
		}

		private void InitEvent()
		{
			//TODO: remove multi touch of view.
			this.ExclusiveTouch = true;
			AddGestureRecognizer(new UITapGestureRecognizer (() => this.OnClick(this, new EventArgs ())));
		}

		private void InitBinding()
		{
			this.WhenAnyValue(x => x.ViewModel)
				.ObserveOn(RxApp.TaskpoolScheduler)
				.Where(vm => vm != null)
				.Subscribe(vm => HandleBinding());

		}

		private void HandleBinding()
		{
			ViewModel.WhenAnyValue(vm => vm.Award)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(award => SetAward(award));

			ViewModel.WhenAnyValue(vm => vm.Colors, vm => vm.Award.IconURL)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(x => SetColor(x.Item1, x.Item2));
		}

		private void SetColor(string color, string image)
		{

			this.BackgroundColor = ColorHub.ColorFromHex(color);
			viewBgColor.BackgroundColor = ColorHub.ColorFromHex(color);

			if(!String.IsNullOrEmpty(image))
			{
				viewBgColor.Layer.Opacity = 0.8f;
				backgroundImage.Image = UIImage.FromFile(image);
			}
			else
			{
				viewBgColor.Layer.Opacity = 1f;
			}
		}

		private void SetAward(Award award)
		{
			if (award.Id > 0)
			{
				lblName.Text = award.Sponsor;
				lblMoney.Text = string.Format ("{0} {1}", award.Value.ToString (), Translator.Translate (award.Unit));
				if (award.Value == 0) 
					lblMoney.Hidden = true;
				else 
					lblMoney.Hidden = false;

				//TODO: need use Datetime.Now not use UtcNow
				// Reset datetime to 0h0m0s by method DateTime.Date when subtract time.
				int day = award.Expiry.Date.Subtract(DateTime.Now.Date).Days;

				//int day = award.Expiry.Subtract (DateTime.UtcNow).Days;
//				int day = award.Expiry.Day - DateTime.UtcNow.Day;
				lblExpire.Text = string.Format ("{0}: {1} {2}", Translator.Translate ("AWARD_HOME_EXP"), day, day > 1 ? Translator.Translate ("ACCOUNT_DAYS") : Translator.Translate ("ACCOUNT_DAY"));

				lblTitle.Text = award.Name;

//				if (award.Type == "IDA")
//					DependencyService.Get<PopupManager> ().ShowAlert ("", award.IconURL);

				if (!String.IsNullOrEmpty (award.IconURL))
				{
					backgroundImage.Image = UIImage.FromFile (award.IconURL);
				}		
			}
		
			else
			{
				lblName.Text = string.Empty;
				lblMoney.Text = string.Empty;

				lblExpire.Text = string.Empty;
				lblTitle.Text = string.Empty;

				backgroundImage.Image = null;
			}
		}

		private void SetFont()
		{
			lblTitle.Font = FontHub.HelveticaLt17;
			lblExpire.Font = FontHub.HelveticaLt13;
			lblMoney.Font = FontHub.HelveticaHvCn20; 
			lblName.Font = FontHub.HelveticaHvCn22;
		}
			
	}

}

