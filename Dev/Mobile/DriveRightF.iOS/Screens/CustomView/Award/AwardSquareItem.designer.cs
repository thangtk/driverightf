// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	partial class AwardSquareItem
	{
		[Outlet]
		UIKit.UIImageView backgroundImage { get; set; }

		[Outlet]
		UIKit.UIImageView imgUsed { get; set; }

		[Outlet]
		UIKit.UILabel lblExpire { get; set; }

		[Outlet]
		UIKit.UILabel lblMoney { get; set; }

		[Outlet]
		UIKit.UILabel lblName { get; set; }

		[Outlet]
		UIKit.UILabel lblTitle { get; set; }

		[Outlet]
		UIKit.UIView viewBgColor { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (backgroundImage != null) {
				backgroundImage.Dispose ();
				backgroundImage = null;
			}

			if (lblExpire != null) {
				lblExpire.Dispose ();
				lblExpire = null;
			}

			if (lblMoney != null) {
				lblMoney.Dispose ();
				lblMoney = null;
			}

			if (lblName != null) {
				lblName.Dispose ();
				lblName = null;
			}

			if (lblTitle != null) {
				lblTitle.Dispose ();
				lblTitle = null;
			}

			if (viewBgColor != null) {
				viewBgColor.Dispose ();
				viewBgColor = null;
			}

			if (imgUsed != null) {
				imgUsed.Dispose ();
				imgUsed = null;
			}
		}
	}
}
