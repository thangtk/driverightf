﻿
using System;

using Foundation;
using UIKit;
using Unicorn.Core.UI;
using CoreGraphics;

namespace DriveRightF.iOS
{
	public partial class GlanceLoadingView : UBaseView
	{
		//		private UIView _rootView;
		private UIActivityIndicatorView _indicatorView;
		private UILabel _lblTag;

		public string LabelText
		{
			get
			{
				return _lblTag.Text;
			}
			set
			{
				_lblTag.Text = value;
			}
		}

		//		public GlanceLoadingView () : base(new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height))
		//		{
		//			InitControls ();
		//		}

		public GlanceLoadingView(CGRect frame) : base(frame)
		{
			InitControls();
		}

		public GlanceLoadingView(NSCoder coder) : base(coder)
		{
			InitControls();
		}

		public GlanceLoadingView(NSObjectFlag t) : base(t)
		{
			InitControls();
		}

		public GlanceLoadingView(IntPtr handle) : base(handle)
		{
			InitControls();
		}

		public UIActivityIndicatorViewStyle IndicatorViewStyle
		{
			get
			{
				return _indicatorView.ActivityIndicatorViewStyle;
			}
			set
			{
				_indicatorView.ActivityIndicatorViewStyle = value;
			}
		}

		private void InitControls()
		{
			_indicatorView = new UIActivityIndicatorView (new CGRect (0, 0, 30, 30));
			_indicatorView.Center = new CGPoint (Frame.Width / 2, Frame.Height / 2 - 100); // FAKE
			_indicatorView.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge;
			_indicatorView.Color = UIColor.FromRGB(255, 180, 0);
			AddSubview(_indicatorView);

			_lblTag = new UILabel (new CGRect (0, _indicatorView.Frame.Bottom + 10, Frame.Width, 20f));
			_lblTag.TextAlignment = UITextAlignment.Center;
			_lblTag.TextColor = UIColor.FromRGB(255, 180, 0);
			_lblTag.Font = FontHub.HelveticaLt15;
			LabelText = "Requesting...";
			AddSubview(_lblTag);

//			_rootView = new UIView (new CGRect (0, _indicatorView.Frame.Y - 15f, Frame.Width, 75));
//			_rootView.BackgroundColor = UIColor.Clear;
//			_rootView.Layer.CornerRadius = 5f;
//			AddSubview (_rootView);
//			SendSubviewToBack (_rootView);
		}

		//		public override void MovedToSuperview ()
		//		{
		//			base.MovedToSuperview ();
		//			this.Center = new CGPoint(Frame.Width/2, Frame.Height/2 - 50f);
		//		}

		public override void SetNeedsLayout()
		{
			base.SetNeedsLayout();
		}

		public void StartAnimating()
		{
			_indicatorView.StartAnimating();
		}

		public void StopAnimating()
		{
			_indicatorView.StopAnimating();
		}
	}
}

