﻿
using System;

using Foundation;
using UIKit;
using CoreGraphics;
using ObjCRuntime;

namespace DriveRightF.iOS
{
	public partial class UICheckBox : UIView
	{
		private bool _isChecked;

		public EventHandler OnTouch { get; set; }
		public event EventHandler<ValueChangedEventArgs> ValueChanged;

		public string Value { get; set; }

		public bool Enabled { get; set; }
		public UIFont Font 
		{
			get
			{
				return LblText.Font;
			}
			set
			{
				LblText.Font = value;
			}
		}
		public string Label
		{
			get
			{
				return LblText.Text;
			}
			set
			{
				LblText.Text = value;
			}
		}

		public string ControlTag { get; set; }

		public bool IsChecked
		{
			get
			{
				return _isChecked;
			}
			set
			{
				_isChecked = value;
				UpdateViewState();
				if (ValueChanged != null)
				{
					var ec = new ValueChangedEventArgs(_isChecked.ToString());
					ValueChanged(this, ec);
				}

			}
		}

		public override CGRect Frame
		{
			get
			{
				return base.Frame;
			}
			set
			{
				base.Frame = value;
			}
		}

		public UIColor TextColor
		{
			get
			{
				return LblText.TextColor;
			}
			set
			{
				LblText.TextColor = value;
			}
		}

		public UICheckBox(IntPtr handle) : base(handle)
		{
			InitializeView();
		}

		public UICheckBox()
		{
			InitializeView();
		}

		void InitializeView()
		{
			LoadNib();

			InitBehaviorsMapping();
			InitCustomization();
		}

		private void LoadNib()
		{
			LblText = new UILabel (new CGRect(40, 0, 200, 25));
			LblText.Font = FontHub.HelveticaMedium15;
			LblText.TextAlignment = UITextAlignment.Left;
			LblText.TextColor = UIColor.Cyan;

			BtnBox = new UIButton(new CGRect(0, 0, 25, 25));

			AddSubviews (LblText, BtnBox);

			var frame = Frame;
			frame.Height = BtnBox.Frame.Height + BtnBox.Frame.Y;
		}

		protected virtual void InitCustomization()
		{
			BtnBox.SetBackgroundImage(UIImage.FromFile("bg_checkbox.png"), UIControlState.Normal);
		}

		private void InitBehaviorsMapping()
		{
			BtnBox.TouchUpInside += HandleTouchDown;
		}

		private void HandleTouchDown(object sender, EventArgs e)
		{
			IsChecked = !_isChecked;

			if (OnTouch != null)
			{
				OnTouch(sender, e);
			}
			UpdateViewState();
		}

		private void UpdateViewState()
		{
			if (IsChecked)
			{
				BtnBox.SetImage(UIImage.FromFile("checkbox.png"), UIControlState.Normal);
			}
			else
			{
				BtnBox.SetImage(UIImage.FromFile("bg_checkbox.png"), UIControlState.Normal);
			}
		}

		public void HideKeyboard()
		{

		}
	}

	public class ValueChangedEventArgs : EventArgs
	{
		public string Value { get; private set; }
		public ValueChangedEventArgs(string value)
		{
			Value = value;
		}
	}
}


