﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Reactive.Linq;
using CoreGraphics;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using Foundation;
using ObjCRuntime;
using ReactiveUI;
using UIKit;
using Unicorn.Core;
using Unicorn.Core.iOS;
using Unicorn.Core.Translation;

namespace DriveRightF.iOS
{
	public partial class CaptureImageView : DriveRightBaseView<CaptureImageViewModel>
	{
		private CGRect _originalImageFrame = CGRect.Empty;
		private nfloat _scaleFactor;
		private nfloat _imageWidth,_imageHeight;
		public DeviceManager DeviceManager {get { return DependencyService.Get<DeviceManager> (); }}
		const int MAX_SCALE = 1;
		const double DURATION_ANIMATION = 0.2;
		const double DURATION_HIDE_ANIMATION = 0.4;
		public static ITranslator Translator {get {return DependencyService.Get<ITranslator>();}}

		public CaptureImageView(CGRect frame) : base (frame)
		{
			InitView ();
//			RxApp.TaskpoolScheduler.Schedule (()=>{
				InitControl ();
//			});

		}
		public CaptureImageView() : base ()
		{
			InitView ();
//			RxApp.TaskpoolScheduler.Schedule (()=>{
				InitControl ();
//			});
		}
		public CaptureImageView(IntPtr h) : base (h)
		{
			InitView ();
//			RxApp.TaskpoolScheduler.Schedule (()=>{
				InitControl ();
//			});
		}


		public void LoadData(UIImage obj){

			_imageCrop.Image = obj;
			_imageWidth = _imageCrop.Image.Size.Width / _scaleFactor;
			_imageHeight = _imageCrop.Image.Size.Height / _scaleFactor;

			// Align center image
			ResizeToFixImage ();
			AlignImage ();
		}

		public void InitView(){
			ViewModel = new CaptureImageViewModel ();
			var arr = NSBundle.MainBundle.LoadNib("CaptureImageView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			v.Frame = this.Frame;
			this.AddSubview (v);
			_scaleFactor = UIScreen.MainScreen.Scale;
			Debug.WriteLine (">>> _scaleFactor:"+_scaleFactor);

			if (!DeviceManager.IsiPhone5) {
				ResizeIpone4 ();
			}

			_panGes.AddTarget(() => HandleDrag(_panGes));
			_pinchGes.AddTarget(() => HandlePinch(_pinchGes));
			_cropView.Layer.BorderColor=UIColor.White.CGColor;
			_cropView.Layer.BorderWidth = (nfloat)1;

		}

		private void InitControl(){

			_btnCancel.SetTitle(Translator.Translate ("BUTTON_CANCEL"), UIControlState.Normal);
			_btnDone.SetTitle(Translator.Translate ("BUTTON_DONE"), UIControlState.Normal);

			var frameCrop = _cropView.Frame;
			ViewModel.CropRectangle =  new RectangleF((float)(frameCrop.X*_scaleFactor),(float)(frameCrop.Y*_scaleFactor),
				(float)(frameCrop.Width*_scaleFactor),(float)(frameCrop.Height*_scaleFactor));
			// TODO: move all logic to viewmodel
//			this.OneWayBind(
//				ViewModel,
//				vm => vm.ImageRectangle,
//				v => v._imageCrop.CurrentRectangle);
//
//			this.OneWayBind(
//				ViewModel,
//				vm => vm.ImageScale,
//				v => v._imageCrop.CurrentScale);

//			this.BindCommand (
//				ViewModel,
//				vm => vm.CancelButtonCommand,
//				v => v._btnCancel);
//
//			this.BindCommand (
//				ViewModel,
//				vm => vm.DoneButtonCommand,
//				v => v._btnDone);
//
			_btnCancel.TouchUpInside += (sender, e) => {
				ViewModel.CancelButtonCommand.Execute(null);
			};
			_btnDone.TouchUpInside += (sender, e) => {
				ViewModel.DoneButtonCommand.Execute(null);
			};
			ViewModel.WhenAnyValue (vm => vm.Visible)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => { 
					//Hidden = !x; 
					var frame = this.Frame;
					if(x){
						//frame.X=0;
						frame.Y=0;
						//this.Frame = frame;
					}else{
						//frame.X=-frame.Width;
						frame.Y=-frame.Height;
						//this.Frame = frame;
						UIView.AnimationWillEnd+=delegate {
							//this.Hidden=
						} ;
					}
					UIView.SetAnimationCurve (UIViewAnimationCurve.EaseInOut);
					UIView.Animate (DURATION_HIDE_ANIMATION, () => {
						this.Frame = frame;
					} );
				} );
			ViewModel
				.WhenAnyValue (vm => vm.ImageData)
				.Where(x => x != null)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					LoadData(x as UIImage);
				} );
		}

		private void HandlePinch(UIPinchGestureRecognizer recognizer)
		{
			var scale = recognizer.Scale;
			Debug.WriteLine(">>>>>>>>>>>>>> Pinch Scale:"+scale);
			//var scaleFactor = UIScreen.MainScreen.Scale;
			if (_imageCrop.IsAnimating == true) {
				return;
			}
			//			// If it's just began, cache the location of the image
			if (recognizer.State == UIGestureRecognizerState.Began || _originalImageFrame == null)
			{
				_originalImageFrame = _imageCrop.Frame;
			}
			//			// Move the image if the gesture is valid
			if (recognizer.State != UIGestureRecognizerState.Cancelled && recognizer.State != UIGestureRecognizerState.Failed
				&& recognizer.State != UIGestureRecognizerState.Possible&& recognizer.State != UIGestureRecognizerState.Began)
			{

				// Move the image by adding the offset to the object's frame
				CGRect newFrame = _originalImageFrame;
				var newW = newFrame.Width * scale;
				var newH = newFrame.Height * scale;
				Debug.WriteLine(">>>>>>>>>>>>>> touch pinch move:newFrame.Width: "+newFrame.Width+"  >> "+newW);
				if (newW <= _imageWidth*MAX_SCALE && newH <= _imageHeight*MAX_SCALE) {
					var deltaX = (newW - newFrame.Width) / 2;
					var deltaY = (newH - newFrame.Height) / 2;
					newFrame.Width = newW;
					newFrame.Height = newH;
					newFrame.X -= deltaX;
					newFrame.Y -= deltaY;
					_imageCrop.Frame = newFrame;
					ViewModel.ImageRectangle=new RectangleF((float)(newFrame.X*_scaleFactor),(float)(newFrame.Y*_scaleFactor),
						(float)(newFrame.Width*_scaleFactor),(float)(newFrame.Height*_scaleFactor));

					ViewModel.ImageScale = _imageCrop.Image == null ? 0 : (ViewModel.ImageRectangle.Width / (float)_imageCrop.Image.Size.Width);
				}

			}
			//
			if (recognizer.State == UIGestureRecognizerState.Ended
				|| recognizer.State == UIGestureRecognizerState.Cancelled
				|| recognizer.State == UIGestureRecognizerState.Failed) {
				Debug.WriteLine(">>>>>>>>>>>>>> touch pinch end");
				_originalImageFrame = CGRect.Empty;
				ReSizePinchGes (true);
				RePointPanGes (true);
			}
		}	

		private void HandleDrag(UIPanGestureRecognizer recognizer)
		{
			if (_imageCrop.IsAnimating == true) {
				return;
			}
			// If it's just began, cache the location of the image
			if (recognizer.State == UIGestureRecognizerState.Began || _originalImageFrame == CGRect.Empty)
			{
				_originalImageFrame = _imageCrop.Frame;
			}
			// Move the image if the gesture is valid
			if (recognizer.State != (UIGestureRecognizerState.Cancelled | UIGestureRecognizerState.Failed
				| UIGestureRecognizerState.Possible))
			{
				// Move the image by adding the offset to the object's frame
				CGPoint offset = recognizer.TranslationInView(_imageCrop);
				CGRect newFrame = _originalImageFrame;
				newFrame.Offset(offset.X, offset.Y);
				_imageCrop.Frame = newFrame;
				ViewModel.ImageRectangle=new RectangleF((float)(newFrame.X*_scaleFactor),(float)(newFrame.Y*_scaleFactor),
					(float)(newFrame.Width*_scaleFactor),(float)(newFrame.Height*_scaleFactor));

				ViewModel.ImageScale = _imageCrop.Image == null ? 0 : (ViewModel.ImageRectangle.Width / (float)_imageCrop.Image.Size.Width);
			}

			if (recognizer.State == UIGestureRecognizerState.Ended
				|| recognizer.State == UIGestureRecognizerState.Cancelled
				|| recognizer.State == UIGestureRecognizerState.Failed) {
				_originalImageFrame = CGRect.Empty;
				RePointPanGes (true);
			}
		}

		private void ReSizePinchGes(bool animation){
			var controlW = _controlView.Frame.Width;
			var controlH = _controlView.Frame.Height;
			var imgF = _imageCrop.Frame;
			bool isChange = false;

			if (imgF.Width >= imgF.Height && controlW >= imgF.Height) {
				isChange = true;
				var newH = controlW;
				imgF.Width = imgF.Width*newH/imgF.Height;
				imgF.Height = newH;
			}


			if (imgF.Width < imgF.Height && controlW >= imgF.Width) {
				isChange = true;
				var newW = controlW;
				imgF.Height = imgF.Height*newW/imgF.Width;
				imgF.Width = newW;
			}

			if (isChange) {
				UIView.SetAnimationCurve (UIViewAnimationCurve.EaseInOut);
				UIView.Animate (DURATION_ANIMATION, () => {
					_imageCrop.Frame = imgF;
				} );
				ViewModel.ImageRectangle=new RectangleF((float)(imgF.X*_scaleFactor),(float)(imgF.Y*_scaleFactor),
					(float)(imgF.Width*_scaleFactor),(float)(imgF.Height*_scaleFactor));

				ViewModel.ImageScale = _imageCrop.Image == null ? 0 : (ViewModel.ImageRectangle.Width / (float)_imageCrop.Image.Size.Width);
			}
		}

		private void RePointPanGes(bool animation){
			var controlW = _controlView.Frame.Width;
			var controlH = _controlView.Frame.Height;
			var imgF = _imageCrop.Frame;
			var _ovTopF = _overlayTop.Frame;

			var imgW = imgF.Width;
			var imgH = imgF.Height;
			var imgX = imgF.X;
			var imgY = imgF.Y;
			bool _needRepoint = false;
			if (imgX > 0 || (imgX + imgW) < controlW) {
				_needRepoint = true;
				if (imgX > 0) {
					imgX = 0;
				}  else {
					imgX = controlW - imgW;
				}
			}

			if (imgY > _ovTopF.Height || (imgY + imgH) < (_ovTopF.Height + controlW)) {
				_needRepoint = true;
				if (imgY > _ovTopF.Height) {
					imgY = _ovTopF.Height;
				}  else {
					imgY = _ovTopF.Height + controlW - imgH;
				}
			}

			if (_needRepoint) {
				imgF.X = imgX;
				imgF.Y = imgY;

				UIView.SetAnimationCurve (UIViewAnimationCurve.EaseInOut);
				UIView.Animate (DURATION_ANIMATION, () => {
					_imageCrop.Frame = imgF;
				} );
				ViewModel.ImageRectangle=new RectangleF((float)(imgF.X*_scaleFactor),(float)(imgF.Y*_scaleFactor),
					(float)(imgF.Width*_scaleFactor),(float)(imgF.Height*_scaleFactor));

				ViewModel.ImageScale = _imageCrop.Image == null ? 0 : (ViewModel.ImageRectangle.Width / (float)_imageCrop.Image.Size.Width);
			}

		}


		private void AlignImage(){
			var controlW = _controlView.Frame.Width;
			var controlH = _controlView.Frame.Height;
			var imgF = _imageCrop.Frame;

			var alignX = (controlW - imgF.Width) / 2;
			var alignY = (controlH - imgF.Height) / 2;
			imgF.X = alignX;
			imgF.Y = alignY;
			_imageCrop.Frame = imgF;
			ViewModel.ImageRectangle=new RectangleF((float)(imgF.X*_scaleFactor),(float)(imgF.Y*_scaleFactor),
				(float)(imgF.Width*_scaleFactor),(float)(imgF.Height*_scaleFactor));

			ViewModel.ImageScale = _imageCrop.Image == null ? 0 : (ViewModel.ImageRectangle.Width / (float)_imageCrop.Image.Size.Width);
		}

		private void ResizeToFixImage(){
			var controlW = _controlView.Frame.Width;
			var controlH = _controlView.Frame.Height;
			//var scaleFactor = UIScreen.MainScreen.Scale;
			var imgF = _imageCrop.Frame;

			var image = _imageCrop.Image;
			if (image == null) {
				return;
			}
			imgF.Width = _imageWidth;
			imgF.Height = _imageHeight;
			if (imgF.Width >= imgF.Height && controlW >= imgF.Height) {
				var newH = controlW;
				imgF.Width = imgF.Width*newH/imgF.Height;
				imgF.Height = newH;
				_imageCrop.Frame = imgF;
			}else if (imgF.Width >= imgF.Height && imgF.Width > controlW ) {
				var newW = controlW;
				var newH = imgF.Height * newW / imgF.Width;
				if (newH < controlW) {
					newH = controlW;
					newW = imgF.Width * newH/imgF.Height;
				}
				imgF.Height = imgF.Height * newW / imgF.Width;
				imgF.Width = newW;
				_imageCrop.Frame = imgF;
			}


			if (imgF.Width < imgF.Height && controlW >= imgF.Width) {
				var newW = controlW;
				imgF.Height = imgF.Height*newW/imgF.Width;
				imgF.Width = newW;
				_imageCrop.Frame = imgF;
			}else if (imgF.Width < imgF.Height && imgF.Height > controlW ) {
				var newH = controlW;
				var newW = imgF.Width * newH / imgF.Height;
				if (newW < controlW) {
					newW = controlW;
					newH = imgF.Height * newW/imgF.Width;
				}
				imgF.Width = newW;
				imgF.Height = newH;
				_imageCrop.Frame = imgF;
			}

			ViewModel.ImageRectangle=new RectangleF((float)(imgF.X*_scaleFactor),(float)(imgF.Y*_scaleFactor),
				(float)(imgF.Width*_scaleFactor),(float)(imgF.Height*_scaleFactor));
			ViewModel.ImageScale = _imageCrop.Image == null ? 0 : (ViewModel.ImageRectangle.Width / (float)_imageCrop.Image.Size.Width);
		}

		private void ResizeIpone4(){
			var delta = -44;

			var newF = _overlayTop.Frame;
			newF.Height = newF.Height + delta;
			_overlayTop.Frame = newF;

			newF = _cropView.Frame;
			newF.Y = newF.Y + delta;
			_cropView.Frame = newF;

			newF = _overlayBottom.Frame;
			newF.Height += delta;
			newF.Y += delta;
			_overlayBottom.Frame = newF;

			newF = _btnContainer.Frame;
			newF.Y += 2 * delta;
			_btnContainer.Frame = newF;

			newF = _controlView.Frame;
			newF.Height += 2*delta;
			_controlView.Frame = newF;

			newF = _imageCrop.Frame;
			newF.Height += 2 * delta;
			_imageCrop.Frame = newF;
			ViewModel.ImageRectangle=new RectangleF((float)(newF.X*_scaleFactor),(float)(newF.Y*_scaleFactor),
				(float)(newF.Width*_scaleFactor),(float)(newF.Height*_scaleFactor));

			ViewModel.ImageScale = _imageCrop.Image == null ? 0 : (ViewModel.ImageRectangle.Width / (float)_imageCrop.Image.Size.Width);
		}
	}
}


