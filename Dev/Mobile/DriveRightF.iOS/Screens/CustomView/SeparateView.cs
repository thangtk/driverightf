﻿using System;
using UIKit;
using CoreGraphics;
using Unicorn.Core.iOS;
using Foundation;

namespace DriveRightF.iOS
{
	[Register("SeparateView")]
	public class SeparateView : UIView
	{
		private const float HEIGHT_VIEW_BOTTOM = 60f;
		private const float ANIMATION_DURATION = 1;
		private new UITapGestureRecognizer _tapOpen, _tapClose;
		private bool _isFirstTime = true;
		private CGRect _originFrame;
		private UIView _overLayView;

		public UIView TopView {
			get;
			private set;
		}

		public UIView ContentView {
			get;
			private set;
		}

		public UIView BottomView {
			get;
			set;
		}

		public SeparateView(CGRect frame) : base (frame)
		{
			InitView ();
		}

		public SeparateView() : base ()
		{
			InitView ();
		}

		public SeparateView(IntPtr h) : base (h)
		{
			InitView ();
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
		}

		private void InitView()
		{
			BottomView = new UIView ();
			BottomView.BackgroundColor = UIColor.Black;

			_overLayView = new UIView ();
			_overLayView.BackgroundColor = UIColor.Clear;

			this.AddSubviews (BottomView, _overLayView);
		}

		public void AddViewGroup(UIView topView, UIView contentView)
		{
			this.ClipsToBounds = true;
			this.BackgroundColor = UIColor.Clear;
			_originFrame = Frame;

			TopView = topView;
			ContentView = contentView;
			TopView.RemoveFromSuperview ();
			ContentView.RemoveFromSuperview ();

			if (_tapOpen == null) {
				_tapOpen = new UITapGestureRecognizer(()=>
					{
						OpenView();
					});
				_overLayView.AddGestureRecognizer(_tapOpen);
			}

			if (_tapClose == null) {
				_tapClose = new UITapGestureRecognizer (() => {
					CloseView();
				});
					
				TopView.AddGestureRecognizer(_tapClose);
			}

			AddSubviews (TopView, contentView);
			UpdatePosition ();

		}

		private void UpdatePosition()
		{
			TopView.ChangeFrameAttributes (0, 0);
			ContentView.ChangeFrameAttributes (0, (float)(TopView.Frame.Height));

//			BottomView.ChangeFrameAttributes (0, (float)TopView.Frame.Height - HEIGHT_VIEW_BOTTOM, (float)Frame.Width, HEIGHT_VIEW_BOTTOM);
			_overLayView.Frame = ContentView.Frame;
			this.BringSubviewToFront (_overLayView);
			_overLayView.UserInteractionEnabled = false;
		}

		public void OpenView()
		{
			if (TopView == null || ContentView == null)
				return;
			AnimationOpen ();
		}

		private void AnimationOpen ()
		{
			//TODO: update position of supperview and childen view .
			this.UserInteractionEnabled = false;
			Frame = _originFrame;
			TopView.ChangeFrameAttributes (0, (float)-TopView.Frame.Height);
			ContentView.ChangeFrameAttributes(0f, (float)(Frame.Height - HEIGHT_VIEW_BOTTOM ));
			_overLayView.ChangeFrameAttributes (0f, (float)(Frame.Height- HEIGHT_VIEW_BOTTOM));
//			BottomView.ChangeFrameAttributes(0f, (float)(Frame.Height- HEIGHT_VIEW_BOTTOM));

//			BottomView.Alpha = 1;
			UIView.Animate (ANIMATION_DURATION, () => {
				TopView.ChangeFrameAttributes(0f,0);
				ContentView.ChangeFrameAttributes(0f,(float)TopView.Frame.Height);
				_overLayView.ChangeFrameAttributes(0f,(float)TopView.Frame.Height);
//				BottomView.ChangeFrameAttributes(0f, (float)(TopView.Frame.Height - HEIGHT_VIEW_BOTTOM));
			}, () =>
				{
					this.UserInteractionEnabled = true;
					_overLayView.UserInteractionEnabled = false;
				});
		}

		private void AnimationClose()
		{
			this.UserInteractionEnabled = false;
			BottomView.Alpha = 0;
			UIView.Animate (ANIMATION_DURATION, () => {
				TopView.ChangeFrameAttributes(0f,(float)-TopView.Frame.Height);
				ContentView.ChangeFrameAttributes(0f, (float)(Frame.Height - HEIGHT_VIEW_BOTTOM ));
//				BottomView.ChangeFrameAttributes(0f, (float)(Frame.Height- HEIGHT_VIEW_BOTTOM));
//				BottomView.Alpha = 1;
			}, () =>
				{
					//TODO: move position of superview to buttomview.
					this.ChangeFrameAttributes((float)Frame.X,(float) (Frame.Y + Frame.Height) -HEIGHT_VIEW_BOTTOM,(float) Frame.Width, HEIGHT_VIEW_BOTTOM);
					ContentView.ChangeFrameAttributes(0,0);
					_overLayView.ChangeFrameAttributes(0,0);
					this.UserInteractionEnabled = true;
					_overLayView.UserInteractionEnabled = true;
				});
		}

		private void ResetView()
		{
		}

		public void CloseView()
		{
			if (TopView == null || ContentView == null)
				return;
			AnimationClose ();
		}

		public void HideView()
		{
			if (TopView == null || ContentView == null)
				return;
		}

		public void ShowView()
		{
			if (TopView == null || ContentView == null)
				return;
		}
	}
}

