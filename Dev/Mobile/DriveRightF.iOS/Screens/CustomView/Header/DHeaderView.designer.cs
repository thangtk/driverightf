// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	partial class DHeaderView
	{
		[Outlet]
		UIKit.UIButton btnBack { get; set; }

		[Outlet]
		UIKit.UIButton btnNotification { get; set; }

		[Outlet]
		UIKit.UIButton btnProfile { get; set; }

		[Outlet]
		UIKit.UILabel lblHeader { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnNotification != null) {
				btnNotification.Dispose ();
				btnNotification = null;
			}

			if (btnProfile != null) {
				btnProfile.Dispose ();
				btnProfile = null;
			}

			if (lblHeader != null) {
				lblHeader.Dispose ();
				lblHeader = null;
			}

			if (btnBack != null) {
				btnBack.Dispose ();
				btnBack = null;
			}
		}
	}
}
