﻿using System;
using Unicorn.Core.UI;
using DriveRightF.Core.ViewModels;
using Foundation;
using DriveRightF.Core;
using UIKit;
using ObjCRuntime;
using ReactiveUI;
using System.Reactive.Linq;

namespace DriveRightF.iOS
{
	[Register ("DHeaderView_AL")]
	public partial class DHeaderView_AL : DriveRightBaseView<HeaderViewModel>
	{
		private UIView _rootView;

		public event EventHandler<string> OnBtnNotificationClicked;
		public event EventHandler<string> OnBtnProfileClicked;
		public event EventHandler<string> OnBtnBackClicked;

		public DHeaderView_AL (HeaderViewModel vm) : base ()
		{
			ViewModel = vm;

			InitializeView ();
		}

		public DHeaderView_AL () : base ()
		{
			InitializeView ();
		}

		public DHeaderView_AL (IntPtr h) : base (h)
		{
			InitializeView ();
		}

		public DHeaderView_AL (CoreGraphics.CGRect frame) : base (frame)
		{
			InitializeView ();
		}

		private void LoadNib ()
		{
			var nibObjects = NSBundle.MainBundle.LoadNib ("DHeaderView_AL", this, null);
			_rootView = Runtime.GetNSObject ((nibObjects.ValueAt (0))) as UIView;
			FixSupperView (this, _rootView);
		}

		private void FixSupperView (UIView supperView, UIView childView)
		{
			childView.TranslatesAutoresizingMaskIntoConstraints = false;
			supperView.Add (childView);
			NSLayoutConstraint[] constraints = {
				NSLayoutConstraint.Create (childView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create (childView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create (childView, NSLayoutAttribute.Trailing, NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Trailing, 1, 0),
				NSLayoutConstraint.Create (childView, NSLayoutAttribute.Leading, NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Leading, 1, 0)
			};
			supperView.AddConstraints (constraints);
		}

		private void InitializeView ()
		{
			LoadNib ();
			InitControls ();
			InitBinding ();
		}

		private void InitControls ()
		{
			this.BackgroundColor = UIColor.Clear;

//			btnNotification.TouchUpInside += (object sender, EventArgs e) => 
//			{
//				if(OnBtnNotificationClicked != null)
//				{
//					OnBtnNotificationClicked(this, "touch");
//				}
//			};

			btnNotification.OnTapped += () => {
				if (OnBtnNotificationClicked != null) {
					OnBtnNotificationClicked (this, "touch");
				}
			};

			btnBack.TouchUpInside += (object sender, EventArgs e) => {
				if (OnBtnBackClicked != null) {
					OnBtnBackClicked (this, "touch");
				}
			};

			btnInfo.TouchUpInside += (object sender, EventArgs e) => {
				ViewModel.BtnInfoClickCommand.Execute (null);
			};

			btnShare.TouchUpInside += (object sender, EventArgs e) => {
				ViewModel.BtnShareClickCommand.Execute (null);
			};
		}

		private void InitBinding ()
		{
			this.WhenAnyValue (x => x.ViewModel)
				.Where (vm => vm != null)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (vm => HandleBinding ());
		}

		private void HandleBinding ()
		{
			ViewModel.WhenAnyValue (x => x.Title)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (t => {
				lblHeader.Text = t;
			});

//			ViewModel.WhenAnyValue (x => x.IconProfile)
//				.Where (x => !string.IsNullOrEmpty (x))
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (x => {
//					btnProfile.SetImage (UIImage.FromFile (x), UIControlState.Normal);
//				});

//			ViewModel.WhenAnyValue (x => x.IconNotification)
//				.Where (x => !string.IsNullOrEmpty (x))
//				.ObserveOn (RxApp.MainThreadScheduler)
//				.Subscribe (x => {
//					btnNotification.SetImage (UIImage.FromFile (x), UIControlState.Normal);
//				});

			ViewModel.WhenAnyValue (x => x.NumOfNewNotification)
				.Where (x => x >= 0)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				btnNotification.Count = x;
			});

			ViewModel.WhenAnyValue (vm => vm.IsShowBtnBack)
//				.Where (x => !string.IsNullOrEmpty (x))
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
				UIView.Animate (0.3, () => {
					btnBack.Alpha = x ? 1 : 0;
					btnNotification.Alpha = x ? 0 : 1;
				});
			});

			ViewModel.WhenAnyValue (vm => vm.IsEnableBtnBack)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => btnBack.UserInteractionEnabled = x);

			ViewModel.WhenAnyValue (vm => vm.IsShowBtnShare)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => btnShare.Alpha = x ? 1 : 0);

			ViewModel.WhenAnyValue (vm => vm.IsShowBtnInfo)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => btnInfo.Alpha = x ? 1 : 0);

			ViewModel.WhenAnyValue (vm => vm.IsShowBtnNotification)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => btnNotification.Alpha = x ? 1 : 0);
		}

	}
}

