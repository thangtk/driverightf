﻿using System;
using Unicorn.Core.UI;
using DriveRightF.Core.ViewModels;
using Foundation;
using DriveRightF.Core;
using UIKit;
using ObjCRuntime;
using ReactiveUI;
using System.Reactive.Linq;
using Unicorn.Core;
using System.Reactive.Concurrency;

namespace DriveRightF.iOS
{
	[Register ("DHeaderView")]
	public partial class DHeaderView : DriveRightBaseView<HeaderViewModel>
	{
		private UIView _rootView;
		public UIView RootView {get{ return _rootView;}}
		public event EventHandler<string> OnBtnNotificationClicked;
		public event EventHandler<string> OnBtnProfileClicked;
		public event EventHandler<string> OnBtnBackClicked;

		public DHeaderView (HeaderViewModel vm) : base ()
		{
			ViewModel = vm;

			InitializeView ();
		}

		public DHeaderView () : base ()
		{
			InitializeView ();
		}

		public DHeaderView (IntPtr h) : base (h)
		{
			InitializeView ();
		}

		public DHeaderView (CoreGraphics.CGRect frame) : base (frame)
		{
			InitializeView ();
		}

		private void LoadNib ()
		{
			var nibObjects = NSBundle.MainBundle.LoadNib ("DHeaderView", this, null);
			_rootView = Runtime.GetNSObject ((nibObjects.ValueAt (0))) as UIView;
//			FixSupperView (this, _rootView);
		}

		private void InitializeView ()
		{
			LoadNib ();
			SetupView ();
			InitControls ();
			RxApp.TaskpoolScheduler.Schedule (InitBinding);
		}

		private void InitControls()
		{
			this.BackgroundColor = UIColor.Clear;
			lblHeader.Font = FontHub.HelveticaMedium17;

			btnNotification.TouchUpInside += (object sender, EventArgs e) => 
			{
				if(OnBtnNotificationClicked != null)
				{
					OnBtnNotificationClicked(this, "touch");
				}
			};

			btnProfile.TouchUpInside += (object sender, EventArgs e) => 
			{
				if(OnBtnProfileClicked != null)
				{
					OnBtnProfileClicked(this, "touch");
				}
			};

			btnBack.TouchUpInside += (object sender, EventArgs e) => 
			{
				if(OnBtnBackClicked != null)
				{
					OnBtnBackClicked(this, "touch");
				}
			};
		}

		/// <summary>
		/// Too slow. I don't know why
		/// </summary>
		private void InitBinding ()
		{
			this.WhenAnyValue (x => x.ViewModel)
				.Where (vm => vm != null)
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Subscribe (vm => HandleBinding ());
		}
		public void SetupView(){
			lblHeader.Text = ViewModel.Title;
			btnProfile.SetImage (UIImage.FromFile (ViewModel.IconProfile), UIControlState.Normal);
			btnNotification.SetImage (UIImage.FromFile (ViewModel.IconNotification), UIControlState.Normal);
		}
		private void HandleBinding ()
		{
			MyDebugger.ShowThreadInfo ("HandleBinding - HeaderView");
			ViewModel.WhenAnyValue (x => x.Title)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (t => {
					lblHeader.Text = t;
				});

			ViewModel.WhenAnyValue (x => x.IconProfile)
				.Where (x => !string.IsNullOrEmpty (x))
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					btnProfile.SetImage (UIImage.FromFile (x), UIControlState.Normal);
				});

			ViewModel.WhenAnyValue (x => x.IconNotification)
				.Where (x => !string.IsNullOrEmpty (x))
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					btnNotification.SetImage (UIImage.FromFile (x), UIControlState.Normal);
				});

			ViewModel.WhenAnyValue (vm => vm.IsShowBtnBack)
//				.Where (x => !string.IsNullOrEmpty (x))
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					UIView.Animate(0.3, () =>
						{
							btnBack.Alpha = x ? 1 : 0;
							btnProfile.Alpha = x ? 0 : 1;
							btnNotification.Alpha = x ? 0 : 1;
						});
				});

			ViewModel.WhenAnyValue (vm => vm.IsEnableBtnBack)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => btnBack.UserInteractionEnabled = x);
		}

	}
}

