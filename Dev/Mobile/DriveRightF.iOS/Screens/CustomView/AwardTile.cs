﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;
using CoreAnimation;
using ObjCRuntime;
using Unicorn.Core.UI;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using DriveRightF.Core.Enums;
using System.Reactive.Linq;
using DriveRightF.Core;

namespace DriveRightF.iOS
{
	[Register ("AwardTile")]
	public sealed partial class AwardTile : UBaseView<AwardTileViewModel>
	{
		private string _backgroundColor;
		private string _image;
		private AwardType _type;
		private Award _award;
		private CALayer _layer;

		public event EventHandler OnClick;

		public Award Award {
			get
			{
				return _award;
			}
			set
			{
				_award = value;
			}
		}

		public string Image {
			set {
				_image = value;
				if (ViewModel != null)
					ViewModel.Image = value;
			}
		}

//		public string BackgroundColor {
//			set {
//				_backgroundColor = value;
//				if (ViewModel != null)
//					ViewModel.Colors = value;
//			}
//		}

		public AwardType Type {
			get {
				return _type;
			}
			set {
				_type = value;
				SetFont ();
				switch (_type) {
					case AwardType.AwardLarge:
						lblMoney.Hidden = true;
						lblExpire.Font = FontHub.HelveticaHvCn22;
						lblName.Font = FontHub.HelveticaHvCn22;
						lblTitle.Font = FontHub.HelveticaHvCn22;
						break;
					case AwardType.AwardSmall:
						lblName.Hidden = true;
						lblMoney.Hidden = true;
						lblName.Font = FontHub.HelveticaThin17;
						lblTitle.Font = FontHub.HelveticaThin17;
						break;
					case AwardType.CardLarge:
						lblExpire.Font = FontHub.HelveticaHvCn22;
						lblName.Font = FontHub.HelveticaHvCn22;
						lblTitle.Font = FontHub.HelveticaHvCn22;
						break;
					case AwardType.CardSmall:
						lblName.Hidden = true;
						break;
					default:
						break;
				}
			}
		}

		public AwardTile (UIColor color) : base ()
		{
			BackgroundColor = color;
			InitView ();
			SetFont ();
			InitBinding ();
			InitEvent ();
		}

		public AwardTile (IntPtr h) : base (h)
		{
			InitView ();
			SetFont ();
//			Observable.Start (()=>{
			InitBinding ();
//			},RxApp.TaskpoolScheduler);
			InitEvent ();
		}

		private void InitEvent()
		{
			AddGestureRecognizer (new UITapGestureRecognizer (() => this.OnClick (this, new EventArgs ())));
		}

		private void InitBinding ()
		{
			this.WhenAnyValue (x => x.ViewModel)
				.ObserveOn (RxApp.TaskpoolScheduler)
				.Where (vm => vm != null)
				.Subscribe (vm => HandleBinding ());
		}

		private void HandleBinding ()
		{
			ViewModel.WhenAnyValue (vm => vm.Award)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (award => {
					Award = award;
					if(Award.Id > 0)
					{
						lblName.Text = award.Sponsor;
						lblMoney.Text = string.Format ("{0} {1}", award.Value.ToString (), Translator.Translate(award.Unit));
						int day = award.Expiry.Subtract(DateTime.UtcNow).Days;
						lblExpire.Text = string.Format ("Exp: {0}{1}", day, day > 1 ? Translator.Translate("DAYS") : Translator.Translate("DAY"));
						lblTitle.Text = award.Name;

						if (!String.IsNullOrEmpty (award.ImageURL))
							backgroundImage.Image = UIImage.FromFile (award.ImageURL);
					}
					else
					{
						lblName.Text = string.Empty;
						lblMoney.Text = string.Empty;

						lblExpire.Text = string.Empty;
						lblTitle.Text = string.Empty;

						backgroundImage.Image = null;
					}
			});

			ViewModel.WhenAnyValue (vm => vm.Colors, vm => vm.Award.ImageURL)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					if(_layer != null)
					{
						_layer.RemoveFromSuperLayer();
					}
				
					_layer = new CALayer ();
					_layer.BackgroundColor = ColorHub.ColorFromHex (x.Item1).CGColor;
					_layer.Position = new CGPoint ((Frame.Right - Frame.Left) / 2f, (Frame.Bottom - Frame.Top) / 2f);
//					_layer.Position = Frame.Location;
					_layer.Bounds = Frame;
					if (!String.IsNullOrEmpty (x.Item2)) 
					{	
						backgroundImage.Image = UIImage.FromFile (x.Item2);
						_layer.Opacity = 0.85f;
					} 
					else
						_layer.Opacity = 1f;

					backgroundImage.Layer.AddSublayer (_layer);
				});
		}

		private void InitView ()
		{
			var arr = NSBundle.MainBundle.LoadNib ("AwardTile", this, null);
			var v = Runtime.GetNSObject (arr.ValueAt (0)) as UIView;
			FixSupperView (this, v);
		}

		private void FixSupperView(UIView supperView, UIView childView)
		{
//			childView.TranslatesAutoresizingMaskIntoConstraints = false;
			supperView.Add (childView);
//			NSLayoutConstraint[] constraints = {
//				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Top,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Top,1,0),
//				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Bottom,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Bottom,1,0),
//				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Trailing,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Trailing,1,0),
//				NSLayoutConstraint.Create(childView,NSLayoutAttribute.Leading,NSLayoutRelation.Equal, supperView, NSLayoutAttribute.Leading,1,0)
//			};
//			supperView.AddConstraints (constraints);
		}
		private void SetFont ()
		{
			lblTitle.Font = FontHub.HelveticaLt17;
			lblExpire.Font = FontHub.HelveticaLt13;
			lblMoney.Font = FontHub.HelveticaHvCn20; 
			lblName.Font = FontHub.HelveticaHvCn22;
		}
	}
}
