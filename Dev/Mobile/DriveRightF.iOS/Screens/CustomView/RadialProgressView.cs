﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;
using System.Reactive.Linq;
using ReactiveUI;
using System.Timers;

namespace DriveRightF.iOS
{
	public partial class RadialProgressView : UIView
	{
		private UIColor _defaultColor = UIColor.FromRGB (44, 56, 104);
		private UIColor _loadingBackgroundColor = UIColor.FromRGB (216, 216, 216);
		private int _value;
		private nfloat _fillStroke;
		private nfloat _ratio = 0f;
		private UILabel _lblPercentage;
		private int _originalValue;
		Timer _timer;

		public bool HidePercentText {
			get;
			set;
		}

		/// <summary>
		/// Chua duoc
		/// </summary>
		/// <value><c>true</c> if auto adjust label; otherwise, <c>false</c>.</value>
		public bool AutoAdjustLabel {
			get;
			set;
		}

		public int Value {
			get {
				return _value;
			}
			private set { 
				if (value >= 0 && value <= MaxValue) {
					_originalValue = _value;
					_value = value;
					_ratio = value / (nfloat)MaxValue; 
				}
			}
		}

		public int MaxValue {
			get;
			set;
		}

		public virtual UIColor Color {
			get {
				return _defaultColor;
			}
			set {
				_defaultColor = value;
			}
		}

		public UIColor LoadingBackgroundColor {
			get {
				return _loadingBackgroundColor;
			}
			set {
				_loadingBackgroundColor = value;
			}
		}

		public UIFont LabelTextFont {
			get {
				return _lblPercentage.Font;
			}
			set {
				_lblPercentage.Font = value;
			}
		}

		public UIColor TextColor {
			get { 
				return _lblPercentage.TextColor;
			}
			set { 
				_lblPercentage.TextColor = value;
			}
		}

		public bool IsRoundHead {
			get;
			set;
		}

		public int DelayDuration {
			get;
			set;
		}

		public nfloat FillStroke {
			get {
				return _fillStroke;
			}
			set {
				if (value > 0)
					_fillStroke = value;
			}
		}

		public RadialProgressView (CGPoint location, nfloat size) : base (new CGRect (location.X, location.Y, size, size))
		{
			Initialize ();
		}

		public RadialProgressView (CGPoint location, nfloat size, int value) : base (new CGRect (location.X, location.Y, size, size))
		{
			Initialize ();
			Value = value;
		}

		public RadialProgressView (IntPtr handler) : base (handler)
		{
			Initialize ();
		}

		private void Initialize ()
		{
			InitParams ();
			InitViews ();
		}

		protected virtual void InitParams ()
		{
			MaxValue = 100;
			_ratio = 0;
			BackgroundColor = UIColor.Clear;
			Layer.CornerRadius = Frame.Width / 2;
			FillStroke = 0.1f * Frame.Width;
			DelayDuration = 0;
			IsRoundHead = true;
		}

		private void InitViews ()
		{
			if (AutoAdjustLabel) {
				nfloat labelHeigh = Frame.Height / 2;
				_lblPercentage = new UILabel (new CGRect (0, 0, Frame.Width, labelHeigh));
				_lblPercentage.Center = new CGPoint (Frame.Width / 2, Frame.Height / 2 + 3);
				_lblPercentage.TextAlignment = UITextAlignment.Center;
				_lblPercentage.Font = FontHub.HelveticaHvCn15;
				_lblPercentage.AdjustsFontSizeToFitWidth = true; // ??? sao ko dc
				TextColor = Color;
				//
				AddSubview (_lblPercentage);
			} else {
				_lblPercentage = new UILabel (new CGRect (0, 0, Frame.Width, Frame.Height));
				_lblPercentage.TextAlignment = UITextAlignment.Center;
				_lblPercentage.Font = FontHub.HelveticaHvCn15;
//				_lblPercentage.TextColor = Color;
				TextColor = Color;
							
				AddSubview (_lblPercentage);
			}
		}

		public void SetValue (int value, bool isVisible = false)
		{
			Value = value;
			if (!isVisible)
				_ratio = 0f;
			SetNeedsDisplay ();
		}

		public override void Draw (CGRect rect)
		{
			base.Draw (rect);

			using (CGContext context = UIGraphics.GetCurrentContext ()) {

				var center = new CGPoint (rect.Width / 2, rect.Height / 2);

				context.Flush ();
				context.SaveState ();
				context.SetFillColor (LoadingBackgroundColor.CGColor);
				Bagel (new CGPoint (center.X, center.Y), center.Y - FillStroke - 1, center.Y - 1, 0, (nfloat)Math.PI * 2f).Fill ();
				context.RestoreState ();

				context.SaveState ();
				context.SetFillColor (Color.CGColor);
				Bagel (new CGPoint (center.X, center.Y), center.Y - FillStroke - 1, center.Y - 1, 0, _ratio * (nfloat)Math.PI * 2f, IsRoundHead).Fill ();
				context.RestoreState ();
			}

			_lblPercentage.Text = ((int)(_ratio * (nfloat)MaxValue)).ToString () + (HidePercentText ? "" : "%");
		}

		public UIBezierPath Bagel (CGPoint center, nfloat startRadius, nfloat endRadius, nfloat startAngle, nfloat endAngle, bool arc = false)
		{
			UIBezierPath uiBezierPath = new UIBezierPath ();
			nfloat num1 = -(nfloat)Math.PI / 2;
			nfloat radius = (nfloat)((startRadius + endRadius) / 2.0);
			nfloat num2 = (nfloat)((endRadius - startRadius) / 2.0);
			uiBezierPath.AddArc (center, startRadius, startAngle + num1, endAngle + num1, true);
			uiBezierPath.AddArc (center, endRadius, endAngle + num1, startAngle + num1, false);
			uiBezierPath.ClosePath ();
			if (arc && endAngle - startAngle > (nfloat)Math.PI / 15f) {
				uiBezierPath.AddArc (RotatePoint (center, radius, startAngle + num1), num2, -(nfloat)Math.PI / 2f, -(nfloat)Math.PI * 3 / 2, true);
				uiBezierPath.AddArc (RotatePoint (center, radius, endAngle + num1), num2, (nfloat)Math.PI / 2f + endAngle, -(nfloat)Math.PI * 3f / 2f + endAngle, false);
			}
			return uiBezierPath;
		}

		private CGPoint RotatePoint (CGPoint center, nfloat radius, double phi)
		{
			double num1 = Math.Sin (phi);
			double num2 = Math.Cos (phi);

			return new CGPoint (center.X + radius * (nfloat)num2, center.Y + radius * (nfloat)num1);
		}


		/// <summary>
		/// Starts the animating.
		/// </summary>
		/// <param name="delayAnimation">Delay animation in milliseconds.</param>
		public void StartAnimating (int delayAnimation = 0)
		{
//			if (Value == 0)
//				return;

			DelayDuration = delayAnimation;
			int max = Value;
			int count = 0;

			IReactiveCommand<object> animationCommand = ReactiveCommand.Create (); 
			animationCommand
					.ObserveOn (RxApp.TaskpoolScheduler)
					.Subscribe (x => {
				_timer = new Timer (10);
				_timer.Elapsed += (sender, e) => {
					int value;
					if (max > count)
						value = count++;
					else if( max < count)
						value = count--;
					else
						value = count;
							
					Animating (value, max);
				};
				System.Threading.Thread.Sleep (DelayDuration);
				_timer.Start ();
			});
						
			animationCommand.Execute (null);
		}

		private void Animating (int value, int max = -1)
		{
			_ratio = value / (nfloat)MaxValue; 
			InvokeOnMainThread (() => {
				SetNeedsDisplay ();
				if (value == max) {
					_timer.Stop ();
					_originalValue = max;
				}
			});
		}
	}
}

