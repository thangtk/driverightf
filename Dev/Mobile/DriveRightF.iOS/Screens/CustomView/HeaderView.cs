﻿
using System;
using Foundation;
using UIKit;
using CoreGraphics;
using System.ComponentModel;
using Unicorn.Core;
using Unicorn.Core.Navigator;

namespace DriveRightF.iOS
{
	[Register("HeaderView")]
	public partial class HeaderView : UIView
	{
		private UIButton _btnBack;
		private UIImageView _imgBackground;

		public UIImage BackgroundImage {
			get { return _imgBackground.Image;}
			set { _imgBackground.Image = value;}
		}
			
		public HeaderView () : base ()
		{
			Frame = new CGRect (0, 0, 320, 80);
		}

		public HeaderView(CGRect frame) : base(frame)
		{
		}

		public HeaderView(IntPtr handler) : base(handler)
		{
		}

		public Action<object, EventArgs> BackClicked;

		protected virtual void InitView()
		{
			BackgroundColor = UIColor.Clear;
			_imgBackground = new UIImageView (new CGRect(0, 0, Frame.Width, Frame.Height));
			_imgBackground.BackgroundColor = UIColor.Clear;
			AddSubview (_imgBackground);

			_btnBack = new UIButton (new CGRect (0, 22, 64, 42));
			_btnBack.SetImage (UIImage.FromFile ("icon_arrow_left@2x.png"), UIControlState.Normal);
			_btnBack.ImageEdgeInsets = new UIEdgeInsets (0, 0, 0, 25);
			_btnBack.TouchUpInside += (sender, e) => {
				if(BackClicked != null)
					BackClicked(sender, e);
				else
					DependencyService.Get<INavigator>().NavigateBack(true);
			};
			AddSubview (_btnBack);
		}
	}
}

