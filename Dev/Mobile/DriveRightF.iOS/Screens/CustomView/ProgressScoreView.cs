﻿using System;
using CoreGraphics;
using Unicorn.Core;
using DriveRightF.Core;
using UIKit;
using Foundation;

namespace DriveRightF.iOS
{
	[Register("ProgressScoreView")]
	public class ProgressScoreView : RadialProgressView
	{
		public ProgressScoreView (IntPtr handler) : base(handler)
		{
			InitData ();
		}

		public ProgressScoreView (CGPoint location, nfloat size) : base(location, size)
		{
			InitData ();
		}

		protected override void InitParams ()
		{
			base.InitParams ();
			IsRoundHead = false;
			AutoAdjustLabel = true;
			HidePercentText = true;
		}

		private void InitData(){
		}

		public override UIColor Color {
			get {
				var c = DependencyService.Get<ScoreManager> ().GetColorByScore (Value);
				return UIColor.FromRGBA (c.R, c.G, c.B, c.A);
			}
			set {
				base.Color = value;
			}
		}
	}
}

