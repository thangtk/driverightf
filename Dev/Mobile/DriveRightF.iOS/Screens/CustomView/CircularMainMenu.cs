﻿
using System;
using System.Drawing;

using Foundation;
using UIKit;
using System.Collections.Generic;
using CoreGraphics;
using Unicorn.Core.iOS;

namespace DriveRightF.iOS
{
	public partial class CircularMainMenu : UIView
	{
		private List<UIButton> _contentViewList;
		private UIButton _contentButton;
		private UIView _contentView;
		private bool _visible;
		private float _offsets;
		public event Action<UIView> WillShowMenuView;
		public event Action<UIView> DidShowMenuView;
		public event Action<UIView> WillHideMenuView;
		public event Action<UIView> DidHideMenuView;

		public bool IsVisible {
			get { return _visible; }
			set {
				if (value != _visible) {
					if (value)
						DoShowMenuView ();
					else
						HideMenuView ();
				}
				_visible = value; }
		}

		public double AnimationDuration {
			get;
			set;
		}

		public bool IsScaleOnAnimaton {
			get;
			set;
		}

		public bool IsRotateOnAnimation {
			get;
			set;
		}

		public bool IsUsingMainButton {
			get;
			set;
		}

		public nfloat RotatingAngle {
			get;
			set;
		}

		public List<UIButton> ContentViewList
		{
			get { return _contentViewList; }
			set { 
				if(_contentViewList != null)
					foreach (var view in _contentViewList) {
						RehideView (view);
					}

				_contentViewList = value;
			}
		}

		public CircularMainMenu () : base ()
		{
			InitParams ();
		}

		public CircularMainMenu(CGRect frame, List<UIButton> contentView) : base(frame)
		{
			_contentView = new UIView (frame);
			ContentViewList = contentView;
			InitParams ();
			InitViews ();
		}

		private void InitParams()
		{
			AnimationDuration = 0.8f;
			RotatingAngle = (nfloat)Math.PI;
			IsScaleOnAnimaton = true;
			IsRotateOnAnimation = true;
			IsUsingMainButton = true;

			_offsets = DeviceUtil.IsiPhone5 ? 10 : 0;
		}

		private	void InitViews()
		{
			BackgroundColor = UIColor.Clear;
			_visible = false;

			_contentView.Frame = new CGRect (0, 0, Frame.Width, Frame.Height);

			AddSubview (_contentView);

			AddContentButton ();

			_contentView.Transform = CGAffineTransform.Rotate (CGAffineTransform.MakeScale(0.01f, 0.01f), -RotatingAngle);
		}

		private void RehideView(UIView view)
		{
			view.RemoveFromSuperview ();
		}

		private void RedisplayView(UIView view)
		{
			AddSubview (view);
			BringSubviewToFront (view);
		}

		private void AddContentButton ()
		{
			_contentButton = UIButton.FromType (UIButtonType.Custom);
			_contentButton.Frame = new CGRect (0, 0, 80f, 80f);
			_contentButton.Center = _contentView.Center;
			_contentButton.BackgroundColor = UIColor.Red;
			_contentButton.SetImage (UIImage.FromFile ("button_getstart.png"), UIControlState.Normal);
			_contentButton.Layer.CornerRadius = _contentButton.Frame.Height / 2;
			_contentButton.TouchUpInside += (sender, e) => {
				if(_visible)
					HideMenuView();
				else
					DoShowMenuView();
			};
			AddSubview (_contentButton);

			_contentViewList [0].Center = new CGPoint (_contentView.Frame.Width / 2, _contentViewList[0].Frame.Height / 2 - _offsets);
			_contentViewList [1].Center = new CGPoint (_contentView.Frame.Width - _contentViewList[1].Frame.Width / 2 + _offsets, _contentView.Frame.Height / 2);
			_contentViewList [2].Center = new CGPoint (_contentView.Frame.Width / 2, _contentView.Frame.Height - _contentViewList[2].Frame.Height / 2 + _offsets);
			_contentViewList [3].Center = new CGPoint (_contentViewList[3].Frame.Width / 2 - _offsets, _contentView.Frame.Height / 2);

			for (int i = 0; i < 4; i++) {
				_contentViewList [i].SetTitle(i.ToString (), UIControlState.Normal);
				_contentViewList [i].Layer.CornerRadius = _contentViewList [i].Frame.Width / 2;
				_contentView.AddSubview (_contentViewList [i]);
			}
		}

		private void DoShowMenuView(bool animated = true)
		{
			Window.EndEditing (true);

			//WillShowMenuView (this);
			UIView.Animate (animated ? AnimationDuration : 0f, () =>
				{
					if(IsRotateOnAnimation)
					{
						_contentButton.Alpha = 0f;
						_contentView.Transform = CGAffineTransform.MakeIdentity();
					}
				}, () =>
				{
					//					DidShowMenuView(this);
					_visible = true;
				});
		}

		private void HideMenuView(bool animated = true)
		{
			Window.EndEditing (true);

			//			WillHideMenuView (this);
			UIView.Animate (animated ? AnimationDuration : 0f, () =>
				{
					if(IsRotateOnAnimation)
					{
						_contentButton.Alpha = 1f;
						_contentView.Transform = CGAffineTransform.Rotate(CGAffineTransform.MakeScale(0.01f, 0.01f), -RotatingAngle);
					}
				}, () =>
				{
					//					DidHideMenuView(this);
				});
			_visible = false;
		}

	}
}

