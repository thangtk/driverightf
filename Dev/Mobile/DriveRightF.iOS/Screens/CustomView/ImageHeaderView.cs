﻿using System;
using UIKit;
using CoreGraphics;
using Foundation;
using System.ComponentModel;

namespace DriveRightF.iOS
{
	[Register("ImageHeaderView")]
	public class ImageHeaderView : HeaderView
	{
		private UIImageView _imgMain;

		public UIImage MainImage {
			get { return _imgMain.Image;}
			set { _imgMain.Image = value;}
		}

		public ImageHeaderView ()
		{
			InitView ();
		}

		public ImageHeaderView(CGRect frame) : base(frame)
		{
			InitView ();
		}

		public ImageHeaderView(IntPtr handler) : base(handler)
		{
			InitView ();
		}

		protected override void InitView ()
		{
			base.InitView ();

			BackgroundImage = UIImage.FromFile ("award_background_header.png");

			_imgMain = new UIImageView (new CGRect (0, 0, 50, 50));
			_imgMain.Center = Center;
			var frame = _imgMain.Frame;
			frame.Y = Frame.Bottom - frame.Height - 5f;
			_imgMain.Frame = frame;
			_imgMain.Layer.CornerRadius = _imgMain.Frame.Width / 2f;
			AddSubview (_imgMain);
		} 

	}
}

