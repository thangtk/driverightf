﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;
using UIKit;

namespace DriveRightF.iOS
{
	[Register ("UICheckBox")]
	partial class UICheckBox
	{
		private UIButton BtnBox { get; set; }

		private UILabel LblText { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (LblText != null) {
				LblText.Dispose ();
				LblText = null;
			}

			if (BtnBox != null) {
				BtnBox.Dispose ();
				BtnBox = null;
			}
		}
	}
}
