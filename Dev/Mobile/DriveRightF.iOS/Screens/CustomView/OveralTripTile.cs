﻿
using System;

using Foundation;
using UIKit;
using ObjCRuntime;
using CoreGraphics;

namespace DriveRightF.iOS
{
	[Register ("OveralTripTile")]
	public partial class OveralTripTile : UIView
	{
		private float _score;
		public float Score {
			get
			{
				return _score;
			}
			set
			{
				_score = value;
				lblScore.Text = string.Format ("Score: {0}", value);
			}
		}
		public OveralTripTile() : base()
		{
			InitView();
			SetFont ();

		}

		public OveralTripTile(IntPtr h): base(h)
		{
			InitView ();
			SetFont ();

		}
		private void InitView()
		{
			var arr = NSBundle.MainBundle.LoadNib("OveralTripTile", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			v.Frame = new CGRect(0, 0, Frame.Width, Frame.Height);
			AddSubview(v);
		}

		private void SetFont()
		{
			lblScore.Font = FontHub.HelveticaHvCn20;
		}
	}
}

