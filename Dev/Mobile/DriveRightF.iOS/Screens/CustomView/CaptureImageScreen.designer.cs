// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("CaptureImageScreen")]
	partial class CaptureImageScreen
	{
		[Outlet]
		UIKit.UIButton _btnCancel { get; set; }

		[Outlet]
		UIKit.UIView _btnContainer { get; set; }

		[Outlet]
		UIKit.UIButton _btnDone { get; set; }

		[Outlet]
		UIKit.UIView _controlView { get; set; }

		[Outlet]
		UIKit.UIView _cropView { get; set; }

		[Outlet]
		UIKit.UIImageView _imageCrop { get; set; }

		[Outlet]
		UIKit.UIView _overlayBottom { get; set; }

		[Outlet]
		UIKit.UIView _overlayTop { get; set; }

		[Outlet]
		UIKit.UIPanGestureRecognizer _panGes { get; set; }

		[Outlet]
		UIKit.UIPinchGestureRecognizer _pinchGes { get; set; }

		[Outlet]
		UIKit.UIButton btnCancel { get; set; }

		[Outlet]
		UIKit.UIButton btnDone { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (_btnCancel != null) {
				_btnCancel.Dispose ();
				_btnCancel = null;
			}

			if (_btnContainer != null) {
				_btnContainer.Dispose ();
				_btnContainer = null;
			}

			if (_btnDone != null) {
				_btnDone.Dispose ();
				_btnDone = null;
			}

			if (_controlView != null) {
				_controlView.Dispose ();
				_controlView = null;
			}

			if (_cropView != null) {
				_cropView.Dispose ();
				_cropView = null;
			}

			if (_imageCrop != null) {
				_imageCrop.Dispose ();
				_imageCrop = null;
			}

			if (_overlayBottom != null) {
				_overlayBottom.Dispose ();
				_overlayBottom = null;
			}

			if (_overlayTop != null) {
				_overlayTop.Dispose ();
				_overlayTop = null;
			}

			if (_panGes != null) {
				_panGes.Dispose ();
				_panGes = null;
			}

			if (_pinchGes != null) {
				_pinchGes.Dispose ();
				_pinchGes = null;
			}

			if (btnDone != null) {
				btnDone.Dispose ();
				btnDone = null;
			}

			if (btnCancel != null) {
				btnCancel.Dispose ();
				btnCancel = null;
			}
		}
	}
}
