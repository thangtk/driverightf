﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;

namespace DriveRightF.iOS
{
	[Register("TitleHeaderView")]
	public class TitleHeaderView : HeaderView
	{
		private UIImageView _rightLogo;
		private UILabel _title;
		private UIFont _titleFont;

		public UIImage LogoImage {
			get { return _rightLogo.Image;}
			set { _rightLogo.Image = value;}
		}

		public string Title {
			get { return _title.Text;}
			set { _title.Text = value;}
		}

		public UIFont TitleFont {
			get { return _titleFont;}
			set { _title.Font = value;}
		}

		public TitleHeaderView () : base()
		{
			InitView ();
		}

		public TitleHeaderView(CGRect frame) : base(frame)
		{
			InitView ();
		}

		public TitleHeaderView(IntPtr handler) : base(handler)
		{
			InitView ();
		}

		protected override void InitView ()
		{
			base.InitView ();

			BackgroundImage = null;

			_rightLogo = new UIImageView (new CGRect (280, 25, 30, 30));
			AddSubview (_rightLogo);

			_title = new UILabel (new CGRect(0, 25, 320, 40));
			_title.BackgroundColor = UIColor.Clear;
			_title.TextAlignment = UITextAlignment.Center;
			_title.TextColor = UIColor.White;
			_title.Font = FontHub.HelveticaMedium17;
			AddSubview(_title);
		}
	}
}

