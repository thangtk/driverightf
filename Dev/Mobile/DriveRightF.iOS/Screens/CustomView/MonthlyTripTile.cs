﻿
using System;

using Foundation;
using UIKit;
using ObjCRuntime;
using CoreGraphics;
using Unicorn.Core.UI;
using DriveRightF.Core.ViewModels;

namespace DriveRightF.iOS
{
	[Register ("MonthlyTripTile")]
	public partial class MonthlyTripTile : UBaseView<TripTileViewModel>
	{
		private float _score;
		private string _local;
		private string _month;
		public float Score {
			get
			{
				return _score;
			}
			set
			{
				_score = value;
				lblScore.Text = value.ToString();
			}
		}
		public string Local {
			get
			{
				return _local;
			}
			set
			{
				_local = value;
				lblScore.Text = value.ToString();
			}
		}
		public string Month {
			get
			{
				return _month;
			}
			set
			{
				_month = value;
				lblScore.Text = value.ToString();
			}
		}
		public MonthlyTripTile() : base()
		{
			InitView();
			SetFont ();

		}

		public MonthlyTripTile(IntPtr h): base(h)
		{
			InitView ();
			SetFont ();

		}
		private void InitView()
		{
			var arr = NSBundle.MainBundle.LoadNib("MonthlyTripTile", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			v.Frame = new CGRect(0, 0, Frame.Width, Frame.Height);
			AddSubview(v);
		}

		private void SetFont()
		{
			lblScore.Font = FontHub.HelveticaHvCn20;
			lblLocal.Font = FontHub.HelveticaThin13;
			lblMonth.Font = FontHub.HelveticaThin13;
		}
	}
}

