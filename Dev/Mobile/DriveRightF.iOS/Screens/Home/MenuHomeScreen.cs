﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using UIKit;
using Unicorn.Core.iOS;

namespace DriveRightF.iOS
{
	public class MenuHomeScreen : DriveRightBaseViewController<HomeViewModel>
	{
		private const float ANIMATION_DURATION = 0.3f;
		private const float MAX_ALPHA_OVERLAY = 0.3f;

		private UIViewController _contentViewController;
		private UIViewController _leftMenuViewController;
		private UIView _overlayView;
		private UIButton _btnMenu;

		private bool _isMenuVisible = false;
		private float _maxOffSetLeftMenuRatio = 0.8f;
		private bool _isScrolled = false;
		private CGPoint _lastTouchPosition;
		private CGPoint _originPosition;

		public UIViewController ContentViewController {
			private get {
				return _contentViewController;
			}
			set {
				_contentViewController = value;
			}
		}

		public UIViewController LeftMenuViewController {
			private get {
				return _leftMenuViewController;
			}
			set {
				_leftMenuViewController = value;
			}
		}

		public MenuHomeScreen ()
		{

		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			InitView ();
			InitBinding ();
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);

			List<UIView> subViews = new List<UIView> (_contentViewController.View.Subviews);
			subViews.ForEach (v => {
				if (v is CircularMainMenu)
					(v as CircularMainMenu).IsVisible = true;
			});
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);

			if (_isMenuVisible)
				ShowContentViewController ();
		}

		private void InitView()
		{
			_overlayView = new UIView () {
				Frame = _contentViewController.View.Frame,
				Alpha = 0,
				BackgroundColor = UIColor.Black,
				UserInteractionEnabled = false
			};

			_btnMenu = new UIButton () {
				Frame = new CGRect (18, 29, 50, 40),
				ImageEdgeInsets = new UIEdgeInsets(0, 0, 15, 25),
				BackgroundColor = UIColor.Clear,
			};
			_btnMenu.SetImage (UIImage.FromFile ("logo_menu.png"), UIControlState.Normal);
			_contentViewController.View.AddSubview (_btnMenu);
			_contentViewController.View.AddGestureRecognizer (OnTapContentViewController ());

			View.AddSubview (_contentViewController.View);
			View.AddSubview (_leftMenuViewController.View);
			View.AddSubview (_overlayView);

			View.BringSubviewToFront (_contentViewController.View);
			View.BringSubviewToFront (_overlayView);

			View.AddGestureRecognizer (new UIPanGestureRecognizer (OnPanGestureRecognizer));
		}

		private void InitBinding()
		{
			ViewModel = new HomeViewModel ();
			this.BindCommand (
				ViewModel,
				vm => vm.MenuButtonClickCommand,
				v => v._btnMenu,
				() => new Action (delegate {
					if (_isMenuVisible)
						ShowContentViewController ();
					else
						HideContentViewController ();
				}));
		}

		private void HideContentViewController()
		{
			float deltaX = (float)View.Frame.Width * _maxOffSetLeftMenuRatio;

			UIView.Animate (ANIMATION_DURATION,
				() => {
					_overlayView.Alpha = MAX_ALPHA_OVERLAY;
					FrameExtension.ChangeFrameAttributes(view: _overlayView, x: deltaX);

					FrameExtension.ChangeFrameAttributes(view: _contentViewController.View, x: deltaX);
				},
				() => {
					_isMenuVisible = true;
					SetUserInteractionContentView (false);
				}
			);
		}

		private void ShowContentViewController()
		{
			UIView.Animate (ANIMATION_DURATION,
				() => {
					_overlayView.Alpha = 0;
					FrameExtension.ChangeFrameAttributes(view: _overlayView, x: 0);

					FrameExtension.ChangeFrameAttributes(view: _contentViewController.View, x: 0);
				},
				() => {
					_isMenuVisible = false;
					SetUserInteractionContentView (true);
				}
			);
		}

		private void SetUserInteractionContentView(bool value)
		{
			List<UIView> subViews = new List<UIView> (_contentViewController.View.Subviews);

			subViews.ForEach (v => {
				v.UserInteractionEnabled = value;
			});

			_btnMenu.UserInteractionEnabled = true;
		}

		private UITapGestureRecognizer OnTapContentViewController()
		{
			return new UITapGestureRecognizer (() => {
				ShowContentViewController ();
			});
		}

		private void OnPanGestureRecognizer(UIPanGestureRecognizer recognizer)
		{
			CGPoint touchPosition = recognizer.LocationInView (View);
			float destinationOffset = (float)View.Frame.Width * _maxOffSetLeftMenuRatio;

			if (recognizer.State == UIGestureRecognizerState.Began) {
				_isScrolled = true;
				_originPosition = _contentViewController.View.Frame.Location;
			} else if (recognizer.State == UIGestureRecognizerState.Changed) {
				if (_isScrolled) {
					float delta = (float)(touchPosition.X - _lastTouchPosition.X);

					if ((float)_contentViewController.View.Frame.X + delta >= 0 && (float)_contentViewController.View.Frame.X + delta <= destinationOffset) {
						FrameExtension.ChangeFrameAttributes (view: _contentViewController.View,
							x: (float)_contentViewController.View.Frame.X + delta);
						FrameExtension.ChangeFrameAttributes (view: _overlayView,
							x: (float)_overlayView.Frame.X + delta);
						_overlayView.Alpha = MAX_ALPHA_OVERLAY * _overlayView.Frame.X / destinationOffset;
					}
				}
			} else if (recognizer.State == UIGestureRecognizerState.Ended) {
				_isScrolled = !_isScrolled;

				float velocity = (float)recognizer.VelocityInView (View).X;

				if (velocity < 0) {
					if (velocity < -350)
						ShowContentViewController ();
					else
						if (_contentViewController.View.Frame.X < destinationOffset / 2)
							ShowContentViewController ();
						else
							HideContentViewController ();
				} else {
					if (velocity > 350)
						HideContentViewController ();
					else
						if (_contentViewController.View.Frame.X > destinationOffset / 2)
							HideContentViewController ();
						else
							ShowContentViewController ();
				}
			}

			_lastTouchPosition = touchPosition;
		}

		public override void ReloadView(){
			if (_isMenuVisible)
				ShowContentViewController ();
			(_leftMenuViewController as LeftMenuScreen).ReloadView ();
		}

		public override void LoadData(params object[] objs){
		}

		public override bool IsShouldCache(){
			return true;
		}
	}
}

