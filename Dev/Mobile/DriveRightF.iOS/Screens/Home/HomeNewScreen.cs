﻿
using System;

using System.Reactive.Concurrency;
using System.Reactive.Linq;
using DriveRightF.Core;
using DriveRightF.Core.Enums;
using DriveRightF.Core.ViewModels;
using Foundation;
using ReactiveUI;
using UIKit;
using Unicorn.Core;
using System.Diagnostics;

namespace DriveRightF.iOS
{
	public partial class HomeNewScreen : BaseHandlerHeaderViewController<HomeNewViewModel>
	{
		public HomeNewScreen () : base ("HomeNewScreen", null)
		{
		}

		public override void LoadData(params object[] objs)
		{
			base.LoadData(objs);

			if (ViewModel != null) {
			}
		}

		private void InitTranslator()
		{
			lblAward.Text = Translator.Translate("HOME_AWARDS");
			lblBalance.Text = Translator.Translate("HOME_BALANCE");
			lblTrips.Text = Translator.Translate("HOME_TRIPS");
			lblAwardDesc.Text = Translator.Translate("HOME_AWARDS_DESC");
			lblBalanceDesc.Text = Translator.Translate("HOME_BALANCE_DESC");
			lblTripsDesc.Text = Translator.Translate("HOME_TRIPS_DESC");
		}

		public override void ReloadView ()
		{
			base.ReloadView ();
			if (ViewModel != null) {
				ViewModel.LoadLocalData.Execute (null);
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			if(ViewModel == null) ViewModel = new HomeNewViewModel ();
			InitCommands ();
			RxApp.TaskpoolScheduler.Schedule (InitBindings);
//			ViewModel.LoadLocalData.Execute (null);
			// TODO: use this control later
			notificationView.Hidden = true;

			InitTranslator();
		}

		private void InitBindings(){
			ViewModel.WhenAnyValue(vm => vm.BannerImages).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x => homeBanner.BannerImages = x);
			ViewModel.WhenAnyValue(vm => vm.SummaryTripScore).Skip(1).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x => lblSummaryTripScore.Text = x <=0 ? "--" : x.ToString());
			ViewModel.WhenAnyValue(vm => vm.NumOfAwards).Skip(1).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x => lblNumOfAwards.Text = x.ToString());
			ViewModel.WhenAnyValue(vm => vm.CurrentBalance).Skip(1).ObserveOn(RxApp.MainThreadScheduler).Subscribe(x => {
				lblCurrentBalance.Text = x.ToString();
//				Debug.WriteLine(" >>> " + x + " dddd " + ViewModel.CurrentBalance);
			});
			// Load data after init bindings
			ViewModel.LoadLocalData.Execute (null);
		}

		void InitCommands ()
		{
			vAwardItem.AddGestureRecognizer (new UITapGestureRecognizer (() =>  {
				//				if (DependencyService.Get<IHomeTabNavigator> ().CurrentScreen != (int)Screen.TabAward) {
				//					DependencyService.Get<IHomeTabNavigator> ().Navigate ((int)Screen.TabAward);
				//				} else {
				//					DependencyService.Get<IHomeTabNavigator> ().CurrentTab.TabClick ();
				//				}
				DependencyService.Get<IHomeTabNavigator> ().CurrentTab.Navigate ((int)Screen.HomeAward);
			}));
			vTripsItem.AddGestureRecognizer (new UITapGestureRecognizer (() =>  {
				//				if(DependencyService.Get<IHomeTabNavigator> ().CurrentScreen != (int)Screen.TabTrips){
				//					DependencyService.Get<IHomeTabNavigator> ().Navigate ((int)Screen.TabTrips);
				//				}else{
				//					DependencyService.Get<IHomeTabNavigator> ().CurrentTab.TabClick();
				//				}
				DependencyService.Get<IHomeTabNavigator> ().CurrentTab.Navigate ((int)Screen.HomeTrips);
			}));
			vAccountItem.AddGestureRecognizer (new UITapGestureRecognizer (() =>  {
				//				if(DependencyService.Get<IHomeTabNavigator> ().CurrentScreen != (int)Screen.TabAccount){
				//					DependencyService.Get<IHomeTabNavigator> ().Navigate ((int)Screen.TabAward);
				//				}else{
				//					DependencyService.Get<IHomeTabNavigator> ().CurrentTab.TabClick();
				//				}
				DependencyService.Get<IHomeTabNavigator> ().CurrentTab.Navigate ((int)Screen.HomeAccount);
			}));
		}
	}
}

