// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("HomeNewScreen")]
	partial class HomeNewScreen
	{
		[Outlet]
		DriveRightF.iOS.HomeBanner homeBanner { get; set; }

		[Outlet]
		UIKit.UILabel lblAward { get; set; }

		[Outlet]
		UIKit.UILabel lblAwardDesc { get; set; }

		[Outlet]
		UIKit.UILabel lblBalance { get; set; }

		[Outlet]
		UIKit.UILabel lblBalanceDesc { get; set; }

		[Outlet]
		UIKit.UILabel lblCurrentBalance { get; set; }

		[Outlet]
		UIKit.UILabel lblNumOfAwards { get; set; }

		[Outlet]
		UIKit.UILabel lblSummaryTripScore { get; set; }

		[Outlet]
		UIKit.UILabel lblTrips { get; set; }

		[Outlet]
		UIKit.UILabel lblTripsDesc { get; set; }

		[Outlet]
		DriveRightF.iOS.NotificationView notificationView { get; set; }

		[Outlet]
		UIKit.UIView vAccountItem { get; set; }

		[Outlet]
		UIKit.UIView vAwardItem { get; set; }

		[Outlet]
		UIKit.UIView vTripsItem { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (homeBanner != null) {
				homeBanner.Dispose ();
				homeBanner = null;
			}

			if (lblCurrentBalance != null) {
				lblCurrentBalance.Dispose ();
				lblCurrentBalance = null;
			}

			if (lblNumOfAwards != null) {
				lblNumOfAwards.Dispose ();
				lblNumOfAwards = null;
			}

			if (lblSummaryTripScore != null) {
				lblSummaryTripScore.Dispose ();
				lblSummaryTripScore = null;
			}

			if (notificationView != null) {
				notificationView.Dispose ();
				notificationView = null;
			}

			if (vAccountItem != null) {
				vAccountItem.Dispose ();
				vAccountItem = null;
			}

			if (vAwardItem != null) {
				vAwardItem.Dispose ();
				vAwardItem = null;
			}

			if (vTripsItem != null) {
				vTripsItem.Dispose ();
				vTripsItem = null;
			}

			if (lblAward != null) {
				lblAward.Dispose ();
				lblAward = null;
			}

			if (lblAwardDesc != null) {
				lblAwardDesc.Dispose ();
				lblAwardDesc = null;
			}

			if (lblTrips != null) {
				lblTrips.Dispose ();
				lblTrips = null;
			}

			if (lblTripsDesc != null) {
				lblTripsDesc.Dispose ();
				lblTripsDesc = null;
			}

			if (lblBalanceDesc != null) {
				lblBalanceDesc.Dispose ();
				lblBalanceDesc = null;
			}

			if (lblBalance != null) {
				lblBalance.Dispose ();
				lblBalance = null;
			}
		}
	}
}
