﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core.ViewModels;
using DriveRightF.Core;
using System.Collections.Generic;
//using DriveRightF.iOS.Navigator;
using Unicorn.Core;
using DriveRightF.Core.Enums;
using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading;
using Unicorn.Core.Navigator;
using DriveRightF.Core.Constants;
using Unicorn.Core.UI;
using CoreLocation;
using Unicorn.Core.iOS;
using ObjCRuntime;
using DriveRightF.Core.iOS;
using DriveRightF.Core.Models;

namespace DriveRightF.iOS
{
	public partial class HomeScreen : BaseAutoLayoutScreen<BaseViewModel>
	{
		private HomeNavigator _nav;
		private HeaderViewModel _headerViewModel;

		private CLLocationManager _locationManager;
		private PopupManager _popupManager;

		public HomeScreen () : base ()
		{
			HomeNavigator nav = new HomeNavigator (this);
			_nav = nav;

			DependencyService.RegisterGlobalInstance<IHomeTabNavigator> (nav);

			UDelegate = new HomeTransitionDelegate (this);
			_headerViewModel = new HeaderViewModel () {
				Title = Translator.Translate("DRIVE_RIGHT_HEADER"),
				IconProfile = "",
				IconNotification = "message_ico@2x.png",
				IsShowBtnBack = false,
			};
			DependencyService.RegisterGlobalInstance<HeaderViewModel> (_headerViewModel);

			MessagingCenter.Subscribe<BaseViewModel> (this, MessageConstant.SCREEN_BLOCK, x=>
				{
					RxApp.MainThreadScheduler.Schedule(() =>
						{
							if(View != null)
							{
								View.UserInteractionEnabled = !x.IsBlockUI;
							}
						});

				});

			MessagingCenter.Subscribe<BaseViewModel> (this, MessageConstant.TAB_BAR_BLOCK, x => {
				RxApp.MainThreadScheduler.Schedule (() => {
					if (TabView != null) {
						IsEnableTabBar = x.IsBlockTabBar;
					}
				});
			});

			MessagingCenter.Subscribe<BaseViewModel> (this, MessageConstant.NAVIGATE_TO_HOME, x => {
				RxApp.MainThreadScheduler.Schedule (() => _headerViewModel.IsNavigateToHome = true);
			});

			//DependencyService.Get<IHomeTabNavigator>().
		}

		public override void LoadView(){
			base.LoadView();
		}

		public override void ViewWillLayoutSubviews(){
			base.ViewWillLayoutSubviews();
		}

		public override void ViewDidLayoutSubviews(){
			base.ViewDidLayoutSubviews();
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		private void InitMaskSupperView()
		{
			MaskSupperView = (UIView)DependencyService.Get<ICoinView> ();
		}

		private void InitHeader ()
		{
			HeaderView = new DHeaderView (_headerViewModel);
			//			(HeaderView as DHeaderView).ViewModel = _headerViewModel;
			(HeaderView as DHeaderView).OnBtnNotificationClicked += (object sender, string e) =>  {
				if (RightMenu == null) {
					InitRightMenu ();
				}
//				this.OpenMenu ();

				// Navigate instead of open menu
				DependencyService.Get<INavigator>().Navigate((int)Screen.Notification);
			};
			(HeaderView as DHeaderView).OnBtnProfileClicked += (object sender, string e) =>  {
				this.CloseView ();
			};
			(HeaderView as DHeaderView).OnBtnBackClicked += (object sender, string e) => {
				if (ViewControllers.Count > 0) {
					var top = TopViewController;
					if (top is UBaseViewController) {
						(top as UBaseViewController).BeforeBack ();
					}
				}

				if (!_headerViewModel.IsNavigateToHome)
					DependencyService.Get<IHomeTabNavigator> ().CurrentTab.NavigateBack (true);
				else {	
					DependencyService.Get<IHomeTabNavigator> ().CurrentTab.Navigate ((int)Screen.HomeAward);
					_headerViewModel.IsNavigateToHome = false;
				}
			};
		}

		private void InitTabBar ()
		{
			var vmT = new DRTabBarViewModel ();
			TabView = new DTabBar ()
			{
				ViewModel = vmT
			};
			List<DTabItem> items = new List<DTabItem> {
				new DTabItem (vmT.ItemModels[0],null),
				new DTabItem (vmT.ItemModels[1],null),
				new DTabItem (vmT.ItemModels[2],null)
			};
			(TabView as DTabBar).AddTabItems (items);
			(TabView as DTabBar).OnTabSelected += (object sender, int e) =>  {
				switch (e) {
				case 0:
					if(_nav.CurrentScreen != (int)Screen.TabMain){
						DependencyService.Get<IHomeTabNavigator> ().Navigate ((int)Screen.TabMain);
					}else{
						DependencyService.Get<IHomeTabNavigator> ().CurrentTab.TabClick();
					}

					break;
				case 1:
					if(_nav.CurrentScreen != (int)Screen.TabProfile){
						DependencyService.Get<IHomeTabNavigator> ().Navigate ((int)Screen.TabProfile);
					}else{
						DependencyService.Get<IHomeTabNavigator> ().CurrentTab.TabClick();
					}
					//TODO Navigate to profile.
//					DependencyService.Get<INavigator> ().Navigate ((int)Screen.Profile);
					break;
				case 2:
					if(_nav.CurrentScreen != (int)Screen.TabMore){
						DependencyService.Get<IHomeTabNavigator> ().Navigate ((int)Screen.TabMore);
					}else{
						DependencyService.Get<IHomeTabNavigator> ().CurrentTab.TabClick();
					}
					break;
				}
			};
		}

		private void InitRightMenu ()
		{
			RightMenu = new NotificationScreen ().View;
		}

		private void InitUnderGroundView ()
		{
			UnderGroundView = new DMenu () {
				ViewModel = new MenuViewModel ()
			};
		}

		private void StartSDKService()
		{
			DependencyService.Get<IPolaraxSDK> ().StartService (x => Console.WriteLine (x.ErrorMessage));
		}

		private void CheckPermissitonLocation ()
		{
			ApplicationStatus status = DependencyService.Get<ApplicationUtil> ().Status;
			if (status == ApplicationStatus.New) {
				DependencyService.Get<GpsNotificationManager> ().RequestAuthorization ();
				DependencyService.Get<ApplicationUtil> ().Status = ApplicationStatus.AtFirstTime;
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			Debugger.ShowCost ("Navigate ((int)Screen.HomeAward) - begin");
			_nav.Navigate ((int)Screen.TabMain);
			Debugger.ShowCost ("Navigate ((int)Screen.HomeAward) - done");
			InitHeader ();
			Debugger.ShowCost ("Home Screen - ViewDidLoad - 1");
			InitTabBar ();
			Debugger.ShowCost ("Home Screen - ViewDidLoad - 2");
			InitUnderGroundView ();
			Debugger.ShowCost ("Home Screen - ViewDidLoad - 3");
//			InitRightMenu ();	
			Debugger.ShowCost ("Home Screen - ViewDidLoad - 4");
			InitMaskSupperView ();
			//TODO: start Service PolaraxSDK
			StartSDKService();
//			Unicorn.Core.Debugger.ShowCost(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> HomeScreen viewdidload");
			// Perform any additional setup after loading the view, typically from a nib.
			Debugger.ShowCost ("Home Screen - ViewDidLoad - done");

			// Ask for allow location service
			CheckPermissitonLocation();
		}
	}

	public class HomeTransitionDelegate : UTransitionControllerDelegate {
		HomeScreen _home;
		public HomeTransitionDelegate(HomeScreen screen){
			_home= screen;
		}

		public override void PopViewDidFinish(UIViewController transitionController ,UIViewController viewController ,bool animated){
			_home.View.UserInteractionEnabled = true;
		}

		public override void PushViewDidFinish(UIViewController transitionController ,UIViewController viewController ,bool animated){
			_home.View.UserInteractionEnabled = true;
		}

		public override void StartPopView(UIViewController transitionController ,UIViewController viewController ,bool animated){
			_home.View.UserInteractionEnabled = false;
		}

		public override void StartPushView(UIViewController transitionController ,UIViewController viewController ,bool animated){
			_home.View.UserInteractionEnabled = false;
		}
	}
}

