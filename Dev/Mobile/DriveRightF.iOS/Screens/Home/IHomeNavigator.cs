﻿using System;

namespace DriveRightF.iOS
{
	public interface IHomeNavigator
	{
		void NavigateToStart (int screenId);

		void Navigate (int screenId,object[] objs = null,object[] transitionParam = null);

		void NavigateBack (bool isRefresh = false);
		//void NavigateBack (bool isRefresh = false,object[] objs = null);
		void NavigateBack (int screenId,bool isRefresh = false);

		int CurrentScreen { get;}


		void OpenMenu ();

		void CloseMenu ();

		void ToggleMenu ();
	}
}

