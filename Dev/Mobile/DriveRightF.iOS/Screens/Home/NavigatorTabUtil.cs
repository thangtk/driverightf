﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;

namespace DriveRightF.iOS
{
	public class NavigateTabScreen
	{
		public int RootScreen { get; set;}
		public int CurrentScreen {
			get{ 
				return PreviousScreens.ElementAt(0);
			}
		}
		public Stack<int> _previousScreens = new Stack<int> ();
		public Stack<int> PreviousScreens
		{
			get {
				return _previousScreens;
			}
			set {
				_previousScreens = value;
			}
		}
	}

	public class NavigatorTabUtil
	{
		public NavigatorTabUtil ()
		{
		}

		public static NavigateTabScreen GetCurrentScreen(int screen, ConcurrentDictionary<int,NavigateTabScreen> tabScreen)
		{
			for (int i = 0; i < tabScreen.Count; i++) {
				var item = tabScreen.Values.ElementAt (i);
				if (item.PreviousScreens.Contains (screen)) {
					return item;
				}
			}

			return null;

		}

		public static NavigateTabScreen GetCurrentScreen(int screen,int lastCurrentScreen, ConcurrentDictionary<int,NavigateTabScreen> tabScreen)
		{
			if(tabScreen.ContainsKey(screen))
			{
				return tabScreen[screen];
			}

			return GetCurrentScreen(lastCurrentScreen,tabScreen);
		}
	}
}

