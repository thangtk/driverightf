﻿using System;
using Unicorn.iOS.Controls;
using CoreGraphics;
using UIKit;
using Foundation;
using System.Collections.Generic;

namespace DriveRightF.iOS
{
	[Register("HomeBanner")]
	public class HomeBanner : ViewPager3DTransition
	{

		public HomeBanner (CGRect frame) : base (frame)
		{
		}

		public HomeBanner (IntPtr handler) : base (handler)
		{
		}

		protected override void InitData ()
		{
			PageCount = 1;
		}

		protected override UIView GetPageView (int index)
		{
			if (BannerImages == null || BannerImages.Count == 0) {
				if (PageCount > 0) {
					// Return default image
					return new UIImageView(UIImage.FromBundle("banner_default.png"));
				}
			}
			UIImageView v = null;
			if (BannerImages != null) {
				v = new UIImageView();
				string name = string.Format (BannerImages[index]);
				v.Image = UIImage.FromBundle(name);
			}
			//
			return v?? new UIImageView();
		}

		private List<string> _bannerImages;
		public List<string> BannerImages {
			get{ 
				return _bannerImages;
			}
			set{ 
				if (_bannerImages != value) {
					_bannerImages = value;
					PageCount = _bannerImages != null ? _bannerImages.Count : 0;
				}
			}
		}
	}
}

