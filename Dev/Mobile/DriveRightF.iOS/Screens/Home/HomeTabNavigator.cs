﻿using System;
using System.Collections.Concurrent;
[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.iOS.HomeTabNavigator))]
namespace DriveRightF.iOS
{
//	public interface IHomeTabNavigator
//	{
//		void Navigate (int screenId,object[] objs = null,object[] transitionParam = null, bool isRoot = false);
//	}

	public abstract class HomeTabNavigator : BaseHomeNavigator, IHomeTabNavigator
	{
		private readonly ConcurrentDictionary<int,NavigateTabScreen> _listTabScreen = new ConcurrentDictionary<int,NavigateTabScreen> ();

		public HomeTabNavigator (HomeScreen screen):base(screen)
		{
		}

		#region IHomeTabNavigator implementation

		public void Navigate (int screenId, object[] objs = null, object[] transitionParam = null, bool isRoot = false)
		{
			int updateScreenId = screenId;
			if (isRoot) {
				NavigateTabScreen navigateScreen;
				if (_listTabScreen.ContainsKey (screenId)) {

					navigateScreen = _listTabScreen [screenId];
					NavigateTabScreen navigatePreviousScreen = NavigatorTabUtil.GetCurrentScreen (CurrentScreen, _listTabScreen);

					if (navigatePreviousScreen.RootScreen != navigateScreen.RootScreen) {
						updateScreenId = navigateScreen.CurrentScreen;
					} else {
						updateScreenId = screenId;
					}
					navigateScreen.PreviousScreens.Push (updateScreenId);

				} else {
					navigateScreen = new NavigateTabScreen () {
						RootScreen = screenId,
					};
					navigateScreen.PreviousScreens.Push (screenId);
					_listTabScreen.TryAdd (screenId, navigateScreen);
				}

			} else {

				//TODO: check current screen:
				// Need check tab 2 times to tab => navigate to roo
				NavigateTabScreen navigateScreen = NavigatorTabUtil.GetCurrentScreen (screenId,CurrentScreen,_listTabScreen);
				navigateScreen.PreviousScreens.Push (updateScreenId);

			}

			base.Navigate (updateScreenId, objs, transitionParam);
		}

		public override void Navigate (int screenId, object[] objs = null, object[] transitionParam = null)
		{
			this.Navigate (screenId, objs, transitionParam, false);
		}

		public override void NavigateBack (bool isRefresh = false)
		{
			NavigateTabScreen navigateScreen = NavigatorTabUtil.GetCurrentScreen (CurrentScreen, _listTabScreen);
			navigateScreen.PreviousScreens.Pop ();
			base.NavigateBack (isRefresh);

		}

		#endregion
	}



}

