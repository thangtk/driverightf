﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRight.Core.ViewModels;
using CoreGraphics;
using DriveRightF.Core.ViewModels;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using DriveRightF.Core.Enums;
using Unicorn.Core;

namespace DriveRightF.iOS
{
	public partial class AccountScreen : BaseHandlerHeaderViewController<AccountListViewModel>
	{
		public AccountScreen() : base ("AccountScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void LoadData (params object[] objs)
		{
			base.LoadData (objs);
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad ();
			
			InitView ();

			ViewModel = new AccountListViewModel ();

			SetData ();
		}

		private void SetData()
		{
			ViewModel.ParentSize = new Size ((int)View.Frame.Width, (int)View.Frame.Height);
			ViewModel.ComputeTileFrame (ViewModel.TileViewModels);

			int bottom = 0, top = 0, left = 0, right = 0;
			bool IsSet = false;
			ViewModel.TileViewModels.ForEach (tile => {
				int index = ViewModel.TileViewModels.IndexOf (tile);
				Console.WriteLine("+++" + tile.Position.Y);
				UIView cellView;
				if(index == 0)
				{
					cellView = new BigItemAccountView()
					{
						Frame = new CGRect (tile.Position.X, tile.Position.Y, tile.Size.Width, tile.Size.Height),
						ViewModel = tile as BigItemAccountViewModel
					};
				}
				else
				{
					cellView = new SmallItemAccountView()
					{
						Frame = new CGRect (tile.Position.X, tile.Position.Y, tile.Size.Width, tile.Size.Height),
						ViewModel = tile as SmallItemAccountViewModel
					};

					((SmallItemAccountView)cellView).OnClick += (sender, e) => {
						//TODO: retrive month data from ViewModel

						DependencyService.Get<IHomeNavigator>().Navigate((int)Screen.AccountStatement, new object[] {tile.Month.ToString("yyyy-MM")});
					};

				}

				if (!IsSet) {
					bottom = tile.Position.Y;
					left = tile.Position.X;
					right = tile.Position.X + tile.Size.Width;
					top = tile.Position.Y + tile.Size.Height;	
					IsSet = true;
				}

				if (tile.Position.Y < bottom) 
					cellView.Transform = CGAffineTransform.MakeTranslation (0, -100);
				if (tile.Position.Y > bottom) 
					cellView.Transform = CGAffineTransform.MakeTranslation (0, 100);
				if (tile.Position.X > right)
					cellView.Transform = CGAffineTransform.MakeTranslation (100, 0);
				if (tile.Position.Y < bottom && tile.Position.X > right)
					cellView.Transform = CGAffineTransform.MakeTranslation (100, -100);
				if (tile.Position.Y > top && tile.Position.X > right)
					cellView.Transform = CGAffineTransform.MakeTranslation (100, 100);

				UIView.Animate (
					0.5,
					0.2,
					UIViewAnimationOptions.CurveEaseOut,
					() => {
						cellView.Transform = CGAffineTransform.MakeIdentity ();
					},
					() => {

					}
				);
				contentView.AddSubview (cellView);
			});

			float rows = (float)ViewModel.TileViewModels.Count / 3f + 1f;
			contentView.Frame = new CGRect (0, 0, contentView.Frame.Width, rows * (float)View.Frame.Width / 3f);

			scrollView.ContentSize = contentView.Frame.Size;
		}

		private void InitView()
		{
			CGSize contentSize = scrollView.ContentSize;
			contentSize.Width = View.Frame.Width;
			contentSize.Height = contentView.Frame.Height;
			scrollView.ContentSize = contentSize;
			scrollView.AddSubview (contentView);

			CGPoint contentOffset = scrollView.ContentOffset;
			contentOffset.X = 0;
			contentOffset.Y = 0;
			scrollView.SetContentOffset (contentOffset, false);

			View.AddSubview (scrollView);
		}
	}
}

