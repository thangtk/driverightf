﻿
using System;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRight.Core.ViewModels;
using CoreGraphics;
using DriveRightF.Core.ViewModels;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using DriveRightF.Core.Enums;

namespace DriveRightF.iOS
{
	public partial class TripsScreen : BaseHandlerHeaderViewController<TripTileListViewModel>
	{
		public TripsScreen() : base ("TripsScreen", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad ();
			
			InitView ();

			ViewModel = new TripTileListViewModel ();

			SetData ();
		}

		private void SetData()
		{
			ViewModel.ParentSize = new Size ((int)View.Frame.Width, (int)View.Frame.Height);
			ViewModel.ComputeTileFrame (ViewModel.TileViewModels);

			int bottom = 0, top = 0, left = 0, right = 0;
			bool IsSet = false;
			ViewModel.TileViewModels.ForEach (tile => {
//				int index = ViewModel.TileViewModels.IndexOf (tile);

				var trip = new TripTile (tile) {
					BackgroundColor = tile.Colors,

					Image = tile.Image,
					Month = tile.Month,
					Score = tile.Score,
					Location = tile.Location,
					Frame = new CGRect (tile.Position.X, tile.Position.Y, tile.Size.Width, tile.Size.Height),
					Type = tile.TileType
				};

				if (!IsSet) {
					bottom = tile.Position.Y;
					left = tile.Position.X;
					right = tile.Position.X + tile.Size.Width;
					top = tile.Position.Y + tile.Size.Height;	
					IsSet = true;
				}


				if (tile.Position.Y < bottom) 
					trip.Transform = CGAffineTransform.MakeTranslation (0, -100);
				if (tile.Position.Y > bottom) 
					trip.Transform = CGAffineTransform.MakeTranslation (0, 100);
				if (tile.Position.X > right)
					trip.Transform = CGAffineTransform.MakeTranslation (100, 0);
				if (tile.Position.Y < bottom && tile.Position.X > right)
					trip.Transform = CGAffineTransform.MakeTranslation (100, -100);
				if (tile.Position.Y > top && tile.Position.X > right)
					trip.Transform = CGAffineTransform.MakeTranslation (100, 100);

				UIView.Animate (
					.5,
					0.2,
					UIViewAnimationOptions.CurveEaseOut,
					() => {
						trip.Transform = CGAffineTransform.MakeIdentity ();
					},
					() => {

					}
				);
				contentView.AddSubview (trip);
			});

			float rows = (float)ViewModel.TileViewModels.Count / 3f + 1f;
			contentView.Frame = new CGRect (0, 0, contentView.Frame.Width, rows * (float)View.Frame.Width / 3f);

			scrollView.ContentSize = contentView.Frame.Size;
		}

		private void InitView()
		{
			CGSize contentSize = scrollView.ContentSize;
			contentSize.Width = View.Frame.Width;
			contentSize.Height = contentView.Frame.Height;
			scrollView.ContentSize = contentSize;
			scrollView.AddSubview (contentView);

			CGPoint contentOffset = scrollView.ContentOffset;
			contentOffset.X = 0;
			contentOffset.Y = 0;
			scrollView.SetContentOffset (contentOffset, false);

			View.AddSubview (scrollView);
		}
	}
}

