﻿
using System;
using System.Drawing;

using Foundation;
using UIKit;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using DriveRightF.Core.iOS;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using System.Timers;
using CoreAnimation;
using CoreGraphics;
using System.Collections.Generic;
using DriveRightF.Core.Services;
using Unicorn.iOS.Controls;


namespace DriveRightF.iOS
{
	public partial class SplashScreen : DriveRightBaseViewController<SplashViewModel>
	{
		public SplashScreen () : base ("SplashScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			lblDesc.Text = Translator.Translate("SIGN_UP_DESCRIPTION_1");
			lblDesc.Font = FontHub.GetFont(FontHub.HelveticaCn, 16f);

//			ViewPager3DTransition v = new ViewPager3DTransition(new CGRect(0,100,320,160));
//			v.PageCount = 5;
//
//			this.Add (v);
			//
			base.ViewDidLoad ();
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);

//			DriveRightFServiceManager servive = DependencyService.Get<DriveRightFServiceManager> ();
//			servive.UserId = "030744b6-07cc-4f73-97bb-433959337080";
//			servive.UpdateRawImageProfile ("e7731ef3-7b1d-4d6c-86bf-43431d75ad4e","/Users/wolf-leader/Desktop/star_active.png","image/png");

			NSTimer.CreateScheduledTimer (0.01, (timer) => {
				CoinsAnimationView coinView = new CoinsAnimationView ();
				DependencyService.RegisterGlobalInstance<ICoinView> (coinView);
				coinView.AnimateCoins(1,null,isInit:true);
				ViewModel = new SplashViewModel ();
				ViewModel.LoadComponentCommand.Execute (null);
			});

	
			//Creates transformation animation
//			View.Layer.Transform = CATransform3D.MakeRotation ((float)Math.PI * 2, 0.5f, 0, 1);       
//			var animRotate = (CAKeyFrameAnimation)CAKeyFrameAnimation.FromKeyPath ("transform");  
//			animRotate.Duration = 5;
//			animRotate.Values = new NSObject[] { 
//				NSNumber.FromFloat (0f), 
//				NSNumber.FromFloat ((float)Math.PI / 2f), 
//				NSNumber.FromFloat ((float)Math.PI),
//				NSNumber.FromFloat ((float)Math.PI * 2)};
//			animRotate.ValueFunction = CAValueFunction.FromName (CAValueFunction.RotateY);
//
//			View.Layer.AddAnimation (animRotate, null);    
		}
	}
}

