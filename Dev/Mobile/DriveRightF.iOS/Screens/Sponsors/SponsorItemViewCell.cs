﻿
using System;
using System.Collections.Generic;

using System.Drawing;
using System.Linq;
using CoreGraphics;
using DriveRightF.Core.Enums;
using DriveRightF.Core.Models;
using DriveRightF.Core.ViewModels;
using Foundation;
using ReactiveUI;
using UIKit;
using Unicorn.Core.UI;
using DriveRightF.Core;
using ObjCRuntime;
using System.Reactive.Linq;

namespace DriveRightF.iOS
{
	public partial class SponsorItemViewCell : DriveRightTableViewCell<Sponsor,SponsorItemViewModel>
	{
		private UIButton _btnAdd;

		private SponsorItemViewCell (IntPtr handle) : base (handle)
		{
		}

		public override void Initialize(){
			InitControls ();
			InitBindings ();
		}
			
		private void InitBindings()
		{
			ViewModel = new SponsorItemViewModel ();

			ItemIcon.ViewModel = new ImageMultiStatusViewModel ((int)Screen.Sponsors);
			ViewModel.ItemIconViewModel = ItemIcon.ViewModel;

			var gr = new UITapGestureRecognizer (()=> ViewModel.RemoveItemStatusCommand.Execute(null));
			this.AddGestureRecognizer (gr);
			//
//			ViewModel.WhenAnyValue (vm => vm.ItemIconViewModel)
//				.ObserveOn(RxApp.MainThreadScheduler)
//				.Where (x => x!= null)
//				.Subscribe (ItemIcon.SetViewModel);
			this.OneWayBind (ViewModel, vm => vm.Name, v => v.lblName.Text);
			ViewModel.WhenAnyValue (vm => vm.Type, vm => vm.Status)
				.Where (c => c.Item1 == SponsorType.Insurer)
				.Subscribe (c => AddButton (c.Item2));
			// TODO: Change the way to add button
		}

		private void InitControls(){

			lblName.Font = FontHub.RobotoRegular17;
		}

		private void AddButton(SponsorStatus s)
		{
			_btnAdd = new UIButton () {
				Frame = new CGRect (255, 26, 45, 22)
			};

			if (s == SponsorStatus.NotRegistered)
				_btnAdd.SetImage (UIImage.FromFile("button_add2.png"), UIControlState.Normal);
			else if(s == SponsorStatus.Registered)
				_btnAdd.SetImage (UIImage.FromFile("edit_button.png"), UIControlState.Normal);
			_btnAdd.BackgroundColor = UIColor.Clear;

			_btnAdd.TouchUpInside += (sender, e) => {
				if (ViewModel != null) {
					ViewModel.RemoveItemStatusCommand.Execute(null);
					ViewModel.AddClickCommand.Execute(null);
				}
			};

			viewCell.AddSubview (_btnAdd);
		}
		
		public override void UpdateCell(Sponsor item, int index){
			ViewModel.Item = item;
		}
	}
}

