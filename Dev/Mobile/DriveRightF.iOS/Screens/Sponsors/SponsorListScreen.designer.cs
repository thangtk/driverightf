// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DriveRightF.iOS
{
	[Register ("SponsorListScreen")]
	partial class SponsorListScreen
	{
		[Outlet]
		DriveRightF.iOS.ButtonNewData _btnNewData { get; set; }

		[Outlet]
		UIKit.UITableView _tblSponsorList { get; set; }

		[Outlet]
		UIKit.UIView _viewHeader { get; set; }

		[Outlet]
		UIKit.UIButton btnSponsor { get; set; }

		[Outlet]
		DriveRightF.iOS.TitleHeaderView headerView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (_btnNewData != null) {
				_btnNewData.Dispose ();
				_btnNewData = null;
			}

			if (_viewHeader != null) {
				_viewHeader.Dispose ();
				_viewHeader = null;
			}

			if (btnSponsor != null) {
				btnSponsor.Dispose ();
				btnSponsor = null;
			}

			if (headerView != null) {
				headerView.Dispose ();
				headerView = null;
			}

			if (_tblSponsorList != null) {
				_tblSponsorList.Dispose ();
				_tblSponsorList = null;
			}
		}
	}
}
