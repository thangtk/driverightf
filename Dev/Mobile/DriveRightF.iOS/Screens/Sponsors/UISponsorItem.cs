﻿
using System;
using System.Drawing;

using Foundation;
using UIKit;
using CoreGraphics;
using DriveRightF.Core.Models;
using Unicorn.Core.UI;
using ReactiveUI;
using DriveRightF.Core.Enums;
using DriveRightF.Core.ViewModels;

namespace DriveRightF.Core
{
	public partial class UISponsorItem : UBaseTableViewCell<SponsorItemViewModel>
	{
		private Sponsor _sponsor;
		public UISponsorItem (Sponsor sponsor, bool hasData)
		{
			_sponsor = sponsor;
			InitView (sponsor, hasData);
			if (sponsor.Type == SponsorType.Insurer && hasData)
				AddButton ();
			InitBinding ();
		}

		private void InitBinding()
		{
			ViewModel = new SponsorItemViewModel ();
			ViewModel.Item = _sponsor;
			this.BindCommand (
				ViewModel, 
				vm => vm.BtnAddClick, 
				v => v._btnAdd
//				_sponsor != null ? String.Empty: _sponsor.SponsorID
			);
		}
	}
}

