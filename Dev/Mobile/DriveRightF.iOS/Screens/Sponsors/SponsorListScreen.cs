﻿
using System;
using System.Collections.Generic;
using System.Linq;

using System.Drawing;
using System.Reactive.Linq;
using CoreAnimation;
using CoreGraphics;
using DriveRightF.Core;
using DriveRightF.Core.Models;
using DriveRightF.Core.ViewModels;
using Foundation;
using ReactiveUI;
using UIKit;
using Unicorn.Core;
using DriveRightF.Core.Managers;

namespace DriveRightF.iOS
{
	public partial class SponsorListScreen : BaseAutoRequestListScreen<Sponsor> //DriveRightBaseViewController<SponsorsViewModel>
	{
		private SponsorTableViewSource _tableSource = new SponsorTableViewSource ();

		private string _sponsorId = string.Empty;
		public SponsorListScreen(): base()
		{
			MessagingCenter.Subscribe<SponsorItemViewModel> (this, "Click", (sender) => {
				// do something whenever the "Hi" message is sent
				_sponsorId = sender.Item.SponsorID;
			});
		}

		#region implemented abstract members of BaseAutoRequestListScreen

		public override void InitViewModel ()
		{
			ViewModel = new SponsorViewModel ();
		}

		public override void LoadData (params object[] objs)
		{
			base.LoadData (objs);
		}

		public override void ReloadView ()
		{
			base.ReloadView ();
			if (ViewModel != null) {
				if (!string.IsNullOrEmpty (_sponsorId)) {
					int index = _tableSource.Items.FindIndex (x => x.SponsorID == _sponsorId);
					_tableSource.Items [index] = DependencyService.Get<SponsorManager> ().Get (_sponsorId);
					ReloadRow (index);
					_sponsorId = string.Empty;
				}
			}
		}

		public override void InitControls ()
		{
			base.InitControls();

			_btnNewData.Alpha = 0;
			View.BringSubviewToFront (_btnNewData);
		}

		protected override UITableView ContentTableView {
			get {
				return _tblSponsorList;
			}
		}

		protected override DRTableViewSource<Sponsor> ContentTableSource {
			get {
				return _tableSource;
			}
		}

		#endregion

		public override void InitBindings ()
		{
			base.InitBindings ();
			//
			_btnNewData.OnClick += (sender, e) => {
				if (ViewModel != null)
					ViewModel.AppendNewdata.Execute(true);
				_btnNewData.Hide();
			};
			this.OneWayBind(
				ViewModel,
				vm => vm.HeaderTitle,
				v => v.headerView.Title);
		
			ViewModel.WhenAnyValue (x => x.HeaderLogo)
				.ObserveOn (RxApp.MainThreadScheduler)
				.Subscribe (x => {
					headerView.LogoImage = UIImage.FromBundle(x);		
			});
		}
	}
}

