﻿using System;
using UIKit;
using CoreGraphics;
using DriveRightF.Core;

using ReactiveUI;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using Unicorn.Core;
using System.Threading;
using AVFoundation;
using Foundation;
using System.Reactive.Concurrency;
using DriveRightF.Core.Constants;
using System.Drawing;


[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.iOS.CoinsAnimationView))]
namespace DriveRightF.iOS
{
	public class CoinsAnimationView : UIView, ICoinView
	//	DriveRightBaseView<CoinViewModel>
	//	, ICoinView
	{

		private UIImage[] _coinImageList;
		private UIView _maskView;

		AVAudioPlayer _player = AVAudioPlayer.FromUrl (NSUrl.FromFilename ("coinsflying.mp3"));
		float _defaultVolumn;
		public CoinsAnimationView () : base ()
		{

			InitializationView();
			_defaultVolumn = _player.Volume;
			//_player.Volume = 0.0f;
		}

		private void InitializationView()
		{
			InitControls ();
		}

		private void InitControls()
		{
			_coinImageList = new UIImage[20] {
				UIImage.FromBundle ("coin_1.png"),
				UIImage.FromBundle ("coin_2.png"),
				UIImage.FromBundle ("coin_3.png"),
				UIImage.FromBundle ("coin_4.png"),
				UIImage.FromBundle ("coin_5.png"),
				UIImage.FromBundle ("coin_6.png"),
				UIImage.FromBundle ("coin_7.png"),
				UIImage.FromBundle ("coin_8.png"),
				UIImage.FromBundle ("coin_9.png"),
				UIImage.FromBundle ("coin_10.png"),
				UIImage.FromBundle ("coin_11.png"),
				UIImage.FromBundle ("coin_12.png"),
				UIImage.FromBundle ("coin_13.png"),
				UIImage.FromBundle ("coin_14.png"),
				UIImage.FromBundle ("coin_15.png"),
				UIImage.FromBundle ("coin_16.png"),
				UIImage.FromBundle ("coin_17.png"),
				UIImage.FromBundle ("coin_18.png"),
				UIImage.FromBundle ("coin_19.png"),
				UIImage.FromBundle ("coin_20.png"),
			};
		}

		private void InitBinding()
		{
			//			this.WhenAnyValue (v => v.ViewModel)
			//				.Where (vm => vm != null)
			//				.Subscribe (vm => {
			//				if (ViewModel.CoinView == null)
			//					ViewModel.CoinView = this;
			//			});
		}
		private bool _firstTime = false;
		public void AnimateCoins (int quantity, Action endAnimate, Point startPosition =  new Point(), Point endPosition =  new Point(),bool isInit = false)
		{
			// TODO handle send message animation
			MessagingCenter.Send<ICoinView> (this, MessageConstant.COIN_ANIMATE);
			RxApp.MainThreadScheduler.Schedule (() => {
				if (_player != null) {
					if(isInit){
						_player.Volume = 0.0f;
					}else{
						_player.Volume = _defaultVolumn;
					}
					_player.Play ();

				}
			});


			if (_maskView == null) {
				_maskView = new UIView (Frame);
			} else {
				_maskView.RemoveFromSuperview ();
				_maskView = new UIView (Frame);
			}
			this.Add (_maskView);
			//			CGPoint aaa = new CGPoint ();
			nfloat x = 0;
			nfloat y = (UIScreen.MainScreen.Bounds.Height) / 3;
			nfloat width = UIScreen.MainScreen.Bounds.Width - x;
			nfloat height = UIScreen.MainScreen.Bounds.Height - y - 15f;

			if (!endPosition.IsEmpty) {
				x = startPosition.X;
				y = startPosition.Y;
				width = endPosition.X - x;
				height = endPosition.Y - y;
			}


			for (int i = 0; i < quantity; i++) {
				var animatedCircleImage = new UIImageView ();
				animatedCircleImage.AnimationImages = _coinImageList;
				animatedCircleImage.AnimationRepeatCount = 1;
				animatedCircleImage.AnimationDuration = isInit ? 0.01f : (1 + i * 0.2f);
				animatedCircleImage.Frame = new CGRect (x, y, width, height);
				animatedCircleImage.StartAnimating ();
				_maskView.AddSubview (animatedCircleImage);
			}

			//Delay 2 second.
			if (!isInit) {
				RxApp.TaskpoolScheduler.Schedule (() => {
					Thread.Sleep (2000);
					if (endAnimate != null) {
						endAnimate.Invoke ();
					}
				});
			}

//			Observable.Start ().Delay (TimeSpan.FromMilliseconds (2000)).Subscribe (() => {
//				if(endAnimate != null)
//				{
//					endAnimate.Invoke();
//				}
//			});

//			Observable.Interval(TimeSpan.FromSeconds(1)).Subscribe(()
//				=> Console.WriteLine("hello word"));

//			Observable.Delay (TimeSpan.FromSeconds (2), () => {
//				if(endAnimate != null)
//				{
//					endAnimate.Invoke();
//				}
//			});

//			Observable.Delay (TimeSpan.FromSeconds (2)).Subscribe (() => {
//				if(endAnimate != null)
//				{
//					endAnimate.Invoke();
//				}
//			});

//			Observable.Delay(this,TimeSpan.FromSeconds(2)).Subscribe(() => {
//								if(endAnimate != null)
//								{
//									endAnimate.Invoke();
//								}
//							});

			//TODO: must remove all image from supperview.
		}
	}
}

