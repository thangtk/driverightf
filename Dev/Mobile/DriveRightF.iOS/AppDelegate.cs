﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using DriveRightF.iOS;
using DriveRightF.Core;
using CoreGraphics;
using CoreAnimation;
using Unicorn.Core;
using Unicorn.Core.Navigator;
using DriveRightF.Core.iOS;
using DriveRightF.Core.Enums;
using Splat;
using ReactiveUI;
using DriveRightF.Core.Storages;
using CoreLocation;
using Unicorn.Core.iOS;
using AVFoundation;

namespace ProjectName.iOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		private static bool _isShowPopup = false;

		// class-level declarations
		UIWindow window;

		//
		// This method is invoked when the application has loaded and is ready to run. In this
		// method you should instantiate the window, load the UI into it and then make the window
		// visible.
		//
		// You have 17 seconds to return from this method, or iOS will terminate your application.
		//
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			// Set the text color of status bar is white
			UIApplication.SharedApplication.SetStatusBarStyle (UIStatusBarStyle.Default, false);

			// create a new window instance based on the screen size

			window = new UIWindow (UIScreen.MainScreen.Bounds);

			Locator.CurrentMutable.RegisterConstant (new UnicorniOSDefaultPropertyBinding (), typeof(IDefaultPropertyBindingProvider));
			Locator.CurrentMutable.RegisterConstant (new UnicorniOSObservableForWigets (), typeof(ICreatesObservableForProperty));
			// If you have defined a view, add it here:

			Navigator nav = new Navigator (window);
			DependencyService.RegisterGlobalInstance<INavigator> (nav);
//			DependencyService.Get<DriveRightRepository> ();

			nav.Navigate ((int)Screen.Splash, null);

			// make the window visible
			window.MakeKeyAndVisible ();
			MyDebugger.ShowThreadInfo ("Main");
			//
			AskPermissionToReceiveNotification ();
//			Allow playing other audio
			AVAudioSession.SharedInstance().SetCategory(AVAudioSessionCategory.Ambient);
			// TEST
//			DependencyService.Get<GpsNotificationManager> ().RequestAuthorization();
			//
			return true;
		}

		#region GPS Checking

		public override void DidEnterBackground (UIApplication application)
		{
			System.Diagnostics.Debug.WriteLine (">>>>>>>>>>> DidEnterBackground");
			DependencyService.Get<GpsNotificationManager> ().RegisterNofifyGpsOff ();
		}

		public override void WillEnterForeground (UIApplication application)
		{
			System.Diagnostics.Debug.WriteLine (">>>>>>>>>>> WillEnterForeground");
			DependencyService.Get<GpsNotificationManager> ().UnRegisterNotifyGpsOff ();
		}

		private void AskPermissionToReceiveNotification ()
		{
			if (DeviceUtil.OSVersion >= 8) {
				var settings = UIUserNotificationSettings.GetSettingsForTypes (
					               UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound
					, null);
				UIApplication.SharedApplication.RegisterUserNotificationSettings (settings);
			} else {
				UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
				UIApplication.SharedApplication.RegisterForRemoteNotificationTypes (notificationTypes);
			}
		}

		public override void ReceivedLocalNotification (UIApplication application, UILocalNotification notification)
		{
			ApplicationStatus status = DependencyService.Get<ApplicationUtil> ().Status;

			if (status == ApplicationStatus.AtOtherTimes) {
				if (_isShowPopup == false) {
					ShowPopupGPSOff ();
					_isShowPopup = true;
				} else {
					_isShowPopup = false;
				}
			}
		}

		#endregion

		public override void OnActivated (UIApplication application)
		{
			ApplicationStatus status = DependencyService.Get<ApplicationUtil> ().Status;

			if (status == ApplicationStatus.AtOtherTimes) {
				if (_isShowPopup == false) {
					ShowPopupGPSOff ();
				} else {
					_isShowPopup = false;
				}
			} else if (status == ApplicationStatus.AtFirstTime) {
				DependencyService.Get<ApplicationUtil> ().Status = ApplicationStatus.AtOtherTimes;
			}
		}

		private void ShowPopupGPSOff()
		{
			DependencyService.Get<GpsNotificationManager> ().RequestAuthorization ();
			// reset our badge
			UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
			GpsNotificationManager.NumberBagdes = 0;
		}
	}
}