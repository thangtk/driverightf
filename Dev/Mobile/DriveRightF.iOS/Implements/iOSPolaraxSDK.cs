﻿using System;
using DriveRightF.Core;
using PolaraxSDKBinding;
using DriveRightF.Core.Managers;
using Unicorn.Core;
using DriveRightF.Core.Models;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.iOS.iOSPolaraxSDK))]
namespace DriveRightF.iOS
{
	public class iOSPolaraxSDK : IPolaraxSDK
	{
		public bool IsEnable {
			get {
				return AppConfig.SDK_ENABLED;
			}
		}

		private UserManager UserManager {
			get { return DependencyService.Get<UserManager> ();}
		}


		#region IPolaraxSDK implementation
		public void StartService (string customerId, Action<SDKResponse> result)
		{
			if (!IsEnable)
				return;

			PolaraxSDK.Init (customerId);
			PolaraxSDK.StartService(x =>
				{
					SDKResponse response = new SDKResponse();
					if(x == null || x.StatusCode == PolaraxSDKStatus.START_SUCCESS)
					{
						response.Response = "success";
						response.IsSuccess = true;
					}
					else
					{
						response.Response = "failed";
						response.IsSuccess = false;

						response.ErrorMessage = x.StatusMessage;
					}

					if(result != null)
					{
						result(response);
					}
				});
		}



		public void StartService (Action<SDKResponse> result)
		{
			// TEST
			if (!IsEnable)
				return;

			string customerRef = UserManager.GetCustomerRef();
			if (!string.IsNullOrEmpty (customerRef)) {
				StartService (customerRef, result);
			} else {
				SDKResponse response = new SDKResponse()
				{
					IsSuccess = false,
					ErrorMessage = "Empty customer reference"
				};

				if (result != null) {
					result (response);
				}
			}
		}

		public bool IsEnable3G {
			//Test
			get {
				if (!IsEnable)
					return true;
				return PolaraxSDK.Get3GSetting ();
			}
			set {
				if (!IsEnable)
					return;
				PolaraxSDK.Change3GSetting (value);
			}
		}
		#endregion
		
	}
}

