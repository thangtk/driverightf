﻿using System;
using Unicorn.Core.Navigator;
using Unicorn.Core.UI;
using UIKit;
using DriveRightF.Core.iOS;
using DriveRightF.Core.Enums;
using DriveRightF.Core.ViewModels;

[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.iOS.Navigator))]
namespace DriveRightF.iOS
{
	public class Navigator : UBaseNavigator
	{
		public Navigator(UIWindow window) : base(window)
		{
		}

		protected override UBaseViewController CreateViewController(int s)
		{
			Screen screen = (Screen)s;
			switch (screen)
			{
				case Screen.Home:
//				return new AccountStatementScreen();
//				MenuHomeScreen home = new MenuHomeScreen ();
					return new HomeScreen ();
//				home.LeftMenuViewController = new LeftMenuScreen ();
//				return home;
				case Screen.GetStart:
					return new GetStartedScreen ();
				case Screen.Award:
//				return new AwardScreen ();
					return new AwardHomeScreen ();
				case Screen.Signup:
					return new SignupScreen ();
				case Screen.SecondSignup:
					return new SecondStepSignupScreen ();
				case Screen.ThirdSignup:
					return new ThirdStepSignupScreen ();
				case Screen.Trips:
					return new ListTripScreen ();
				case Screen.Profile:
					return new ProfileScreenX ();
				case Screen.AwardDetail:
					return new AwardDetailScreen ();
				case Screen.Splash:
					return new SplashScreen ();
				case Screen.TripDetail:
					return new TripDetailScreen ();		
				case Screen.Setting:
					return new SettingViewController ();
				case Screen.About:
					return new AboutViewController ();
				case Screen.Legal:
					return new LegalViewController ();
				case Screen.Help:
					return new HelpViewController ();
				case Screen.Gamble:
					return new SlotMachineScreen ();
				case Screen.Test:
					return new SlotMachineScreen ();
				case Screen.TripStatement:
					return new TripStatementScreen ();
				case Screen.AccountStatement:
					return new AccountStatementScreen ();
				case Screen.AccountDetail:
					return new AccountDetailScreen ();
				case Screen.TripStatementInfo:
					return new ScoreInformationViewController ();
				case Screen.NewHome:
					return new HomeNewScreen ();
				case Screen.Menu:
					return new MenuHomeScreen ();

			}
			throw new Exception ("Unsupport view controller");
		}
	}
}

