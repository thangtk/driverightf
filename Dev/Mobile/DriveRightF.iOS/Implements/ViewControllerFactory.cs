﻿using System;
using Unicorn.Core.UI;
using DriveRightF.Core.Enums;
using System.Collections.Concurrent;
using Unicorn.Core.Navigator;


[assembly:Unicorn.Core.Dependency.Dependency(typeof(DriveRightF.iOS.ViewControllerFactory))]
namespace DriveRightF.iOS
{
	public class ViewControllerFactory:UBaseViewControllerFactory
	{
		

		protected override UBaseViewController CreateViewInnerController(int s)
		{
			Screen screen = (Screen)s;
			switch (screen)
			{
				case Screen.Home:
					return new HomeScreen ();
				case Screen.GetStart:
					return new GetStartedScreen ();
				case Screen.Award:
//				return new AwardScreen ();
					return new AwardHomeScreen ();
				case Screen.Signup:
					return new SignupScreen ();
				case Screen.SecondSignup:
					return new SecondStepSignupScreen ();
				case Screen.ThirdSignup:
					return new ThirdStepSignupScreen ();
				case Screen.Profile:
					return new ProfileScreenX ();
				case Screen.HomeMore:
					return new MenuHomeScreen ();
				case Screen.Splash:
					return new SplashScreen ();
				case Screen.Setting:
					return new SettingViewController ();
				case Screen.About:
					return new AboutViewController ();
				case Screen.Legal:
					return new LegalViewController ();
				case Screen.Help:
					return new HelpViewController ();
				case Screen.TripStatementInfo:
					return new ScoreInformationViewController ();
				case Screen.HomeAward:
//				return new AwardScreen ();
					return new AwardHomeScreen();
//					return new HomeNewScreen ();
				case Screen.HomeAccount:
//				return new AccountScreen ();
					return new AccountHomeScreen ();
				case Screen.HomeTrips:
//				return new TripsScreen ();
					return new TripHomeScreen ();
//				return new ProfileScreenX();

				case Screen.TabAward:
					return new AwardTabScreen ();
//					return new ProfileScreenX();
				case Screen.TabAccount:
					return new AccountTabScreen ();
				case Screen.TabTrips:
					return new TripTabScreen ();
				case Screen.TabProfile:
					return new ProfileTabScreen ();
				case Screen.TabMore:
					return new MenuTabScreen ();
				case Screen.TabMain:
					return new HomeMainTabScreen ();

			
				case Screen.AwardDetail:
					return new AwardDetailScreen ();
				case Screen.CardDetail:
					return new CardDetail ();
				case Screen.Gamble:
					return new SlotMachineScreen ();
				case Screen.AccountStatement:
					return new AccountStatementScreen ();
				case Screen.AccountDetail:
					return new AccountDetailScreen ();
				case Screen.TripStatement:
					return new TripStatementScreen ();
				case Screen.Trips:
					return new ListTripScreen ();
				case Screen.TripDetail:
					return new TripDetailScreen ();
				case Screen.Notification:
					return new NotificationScreen ();
				case Screen.NewHome:
					return new HomeNewScreen ();
				case Screen.Disclaimer:
					return new DisclaimerViewController ();
				case Screen.ProductIntroduction:
					return new ProductionViewController ();
			}
			throw new Exception ("Unsupport view controller");
		}
	}
}

