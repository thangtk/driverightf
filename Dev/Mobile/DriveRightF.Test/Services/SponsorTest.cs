﻿using System;
using NUnit.Framework;
using DriveRightF.Services;
using DriveRightF.Core;
using DriveRightF.Services.Responses;
using DriveRightF.Services.Requests;
using DriveRightF.Core.Models;
using System.Collections.Generic;

namespace DriveRightF.Test.iOS
{
	[TestFixture]
	public class SponsorTest
	{
		[Test]
		public void GetListSponsor ()
		{
			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
			ListSponsorResponse result = service.InvokeGetWebService<ListSponsorResponse> ("drivers/030744b6-07cc-4f73-97bb-433959337080/sponsorships/", new ListSponsorRequest (){

			});
			Console.WriteLine (">>>>>>>>>>>>>>>>>result.Count:"+result.Count);
			Assert.IsTrue (result.IsSuccess);
			Assert.IsTrue (result.ListSponsor.Count > 0);
//			foreach (var sponsor in result.ListSponsor) {
//				Console.WriteLine (">>>>>>>>>>>>>>>>>"+sponsor.SponsorID+" >> score:"+sponsor.IconURL);
//			}
			foreach (var sponsor in result.ListSponsor) {
//				sponsor.IconURL = string.Format(result.BaseURL+"sponsors/{0}/icon?format=image",GetSponsorId(sponsor.SponsorID));
				Console.WriteLine (">>>>>>>>>>>>sponsor.IconURL:"+sponsor.IconURL);
			}
		}

//		public string GetSponsorId(string sponsorShipId){
//			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
//			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
//			string path = string.Format("drivers/{0}/sponsorships/{1}/","030744b6-07cc-4f73-97bb-433959337080",sponsorShipId);
//			var response = service.InvokeGetWebService<SonsorShipDetailResponse> (path, new SonsorShipDetailRequest ());
//			return response.SponsorUuid;
//		}
	}
}

