﻿using System;
using NUnit.Framework;
using DriveRightF.Services;
using DriveRightF.Core;
using DriveRightF.Services.Responses;
using DriveRightF.Services.Requests;
using DriveRightF.Core.Models;
using Unicorn.Core;
using DriveRightF.Core.Services;

namespace DriveRightF.Test.iOS
{
	[TestFixture]
	public class ContractTest
	{
		[Test]
		public void ListContract ()
		{
			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
			ListContractResponse result = service.InvokeGetWebService<ListContractResponse> ("drivers/030744b6-07cc-4f73-97bb-433959337080/sponsorships/2727a772-6718-4eef-86e9-04a82328c4c9/contracts/", 
				new ListContractRequest (){

			});
			Console.WriteLine (">>>>>>>>>>>>>>>>>result.Count:"+result.Count);
			Assert.IsTrue (result.IsSuccess);
			Assert.IsTrue (result.ListContract.Count > 0);
			foreach (var contract in result.ListContract) {
				Console.WriteLine (">>>>>>>>>>>>>>>>>"+contract.ContractId+" >> score:"+contract.Insurer);
			}
		}

		[Test]
		public void ContractDetail ()
		{
			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
			ContractDetailResponse result = service.InvokeGetWebService<ContractDetailResponse> ("drivers/030744b6-07cc-4f73-97bb-433959337080/sponsorships/2727a772-6718-4eef-86e9-04a82328c4c9/contracts/c1213da5-38e2-4b19-93df-d8beba7dcee6/", new ContractDetailRequest (){

			});
			Console.WriteLine (">>>>>>>>>>>>>>>>>result.Count:"+result.PolicyExpiry);
			Assert.IsTrue (result.IsSuccess);
		}

		[Test]
		public void ContractUpdate ()
		{
			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
			UpdateContractResponse result = service.InvokePutWebService<UpdateContractResponse> ("drivers/030744b6-07cc-4f73-97bb-433959337080/sponsorships/2727a772-6718-4eef-86e9-04a82328c4c9/contracts/18184599-94e9-4d61-a3f3-8ea1ae405a2c/", 
				new UpdateContractRequest (){
					Contract = new Contract(){
						Insurer = "Test Insurer 10",
						PolicyNo="1234567890",
						PolicyExpiry=DateTime.Now.AddYears(10),
						LicenseNo="098765431",
						LicenceExpiry=DateTime.Now.AddYears(20),
						CarNo="XZ342432",
						CarModel= "Mercedes"
					}
				}
			);
			Console.WriteLine (">>>>>>>>>>>>>>>>>result.Count:"+result.PolicyExpiry);
			Assert.IsTrue (result.IsSuccess);

		}

		[Test]
		public void ContractAdd ()
		{
			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
			AddContractResponse result = service.InvokePostWebService<AddContractResponse> ("drivers/030744b6-07cc-4f73-97bb-433959337080/sponsorships/2727a772-6718-4eef-86e9-04a82328c4c9/contracts/", 
				new AddContractRequest (){
//					Contract = new Contract(){
						Insurer = "Test Insurer 3",
						PolicyNo="1234567890",
						PolicyExpiry=DateTime.Now.AddYears(10),
						LicenseNo="098765431",
						LicenceExpiry=DateTime.Now.AddYears(20),
						CarNo="XZ342432",
						CarModel= "Mercedes"
//					}
				}
			);
			Console.WriteLine (">>>>>>>>>>>>>>>>>result.Count:"+result.PolicyExpiry);
			Assert.IsTrue (result.IsSuccess);

		}

		[Test]
		public void ContractDetailServiceManager ()
		{
			var service =  DependencyService.Get<DriveRightFServiceManager> ();
			service.UserId = "030744b6-07cc-4f73-97bb-433959337080";
			var sponsor = service.GetSponsorRegistration ("2727a772-6718-4eef-86e9-04a82328c4c9");
			Console.WriteLine (">>>>>>>>>>>>>>>>"+sponsor.Insurance.Insurer);
		}

		[Test]
		public void ContractUpdateServiceManager ()
		{
			var service =  DependencyService.Get<DriveRightFServiceManager> ();
			service.UserId = "030744b6-07cc-4f73-97bb-433959337080";
			string sponsorId = "2727a772-6718-4eef-86e9-04a82328c4c9";
			SponsorRegistration sponsor = new SponsorRegistration () {
				CarRegistration = new CarRegistration () {
					SponsorId = sponsorId,
					CarModel = "tank",
					CarNo = "asdasd"
				},
				DriverLicense = new DriverLicense () {
					SponsorId = sponsorId,
					LicenseNo = "asdasdsd",
					Insurer = "test thoi test thoi",
					LicenseExpiry = DateTime.Now.AddYears(19)
				},
				Insurance = new Insurance () {
					SponsorId = sponsorId,
					PolicyNo = "sadasdsad",
					Insurer = "test thoi test thoi",
					Expiry = DateTime.Now.AddYears(19)

				}
			};
			service.UpdateSponsorRegistration (sponsor);

		}
	}
}

