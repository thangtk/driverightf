﻿using System;
using NUnit.Framework;
using DriveRightF.Services;
using DriveRightF.Core;
using DriveRightF.Services.Responses;
using DriveRightF.Services.Requests;

namespace DriveRightF.Test.iOS
{
	[TestFixture]
	public class ChallengeTest
	{
		[Test]
		public void ListChallenge ()
		{
			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
			ListChallengeResponse result = service.InvokeGetWebService<ListChallengeResponse> ("drivers/030744b6-07cc-4f73-97bb-433959337080/challenges/", new ListChallengeRequest (){

			});
			Console.WriteLine (">>>>>>>>>>>>>>>>>result.Count:"+result.Count);
			Assert.IsTrue (result.IsSuccess);
			Assert.IsTrue (result.ListEarning.Count > 0);
			foreach (var earning in result.ListEarning) {
				Console.WriteLine (">>>>>>>>>>>>>>>>>"+earning.EarningId+" >> score:"+earning.Archieve);
			}
		}
	}
}

