﻿
using System;
using NUnit.Framework;
using DriveRightF.Services;
using DriveRightF.Core;
using DriveRightF.Services.Requests;
using DriveRightF.Services.Responses;
using DriveRightF.Core.Services;

namespace DriveRightF.Test.iOS
{
	[TestFixture]
	public class TripTest
	{
		[Test]
		public void GetListTrip ()
		{
			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
			MonthlyTripResponse result = service.InvokeGetWebService<MonthlyTripResponse> ("drivers/030744b6-07cc-4f73-97bb-433959337080/trips/", new MonthlyTripRequest (){
				Month = "0115",
				PageSize=100
			});
			Console.WriteLine (">>>>>>>>>>>>>>>>>result.Count:"+result.Count);
			Assert.IsTrue (result.IsSuccess);
			Assert.IsTrue (result.ListTrip.Count > 0);
			foreach (var trip in result.ListTrip) {
				Console.WriteLine (">>>>>>>>>>>>>>>>>"+trip.TripId+" >> score:"+trip.Score);
			}
		}

		[Test]
		public void GetTripDetail ()
		{
			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
			TripDetailResponse result = service.InvokeGetWebService<TripDetailResponse> ("drivers/030744b6-07cc-4f73-97bb-433959337080/trips/cdd6d98f-03ff-40aa-a9ee-579f63794b86/", new TripDetailRequest (){
	
			});
			Assert.IsTrue (result.IsSuccess);
			Assert.IsTrue (result.Coordinates.Count > 0);
			foreach (var gps in result.Coordinates) {
				Console.WriteLine (">>>>>>>>>>>>>>>>>"+gps.Time+" >> score:"+gps.Latitude);
			}
			Assert.IsTrue (result.IsSuccess);
		}

		[Test]
		public void GetTripMonthDetail ()
		{
			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
			//app/drivers/030744b6-07cc-4f73-97bb-433959337080/summaries/0215
			GetTripMonthDetailResponse result = service.InvokeGetWebService<GetTripMonthDetailResponse> ("drivers/030744b6-07cc-4f73-97bb-433959337080/summaries/0215", new GetTripMonthDetailRequest (){

			});
			Assert.IsTrue (result.IsSuccess);
			Console.WriteLine (">>>>>>>>>>>"+result);
		}

		[Test]
		public void GetTripMonthSummary ()
		{
			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
			ListTripSummaryResponse result = service.InvokeGetWebService<ListTripSummaryResponse> ("drivers/030744b6-07cc-4f73-97bb-433959337080/summaries", new ListTripSummaryRequest (){
			});
			Console.WriteLine (">>>>>>>>>>>>>>>>>result.Count:"+result.Count);
			Assert.IsTrue (result.IsSuccess);
			Assert.IsTrue (result.ListTripMonth.Count > 0);
			foreach (var trip in result.ListTripMonth) {
				Console.WriteLine (">>>>>>>>>>>>>>>>>"+trip.MonthRaw+" >> score:"+trip.Score);
			}
		}

		[Test]
		public void GetTrip6MonthSummary ()
		{
			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
			GetTripMonthDetailResponse result = service.InvokeGetWebService<GetTripMonthDetailResponse> ("drivers/030744b6-07cc-4f73-97bb-433959337080/summaries/last6months", new GetTripMonthDetailRequest (){
			});
			Console.WriteLine (">>>>>>>>>>>>>>>>>result.Count:"+result.Month);
			Assert.IsTrue (result.IsSuccess);

		}

		[Test]
		public void GetTrip6MonthSummaryFromManager ()
		{
			DriveRightFServiceManager manager = new DriveRightFServiceManager ();
			manager.UserId = "030744b6-07cc-4f73-97bb-433959337080";
			var list = manager.GetTripMonthSummary ();
			foreach (var trip in list) {
				Console.WriteLine (">>>>>>>>>>>>>>>>"+trip.Score);
			}
//			Assert.IsTrue (manager.);

		}
	}
}
