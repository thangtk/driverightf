﻿using System;
using NUnit.Framework;
using DriveRightF.Services;
using DriveRightF.Core;
using DriveRightF.Services.Responses;
using DriveRightF.Services.Requests;

namespace DriveRightF.Test.iOS
{
	[TestFixture]
	public class PerkTest
	{
		[Test]
		public void GetListPerk ()
		{
			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
			ListPerkResponse result = service.InvokeGetWebService<ListPerkResponse> ("drivers/030744b6-07cc-4f73-97bb-433959337080/perks/", new ListPerkRequest (){

			});
			Console.WriteLine (">>>>>>>>>>>>>>>>>result.Count:"+result.Count);
			Assert.IsTrue (result.IsSuccess);
			Assert.IsTrue (result.ListPerk.Count > 0);
			foreach (var perk in result.ListPerk) {
				Console.WriteLine (">>>>>>>>>>>>>>>>>"+perk.PerkId+" >> score:"+perk.Expiry);
			}
		}
	}
}

