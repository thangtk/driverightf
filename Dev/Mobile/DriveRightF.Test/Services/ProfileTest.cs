﻿	using System;
using NUnit.Framework;
using DriveRightF.Services;
using DriveRightF.Core;
using DriveRightF.Services.Responses;
using DriveRightF.Services.Requests;
using DriveRightF.Core.Models;
using Unicorn.Core.Services.Base;
using Unicorn.Core;
using DriveRightF.Core.Services;
using UIKit;

namespace DriveRightF.Test.iOS
{
	[TestFixture]
	public class ProfileTest
	{
		[Test]
		public void UpdateProfile ()
		{
			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
			UpdateProfileResponse result = service.InvokePutWebService<UpdateProfileResponse> ("drivers/030744b6-07cc-4f73-97bb-433959337080/profiles/0006caa1-cd0c-41be-9e84-822cfb4e81b2/", 
				new UpdateProfileRequest (){
						Name="cuong.hoang.minh",
						PhoneNumber="324345435465",
						SocialSecurityNo="AASasASDAS",
						WeChat="dsadasdasdas",
						DOB = "1986-1-1"
				});
			Assert.IsTrue (result.IsSuccess);
		}

		[Test]
		public void UpdateProfileServiceManager ()
		{
			var service =  DependencyService.Get<DriveRightFServiceManager> ();
			service.UserId = "a49ef462-abc2-4283-854b-8b4b520ac200";
			var sponsor = service.UpdateProfile (new User(){
				Name="cuong.hoang.minh",
				PhoneNumber="324345435465",
				SocialSecurityNo="AASasASDAS",
				WeChat="dsadasdasdas",
				DOB = "1986-1-1"
			});
		}

		[Test]
		public void AddProfile ()
		{
			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
			UpdateProfileResponse result = service.InvokePostWebService<UpdateProfileResponse> ("drivers/a49ef462-abc2-4283-854b-8b4b520ac200/profiles/", 
				new UpdateProfileRequest (){
						Name="cuong.hoang.minh",
						PhoneNumber="324345435465",
						SocialSecurityNo="AASasASDAS",
						WeChat="dsadasdasdas",
						DOB = "1986-1-1"
				});
			Assert.IsTrue (result.IsSuccess);
		}
		[Test]
		public void GetProfileDetail ()
		{
			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
			ProfileDetailResponse result = service.InvokeGetWebService<ProfileDetailResponse> ("drivers/030744b6-07cc-4f73-97bb-433959337080/profiles/0006caa1-cd0c-41be-9e84-822cfb4e81b2/", 
				new ProfileDetailRequest ());
			Assert.IsTrue (result.IsSuccess);
			Console.WriteLine (result.DOB);

		}

		[Test]
		public void GetProfileList ()
		{
			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
			ListProfileResponse result = service.InvokeGetWebService<ListProfileResponse> ("drivers/030744b6-07cc-4f73-97bb-433959337080/profiles/", 
				new ListProfileRequest());
			//			Console.WriteLine (">>>>>>>>>>>>>>>>>result.Count:"+result.);
			Assert.IsTrue (result.IsSuccess);
			foreach (var prof in result.ListProfile) {
				Console.WriteLine (">>>>>>>>>>>>>>>>>"+prof.Uuid+" >> score:"+prof.Name);
			}
		}

		[Test]
		public void Login ()
		{
			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
			try{
			LoginResponse result = service.InvokeGetWebService<LoginResponse> ("logins/5gi4pontqrkyetlbmf9oma/09164993184121", 
				new LoginRequest());
				Console.WriteLine (">>>>>>>>>>>>>>>>>result.CustomerRef:"+result.DriverId);
				Assert.IsTrue (result.IsSuccess);
			}catch(ServiceException e){
				Assert.IsTrue (e.ErrorType == ErrorType.NotFound);
			}


		}

		[Test]
		public void Register ()
		{
			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
			RegisterResponse result = service.InvokePostWebService<RegisterResponse> ("logins/", 
				new RegisterRequest(){
//					Register = new Register(){ 
//					I25LNCINTAEQEQYWAJBFWW/0973606242
						Partner="5gi4pontqrkyetlbmf9oma",Customer="09164993186"
//				}
				});
			Console.WriteLine (">>>>>>>>>>>>>>>>>result.CustomerRef:"+result.Customer);
			Assert.IsTrue (result.IsSuccess);

		}
		[Test]
		public void UploadFile ()
		{
//			DriveRightFServiceConfig config = new DriveRightFServiceConfig();
//			DriveRightFRestFulService service = new DriveRightFRestFulService (config);
//			string path = "drivers/030744b6-07cc-4f73-97bb-433959337080/profiles/e7731ef3-7b1d-4d6c-86bf-43431d75ad4e/picture?format=json";//"drivers/030744b6-07cc-4f73-97bb-433959337080/profiles/e7731ef3-7b1d-4d6c-86bf-43431d75ad4e/picture";
//			//"/Users/wolf-leader/Desktop/star_active.png"
//			var imagedata = Convert.ToBase64String(UIImage.FromFile("/Users/wolf-leader/Desktop/star_active.png").ToNSData());
//			//			var imagedata = Convert.ToBase64String(ReadToEnd(new StreamReader("/Users/wolf-leader/Desktop/star_active.png")));
//			var result = service.InvokePutWebService<UploadFileResponse>(path,new UploadFileRequest(){
//				ImageBase64=imagedata
//			});

		}
		//http://ec2-54-154-200-156.eu-west-1.compute.amazonaws.com/app/logins/
	}
}

