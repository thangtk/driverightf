﻿using System;
using DriveRightF.Core;
using DriveRightF.Core.ViewModels;
using ReactiveUI;
using Unicorn.Core;
using DriveRightF.iOS;
using Unicorn.Core.Navigator;

namespace TestRUI
{
	public class Test1ViewModel: BaseViewModel
	{
		#region implemented abstract members of BaseViewModel

		protected override void InitCommands ()
		{
			throw new NotImplementedException ();
		}

		public override int ScreenToNotify {
			get {
				throw new NotImplementedException ();
			}
		}

		#endregion

		private ReactiveCommand<object> _btnNext;

		public ReactiveCommand<object> NextScreenCommand { get{return _btnNext;}}

		public Test1ViewModel ()
		{
			_btnNext = ReactiveCommand.Create ();
			 _btnNext.Subscribe((obj)=>{
				DependencyService.Get<INavigator>().Navigate((int)Screen.Screen2);
			});
		}
	}
}

