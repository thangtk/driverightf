﻿using System;
using Unicorn.Core.Navigator;
using Unicorn.Core.UI;
using UIKit;
using TestRUI;

namespace DriveRightF.iOS
{
	public class Navigator : UBaseNavigator
	{
//		static private Navigator _nav;
		static public Navigator Instance {
			get ;
			private set;
		}

		public Navigator (UIWindow window):base(window){
			Instance = this;
		}

		protected override UBaseViewController CreateViewController (int s){
			Screen screen = (Screen)s;
			switch (screen) {
			case Screen.Screen1:
				return new Test1 ();
			case Screen.Screen2:
				return new Test2 ();
			}
			throw new Exception ("Unsupport view controller");
		}
	}
}

