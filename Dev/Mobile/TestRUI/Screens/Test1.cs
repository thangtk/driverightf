﻿
using System;
using System.Drawing;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.iOS;
using Unicorn.Core;
using Unicorn.Core.Navigator;

namespace TestRUI
{
	public partial class Test1 : DriveRightBaseViewController<Test1ViewModel>
	{


		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			ViewModel = new Test1ViewModel ();
			View.BackgroundColor = UIColor.Blue;
			base.ViewDidLoad ();

			_to2.TouchDown+= delegate {
				DependencyService.Get<INavigator>().Navigate((int)Screen.Screen2);
			};
			// Perform any additional setup after loading the view, typically from a nib.
		}
		public override void ReloadView(){
		}
		public override void LoadData(params object[] objs){
		}
		public override bool IsShouldCache(){
			return true;
		}
	}
}

