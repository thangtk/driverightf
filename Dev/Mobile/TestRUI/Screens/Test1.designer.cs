﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace TestRUI
{
	[Register ("Test1")]
	partial class Test1
	{
		[Outlet]
		UIKit.UIButton _to2 { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (_to2 != null) {
				_to2.Dispose ();
				_to2 = null;
			}
		}
	}
}
