﻿
using System;
using System.Drawing;

using Foundation;
using UIKit;
using DriveRightF.Core;
using DriveRightF.iOS;
using Unicorn.Core;
using Unicorn.Core.Navigator;

namespace TestRUI
{
	public partial class Test2 : DriveRightBaseViewController<Test2ViewModel>
	{


		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			View.BackgroundColor = UIColor.Red;
			base.ViewDidLoad ();

			back.TouchDown += delegate {
				DependencyService.Get<INavigator>().NavigateBack(true);
			};
			// Perform any additional setup after loading the view, typically from a nib.
		}
		public override void ReloadView(){
		}
		public override void LoadData(params object[] objs){
		}
		public override bool IsShouldCache(){
			return true;
		}
	}
}

