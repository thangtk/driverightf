﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace TestRUI
{
	[Register ("Test2")]
	partial class Test2
	{
		[Outlet]
		UIKit.UIButton back { get; set; }
		public Test2():base("Test2",null){
		}
		void ReleaseDesignerOutlets ()
		{
			if (back != null) {
				back.Dispose ();
				back = null;
			}
		}
	}
}
